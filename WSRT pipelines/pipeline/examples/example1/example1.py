import pyfits
from pylab import *
f = '/local/pipeline/examples/example1/example1.fits'
print 'LOADING FILE %s' % f
pyfits.info(f)
data = pyfits.getdata(f)
print 'DATA LOADED: %sx%sx%s IMAGE' % (data.shape[1],data.shape[2], data.shape[3])
fig = data[0,0,:,:]
print 'SHOWING FITS IMAGE'
imshow(fig)
figure()
lf = log10(fig - 1.01 * fig.min())
print 'SHOWING LOG10 PLOT'
imshow(lf, vmin=-4, origin='lower')
hold(True)
print 'SHOWING CONTOUR PLOT'
contour(lf, [-3, -2, -1])
figure()
print 'SHOWING HISTOGRAM'
hist(lf, bins=128)

