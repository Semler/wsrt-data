#!/usr/bin/env python
from WSRTrecipe import *
from ingredient import *

class export_ms(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['ExportID']            = None
        self.inputs['SequenceNumber']      = ''
        self.inputs['ZapExisting']         = True
        self.inputs['filepath']            = '.'
        self.inputs['InspectionPath']      = '/raid'
        self.inputs['AddInspection']       = True
        self.inputs['epochtype']           = 'J2000' ## j2convert options
        self.inputs['dofillpos']           = False
        self.inputs['unflag']              = False
        
        ## List of outputs
        self.outputs['MeasurementSets']   = []
        self.outputs['SequenceNumber']    = ''
        self.outputs['ReadmeFiles']       = []
        self.outputs['MSinfo']            = []
        
        ## Help text
        self.helptext = """
        Script to automatically retreive an observation from the
        archive and apply Epoch conversion. Results will be stored
        under filepath/ExportID/SequenceNumber."""
        ## example: ./export_ms.py -f/dop77_1/renting --ExportID=5678 -O10400060 -ZT -v
    
    ## Code to generate results ---------------------------------------------
    def create_jobdir(self):
        """Create the job directory, delete a previous one if ZapExisting"""
        import os, os.path
        Dir = self.inputs['filepath'] + '/' + str(self.inputs['ExportID'])
        if os.path.isdir(Dir):
            if self.inputs['ZapExisting']:
                self.zap(Dir)
                self.print_debug('Deleted: ' + Dir)
            else:
                raise RecipeError ('Directory already exists: ' + Dir)
        os.makedirs(Dir)
        return Dir

    def go(self):
        import os, os.path, pickle
        Dir         = self.create_jobdir()
        Ingredients = self.inputs.copy()
        MSDir       = Dir + '/' + self.inputs['SequenceNumber']
        os.makedirs(MSDir)
        Ingredients['filepath'] = MSDir

        self.cook_recipe('get_ms_from_WSRT_archive', Ingredients, Ingredients)
        ## Should have added 'MeasurementSets' to Ingredients
        self.cook_recipe('get_export_info', Ingredients, Ingredients)
        ## Should have added 'MSinfo' and 'ReadmeFiles' to Ingredients
        self.cook_recipe('convert_ms_to_epoch', Ingredients, Ingredients)
        ## This doesn't add anything to Ingredients

        self.outputs['MeasurementSets']   = Ingredients['MeasurementSets']
        self.outputs['ReadmeFiles']       = Ingredients['ReadmeFiles']
        self.outputs['MSinfo']            = Ingredients['MSinfo']
        self.outputs['SequenceNumber']    = self.inputs['SequenceNumber'] #I think this is no longer needed?
        fd = open(Dir + '/' + 'results.pickle', 'w')
        pickle.dump(self.outputs, fd)
        fd.close()

## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = export_ms()
    standalone.main()

