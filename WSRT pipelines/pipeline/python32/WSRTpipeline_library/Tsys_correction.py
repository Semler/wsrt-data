#!/usr/bin/env python
from WSRTrecipe import *

class Tsys_correction(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MiriadUvFiles'] = []
        self.inputs['filepath']      = '.'
        
        ## List of outputs
        self.outputs['CorrectedFiles'] = []
        
        ## Help text
        self.helptext = """
        Apply the system temperature correction
        using the attsys found in $MIRBIN
        Step 2.4 in SWOM"""
    
    ## Code to generate results ---------------------------------------------
    def go(self):
        import os
        for uv in self.inputs['MiriadUvFiles']:
            self.zap(self.inputs['filepath'] + '/' + str(uv) + '_temp')
#            self.cook_system('/dop89_0/oosterloo/suse9.2-32/wsrtMiriad/bin/linux/tsysMed', ' vis=' + self.inputs['filepath'] + '/' + str(uv) +
            self.cook_miriad('tsysmed', ' vis=' + self.inputs['filepath'] + '/' + str(uv) +
                                        ' out=' + self.inputs['filepath'] + '/' + str(uv) + '_temp') # + 
#                                        ' freq=1 source=1 fields=1 device=/ps')
            self.zap('pgplot.ps')
            self.zap(self.inputs['filepath'] + '/' + str(uv) + '_ts')
 #           self.cook_system('/dop89_0/oosterloo/suse9.2-32/wsrtMiriad/bin/linux/attsys', ' vis=' + self.inputs['filepath'] + '/' + str(uv) + '_temp' + 
            self.cook_miriad('attsys', ' vis=' + self.inputs['filepath'] + '/' + str(uv) + '_temp' + 
                                       ' out=' + self.inputs['filepath'] + '/' + str(uv) + '_ts' + 
                                       ' options=apply')
            self.zap(self.inputs['filepath'] + '/' + str(uv) + '_temp')
            self.outputs['CorrectedFiles'].append(str(uv) + '_ts')
    
## Stand alone execution code ------------------------------------------
if __name__ == "__main__":
    standalone = Tsys_correction()
    standalone.main()
