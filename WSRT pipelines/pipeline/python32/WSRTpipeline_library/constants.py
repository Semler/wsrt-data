##file with global constants

Low_Bandpass_Top = 0.1                ## Top fraction of the band which is
                                      ## is to be discarded due to low signal
Low_Bandpass_Bottom = 0.01            ## Bottom fraction of the band which is
                                      ## is to be discarded due to low signal
Narrow_Bandwidth_Limit = 10.0         ## Above this bandwidth (MHz) we use 
                                      ## invert with mfs
Central_Line_Block_Fraction = 0.33    ## Fraction of the band blocked for
                                      ## continuum fit (central part)


##imsize  = 257 # avoid 2**N, image size 2**N + 1 is good.  [or calculate from image]
startChan = 1

## Physical parameters
h21cm      = 1.42041
lightspeed = 3.0e5

WSRTshadow = 'shadow(27)' ## WSRT Disks have 27m shadow

## Tuple: (Dynamic Range, Clip Ratio, doAmplitude, Interval)
CleanSteps = {'Easy':
                [(100,     10.0, False, 5),
                 (500,     30.0, True, 2),
                 (1000, -1000.0, True, 2),
                 (1000, -1000.0, True, 1)],
              'Standard':
                [(25,      10.0, False, 5),
                 (50,      30.0, False, 3),
                 (200,     30.0, False, 2),
                 (500,  -1000.0, False, 2),
                 (1000, -1000.0, False, 1),
                 (1000, -1000.0, True, 1)],
              'Hard':
                [(3,      10.0, False, 5), # minimizes banana's ?
                 (25,      10.0, False, 5),
                 (50,      30.0, False, 5),
                 (200,  -1000.0, False, 3),
                 (500,  -1000.0, False, 2),
                 (1000, -1000.0, False, 2),
                 (1000, -1000.0, True, 1),
                 (1000, -1000.0, True, 1)]
                 }
## maybe add something about point sources vs. extended sources.
