#!/usr/bin/env python
from WSRTrecipe import *

class get_image_max(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MiriadImage'] = ''
        self.inputs['filepath']    = '.'
        
        ## List of outputs
        self.outputs['MiriadImage'] = ''
        self.outputs['min']         = 0.0
        self.outputs['max']         = 0.0
        
        ## Help text
        self.helptext = """
        Obtains maximum of image."""
    
    ## Code to generate results ---------------------------------------------
    def go(self):
        import os
        ## check which files already exist
        Min = Max = None
        self.messages.store = True
        self.cook_miriad('minmax', ' in=' + self.inputs['filepath'] + '/' + self.inputs['MiriadImage'])
        self.messages.store = False
        for i in range(-(min(len(self.messages), 10)),-1):
            line = self.messages[i][2]
            if 'Old minimum is' in line: ## if the min/max don't change this is the only line
                Min = line.split()[-5]
                Max = line.split()[-1]
            if 'New minimum is' in line: ## Will update Min and Max if they got updated.
                Min = line.split()[-5]
                Max = line.split()[-1]
        if Min and Max:
            self.outputs['min'] = float(Min)
            self.outputs['max'] = float(Max)
        else: raise RecipeError ('Unable to determine min and max')
        ## The Sun is 10000 Jy, CasA and CygA are 1000 Jy
        if float(Max) > 10000: raise RecipeError ('Sanity error: I do not beleive a max of: ' + str(Max))
        
        # We should only do this on succes
        self.outputs['MiriadImage'] = self.inputs['MiriadImage']
    
## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = get_image_max()
    standalone.main()
