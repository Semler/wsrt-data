#!/usr/bin/env python
from WSRTrecipe import *

class convert_ms_to_uvfits(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MeasurementSets'] = []
        self.inputs['filepath']        = '.'
        self.inputs['column']          = 'DATA' ## ms2uvfits options
        self.inputs['writesyscal']     = True
        self.inputs['multisource']     = True
        self.inputs['combinespw']      = True
        
        ## List of outputs
        self.outputs['UvfitsFiles'] = []
        
        ## Help text
        self.helptext = """
        Convert Measurement sets from the archive, currently using 
        ms2uvfits program from AIPS++, these need to be in your $PATH"""
    
## Code to generate results ---------------------------------------------
    def go(self):
        import os
        ## check which files already exist
        AlreadyExisted = os.listdir(self.inputs['filepath'])

        for ms in self.inputs['MeasurementSets']:
            ## run the ms2uvfits program and read it's output
            if self.cook_system('ms2uvfits_test', ' in=' + self.inputs['filepath'] + '/' + str(ms) + 
                                             ' out=' + self.inputs['filepath'] + '/' + ms.replace('MS','UVF') + 
                                             ' column=' + self.inputs['column'] +
                                             ' writesyscal=' + str(self.inputs['writesyscal'])[0] +
                                             ' multisource=' + str(self.inputs['multisource'])[0] +
                                             ' combinespw=' + str(self.inputs['combinespw'])[0]):
                raise RecipeError ('ms2uvfits failed on: ' + self.inputs['filepath'] + '/' + str(ms))

        ## check to see what we created
        list = os.listdir(self.inputs['filepath'])
        for file in list:
            if file not in AlreadyExisted:
                self.outputs['UvfitsFiles'].append(file)
        if not self.outputs['UvfitsFiles']:
            raise RecipeError ('No UVFITS files were created!')
    
## Stand alone execution code ------------------------------------------
if __name__ == "__main__":
    standalone = convert_ms_to_uvfits()
    standalone.main()
