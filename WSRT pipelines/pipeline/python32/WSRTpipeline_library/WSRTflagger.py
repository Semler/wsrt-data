#!/usr/bin/env python
from WSRTrecipe import *

class WSRTflagger(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MeasurementSets'] = []
        self.inputs['FlagMinimum']     = 4
        self.inputs['FlagMaximum']     = 4
        self.inputs['filepath']        = '.'
        
        ## List of outputs
        self.outputs['MeasurementSets'] = []
        self.outputs['AntennaFlags']    = []
        
        ## Help text
        self.helptext = """
        This function runs the WSRTflagger. It can only be used to
        flag Measurement sets and is therefore only used before conversion to Miriad"""
    
    ## Code to generate results ---------------------------------------------
    def go(self):
        import os
        length = len(self.messages)
        self.messages.store = True
        for uv in self.inputs['MeasurementSets']:
            status = self.cook_system('WSRT_flagger', ' ms=' + self.inputs['filepath'] + '/' + uv + 
                                      ' crosspol=false window=13 min=' +str(self.inputs['FlagMinimum']) + 
                                      ' max=' + str(self.inputs['FlagMaximum']) + 
                                      ' flagrms=true flagdata=true')
            if status != 0:
                raise RecipeError ('WSRTflagger failed with: ' + str(status))
        self.messages.store = False
        for i in range(length - len(self.messages), -1):
            line = self.messages[i][2]
            if 'Antennae (flagged %):' in line:
                line   = self.messages[i+1][2]
                values = [int(str(x)) for x in (line.split("%"))[:-1]]
        
        self.outputs['MeasurementSets'] = self.inputs['MeasurementSets']
        self.outputs['AntennaFlags']    = values
    
## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = WSRTflagger()
    standalone.main()

## Typical flagger output
## Band: 1
## RT0    RT1    RT2    RT3    RT4    RT5    RT6    RT7    RT8    RT9    RTA    RTB    RTC    RTD
## RT0    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## RT1    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## RT2    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## RT3    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## RT4    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## RT5    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## RT6    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## RT7    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## RT8    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## RT9    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## RTA    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## RTB    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## RTC    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## RTD    0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
## Bands (flagged %):    IVC0
## 0%
## Antennae (flagged %): RT0    RT1    RT2    RT3    RT4    RT5    RT6    RT7    RT8    RT9    RTA    RTB    RTC    RTD
## 0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%     0%
