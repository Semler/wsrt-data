#!/usr/bin/env python
from WSRTrecipe import *

class convert_ms_to_scn(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MeasurementSets'] = []
        self.inputs['filepath']        = '.'
        self.inputs['row']             = '-1' ## ms2scn options, with ms2scn defaults,
        self.inputs['haincr']          = '0'  ## export_scn has Ger de Bruyn defaults
        self.inputs['spwid']           = '-1'
        self.inputs['channel']         = '-1'
        self.inputs['factor']          = '1'
        self.inputs['applytrx']        = '1'
        self.inputs['autocorr']        = '0'
        self.inputs['showcache']       = '0'
        self.inputs['writeIF']         = '0'
        
        ## List of outputs
        self.outputs['ScnFiles'] = []
        
        ## Help text
        self.helptext = """
        Convert Measurement sets from the archive, currently using the j2convert and
        ms2uvfits programs from AIPS++, these need to be in your $PATH"""
    
## Code to generate results ---------------------------------------------
    def go(self):
        import os
        inputs = self.inputs
        ## check which files already exist
        AlreadyExisted = os.listdir(self.inputs['filepath'])

        for ms in inputs['MeasurementSets']:
            ## run the ms2uvfits program and read it's output
            if self.cook_system('ms2scn', ' in=' + inputs['filepath'] + '/' + str(ms) + 
                                          ' out=' + inputs['filepath'] + '/' + ms.replace('.MS','.SCN') +
                                          ' row=' + inputs['row'] +
                                          ' haincr=' + inputs['haincr'] +
                                          ' spwid=' + inputs['spwid'] +
                                          ' channel=' + inputs['channel'] +
                                          ' factor=' + inputs['factor'] +
                                          ' applytrx=' + inputs['applytrx'] +
                                          ' autocorr=' + inputs['autocorr'] +
                                          ' showcache=' + inputs['showcache'] +
                                          ' writeIF=' + inputs['writeIF']):
                raise RecipeError ('ms2scn failed on: ' + inputs['filepath'] + '/' + str(ms))

        ## check to see what we created
        list = os.listdir(self.inputs['filepath'])
        for file in list:
            if file not in AlreadyExisted:
                s = os.stat(self.inputs['filepath'] + '/' + file)
                if s.st_size < 1024:
                    raise RecipeError ('ms2scn failed (invalid size): ' + self.inputs['filepath'] + '/' + file)
                self.outputs['ScnFiles'].append(file)
        if not self.outputs['ScnFiles']:
            raise RecipeError ('No SCN files were created!')
    
## Stand alone execution code ------------------------------------------
if __name__ == "__main__":
    standalone = convert_ms_to_scn()
    standalone.main()
