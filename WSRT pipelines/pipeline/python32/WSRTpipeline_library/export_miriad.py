#!/usr/bin/env python
from WSRTrecipe import *
from ingredient import *

class export_miriad(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['ExportID']            = None
        self.inputs['SequenceNumber']      = ''
        self.inputs['ZapExisting']         = True
        self.inputs['DeleteIntermediates'] = True
        self.inputs['filepath']            = '.'
        self.inputs['InspectionPath']      = '/raid'
        self.inputs['AddInspection']       = True
        self.inputs['epochtype']           = 'J2000' ## j2convert options
        self.inputs['dofillpos']           = False
        self.inputs['unflag']              = False
        self.inputs['column']              = 'DATA' ## ms2uvfits options
        self.inputs['writesyscal']         = True
        self.inputs['multisource']         = True
        self.inputs['combinespw']          = True
        self.inputs['velocity']            = 'optbary' ## wsrtfits options
        
        ## List of outputs
        self.outputs['MiriadUvFiles'] = []
        
        ## Help text
        self.helptext = """
        Script to automatically retreive an observation from the
        archive and prepare it for use by Miriad"""
    
    ## Code to generate results ---------------------------------------------
    def create_jobdir(self):
        """Create the job directory, delete a previous one if ZapExisting"""
        import os, os.path
        Dir = self.inputs['filepath'] + '/' + str(self.inputs['ExportID'])
        if os.path.isdir(Dir):
            if self.inputs['ZapExisting']:
                self.zap(Dir)
                self.print_debug('Deleted: ' + Dir)
            else:
                raise RecipeError ('Directory already exists: ' + Dir)
        os.makedirs(Dir)
        return Dir

    def go(self):
        ## could just call export_uvfits for the fist part
        import os, os.path, pickle
        Dir         = self.create_jobdir()
        Ingredients = self.inputs.copy()
        MSDir       = Dir + '/' + self.inputs['SequenceNumber']
        os.makedirs(MSDir)
        Ingredients['filepath'] = MSDir

        self.cook_recipe('get_ms_from_WSRT_archive', Ingredients, Ingredients)
        ## Should have added 'MeasurementSets' to Ingredients
        self.cook_recipe('get_export_info', Ingredients, Ingredients)
        ## Should have added 'MSinfo' and 'ReadmeFiles' to Ingredients
        self.cook_recipe('convert_ms_to_epoch', Ingredients, Ingredients)
        ## This doesn't add anything to Ingredients
        self.cook_recipe('convert_ms_to_uvfits', Ingredients, Ingredients)
        ## Should have added 'UvfitsFiles' to Ingredients
        self.cook_recipe('convert_uvfits_to_miriad', Ingredients, Ingredients)
        ## Should have added 'MiriadUvFiles' to Ingredients

        if self.inputs['DeleteIntermediates']:
            for m in Ingredients['MeasurementSets']:
                self.zap(MSDir + '/' + m)
            for u in Ingredients['UvfitsFiles']:
                self.zap(MSDir + '/' + u)
        
        self.outputs['MiriadUvFiles']  = Ingredients['MiriadUvFiles']
        self.outputs['ReadmeFiles']    = Ingredients['ReadmeFiles']
        self.outputs['MSinfo']         = Ingredients['MSinfo']
        self.outputs['SequenceNumber'] = self.inputs['SequenceNumber']
        fd = open(Dir + '/' + 'results.pickle', 'w')
        pickle.dump(self.outputs, fd)
        fd.close()

## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = export_miriad()
    standalone.main()

