#!/usr/bin/env python
from WSRTrecipe import *

class concatenate_uvfiles(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MiriadUvFiles'] = []
        self.inputs['outputname']    = ''
        self.inputs['filepath']      = '.'
        
        ## List of outputs
        self.outputs['MiriadUvFile'] = ''
        
        ## Help text
        self.helptext = """
        This function concatenates a list of Miriad "uvfiles" into
        one "uvfile" using uvcat from in $MIRBIN"""
    
    ## Code to generate results ---------------------------------------------
    def go(self):
        import string, shutil
        temp = []
        if len(self.inputs['MiriadUvFiles']) == 1: ## then uvcat does basically a copy
            shutil.copytree(self.inputs['filepath'] + '/' + self.inputs['MiriadUvFiles'][0], 
                            self.inputs['filepath'] + '/' + self.inputs['outputname'])
            status = 0
        else:
            for m in self.inputs['MiriadUvFiles']:
                temp.append(self.inputs['filepath'] + '/' + m)
            status = self.cook_miriad('uvcat', ' vis=' + string.join(temp, ',') +
                                               ' out=' + self.inputs['filepath'] + '/' + self.inputs['outputname']);
        if status:
            raise RecipeError ('Concatenating files failed: ' + self.inputs['MiriadUvFiles'][0])
        self.outputs['MiriadUvFile'] = self.inputs['outputname']

## Stand alone execution code ------------------------------------------    
if __name__ == '__main__':
    standalone = concatenate_uvfiles()
    standalone.main()
