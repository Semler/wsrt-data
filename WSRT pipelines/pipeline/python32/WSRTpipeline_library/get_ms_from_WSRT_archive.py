#!/usr/bin/env python
from WSRTrecipe import *

class get_ms_from_WSRT_archive(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['SequenceNumber'] = ''
        self.inputs['filepath']       = '.'
        
        ## List of outputs
        self.outputs['MeasurementSets'] = []
        
        ## Help text
        self.helptext = """
        Gets the MS from the archive, currently using the script getms"""
    
## Code to generate results ---------------------------------------------
    def go(self):
        import os
        if not len(self.inputs['SequenceNumber']) == 8:
            raise RecipeError ('Observation number is not 8 characters long')
        ## get current directory so we can find getms, then change to dir
        cwd    = os.getcwd()
        self.inputs['filepath'] = os.path.abspath(self.inputs['filepath'])
        os.chdir(self.inputs['filepath'])
        AlreadyExisted = os.listdir(self.inputs['filepath'])
    
        ## run the getms script and read it's output
        status = self.cook_system('getms', str(self.inputs['SequenceNumber']))
        os.chdir(cwd) ## get back to the original dir
        if status:
            raise RecipeError ('getms failed')
        else:
            ## check to see what we created
            list = os.listdir(self.inputs['filepath'])
            for file in list:
                if file not in AlreadyExisted:
                    if file[-3:] == '.MS':
                        self.outputs['MeasurementSets'].append(file)
                    else:
                        self.print_debug(str("There is something in the archive that's not a normal measurement:") + file)
            if not self.outputs['MeasurementSets']:
                raise RecipeError ('getms created no Measurement sets')
    
## Stand alone execution code ------------------------------------------
if __name__ == "__main__":
    standalone = get_ms_from_WSRT_archive()
    standalone.main()
