#!/usr/bin/env python
from WSRTrecipe import *

class split_in_bands(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MiriadUvFile'] = ''
        self.inputs['filepath']     ='.'
        
        ## List of outputs
        self.outputs['MiriadUvFiles'] = []
        
        ## Help text
        self.helptext = """
        This function splits off a separate UV data set for each spectral window
        from a UV data set containing multiple bands using uvcat found in $MIRLIB
        Step 2.5 in SWOM"""
    
    # Code to generate results ---------------------------------------------
    def go(self):
        import mirlib
        NumBands = mirlib.varvalue(self.inputs['filepath'] + '/' + self.inputs['MiriadUvFile'], 'nspect')[0]
        sourcefile = self.inputs['filepath'] + '/' + self.inputs['MiriadUvFile']
        if NumBands > 1:
            for band in range(NumBands):
                self.zap(sourcefile + '_' + str(band))
                self.cook_miriad('uvcat', ' vis=' + sourcefile + 
                                          ' out=' + sourcefile + '_' + str(band) + 
                                          ' select=window(' + str(band+1) + ')')
                self.outputs['MiriadUvFiles'].append(self.inputs['MiriadUvFile'] + '_' + str(band))
        else:
            self.outputs['MiriadUvFiles'] = [ self.inputs['MiriadUvFile'] ]
    
# Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = split_in_bands()
    standalone.main()

