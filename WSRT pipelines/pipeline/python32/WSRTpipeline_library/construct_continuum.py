#!/usr/bin/env python
from WSRTrecipe import *

class construct_continuum(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MiriadUvFile']     = ''
        self.inputs['NumberOfChannels'] = 0
        self.inputs['UseMfs']           = False
        self.inputs['linedata']         = False
        self.inputs['filepath']         = '.'
        
        ## List of outputs
        self.outputs['ContinuumUvFile'] = ''

        ## Help text
        self.helptext = """
        Construct a continuum UV dataset, by flagging the innermost 30%. This uses
        uvcat, uvflag or uvlin found in $MIRLIB
        Steps 2.10.1 and 2.11.1,2 in SWOM"""
    
    ## Code to generate results ---------------------------------------------
    def go(self):
        import shutil
        from constants import Central_Line_Block_Fraction
        block_chan  = int(self.inputs['NumberOfChannels'] * Central_Line_Block_Fraction)
        start_block = int((self.inputs['NumberOfChannels'] - block_chan)/2)
        infile      = self.inputs['filepath'] + '/' + self.inputs['MiriadUvFile']
        outfile     = infile + '_cont'
        self.zap(outfile)
        if self.inputs['UseMfs']:
            ## Construct line string to flag the central portion of the band
            ## (line emission presumed to be there)
            self.print_message('Creating Wide band continuum')
            if not self.inputs['linedata']:
                self.outputs['ContinuumUvFile'] = self.inputs['MiriadUvFile']
                return
            else:
                line_str = ' line=channel,' + str(block_chan) + ',' + str(start_block)
                self.print_message('Wide band: '+ line_str)
                tempfile = infile + '_temp'
                self.zap(tempfile)
                shutil.copytree(infile, tempfile)
                self.cook_miriad('uvflag', ' vis=' + tempfile + 
                                             line_str + ' flagval=flag')
                self.cook_miriad('uvcat', ' vis=' + tempfile + 
                                          ' out=' + outfile + 
                                          ' options=unflagged')
                self.zap(tempfile)
        else:
            ## Construct line string to deselect the central
            ## Central_Line_Block_Fraction part of the band
            self.print_message('Creating narrow band continuum')
            end_block   = start_block + block_chan
            if self.inputs['linedata']:
                line_str    = ' chans=1,' + str(start_block) + ',' + str(end_block) + ',' + str(self.inputs['NumberOfChannels'])
                self.cook_miriad('uvlin', ' vis=' + infile + 
                                            line_str +
                                          ' out=' + outfile + 
                                          ' order=2 mode=chan0')
            else:
                self.cook_miriad('uvlin', ' vis=' + infile + 
                                          ' out=' + outfile + 
                                          ' order=2 mode=chan0')
        
        self.outputs['ContinuumUvFile'] = self.inputs['MiriadUvFile'] + '_cont'

## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = construct_continuum()
    standalone.main()

