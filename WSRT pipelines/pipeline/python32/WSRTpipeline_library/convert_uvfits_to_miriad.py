#!/usr/bin/env python
from WSRTrecipe import *

class convert_uvfits_to_miriad(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['UvfitsFiles'] = []
        self.inputs['filepath']    = '.'
        
        ## List of outputs
        self.outputs['MiriadUvFiles'] = []
        
        ## Help text
        self.helptext = """
        Convert uvfits files to Miriad format using wsrtfits
        this should be in your $MIRBIN"""
    
## Code to generate results ---------------------------------------------
    def go(self):
        import os
        ## check which files already exist
        AlreadyExisted = os.listdir(self.inputs['filepath'])

        for uv in self.inputs['UvfitsFiles']:
            ## run the wsrtfits program and read it's output
            self.cook_miriad('wsrtfits', ' in=' + self.inputs['filepath'] + '/' + str(uv) + 
                                         ' out=' + self.inputs['filepath'] + '/' + str(uv).replace('UVF','MIR') +
                                         ' op=uvin velocity=optbary')
        
        ## check to see what we created
        list = os.listdir(self.inputs['filepath'])
        for file in list:
            if file not in AlreadyExisted:
                self.outputs['MiriadUvFiles'].append(file)
    
## Stand alone execution code ------------------------------------------
if __name__ == "__main__":
    standalone = convert_uvfits_to_miriad()
    standalone.main()
