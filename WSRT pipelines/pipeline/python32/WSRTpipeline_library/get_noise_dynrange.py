#!/usr/bin/env python
from WSRTrecipe import *

class get_noise_dynrange(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MiriadImage'] = ''
        self.inputs['filepath']    = '.'
        
        ## List of outputs
        self.outputs['Noise']    = 0.0
        self.outputs['DynRange'] = 0.0
        self.outputs['Min']      = 0.0
        self.outputs['Max']      = 0.0

        ## Help text
        self.helptext = """
        Reads Miriad image and retrieves min, max, noise and dynamic range."""
    
## Code to generate results ---------------------------------------------
    def go(self):
#        from mirMinMax import mirMinMax
#        from mirStat import mirStat
        temp = self.inputs.copy()
        self.cook_recipe('get_image_max', temp, temp)
#        minmax = mirMinMax(self.inputs['filepath'] + '/' + self.inputs['MiriadImage'])
#        noise  = mirStat(self.inputs['filepath'] + '/' + self.inputs['MiriadImage'])
#        drange = (minmax.max - noise[0]) / noise[0]
        self.cook_recipe('get_noise', temp, temp)
        drange = (temp['max'] - temp['noise']) / temp['noise']
        self.outputs['Noise']    = temp['noise']
        self.outputs['DynRange'] = drange
        self.outputs['Min']      = temp['min'] #minmax.min
        self.outputs['Max']      = temp['max'] #minmax.max
    
## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = get_noise_dynrange()
    standalone.main()
