#!/usr/bin/env python
from WSRTrecipe import *
from ingredient import *

class preprocess_for_miriad(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['SequenceNumber']      = ''
        self.inputs['ZapExisting']         = False
        self.inputs['DeleteIntermediates'] = True
        self.inputs['FlagMinimum']         = 4
        self.inputs['FlagMaximum']         = 4
        self.inputs['filepath']            = '.'
        
        ## List of outputs
        self.outputs['MiriadUvFile']      = ''
        self.outputs['AntennaFlags']      = []
        
        ## Help text
        self.helptext = """
        Script to automatically retreive an observation from the
        archive and prepare it for use by Miriad"""
    
    ## Code to generate results ---------------------------------------------
    def create_jobdir(self):
        """Create the job directory, delete a previous one if ZapExisting"""
        import os, os.path
        Dir = self.inputs['filepath'] + '/' + self.inputs['SequenceNumber']
        if os.path.isdir(Dir):
            if self.inputs['ZapExisting']:
                self.zap(Dir)
                self.print_debug('Deleted: ' + Dir)
            else:
                old_results = self.get_run_info(Dir)
                if (old_results):
                    self.print_notification('Assuming existing file can be used')
                    self.outputs = old_results['outputs']
                    return ''
                else:
                    raise RecipeError ('Directory already exists: ' + Dir)
        os.makedirs(Dir)
        return Dir

    def go(self):
        import os, os.path
        Ingredients = self.inputs.copy()
        Dir = self.create_jobdir()
        if Dir == '': return
        Ingredients['filepath'] = Dir
        self.cook_recipe('get_ms_from_WSRT_archive', Ingredients, Ingredients)
        ## Should have added 'MeasurementSets' to Ingredients
        
        self.cook_recipe('convert_ms_to_epoch', Ingredients, Ingredients)
        ## This doesn't add anything
        self.cook_recipe('WSRTflagger', Ingredients, Ingredients)
        ## flags most of the bad Measurements
        self.cook_recipe('convert_ms_to_uvfits', Ingredients, Ingredients)
        ## Should have added 'UvfitsFiles' to Ingredients
        self.cook_recipe('convert_uvfits_to_miriad', Ingredients, Ingredients)
        ## Should have added 'MiriadUvFiles' to Ingredients
        self.cook_recipe('flag_shadow_autocorrelations', Ingredients, Ingredients)

        Ingredients['outputname']  = self.inputs['SequenceNumber'] 
        self.cook_recipe('concatenate_uvfiles', Ingredients, Ingredients)
        ## Should have added 'MiriadUvFile' to Ingredients

        if self.inputs['DeleteIntermediates']:
            for m in Ingredients['MeasurementSets']:
                self.zap(Dir + '/' + m)
            for u in Ingredients['UvfitsFiles']:
                self.zap(Dir + '/' + u)
            for f in Ingredients['MiriadUvFiles']:
                self.zap(Dir + '/' + f)
        
        Ingredients['MiriadUvFiles'] = [ Ingredients['MiriadUvFile'] ]
        self.cook_recipe('Tsys_correction', Ingredients, Ingredients)
        ## Should have added 'CorrectedFiles' to Data

        self.outputs['MiriadUvFile'] = Ingredients['CorrectedFiles'][0]
        self.outputs['AntennaFlags'] = Ingredients['AntennaFlags']
        self.set_run_info(Dir)

## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = preprocess_for_miriad()
    standalone.main()

