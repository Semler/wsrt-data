#!/usr/bin/env python
from WSRTrecipe import *
from ingredient import *
import davlib

class export_webdav(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['ExportID']        = None
        self.inputs['jobs']            = []
        self.inputs['destination']     = 'localhost'
        self.inputs['filepath']        = '.'
        self.inputs['resultdir']       = '/repository/export'
        self.inputs['StructureDetail'] = '/'
        
        ## List of outputs
        self.outputs['Inventory'] = []
        self.outputs['files']     = []
        
        ## Help text
        self.helptext = """
        Script to automatically move the files identified by
        JobNumbers to destination and report the moved files"""
        self.session_cookies = None
    
## Code to generate results---------------------------------------------
    def getConfig(self, name=None): ## also using the FTP config for WebDAV as it contains the same user/password
        ## Find the config file     ## might need to change in the future
        import os.path, re
        r_field = re.compile(r'(?s)([^\n:]+): (.*?)(?=\n[^ \t]|\Z)')
        cwd = os.path.realpath(os.path.dirname(__file__))  + '/'
        config  = cwd + '.nftprc'
        self.print_debug('using FTP cofiguration in: ' + config)
        if os.path.exists(config): 
            s = open(config).read()
        else: 
            raise RecipeError ('FTP config not found')
    
        ## Parse the config file
        conf = {}
        s = s.replace('\r\n', '\n')
        s = s.replace('\r', '\n')
        for item in s.split('\n\n'):
            meta = dict(r_field.findall(item.strip()))
            if meta.has_key('name'):
                fname = meta['name']
                del meta['name']
                conf[fname] = meta
            else: raise RecipeError ('FTP config must include a name')
    
        if name is not None: 
            return conf[name]
        else: return conf

    def wd_login(self):
        import binascii
        meta      = self.getConfig(self.inputs['destination'])
        auth      = binascii.b2a_base64(meta['username'] + ':' + meta['password'])
        self.dav  = davlib.DAV(meta['host'], meta['port']) 
        self.dav.remotedir = meta['remotedir']
#        self.dav.set_debuglevel(1) # for testing
        self.dav.connect()
        response = self.dav.get(self.dav.remotedir + self.inputs['resultdir'], {'Authorization': 'Basic %s' % auth.strip()})
        self.session_cookies = response.getheader('Set-Cookie')
        self.print_debug('Status = %s, Reason = %s, Version = %s' % (response.status, response.reason, response.version))
        if not (response.version >= 10):
            raise RecipeError ('Unknown protocol version:' + str(response.version))
        if not (response.status == 200 and response.reason == 'OK'):
            self.print_error('Got unrecognised answer with Status = %s, Reason = %s' % (response.status, response.reason))
            raise RecipeError ('Problem logging in to WebDAV default repository: ' + meta['host']+ ':' + str(meta['port']) + self.inputs['remotedir'])
        response.close()
        self.dav.close()

    def wd_get(self, folder):
        if not self.session_cookies:
            self.wd_login()
        self.dav.connect()
        response = self.dav.get(self.dav.remotedir + folder, {'Cookie':self.session_cookies})
        self.print_debug('Status = %s, Reason = %s, Version = %s' % (response.status, response.reason, response.version))
        if not (response.version >= 10):
            raise RecipeError ('Unknown protocol version:' + str(response.version))
        if not (response.status == 404 or response.status == 200): ##405 == does not exist, 200 == exists
            raise RecipeError ('Unknown status response:' + str(response.status) + ':' + str(response.reason))
        response.close()
        self.dav.close()
        return response.status
        
    def wd_mkdir(self, folder):
        if not self.session_cookies:
            self.wd_login()
        self.dav.connect()
        response = self.dav.mkcol(self.dav.remotedir + folder, {'Cookie':self.session_cookies})
        self.print_debug('Status = %s, Reason = %s, Version = %s' % (response.status, response.reason, response.version))
        if not (response.version >= 10):
            raise RecipeError ('Unknown protocol version:' + str(response.version))
        if not (response.status == 405 or response.status == 201): ##405 == exists, 201 == created
            raise RecipeError ('Unknown status response:' + str(response.status) + ':' + str(response.reason))
        response.close()
        self.dav.close()
        return response.status

    def wd_storbinary(self, filename, binfile):
        if not self.session_cookies:
            self.wd_login()
        self.dav.connect()
        response = self.dav.put(self.dav.remotedir + filename, binfile, None, None, {'Cookie':self.session_cookies})
        self.print_debug('Status = %s, Reason = %s, Version = %s' % (response.status, response.reason, response.version))
        if not (response.version >= 10):
            raise RecipeError ("Unknown protocol version:" + str(response.version))
        if not (response.status == 201): ##201 == created
            raise RecipeError ('Unexpected status response:' + str(response.status) + ':' + str(response.reason))
        response.close()
        self.dav.close()
        return response.status

    def upload(self, target): 
        import os.path
        path, filename = os.path.split(target[1])
        if filename.find('.readme') >= 0: #quick hack to detect readme files
            path = '/inspectionfiles/' + path
        else:
            path = '/datasets/' + path
        if self.wd_get(self.inputs['resultdir'] + path) == 404:
            self.print_message('Creating folder %s' % path)
            temppath = self.inputs['resultdir']
            for folder in path.split('/'): 
                temppath += '/' + folder
                self.wd_mkdir(temppath)
        binfile = open(target[0] + '/' + target[1], 'rb')
        self.print_message('Storing %s' % target[0] + '/' + target[1])
        try:
            ## does this raise an exception if something's wrong ?
            result = self.wd_storbinary(self.inputs['resultdir'] + path + '/' + filename, binfile) 
        except Exception, e:
            self.print_error('Unable to store file %s' % target[1])
            binfile.close()
            raise RecipeError ('WebDAV transfer failed with error: ' + str(e))
        binfile.close()

    def walk(self, path, subdir, target):
        """walks the path + subdir and returns subdir/*.*, total size in (targets, total)
        targets: source = [0]+[1], destination=[1]"""
        import os
        targets = []
        if os.path.isfile(path  + '/' + subdir + '/' + target):
            targets.append((path + '/' + subdir, target)) ## source = [0]+[1], destination=[1]
        else:
            for root, dirs, files in os.walk(path  + '/' + subdir + '/' + target):
                dir = root.replace(path  + '/' + subdir + '/', '')
                for f in files:
                    targets.append((path  + '/' + subdir, dir + '/' + f)) ## source = [0]+[1], destination=[1]
        return targets

    def go(self):
        import os.path, pickle
        targets = []
        files   = []
        for j in self.inputs['jobs']:
            try:
                fd = open(self.inputs['filepath'] + '/' + str(j) + '/results.pickle')
                results = pickle.load(fd)
                number  = results['SequenceNumber']
                for k in results.keys():
                    if k in ['MeasurementSets', 'UvfitsFiles', 'MiriadUvFiles', 'ScnFiles', 'ReadmeFiles']:
                        for f in results[k]:
                            targets.append(str(j) + '/' + str(number) + '/' + f)
                            files.extend(self.walk(self.inputs['filepath'],
                                         str(j) + '/' +
                                         str(results['SequenceNumber']), f))
            except:
                raise RecipeError ('loading and reading pickle for job failed: ' + str(j))
        for f in files:
            self.upload(f)
        self.session_cookies = None

        self.outputs['Inventory'] = targets
#            self.outputs['files']     = files

## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = export_webdav()
    standalone.main()

