#!/usr/bin/env python
from WSRTrecipe import *

class convert_ms_to_epoch(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MeasurementSets' ] = []
        self.inputs['filepath']         = '.'
        self.inputs['epochtype']        = 'J2000'
        self.inputs['dofillpos']        = False
        self.inputs['unflag']           = False
        
        ## List of outputs
        self.outputs['MeasurementSets'] = []
        
        ## Help text
        self.helptext = """
        Convert Measurement sets from the archive, currently using the j2convert
        program from AIPS++, these need to be in your $PATH, usually set by aipsinit"""
    
## Code to generate results ---------------------------------------------
    def go(self):
        for ms in self.inputs['MeasurementSets']:
            ## run the J2000 conversion program and read it's output
            if self.cook_system('j2convert', ' in=' + self.inputs['filepath'] + '/' + str(ms) + 
                                             ' type=' + self.inputs['epochtype'] +
                                             ' fillpos=' + str(self.inputs['dofillpos'])[0] +
                                             ' unflag=' + str(self.inputs['unflag'])[0]):
                raise RecipeError ('j2convert failed')
        self.outputs['MeasurementSets'] = self.inputs['MeasurementSets']
    
## Stand alone execution code ------------------------------------------
if __name__ == "__main__":
    standalone = convert_ms_to_epoch()
    standalone.main()
