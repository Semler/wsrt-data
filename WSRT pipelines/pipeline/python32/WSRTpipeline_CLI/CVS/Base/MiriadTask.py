"""

This module provides the MiriadTask class.  It adapts the Task class from
the Task module to be able to run Miriad tasks:

>>> tvclip = MiriadTask('tvclip')

The resulting class instance has all associated adverbs as attributes:

>>> print tvclip.ind
0.0
>>> imean.ind = 1
>>> print tvclip.indisk
1.0
>>> imean.indi = 2.0
>>> print tvclip.ind
2.0

"""

# Global AIPS defaults.
from Miriad import Miriad

# Generic Task implementation.
from MinimalMatch import MinimalMatch

# Generic Python stuff.
import glob, os, pickle, sys

class MiriadTask(MinimalMatch):

    """This class implements running Miriad tasks."""

    # Package.
    _package = 'Miriad'

    # Default version.
##    version = os.environ.get('VERSION', 'NEW')
    version = '0'

    # Default verbosity level.
    msgkill = 0

    # Default to batch mode.
    isbatch = 32000

    def __init__(self, name, **kwds): # kwds == keywords ?
##        MinimalMatch.__init__(self)
        self._name = name
        self._adverb_dict = {}
        self._input_list  = []
        self._output_list = []
        self._help_string = ''

        # Optional arguments.
        if 'version' in kwds:
            self.version = kwds['version']

        # See if there is a proxy that can hand us the details for
        # this task.
        params = None
        for proxy in Miriad.proxies:
            try:
                inst = getattr(proxy, self.__class__.__name__)
                params = inst.params(name, self.version)
            except:
                continue
            break
        if not params:
            msg = "%s task '%s' is not available" % (self._package, name)
            raise RuntimeError, msg

        # The XML-RPC proxy will return the details as a dictionary,
        # not a class.
        self._adverb_dict = params['adverb_dict']
        self._input_list = params['input_list']
        self._output_list = params['output_list']
        self._help_string = params['help_string']

        # Initialize all adverbs to their default values.
        self.__dict__.update(self._adverb_dict)

    def help(self):
        """Display help for this task."""

        if self._help_string:
            print self._help_string
##            pydoc.pager(self._help_string)


##    def defaults(self):
##        """Set adverbs to their defaults."""
##        self.__dict__.update(self._adverb_dict)
##
    def __display_adverbs(self, adverbs):
        """Display ADVERBS."""

        for adverb in adverbs:
            if self.__dict__[adverb] == '':
                print "'%s': ''" % adverb
            else:
                print "'%s': %s" % (adverb, self.__dict__[adverb])

    def inputs(self):
        """Display all inputs for this task."""
        self.__display_adverbs(self._input_list)

    def outputs(self):
        """Display all outputs for this task."""
        self.__display_adverbs(self._output_list)

    def spawn(self):
        """Spawn the task."""
        input_dict = {}
        for adverb in self._input_list:
            input_dict[adverb] = self.__dict__[adverb]

        # Figure out what proxy to use for running the task, and
        # translate the related disk numbers.
        url = None
        proxy = None
        for adverb in self._disk_adverbs:
            if adverb in input_dict:
                disk = int(input_dict[adverb])
                if disk == 0:
                    continue
                if not url and not proxy:
                    url = AIPS.disks[disk].url
                    proxy = AIPS.disks[disk].proxy()
                if AIPS.disks[disk].url != url:
                    raise RuntimeError, \
                          "AIPS disks are not on the same machine"
                input_dict[adverb] = float(AIPS.disks[disk].disk)
        if not proxy:
            raise RuntimeError, \
                  "Unable to determine where to execute task"

        inst = getattr(proxy, self.__class__.__name__)
        tid = inst.spawn(self._name, self.version, self.userno,
                         self.msgkill, self.isbatch, input_dict)
        return (proxy, tid)

    def finished(self, proxy, tid):
        """Determine whether the task specified by PROXY and TID has
        finished."""
        inst = getattr(proxy, self.__class__.__name__)
        return inst.finished(tid)

    def messages(self, proxy, tid):
        """Return messages for the task specified by PROXY and TID to
        finish."""

        inst = getattr(proxy, self.__class__.__name__)
        return inst.messages(tid)

    def wait(self, proxy, tid):
        """Wait for the task specified by PROXY and TID to finish."""

        while not self.finished(proxy, tid):
            self.messages(proxy, tid)
        inst = getattr(proxy, self.__class__.__name__)
        output_dict = inst.wait(tid)
        for adverb in self._output_list:
            self.__dict__[adverb] = output_dict[adverb]

    def go(self):
        """Run the task."""

        (proxy, tid) = self.spawn()
        count = 0
        rotator = ['|\b', '/\b', '-\b', '\\\b']
        while not self.finished(proxy, tid):
            msg = self.messages(proxy, tid)
            if msg:
                sys.stdout.write(msg)
            else:
                sys.stdout.write(rotator[count % 4])
                sys.stdout.flush()
            count += 1
        return self.wait(proxy, tid)

    def __call__(self):
        self.go()

    def __setattr__(self, name, value):
        (attr, dict) = self._findattr(name)
        # could do some validation here
        self.__dict__[attr] = value

# Tests.
if __name__ == '__main__':
    import doctest, sys
    doctest.testmod(sys.modules[__name__])
