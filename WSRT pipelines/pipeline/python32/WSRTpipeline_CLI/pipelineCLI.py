#!/usr/bin/env python
import ingredient, cook, message, MinimalMatch
PipelineLog = message.WSRTmessages()
PipelineLog.setloglevel(message.VerboseLevel, message.sys.stdout)
PipelineLog.append(message.VerboseLevel, 'Pipeline logging started in variable PipelineLog')

class CLICook(MinimalMatch.MinimalMatch):
    """Based on MinimalMatch by Mark Kettenis."""
    def __init__(self, task, inputs=None, outputs=None, messages=None):
        if inputs:  self.__dict__['_inputs']    = ingredient.WSRTingredient(inputs)
        else:       self.__dict__['_inputs']    = ingredient.WSRTingredient()  
        if outputs: self.__dict__['_outputs']   = ingredient.WSRTingredient(outputs)
        else:       self.__dict__['_outputs']   = ingredient.WSRTingredient()  
        if messages: self.__dict__['_messages'] = messages
        else:        self.__dict__['_messages'] = message.WSRTmessages()  
        self.__dict__['_task']     = task.strip()
        self.__dict__['_cook']     = None
        self.__dict__['_helptext'] = """
        This is a base cook class, no actual functionality implemented"""

    def sync(self):
        for adverb in self._inputs:
            self._cook.inputs[adverb] = self.__dict__[adverb]
        for adverb in self._outputs:
            self._cook.outputs[adverb] = self._outputs[adverb]

    def go(self):
        self.help()

    def help(self):
        """Shows helptext and inputs and outputs"""
        print self.__repr__()

    def __repr__(self):
        text = 'Cook: ' + self._task + '\n'
        text += self._helptext + '\n'
        text += '        Inputs:\n'
        text += self.__display_adverbs(self._inputs)
        text += '        Outputs:\n'
        text += self.__display_adverbs(self._outputs)
        text += """
        Use go() to start the cook.\n"""
        return text

    def _update_adverbs(self, adverbs):
        self.__dict__.update(adverbs)

    def __display_adverbs(self, adverbs):
        """Display ADVERBS."""
        text = ''
        for adverb in adverbs:
            if self.__dict__[adverb]:
                if type(adverbs[adverb]) == str:
                    text += "            '%s': '%s' [Default: '%s']\n" % (adverb, self.__dict__[adverb], adverbs[adverb])
                else:
                    text += "            '%s': %s [Default: %s]\n" % (adverb, self.__dict__[adverb], adverbs[adverb])
            else:
                if type(adverbs[adverb]) == list:
                    text += "            '%s': %s [No Default: can be a list]\n" % (adverb, self.__dict__[adverb])
                else:
                    text += "            '%s': %s [No Default]\n" % (adverb, self.__dict__[adverb])
        return text

    def inputs(self):
        """Display all inputs for this task."""
        print self.__display_adverbs(self._inputs)

    def outputs(self):
        """Display all outputs for this task."""
        print self.__display_adverbs(self._outputs)

    def __call__(self):
        self.go()

    def __setattr__(self, name, value):
        attr = self._findattr(name)
        # Validate based on the value already present.
        if hasattr(self, attr):
            if type(self.__dict__[attr]) == type(value):
                self.__dict__[attr] = value
            else:
                raise AttributeError (attr + ' is a of adifferent type')

#####################################################################

class PipelineCook(CLICook):
    def __init__(self, task, inputs=None, outputs=None):
        CLICook.__init__(self, task, inputs, outputs, PipelineLog)
        self.__dict__['_cook'] = cook.PipelineCook(self._task, self._inputs, self._outputs, self._messages)
        self._inputs.update(self._cook.recipe.inputs)
        self._outputs.update(self._cook.recipe.outputs)
        self._helptext = self._cook.recipe.helptext
        self._update_adverbs(self._inputs)
        self._update_adverbs(self._outputs)

    def go(self):
        self.sync()
        self._cook.spawn()
        self._outputs.update(self._cook.outputs)
        self._update_adverbs(self._outputs)

#################################################################

class SystemCook(CLICook):
    def __init__(self, task, inputs=None, outputs=None):
        CLICook.__init__(self, task, inputs, outputs, PipelineLog)
        self._outputs['exitstatus'] = 0
        self._update_adverbs(self._inputs)
        self._update_adverbs(self._outputs)

    def go(self):
        l = [self._task]
        #self.print_debug('running ' + command + ' ' + str(options))
        for adverb in self._inputs:
              l.append('%s%s' % (adverb, self.__dict__[adverb]))
        c = cook.SystemCook(command, l, {}, self.messages)
        c.spawn()
        while c.handle_messages():
            pass ## we could have a timer here
        c.wait()
        self._outputs['exitstatus'] = c.exitstatus

#################################################################

class MiriadCook(CLICook):
    def __init__(self, task, inputs=None, outputs=None):
        CLICook.__init__(self, task, inputs, outputs, PipelineLog)
        self._parse(task)
        self._update_adverbs(self._inputs)
        self._update_adverbs(self._outputs)

    def _parse(self, name):
        """Determine the proper attributes for the Miriad task name by
        parsing its help files. Version not used"""
        import os
        path  = os.environ['MIRBIN'] + '/'
        tasks = os.listdir(path)
        for t in tasks:
            if (t == name):
                p = os.popen3(path + 'doc %s | grep ^Keyword' % t)
                for line in p[1].readlines():
                    adverb = line[8:].strip()
                    self._inputs[adverb] = ''
                self._outputs['exitstatus'] = 0 # what outputs does a Miriad task have?
                p = os.popen3(path + 'doc %s' % t)
                self._helptext = p[1].read()

    def go(self):
        l = [self._task]
        for adverb in self._inputs:
            if self.__dict__[adverb]: #str(self.__dict__[adverb]): everything is a string currently
                l.append('%s=%s' % (adverb, self.__dict__[adverb]))
        c = cook.SystemCook(self._task, l, {}, self._messages)
        c.spawn()
        while c.handle_messages():
            pass ## we could have a timer here
        c.wait()
        self.__dict__['exitstatus'] = c.exitstatus

class GlishCook(CLICook):
    def __init__(self, task, inputs, outputs):
        CLICook.__init__(self, task, inputs, outputs, PipelineLog)
        self._update_adverbs(self._inputs)
        self._update_adverbs(self._outputs)

class AIPSCook(CLICook):
    def __init__(self, task, inputs, outputs):
        CLICook.__init__(self, task, inputs, outputs, PipelineLog)
        self.__dict__['_cook'] = cook.AIPSCook(self._task, self._inputs, self._outputs, self._messages)
        self._inputs.update(self._cook.task.inputs)
        self._outputs.update(self._cook.task.outputs)
        self._helptext = self._cook.task.helptext
        self._update_adverbs(self._inputs)
        self._update_adverbs(self._outputs)

    def go(self):
        self._cook.spawn()

class AIPS2Cook(CLICook):
    def __init__(self, task, inputs, outputs, messages):
        CLICook.__init__(self, task, inputs, outputs, PipelineLog)
        self._update_adverbs(self._inputs)
        self._update_adverbs(self._outputs)
