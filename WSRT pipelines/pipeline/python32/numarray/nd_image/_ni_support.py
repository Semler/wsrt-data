# Copyright (C) 2003-2005 Peter J. Verveer
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met: 
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#
# 3. The name of the author may not be used to endorse or promote
#    products derived from this software without specific prior
#    written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.      

import types
import numarray

def _extend_mode_to_code(mode):
    """Convert an extension mode to the corresponding integer code.
    """
    if mode == 'nearest':
        return 0
    elif mode == 'wrap':
        return 1
    elif mode == 'reflect':
        return 2
    elif mode == 'constant':
        return 4
    else:
        raise RuntimeError, 'boundary mode not supported'

def _normalize_sequence(input, array):
    """If input is a scalar, create a sequence of length equal to the
    rank of array by duplicating the input. If input is a sequence,
    check if its length is equal to the lenght of array.
    """
    if isinstance(array, numarray.NumArray):
        rank = array.rank
    else:
        rank = 1
    if (isinstance(input, (types.IntType, types.LongType,
                           types.FloatType))):
        normalized = [input] * rank
    else:
        normalized = list(input)
        if len(normalized) != rank:
            err = "sequence argument must have length equal to input rank"
            raise RuntimeError, err
    return normalized
    
def _get_output(output, shape, type, check_type = True):
    if output == None:
        output = numarray.zeros(shape, type = type)
        return_value = output
    else:
        if output.shape != shape:
            raise RuntimeError, "output shape not correct"
        if check_type and output.type() != type:
            raise RuntimeError, "output type not correct"
        return_value = None
    return output, return_value

## new stuff:

import warnings
def decrepate(output, output_type):
    if output_type != None:
        msg = "'output_type' argument is decrepated."
        msg += " Assign type to 'output' instead."
        raise RuntimeError, msg
        warnings.warn(msg, DeprecationWarning)
    if isinstance(output, numarray.NumericType):
        output_type = output
        output = None
    elif output != None:
        output_type = output.type()
    return output, output_type

def _normalize_frame(frame, cell, rank):
    if cell != None:
        if frame != None:
            raise RuntimeError, 'only one of frame/cell may be given'
        frame = cell
    elif frame == None:
        return []
    if (isinstance(frame, (types.IntType, types.LongType))):
        frame = [frame]
    result = []
    for ff in frame:
        if ff < 0:
            ff += rank
        if ff < 0 or ff >= rank:
            raise RuntimeError, "invalid frame"
        if ff not in result:
            result.append(ff)
    if cell != None:
        result = [ii for ii in range(rank) if ii not in result]
    return result
    
def _frame_to_int(frame):
    result = 0
    for ff in frame:
        result |= 1 << ff
    return result
            
def _normalize_sequence2(input, rank, frame = [], array_type = None):
    """If input is a scalar, create a sequence of length equal to the
    rank by duplicating the input. If input is a sequence,
    check if its length is equal to the length of array.
    """
    rank -= len(frame)
    if (isinstance(input, (types.IntType, types.LongType,
                           types.FloatType))):
        normalized = [input] * rank
    else:
        normalized = list(input)
        if len(normalized) != rank:
            err = "sequence argument must have length equal to input rank"
            raise RuntimeError, err
    return normalized
    
def _get_output2(output, input):
    if output == None:
        output = numarray.zeros(input.shape, type = input.type())
        return_value = output
    elif isinstance(output, numarray.NumericType):
        output = numarray.zeros(input.shape, type = output)
        return_value = output        
    else:
        if output.shape != input.shape:
            raise RuntimeError, "output shape not correct"
        return_value = None
    return output, return_value
    
def _check_axis(axis, rank):
    if axis < 0:
        axis += rank
    if axis < 0 or axis >= rank:
        raise ValueError, 'invalid axis'
    return axis
