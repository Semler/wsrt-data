import sys, time

##global Message Levels
ErrorLevel   = 70 ## Error
NotifyLevel  = 50 ## Always visible, stderr of external processes
VerboseLevel = 30 ## Visible on verbose, stdout of external processes
DebugLevel   = 10 ## Visible on debug

class WSRTmessages(list):
    """Class for handling message logging redirection and reporting tresholds."""
    def __init__(self):
        list.__init__(self)  ## inheriting from list for backward compatibility and debug reasons.
        self.log = {sys.stdout:NotifyLevel} ## NotifyLevel and above are always sent to stdout
        self.store = False
    
    def append(self, level, item):
        """level determines if the message gets reported, item should basically be a string"""
        t = time.gmtime()
        if self.store: list.append(self, (t, level, item)) ## storing the item for backward compatibility and debug reasons.
        for output in self.log.keys():
            if self.log[output] <= level:
                if level >= ErrorLevel:
                    e = ' Error   : '
                elif level >= NotifyLevel:
                    e = ' Notification: '
                elif level >= VerboseLevel:
                    e = '     Message : '
                elif level >= DebugLevel:
                    e = '        Debug: '
                output.write('%04d-%02d-%02d %02d:%02d:%02d' % (t[0], t[1], t[2], t[3], t[4], t[5])
                             + e + item.strip() + '\n')
                output.flush()

    def __repr__(self):
        text = ''
        for i in self:
            t     = i[0]
            level = i[1]
            item  = i[2]
            if level >= ErrorLevel:
                e = ' Error   : '
            elif level >= NotifyLevel:
                e = ' Notification: '
            elif level >= VerboseLevel:
                e = '     Message : '
            elif level >= DebugLevel:
                e = '        Debug: '
            text += ('%04d-%02d-%02d %02d:%02d:%02d' % (t[0], t[1], t[2], t[3], t[4], t[5])
                         + e + item.strip() + '\n')
        return text

    def addlogger(self, level, logger):
        """The level should be one of the above 
        global levels and the logger should support
        a write(string) method."""
        self.log[logger] = level

    def setloglevel(self, level, logger):
        """Changes the level at which logging info is written to the logger."""
        for output in self.log.keys():
            if logger == output:
                self.log[logger] = level
