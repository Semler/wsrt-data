import LocalProxy
from xmlrpclib import ServerProxy

class MiriadUrl:
    
    """Class representing a (possibly remote) AIPS disk.  An instance
       of this class stores an AIPS disk number and the URL of the
       proxy through which it can be accessed.  For local AIPS disks
       the URL will be None."""

    def __init__(self, url):
        self.url = url

    def proxy(self):
        """Return the proxy through which this AIPS disk can be
           accessed."""
        if self.url:
            return ServerProxy(self.url)
        else:
            return LocalProxy

class Miriad:
    
    # Not sure if I need this for Miriad
    urls  = [ MiriadUrl(None),
              MiriadUrl(None),
              MiriadUrl('http://localhost:8000'),
              MiriadUrl('http://localhost:8000') ]

    # List of available proxies.
    proxies = [ LocalProxy, ServerProxy('http://localhost:8000') ]
