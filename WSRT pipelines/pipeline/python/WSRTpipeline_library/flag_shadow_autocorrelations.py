#!/usr/bin/env python
from WSRTrecipe import *

class flag_shadow_autocorrelations(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MiriadUvFiles'] = []
        self.inputs['filepath']      = '.'
        
        ## List of outputs
        self.outputs['MiriadUvFiles'] = []

        ## Help text
        self.helptext = """
        This function discards autocorrelations, and flags for
        shadowing.
        Step 2.2 in SWOM"""

## Code to generate results ---------------------------------------------
    def go(self):
        from constants import WSRTshadow
        for uv in self.inputs['MiriadUvFiles']:
            ## run the uvflag program and read it's output
            status = self.cook_miriad('uvflag', ' vis=' + self.inputs['filepath'] + '/' + str(uv) + 
                                                ' select=auto,or,' + WSRTshadow + ' flagval=flag') 
        if status:
            raise RecipeError('Flagging dataset failed: ' + uv)
        # We should only do this on succes
        self.outputs['MiriadUvFiles'] = self.inputs['MiriadUvFiles']

## Stand alone execution code ------------------------------------------
if __name__ == "__main__":
    standalone = flag_shadow_autocorrelations()
    standalone.main()
