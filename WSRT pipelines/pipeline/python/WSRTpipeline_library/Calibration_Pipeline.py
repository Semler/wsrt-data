#!/usr/bin/env python
from WSRTrecipe import *
from ingredient import *

class Calibration_Pipeline(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['SourceNumber']      = ''
        self.inputs['CalibratorNumber']  = ''
        self.inputs['Difficulty']        = 'Standard'
        self.inputs['linedata']          = False
        self.inputs['ZapExisting']       = True
        self.inputs['KeepIntermediates'] = False
        self.inputs['Log']               = 'stdout'
        self.inputs['filepath']          = '.'
        
        ## List of outputs
        self.outputs['cleanmaps']         = []
        self.outputs['DynamicRanges']     = []
        self.outputs['Noises']            = []
        self.outputs['MiriadUvFiles']     = []
        self.outputs['CalibratedUvFiles'] = []
        
        ## Help text
        self.helptext = """
        Script to automatically reduce single pointing continuum or
        spectral line measurements from the WSRT.
        Possible Difficulty settings are: Hard, Standard, Easy."""
    
## Code to generate results ---------------------------------------------
    def go(self):
        if self.inputs['Log'] != 'stdout':
            f = open(self.inputs['Log'], 'a')
            self.messages.addlogger(DebugLevel, f)

        import os, os.path, mirlib
        CalDir    = self.inputs['filepath'] + '/' + self.inputs['CalibratorNumber'] 
        CalUv = WSRTingredient()
        CalUv['SequenceNumber']      = self.inputs['CalibratorNumber']
        CalUv['ZapExisting']         = False
        CalUv['DeleteIntermediates'] = not self.inputs['KeepIntermediates']
        CalUv['FlagMinimum']         = 10
        CalUv['FlagMaximum']         = 15
        CalUv['filepath']            = self.inputs['filepath']
        self.cook_recipe('preprocess_for_miriad', CalUv, CalUv)
        ## Should have added 'MiriadUvFile' to CalUv
        
        SourceDir = self.inputs['filepath'] + '/' + self.inputs['SourceNumber']
        SrcUv = WSRTingredient()
        SrcUv['SequenceNumber']      = self.inputs['SourceNumber']
        SrcUv['ZapExisting']         = False
        SrcUv['DeleteIntermediates'] = not self.inputs['KeepIntermediates']
        SrcUv['FlagMinimum']         = 5
        SrcUv['FlagMaximum']         = 7
        SrcUv['filepath']            = self.inputs['filepath']
        self.cook_recipe('preprocess_for_miriad', SrcUv, SrcUv)
        ## Should have added 'MiriadUvFile' to SrcUv
        
        SrcUv['filepath'] = SourceDir
        CalUv['filepath'] = CalDir
        ##for frequency dependency, but we want to use the same value for all bands
        SrcUv['cellsize'] = mirlib.cellsize(SourceDir + '/' +  SrcUv['MiriadUvFile'])

        SrcUv['SrcAntennaFlagging'] = SrcUv['AntennaFlags']
        SrcUv['CalAntennaFlagging'] = CalUv['AntennaFlags']
        self.cook_recipe('process_flagging', SrcUv, SrcUv)
        
        self.cook_recipe('split_in_bands', CalUv, CalUv)
        ## Should have replaced 'MiriadUvFiles' in CalUv
        self.cook_recipe('split_in_bands', SrcUv, SrcUv)
        ## Should have replaced 'MiriadUvFiles' in SrcUv
        
        for SourceFile in SrcUv['MiriadUvFiles']:
            try:
                BandUv = WSRTingredient()
                BandUv['MiriadUvFile'] = SourceFile
                BandUv['linedata']     = self.inputs['linedata']
                BandUv['filepath']     = SrcUv['filepath']
                BandUv['cellsize']     = SrcUv['cellsize']
    
                temp = WSRTingredient() ## temporary as it otherwise messes up SrcUv
                if len(SrcUv['MiriadUvFiles']) > 1:
                    temp['CalibratorUvFile']  = CalDir    + '/' + CalUv['MiriadUvFile'] + SourceFile[-2:]
                else:
                    temp['CalibratorUvFile']  = CalDir    + '/' + CalUv['MiriadUvFile']
                temp['ObservationUvFile'] = SourceDir + '/' + SourceFile
                self.cook_recipe('cross_calibrate', temp, temp)
            
                self.cook_recipe('galactic_flagging', BandUv, BandUv)
                
                from constants import Narrow_Bandwidth_Limit
                self.cook_recipe('miriad_inspect_uvdata', BandUv, BandUv)
                bandwidth = BandUv['ChannelWidth'] * BandUv['NumberOfChannels'] * 1000.0
                if bandwidth < Narrow_Bandwidth_Limit:
                    BandUv['UseMfs'] = False ## we only use channel 0, this gives some bandwidth smearing
                else:
                    BandUv['UseMfs'] = True ## we do one selfcal with the "MFS"
                
                self.cook_recipe('construct_continuum', BandUv, BandUv)
                BandUv['MiriadUvFile'] = BandUv['ContinuumUvFile'] 
    
                ## SWOM Steps 2.11.3-8
                from constants import CleanSteps
                ## Tuple: (Dynamic Range, Clip Ratio, doAmplitude, Interval)
                Steps = CleanSteps[ self.inputs['Difficulty'] ]
                for step in Steps:
                    BandUv['DynamicRange'] = step[0]
                    BandUv['ClipRatio']    = step[1]
                    BandUv['DoAmplitude']  = step[2]
                    BandUv['Interval']     = step[3]
                    self.cook_recipe('miriad_single_band_clean', BandUv, BandUv)
                
                SrcUv['MiriadImage'] = BandUv['CleanMap']
                self.cook_recipe('get_noise_dynrange', SrcUv, SrcUv)
                self.outputs['Noises'].append(SrcUv['Noise'])
                self.outputs['DynamicRanges'].append(SrcUv['DynRange']) ##please note this is a different DynamicRange as in the Steps
                self.outputs['cleanmaps'].append(BandUv['CleanMap'])
                self.outputs['CalibratedUvFiles'].append(BandUv['MiriadUvFile'])
            except:
                if len(SrcUv['MiriadUvFiles']) > 1:
                    self.outputs['cleanmaps'].append('')
                    self.outputs['Noises'].append(0.0)
                    self.outputs['DynamicRanges'].append(0.0)
                    self.outputs['MiriadUvFiles'].append('')
                    self.outputs['CalibratedUvFiles'].append('')
                    self.print_error('Failed processing band: ' + SourceFile[-1:])
                else: raise

        if BandUv['linedata']:
            self.outputs['MiriadUvFiles'] = SrcUv['MiriadUvFiles']

## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = Calibration_Pipeline()
    standalone.main()

