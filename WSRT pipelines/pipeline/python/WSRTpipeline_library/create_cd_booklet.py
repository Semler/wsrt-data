#!/usr/bin/env python
from WSRTrecipe import *
from ingredient import *

from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.lib import colors
import math, time

class XMLdata:
    def __init__(self, items):
        self.name  = 'EXP.' + items[0]['Semester'] + '-' + items[0]['PrjNr']
        t          = time.gmtime()
        self.date  = time.strftime('%B %d, %Y', t)
        self.items = items

class create_cd_booklet(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['filepath'] = '.'
#        self.inputs['XMLfile']  = ''
        self.inputs['XMLdata']  = ''
        self.inputs['CDindex']  = ''
        
        ## List of outputs
        self.outputs['PDFfile'] = []
        
        ## Help text
        self.helptext = """
        Script to automatically geretate a booklet that displays
        the information in the XMLFile in a PDF with suitable
        mark-up to create a CD-booklet. 
        It will print the booklet on rainbowds"""

    def frontpage(self, c, ObsData):
        c.rect(0,0, 121*mm, 119*mm, stroke=1, fill=0)
    
        c.setFont('Times-Roman', 24)
        c.drawString(6*mm, 106*mm, ObsData.name)
        c.line(6*mm, 105*mm, 116*mm, 105*mm)
        c.setFont('Times-Roman', 12)
        c.drawRightString(116*mm, 101*mm, 'Westerbork Synthesis Radio Telescope')
        c.setFont('Times-Roman', 10)
        c.drawRightString(116*mm, 5*mm, 'Recorded ' + ObsData.date + ' at the WSRT')
        
        c.drawInlineImage('../WSRTexport_pipeline/pic1.ps', 6*mm, 9*mm , 110*mm, 89*mm)
        c.drawInlineImage('../WSRTexport_pipeline/astron.ps', 96*mm, 106*mm, 21*mm, 8*mm)
    
    
    def datapage(self, c, ObsData, page, maxpage):
        c.setFont('Times-Roman', 8)
        c.setFillColor(colors.black)
        c.rect(0, 0, 120*mm, 119*mm, stroke=1, fill=0)
        c.drawString(3*mm, 115*mm, 'Table of Contents ' + ObsData.name + ' Page ' + str(page) + '/' + str(maxpage))
        c.drawString(3*mm, 112*mm, 'Name')
        c.drawString(34*mm, 112*mm, 'Format')
        c.drawString(45*mm, 112*mm, 'ObsName')
    
        c.line(1*mm, 111*mm, 71*mm, 111*mm)
        index = (page - 1)*32
        while((index < page*32) and (index < len(ObsData.items))):
            c.drawString(3*mm, 107.5*mm - (index % 32)*3.3*mm, ObsData.items[index]['filedir'])
            c.drawString(34*mm, 107.5*mm - (index % 32)*3.3*mm, ObsData.items[index]['type'])
            c.drawString(45*mm, 107.5*mm - (index % 32)*3.3*mm, (ObsData.items[index]['Semester'] + '/' +
                                                                 ObsData.items[index]['PrjNr'] + '/' +
                                                                 ObsData.items[index]['ObjName']))
            index += 1
        
    
    def backside(self, c, ObsData):
        c.setFont('Times-Roman', 8)
        c.setFillColor(colors.black)
        c.translate(45*mm, 45*mm)
        c.rect(0, 0, 137*mm, 119*mm, stroke=1, fill=0)
        c.rect(0, 0, -6.5*mm, 119*mm, stroke=1, fill=0)
        c.rect(137*mm, 0, 6.5*mm, 119*mm, stroke=1, fill=0)
        c.drawString(3*mm, 114*mm, 'Name')
        c.drawString(34*mm, 114*mm, 'Format')
        c.drawString(45*mm, 114*mm, 'ObsName')
    
        c.line(1*mm, 113*mm, 71*mm, 113*mm)
        index = 0
        while((index < 31) and (index < len(ObsData.items))):
            c.drawString(3*mm, 109.5*mm - (index % 32)*3.3*mm, ObsData.items[index]['filedir'])
            c.drawString(34*mm, 109.5*mm - (index % 32)*3.3*mm, ObsData.items[index]['type'])
            c.drawString(45*mm, 109.5*mm - (index % 32)*3.3*mm, (ObsData.items[index]['Semester'] + '/' +
                                                                 ObsData.items[index]['PrjNr'] + '/' +
                                                                 ObsData.items[index]['ObjName']))
            index += 1
        if index < len(ObsData.items):
            c.drawString(5*mm, 5*mm, '~ and more...' + str(len(ObsData.items)) + ' Measurements burned on ' + ObsData.name)
        c.rotate(90)
        c.drawString(2*mm, 3*mm, ObsData.name)
        c.drawRightString(117*mm, 3*mm, 'Recorded ' + ObsData.date + ' at the WSRT')
        c.rotate(180)
        c.drawString(-117*mm, 140*mm, ObsData.name)
        c.drawRightString(-2*mm, 140*mm, 'Recorded ' + ObsData.date + ' at the WSRT')
    
    
    def go(self):
        from string import ascii_uppercase as Capitals
        data = XMLdata(self.inputs['XMLdata'])
        data.name +=  ' ' + Capitals[int(self.inputs['CDindex']) -1]
        c = canvas.Canvas(self.inputs['filepath'] + '/bookletCD_' + self.inputs['CDindex'] + '.pdf')
        c.rotate(-90)
        c.translate(-166*mm, 45*mm)
        self.frontpage(c, data)
        pagecount = int(math.ceil(len(data.items)/32.0)) ## 32.0 instead of 32, otherwise int will truncate
        sidecount = int(math.ceil(pagecount/4.0) * 2)
        side      = 0
        while (side < sidecount):
            page = side
            if page: ## we skip page == 0, the frontpage
                if side % 2:
                    c.rotate(90)
                    c.translate(45.5*mm, -158*mm)
                else:
                    c.rotate(-90)
                    c.translate(-166*mm, 45*mm)
                self.datapage(c, data, page, pagecount)
            page = sidecount*2 - side - 1
            if page <= pagecount:
                if side % 2:
                    c.translate(120*mm, 0)
                else:
                    c.translate(-120*mm, 0)
                self.datapage(c, data, page, pagecount)
            else:
                if side % 2:
                    c.translate(120*mm, 0)
                else:
                    c.translate(-120*mm, 0)
                c.rect(0, 0, 120*mm, 119*mm, stroke=1, fill=0)
            c.showPage() ## next page
            side += 1
        
        self.backside(c, data)
        c.showPage() ##next page
        c.save()
        self.outputs['PDFfile'] = self.inputs['filepath'] + '/bookletCD_' + self.inputs['CDindex'] + '.pdf'
        self.cook_system('acroread', ' -toPostScript ' + self.outputs['PDFfile'])
        self.cook_system('lpr', ' -P rainbowds ' + self.inputs['filepath'] + '/bookletCD_' + self.inputs['CDindex'] + '.ps')

## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = create_cd_booklet()
    standalone.main()
