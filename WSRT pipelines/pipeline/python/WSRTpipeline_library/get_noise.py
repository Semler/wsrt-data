#!/usr/bin/env python
from WSRTrecipe import *

class get_noise(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MiriadImage'] = ''
        self.inputs['filepath']    = '.'
        
        ## List of outputs
        self.outputs['MiriadImage'] = ''
        self.outputs['noise']       = 0.0
        
        ## Help text
        self.helptext = """
        Obtains noise in image"""
    
    ## Code to generate results ---------------------------------------------
    def go(self):
        import os
        self.messages.store = True
        self.cook_system('mirStat', ' in=' + self.inputs['filepath'] + '/' + self.inputs['MiriadImage'])
        self.messages.store = False
        Noise = float(self.messages[-3][2].split(' ')[-2])
        if Noise > 10000: raise RecipeError ('Sanity error: I do not beleive a noise of: ' + str(Noise))
        self.outputs['noise'] = Noise
        # We should only do this on succes
        self.outputs['MiriadImage'] = self.inputs['MiriadImage']
    
## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = get_noise()
    standalone.main()
