#!/usr/bin/env python
from WSRTrecipe import *

class cross_calibrate(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['CalibratorUvFile']  = ''
        self.inputs['ObservationUvFile'] = ''
        self.inputs['refant']            = 3
        
        ## List of outputs
        self.outputs['MiriadUvFile'] = ''
        
        ## Help text
        self.helptext = """
        This function applies the calibrator to the observation
        Step 2.7 and 2.9 in SWOM"""
    
## Code to generate results ---------------------------------------------
    def go(self):
        import os, mirlib
        from constants import Low_Bandpass_Top, Low_Bandpass_Bottom

        ## Find out the number of bands in the data
        nspect = mirlib.varvalue(self.inputs['CalibratorUvFile'], 'nspect')[0]
        if nspect != 1: raise RecipeError ('mfcal only works correctly on signle band data')
        ## Find out the number of channels in the data
        nschan   = mirlib.varvalue(self.inputs['CalibratorUvFile'], 'nschan')
        ## Leave out the a fraction of the lowest and highest channels
        ## (due to zero bandpass), assuming all windows are same width
        up_chan  = (nschan[0] - int(Low_Bandpass_Top * nschan[0])) + 1  ## Miriad is one-based
        low_chan = max([(int(Low_Bandpass_Bottom * nschan[0])), 1]) + 1 ## and always skip at least 1 channel
        numchan  = up_chan - low_chan
        line_str = ' line=channel,' + str(numchan) + ',' + str(low_chan)

        ## Calibrate gains & copy tables to src
        self.zap(self.inputs['CalibratorUvFile'] + '/gains')
        self.zap(self.inputs['CalibratorUvFile'] + '/bandpass')
        status = self.cook_miriad('mfcal', ' vis=' + self.inputs['CalibratorUvFile'] + 
                                             line_str +
                                           ' refant=' + str(self.inputs['refant']) +
                                           ' interval=10000')
        if status:
            raise RecipeError ('mfcal failed')
    
        ## changes header item to 1.0: calibrator valid for 1 day
        self.cook_miriad('puthd', ' in=' + self.inputs['CalibratorUvFile'] + '/interval' +
                                  ' value=1.0' + ' type=double')
        ## Copy the calibration tables to the source data    
        self.zap(self.inputs['ObservationUvFile'] + '/gains')
        self.zap(self.inputs['ObservationUvFile'] + '/bandpass')
        self.cook_miriad('gpcopy', ' vis=' + self.inputs['CalibratorUvFile'] +
                                   ' out=' + self.inputs['ObservationUvFile'])
    
        ## Flag the data which we did not calibrate
        ## Leave out the lower 1% and the upper 10% (WSRT bandpass characteristics)
        line_str = ' line=channel,' + str(low_chan-1) + ',1'
        self.print_debug('flagging lowest 1%: ' + line_str)
        self.cook_miriad('uvflag', ' vis=' + self.inputs['ObservationUvFile'] + 
                                     line_str +
                                   ' flagval=flag')
        line_str = ' line=channel,' + str(nschan[0] - up_chan + 1) + ',' + str(up_chan)
        self.print_debug('flagging highest 10%: ' + line_str)
        self.cook_miriad('uvflag', ' vis=' + self.inputs['ObservationUvFile'] +
                                     line_str +
                                   ' flagval=flag')
        
        self.outputs['MiriadUvFile'] = self.inputs['ObservationUvFile']
    
## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = cross_calibrate()
    standalone.main()

