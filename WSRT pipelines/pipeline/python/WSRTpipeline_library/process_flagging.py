#!/usr/bin/env python
from WSRTrecipe import *

class process_flagging(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['SrcAntennaFlagging'] = []
        self.inputs['CalAntennaFlagging'] = []
        self.inputs['filepath']           = '.'
        self.inputs['MiriadUvFile']       = ''
        
        ## List of outputs
        self.outputs['SrcAntennaFlagging'] = []
        self.outputs['CalAntennaFlagging'] = []
        self.outputs['refant']             = 0
        self.outputs['MiriadUvFile']       = ''
        
        ## Help text
        self.helptext = """
        Flag antennae in the source uvfile that are flagged in the calibrator,
        and returns refant as the antenna with the least flagged data."""
    
## Code to generate results ---------------------------------------------
    def go(self):
        vis      = self.inputs['filepath'] + '/' + self.inputs['MiriadUvFile']
        calflags = self.inputs['CalAntennaFlagging']
        srcflags = self.inputs['SrcAntennaFlagging']
        for antenna in range(len(calflags)):
            if calflags[antenna] > 50: ## 50% of calibrator antenna is flagged
                self.print_notification('Flagging antenna: ' + str(antenna))
                self.cook_miriad('uvflag', ' vis=' + vis + 
                                           ' select=ant(' + str(antenna) + ')' +
                                           ' flagval=flag')
                srcflags[antenna] = 100
        if min(srcflags) > 75: raise RecipeError ('Almost all data is flagged!')
        self.outputs['SrcAntennaFlagging'] = srcflags
        self.outputs['CalAntennaFlagging'] = calflags
        self.outputs['refant']             = srcflags.index(min(srcflags))
        self.outputs['MiriadUvFile']       = self.inputs['MiriadUvFile']
    
## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = process_flagging()
    standalone.main()
