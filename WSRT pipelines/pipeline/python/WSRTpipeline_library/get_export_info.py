#!/usr/bin/env python
from WSRTrecipe import *

class get_export_info(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MeasurementSets'] = []
        self.inputs['filepath']        = '.'
        self.inputs['InspectionPath']  = '.'
        self.inputs['AddInspection']   = False
        
        ## List of outputs
        self.outputs['MeasurementSets'] = []
        self.outputs['ReadmeFiles']     = []
        self.outputs['MSinfo']          = []
        
        ## Help text
        self.helptext = """
        obtains MS info from glish script and README inspection file"""
    
    ## Code to generate results ---------------------------------------------
    def go(self):
        for ms in self.inputs['MeasurementSets']:
            temp = ingredient.WSRTingredient()
            temp['filepath']       = self.inputs['filepath']
            temp['MeasurementSet'] = ms
            self.cook_recipe('get_msinfo', temp, temp)
            self.outputs['MSinfo'].append(temp)
            if self.inputs['AddInspection']:
                temp['InspectionPath'] = self.inputs['InspectionPath']
                self.cook_recipe('get_readme', temp, temp)
                print temp
                if temp['ReadmeFile']:
                    self.outputs['ReadmeFiles'].append(temp['ReadmeFile'])
        self.outputs['MeasurementSets'] = self.inputs['MeasurementSets']
    
## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = get_export_info()
    standalone.main()
