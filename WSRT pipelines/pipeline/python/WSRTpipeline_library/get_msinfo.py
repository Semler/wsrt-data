#!/usr/bin/env python
from WSRTrecipe import *

class get_msinfo(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MeasurementSet'] = ''
        self.inputs['filepath']       = '.'
        
        ## List of outputs
        self.outputs['MeasurementSet'] = ''
        self.outputs['SeqNr']          = ''
        self.outputs['ObjName']        = ''
        self.outputs['Semester']       = ''
        self.outputs['PrjNr']          = ''
        self.outputs['DateStart']      = ''
        self.outputs['TimeStart']      = ''
        self.outputs['MFFEBand']       = ''
        self.outputs['Baselines']      = ''
        
        ## Help text
        self.helptext = """
        obtains MS info from glish script"""
    
    ## Code to generate results ---------------------------------------------
    def go(self):
        import os
        self.messages.store = True
        length = len(self.messages)
        self.cook_glish('MSinfo', self.inputs['filepath'] + '/' + self.inputs['MeasurementSet'])
        self.messages.store = False
        for i in range(length - len(self.messages), -1):
            line = self.messages[i][2]
            pos = line.find('SeqNr =')
            if pos >= 0:
                self.outputs['SeqNr'] = line[(pos+7):].strip()
            pos = line.find('ObjName =')
            if pos >= 0:
                self.outputs['ObjName'] = line[(pos+9):].strip()
            pos = line.find('Semester =')
            if pos >= 0:
                self.outputs['Semester'] = line[(pos+10):].strip()
            pos = line.find('PrjNr =')
            if pos >= 0:
                self.outputs['PrjNr'] = line[(pos+7):].strip()
            pos = line.find('DateStart =')
            if pos >= 0:
                self.outputs['DateStart'] = line[(pos+11):].strip()
            pos = line.find('TimeStart =')
            if pos >= 0:
                self.outputs['TimeStart'] = line[(pos+11):].strip()
            pos = line.find('MFFEBand =')
            if pos >= 0:
                self.outputs['MFFEBand'] = line[(pos+10):].strip()
            pos = line.find('Baselines =')
            if pos >= 0:
                self.outputs['Baselines']  = line[(pos+11):].strip() + ','
                self.outputs['Baselines'] += self.messages[i+1][2][(pos+11):].strip() + ','
                self.outputs['Baselines'] += self.messages[i+2][2][(pos+11):].strip() + ','
                self.outputs['Baselines'] += self.messages[i+3][2][(pos+11):].strip()

        # We should only do this on succes
        self.outputs['MeasurementSet'] = self.inputs['MeasurementSet']
        
    
## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = get_msinfo()
    standalone.main()
