#!/usr/bin/env python
from WSRTrecipe import *

class tvclip_flagging(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        # List of inputs with defaults
        self.inputs['MiriadUvFiles'] = []
        self.inputs['filepath']      = '.'
        
        # List of outputs
        self.outputs['MiriadUvFiles'] = []
        
        # Help text
        self.helptext = """
        This function runs tvclip for automatic flagging
        of a Miriad UV dataset. If flags both for amplitude
        and for phase.
        Step 2.6 in SWOM"""
    
    # Code to generate results ---------------------------------------------
    def go(self):
        import os
        for uv in self.inputs['MiriadUvFiles']:
            if self.cook_miriad('tvclip', ' vis=' + self.inputs['filepath'] + '/' + uv + 
                                          ' commands=diff,clip options=notv'):
                pass #raise RecipeError ('tvclip failed')
            if self.cook_miriad('tvclip', ' vis=' + self.inputs['filepath'] + '/' + uv + 
                                          ' mode=phase commands=diff,clip options=notv'):
                pass #raise RecipeError ('tvclip failed')

        self.outputs['MiriadUvFiles'] = self.inputs['MiriadUvFiles']
    
# Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = tvclip_flagging()
    standalone.main()
