/* $Id: ObitTableSUUtil.c,v 1.5 2005/06/14 13:42:15 bcotton Exp $                            */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003,2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "ObitTableSUUtil.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitTableSUUtil.c
 * ObitTableSU class utility function definitions.
 */

/*----------------------Public functions---------------------------*/

/**
 * Lookup a list of sources in an SU table.
 * This is intended for finding user selected sources.
 * \param in       Table to obtain data from
 * \param dim      dimensionality of inlist, first, the length if the source names, 
 *                 then the number of entries.
 * \param inlist   List of source names in single array with no nulls,
 *                 any leading '-' is stripped;
 *                 Any nonblank entries that are not found in the SU table
 *                 will generate  a warning.
 * \param outlist  List of source IDs corresponding to inlist, -1 => not found.
 * \param deselect TRUE if no source name began with a '-'
 * \param *err     ObitErr error stack.
 * \return I/O Code  OBIT_IO_OK = OK.
 */
ObitIOCode ObitTableSULookup (ObitTableSU *in, gint32 *dim, gchar *inlist, 
			      gint *outlist, gboolean *select, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitTableSURow *row;
  gint i, j, l;
  gchar tempName[101], temp2Name[101]; /* should always be big enough */
  gchar *routine = "ObitTableSULookup";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitTableSUIsA(in));

  /* initialize */
  *select = TRUE;
  for (i=0; i<dim[1]; i++) outlist[i] = -1;

  /* Open table */
  retCode = ObitTableSUOpen (in, OBIT_IO_ReadOnly, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine,in->name, retCode);

  /* Create table row */
  row = newObitTableSURow (in);

  /* loop over table */
  while (retCode==OBIT_IO_OK) {
    retCode = ObitTableSUReadRow (in, -1, row, err);
    if (retCode == OBIT_IO_EOF) break;
    if ((retCode != OBIT_IO_OK) || (err->error)) 
      Obit_traceback_val (err, routine,in->name, retCode);
    /* Blank fill Source to dim[0], in temp2Name */
    for (i=0; i<dim[0]; i++) temp2Name[i] = ' ';  temp2Name[i] = 0;
    l = MIN (100, strlen(row->Source));
    for (i=0; i<l; i++)  temp2Name[i] = row->Source[i];
    temp2Name[dim[0]] = 0;  /* to be sure terminated */

    /* loop through inlist and check if it matches */
    for (i=0; i<dim[1]; i++) {
      for (j=0; j<dim[0]; j++) tempName[j] = ' '; tempName[j] = 0;
      /* get blank padded name from list */
      for (j=0; j<dim[0]; j++) {
	if (inlist[i*dim[0]+j]==0) break;  /* only values in string */
	tempName[j] = inlist[i*dim[0]+j]; 
      }
      /* Have an initial '-'? */
      if (tempName[0]=='-') {
	*select = FALSE;
	for (j=0; j<dim[0]; j++) tempName[j]= tempName[j+1]; /* remove '-' */
      }
      /* Is this a match? */
      if (!strncmp (temp2Name, tempName, dim[0])) outlist[i] = row->SourID;
    }
  }
  
  /* check for errors */
  if ((retCode > OBIT_IO_EOF) || (err->error))
    Obit_traceback_val (err, routine,in->name, retCode);
  
  /* Release table row */
  row = ObitTableSURowUnref (row);

  /* Close table */
  retCode = ObitTableSUClose (in, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine,in->name, retCode);

  /* Be sure all nonblank entries found else issue warning */
  for (i=0; i<dim[1]; i++) {
    if (outlist[i]<0) {/* Not found */
      /* get name from list */
      for (j=0; j<dim[0]; j++) tempName[j] = inlist[i*dim[0]+j]; tempName[j] = 0;
      /* Have an initial '-'? */
      if (tempName[0]=='-') for (j=0; j<dim[0]; j++) tempName[j]= tempName[j+1]; /* remove '-' */

      /* all blank is OK */
      if (strncmp(tempName, "                ", dim[0]))
	  Obit_log_error(err, OBIT_InfoWarn, 
			 "%s: Warning: Source %s not found in source table", routine, tempName);
    }
  }
  return retCode;
} /* end ObitTableSULookup */

/**
 * Convert the contents of a ObitTableSU into an ObitSourceList.
 * \param in       Table to obtain data from
 * \param *err     ObitErr error stack.
 * \return requested ObitSourceList
 */
ObitSourceList* ObitTableSUGetList (ObitTableSU *in, ObitErr *err) {
  ObitSourceList *out=NULL;
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitTableSURow *row;
  gint maxSUid, sid;
  glong irow;
  gchar tempName[101]; /* should always be big enough */
  gchar *routine = "ObitTableSUGetList";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return out;
  g_assert (ObitTableSUIsA(in));

  /* Open table */
  retCode = ObitTableSUOpen (in, OBIT_IO_ReadOnly, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, in->name, out);
  
  /* Create table row */
  row = newObitTableSURow (in);

  /* loop over table looking for highest number */
  maxSUid = -1;
  irow = 0;
  while (retCode==OBIT_IO_OK) {
    irow++;
    retCode = ObitTableSUReadRow (in, irow, row, err);
    if (retCode == OBIT_IO_EOF) break;
    if ((retCode != OBIT_IO_OK) || (err->error)) 
      Obit_traceback_val (err, routine, in->name, out);
    
    maxSUid = MAX (maxSUid, row->SourID);

  } /* end loop over file */
  
  /* check for errors */
  if ((retCode > OBIT_IO_EOF) || (err->error))
    Obit_traceback_val (err, routine, in->name, out);

  /* Create output */
  g_snprintf (tempName, 100, "Source List for %s",in->name);
  out = ObitSourceListCreate (tempName, maxSUid);
  
  /* loop over table saving information */
  retCode = OBIT_IO_OK;
  irow = 0;
  while (retCode==OBIT_IO_OK) {
    irow++;
    retCode = ObitTableSUReadRow (in, irow, row, err);
    if (retCode == OBIT_IO_EOF) break;
    if ((retCode != OBIT_IO_OK) || (err->error)) 
      Obit_traceback_val (err, routine, in->name, out);

    sid = row->SourID - 1;
    out->SUlist[sid]->SourID  = row->SourID;
    out->SUlist[sid]->equinox = row->Epoch;   /* correct AIPS misnaming */
    out->SUlist[sid]->RAMean  = row->RAMean;
    out->SUlist[sid]->DecMean = row-> DecMean;
    out->SUlist[sid]->RAApp   = row->RAApp;
    out->SUlist[sid]->DecApp  = row->DecApp;
    strncpy (out->SUlist[sid]->SourceName, row->Source, 17);
    out->SUlist[sid]->SourceName[16] = 0;  /* to be sure */

    /* Save name as name of object */
    if (out->SUlist[sid]->name) g_free (out->SUlist[sid]->name);
    strncpy (tempName, row->Source, 100);
    tempName[in->myDesc->repeat[in->SourceCol]] = 0; /* Null terminate */
    out->SUlist[sid]->name = g_strdup(tempName);

  } /* end second loop over table */

 /* Release table row */
  row = ObitTableSURowUnref (row);

  /* Close table */
  retCode = ObitTableSUClose (in, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, in->name, out);

  return out;
} /* end ObitTableSUGetList */
