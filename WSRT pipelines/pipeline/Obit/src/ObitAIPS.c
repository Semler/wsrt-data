/* $Id: ObitAIPS.c,v 1.10 2005/07/21 12:30:31 bcotton Exp $  */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
#include <math.h>
#include <string.h>
#include "ObitAIPS.h"
#include "ObitAIPSCat.h"

/*-------- ObitIO: Software for the recently deceased ------------------*/
/**
 * \file ObitAIPS.c
 * ObitAIPS class function definitions.
 */

/*-----------------File Globals ---------------------------*/
/** number of bytes per "sector" in ancient aipsish */
static gint AIPS_NBPS = 256*sizeof(AIPSint); 

/** Current AIPS data revision code */
gchar AIPS_Version = 'D';

/** Class information structure */
static ObitAIPS ObitAIPSInfo = {"ObitAIPS", FALSE};

/** Pointer to Class information structure */
static ObitAIPS *myAIPSInfo = &ObitAIPSInfo;

/* AIPS magic blanking value */
static union FBLANKequiv {
  gchar string[4];
  gfloat fblank;
} FBLANK;

/*------------------ Structures -----------------------------*/

/*---------------Private function prototypes----------------*/
/*---------------Public functions---------------------------*/
/**
 * Save names of the AIPS directories.
 * \param number of disks defined [0,MAXAIPSDISK]], >=0 -> none
 * \param dir the names of the directories
 *        If NULL, check for environment variables $DA01...
 * \param F_TRUE   Value of Fortran TRUE (used in Fortran interface)
 * \param F_FALSE  Value of Fortran FALSE
 */
void ObitAIPSClassInit (gint number, gchar* dir[], oint F_TRUE, oint F_FALSE)
{
  gint i;
  gchar daxx[5], *ev;
  gchar *da[]={"01","02","03","04","05","06","07","08","09","10",
	       "11","12","13","14","15","16","17","18","19","20"};

  myAIPSInfo->NumberDisks = 0;
  for (i=0; i<MAXAIPSDISK; i++) {
    myAIPSInfo->AIPSdir[i] = NULL;
  }

  /* now initialized */
  myAIPSInfo->initialized = TRUE;

  /* Are directories given or should I look for them? */
  if (dir==NULL) { /* Look in environment */
    for (i=0; i<MAXAIPSDISK; i++) {
      sprintf (daxx,"DA%s",da[i]);
      ev = getenv (daxx);
      if (ev) {
	myAIPSInfo->AIPSdir[myAIPSInfo->NumberDisks] =
	  /* strip DA?? = and add / */
	  g_strconcat(ev, "/", NULL);
	myAIPSInfo->NumberDisks++;
      } else {
	break;
      }
    }
  } else { /* use what are given */
    
    /* error checks */
    g_assert (number<=MAXAIPSDISK);
    
    if (number<=0) {
      myAIPSInfo->NumberDisks = 0;
      myAIPSInfo->AIPSdir[0] = NULL;
      return;
    }
    
    /* save directory names */
    myAIPSInfo->NumberDisks = number;
    for (i=0; i<number; i++) 
      myAIPSInfo->AIPSdir[i] =  g_strconcat(dir[i], "/", NULL);
    
  } /* end of initialize data directories */

  /* initialize catalog header structure info in ObitAIPSCat class */
  ObitAIPSCatInitDHDR();

  /* Save true and false */
  myAIPSInfo->F_TRUE = F_TRUE;
  myAIPSInfo->F_FALSE = F_FALSE;

  /* initialise AIPS magic blanking value float equiv of 'INDE' */
  FBLANK.fblank = ObitMagicF();
  
} /* end ObitAIPSClassInit */

/**
 * Frees directory strings
 */
void ObitAIPSShutdown (void)
{
  gint i;

  myAIPSInfo->NumberDisks = 0;
  for (i=0; i<MAXAIPSDISK; i++) {
    if (myAIPSInfo->AIPSdir[i]) g_free(myAIPSInfo->AIPSdir[i]);
    myAIPSInfo->AIPSdir[i] = NULL;
  }

  /* now uninitialized */
  myAIPSInfo->initialized = FALSE;

} /*  end ObitAIPSShutdown */

/**
 * Forms file name from the various parts.
 * #ObitAIPSClassInit must have been used to initialize.
 * \param type File type code
 * \param disk AIPS "disk" number. 1-rel
 * \param cno AIPS catalog slot number.
 * \param userid user number.
 * \param tabType two character code for table type, 
 *                NULL if not needed.
 * \param tabVer table version number.
 * \param err    Error stack for any error messages.
 * \return file name string, should be g_freeed when done.
 */
gchar* 
ObitAIPSFilename (ObitAIPSFileType type, gint disk, gint cno, 
		  gint userid, gchar *tabType, gint tabVer, ObitErr *err)
{
  gchar *out;
  gchar idEhex[4], cnoEhex[4], verEhex[3], file[15];
  gchar *types[] = {"CA","CB","MA","UV","SC","UK","HI","PL","SL"};
  
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return NULL;
  if (!myAIPSInfo->initialized) /* AIPS directories un initialized */
    Obit_log_error(err, OBIT_Error, 
		   "AIPS directories uninitialized");
  if ((disk<=0) || (disk>myAIPSInfo->NumberDisks)) /* Disk number out of range */
    Obit_log_error(err, OBIT_Error, 
		   "AIPS disk number %d out of range [%d,%d]", 
		   disk, 1, myAIPSInfo->NumberDisks);
  if (err->error) return NULL;

  /* put together basic file name */
  /* File type */
  file[0]=types[type][0]; file[1]=types[type][1];
  
  /* date revision code */
  file[2] = AIPS_Version;
  
  /* catalog slot */
  /* Convert to EHex */
  ObitAIPSEHex(cno, (gchar*)cnoEhex);
  file[3] = cnoEhex[0]; file[4] = cnoEhex[1]; file[5] = cnoEhex[2];
  
  /* version by type */
  if (type==OBIT_AIPS_Catalog) {
    verEhex[0] = '0'; verEhex[1] = '0';verEhex[2] = '0';
  } else if (type==OBIT_AIPS_Table) {
    ObitAIPSEHex(tabVer, (gchar*)verEhex);

    /* set table type */
    file[0] = tabType[0]; file[1] = tabType[1];
  } else { /* Everything else is one */
    verEhex[0] = '0'; verEhex[1] = '0';verEhex[2] = '1';
  }
  file[6] = verEhex[0]; file[7] = verEhex[1]; file[8] = verEhex[2];

  
  /* User id */
  /* Convert to EHex */
  ObitAIPSEHex(userid, (gchar*)idEhex);
  file[9] = '.';        file[10] = idEhex[0];
  file[11] = idEhex[1]; file[12] = idEhex[2];
  
  /* Top it all off with a VAXish */
  file[13] = ';';         file[14] = 0;
  
  /* put it all together */
  out = g_strconcat (ObitAIPSDirname(disk, err), file, NULL);
  
  return out;
} /* end ObitAIPSFilename */

/**
 * Change directory name
 * \param disk  disk (1-rel) to be changed, must have been assigned at startup
 * \param dir   new name of the directory
 */
void ObitAIPSSetDirname (gint disk, gchar* dir, ObitErr *err)
{
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  if (!myAIPSInfo->initialized) /* AIPS directories un initialized */
    Obit_log_error(err, OBIT_Error, "AIPS directories uninitialized");
  if ((disk<1) || (disk>myAIPSInfo->NumberDisks)) /* Disk number out of range */
    Obit_log_error(err, OBIT_Error, 
		   "AIPS disk number %d out of range [%d,%d]", 
		   disk, 1, myAIPSInfo->NumberDisks);
  if (err->error) return;

  /* replace directory name */
  g_free(myAIPSInfo->AIPSdir[disk-1]);
  myAIPSInfo->AIPSdir[disk-1] =  g_strdup(dir);

} /* end ObitAIPSSetDirname */

/**
 * Returns pointer to directory string by AIPS disk.
 * \param disk AIPS disk number.
 * \param err  Error stack for any error messages.
 * \return directory name string, this is a pointer into a global 
 *         class structure and should not be g_freeed.
 */
gchar* ObitAIPSDirname (gint disk, ObitErr *err)
{
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return NULL;
  if (!myAIPSInfo->initialized) /* AIPS directories uninitialized */
    Obit_log_error(err, OBIT_Error, 
		   "AIPS directories uninitialized");
  if ((disk<=0) || (disk>myAIPSInfo->NumberDisks)) /* Disk number out of range */
    Obit_log_error(err, OBIT_Error, 
		   "AIPS disk number %d out of range [%d,%d]", 
		   disk, 1, myAIPSInfo->NumberDisks);
  if (myAIPSInfo->AIPSdir[disk-1]==NULL) /* Directory not defined */
    Obit_log_error(err, OBIT_Error, 
		   "AIPS directory %d not defined", disk);
  if (err->error) return NULL;

  return myAIPSInfo->AIPSdir[disk-1];
} /* ObitAIPSDirname  */

/**
 * Returns number of defined AIPS disks
 * \param err  Error stack for any error messages.
 * \return number of disks
 */
gint ObitAIPSGetNumDisk (ObitErr *err)
{
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return 0;
  if (!myAIPSInfo->initialized) /* AIPS directories un initialized */
    Obit_log_error(err, OBIT_Error, 
		   "AIPS directories uninitialized");
  if (err->error) return 0;

  g_assert(myAIPSInfo->initialized);

  return myAIPSInfo->NumberDisks;
} /* ObitAIPSGetNumDisk  */

/**
 * Calculate first byte offset in an AIPS image file of a given pixel.
 * AIPS images have planes starting on an even #AIPS_NBPS byte boundry.
 * If the row size is less than AIPS_NBPS bytes, multiple rows are
 * stored in a sector but are not allowed to cross a sector boundry;
 * the end of a sector is padded and the next row starts at the beginning
 * of a new sector.
 * Rows longer than a sector have the last sector padded and the next 
 * row starts at the beginning of a new sector.
 * This was necessary back at the dawn of time when MODCOMP computers 
 * roamed the earth and could only start a transfer at the beginning 
 * of a disk sector.
 * This routine patterned after the aipsish $APLSUB/COMOFF.FOR.
 * (The original is incomprehensible).
 * \param naxis number of axes in image
 * \param naxes number of pixels on each axis.
 * \param pos   1-rel pixel position
 * \return byte offset from beginning of image file 
 */
ObitFilePos ObitAIPSImageFileOffset (gint naxis, gint *naxes, gint *pos)
{
  glong nspp, nrps, nspr, np, ns;
    ObitFilePos filePos=0;

  /* error checks */
  g_assert((naxis>0) && (naxis<=6));
  g_assert(naxes!=NULL);
  g_assert(pos!=NULL);

  /* how many sectors per plane? */
  nrps = AIPS_NBPS / (naxes[0] * sizeof(float)); /* # rows per sector */
  if (nrps > 0) { /* multiple rows per sector */
    nspp = (naxes[1] - 1) / nrps + 1;
  } else { /* multiple sectors per row */
    nspp = (naxes[0] * sizeof(float) - 1) / AIPS_NBPS + 1;
    nspp *= naxes[1];
  }

  /* how many planes down? */
  np = 0;
  if (naxis>2) np += (pos[2]-1);          /* third dimension */
  if (naxis>3) np += (pos[3]-1)*naxes[2]; /* 4th dim*/
  if (naxis>4) np += (pos[4]-1)*(naxes[3]*naxes[2]); /* 5th */
  if (naxis>5) np += (pos[5]-1)*(naxes[4]*naxes[3]*naxes[2]); /* 6th */

  /* byte offset to the beginning of the plane */
  filePos = np * nspp * AIPS_NBPS;

  /* correction for row */
  if (nrps > 0) { /* multiple rows per sector */
    /* integral number of rows per sector */
    /* number of whole sectors since beginning of plane: */
    ns = (pos[1]-1)/nrps;  
    filePos += ns * AIPS_NBPS;
    /* fractional sector: */
    filePos += (pos[1]-ns*nrps-1) * (naxes[0] * sizeof(float));
  } else { /* multiple sectors per row */
    /* each row takes an integral number of sectors */
    /* # sectors per row */
    nspr = 1 + (((naxes[0] * sizeof(float))-1)/ AIPS_NBPS); 
    filePos += nspr * AIPS_NBPS * (pos[1]-1);
  }

  /* correction for column */
  filePos += (pos[0]-1) * sizeof(float);

  return filePos;
}  /* end ObitAIPSImageFileOffset */


/**
 * Calculate first byte offset in an AIPS table file of the beginning of
 * a row.  Tables rows are written either an integral number of sectors
 * (#AIPS_NBPS bytes) per row, or an integral number of rows per sector.
 * The last "sector" is padded after the last datum.
 * \param start Byte offset of the beginning of the row data.
 * \param lrow  The length of a row in bytes.
 * \param row   1-rel row number desired.
 * \return byte offset from beginning of table file 
 */
ObitFilePos ObitAIPSTableFileOffset (ObitFilePos start, gint lrow, gint row)
{
  glong nrps, nspr, rowoff;
  ObitFilePos ns, nr, filePos=0;

  /* error checks */
  g_assert(start>0);
  g_assert(lrow>0);
  g_assert(row>0);

  /* how many sectors per row? */
  rowoff = row - 1;
  nrps = AIPS_NBPS / lrow; /* # rows per sector */
  if (nrps > 0) { /* multiple rows per sector */
    /* How many whole sectors */
    ns = rowoff / nrps;
    /* How many rows in this sector? */
    nr = rowoff % nrps;
  } else { /* multiple sectors per row */
    /* Number of sectors per row */
    nspr = 1 + (lrow-1) / AIPS_NBPS;
    /* How many whole sectors */
    ns = rowoff * nspr;
    /* How many rows in this sector? */
    nr = 0;
  }

  /* put the pieces together */
  filePos = start + ns * AIPS_NBPS + nr * lrow;

  return filePos;
}  /* end ObitAIPSTableFileOffset */

/**
 * Calculate where the end of the current "ModComp" AIPSish sector ends.
 * Tables rows are written either an integral number of sectors
 * (#AIPS_NBPS bytes) per row, or an integral number of rows per sector.
 * The last "sector" is padded after the last datum.
 * This routine calculates the location of the end of this sector.
 * \param start Byte offset of the beginning of the row data.
 * \param lrow  The length of a row in bytes.
 * \param nrow   Number of rows in the table.
 * \return byte offset from beginning of table file 
 */
ObitFilePos ObitAIPSTableEOF (ObitFilePos start, gint lrow, gint nrow)
{
  glong nrps, nspr;
  ObitFilePos ns, filePos=0;

  /* error checks */
  g_assert(start>0);
  g_assert(lrow>0);

  /* how many sectors per row? */
  nrps = AIPS_NBPS / lrow; /* # rows per sector */
  if (nrps > 0) { /* multiple rows per sector */
    /* How many whole sectors */
    ns = (glong)(((gfloat)nrow / (gfloat)nrps) + 0.9999);
  } else { /* multiple sectors per row */
    /* Number of sectors per row */
    nspr = 1 + (lrow-1) / AIPS_NBPS;
    /* How many whole sectors */
    ns = nrow * nspr;
  }

  /* put the pieces together */
  filePos = start + ns * AIPS_NBPS;

  return filePos;
}  /* end ObitAIPSTableEOF */

/**
 * Determine target padding size for wonky AIPS uvdata file
 * size rules.  Must be an integral number of AIPS blocks
 * \param curPos  The byte offset of the current end of the file.
 * \return target file position index in bytes to be padded to.
 */
ObitFilePos ObitAIPSUVWonkyPad (ObitFilePos curPos)
{
  ObitFilePos ns, out = curPos;

  /* If it's already an integral multiple of AIPS_NBPS, nothing more needed */
  if ((curPos %  AIPS_NBPS) == 0) return out;

  /* Need to pad the current AIPS block */
  /* number of whole blocks */
  ns = curPos / AIPS_NBPS;

  out = (ns + 1) * AIPS_NBPS;
  return out;
} /* end ObitAIPSUVWonkyPad */

/**
 * Converts an integer into an extended (base 36) Hex string
 * Only works up to 3 EHex digits.
 * \param in integer to be converted
 * \param out preexisting string into which to write value
 *            must be at least 4 characters.
 */
void ObitAIPSEHex (gint in, gchar *out)
{
  gchar *hexc = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  glong i, j, iv, jv, work = in;

  /* error tests */
  g_assert(in>=0); 
  g_assert(out!=NULL);

  /* init */
  out[0] = '0'; out[1] = '0'; out[2] = '0'; out[4] = 0; 

  /* loop doing converting */
  for (i=0; i<3; i++) {
    j = 2 - i;
    iv = work / 36;
    jv = work - iv*36;
    out[j] = hexc[jv];
    work = iv;
  }

} /* end ObitAIPSEHex */

/**
 * Assigns scratch file naming information and writes to the info.
 * Deliberately does not follow AIPS conventions.
 * name  = "OBIT SCRATCH"
 * class = "pgmName+pgmNumber"
 * seq   = scrNo
 * Makes name "pgmName+pgmNumber+'Scr'+scrNo"
 * \param pgmName    Program name
 * \param pgmNumber  Program incarnation number
 * \param disk       AIPS file type ("MA", "UV").
 * \param user       AIPS user number
 * \param disk       AIPS disk number.
 * \param scrNo      Which scratch file number
 * \param info       ObitInfoList to write to
 * \param err        Error stack for any error messages.
 */
void ObitAIPSAssign(gchar *pgmName, gint pgmNumber, gchar *type,
		    gint user, gint disk, gint scrNo, ObitInfoList *info, 
		    ObitErr *err)
{
  gchar name[13]="OBIT SCRATCH", class[7];
  gint seq, cno;
  gint32 dim[MAXINFOELEMDIM] = {1,1,1,1,1};
  gboolean exist;
  ObitIOType ft;

  /* error check */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitInfoListIsA(info));

  /* form AIPSish class, seq */
  g_snprintf (class, 7, "%s%d", pgmName, pgmNumber);
  /* Make sure "POPS" number gets into class */
  if (strlen(pgmName)>=6) g_snprintf (&class[5], 2, "%d", pgmNumber);
  seq = scrNo;

  /* get assignment */
  cno = ObitAIPSDirAlloc(disk, user, name, class, type, seq, 
			  &exist, err);
  if (cno<0)
    Obit_log_error(err, OBIT_Error, 
		   "ERROR assigning AIPS scratch file CNO");

  /* write assignment to info */
  ft = OBIT_IO_AIPS;
  ObitInfoListPut (info, "FileType", OBIT_int, dim, (gpointer)&ft,   err);
  ObitInfoListPut (info, "Disk",     OBIT_int, dim, (gpointer)&disk, err);
  ObitInfoListPut (info, "CNO",      OBIT_int, dim, (gpointer)&cno,  err);
  ObitInfoListPut (info, "User",     OBIT_int, dim, (gpointer)&user, err);

} /* end ObitAIPSAssign */

/**
 * Convert Fortran Logical to gboolean
 * \param logical Fortran LOGICAL value
 * \return gboolean equivalent (TRUE or FALSE)
 */
gboolean ObitAIPSBooleanF2C (oint logical)
{
  gboolean out;
  out = logical==myAIPSInfo->F_TRUE;
  return out;
} /* end ObitAIPSBooleanF2C */

/**
 * Convert gboolean to Fortran Logical
 * \param bool boolean (TRUE or FALSE)
 * \return Fortran LOGICAL value equivalent
 */
 oint ObitAIPSBooleanC2F (gboolean bool)
{
  oint out;

  if (bool) out = myAIPSInfo->F_TRUE;
  else      out = myAIPSInfo->F_FALSE;
  return out;
} /* end ObitAIPSBooleanC2F */

/*---------------Private functions---------------------------*/

