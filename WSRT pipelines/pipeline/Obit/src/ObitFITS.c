/* $Id: ObitFITS.c,v 1.5 2005/02/05 02:12:13 bcotton Exp $ */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
#include <math.h>
#include <string.h>
#include "ObitFITS.h"

/*-------- ObitIO: Software for the recently deceased ------------------*/
/**
 * \file ObitFITS.c
 * ObitFITS class function definitions.
 */

/*-----------------File Globals ---------------------------*/
/** Class information structure */
static ObitFITS ObitFITSInfo = {"ObitFITS",FALSE};

/** Pointer to Class information structure */
static ObitFITS *myFITSInfo = &ObitFITSInfo;

/*------------------ Structures -----------------------------*/

/*---------------Private function prototypes----------------*/
/*---------------Public functions---------------------------*/
/**
 * Save names of the FITS directories.
 * \param number of disks defined [0,MAXFITSDISK], >=0 -> none
 * \param dir the names of the directories
 *    If NULL then look for environment variables FITS, FITS01, FITS02...
 */
void ObitFITSClassInit (gint number, gchar* dir[])
{
  gint i;
  gchar fitsxx[5], *ev;
  gchar *da[]={"01","02","03","04","05","06","07","08","09","10",
	       "11","12","13","14","15","16","17","18","19","20"};


  myFITSInfo->NumberDisks = 0;
  for (i=0; i<MAXFITSDISK; i++) {
    myFITSInfo->FITSdir[i] = NULL;
  }

  /* now initialized */
  myFITSInfo->initialized = TRUE;

  /* Are directories given or should I look for them? */
  if (dir==NULL) { /* Look in environment */
    myFITSInfo->NumberDisks = 0;

    /* First try $FITS */
    ev = getenv ("FITS");
    if (ev) {
      myFITSInfo->FITSdir[myFITSInfo->NumberDisks] =
	g_strconcat(ev, "/", NULL);
      myFITSInfo->NumberDisks++;
    }

    /* Look for FITS01, FITS02... */
    for (i=0; i<MAXFITSDISK; i++) {
      sprintf (fitsxx,"FITS%s",da[i]);
      ev = getenv (fitsxx);
      if (ev) {
	myFITSInfo->FITSdir[myFITSInfo->NumberDisks] =
	  g_strconcat(ev, "/", NULL);
	myFITSInfo->NumberDisks++;
      } else {
	break;
      }
    }
  } else { /* use what are given */
    
    /* error checks */
    g_assert (number<=MAXFITSDISK);
    
    if (number<=0) {
      myFITSInfo->NumberDisks = 0;
      myFITSInfo->FITSdir[0] = NULL;
      return;
    }
    
    /* save directory names */
    myFITSInfo->NumberDisks = number;
    for (i=0; i<number; i++) 
      myFITSInfo->FITSdir[i] = g_strconcat(dir[i], "/", NULL);
  } /* end of initialize data directories */
    
} /* end ObitFITSClassInit */

/**
 * Frees directory strings
 */
void ObitFITSShutdown (void)
{
  gint i;

  myFITSInfo->NumberDisks = 0;
  for (i=0; i<MAXFITSDISK; i++) {
    if (myFITSInfo->FITSdir[i]) g_free(myFITSInfo->FITSdir[i]);
    myFITSInfo->FITSdir[i] = NULL;
  }

  /* now uninitialized */
  myFITSInfo->initialized = FALSE;

} /*  end ObitFITSShutdown */

/**
 * Add a directory to the list of directories for FITS files
 * #ObitFITSClassInit must have been used to initialize.
 * \param dir   names of the directories with terminal '/'
 * \param err   Error stack for any error messages.
 * \return new 1-rel disk number, -1 on failure
 */
gint ObitFITSAddDir (gchar* dir, ObitErr *err)
{
  gint out = -1;

  if (err->error) return out;
  if (myFITSInfo->NumberDisks>=MAXFITSDISK) {
    /* too many */
    Obit_log_error(err, OBIT_Error, "FITS directory list FULL");
    return out;
  }

  /* add to list */
  myFITSInfo->FITSdir[myFITSInfo->NumberDisks] =
    g_strconcat(dir, "/", NULL);
  out = ++myFITSInfo->NumberDisks;
  return out;
} /* end ObitFITSAddDir */

/**
 * Forms file name from the various parts.
 * #ObitFITSClassInit must have been used to initialize.
 * \param disk      FITS "disk" number. 1-rel, =0 => ignore directory
 * \param fileName  name
 * \param err       Error stack for any error messages.
 * \return full path name string, should be deallocated when done
 */
gchar* 
ObitFITSFilename (gint disk, gchar* fileName, ObitErr *err)
{
  gchar *out;
  
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return NULL;
  /* if disk <0 just return fileName */
  if (disk<=0) {
    out = g_strconcat (fileName, NULL);
    return out;
  }
  if (!myFITSInfo->initialized) /* FITS directories uninitialized */
    Obit_log_error(err, OBIT_Error, 
		   "FITS directories uninitialized");
  if ((disk<0) || (disk>myFITSInfo->NumberDisks)) /* Disk number out of range */
    Obit_log_error(err, OBIT_Error, 
		   "FITS disk number %d out of range [%d,%d]", 
		   disk, 1, myFITSInfo->NumberDisks);
  if (err->error) return NULL;


  /* if fileName begins with '!', put it at the beginning */
  /* put it all together */
  if (fileName[0]=='!' )
    out = g_strconcat ("!", ObitFITSDirname(disk, err), &fileName[1], NULL);
  else
    out = g_strconcat (ObitFITSDirname(disk, err), fileName, NULL);
  
  return out;
} /* end ObitFITSFilename */

/**
 * Returns pointer to directory string by FITS disk.
 * \param disk FITS disk number.
 * \param err  Error stack for any error messages.
 * \return directory name string, this is a pointer into a global 
 *         class structure and should not be g_freeed.
 */
gchar* ObitFITSDirname (gint disk, ObitErr *err)
{

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return NULL;
  if (!myFITSInfo->initialized) /* FITS directories uninitialized */
    Obit_log_error(err, OBIT_Error, 
		   "FITS directories uninitialized");
  if ((disk<0) || (disk>myFITSInfo->NumberDisks)) /* Disk number out of range */
    Obit_log_error(err, OBIT_Error, 
		   "FITS disk number %d out of range [%d,%d]", 
		   disk, 1, myFITSInfo->NumberDisks);
  if (myFITSInfo->FITSdir[disk-1]==NULL) /* Directory not defined */
    Obit_log_error(err, OBIT_Error, 
		   "FITS directory %d not defined", disk);
  if (err->error) return NULL;

  return myFITSInfo->FITSdir[disk-1];
} /* ObitFITSDirname  */


/**
 * Assigns scratch file naming information and writes to the info.
 * Makes name "pgmName+pgmNumber+'Scr'+scrNo"
 * \param pgmName    Program name
 * \param pgmNumber  Program incarnation number
 * \param disk       FITS disk number.
 * \param scrNo      Which scratch file number
 * \param info       ObitInfoList to write to
 * \param err        Error stack for any error messages.
 */
/** Public: Assign a scratch file info */
void ObitFITSAssign(gchar *pgmName, gint pgmNumber, 
		    gint disk, gint scrNo, ObitInfoList *info, 
		    ObitErr *err)
{
  gchar name[121];
  gint32 dim[MAXINFOELEMDIM] = {1,1,1,1,1};
  ObitIOType ft;

  /* error check */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitInfoListIsA(info));

  /* form name string */
  g_snprintf (name, 120, "%s%dScr%d", pgmName, pgmNumber, scrNo);

  /* write assignment to info */
  ft = OBIT_IO_FITS;
  ObitInfoListPut (info, "FileType", OBIT_int, dim, (gpointer)&ft,   err);
  ObitInfoListPut (info, "Disk",     OBIT_int, dim, (gpointer)&disk, err);

  dim[0] = strlen(name);
  ObitInfoListPut (info, "FileName", OBIT_string, dim, 
		   (gpointer)name, err);

} /* end ObitFITSAssign */

/*---------------Private functions---------------------------*/
