/* $Id: ObitUVEdit.c,v 1.3 2005/09/15 19:54:16 bcotton Exp $  */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2005                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include <sys/types.h>
#include <time.h>
#include "ObitUVUtil.h"
#include "ObitTableFG.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitUVEdit.c
 * ObitUVEdit class function definitions.
 * Routines for automatically editing data.
 */

/*---------------Private function prototypes----------------*/
/** 
 * Private: Determine minimum clipping levels based on histogram of baseline  
 * RMSes. 
 */
static void editHist (gint ncorr, gint numCell, gfloat hisinc, gint *hissig, 
		      gfloat* hiclip);

/** Digest correlator information */
static void digestCorr (ObitUVDesc *inDesc, gfloat *maxRMS, gfloat *maxRMS2, 
			gint *crossBL1, gint *crossBL2, 
			gint *corChan, gint *corIF, gint *corStok);

/*----------------------Public functions---------------------------*/
/**
 * Time-domain editing of UV data - produces FG table
 * Fill flagging table with clipping by RMS values of the real and imaginary
 * parts.  All correlations are clipped on each baseline if the RMS is
 * larger than  the maximum.  The clipping is done independently in
 * each time interval defined by timeAvg. 
 *    The clipping level is given by MIN (A, MAX (B,C)) where:
 * A = sqrt (maxRMS[0]**2 + (avg_amp * maxRMS[1])**2)
 *     and avg_amp is the average amplitude on each baseline.
 * B = median RMS + 3 * sigma of the RMS distribution.
 * C = level corresponding to 3% of the data.
 *    All data on a given baseline/correlator are flagged if the RMS
 * exceeds the limit.  If a fraction of bad baselines on any correlator
 * exceeds maxBad, then all data to that correlator is flagged.  In
 * addition, if the offending correlator is a parallel hand correlator
 * then any corresponding cross hand correlations are also flagged.
 * Flagging entries are written into FG table flagTab.
 * Control parameters on info member of inUV:
 * \li "flagTab" OBIT_int    (1,1,1) FG table version number [ def. 1]
 * \li "timeAvg" OBIT_float  (1,1,1) Time interval over which to determine 
 *               data to be flagged (min) [def = 1 min.]
 *               NB: this should be at least 2 integrations.
 * \li "maxRMS"  OBIT_float (2,1,1) Maximum RMS allowed, constant plus 
 *               amplitude coefficient. 
 * \li "maxBad"  OBIT_float (1,1,1) Fraction of allowed flagged baselines 
 *               to a poln/channel/IF above which all baselines are flagged.
 *               [default 0.25]
 *
 * Routine adapted from the AIPSish TDEDIT.FOR/TDEDIT
 * \param inUV     Input uv data to edit. 
 * \param outUV    UV data onto which the FG table is to be attached.
 *                 May be the same as inUV.
 * \param err      Error stack, returns if not empty.
 */
void ObitUVEditTD (ObitUV *inUV, ObitUV *outUV, ObitErr *err)
{
  ObitIOCode iretCode, oretCode;
  ObitTableFG *outFlag=NULL;
  ObitTableFGRow *row=NULL;
  gboolean doCalSelect;
  glong i, j, k, firstVis, startVis, suba, iFGRow, ver;
  glong countAll, countBad;
  gint lastSourceID, curSourceID, lastSubA, lastFQID;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM];
  ObitIOAccess access;
  ObitUVDesc *inDesc;
  gfloat timeAvg, lastTime, maxRMS[2], *maxRMS2=NULL, maxBad, cbase, hisicc;
  gint *hissig=NULL, *blLookup=NULL, *crossBL1=NULL, *crossBL2=NULL;
  gint *corChan=NULL, *corIF=NULL, *corStok=NULL;
  gint *BLAnt1=NULL, *BLAnt2=NULL, BIF, BChan;
  gint flagTab, indx, jndx, kndx, nVisPIO, itemp, ant1, ant2;
  gint numCell, ncorr, numAnt, numBL, blindx, hicell;
  gboolean gotOne, done, isbad, *badCor=NULL;
  gfloat *acc=NULL, *corCnt=NULL, *corBad=NULL, *hiClip=NULL, *Buffer;
  gfloat startTime, endTime, curTime, sigma, hisinc, rms2, rms3, ampl2;
  gfloat mxamp2, sec;
  gchar *tname, reason[25];
  struct tm *lp;
  time_t clock;
  gchar *routine = "ObitUVEditTD";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitUVIsA(inUV));
  g_assert (ObitUVIsA(outUV));

  /* Get control parameters */
  flagTab = 1;
  ObitInfoListGetTest(inUV->info, "flagTab", &type, dim, &flagTab);
  /* Time interval */
  timeAvg = 1.0;  /* default 1 min */
  ObitInfoListGetTest(inUV->info, "timeAvg", &type, dim, &timeAvg);
  if (timeAvg<=(1.0/60.0)) timeAvg = 1.0;
  timeAvg /= 1440.0;  /* convert to days */
  /* RMS clipping parameters, no default */
  ObitInfoListGet(inUV->info, "maxRMS", &type, dim, maxRMS, err);
  mxamp2 = maxRMS[1] * maxRMS[1];
  /* max. fraction bad baselines */
  maxBad = 0.25;           /* default 0.25 */
  ObitInfoListGetTest(inUV->info, "maxBad", &type, dim,  &maxBad);  
  if (err->error) Obit_traceback_msg (err, routine, inUV->name);

   /* Data Selection */
  BIF = 1;
  ObitInfoListGetTest(inUV->info, "BIF", &type, dim, &BIF);
  if (BIF<1) BIF = 1;
  BChan = 1;
  ObitInfoListGetTest(inUV->info, "BChan", &type, dim, &BChan);
  if (BChan<1) BChan = 1;

 /* Set number of vis per read to 1 */
  nVisPIO = 1;
  ObitInfoListGetTest(inUV->info, "nVisPIO", &type, dim, &nVisPIO);
  itemp = 1;
  dim[0] = dim[1] = dim[2] = 1;
  ObitInfoListAlwaysPut(inUV->info, "nVisPIO", OBIT_int, dim, &itemp);

  /* Selection of input? */
  doCalSelect = FALSE;
  ObitInfoListGetTest(inUV->info, "doCalSelect", &type, dim, &doCalSelect);
  if (doCalSelect) access = OBIT_IO_ReadCal;
  else access = OBIT_IO_ReadOnly;

  /* test open to fully instantiate input and see if it's OK */
  iretCode = ObitUVOpen (inUV, access, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_msg (err, routine, inUV->name);
  inDesc  = inUV->myDesc;  /* Get descriptor */

  /* Create output FG table */
  tname = g_strconcat ("Flag Table for: ", outUV->name, NULL);
  ver = flagTab;
  outFlag = newObitTableFGValue(tname, (ObitData*)outUV, &ver, OBIT_IO_ReadWrite, 
				err);
  g_free (tname);
  if (err->error) Obit_traceback_msg (err, routine, inUV->name);
 
  /* Allocate arrays */
  numCell = 800;  /* number of cells in histogram */
  suba    = 1;
  numAnt  = inUV->myDesc->numAnt[suba-1];/* actually highest antenna number */
  numBL   = (numAnt*(numAnt-1))/2;
  ncorr   = inUV->myDesc->ncorr;
  /* acc index = type + corr * (6) + BL * (6*ncorr)  where BL = 0-rel baseline index */
  acc    = g_malloc (ncorr * 6 * numBL * sizeof(gfloat));
  /* hissig index  = cell + numCell*corr */
  hissig = g_malloc (ncorr * numCell * sizeof(gint));
  hisicc = 0.005;  /* Histogram resolution ??? This seems critical */
  hisinc = hisicc*maxRMS[0]; /* Histogram increment */
  hiClip = g_malloc (ncorr * sizeof(gfloat));
  corCnt = g_malloc (ncorr * sizeof(gfloat));
  corBad = g_malloc (ncorr * sizeof(gfloat));
  badCor = g_malloc (ncorr * sizeof(gboolean));
  maxRMS2  = g_malloc (ncorr * sizeof(gfloat));  /* Maximum variance per correlator */
  crossBL1 = g_malloc (ncorr * sizeof(gint));    /* associated cross baseline 1 */
  crossBL2 = g_malloc (ncorr * sizeof(gint));    /* associated cross baseline 2 */
  corChan  = g_malloc (ncorr * sizeof(gint));    /* Correlator Channel */
  corIF    = g_malloc (ncorr * sizeof(gint));    /* Correlator IF */
  corStok  = g_malloc (ncorr * sizeof(gint));    /* Correlator Stokes */

  /* Baseline tables */
  blLookup = g_malloc0(numAnt*sizeof(gint));
  BLAnt1   = g_malloc (numBL * sizeof(gint));    /* First antenna of baseline */
  BLAnt2   = g_malloc (numBL * sizeof(gint));    /* Second antenna of baseline */
  blLookup[0] = 0;
  k = 0;
  for (j=2; j<=numAnt; j++) {BLAnt1[k]=1; BLAnt2[k]=j; k++;}
  for (i=1; i<numAnt; i++) {
    blLookup[i] = blLookup[i-1] + numAnt-i;
    for (j=i+2; j<=numAnt; j++) {BLAnt1[k]=i+1; BLAnt2[k]=j; k++;}
  }

  /* Initialize things */
  startTime = -1.0e20;
  endTime   =  1.0e20;
  lastSourceID = -1;
  curSourceID  = 0;
  lastSubA     = 0;
  countAll     = 0;
  countBad     = 0;
  for (i=0; i<6*ncorr*numBL; i++) acc[i] = 0.0;

  Buffer = inUV->buffer;

  /* Digest visibility info */
  digestCorr (inDesc, maxRMS, maxRMS2, crossBL1, crossBL2, 
	    corChan, corIF, corStok);

  /* Open output table */
  oretCode = ObitTableFGOpen (outFlag, OBIT_IO_ReadWrite, err);
  if (err->error) Obit_traceback_msg (err, routine, outFlag->name);
  
  /* Create Row */
  row = newObitTableFGRow (outFlag);
  
  /* Attach row to output buffer */
  ObitTableFGSetRow (outFlag, row, err);
  if (err->error) Obit_traceback_msg (err, routine, inUV->name);
  
  /* Initialize solution row */
  row->SourID  = 0; 
  row->SubA    = 0; 
  row->freqID  = 0; 
  row->ants[0] = 0; 
  row->ants[1] = 0; 
  row->TimeRange[0] = -1.0e20; 
  row->TimeRange[1] =  1.0e20; 
  row->ifs[0]    = BIF; 
  row->ifs[1]    = 0; 
  row->chans[0]  = BChan; 
  row->chans[1]  = 0; 
  row->pFlags[0] = 0; 
  row->pFlags[1] = 0; 
  row->pFlags[2] = 0; 
  row->pFlags[3] = 0; 
  /* Reason includes time/date */
  /* Get time since 00:00:00 GMT, Jan. 1, 1970 in seconds. */
  time (&clock);
  /* Convert to  broken-down time. */
  lp = localtime (&clock);
  if (lp->tm_year<1000)  lp->tm_year += 1900; /* full year */
  sec = (gfloat)lp->tm_sec;
  lp->tm_mon = lp->tm_mon+1; /* For some bizzare reason, month is 0-rel */
  g_snprintf (reason, 25, "Edit %d/%d/%d %d:%d:%f", 
	      lp->tm_year, lp->tm_mon, lp->tm_mday, 
	      lp->tm_hour, lp->tm_min, sec);
  row->reason    = reason; /* Unique string */
  
  /* Loop over intervals */
  done   = FALSE;
  gotOne = FALSE;
  while (!done) {
    
    /* we're in business - loop through data - one vis per read */
    while ((iretCode==OBIT_IO_OK) && (oretCode==OBIT_IO_OK)) {
      if (!gotOne) { /* need to read new record? */
	if (doCalSelect) iretCode = ObitUVReadSelect (inUV, inUV->buffer, err);
	else iretCode = ObitUVRead (inUV, inUV->buffer, err);
	/*if (iretCode!=OBIT_IO_OK) break;*/
	firstVis = inDesc->firstVis;
      }

      gotOne = FALSE;
      /* Time */
      curTime = Buffer[inDesc->iloct];
      if (inDesc->ilocsu>=0) curSourceID = Buffer[inDesc->ilocsu];
      if (startTime < -1000.0) {  /* Set time window etc. if needed */
	startTime = curTime;
	lastTime = curTime;
	endTime   = startTime +  timeAvg;
	startVis  = firstVis;
	lastSourceID = curSourceID;
      }

      /* Still in current interval/source? */
      if ((curTime<endTime) && (curSourceID == lastSourceID) && 
	  (inDesc->firstVis<=inDesc->nvis) && (iretCode==OBIT_IO_OK)) {
	/* accumulate statistics */
	cbase = Buffer[inUV->myDesc->ilocb]; /* Baseline */
	ant1 = (cbase / 256.0) + 0.001;
	ant2 = (cbase - ant1 * 256) + 0.001;
	lastSubA = (gint)(100.0 * (cbase -  ant1 * 256 - ant2) + 0.5);
	/* Baseline index this assumes a1<a2 always */
	blindx =  blLookup[ant1-1] + ant2-ant1-1;
	if (inDesc->ilocfq>=0) lastFQID = (gint)(Buffer[inDesc->ilocfq]+0.5);
	else lastFQID = 0;
	lastTime = curTime;

	/* Accumulate
	   (1,*) =  count 
	   (2,*) =  sum r then max rms**2 
	   (3,*) =  sum**2 real then rms**2 
	   (4,*) =  sum imaginary 
	   (5,*) =  sum**2 imaginary then rms**2 
	   (6,*) =  sum amplitude */
	indx = inDesc->nrparm; /* offset of start of vis data */
	for (i=0; i<ncorr; i++) { /* loop 120 */
	  if (Buffer[indx+2] > 0.0) {
	    jndx = i*6 + blindx*6*ncorr;
	    acc[jndx]   += 1.0;
	    acc[jndx+1] += Buffer[indx];
	    acc[jndx+2] += Buffer[indx] * Buffer[indx];
	    acc[jndx+3] += Buffer[indx+1];
	    acc[jndx+4] += Buffer[indx+1] * Buffer[indx+1];
	    acc[jndx+5] += sqrt (Buffer[indx]*Buffer[indx] + Buffer[indx+1]*Buffer[indx+1]);
	  } 
	  indx += 3;
	} /* end loop  L120 over correlations */;
      } else {  /* process interval */
	
	/* Now have the next record in the IO Buffer */
	if (iretCode==OBIT_IO_OK) gotOne = TRUE;
	    
	/* Get RMS/collect histogram */
	for (i=0; i<ncorr * numCell; i++)  hissig[i] = 0;
	for (i=0; i<numBL; i++) { /* loop 170 */
	  for (j=0; j<ncorr; j++) { /* loop 160 */
 	    jndx = j*6 + i*6*ncorr;
	    if (acc[jndx] > 0.0) countAll++;  /* count all possibilities */
	    if (acc[jndx] > 1.1) {
	      /* Real part */
	      rms2 = (acc[jndx+2] - ((acc[jndx+1]*acc[jndx+1]) / acc[jndx])) / (acc[jndx]-1.0);
	      rms2 = fabs (rms2);
	      acc[jndx+2] = rms2;
	      /* Imaginary part */
	      rms3 = (acc[jndx+4] - ((acc[jndx+3]*acc[jndx+3]) / acc[jndx])) / (acc[jndx]-1.0);
	      rms3 = fabs (rms3);
	      acc[jndx+4] = rms3;
	      /* Histogram */
	      sigma = sqrt (MAX (rms2, rms3));
	      hicell = sigma / hisinc;
	      hicell = MIN (hicell, numCell-1);
	      hissig[hicell+j*numCell]++;
	    } 
	  } /* end loop  L160: */
	} /* end loop  L170: */

	/* Set histogram clipping levels. */
	editHist (ncorr, numCell, hisinc, hissig, hiClip);

	/* No histogram flagging if clip level exceeds 10. */
	if (maxRMS[0] >= 10.0) {
	  for (j=0; j<ncorr; j++) { /* loop 180 */
            hiClip[j] = maxRMS[0] * maxRMS[0];
	  } /* end loop  L180: */;
	}

	/* initialize counters */
	for (i=0; i<ncorr; i++) corCnt[i] = corBad[i] = 0;

	/* Find bad baselines. */
	for (i=0; i<numBL; i++) { /* loop 200 */
	  for (j=0; j<ncorr; j++) { /* loop 190 */
 	    jndx = j*6 + i*6*ncorr;
            if (acc[jndx] > 1.1) {
	      /* Real part */
	      rms2 = acc[jndx+2];
	      /* Convert sum to maximum rms**2 */
	      ampl2 = (acc[jndx+5] / acc[jndx]) * (acc[jndx+5] / acc[jndx]);
	      acc[jndx+1] = MIN ((maxRMS2[j] + (mxamp2*ampl2)), hiClip[j]);
	      /* Is this one bad? */
	      isbad = rms2  >  acc[jndx+1];
	      /* Imaginary part */
	      rms2 = acc[jndx+4];
	      /* Convert sum to maximum rms**2 */
	      acc[jndx+3] = MIN ((maxRMS2[j] + (mxamp2*ampl2)), hiClip[j]);
	      /* Is this one bad? */
	      isbad = isbad  ||  (rms2  >  acc[jndx+3]);

	      /* Correlator info */
	      corCnt[j]++;
	      if (isbad) {
		/* Make sure it is flagged. */
		acc[jndx+2] = 1.0e20;
		acc[jndx+1] = 0.0;
		corBad[j]++;
		/* If parallel and bad, kill its crosspolarized relatives */
		if (crossBL1[j]>0) {
		  kndx = crossBL1[j]*6 + i*6*ncorr;
		  acc[kndx+2] = 1.0e20;
		  acc[kndx+1] = 0.0;
		}
		if (crossBL2[j]>0) {
		  kndx = crossBL2[j]*6 + i*6*ncorr;
		  acc[kndx+2] = 1.0e20;
		  acc[kndx+1] = 0.0;
		}
	      } 
	    } else if (acc[jndx] > 0.0) {
	      /* Flag correlators without enough data. */
	      acc[jndx+2] = 1.0e20;
	      acc[jndx+1] = 0.0;
	    }
	  } /* end loop  L190: */
	} /* end loop  L200: */
	
	/* Check for bad correlators */
	for (i=0; i<ncorr; i++) { /* loop 210 */
	  badCor[i] = FALSE;
	  if (corCnt[i] > 1.1) {
	    if ((corBad[i]/corCnt[i])  >  maxBad) {
	      /* Kill correlator... */
	      badCor[i] = TRUE;
	      /* and all its relatives */
	      if (crossBL1[i]>0) badCor[crossBL1[i]] = TRUE;
	      if (crossBL2[i]>0) badCor[crossBL2[i]] = TRUE;
	    }
	  } 
	} /* end loop  L210: */
	
	
	/* Init Flagging table entry */
	row->SourID  = lastSourceID; 
	row->SubA    = lastSubA; 
	row->freqID  = lastFQID; 
	row->TimeRange[0] = startTime;
	row->TimeRange[1] = lastTime;
	
	/* Loop over correlators flagging bad */
	for (i=0; i<ncorr; i++) { /* loop 210 */
	  if (badCor[i]) {
	    row->ants[0]  = 0; 
	    row->ants[1]  = 0; 
	    row->ifs[0]   = BIF + corIF[i] - 1; 
	    row->ifs[1]   = BIF + corIF[i] - 1; 
	    row->chans[0] = BChan + corChan[i] - 1; 
	    row->chans[1] = BChan + corChan[i] - 1; 
	    row->pFlags[0]=row->pFlags[1]=row->pFlags[2]=row->pFlags[3]=0; 
	    k = abs (corStok[i]);
	    /* bit flag implementation kinda screwy */
	    row->pFlags[0] |= 1<<(k-1);
	    
	    /* Write */
	    iFGRow = -1;
	    oretCode = ObitTableFGWriteRow (outFlag, iFGRow, row, err);
	    if (err->error) Obit_traceback_msg (err, routine, outFlag->name);
	  } /* end bad correlator section */
	} /* end loop flagging correlators */
	
	/* Loop over baselines/correlator flagging bad */
	for (i=0; i<numBL; i++) {
	  for (j=0; j<ncorr; j++) {
	    jndx = j*6 + i*6*ncorr;
	    /* Count flagged interval/correlator */
	    if ((acc[jndx]>0.0) && (badCor[j] || (acc[jndx+2] > acc[jndx+1])))
	      countBad++;
	    if ((!badCor[j])) {  /* Don't flag if correlator already flagged */
	      if ((acc[jndx]>0.0) && (acc[jndx+2] > acc[jndx+1])) {
		row->ants[0]   = BLAnt1[i]; 
		row->ants[1]   = BLAnt2[i]; 
		row->ifs[0]    = BIF + corIF[j] - 1; 
		row->ifs[1]    = BIF + corIF[j] - 1; 
		row->chans[0]  = BChan + corChan[j] - 1; 
		row->chans[1]  = BChan + corChan[j] - 1; 
		row->pFlags[0]=row->pFlags[1]=row->pFlags[2]=row->pFlags[3]=0; 
		k = abs (corStok[j]);
		/* bit flag implementation kinda screwy */
		row->pFlags[0] |= 1<<(k-1);
		
		/* Write */
		iFGRow = -1;
		oretCode = ObitTableFGWriteRow (outFlag, iFGRow, row, err);
		if (err->error) Obit_traceback_msg (err, routine, outFlag->name);
	      }  /* end flag correlator */
	    } /* end correlator not flagged */
	  } /* end loop over correlators */
	} /* end loop over baselines */
	
	/* Are we there yet??? */
	done = (inDesc->firstVis >= inDesc->nvis) || 
	  (iretCode==OBIT_IO_EOF);

	/* Reinitialize things */
	startTime = -1.0e20;
	endTime   =  1.0e20;
	for (i=0; i<6*ncorr*numBL; i++) acc[i] = 0.0;

      } /* end process interval */
      
    } /* end loop processing data */
    if (done) break;
  } /* end loop over intervals */
  
  /* check for errors */
  if ((iretCode > OBIT_IO_EOF) || (oretCode > OBIT_IO_EOF) ||
      (err->error)) Obit_traceback_msg (err, routine,inUV->name);
  
  /* close uv file */
  iretCode = ObitUVClose (inUV, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) 
    Obit_traceback_msg (err, routine, inUV->name);
  
  /* Close output table */
  oretCode = ObitTableFGClose (outFlag, err);
  if (err->error) Obit_traceback_msg (err, routine, outFlag->name);
  
  /* Reset number of vis per read to original value */
  dim[0] = dim[1] = dim[2] = 1;
  ObitInfoListAlwaysPut(inUV->info, "nVisPIO", OBIT_int, dim, &nVisPIO);

  /* Cleanup */
  /* Deallocate arrays */
  row     = ObitTableFGRowUnref(row);
  outFlag = ObitTableFGUnref(outFlag);
  g_free(acc);
  g_free(hissig);
  g_free(hiClip);
  g_free(corCnt);
  g_free(corBad);
  g_free(badCor);
  g_free(blLookup);
  g_free(maxRMS2);
  g_free(crossBL1);
  g_free(crossBL2);
  g_free(corChan);
  g_free(corIF);
  g_free(corStok);
  g_free(BLAnt1);
  g_free(BLAnt2);

  /* Give report */
  Obit_log_error(err, OBIT_InfoErr, "EditTD: flag %ld of %ld vis/interval= %5.1f percent\n",
		 countBad, countAll, 100.0*(gfloat)countBad/((gfloat)countAll));

  return;
} /* end  ObitUVEditTD */

/**
 * Stokes editing of UV data, FG table out
 *    All data on a given baseline/correlator are flagged if the 
 * amplitude of the datatype "FlagStok"  exceeds maxAmp.  
 * If a fraction of bad baselines on any antenna/channel/IF exceeds 
 * maxBad, then all data to that correlator is flagged.  
 *    The actual clipping level is the lesser of maxAmp and a value 
 * determined from a statistical analysis of each interval intended 
 * to flag the most discrepant 3 percent of the data.
 * Flagging entries are written into FG table flagTab.
 * Results are unpredictable for uncalibrated data.
 * Control parameters on info member of inUV:
 * \li "flagStok" OBIT_string (1,1,1) Stokes value to clip (I, Q, U, V, R, L)
 *                default = "V"
 * \li "flagTab" OBIT_int    (1,1,1) FG table version number [ def. 1]
 *               NB: this should not also being used to flag the input data!
 * \li "timeAvg" OBIT_float  (1,1,1) Time interval over which to determine
 *               data to be flagged (min) [def = 1 min.]
 * \li "maxAmp"  OBIT_float (1,1,1) Maximum VPol allowed
 * \li "maxBad"  OBIT_float (1,1,1) Fraction of allowed flagged baselines 
 *               to an antenna above which all baselines are flagged.
 *               [default 0.25]
 *
 * \param inUV     Input uv data to edit. 
 * \param outUV    UV data onto which the FG table is to be attached.
 *                 May be the same as inUV.
 * \param err      Error stack, returns if not empty.
 */
void ObitUVEditStokes (ObitUV *inUV, ObitUV *outUV, ObitErr *err)
{
  ObitIOCode iretCode, oretCode;
  ObitTableFG *outFlag=NULL;
  ObitTableFGRow *row=NULL;
  glong i, j, k, firstVis, startVis, suba, iFGRow, ver;
  glong countAll, countBad;
  gint lastSourceID, curSourceID, lastSubA, lastFQID;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM];
  gboolean gotOne;
  ObitIOAccess access;
  ObitUVDesc *inDesc;
  gfloat timeAvg, lastTime, maxAmp, maxBad, cbase, hisicc;
  gint *hissig=NULL, *blLookup=NULL;
  gint *corChan=NULL, *corIF=NULL, *corStok=NULL;
  gint *BLAnt1=NULL, *BLAnt2=NULL, BIF, BChan;
  gint flagTab, indx, jndx, kndx,  kndx2, nVisPIO, itemp, ant1, ant2;
  gint numCell, ncorr, numAnt, numBL, blindx, hicell;
  gboolean done, isbad, *badAnt=NULL;
  gfloat *acc=NULL, *corCnt=NULL, *corBad=NULL, *hiClip=NULL, *Buffer;
  gfloat startTime, endTime, curTime, avg, avg2, hisinc;
  gfloat mxamp2, sec;
  gchar *tname, Stokes[5], oStokes[5], reason[25];
  struct tm *lp;
  time_t clock;
  gchar *routine = "ObitUVEditStokes";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitUVIsA(inUV));
  g_assert (ObitUVIsA(outUV));

  /* Get control parameters */
  flagTab = 1;
  ObitInfoListGetTest(inUV->info, "flagTab", &type, dim, &flagTab);
  /* Time interval */
  timeAvg = 1.0;  /* default 1 min */
  ObitInfoListGetTest(inUV->info, "timeAvg", &type, dim, &timeAvg);
  if (timeAvg<=(1.0/60.0)) timeAvg = 1.0;
  timeAvg /= 1440.0;  /* convert to days */
 /* RMS clipping parameters, no default */
  ObitInfoListGet(inUV->info, "maxAmp", &type, dim, &maxAmp, err);
  mxamp2 = maxAmp * maxAmp;
  /* max. fraction bad baselines */
  maxBad = 0.25;           /* default 0.25 */
  ObitInfoListGetTest(inUV->info, "maxBad", &type, dim,  &maxBad);  
  if (err->error) Obit_traceback_msg (err, routine, inUV->name);
  /* Clipping Stokes type */
  Stokes[0] = 'V'; Stokes[1] = Stokes[2] = Stokes[3] = ' '; Stokes[4] = 0;
  ObitInfoListGetTest(inUV->info, "flagStok",   &type, dim, Stokes);  

  /* Data Selection */
  BIF = 1;
  ObitInfoListGetTest(inUV->info, "BIF", &type, dim, &BIF);
  if (BIF<1) BIF = 1;
  BChan = 1;
  ObitInfoListGetTest(inUV->info, "BChan", &type, dim, &BChan);
  if (BChan<1) BChan = 1;

  /* Set number of vis per read to 1 */
  nVisPIO = 1;
  ObitInfoListGetTest(inUV->info, "nVisPIO", &type, dim, &nVisPIO);
  itemp = 1;
  dim[0] = dim[1] = dim[2] = 1;
  ObitInfoListAlwaysPut(inUV->info, "nVisPIO", OBIT_int, dim, &itemp);

  /* Set Stokes to 'V' */
  oStokes[0] = oStokes[1] = oStokes[2] = oStokes[3] = ' '; oStokes[4] = 0;
  ObitInfoListGetTest(inUV->info, "Stokes", &type, dim, oStokes);
  dim[0] = strlen(Stokes); dim[1] = dim[2] = 1;
  ObitInfoListAlwaysPut(inUV->info, "Stokes", OBIT_string, dim, Stokes);

  /* Selection of input - generally will need to convert */
  access = OBIT_IO_ReadCal;

  /* test open to fully instantiate input and see if it's OK */
  iretCode = ObitUVOpen (inUV, access, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_msg (err, routine, inUV->name);
  inDesc  = inUV->myDesc;  /* Get descriptor */

  /* Create output FG table */
  tname = g_strconcat ("Flag Table for: ", outUV->name, NULL);
  ver = flagTab;
  outFlag = newObitTableFGValue(tname, (ObitData*)outUV, &ver, OBIT_IO_ReadWrite, 
				err);
  g_free (tname);
  if (err->error) Obit_traceback_msg (err, routine, inUV->name);
 
  /* Allocate arrays */
  numCell = 800;  /* number of cells in histogram */
  suba    = 1;
  numAnt  = inUV->myDesc->numAnt[suba-1];/* actually highest antenna number */
  numBL   = (numAnt*(numAnt-1))/2;
  ncorr   = inUV->myDesc->ncorr;
  /* acc index = type + corr * (3) + BL * (3*ncorr)  where BL = 0-rel baseline index */
  acc    = g_malloc (ncorr * 3 * numBL * sizeof(gfloat));
  /* hissig index  = cell + numCell*corr */
  hissig = g_malloc (ncorr * numCell * sizeof(gint));
  hisicc = 0.005;  /* Histogram resolution ??? This seems critical */
  hisinc = hisicc*maxAmp; /* Histogram increment */
  hiClip = g_malloc (ncorr * sizeof(gfloat));
  /* corCntt index  = ant + corr*numAnt , ant = 0-rel ant number */
  corCnt = g_malloc (ncorr * numAnt * sizeof(gfloat));
  /* corBad index  = ant + corr*numAnt , ant = 0-rel ant number */
  corBad = g_malloc (ncorr * numAnt * sizeof(gfloat));
  /* badAnt index  = ant + corr*numAnt , ant = 0-rel ant number */
  badAnt = g_malloc (ncorr * numAnt * sizeof(gboolean));
  corChan  = g_malloc (ncorr * sizeof(gint));    /* Correlator Channel */
  corIF    = g_malloc (ncorr * sizeof(gint));    /* Correlator IF */
  corStok  = g_malloc (ncorr * sizeof(gint));    /* Correlator Stokes */

  /* Baseline tables */
  blLookup = g_malloc0(numAnt*sizeof(gint));
  BLAnt1   = g_malloc (numBL * sizeof(gint));    /* First antenna of baseline */
  BLAnt2   = g_malloc (numBL * sizeof(gint));    /* Second antenna of baseline */
  blLookup[0] = 0;
  k = 0;
  for (j=2; j<=numAnt; j++) {BLAnt1[k]=1; BLAnt2[k]=j; k++;}
  for (i=1; i<numAnt; i++) {
    blLookup[i] = blLookup[i-1] + numAnt-i;
    for (j=i+2; j<=numAnt; j++) {BLAnt1[k]=i+1; BLAnt2[k]=j; k++;}
  }

  /* Initialize things */
  startTime = -1.0e20;
  endTime   =  1.0e20;
  lastSourceID = -1;
  curSourceID  = 0;
  lastSubA     = 0;
  countAll     = 0;
  countBad     = 0;
  for (i=0; i<3*ncorr*numBL; i++) acc[i] = 0.0;

  Buffer = inUV->buffer;

  /* Digest visibility info */
  digestCorr (inDesc, &maxAmp, NULL, NULL, NULL, corChan, corIF, corStok);

  /* Open output table */
  oretCode = ObitTableFGOpen (outFlag, OBIT_IO_ReadWrite, err);
  if (err->error) Obit_traceback_msg (err, routine, outFlag->name);
  
  /* Create Row */
  row = newObitTableFGRow (outFlag);
  
  /* Attach row to output buffer */
  ObitTableFGSetRow (outFlag, row, err);
  if (err->error) Obit_traceback_msg (err, routine, inUV->name);
  
  /* Initialize solution row */
  row->SourID  = 0; 
  row->SubA    = 0; 
  row->freqID  = 0; 
  row->ants[0] = 0; 
  row->ants[1] = 0; 
  row->TimeRange[0] = -1.0e20; 
  row->TimeRange[1] =  1.0e20; 
  row->ifs[0]    = BIF; 
  row->ifs[1]    = 0; 
  row->chans[0]  = BChan; 
  row->chans[1]  = 0; 
  row->pFlags[0] = 1<<0 | 1<<1 | 1<<2 | 1<<3; 
  row->pFlags[1] = 0; 
  row->pFlags[2] = 0; 
  row->pFlags[3] = 0; 
  /* Reason includes time/date */
  /* Get time since 00:00:00 GMT, Jan. 1, 1970 in seconds. */
  time (&clock);
  /* Convert to  broken-down time. */
  lp = localtime (&clock);
  if (lp->tm_year<1000)  lp->tm_year += 1900; /* full year */
  lp->tm_mon = lp->tm_mon+1; /* For some bizzare reason, month is 0-rel */
  sec = (gfloat)lp->tm_sec;
  g_snprintf (reason, 25, "Edit %d/%d/%d %d:%d:%f", 
	      lp->tm_year, lp->tm_mon, lp->tm_mday, 
	      lp->tm_hour, lp->tm_min, sec);
  row->reason    = reason; /* Unique string */
  
  /* Loop over intervals */
  done   = FALSE;
  gotOne = FALSE;
  while (!done) {
    
    /* we're in business - loop through data - one vis per read */
    while ((iretCode==OBIT_IO_OK) && (oretCode==OBIT_IO_OK)) {
      if (!gotOne) { /* need to read new record? */
	iretCode = ObitUVReadSelect (inUV, inUV->buffer, err);
	/*if (iretCode!=OBIT_IO_OK) break;*/
	firstVis = inDesc->firstVis;
      }

      gotOne = FALSE;
      /* Time */
      curTime = Buffer[inDesc->iloct];
      if (inDesc->ilocsu>=0) curSourceID = Buffer[inDesc->ilocsu];
      if (startTime < -1000.0) {  /* Set time window etc. if needed */
	startTime = curTime;
	lastTime = curTime;
	endTime   = startTime +  timeAvg;
	startVis  = firstVis;
	lastSourceID = curSourceID;
      }

      /* Still in current interval/source? */
      if ((curTime<endTime) && (curSourceID == lastSourceID) && 
	  (inDesc->firstVis<=inDesc->nvis) && (iretCode==OBIT_IO_OK)) {
	/* accumulate statistics */
	cbase = Buffer[inUV->myDesc->ilocb]; /* Baseline */
	ant1 = (cbase / 256.0) + 0.001;
	ant2 = (cbase - ant1 * 256) + 0.001;
	lastSubA = (gint)(100.0 * (cbase -  ant1 * 256 - ant2) + 0.5);
	/* Baseline index this assumes a1<a2 always */
	blindx =  blLookup[ant1-1] + ant2-ant1-1;
	if (inDesc->ilocfq>=0) lastFQID = (gint)(Buffer[inDesc->ilocfq]+0.5);
	else lastFQID = 0;
	lastTime = curTime;

	/* Accumulate
	   (1,*) =  count 
	   (2,*) =  sum real then amplitude
	   (3,*) =  sum imaginary then limit */
	indx = inDesc->nrparm; /* offset of start of vis data */
	for (i=0; i<ncorr; i++) { /* loop 120 */
	  if (Buffer[indx+2] > 0.0) {
	    jndx = i*3 + blindx*3*ncorr;
	    acc[jndx]   += 1.0;
	    acc[jndx+1] += Buffer[indx];
	    acc[jndx+2] += Buffer[indx+1];
	  } 
	  indx += 3;
	} /* end loop  L120 over correlations */;
      } else {  /* process interval */
	
	/* Now have the next record in the IO Buffer */
	if (iretCode==OBIT_IO_OK) gotOne = TRUE;
	    
	/* Get average amplitude/collect histogram */
	for (i=0; i<ncorr * numCell; i++)  hissig[i] = 0;
	for (i=0; i<numBL; i++) { /* loop 170 */
	  for (j=0; j<ncorr; j++) { /* loop 160 */
 	    jndx = j*3 + i*3*ncorr;
	    if (acc[jndx] > 0.0) countAll++;  /* count all possibilities */
	    if (acc[jndx] > 0.5) {
	      /* Average amplitude */
	      avg = sqrt (acc[jndx+1]*acc[jndx+1] + acc[jndx+2]*acc[jndx+2]) / acc[jndx];
	      acc[jndx+1] = avg*avg;
	      /* Histogram */
	      hicell = avg / hisinc;
	      hicell = MIN (hicell, numCell-1);
	      hissig[hicell+j*numCell]++;
	    } 
	  } /* end loop  L160: */
	} /* end loop  L170: */

	/* Set histogram clipping levels. */
	editHist (ncorr, numCell, hisinc, hissig, hiClip);

	/* No histogram flagging if clip level exceeds 10. */
	if (maxAmp >= 10.0) {
	  for (j=0; j<ncorr; j++) { /* loop 180 */
            hiClip[j] = maxAmp * maxAmp;
	  } /* end loop  L180: */;
	}

	/* initialize counters */
	for (i=0; i<ncorr*numAnt; i++) corCnt[i] = corBad[i] = 0;

	/* Find bad baselines. */
	for (i=0; i<numBL; i++) { /* loop 200 */
	  for (j=0; j<ncorr; j++) { /* loop 190 */
 	    jndx = j*3 + i*3*ncorr;  /* acc index */
 	    kndx  = BLAnt1[i]-1 + j*numAnt;      /* first antenna index */
 	    kndx2 = BLAnt2[i]-1 + j*numAnt;      /* second antenna index */
	    if (acc[jndx] > 0.5) {
	      avg2 = acc[jndx+1];
	      /* Is this one bad? */
	      acc[jndx+2] = MIN (mxamp2, hiClip[j]);
	      isbad = avg2  >  acc[jndx+2];

	      /* Ant/Correlator info */
	      corCnt[kndx]++;
	      corCnt[kndx2]++;
	      if (isbad) {
		/* Make sure it is flagged. */
		acc[jndx+1] = 1.0e20;
		acc[jndx+2] = 0.0;
		corBad[kndx]++;
		corBad[kndx2]++;
	      } 
	    } else if (acc[jndx] > 0.0) {
	      /* Flag correlators without enough data. */
	      acc[jndx+1] = 1.0e20;
	      acc[jndx+2] = 0.0;
	    }
	  } /* end loop  L190: */
	} /* end loop  L200: */
	
	/* Check for bad antenna/correlators */
	for (i=0; i<numAnt; i++) {
	  for (j=0; j<ncorr; j++) {
	    kndx = i + j*numAnt;
	    badAnt[kndx] = FALSE;
	    if (corCnt[kndx] > 1.1) {
	      if ((corBad[kndx]/corCnt[kndx])  >  maxBad) {
		/* Kill antenna/correlator... */
		badAnt[kndx] = TRUE;
	      }
	    }
	  } 
	}
	
	/* Init Flagging table entry */
	row->SourID  = lastSourceID; 
	row->SubA    = lastSubA; 
	row->freqID  = lastFQID; 
	row->TimeRange[0] = startTime;
	row->TimeRange[1] = lastTime;
	
	/* Loop over antennas/correlator flagging bad */
	for (i=0; i<numAnt; i++) { /* loop 200 */
	  for (j=0; j<ncorr; j++) { /* loop 190 */
 	    kndx  = i + j*numAnt;      /* antenna index */
	    if (badAnt[kndx]) {
	      row->ants[0]  = i+1;   /* 1-rel antenna number */
	      row->ants[1]  = 0; 
	      row->ifs[0]   = BIF + corIF[j] - 1; 
	      row->ifs[1]   = BIF + corIF[j] - 1; 
	      row->chans[0] = BChan + corChan[j] - 1; 
	      row->chans[1] = BChan + corChan[j] - 1; 
	      
	      /* Write */
	      iFGRow = -1;
	      oretCode = ObitTableFGWriteRow (outFlag, iFGRow, row, err);
	      if (err->error) Obit_traceback_msg (err, routine, outFlag->name);
	    } /* end bad correlator section */
	  } /* end loop flagging correlators */
	} /* end loop over antennas */
	
	/* Loop over baselines/correlator flagging bad */
	for (i=0; i<numBL; i++) {
	  for (j=0; j<ncorr; j++) {
	    jndx = j*3 + i*3*ncorr;  /* acc index */
 	    kndx  = BLAnt1[i]-1 + j*numAnt;      /* first antenna index */
 	    kndx2 = BLAnt2[i]-1 + j*numAnt;      /* second antenna index */
	    isbad = badAnt[kndx] || badAnt[kndx2];
	    /* Count flagged interval/correlator */
	    if ((acc[jndx]>0.0) && (isbad || (acc[jndx+1] > acc[jndx+2])))
	      countBad++;
	    if (!isbad) {  /* Don't flag if ant/correlator already flagged */
	      if ((acc[jndx]>0.0) && (acc[jndx+1] > acc[jndx+2])) {
		row->ants[0]   = BLAnt1[i]; 
		row->ants[1]   = BLAnt2[i]; 
		row->ifs[0]    = BIF + corIF[j] - 1; 
		row->ifs[1]    = BIF + corIF[j] - 1; 
		row->chans[0]  = BChan + corChan[j] - 1; 
		row->chans[1]  = BChan + corChan[j] - 1; 
		
		/* Write */
		iFGRow = -1;
		oretCode = ObitTableFGWriteRow (outFlag, iFGRow, row, err);
		if (err->error) Obit_traceback_msg (err, routine, outFlag->name);
	      }  /* end flag correlator */
	    } /* end correlator not flagged */
	  } /* end loop over correlators */
	} /* end loop over baselines */
	
	/* Are we there yet??? */
	done = (inDesc->firstVis >= inDesc->nvis) || 
	  (iretCode==OBIT_IO_EOF);

	/* Reinitialize things */
	startTime = -1.0e20;
	endTime   =  1.0e20;
	for (i=0; i<3*ncorr*numBL; i++) acc[i] = 0.0;

      } /* end process interval */
      
    } /* end loop processing data */
    if (done) break;
  } /* end loop over intervals */
  
  /* check for errors */
  if ((iretCode > OBIT_IO_EOF) || (oretCode > OBIT_IO_EOF) ||
      (err->error)) Obit_traceback_msg (err, routine,inUV->name);
  
  /* close uv file */
  iretCode = ObitUVClose (inUV, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) 
    Obit_traceback_msg (err, routine, inUV->name);
  
  /* Close output table */
  oretCode = ObitTableFGClose (outFlag, err);
  if (err->error) Obit_traceback_msg (err, routine, outFlag->name);
  
  /* Reset number of vis per read to original value */
  dim[0] = 1;
  ObitInfoListAlwaysPut(inUV->info, "nVisPIO", OBIT_int, dim, &nVisPIO);
  /* Reset Stokes */
  dim[0] = strlen(oStokes);
  ObitInfoListAlwaysPut(inUV->info, "Stokes", OBIT_string, dim, oStokes);

  /* Cleanup */
  /* Deallocate arrays */
  row     = ObitTableFGRowUnref(row);
  outFlag = ObitTableFGUnref(outFlag);
  g_free(acc);
  g_free(hissig);
  g_free(hiClip);
  g_free(corCnt);
  g_free(corBad);
  g_free(badAnt);
  g_free(blLookup);
  g_free(corChan);
  g_free(corIF);
  g_free(corStok);
  g_free(BLAnt1);
  g_free(BLAnt2);

  /* Give report */
  Obit_log_error(err, OBIT_InfoErr, "Edit%c: flag %ld of %ld vis/interval= %5.1f percent\n",
		 Stokes[0], countBad, countAll, 100.0*(gfloat)countBad/((gfloat)countAll));

  return;
} /* end  ObitUVEditStokes */

/**
 * Clip a uv data set.  Writes edited UV data
 * Control parameters are on the inUV info member:
 * \li "maxAmp" OBIT_float  (1,1,1) Maximum allowed amplitude
 * \li "oper"   OBIT_string (4,1,1) operation type:
 *            "flag" flag data with amplitudes in excess of maxAmp
 *            "clip" clip amplitudes at maxAmp and preserve phase
 *            default is "flag"
 * \param inUV     Input uv data to clip. 
 * \param scratch  True if scratch file desired, will be same type as inUV.
 * \param outUV    If not scratch, then the previously defined output file
 *                 May be the same as inUV.
 *                 May be NULL for scratch only
 *                 If it exists and scratch, it will be Unrefed
 * \param err      Error stack, returns if not empty.
 * \return the clipped ObitUV.
 */
ObitUV* ObitUVEditClip (ObitUV *inUV, gboolean scratch, ObitUV *outUV, 
			ObitErr *err)
{
  ObitIOCode iretCode, oretCode;
  gboolean doCalSelect;
  gchar *exclude[]={"AIPS CL","AIPS SN","AIPS FG","AIPS CQ","AIPS WX",
		    "AIPS AT","AIPS CT","AIPS OB","AIPS IM","AIPS MC",
		    "AIPS PC","AIPS NX","AIPS TY","AIPS GC","AIPS HI",
		    "AIPS PL",
		    NULL};
  gchar *sourceInclude[] = {"AIPS SU", NULL};
  glong i, j, indx, firstVis;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM];
  gfloat maxAmp, maxAmp2, amp, amp2, ratio;
  gboolean same, doFlag;
  ObitIOAccess access;
  ObitUVDesc *inDesc, *outDesc;
  gchar oper[5];
  gchar *routine = "ObitUVEditClip";
 
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return outUV;
  g_assert (ObitUVIsA(inUV));
  if (!scratch && (outUV==NULL)) {
    Obit_log_error(err, OBIT_Error,"%s Output MUST be defined for non scratch files",
		   routine);
      return outUV;
  }

  /* Get control parameters */
  /* Maximum amplitude */
  ObitInfoListGet(inUV->info, "maxAmp", &type, (gint32*)dim, &maxAmp, err);
  maxAmp2 = MAX (maxAmp*maxAmp, 1.0e-20);  /* get square */
  oper[0] = 'f'; oper[1] = 'l'; oper[2] = 'a'; oper[3] = 'g'; oper[4] = 0; 
  ObitInfoListGetTest(inUV->info, "oper",   &type, (gint32*)dim,  oper);  
  if (err->error) Obit_traceback_val (err, routine, inUV->name, outUV);
  doFlag = !strncmp (oper, "flag", 4);

 /* Create scratch? */
  if (scratch) {
    if (outUV) outUV = ObitUVUnref(outUV);
    outUV = newObitUVScratch (inUV, err);
  } else if (!same) { /* non scratch output must exist - clone from inUV */
    ObitUVClone (inUV, outUV, err);
  }
  if (err->error) Obit_traceback_val (err, routine, inUV->name, outUV);

  /* Selection of input? */
  doCalSelect = FALSE;
  ObitInfoListGetTest(inUV->info, "doCalSelect", &type, (gint32*)dim, &doCalSelect);
  if (doCalSelect) access = OBIT_IO_ReadCal;
  else access = OBIT_IO_ReadOnly;

  /* Are input and output the same file? */
  same = ObitUVSame(inUV, outUV, err);
  if (err->error) Obit_traceback_val (err, routine, inUV->name, outUV);

  /* test open to fully instantiate input and see if it's OK */
  iretCode = ObitUVOpen (inUV, access, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, inUV->name, outUV);

  /* copy Descriptor */
  if (!same) outUV->myDesc = ObitUVDescCopy(inUV->myDesc, outUV->myDesc, err);

  /* use same data buffer on input and output 
     so don't assign buffer for output */
  if (!same && outUV->buffer) ObitIOFreeBuffer(outUV->buffer);
  outUV->buffer = inUV->buffer;
  outUV->bufferSize = -1;

  /* test open output */
  oretCode = ObitUVOpen (outUV, OBIT_IO_WriteOnly, err);
  /* If this didn't work try OBIT_IO_ReadWrite */
  if ((oretCode!=OBIT_IO_OK) || (err->error)) {
    ObitErrClear(err);
    oretCode = ObitUVOpen (outUV, OBIT_IO_ReadWrite, err);
  }
  /* if it didn't work bail out */
  if ((oretCode!=OBIT_IO_OK) || (err->error)) {
    /* unset output buffer (may be multiply deallocated) */
    outUV->buffer = NULL;
    outUV->bufferSize = 0;
    Obit_traceback_val (err, routine, outUV->name, outUV);
  }

  /* Copy tables before data if different files */
  if (!same) {
    iretCode = ObitUVCopyTables (inUV, outUV, exclude, NULL, err);
    /* If multisource out then copy SU table, multiple sources selected or
       sources deselected suggest MS out */
    if ((inUV->mySel->numberSourcesList>1) || (!inUV->mySel->selectSources))
      iretCode = ObitUVCopyTables (inUV, outUV, NULL, sourceInclude, err);
    if (err->error) {
      outUV->buffer = NULL;
      outUV->bufferSize = 0;
      Obit_traceback_val (err, routine, inUV->name, outUV);
    }
  } /* end of copy tables */

  /* reset to beginning of uv data */
  iretCode = ObitIOSet (inUV->myIO,  inUV->info, err);
  oretCode = ObitIOSet (outUV->myIO, outUV->info, err);
  if (err->error) Obit_traceback_val (err, routine,inUV->name, outUV);

  /* Close and reopen input to init calibration which will have been disturbed 
     by the table copy */
  iretCode = ObitUVClose (inUV, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,inUV->name, outUV);

  iretCode = ObitUVOpen (inUV, access, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,inUV->name, outUV);

  /* Get descriptors */
  inDesc  = inUV->myDesc;
  outDesc = outUV->myDesc;

  /* we're in business, copy, zero data, set weight to 1 */
  while ((iretCode==OBIT_IO_OK) && (oretCode==OBIT_IO_OK)) {
    if (doCalSelect) iretCode = ObitUVReadSelect (inUV, inUV->buffer, err);
    else iretCode = ObitUVRead (inUV, inUV->buffer, err);
    if (iretCode!=OBIT_IO_OK) break;
    firstVis = inUV->myDesc->firstVis;

   /* How many */
    outDesc->numVisBuff = inDesc->numVisBuff;

    /* Clip data */
    for (i=0; i<inDesc->numVisBuff; i++) { /* loop over visibilities */
      indx = i*inDesc->lrec + inDesc->nrparm;
      for (j=0; j<inDesc->ncorr; j++) { /* loop over correlations */
	if (inUV->buffer[indx+2] <= 0.0) continue;  /* flagged? */

	/* is value OK */
	amp2 = inUV->buffer[indx]*inUV->buffer[indx] + 
	  inUV->buffer[indx+1]*inUV->buffer[indx+1];
	if (amp2<maxAmp2) continue;

	if (doFlag) {  /* flag excessive values? */
	  inUV->buffer[indx+2] = -fabs(inUV->buffer[indx+2]);
	} /* end if doFlag */ 

	else { /* clip */
	  amp = sqrt (amp2);
	  ratio = maxAmp / MAX (1.0e-10, amp);
	  inUV->buffer[indx]   *= ratio;
	  inUV->buffer[indx+1] *= ratio;
	  inUV->buffer[indx+2] /= ratio;
	}
      } /* end loop over correlations */
    } /* end loop over visibilities */

    /* Write */
    oretCode = ObitUVWrite (outUV, inUV->buffer, err);
    /* suppress vis number update if rewriting the same file */
    if (same) {
      outUV->myDesc->firstVis = firstVis;
      ((ObitUVDesc*)(inUV->myIO->myDesc))->firstVis = firstVis;
    }
  } /* end loop processing data */
  
  /* check for errors */
  if ((iretCode > OBIT_IO_EOF) || (oretCode > OBIT_IO_EOF) ||
      (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,inUV->name, outUV);
  
  /* unset input buffer (may be multiply deallocated ;'{ ) */
  outUV->buffer = NULL;
  outUV->bufferSize = 0;
  
  /* close files */
  iretCode = ObitUVClose (inUV, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) 
    Obit_traceback_val (err, routine, inUV->name, outUV);
  
  oretCode = ObitUVClose (outUV, err);
  if ((oretCode!=OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, outUV->name, outUV);
  
  return outUV;
} /* end ObitUVEditClip */

/**
 * Clip a uv data set by amplitudes of a given Stokes. Writes edited UV data
 * Data with amplitudes of the selected stokes
 * in excess of maxAmp are flagged.  Optionally all correlations associated
 * may be flagged.  Stokes conversion as needed for test.
 * Control parameters are on the inUV info member:
 * \li "flagStok" OBIT_string (1,1,1) Stokes value to clip (I, Q, U, V, R, L)
 *                default = "I"
 * \li "flagAll"  Obit_bool   (1,1,1) if true, flag all associated correlations
 *                default = True
 * \li "maxAmp"   OBIT_float  (1,1,1) Maximum allowed amplitude
 *
 * \param inUV     Input uv data to clip. 
 * \param scratch  True if scratch file desired, will be same type as inUV.
 * \param outUV    If not scratch, then the previously defined output file
 *                 May be the same as inUV.
 *                 May be NULL for scratch only
 *                 If it exists and scratch, it will be Unrefed
 * \param err      Error stack, returns if not empty.
 * \return the clipped ObitUV.
 */
ObitUV* ObitUVEditClipStokes (ObitUV *inUV, gboolean scratch, ObitUV *outUV, 
			      ObitErr *err)
{
  ObitIOCode iretCode, oretCode;
  gboolean doCalSelect;
  gchar *exclude[]={"AIPS CL","AIPS SN","AIPS FG","AIPS CQ","AIPS WX",
		    "AIPS AT","AIPS CT","AIPS OB","AIPS IM","AIPS MC",
		    "AIPS PC","AIPS NX","AIPS TY","AIPS GC","AIPS HI",
		    "AIPS PL",
		    NULL};
  gchar *sourceInclude[] = {"AIPS SU", NULL};
  glong i, indx, cntAll, cntFlagged, firstVis;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM];
  gfloat maxAmp, maxAmp2, amp2, selFact[2], vis[2], sf1, sf2;
  gfloat wt1, wt2, *Buffer;
  gboolean same, flagAll, flagIt, bothCorr, doConjg;
  gint ichan, iif, istok, nchan, nif, nstok, kstoke0;
  gint incs, incf, incif, jadr[2], ioff, lfoff, ivoff, ip1, ip2;
  ObitIOAccess access;
  ObitUVDesc *inDesc, *outDesc;
  gchar stok[5];
  gchar *routine = "ObitUVEditClipStokes";
 
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return outUV;
  g_assert (ObitUVIsA(inUV));
  if (!scratch && (outUV==NULL)) {
    Obit_log_error(err, OBIT_Error,"%s Output MUST be defined for non scratch files",
		   routine);
      return outUV;
  }

  /* Get control parameters */
  /* Flagging Stokes type */
  stok[0] = 'I'; stok[1] = 0;
  ObitInfoListGetTest(inUV->info, "flagStok",   &type, (gint32*)dim,  stok);  
  flagAll = TRUE;
  ObitInfoListGetTest(inUV->info, "flagAll ",   &type, (gint32*)dim,  &flagAll);  
  /* Maximum amplitude */
  ObitInfoListGet(inUV->info, "maxAmp", &type, (gint32*)dim, &maxAmp, err);
  maxAmp2 = MAX (maxAmp*maxAmp, 1.0e-20);  /* get square */
  if (err->error) Obit_traceback_val (err, routine, inUV->name, outUV);

   /* Are input and output the same file? */
  same = ObitUVSame(inUV, outUV, err);
  if (err->error) Obit_traceback_val (err, routine, inUV->name, outUV);

  /* Create scratch? */
  if (scratch) {
    if (outUV) outUV = ObitUVUnref(outUV);
    outUV = newObitUVScratch (inUV, err);
  } else if (!same) { /* non scratch output must exist - clone from inUV */
    ObitUVClone (inUV, outUV, err);
  }
  if (err->error) Obit_traceback_val (err, routine, inUV->name, outUV);

  /* Selection of input? */
  doCalSelect = FALSE;
  ObitInfoListGetTest(inUV->info, "doCalSelect", &type, (gint32*)dim, &doCalSelect);
  if (doCalSelect) access = OBIT_IO_ReadCal;
  else access = OBIT_IO_ReadOnly;

  /* Are input and output the same file? */
  same = ObitUVSame(inUV, outUV, err);
  if (err->error) Obit_traceback_val (err, routine, inUV->name, outUV);

  /* test open to fully instantiate input and see if it's OK */
  iretCode = ObitUVOpen (inUV, access, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, inUV->name, outUV);

  /* copy Descriptor */
  if (!same) outUV->myDesc = ObitUVDescCopy(inUV->myDesc, outUV->myDesc, err);

  /* use same data buffer on input and output 
     so don't assign buffer for output */
  if (!same && outUV->buffer) {
    ObitIOFreeBuffer(outUV->buffer);
    outUV->buffer = inUV->buffer;
    outUV->bufferSize = -1;
  }

  /* test open output */
  oretCode = ObitUVOpen (outUV, OBIT_IO_WriteOnly, err);
  /* If this didn't work try OBIT_IO_ReadWrite */
  if ((oretCode!=OBIT_IO_OK) || (err->error)) {
    ObitErrClear(err);
    oretCode = ObitUVOpen (outUV, OBIT_IO_ReadWrite, err);
  }
  /* if it didn't work bail out */
  if ((oretCode!=OBIT_IO_OK) || (err->error)) {
    /* unset output buffer (may be multiply deallocated) */
    outUV->buffer = NULL;
    outUV->bufferSize = 0;
    Obit_traceback_val (err, routine, outUV->name, outUV);
  }

  /* Copy tables before data if different files */
  if (!same) {
    iretCode = ObitUVCopyTables (inUV, outUV, exclude, NULL, err);
    /* If multisource out then copy SU table, multiple sources selected or
       sources deselected suggest MS out */
    if ((inUV->mySel->numberSourcesList>1) || (!inUV->mySel->selectSources))
      iretCode = ObitUVCopyTables (inUV, outUV, NULL, sourceInclude, err);
    if (err->error) {
      outUV->buffer = NULL;
      outUV->bufferSize = 0;
      Obit_traceback_val (err, routine, inUV->name, outUV);
    }
  } /* end of copy tables */

  /* reset to beginning of uv data */
  iretCode = ObitIOSet (inUV->myIO,  inUV->info, err);
  oretCode = ObitIOSet (outUV->myIO, outUV->info, err);
  if (err->error) Obit_traceback_val (err, routine,inUV->name, outUV);

  /* Close and reopen input to init calibration which will have been disturbed 
     by the table copy */
  iretCode = ObitUVClose (inUV, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,inUV->name, outUV);

  iretCode = ObitUVOpen (inUV, access, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,inUV->name, outUV);
  Buffer = inUV->buffer; /* pointer to buffer to work from */

  /* Get descriptors */
  inDesc  = inUV->myDesc;
  outDesc = outUV->myDesc;

  /* Set up for parsing data */
  nchan = inDesc->inaxes[inDesc->jlocf];
  if (inDesc->jlocf>=0) nif = inDesc->inaxes[inDesc->jlocif];
  else nif = 1;
  if (inDesc->jlocs>=0) nstok = inDesc->inaxes[inDesc->jlocs];
  else nstok = 1;
  /* Get first stokes index */
  if (inDesc->crval[inDesc->jlocs]> 0.0) {
    kstoke0 = inDesc->crval[inDesc->jlocs] + 
      (1.0-inDesc->crpix[inDesc->jlocs]) * inDesc->cdelt[inDesc->jlocs] + 0.5;
  } else {
    kstoke0 = inDesc->crval[inDesc->jlocs] + 
      (1.0-inDesc->crpix[inDesc->jlocs]) * inDesc->cdelt[inDesc->jlocs] - 0.5;
  } 
  /* Data expanded to 3 words per vis */
  incs  = 3 * inDesc->incs  / inDesc->inaxes[0];
  incf  = 3 * inDesc->incf  / inDesc->inaxes[0];
  incif = 3 * inDesc->incif / inDesc->inaxes[0];
  bothCorr = FALSE;
  doConjg  = FALSE;
  
  /* Offsets and stuff by desirec Stokes */
  switch (stok[0]) {
  case ' ':  /* Treat blank as 'I' */
  case 'I':
    if (kstoke0 > 0) {  /* data in Stokes */
      jadr[0] = (1-kstoke0) * incs;
      jadr[1] = jadr[0];
      selFact[0] = 1.0;
      selFact[1] = 0.0;
    } else { /* data correlation based */
      jadr[0] = 0;
      jadr[1] = jadr[0] + incs;
      selFact[0] = 0.5;
      selFact[1] = 0.5;
      /* check if only RR or LL and if so use it. */
      if ((nstok < 2)  ||  (kstoke0 != -1)) jadr[1] = jadr[0];
    }
    break;
  case 'Q':
    if (kstoke0 > 0) {  /* data in Stokes */
      jadr[0] = (2-kstoke0) * incs;
      jadr[1] = jadr[0];
      selFact[0] = 1.0;
      selFact[1] = 0.0;
    } else { /* data correlation based */
      bothCorr = TRUE;    /* Need both correlations for this */
      jadr[0] = (3+kstoke0) * incs;
      jadr[1] = jadr[0] + incs;
      selFact[0] = 0.5;
      selFact[1] = 0.5;
    }
    break;
  case 'U':
    if (kstoke0 > 0) {  /* data in Stokes */
      jadr[0] = (3-kstoke0) * incs;
      jadr[1] = jadr[0];
      selFact[0] = 1.0;
      selFact[1] = 0.0;
    } else { /* data correlation based */
      bothCorr = TRUE;    /* Need both correlations for this */
      doConjg  = TRUE;   /* Need to conjugate result */
      jadr[0] = (3+kstoke0) * incs;
      jadr[1] = jadr[0] + incs;
      selFact[0] = -0.5;
      selFact[1] =  0.5;
    }
    break;
  case 'V':
    if (kstoke0 > 0) {  /* data in Stokes */
      jadr[0] = (4-kstoke0) * incs;
      jadr[1] = jadr[0];
      selFact[0] = 1.0;
      selFact[1] = 0.0;
    } else { /* data correlation based */
      bothCorr = TRUE;    /* Need both correlations for this */
      jadr[0] = (1+kstoke0) * incs;
      jadr[1] = jadr[0] + incs;
      selFact[0] =  0.5;
      selFact[1] = -0.5;
    }
    break;
  case 'R':
    if (kstoke0 > 0) {  /* data in Stokes */
      bothCorr = TRUE;    /* Need both correlations for this */
      jadr[0] = (1-kstoke0) * incs;
      jadr[1] = (4-kstoke0) * incs;
      selFact[0] = 0.5;
      selFact[1] = 0.5;
    } else { /* data correlation based */
      jadr[0] = (1+kstoke0) * incs;
      jadr[1] = jadr[0];
      selFact[0] =  1.0;
      selFact[1] =  0.0;
    }
    break;
  case 'L':
    if (kstoke0 > 0) {  /* data in Stokes */
      bothCorr = TRUE;    /* Need both correlations for this */
      jadr[0] = (1-kstoke0) * incs;
      jadr[1] = (4-kstoke0) * incs;
      selFact[0] = -0.5;
      selFact[1] = -0.5;
    } else { /* data correlation based */
      jadr[0] = (2+kstoke0) * incs;
      jadr[1] = jadr[0];
      selFact[0] =  1.0;
      selFact[1] =  0.0;
    }
    break;
  default:
    Obit_log_error(err, OBIT_Error,"%s Unknown Stokes: %c",
		   routine, stok[0]);
    return outUV;
  }; /* end setup switch 0n Stokes */
  
  /* Abs value of selFact */
  sf1 = fabs(selFact[0]);
  sf2 = fabs(selFact[1]);
  cntAll = 0; cntFlagged = 0;  /* counts */
  
  /* we're in business, check, flag data */
  while ((iretCode==OBIT_IO_OK) && (oretCode==OBIT_IO_OK)) {
    if (doCalSelect) iretCode = ObitUVReadSelect (inUV, inUV->buffer, err);
    else iretCode = ObitUVRead (inUV, inUV->buffer, err);
    if (iretCode!=OBIT_IO_OK) break;
    firstVis = inUV->myDesc->firstVis;

    /* How many in buffer? */
    outDesc->numVisBuff = inDesc->numVisBuff;

    /* Loop over buffer */
    for (i=0; i<inDesc->numVisBuff; i++) { /* loop over visibilities */
      indx = i*inDesc->lrec + inDesc->nrparm; /* offset of start of vis data */
      lfoff = -incif - incf;

      /* loop over IF */
      for (iif=0; iif<nif; iif++) {
	lfoff = lfoff + incif;
	ioff  = lfoff;

	/* Loop over frequency channel */
	for (ichan=0; ichan<nchan; ichan++) { /* loop 60 */
	  ioff  = ioff + incf;
	  ivoff = indx + ioff;  /* Buffer offset of real part of first Stokes */

	  /* set input visibility indices of correlations tested */
	  ip1 = ivoff + jadr[0];
	  ip2 = ivoff + jadr[1];

	  /* Need at least one good */
	  if (((sf1*Buffer[ip1+2])<0.0) && ((sf2*Buffer[ip2+2])<0.0)) continue;
	  /* Need both? */
	  if (bothCorr && 
	      (((sf1*Buffer[ip1+2])<0.0) || ((sf2*Buffer[ip2+2])<0.0))) continue;

	  cntAll++;  /* total previously unflagged */

	  /* Do conversion */
	  if (Buffer[ip1+2]>0.0) wt1 = selFact[0];
	  else wt1 = 0.0;
	  if (Buffer[ip2+2]>0.0) wt2 = selFact[1];
	  else wt2 = 0.0;
	  vis[0] = Buffer[ip1+0] * wt1 + Buffer[ip2+0] * wt2;
	  vis[1] = Buffer[ip1+1] * wt1 + Buffer[ip2+1] * wt2;
	  amp2 = (vis[0]*vis[0] +  vis[1]*vis[1]) / ((wt1+wt2)*(wt1+wt2));

	  /* Flag? */
	  flagIt = amp2 > maxAmp2;
	  if (!flagIt) continue;
	  cntFlagged++;   /* number flagged */

	  /* All stokes or just data in test? */
	  if (flagAll) { /* Flag all Stokes */
	    for (istok=0; istok<nstok; istok++) {
	      Buffer[ivoff+istok*incs+2] = -fabs(Buffer[ivoff+istok*incs+2]);
	    }
	  } else {       /* just test data */
	    Buffer[ip1+2] = - fabs(Buffer[ip1+2]);
	    Buffer[ip2+2] = - fabs(Buffer[ip2+2]);
	  }

	} /* End loop over frequency */
      } /* End loop over IF */     
    } /* End loop over visibilities in buffer */
    
    /* Write */
    oretCode = ObitUVWrite (outUV, inUV->buffer, err);
    /* suppress vis number update if rewriting the same file */
    if (same) {
      outUV->myDesc->firstVis = firstVis;
      ((ObitUVDesc*)(inUV->myIO->myDesc))->firstVis = firstVis;
    }
  } /* End loop processing data */
  
  /* check for errors */
  if ((iretCode > OBIT_IO_EOF) || (oretCode > OBIT_IO_EOF) ||
      (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,inUV->name, outUV);
  
  /* unset input buffer (may be multiply deallocated ;'{ ) */
  outUV->buffer = NULL;
  outUV->bufferSize = 0;
  
  /* close files */
  iretCode = ObitUVClose (inUV, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) 
    Obit_traceback_val (err, routine, inUV->name, outUV);
  
  oretCode = ObitUVClose (outUV, err);
  if ((oretCode!=OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, outUV->name, outUV);

  /* Give accounting */
  Obit_log_error(err, OBIT_InfoErr,
		 "Flagged %ld of %ld visibilities with %cPol > %f",
		 cntFlagged, cntAll, stok[0], maxAmp);
  
  return outUV;
} /* end ObitUVEditClipStokes */

/**
 * Routine translated from the AIPSish TDEDIT.FOR/TDCLHI  
 * Determine minimum clipping levels based on histogram of baseline  
 * RMSes.  The clipping level is the median RMS plus 3 times the width  
 * of the RMS distribution or the point which removes 6 percent of the  
 * baselines which ever is greater if normally distributed. 
 * \param ncorr   Number of correlators present 
 * \param numCell Number of cells in each histogram 
 * \param hisinc  Increment of value in histogram 
 * \param hissig  Histogram, index = cell + numCell*corr
 *        On output, the histogram is integrated.
 * \param hiClip  [out] Clipping level in amp**2 units per correlator. 
 */
static void editHist (gint ncorr, gint numCell, gfloat hisinc, gint *hissig, 
		      gfloat* hiClip) 
{
  gint   icorr, count, i, j, ihalf, i6, i16, i56;
  gfloat rhalf, r6, r16, r56, sig, clev;

  if (ncorr == 0) return;
  
  /* Loop over correlator */
  for (icorr= 0; icorr<ncorr; icorr++) { /* loop 400 */
    /* Integrate histogram */
    count = 0;
    for (i=0; i<numCell; i++) { /* loop 50 */
      j = numCell - i - 1;
      count = count + hissig[j+icorr*numCell];
      hissig[j+icorr*numCell] = count;
    } /* end loop  L50:  over cell */
    
    /* Determine levels */
    rhalf = 0.5 * count;
    r6    = 0.06 * count;
    r16   = count * (1.0 / 6.0);
    r56   = count * (5.0 / 6.0);
    ihalf = 1;
    i6    = 1;
    i16   = 1;
    i56   = 2;
    for (i=0; i<numCell; i++) { /* loop 100 */
      if (hissig[i+icorr*numCell] > rhalf) ihalf = i;
      if (hissig[i+icorr*numCell] > r6) i6 = i;
      if (hissig[i+icorr*numCell] > r16) i16 = i;
      if (hissig[i+icorr*numCell] > r56) i56 = i;
    } /* end loop  L100: over cell */

    /* Compute sigma of distribution */
    sig = 0.5 * (i16 - i56) * hisinc;
    
    /* Set clip level */
    clev = MAX ((i6*hisinc), (ihalf*hisinc+3.0*sig));
    
    /* Trap histogram missing */
    if (ihalf >= (numCell-2)) clev = 1000.0;

    /* Return square of flag level */
    hiClip[icorr] = clev * clev;

    /* If the histogram has run off the  high end return a large value */
    if ((i6 >= numCell)  ||  (ihalf >= numCell)) 
      hiClip[icorr] = 10.0;

  } /* end loop  L400: over correlator */
} /* end of routine editHist */ 


/**
 * Determine associated cross polarized correlations, and set maximum 
 * variance for each correlator.  
 * Clip crosspol (maxRMS2) at 2/3 level of parallel.
 * \param inDesc    UV descriptor for data being indexes
 * \param maxRMS    Clipping level, sqrt (maxRMS[0]**2 + (avg_amp * maxRMS[1])**2)
 * \param maxRMS2   [out] maximum variance per correlator,
 *                  in order they occur in the data
 *                  Ignored if NULL.
 * \param crossBL1  [out] if > 0 then index i is for a parallel hand correlation
 *                  with associated cross-hand data correlator crossBL1[i].
 *                  in order they occur in the data
 *                  Ignored if NULL.
 * \param crossBL2  [out] if > 0 then index i is for a parallel hand correlation
 *                  with associated cross-hand data correlator crossBL2[i].
 *                  in order they occur in the data.
 *                  Ignored if NULL.
 * \param corChan   [out] 1-rel channel number per correlator
 * \param corIF     [out] 1-rel IF number per correlator
 * \param corStok   [out] correlator Stokes -1=RR, -2=LL, -3=RL, -4=LR...
 *                        1=I, 2=Q, 2=U, 4=V
 */
static void digestCorr (ObitUVDesc *inDesc, gfloat *maxRMS, gfloat *maxRMS2, 
			gint *crossBL1, gint *crossBL2, 
			gint *corChan, gint *corIF, gint *corStok) 
{
  gint ichan, iif, istok, nchan, nif, nstok, kstoke0;
  gint incs, incf, incif, jstok, ioff, lfoff, soff;

  /* Set up for parsing data */
  nchan = inDesc->inaxes[inDesc->jlocf];
  if (inDesc->jlocf>=0) nif = inDesc->inaxes[inDesc->jlocif];
  else nif = 1;
  if (inDesc->jlocs>=0) nstok = inDesc->inaxes[inDesc->jlocs];
  else nstok = 1;
  /* Get first stokes index */
  if (inDesc->crval[inDesc->jlocs]> 0.0) {
    kstoke0 = inDesc->crval[inDesc->jlocs] + 
      (1.0-inDesc->crpix[inDesc->jlocs]) * inDesc->cdelt[inDesc->jlocs] + 0.5;
  } else {
    kstoke0 = inDesc->crval[inDesc->jlocs] + 
      (1.0-inDesc->crpix[inDesc->jlocs]) * inDesc->cdelt[inDesc->jlocs] - 0.5;
  } 

  /* get increments (one word per correlator) */
  incs  = inDesc->incs  / inDesc->inaxes[0];
  incf  = inDesc->incf  / inDesc->inaxes[0];
  incif = inDesc->incif / inDesc->inaxes[0];

  /* loop over IF */
  for (iif=0; iif<nif; iif++) {
    lfoff = iif * incif;
    ioff  = lfoff;
    
    /* Loop over frequency channel */
    for (ichan=0; ichan<nchan; ichan++) { /* loop 60 */
      soff = ioff;

      /* Loop over polarization */
      for (istok=0; istok<nstok; istok++) {
	corChan[soff]  = ichan+1; 
	corIF[soff]    = iif+1; 

	if (kstoke0>0) { /* Stokes visibility */
	  jstok = kstoke0 + istok;
	  corStok[soff]  = jstok; 
	} else {         /* Correlation */
	  jstok = -kstoke0 + istok;
	  corStok[soff]  = kstoke0 - istok;
	  if ((crossBL1!=NULL) && (crossBL2!=NULL)) {
	    /* Is this a correlation with associated cross poln? */
	    if (corStok[soff]==-1) {         /* RR */
	      crossBL1[soff] = soff + 2*incs; 
	      crossBL2[soff] = soff + 3*incs; 
	    } else if (corStok[soff]==-2) {  /* LL */
	      crossBL1[soff] = soff +   incs; 
	      crossBL2[soff] = soff + 2*incs; 
	    } else { /* no */
	      crossBL1[soff] = -1; 
	      crossBL2[soff] = -1; 
	    }
	  } /* end determine cross polarizations */
	}

	/* Set cross polarized clipping at 2/3 of parallel */
	if (maxRMS2!=NULL) {
	  if ((corStok[soff]==-3) || (corStok[soff]==-4)) {
	    maxRMS2[soff] = maxRMS[0] * maxRMS[0] * 0.44444;
	  } else {
	    maxRMS2[soff] = maxRMS[0] * maxRMS[0];
	  }
	}
	
	soff += incs;
      } /* end loop over stokes */
      ioff  += incf;     
    } /* end loop over channel */
  } /* end loop over IF */
  
} /* end digestCorr */


