/* $Id: ObitTableCCUtil.c,v 1.10 2005/09/19 12:55:17 bcotton Exp $   */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2004,2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "glib/gqsort.h"
#include "ObitMem.h"
#include "ObitTableCCUtil.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitTableCCUtil.c
 * ObitTableCC class utility function definitions.
 */

/*----------------------Private function prototypes----------------------*/
/** Private: Form sort structure for a table */
static gfloat* 
MakeCCSortStruct (ObitTableCC *in, gint *number, gint *size, gint *ncomp,
		  gfloat *parms, ObitErr *err);

/** Private: Sort comparison function for positions */
static gint CCComparePos (gconstpointer in1, gconstpointer in2, 
		     gpointer ncomp);

/** Private: Sort comparison function for Flux density */
static gint CCCompareFlux (gconstpointer in1, gconstpointer in2, 
		     gpointer ncomp);

/** Private: Merge entries in Sort structure */
static void CCMerge (gfloat *base, gint size, gint number); 

/** Private: reorder table based on Sort structure */
static ObitIOCode 
ReWriteTable(ObitTableCC *out, gfloat *base, gint size, gint number, 
	     gfloat *parms, ObitErr *err);
/*----------------------Public functions---------------------------*/

/**
 * Grid components as points onto grid.  The image from which the components is
 * derived is described in desc.  The output grid is padded by a factor OverSample.
 * If the components are Gaussians, their parameters are returned in gaus.
 * \param in         Table to grid
 * \param OverSample Expansion factor for output image
 * \param first      First component (1-rel) to include, 0=>1, filled in if changed
 * \param last       Last component (1-rel) to include, 0=>all, filled in if changed
 * \param factor     factor to multiply timec fluxes
 * \param minFlux    Minimum abs. value flux density to include (before factor)
 * \param desc       Descriptor for image from which components derived
 * \param grid       [out] filled in array, created, resized if necessary
 * \param gparm      [out] Gaussian parameters (major, minor, PA all in deg) if
 *                   the components in in are Gaussians, else, -1.
 * \param ncomp      [out] number of components gridded.
 * \param err        ObitErr error stack.
 * \return I/O Code  OBIT_IO_OK = OK.
 */
ObitIOCode ObitTableCCUtilGrid (ObitTableCC *in, glong OverSample, 
				glong *first, glong *last,
				gfloat factor, gfloat minFlux,
				ObitImageDesc *desc, ObitFArray **grid, 
				gfloat gparm[3], glong *ncomp, 
				ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitTableCCRow *CCRow = NULL;
  gint itestX, itestY;
  gfloat ftestX, ftestY, maxX, minX, maxY, minY;
  gfloat *array, xpoff, ypoff;
  glong j, irow, xPix, yPix, iAddr;
  gfloat iCellX, iCellY, fNx, fNy;
  glong ndim, naxis[2], nx, ny, count = 0, badCnt = 0;
  gchar *routine = "ObitTableCCGrid";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitTableCCIsA(in));

  gparm[0] = gparm[1] = gparm[2] = -1.0; /* init Gaussian */
  
  /* Create/resize output if necessary */
  ndim = 2;
  naxis[0] = OverSample*desc->inaxes[desc->jlocr];
  naxis[1] = OverSample*desc->inaxes[desc->jlocd];
  /* (re)allocate memory for plane */
  if (*grid!=NULL) *grid = ObitFArrayRealloc(*grid, ndim, naxis);
  else *grid = ObitFArrayCreate("ModelImage", ndim, naxis);

  /* Zero fill */
  ObitFArrayFill (*grid, 0.0);

   /* Get pointer to in->plane data array */
  naxis[0] = 0; naxis[1] = 0; 
  array = ObitFArrayIndex(*grid, naxis);
  
 /* Image size as float */
  nx  = OverSample*desc->inaxes[desc->jlocr];
  ny  = OverSample*desc->inaxes[desc->jlocd];
  fNx = (gfloat)nx;
  fNy = (gfloat)ny;
  /* allowed range of X */
  minX = (-fNx/2.0) * fabs(desc->cdelt[desc->jlocr]);
  maxX = ((fNx/2.0) - 1.0) * fabs(desc->cdelt[desc->jlocr]);
  /* allowed range of Y */
  minY = (-fNy/2.0) * fabs(desc->cdelt[desc->jlocd]);
  maxY = ((fNy/2.0) - 1.0) * fabs(desc->cdelt[desc->jlocd]);

  /* Open CC table */
  retCode = ObitTableCCOpen (in, OBIT_IO_ReadWrite, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, in->name, retCode);
  
  /* Create table row */
  if (!CCRow) CCRow = newObitTableCCRow (in);

  /* Field specific stuff */
  /* Inverse Cell spacings */
  if (desc->cdelt[desc->jlocr]!=0.0) iCellX = 1.0/desc->cdelt[desc->jlocr];
  else iCellX = 1.0;
  if (desc->cdelt[desc->jlocd]!=0.0) iCellY = 1.0/desc->cdelt[desc->jlocd];
  else iCellY = 1.0;

  /*    Get reference pixel offsets from (nx/2+1, ny/2+1) */
  xpoff = (desc->crpix[desc->jlocr] - 
	   (desc->inaxes[desc->jlocr]/2) - 1) *
    desc->cdelt[desc->jlocr];
  ypoff = (desc->crpix[desc->jlocd] - 
	   (desc->inaxes[desc->jlocd]/2) - 1) *
    /*DEBUG (desc->inaxes[desc->jlocd]/2)) *  */
    desc->cdelt[desc->jlocd];

  /* loop over CCs */
  count = 0;  /* Count of components */
  if (*first<=0) *first = 1;
  if (*last<=0) *last = in->myDesc->nrow;
  *last = MIN (*last, in->myDesc->nrow);
  for (j=*first; j<=*last; j++) {
    irow = j;
    retCode = ObitTableCCReadRow (in, irow, CCRow, err);
    if ((retCode != OBIT_IO_OK) || (err->error)) 
      Obit_traceback_val (err, routine, in->name, retCode);
    
    /* Get any Gaussian parameters on first, else check */
    if (j==*first) {
      /* Is this a Gaussian component? */
      if ((in->parmsCol>=0) &&
	  (in->myDesc->dim[in->parmsCol][0]>4) && 
	  (CCRow->parms[3]==1.0)) {
	gparm[0] = CCRow->parms[0];
	gparm[1] = CCRow->parms[1];
	gparm[2] = CCRow->parms[2];
      }
    } else if (gparm[0]>0.0) {
      /* All Gaussians MUST be the same */
      if ((CCRow->parms[0]!=gparm[0]) || (CCRow->parms[1]!=gparm[1]) || 
	  (CCRow->parms[2]!=gparm[2])) {
	Obit_log_error(err, OBIT_Error,"%s: All Gaussians MUST have same size",
		       routine);
	return retCode ;
      }
    } /* end of Gaussian Check */
  
    /* Process component */
    CCRow->Flux *= factor;     /* Apply factor */
    CCRow->DeltaX += xpoff;    /* Reference pixel offset from  (nx/2,ny/2)*/
    CCRow->DeltaY += ypoff;

    /* Component wanted? larger than in->minFlux and not zero */
    if ((fabs(CCRow->Flux)<minFlux)  || (CCRow->Flux==0.0)) continue;
    
    /* Check that comps are on cells */
    ftestX = CCRow->DeltaX * iCellX; /* Convert to cells */
    ftestY = CCRow->DeltaY * iCellY;
    if (ftestX>0.0) itestX = (gint)(ftestX + 0.5);
    else itestX = (gint)(ftestX - 0.5);
    if (ftestY>0.0) itestY = (gint)(ftestY + 0.5);
    else itestY = (gint)(ftestY - 0.5);
  
    /* Count bad cells */
    if ((fabs((ftestX-itestX)>0.1)) || (fabs((ftestY-itestY)>0.1))) {
      badCnt++;
      /* Warn but keep going */
      if (badCnt<50) {
	Obit_log_error(err, OBIT_InfoWarn, "%s Warning: Bad cell %f %f", 
		       routine, CCRow->DeltaX, CCRow->DeltaY);
      }
    }

    /* Clip range of X,Y */
    CCRow->DeltaX = MIN (maxX, MAX (CCRow->DeltaX, minX));
    CCRow->DeltaY = MIN (maxY, MAX (CCRow->DeltaY, minY));

    /* X,Y to cells */
    CCRow->DeltaX *= iCellX;
    CCRow->DeltaY *= iCellY;
    /* 0-rel pixel numbers */
    xPix = (glong)(CCRow->DeltaX + nx/2 + 0.5);
    yPix = (glong)(CCRow->DeltaY + ny/2 + 0.5);

    /* Sum into image */
    iAddr = xPix + nx * yPix;
    array[iAddr] += CCRow->Flux;

    count++;        /* how many */
  } /* end loop over components */

  /* How many? */
  *ncomp = count;
  
  /* Close Table */
  retCode = ObitTableCCClose (in, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, in->name, retCode);
  
  /* Release table/row */
  CCRow   = ObitTableCCRowUnref (CCRow);
  
  return retCode;
} /* end ObitTableCCUtilGrid */

/**
 * Return an ObitFArray containing the list of components in the CC table
 * from one image which appear in another.
 * Returned array has component values on the first axis and one row per
 * overlapping component.  (X cell (0-rel), Y cell (0-rel), flux).  
 * CCs on cells within 0.5 pixels of outDesc are included.
 * If the components are Gaussians, their parameters are returned in gaus.
 * \param in         Table of CCs
 * \param inDesc     Descriptor for image from which components derived
 * \param outDesc    Descriptor for output image 
 * \param grid       [out] filled in array, created, resized if necessary
 * \param gparm      [out] Gaussian parameters (major, minor, PA (all deg)) 
 *                   if the components in in are Gaussians, else, -1.
 *                   These are the values from the first CC.
 * \param ncomp      [out] number of components in output list (generally less 
 *                   than size of FArray).
 * \param err        ObitErr error stack.
 * \return pointer to list of components, may be NULL on failure, 
 *  MUST be Unreffed.
 */
ObitFArray* 
ObitTableCCUtilCrossList (ObitTableCC *inCC, ObitImageDesc *inDesc,  
			  ObitImageDesc *outDesc, gfloat gparm[3], 
			  glong *ncomps, ObitErr *err)
{
  ObitFArray *outArray=NULL;
  ObitIOCode retCode;
  ObitTableCCRow *CCRow = NULL;
  glong i, count, irow, lrec;
  glong nrow, ndim, naxis[2];
  gfloat *table, inPixel[2], outPixel[2];
  gboolean wanted;
  gchar *outName;
  ObitCCCompType modType;
  gchar *routine = "ObitTableCCUtilCrossList";
  
  /* error checks */
  if (err->error) return outArray;

  /* Open */
  retCode = ObitTableCCOpen (inCC, OBIT_IO_ReadOnly, err);
  /* If this fails try ReadWrite */
  if (err->error) { 
    ObitErrClearErr(err);  /* delete failure messages */
    retCode = ObitTableCCOpen (inCC, OBIT_IO_ReadWrite, err);
  }
  if ((retCode != OBIT_IO_OK) || (err->error))
      Obit_traceback_val (err, routine, inCC->name, outArray);

  /* Create FArray list large enough for all CCs in inCC */
  nrow = inCC->myDesc->nrow;
  ndim = 2; naxis[0] = 3; naxis[1] = nrow;
  lrec = naxis[0];   /* size of table row */
  outName =  g_strconcat ("CC List:", inCC->name, NULL);
  outArray = ObitFArrayCreate (outName, ndim, naxis);
  g_free (outName);  /* deallocate name */
  /* Get pointer to array */
  naxis[0] = naxis[1] = 0;
  table = ObitFArrayIndex (outArray, naxis);

  /* Create table row */
  CCRow = newObitTableCCRow (inCC);

  /* Initialize */
  gparm[0] = gparm[1] = gparm[2] = -1.0;  /* No Gaussian yet */
  count = 0;         /* How many CCs accepted */
  /* If only 4 col, or parmsCol 0 size then this is a point model */
  if ((inCC->myDesc->nfield==4) || 
      (inCC->myDesc->dim[inCC->parmsCol]<=0)) 
    modType = OBIT_CC_PointMod;
  else  
    modType = OBIT_CC_Unknown; /* Model type not yet known */
  
  /* Loop over table reading CCs */
  for (i=1; i<=nrow; i++) {

    irow = i;
    retCode = ObitTableCCReadRow (inCC, irow, CCRow, err);
    if ((retCode != OBIT_IO_OK) || (err->error)) 
      Obit_traceback_val (err, routine, inCC->name, outArray);

    /* Get model type  */
    if (modType == OBIT_CC_Unknown) {
      modType = (gint)(CCRow->parms[3] + 0.5);
      /* If Gaussian take model */
      if ((modType==OBIT_CC_GaussMod) || (modType==OBIT_CC_CGaussMod)) {
	gparm[0] = CCRow->parms[0];
	gparm[1] = CCRow->parms[1];
	gparm[2] = CCRow->parms[2];
      }
      /* If neither a point nor Gaussian - barf */
      if ((modType!=OBIT_CC_GaussMod) && (modType!=OBIT_CC_CGaussMod) && 
	  (modType!=OBIT_CC_PointMod)) {
	Obit_log_error(err, OBIT_Error,
		       "%s: Model type %d neither point nor Gaussian in %s",
		       routine, modType, inCC->name);
	return outArray;
      }
    } /* end model type checking */

    /* Is this one within 3 pixels of outDesc? */
    inPixel[0] = CCRow->DeltaX / inDesc->cdelt[0] + inDesc->crpix[0];
    inPixel[1] = CCRow->DeltaY / inDesc->cdelt[1] + inDesc->crpix[1];
    wanted = ObitImageDescCvtPixel (inDesc, outDesc, inPixel, outPixel, err);
    if (err->error) Obit_traceback_val (err, routine, inCC->name, outArray);

    if (wanted) { /* yes */
      table[0] = outPixel[0] - 1.0;  /* Make zero rel. pixels */
      table[1] = outPixel[1] - 1.0;
      table[2] = CCRow->Flux;
      table += lrec;  /* update table pointer */
      count++; /* How many? */
    }
  } /* end loop over TableCC */

  /* Close */
  retCode = ObitTableCCClose (inCC, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, inCC->name, outArray);
  
  /* Release table row */
  CCRow = ObitTableCCRowUnref (CCRow);
    
  /* Zero any extra entries in table. */
  for (i=count; i<nrow; i++) {
    /* Zero entry */
    table[0] = 0.0;
    table[1] = 0.0;
    table[2] = 0.0;
    table += lrec;  /* Update pointer */
  } /* end loop zeroing extra components */

  /* How many? */
  *ncomps = count;

  return outArray;
} /*  end ObitTableCCUtilCrossList */

/**
 * Merge elements of an ObitTableCC on the same position.
 * First sorts table, collapses, sorts to desc. flux
 * \param in      Table to sort
 * \param out     Table to write output to
 * \param err     ObitErr error stack.
 * \return I/O Code  OBIT_IO_OK = OK.
 */
ObitIOCode ObitTableCCUtilMerge (ObitTableCC *in, ObitTableCC *out, 
				 ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gint size, fsize,  number, ncomp;
  gfloat parms[20];
  gfloat *SortStruct = NULL;
  gchar *routine = "ObitTableCCUtilMerge";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitTableCCIsA(in));

  /* Open table */
  retCode = ObitTableCCOpen (in, OBIT_IO_ReadOnly, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, in->name, retCode);
  
  /* Must be something in the table else just return */
  if (in->myDesc->nrow<=0) {
    retCode = ObitTableCCClose (in, err);
    if ((retCode != OBIT_IO_OK) || (err->error))
      Obit_traceback_val (err, routine, in->name, retCode);
    return OBIT_IO_OK;
  }

  /* build sort structure from table */
  SortStruct = MakeCCSortStruct (in, &size, &number, &ncomp, parms, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, retCode);

  /* Close table */
  retCode = ObitTableCCClose (in, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, in->name, retCode);

  /* Sort */
  g_qsort_with_data (SortStruct, number, size, CCComparePos, &ncomp);

  /* Merge entries */
  fsize = size/sizeof(gfloat);
  CCMerge (SortStruct, fsize, number);
  
  /* Sort to descending merged flux densities */
  ncomp = 1;
  g_qsort_with_data (SortStruct, number, size, CCCompareFlux, &ncomp);

  /* Write output table */
  retCode = ReWriteTable (out, SortStruct, fsize, number, parms, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, in->name, retCode);
  
  /* Cleanup */
  if (SortStruct) ObitMemFree(SortStruct);

  return OBIT_IO_OK;
} /* end ObitTableCCUtilMerge */


/*----------------------Private functions---------------------------*/
/**
 * Create/fill sort structure for a CC table
 * The sort structure has one "entry" per row which contains 
 * \li Delta X
 * \li Delta Y
 * \li Delta Flux
 *
 * Each valid row in the table has an entry.
 * \param in     Table to sort, assumed already open;
 * \param size   [out] Number of bytes in entry
 * \param number [out] Number of entries
 * \param ncomp  [out] Number of values to compare
 * \param parms  [out] Parms of first element if they exist
 * \param err     ObitErr error stack.
 * \return sort structure, should be ObitMemFreeed when done.
 */
static gfloat* 
MakeCCSortStruct (ObitTableCC *in, gint *size, gint *number, gint *ncomp,
		  gfloat *parms, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gfloat *out = NULL;
  ObitTableCCRow *row;
  gfloat *entry;
  glong irow, nrow, tsize, count, i;
  gint fsize;
  gchar *routine = "MakeCCSortStruct";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return out;
  g_assert (ObitTableCCIsA(in));

  /* Get table info */
  nrow = in->myDesc->nrow;

  /* element size */
  fsize = 3;
  *size = fsize * sizeof(gfloat);

  /* Total size of structure in case all rows valid */
  tsize = (*size) * (nrow);
  /* create output structure */
  out = ObitMemAlloc0Name (tsize, "CCSortStructure");
  
  /* Compare 2  (X, Y pos) */
  *ncomp = 2;

  /* Create table row */
  row = newObitTableCCRow (in);

 /* loop over table */
  irow = 0;
  count = 0;
  retCode = OBIT_IO_OK;
  while (retCode==OBIT_IO_OK) {
    irow++;
    retCode = ObitTableCCReadRow (in, irow, row, err);
    if (retCode == OBIT_IO_EOF) break;
    if ((retCode != OBIT_IO_OK) || (err->error)) 
      Obit_traceback_val (err, routine, in->name, out);
    if (row->status<0) continue;  /* Skip deselected record */

    /* add to structure */
    entry = (gfloat*)(out + count * fsize);  /* set pointer to entry */
    entry[0] = row->DeltaX;
    entry[1] = row->DeltaY;
    entry[2] = row->Flux;

    /* Save parms if any for first record */
    if ((count<=0) && (in->noParms>0)) {
      for (i=0; i<in->noParms; i++) parms[i] = row->parms[i];
    }
    
    count++;  /* How many valid */
  } /* end loop over file */
  
  /* check for errors */
  if ((retCode > OBIT_IO_EOF) || (err->error))
    Obit_traceback_val (err, routine, in->name, out);
  
  /* Release table row */
  row = ObitTableCCRowUnref (row);
  
  /* Actual number */
  *number = count;

  return out;
} /* end MakeSortStruc */ 

/**
 * Compare two lists of floats
 * Conformant to function type GCompareDataFunc
 * \param in1   First list
 * \param in2   Second list
 * \param ncomp Number of values to compare (2)
 * \return <0 -> in1 < in2; =0 -> in1 == in2; >0 -> in1 > in2; 
 */
static gint CCComparePos (gconstpointer in1, gconstpointer in2, 
			  gpointer ncomp)
{
  gint out = 0;
  gfloat *float1, *float2;

  /* get correctly typed local values */
  float1 = (float*)(in1);
  float2 = (float*)(in2);

  if (float1[0]<float2[0])      out = -1;
  else if (float1[0]>float2[0]) out = 1;
  else                          out = 0;
  if (!out) { /* compare second needed? */
    if (float1[1]<float2[1])      out = -1;
    else if (float1[1]>float2[1]) out = 1;
    else                          out = 0;
  }

  return out;
} /* end CCComparePos */


/**
 * Compare fluxes, to give descending abs order.
 * Conformant to function type GCompareDataFunc
 * \param in1   First list
 * \param in2   Second list
 * \param ncomp Number of values to compare (1)
 * \return <0 -> in1 < in2; =0 -> in1 == in2; >0 -> in1 > in2; 
 */
static gint CCCompareFlux (gconstpointer in1, gconstpointer in2, 
			   gpointer ncomp)
{
  gint out = 0;
  gfloat *float1, *float2;

  /* get correctly typed local values */
  float1 = (float*)(in1 + 2*sizeof(gfloat));
  float2 = (float*)(in2 + 2*sizeof(gfloat));
  if (fabs(*float1)<fabs(*float2))      out =  1;
  else if (fabs(*float1)>fabs(*float2)) out = -1;
  else                          out =  0;
  return out;
} /* end CCCompareFlux */

/**
 * Merge entries in sort structure
 * leaves "X" entry in defunct rows -1.0e20
 * table and then copies over the input table.
 * \param base    Base address of sort structure
 * \param size    Size in gfloats of a sort element
 * \param number  Number of sort elements
 */
static void CCMerge (gfloat *base, gint size, gint number)
{
  gint i, j;
  gfloat *array = base;
  
  i = 0;
  while (i<number) {
    j=i+1;
    while (j<number) {
      if ((array[j*size]!=array[i*size]) || (array[j*size+1]!=array[i*size+1]))
	break;
      array[i*size+2] += array[j*size+2];  /* sum fluxes */
      array[j*size] = -1.0e20;             /* Don't need any more */
      j++;
    } /* end finding matches */
    i = j;   /* move on */
  } /* end loop over table */

} /* end CCMerge */

/**
 * Write valid entries in sort structure
 * \param out     Table write
 * \param base    Base address of sort structure
 * \param size    Size in floats of a sort element
 * \param number  Number of sort elements
 * \param parms   Parms of components
 * \param err     ObitErr error stack.
 * \return I/O Code  OBIT_IO_OK = OK.
 */
static ObitIOCode 
ReWriteTable(ObitTableCC *out, gfloat *base, gint size, gint number, 
	     gfloat *parms, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitTableCCRow *row;
  gfloat *entry;
  glong irow, i, count;
  gchar *routine = "ReWriteTable";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitTableCCIsA(out));

  /* Open table */
  retCode = ObitTableCCOpen (out, OBIT_IO_WriteOnly, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, out->name, retCode);

  /* Mark as sorted by descending flux */
  out->myDesc->sort[0] = -(out->FluxCol+257);
  out->myDesc->sort[1] = 0;

  /* Fooey!  This one counts */
 ((ObitTableDesc*)out->myIO->myDesc)->sort[0] = -(out->FluxCol+257);
  ((ObitTableDesc*)out->myIO->myDesc)->sort[1] = 0;
  

  /* Create row structure */
  row = newObitTableCCRow (out);

  /* loop over table */
  retCode = OBIT_IO_OK;
  entry = (gfloat*)base;
  irow = 0;
  count = 0;
  while (count<number) {

    /* Deleted? */
    if (entry[0]>-1.0e19) {

      /* copy to row */
      row->DeltaX = entry[0];
      row->DeltaY = entry[1];
      row->Flux   = entry[2];
      
      /* copy any parms */
      if (out->noParms>0) {
	for (i=0; i<out->noParms; i++) row->parms[i] = parms[i];
      }
      
      /* Write */
      irow++;
      retCode = ObitTableCCWriteRow (out, irow, row, err);
      if ((retCode != OBIT_IO_OK) || (err->error)) 
	Obit_traceback_val (err, routine, out->name, retCode);
    }
    count++;
    entry += size;  /* pointer in table */
  } /* end loop over file */
  
  /* Close table */
  retCode = ObitTableCCClose (out, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, out->name, retCode);

  /* Release table row */
  row = ObitTableCCRowUnref (row);

  /* Tell what you've done */
  Obit_log_error(err, OBIT_InfoErr,
		 "Merged %d CC components into %ld for %s",
		 number, irow, out->name);

  return retCode;
} /* end ReWriteTable */

