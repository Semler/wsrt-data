/* $Id: ObitUVDesc.c,v 1.9 2005/06/17 12:25:04 bcotton Exp $      */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
#include "Obit.h"
#include "ObitUVDesc.h"
#include "ObitTableFQ.h"
#include "ObitTableFQUtil.h"
#include "ObitSkyGeom.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitUVDesc.c
 * ObitUVDesc Obit UV data descriptor class definition.
 * This contains information about the observations and the coordinates
 * in the image.
 */

/*--------------- File Global Variables  ----------------*/
/** name of the class defined in this file */
static gchar *myClassName = "ObitUVDesc";

/**
 * ClassInfo global structure ObitIOClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitUVDescClassInfo myClassInfo = {FALSE};

/** truncate double to integer precision */
#ifndef AINT  
#define AINT(x) (gdouble)((glong)(x))
#endif

/** Degrees to radians factor */
#ifndef DG2RAD  
#define DG2RAD G_PI / 180.0
#endif

/**  Radians to degrees factor */
#ifndef RAD2DG  
#define RAD2DG 180.0 / G_PI
#endif
/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitUVDescInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitUVDescClear (gpointer in);


/*---------------Public functions---------------------------*/
/**
 * Construct Object.
 * \return pointer to object created.
 */
ObitUVDesc* newObitUVDesc (gchar *name)
{
  ObitUVDesc *out;

   /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitUVDescClassInit();

  /* allocate structure */
  out = g_malloc0(sizeof(ObitUVDesc));

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

 /* set classInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  ObitUVDescInit((gpointer)out);

 return out;
} /* end newObitUVDesc */

/**
 * Returns ClassInfo pointer for the class.
 * Initializes class if needed on first call.
 * \return pointer to the class structure.
 */
gconstpointer ObitUVDescGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitUVDescClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitUVDescGetClass */

/**
 * Copy constructor.
 * The output descriptor will have the size and reference pixel
 * modified to reflect subimaging on the input, i.e. the output
 * descriptor describes the input.
 * \param in Pointer to object to be copied.
 * \param out Pointer to object to be written.  
 *            If NULL then a new structure is created.
 * \param err ObitErr error stack
 * \return Pointer to new object.
 */
ObitUVDesc* ObitUVDescCopy (ObitUVDesc* in, ObitUVDesc* out, ObitErr *err)
{
  const ObitClassInfo *ParentClass;
  gboolean oldExist;
  gchar *outName;
  gint i,j, size, nif, nfreq;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return out;
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));
 
  /* Don't bother it they are the same */
  if (in==out) return out;

  /* Create if it doesn't exist */
  oldExist = out!=NULL;
  if (!oldExist) {
    /* derive object name */
    outName = g_strconcat ("Copy: ",in->name,NULL);
    out = newObitUVDesc(outName);
    g_free(outName);
  }

  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);

  /* initialize/copy */
  out->access  = in->access;
  out->nvis    = in->nvis;
  out->naxis   = in->naxis;
  out->nrparm  = in->nrparm;
  out->epoch   = in->epoch;
  out->equinox = in->equinox;
  out->JDObs   = in->JDObs;
  out->obsra   = in->obsra;
  out->obsdec  = in->obsdec;
  out->firstVis= in->firstVis;
  out->numVisBuff = in->numVisBuff;
  out->altCrpix = in->altCrpix;
  out->altRef   = in->altRef;
  out->restFreq = in->restFreq;
  out->VelReference = in->VelReference;
  out->VelDef   = in->VelDef;
  out->xshift   = in->xshift;
  out->yshift   = in->yshift;
  for (i=0; i<UVLEN_VALUE; i++) out->object[i] = in->object[i];
  for (i=0; i<UVLEN_VALUE; i++) out->teles[i]  = in->teles[i];
  for (i=0; i<UVLEN_VALUE; i++) out->instrument[i] = in->instrument[i];
  for (i=0; i<UVLEN_VALUE; i++) out->observer[i]   = in->observer[i];
  for (i=0; i<UVLEN_VALUE; i++) out->obsdat[i] = in->obsdat[i];
  for (i=0; i<UVLEN_VALUE; i++) out->origin[i] = in->origin[i];
  for (i=0; i<UVLEN_VALUE; i++) out->date[i]   = in->date[i];
  for (i=0; i<UVLEN_VALUE; i++) out->bunit[i]  = in->bunit[i];
  for (i=0; i<3; i++)           out->isort[i]  = in->isort[i];

  /* loop over axes */
  for (j=0; j<UV_MAXDIM; j++) {
    out->inaxes[j] = in->inaxes[j];
    out->cdelt[j]  = in->cdelt[j];
    out->crota[j]  = in->crota[j];
    out->crpix[j]  = in->crpix[j];
    out->crval[j]  = in->crval[j];
    for (i=0; i<UVLEN_KEYWORD; i++) out->ctype[j][i] = in->ctype[j][i];
  }

  /* Random parameter labels */
  for (j=0; j<UV_MAX_RANP; j++) {
   for (i=0; i<UVLEN_KEYWORD; i++) out->ptype[j][i] = in->ptype[j][i];
  }

  /* Free any existing info members */
  if (in->info!=NULL) {
    if (out->info) out->info = ObitInfoListUnref (out->info); 
    out->info = ObitInfoListCopy (in->info);
  }

  /* Frequency related arrays - how big? */
  nfreq = 1;
  if (in->jlocf>=0) nfreq = MAX (1, in->inaxes[in->jlocf]);
  nif = 1;
  if (in->jlocif>=0) nif = MAX (1, in->inaxes[in->jlocif]);
  size = nfreq*nif;
 
  /* Frequencies */
  if (in->freqArr) {
    out->freqArr = g_realloc(out->freqArr, size*sizeof(gdouble));
    for (i=0; i<size; i++) out->freqArr[i] = in->freqArr[i];
  }
  
  /* Frequency Scaling */
  if (in->fscale) {
    out->fscale = g_realloc(out->fscale, size*sizeof(gfloat));
    for (i=0; i<size; i++) out->fscale[i] = in->fscale[i];
  }
  
  /* IF frequency */
  if (in->freqIF) {
    out->freqIF = g_realloc(out->freqIF, nif*sizeof(gdouble));
    for (i=0; i<nif; i++) out->freqIF[i] = in->freqIF[i];
  }
  
  /* IF channel bandwidths */
  if (in->chIncIF) {
    out->chIncIF = g_realloc(out->chIncIF, nif*sizeof(gfloat));
    for (i=0; i<nif; i++) out->chIncIF[i] = in->chIncIF[i];
  }
  
  /* IF sidebands */
  if (in->sideband) {
    out->sideband = g_realloc(out->sideband, nif*sizeof(gint));
    for (i=0; i<nif; i++) out->sideband[i] = in->sideband[i];
  }
  
  /* index output */
  ObitUVDescIndex (out);
  
  return out;
} /* end ObitUVDescCopy */

/**
 * Copy descriptive material (i.e. things that don't define the structure).
 * \param in  Pointer to object to be copied.
 * \param out Pointer to object to be written.  
 * \param err ObitErr error stack
 */
void ObitUVDescCopyDesc (ObitUVDesc* in, ObitUVDesc* out, 
			    ObitErr *err)
{
  gint i,j;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitIsA(out, &myClassInfo));
 
  /* initialize/copy */
  out->epoch   = in->epoch;
  out->equinox = in->equinox;
  out->JDObs   = in->JDObs;
  out->obsra   = in->obsra;
  out->obsdec  = in->obsdec;
  out->altCrpix = in->altCrpix;
  out->altRef   = in->altRef;
  out->restFreq = in->restFreq;
  out->VelReference = in->VelReference;
  out->VelDef   = in->VelDef;
  out->xshift   = in->xshift;
  out->yshift   = in->yshift;
  for (i=0; i<UVLEN_VALUE; i++) out->object[i] = in->object[i];
  for (i=0; i<UVLEN_VALUE; i++) out->teles[i]  = in->teles[i];
  for (i=0; i<UVLEN_VALUE; i++) out->instrument[i] = in->instrument[i];
  for (i=0; i<UVLEN_VALUE; i++) out->observer[i]   = in->observer[i];
  for (i=0; i<UVLEN_VALUE; i++) out->obsdat[i] = in->obsdat[i];
  for (i=0; i<UVLEN_VALUE; i++) out->origin[i] = in->origin[i];
  for (i=0; i<UVLEN_VALUE; i++) out->date[i]   = in->date[i];
  for (i=0; i<UVLEN_VALUE; i++) out->bunit[i]  = in->bunit[i];
  for (i=0; i<3; i++)           out->isort[i]  = in->isort[i];

  /* loop over axes */
  for (j=0; j<UV_MAXDIM; j++) {
    out->cdelt[j]  = in->cdelt[j];
    out->crota[j]  = in->crota[j];
    out->crpix[j]  = in->crpix[j];
    out->crval[j]  = in->crval[j];
    for (i=0; i<UVLEN_KEYWORD; i++) out->ctype[j][i] = in->ctype[j][i];
  }

  /* Random parameter labels */
  for (j=0; j<UV_MAX_RANP; j++) {
   for (i=0; i<UVLEN_KEYWORD; i++) out->ptype[j][i] = in->ptype[j][i];
  }

  if (in->chIncIF)  g_free(in->chIncIF);  in->chIncIF = NULL;
  /* index output */
  ObitUVDescIndex (out);

  return;
} /* end ObitUVDescCopyDesc */

/**
 * Set axis, random parameter order indicators and data increments.
 * \param in Pointer to object.
 */
void ObitUVDescIndex (ObitUVDesc* in)
{
  glong i, size;

  /* error check */
  g_assert (ObitIsA(in, &myClassInfo));

  /* regular axis values */
  /* initialize */
  in->jlocc  = -1;
  in->jlocs  = -1;
  in->jlocf  = -1;
  in->jlocif = -1;
  in->jlocr  = -1;
  in->jlocd  = -1;

  /* loop over axes looking for labels */
  size = 1;
  for (i=0; i<in->naxis; i++) {
    size *= MAX (1, in->inaxes[i]); /* how big */
    if (!strncmp (in->ctype[i], "COMPLEX", 7)) in->jlocc  = i;
    if (!strncmp (in->ctype[i], "STOKES",  6)) in->jlocs  = i;
    if (!strncmp (in->ctype[i], "FREQ",    4)) in->jlocf  = i;
    if (!strncmp (in->ctype[i], "IF",      2)) in->jlocif = i;
    if (!strncmp (in->ctype[i], "RA",      2)) in->jlocr = i;
    if (!strncmp (in->ctype[i], "GLON",    4)) in->jlocr = i;
    if (!strncmp (in->ctype[i], "ELON",    4)) in->jlocr = i;
    if (!strncmp (in->ctype[i], "DEC",     3)) in->jlocd = i;
    if (!strncmp (in->ctype[i], "GLAT",    4)) in->jlocd = i;
    if (!strncmp (in->ctype[i], "ELAT",    4)) in->jlocd = i;
  }
    /* Set increments in visibility array (in units of correlations) */
    in->incs  = 1;
    in->incf  = 1;
    in->incif = 1;
    if (in->jlocs>0) { /* Product of dimensions of previous axes */
      for (i=0; i<in->jlocs; i++) in->incs *= in->inaxes[i];
    }
    if (in->jlocf>0) {
      for (i=0; i<in->jlocf; i++) in->incf *= in->inaxes[i];
    }
    if (in->jlocif>0) {
      for (i=0; i<in->jlocif; i++) in->incif *= in->inaxes[i];
    }

  /* random parameter values */
  /* initialize */
  in->ilocws = -1;
  in->ilocu  = -1;
  in->ilocv  = -1;
  in->ilocw  = -1;
  in->iloct  = -1;
  in->ilocb  = -1;
  in->ilocsu = -1;
  in->ilocfq = -1;
  in->ilocit = -1;
  in->ilocid = -1;

  /* loop over parameters looking for labels */
  for (i=0; i<in->nrparm; i++) {
    /* ignore projection codes */
    if (!strncmp (in->ptype[i], "UU-",      3)) in->ilocu  = i;
    if (!strncmp (in->ptype[i], "VV-",      3)) in->ilocv  = i;
    if (!strncmp (in->ptype[i], "WW-",      3)) in->ilocw  = i;
    if (!strncmp (in->ptype[i], "BASELINE", 8)) in->ilocb  = i;
    if (!strncmp (in->ptype[i], "TIME1",    5)) in->iloct  = i;
    if (!strncmp (in->ptype[i], "TIME",     4)) in->iloct  = i;
    if (!strncmp (in->ptype[i], "DATE",     4)) in->iloct  = i;
    if (!strncmp (in->ptype[i], "SOURCE",   6)) in->ilocsu = i;
    if (!strncmp (in->ptype[i], "FREQSEL",  7)) in->ilocfq = i;
    if (!strncmp (in->ptype[i], "INTTIM",   6)) in->ilocit = i;
    if (!strncmp (in->ptype[i], "CORR-ID",  7)) in->ilocid = i;
    if (!strncmp (in->ptype[i], "WEIGHT",   6)) in->ilocws = i;
  }

  /* number of correlations */
  in->ncorr = size / MAX (1, in->inaxes[in->jlocc]);

  /* trap for FITS compressed uv data for which the descriptors
     are for the data as pairs of shorts */
  if (in->inaxes[in->jlocc]==2) size /= (sizeof(gfloat)/sizeof(gshort));

  /* total size */
  in->lrec = in->nrparm + size;

  /* Set frequency information */
  if (in->jlocf>=0) in->freq = in->crval[in->jlocf];
  else in->freq = 1.0;

  /* Set Julian date if not there */
  if (in->JDObs<1.0) ObitUVDescDate2JD (in->obsdat, &in->JDObs);
} /* end  ObitUVDescIndex */

/**
 * Fills in frequency information in Descriptor from header and FQ table.
 * These are the freqArr, fscale, freqIF sideband, and chIncIF array members.
 * \param in      Descriptor to update.
 * \param fqtab   FQ table with IF frequency information
 *                Actually an ObitTable but passed as parent class to avoid 
 *                circular definitions inf include files.
 * \param err     ObitErr for reporting errors.
 */
void ObitUVDescGetFreq (ObitUVDesc* in, Obit *fqtab, 
			ObitErr *err)
{
  glong size, i, j, fqid, nfreq;
  oint  *sideBand, nif, tnif;
  double *freqOff, *freq;
  gfloat *chBandw;
  ObitIOCode retCode;
  ObitTableFQ *FQtable = (ObitTableFQ*)fqtab;
  gchar *routine = "ObitUVDescGetFreq";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitTableFQIsA(fqtab));

  /* release old memory if necessary */
  if (in->freqArr)  g_free(in->freqArr);  in->freqArr = NULL;
  if (in->fscale)   g_free(in->fscale);   in->fscale = NULL;
  if (in->freqIF)   g_free(in->freqIF);   in->freqIF = NULL;
  if (in->chIncIF)  g_free(in->chIncIF);  in->chIncIF = NULL;
  if (in->sideband) g_free(in->sideband); in->sideband = NULL;

  /* how big */
  nfreq = 1;
  if (in->jlocf>=0) nfreq = MAX (1, in->inaxes[in->jlocf]);
  nif = 1;
  if (in->jlocif>=0) nif = MAX (1, in->inaxes[in->jlocif]);
  size = nfreq*nif;
  
  /* allocate */
  in->freqArr = g_malloc0(size*sizeof(gdouble));
  in->fscale  = g_malloc0(size*sizeof(gfloat));
  in->freqIF  = g_malloc0(nif*sizeof(gdouble));
  in->chIncIF = g_malloc0(nif*sizeof(gfloat));
  in->sideband= g_malloc0(nif*sizeof(gint));

  /* Get information from FQ table */
  fqid = 1; /* may need more */
  retCode = ObitTableFQGetInfo (FQtable, fqid, &tnif, &freqOff, &sideBand, 
				&chBandw, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Make sure reference frequency set */
  in->freq = in->crval[in->jlocf];

  /* put frequencies in list in order they occur in data */
  freq = in->freqArr;
  if ((in->jlocf < in->jlocif) || (in->jlocif<=0)) {
    /* Frequency first */
    for (j=0; j<nif; j++) {
      for (i=0; i<nfreq; i++) {
	*freq = freqOff[j] + in->freq + 
	  (i+1.0 - in->crpix[in->jlocf]) * in->cdelt[in->jlocf];
	freq++;
      }
    }
  } else {
    /* IF first */
    for (i=0; i<nfreq; i++) {
      for (j=0; j<nif; j++) {
	*freq = freqOff[j] + in->freq + 
	  (i+1.0 - in->crpix[in->jlocf]) * in->cdelt[in->jlocf];
	freq++;
      }
    }
  }

  /* Frequency scaling factors to reference frequency */
  for (i=0; i<size; i++) {
    in->fscale[i] = in->freqArr[i] / in->freq;
  }

 /* Save IF frequency info */
  for (j=0; j<nif; j++) {
    in->freqIF[j]  = in->freq + freqOff[j];
    in->chIncIF[j] = chBandw[j];
    in->sideband[j]= sideBand[j];
  }

  /* cleanup */
  if (freqOff)  g_free(freqOff);
  if (sideBand) g_free(sideBand);
  if (chBandw)  g_free(chBandw);

 } /* end ObitUVDescGetFreq */

/**
 * Parse date string as ("yyyy-mm-dd" or "dd/mm/yy" or "yyymmdd")
 * and convert to Julian Date.
 * Algorithm from ACM Algorithm number 199
 * This routine is good from 1 Mar 1900 indefinitely.
 * \param date [in] Date string
 * \param date [out] Julian date.
 */
void ObitUVDescDate2JD (const gchar* date, gdouble *JD)
{
  gint ymd[3], n;
  gdouble ya, m, d, k, c;
  gchar temp[20];
  
  g_assert (date!=NULL);
  g_assert (JD!=NULL);

  /* Get date */
  if (date[2]=='/') { /* old format 'DD/MM/YY' */
    strncpy (temp, date, 8); temp[8] = 0;
    temp[2] = ' '; temp[5] = ' ';
    n = sscanf (temp, "%2d%3d%3d", &ymd[2], &ymd[1], &ymd[0]);
    /* guess century */
    if (ymd[0]>50) ymd[0]+=1900;
    else ymd[0]+=2000;
  } else if (date[4]=='-') {            /* new format 'YYYY-MM-DD' */
     strncpy (temp, date, 10); temp[10] = 0;
     temp[4] = ' '; temp[7] = ' ';
     n = sscanf (temp, "%4d%3d%3d", &ymd[0], &ymd[1], &ymd[2]);
  } else {  /* assume YYYYMMDD */
    n = sscanf (date, "%4d%2d%2d", &ymd[0], &ymd[1], &ymd[2]);
  }
  /* if something went wrong return bad date */
  if (n!=3) {*JD = -1.0; return;}
  
  /* Convert to Days */
  ya = ymd[0];
  m  = ymd[1];
  d  = ymd[2];
  if (m<=2.0) {
    m  = AINT(m+9.0);
    ya = AINT(ya-1.0);
  } else {
    m  = AINT(m-3.0);
  }
  c = AINT(ya/100.0);
  ya = ya - 100.0 * c;
  k = AINT(146097.0 * c * 0.25) +
    AINT(1461.0 * ya * 0.25) +
    AINT((153.0 * m + 2.0) * 0.2) +
    d;

  /* Following good for > 20th cent. */
  *JD = AINT(k) + 1721118.50;
} /* end ObitUVDescDate2JD */

/**
 * Convert a Julian date to a string in form "yyyy-mm-dd".
 * Apdapted from ASM Algorithm no. 199
 * \param date [in] Julian date.
 * \param date [out] Date string, Must be at least 11 characters.
 */
void ObitUVDescJD2Date (gdouble JD, gchar *date)
{
  gint  id, im, iy, ic;
  gdouble j, y, d, m;

  g_assert (date!=NULL);

  /* error check */
  if (JD<1.0) {
    g_snprintf (date, 11, "BAD DATE");
    return;
  }
  
  j = AINT (JD + 0.50 - 1721119.0);
  y = AINT ((4.0*j - 1.00) / 146097.0);
  ic = y + 0.00010;
  j = 4.0*j - 1.00 - 146097.0*y;
  d = AINT (j * 0.250);
  j = AINT ((4.00*d + 3.00) / 1461.00);
  d = 4.00*d + 3.00 - 1461.00*j;
  d = AINT ((d+4.00) * 0.250);
  m = AINT ((5.00*d - 3.00) / 153.00);
  id = 5.00*d - 3.00 - 153.00*m;
  id = (id + 5) / 5;
  iy = j + 100*ic;
  im = m;
  if (im < 10) {
    im = im + 3;
  } else {
    im = im - 9;
  iy = iy + 1;
  }

  /* convert to string */
  g_snprintf (date, 11, "%4.4d-%2.2d-%2.2d", iy, im, id);
} /* end ObitUVDescJD2Date */

/**
 * Determine the phase shift parameters for a position shift
 * from uv data to an image.
 * Recognizes "-SIN" and "-NCP" projections.
 * Assumes 3D imaging for -SIN and not for -NCP.
 * Apdapted from AIPS.
 * \param uvDesc UV data descriptor
 * \param imDesc Image descriptor.
 * \param dxyzc  (out) the derived shift parameters.
 * \param err    ObitErr for reporting errors.
 */
void ObitUVDescShiftPhase (ObitUVDesc* uvDesc, 
			   ObitImageDesc* imDesc, 
			   gfloat dxyzc[3], ObitErr *err)
{
  gfloat maprot;
  gdouble  uvra, uvdec, imra, imdec;
  gchar *routine = "ObitUVDescShiftPhase";

  /* get positions - assume 3D imaging */
  uvra  = uvDesc->crval[uvDesc->jlocr];
  uvdec = uvDesc->crval[uvDesc->jlocd];
  imra  = imDesc->crval[imDesc->jlocr];
  imdec = imDesc->crval[imDesc->jlocd];
  maprot = ObitImageDescRotate(imDesc);
  
  /* which projection type? Use projection code on first image axis to decide. */
  if (!strncmp(&imDesc->ctype[0][4], "-NCP", 4)) {
    
    /* correct positions for shift since not 3D imaging */
    uvra  += uvDesc->xshift;
    uvdec += uvDesc->yshift;
    imra  += imDesc->xshift;
    imdec += imDesc->yshift;

    /*  -NCP projection */
    ObitSkyGeomShiftNCP (uvra, uvdec, maprot, imra, imdec, dxyzc);
    
  } else if (!strncmp(&imDesc->ctype[0][4], "-SIN", 4)) {
    
    /*  -SIN projection */
    ObitSkyGeomShiftSIN (uvra, uvdec, maprot, imra, imdec, dxyzc);
  } /* end "-SIN" projection */

  else {
      Obit_log_error(err, OBIT_Error, 
		     "%s: Unsupported projection %s",
		     routine,&imDesc->ctype[0][4]);
      return;
  }
  
} /* end ObitUVDescShiftPhase */

/**
 * Return uvdata rotation angle on sky.
 * \param uvDesc UV data descriptor
 * \return rotation angle on sky (of u,v,w) in deg.
 */
gfloat ObitUVDescRotate (ObitUVDesc* uvDesc)
{
  gfloat rot;

  rot = uvDesc->crota[uvDesc->jlocd];

  return rot;
} /* end ObitUVDescRotate */

/**
 * Returns re-projection matrices to convert initial ref point (u,v,w)
 * and (X,Y,Z) (phase shift terms) to those at a new tangent point
 * Note that these implement the -SIN projection.  They are not needed
 * for -NCP which should not use 3D imaging.
 * Algorithm adapted  from AIPS. PRJMAT.FOR (E. W. Greisen author)
 * \param uvDesc  UV data descriptor giving initial position 
 * \param imDesc  Image descriptor giving desired position 
 * \param URot3D  [out] 3D rotation matrix for u,v,w 
 * \param PRot3D  [out] 3D rotation matrix for x,y,z
 * \return TRUE if these rotation need to be applied.
 */
gboolean ObitUVDescShift3DMatrix (ObitUVDesc *uvDesc, ObitImageDesc* imDesc,
	       gfloat URot3D[3][3], gfloat PRot3D[3][3])
{
  gdouble ra, dec, xra, xdec;
  gfloat urotat, mrotat;
  glong  i, j, k;
  gdouble rm[3][3], sa, ca, sd, cd, sd0, cd0, t[3][3], r, x[3][3];
  gboolean isNCP, do3Dmul = FALSE;

  /* error checks */
  g_assert (ObitUVDescIsA(uvDesc));
  g_assert (ObitImageDescIsA(imDesc));
  g_assert (URot3D != NULL);
  g_assert (PRot3D != NULL);

  /* Get positions assuming 3D */
  ra  = uvDesc->crval[uvDesc->jlocr];
  dec = uvDesc->crval[uvDesc->jlocd];
  xra  = imDesc->crval[imDesc->jlocr];
  xdec = imDesc->crval[imDesc->jlocd];
  mrotat = ObitImageDescRotate(imDesc);
  urotat = ObitUVDescRotate(uvDesc);

  /* Do these corrections need to be applied? */
  do3Dmul = (mrotat!=0.0) || (urotat!=0.0) ||
    (fabs(xra-ra) > 1.0e-10) || (fabs(xdec-dec) > 1.0e-10);

  /* Don't want for NCP projection */
  isNCP = !strncmp(&imDesc->ctype[0][4], "-NCP", 4);
  do3Dmul = do3Dmul && (!isNCP);
  
  /*  sin's and cos's */
  sa  = sin (DG2RAD * (xra-ra));
  ca  = cos (DG2RAD * (xra-ra));
  sd  = sin (DG2RAD * xdec);
  cd  = cos (DG2RAD * xdec);
  sd0 = sin (DG2RAD * dec);
  cd0 = cos (DG2RAD * dec);
  /* rotation matrix */
  for (i=0; i<3; i++) 
    for (j=0; j<3; j++) 
      rm[i][j] = 0.0;
  rm[2][2] = 1.0;

  /* map + */
  r = DG2RAD * mrotat;
  rm[0][0] = cos (r);
  rm[1][1] = rm[0][0];
  rm[1][0] = -sin (r);
  rm[0][1] = sin (r);

  /* forward matrix */
  x[0][0] = ca;
  x[1][0] = -sd * sa;
  x[2][0] = cd * sa;
  x[0][1] = sd0 * sa;
  x[1][1] = cd * cd0 + sd * sd0 * ca;
  x[2][1] = sd * cd0 - cd * sd0 * ca;
  x[0][2] = -cd0 * sa;
  x[1][2] = cd * sd0 - sd * cd0 * ca;
  x[2][2] = sd * sd0 + cd * cd0 * ca;
  
  /* multiply */
  for (i=0; i<3; i++) {
    for (j=0; j<3; j++) {
      t[j][i] = 0.0;
      for (k=0; k<3; k++) {
	t[j][i] += x[k][i] * rm[j][k];
      }
    }
  }
    
    
  /* uv - */
  r = DG2RAD * urotat;
  rm[0][0] = cos (r);
  rm[1][1] = rm[0][0];
  rm[1][0] = sin (r);
  rm[0][1] = -sin (r);
  
  /* multiply */
  for (i=0; i<3; i++) {
    for (j=0; j<3; j++) {
      r = 0.0;
      for (k=0; k<3; k++) {
	r += rm[k][i] * t[j][k];
      }
      URot3D[j][i] = r;
    }
  }
  
  /* uv + */
  r = DG2RAD * urotat;
  rm[0][0] = cos (r);
  rm[1][1] = rm[0][0];
  rm[1][0] = -sin (r);
  rm[0][1] = sin (r);
  
  /* backward matrix */
  x[0][0] = ca;
  x[1][0] = sd0 * sa;
  x[2][0] = -cd0 * sa;
  x[0][1] = -sd * sa;
  x[1][1] = cd * cd0 + sd * sd0 * ca;
  x[2][1] = sd0 * cd - cd0 * sd * ca;
  x[0][2] = cd * sa;
  x[1][2] = cd0 * sd - sd0 * cd * ca;
  x[2][2] = sd * sd0 + cd * cd0 * ca;
  
 /* multiply */
  for (i=0; i<3; i++) {
    for (j=0; j<3; j++) {
      t[j][i] = 0.0;
      for (k=0; k<3; k++) {
	t[j][i] += x[k][i] * rm[j][k];
      }
    }
  }

  /* map - */
  r = DG2RAD * mrotat;
  rm[0][0] = cos (r);
  rm[1][1] = rm[0][0];
  rm[1][0] = sin (r);
  rm[0][1] = -sin (r);

  /* multiply */
  for (i=0; i<3; i++) {
    for (j=0; j<3; j++) {
      r = 0.0;
      for (k=0; k<3; k++) {
	r += rm[k][i] * t[j][k];
      }
      PRot3D[j][i] = r;
    }
  }

  return do3Dmul; /* need to apply? */
} /* end ObitUVDescShift3DMatrix */

/**
 * Initialize global ClassInfo Structure.
 */
 void ObitUVDescClassInit (void)
{
  const ObitClassInfo *ParentClass;

  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;

  /* Initialize (recursively) parent class first */
  ParentClass = ObitGetClass();
  ObitClassInit();  /* Initialize parent class if needed */

  /* function pointers etc. for this class */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = 
    (ObitClassInitFP)ObitUVDescClassInit;
  myClassInfo.newObit       = (newObitFP)newObitUVDesc;
  myClassInfo.ObitCopy      = (ObitCopyFP)ObitUVDescCopy;
  myClassInfo.ObitClone     = NULL;
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitUVDescClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitUVDescInit;
} /* end ObitUVSelClassInit */


/*---------------Private functions--------------------------*/
/**
 * Creates empty member objects, initialize reference count.
 * Does (recursive) initialization of base class members before 
 * this class.
 * \param inn Pointer to the object to initialize.
 */
void ObitUVDescInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitUVDesc *in = inn;

  /* error checks */
  g_assert (in != NULL);
  
  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->naxis      = -1;
  in->nrparm     = -1;
  in->firstVis   = 0;
  in->numVisBuff = 0;
  in->maxAnt     = 0;
  in->info       = newObitInfoList();
  in->freqArr    = NULL;
  in->fscale     = NULL;
  in->freqIF     = NULL;
  in->chIncIF    = NULL;
  in->sideband   = NULL;
  in->numAnt     = NULL;
} /* end ObitUVDescInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * \param  inn Pointer to the object to deallocate.
 */
void ObitUVDescClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitUVDesc *in = inn;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* free this class members */
  if (in->info) ObitInfoListUnref (in->info); in->info = NULL;
  if (in->freqArr)  g_free(in->freqArr);  in->freqArr = NULL;
  if (in->fscale)   g_free(in->fscale);   in->fscale  = NULL;
  if (in->freqIF)   g_free(in->freqIF);   in->freqIF  = NULL;
  if (in->chIncIF)  g_free(in->chIncIF);  in->chIncIF = NULL;
  if (in->sideband) g_free(in->sideband); in->sideband= NULL;
  if (in->numAnt)   g_free(in->numAnt);   in->numAnt  = NULL;
  
  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);

} /* end ObitUVDescClear */

