/* $Id: ObitUVSelfCal.c,v 1.20 2005/09/01 19:58:54 bcotton Exp $ */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2005                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "ObitUVSelfCal.h"
#include "ObitMem.h"
#include "ObitTableUtil.h"
#include "ObitUVUtil.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitUVSelfCal.c
 * ObitUVSelfCal class function definitions.
 * This class enables self calibration of ObitUV data sets
 */

/** name of the class defined in this file */
static gchar *myClassName = "ObitUVSelfCal";

/**
 * ClassInfo structure ObitUVSelfCalClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitUVSelfCalClassInfo myClassInfo = {FALSE};

/*--------------- File Global Variables  ----------------*/
/** Maximum number of antennas */
#ifndef MAXANT
#define MAXANT 30
#endif

/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitUVSelfCalInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitUVSelfCalClear (gpointer in);

/** Private:  Read and average next solution interval */
static gboolean 
NextAvg (ObitUV *inUV, gfloat interv, 
	 gboolean avgif, gboolean avgpol, gfloat* antwt, gfloat uvrang[2], 
	 gfloat wtuv, glong numAnt, glong numFreq, glong numIF, glong numPol, 
	 gdouble* timec, gfloat* timei, gint* sid, gint* fqid, gfloat* vis, 
	 glong *ant1, glong *ant2, glong *nextVisBuf, ObitErr* err);

/** Private: Solve for Gains for a solution interval */
static void 
doSolve (gfloat* vobs, glong *ant1, glong *ant2, glong numAnt, glong numIF, 
	 glong numPol, gint refant, gboolean avgif, gboolean avgpol, gboolean dol1, 
	 gint mode, gint minno, gfloat snrmin, gint prtlv, 
	 gfloat* creal, gfloat* cimag, gfloat* cwt, gint* refan, 
	 gboolean* gotant, gfloat *gain, gfloat *snr, glong *count, ObitErr *err);

/** Private: Determine SNR of solution */
static void   
calcSNR (gfloat* vobs, glong *ant1, glong *ant2, glong numBL, glong numAnt, 
	 gfloat* gain, gfloat* snr, gfloat closer[2][2], gfloat snrmin, gdouble time, 
	 gint iif, gint ist, glong* count, gint prtlv, gchar* prtsou, ObitErr *err);  

/** Private: Gain Soln: Compute least squares gains */
static void 
gainCalc (gfloat* vobs, glong *ant1, glong *ant2, glong numBL, glong numAnt, gint refant, 
	  gint mode, gint minno, gfloat* g, gint* nref, gint prtlv, 
	  gint* ierr, ObitErr* err);

/** Private: Gain Soln: Does L1 solution for gains  */
static void 
gainCalcL1 (gfloat* vobs, glong *ant1, glong *ant2, glong numBL, glong numAnt, gint refant, 
	    gint mode, gint minno, gfloat* g, gint* nref, gint prtlv, 
	    gint* ierr, ObitErr* err);

/** Private:  Determine number of times and usage of each antenna as reference */
static gint 
*refCount (ObitTableSN *SNTab, gint isub, gint *numtime, ObitErr* err);

/** Private: Refer phases to a common reference antenna  */
static void 
refPhase (ObitTableSN *SNTab, gint isub, gint iif, gint refant, gint ant, 
	  gint mxtime, gfloat* wrktim, 
	  gfloat* work1, gfloat* work2, gfloat* work3, gfloat* work4, 
	  gfloat* work5, ObitErr* err);

/** Private: Smooth the solutions for a given IF and subarray  */
static void 
SNSmooth (ObitTableSN *SNTab, gchar* intType, gfloat alpha, gfloat stamp, gfloat stph, 
	  gint iif, gint sub, gfloat* gncnt, gfloat* gnsum, 
	  gint nxt, gfloat* work1, gfloat* work2, ObitErr* err);

/** Private: Boxcar smoothing of an irregularly spaced array  */
static void 
boxsmo (gfloat width, gfloat* x, gfloat* y, gint n, gfloat* ys);

/** Private: Boxcar smoothing with weighting of an irregularly spaced array */
static void 
SmooBox (gfloat smoTime, gfloat* x, gfloat* y, gfloat *w, gint n, 
	 gfloat* ys, gfloat* ws);

/** Private: Gaussian smoothing with weighting of an irregularly spaced array */
static void 
SmooGauss (gfloat smoTime, gfloat* x, gfloat* y, gfloat *w, gint n, 
	   gfloat* ys, gfloat* ws, gfloat* wtsum);

/** Private: Median Window smoothing with weighting of an irregularly spaced array */
static void 
SmooMWF (gfloat smoTime, gfloat alpha, gfloat* x, gfloat* y, gfloat *w, 
	 gint n, gfloat* ys, gfloat* ws, gfloat* yor, gfloat* wor);

/*----------------------Public functions---------------------------*/
/**
 * Constructor.
 * Initializes class if needed on first call.
 * \param name An optional name for the object.
 * \return the new object.
 */
ObitUVSelfCal* newObitUVSelfCal (gchar* name)
{
  ObitUVSelfCal* out;

  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitUVSelfCalClassInit();

  /* allocate/init structure */
  out = g_malloc0(sizeof(ObitUVSelfCal));

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

  /* set ClassInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  ObitUVSelfCalInit((gpointer)out);

 return out;
} /* end newObitUVSelfCal */

/**
 * Returns ClassInfo pointer for the class.
 * \return pointer to the class structure.
 */
gconstpointer ObitUVSelfCalGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitUVSelfCalClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitUVSelfCalGetClass */

/**
 * Make a deep copy of an ObitUVSelfCal.
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \param err Obit error stack object.
 * \return pointer to the new object.
 */
ObitUVSelfCal* ObitUVSelfCalCopy  (ObitUVSelfCal *in, ObitUVSelfCal *out, ObitErr *err)
{
  const ObitClassInfo *ParentClass;
  gboolean oldExist;
  gchar *outName;
  glong i;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return out;
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));

  /* Create if it doesn't exist */
  oldExist = out!=NULL;
  if (!oldExist) {
    /* derive object name */
    outName = g_strconcat ("Copy: ",in->name,NULL);
    out = newObitUVSelfCal(outName);
    g_free(outName);
  }

  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);

  /*  copy this class */
  out->modelMode = in->modelMode;
  out->skyModel = ObitSkyModelUnref(out->skyModel);
  out->skyModel = ObitSkyModelRef(in->skyModel);
  out->SCData   = ObitUVUnref(out->SCData);
  out->SCData   = ObitSkyModelRef(in->SCData);
  out->hist     = ObitMemFree (out->hist);
  out->hist     = ObitMemAlloc0Name(in->numHist*sizeof(gfloat),"Flux Histogram");
  for (i=0; i<in->numHist; i++) out->hist[i] = in->hist[i];
  out->numHist  = in->numHist;
  out->HistInc  = in->HistInc;

  return out;
} /* end ObitUVSelfCalCopy */

/**
 * Make a copy of a object but do not copy the actual data
 * This is useful to create an UVSelfCal similar to the input one.
 * \param in  The object to copy
 * \param out An existing object pointer for output, must be defined.
 * \param err Obit error stack object.
 */
void ObitUVSelfCalClone  (ObitUVSelfCal *in, ObitUVSelfCal *out, ObitErr *err)
{
  const ObitClassInfo *ParentClass;
  gint i;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitIsA(out, &myClassInfo));

  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);

  /*  copy this class */
  out->modelMode = in->modelMode;
  out->skyModel = ObitSkyModelUnref(out->skyModel);
  out->skyModel = ObitSkyModelRef(in->skyModel);
  out->SCData   = ObitUVUnref(out->SCData);
  out->SCData   = ObitSkyModelRef(in->SCData);
  out->hist     = ObitMemFree (out->hist);
  out->hist     = ObitMemAlloc0Name(in->numHist*sizeof(gfloat),"Flux Histogram");
  for (i=0; i<in->numHist; i++) out->hist[i] = in->hist[i];
  out->numHist  = in->numHist;
  out->HistInc  = in->HistInc;

} /* end ObitUVSelfCalClone */

/**
 * Creates an ObitUVSelfCal 
 * \param name      An optional name for the object.
 * \param skyModel  Sky model to normalize uv data
 * \return the new object.
 */
ObitUVSelfCal* ObitUVSelfCalCreate (gchar* name, ObitSkyModel *skyModel)
{
  ObitUVSelfCal* out;

  /* error checks */
  g_assert (ObitSkyModelIsA(skyModel));

  /* Create basic structure */
  out = newObitUVSelfCal (name);

  /* Save SkyModel */
  out->skyModel = ObitSkyModelRef(skyModel);

  return out;
} /* end ObitUVSelfCalCreate */

/**
 * Determine Self calibration for a uv dataset 
 * inUV is divided by the skyModel and an antenna ain solution is made.
 * On output, the dataset has a new SN table with this calibration
 * and is set up to apply them.
 * Routine determines if self calibration is converged, if so TRUE is returned 
 * (else FALSE) and the best SN table is set to be applied.
 * \param in      Input self cal object. 
 * Control parameters are on the info member.
 * \li "subA"    OBIT_int   (1,1,1) Selected subarray (default 1)
 * \li "solInt"  OBIT_float (1,1,1) Solution interval (min). (default 1 sec)
 * \li "refAnt"  OBIT_int   (1,1,1) Ref ant to use. (default 1)
 * \li "avgPol"  OBIT_bool  (1,1,1) True if RR and LL to be averaged (false)
 * \li "avgIF"   OBIT_bool  (1,1,1) True if all IFs to be averaged (false)
 * \li "SNRMin"  OBIT_float (1,1,1) Minimum acceptable SNR (5)
 * \li "doMGM"   OBIT_bool  (1,1,1) True then find the mean gain modulus (true)
 * \li "solType" OBIT_string (4,1,1 Solution type '  ', 'L1',  (' ')
 * \li "solMode" OBIT_string (4,1,1 Solution mode: 'A&P', 'P', 'P!A', 'GCON' ('P')
 * \li "minNo"   OBIT_int   (1,1,1) Min. no. antennas. (default 4)
 * \li "antWt"   OBIT_float (*,1,1) Antenna weights. (default 1.0)
 * \li "UVR_Full"OBIT_float (2,1,1) Range of baseline lengths with full weight
 *                                  (kilolamda). If none is given then 
 *                                  derive one if possible.
 * \li "WtUV"    OBIT_float (1,1,1) Weight outside of UVRANG. (default 1.0)
 * \li "prtLv"   OBIT_int   (1,1,1) Print level (default no print)
 * \li "minFluxPSC" OBIT_float (1,1,1) min peak flux for phase selfcal               
 * \li "minFluxASC" OBIT_float (1,1,1) min peak flux for A&P selfcal
 * \param inUV     Input UV data. 
 * \param init     If True, this is the first SC in a series.
 * \param noSCNeed If True, no self calibration was needed
 * \param err      Error/message stack, returns if error.
 * \return True if SC converged, else False.  No gain solution if True
 */
gboolean ObitUVSelfCalSelfCal (ObitUVSelfCal *in, ObitUV *inUV, gboolean init, 
			       gboolean *noSCNeed, ObitErr *err)
{
  gboolean converged = FALSE;
  ObitTableSN *TabSN=NULL;
  gint32 dim[MAXINFOELEMDIM] = {1,1,1,1,1};
  ObitInfoType type;
  gint i, nfield, *iatemp, itemp, bestSN=0;
  gfloat best, posFlux;
  gfloat minFluxPSC, minFluxASC, minFlux;
  gchar solmod[5];
  gboolean Tr=TRUE, Fl=FALSE, diverged;
  gchar *routine = "ObitUVSelfCalSelfCal";
  
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return converged;
  g_assert (ObitUVSelfCalIsA(in));
  g_assert (ObitUVIsA(inUV));

  *noSCNeed = FALSE;
  /* First this series? */
  if (init) {
    in->numLast = 0;
    for (i=0; i<5; i++) {in->lastQual[i] = 0.0; in->lastSNVer[i] = -1;}
  }

  /* Quality for convergence test of SkyModel */
  /* Total sum of CCs */
  dim[0] = 1; dim[1] = 1;
  ObitInfoListAlwaysPut(in->skyModel->info, "noNeg", OBIT_bool, dim, &Fl);
  in->sumCC = ObitSkyModelSum (in->skyModel, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, converged);

  /* Compress CC files */
  ObitSkyModelCompressCC (in->skyModel, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, converged);

  /* Sum of only positive components */
  dim[0] = 1; dim[1] = 1;
  ObitInfoListAlwaysPut(in->skyModel->info, "noNeg", OBIT_bool, dim, &Tr);
  posFlux = ObitSkyModelSum (in->skyModel, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, converged);

  /* Self cal needed? */
  /* Should actually use peak in image */
  solmod[0] = solmod[1] = solmod[2] = solmod[3] = '0'; solmod[4] = 0;
  ObitInfoListGetTest(in->info, "solMode", &type, dim, solmod);
  minFluxPSC = 1.0e20;
  ObitInfoListGetTest(in->info, "minFluxPSC", &type, dim, &minFluxPSC);
  minFluxASC = 1.0e20;
  ObitInfoListGetTest(in->info, "minFluxASC", &type, dim, &minFluxASC);
  if (solmod[0] == 'A') minFlux = minFluxASC;
  else minFlux = minFluxPSC;
  if (posFlux<minFlux) {
    Obit_log_error(err, OBIT_InfoErr, "Flux %g < %g - no Self cal needed", 
		   posFlux, minFlux);
    *noSCNeed = TRUE;
    return TRUE;
  }

  /* Tell user */
  Obit_log_error(err, OBIT_InfoErr, "Self cal with model flux = %g", posFlux);

  /* RMS field 1 */
  in->RMSFld1 = ObitImageMosaicGetImageRMS (in->skyModel->mosaic, 0, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, converged);

  /* Quality */
  if (in->sumCC>0.0) in->totalQual = in->RMSFld1 / in->sumCC;
  else in->totalQual = 1.0e20;

  /* Divergence test - call it diverged if solution worse that 1.1*last */
  diverged  = (in->numLast>=1) && (in->totalQual > 1.1*in->lastQual[in->numLast-1]);
  converged = diverged;

  /* Convergence test - not improved in 2 cycles */
  converged = 
    converged || ((in->numLast>=2) &&
		  (in->totalQual >= in->lastQual[in->numLast-1]) &&
		  (in->lastQual[in->numLast] >= in->lastQual[in->numLast-1]));

  /* Tell user */
  Obit_log_error(err, OBIT_InfoErr, "RMS = %g Jy, RMS/SumCC = %g",
		 in->RMSFld1, in->totalQual);
  if (diverged) Obit_log_error(err, OBIT_InfoErr, "Solution diverging");
  else if (converged) Obit_log_error(err, OBIT_InfoErr, "Solution converged");
  ObitErrLog(err);
 
  if (converged) { /* Converged, find best SN table */
    best = 1.0e20;
    for (i=0; i<in->numLast; i++) {
      if (best<in->lastQual[i]) {
	best = in->lastQual[i];
	bestSN = in->lastSNVer[i];
      }
    }
    
  } else { /* Do self cal if not converged */

    /* Keep track of this result */
    if (in->numLast>=5) {  /* slide window if necessary */
      in->numLast = 4;
      for (i=0; i<4; i++) {
	in->lastQual[i]  = in->lastQual[i+1];
	in->lastSNVer[i] = in->lastSNVer[i+1];
      }
    }
    in->lastQual[in->numLast]  = in->totalQual;
    in->lastSNVer[in->numLast] = 0;
  
    /* Reset SkyModel to use all components */
    nfield = in->skyModel->mosaic->numberImages;
    iatemp = ObitMemAlloc(nfield*sizeof(gint));  /* temp. array */
    dim[0] = nfield;
    for (i=0; i<nfield; i++) iatemp[i] = 1;
    ObitInfoListAlwaysPut(in->skyModel->info, "BComp", OBIT_int, dim, iatemp);
    for (i=0; i<nfield; i++) iatemp[i] = in->skyModel->endComp[i];
    ObitInfoListAlwaysPut(in->skyModel->info, "EComp", OBIT_int, dim, iatemp);
    iatemp = ObitMemFree(iatemp);  /* Deallocate memory */
    
    /* Scratch file to use for self calibration */
    if (in->SCData==NULL) {
      in->SCData = newObitUVScratch (inUV, err);
      if (err->error) Obit_traceback_val (err, routine, inUV->name, converged);
    }
    
    /* Don't apply any  calibration to inUV */
    dim[0] = 1; dim[1] = 1;
    ObitInfoListAlwaysPut(inUV->info, "doCalSelect", OBIT_bool, dim, &Fl);
    itemp = -1;
    ObitInfoListAlwaysPut(inUV->info, "doCalib", OBIT_int, dim, &itemp);
    
    /* Divide data by model */
    ObitSkyModelDivUV (in->skyModel, inUV, in->SCData, err);
    if (err->error) Obit_traceback_val (err, routine, in->skyModel->name, converged);
    
    /* Self Calibrate */
    TabSN =  ObitUVSelfCalCal (in, in->SCData, inUV, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, converged);
    /* Get SN version number */
    in->lastSNVer[in->numLast-1] = ObitTableGetVersion ((ObitTable*)TabSN, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, converged);
    TabSN =  ObitTableSNUnref (TabSN);
  } /* end of self calibrate if not converged */

  /* Apply Calibration */
  dim[0] = 1; dim[1] = 1;
  ObitInfoListAlwaysPut(inUV->info, "doCalSelect", OBIT_bool, dim, &Tr);
  itemp = 2;
  ObitInfoListAlwaysPut(inUV->info, "doCalib", OBIT_int, dim, &itemp);
  itemp = bestSN;
  ObitInfoListAlwaysPut(inUV->info, "gainUse", OBIT_int, dim, &itemp);
  /* Use all components */
  ObitInfoListAlwaysPut(in->skyModel->info, "noNeg", OBIT_bool, dim, &Fl);

  return converged;
} /* end ObitUVSelfCalSelfCal */

/**
 * Determine phase or amp & phase calibration for an UV dataset divided by
 * a source model.
 * Routine translated from the AIPSish UVUTIL.FOR/SLFCAL 
 * \param in      Input self cal object. 
 * Control parameters are on the info member.
 * \li "subA"    OBIT_int   (1,1,1) Selected subarray (default 1)
 * \li "solInt"  OBIT_float (1,1,1) Solution interval (min). (default 1 sec)
 * \li "refAnt"  OBIT_int   (1,1,1) Ref ant to use. (default 1)
 * \li "avgPol"  OBIT_bool  (1,1,1) True if RR and LL to be averaged (false)
 * \li "avgIF"   OBIT_bool  (1,1,1) True if all IFs to be averaged (false)
 * \li "minSNR"  OBIT_float (1,1,1) Minimum acceptable SNR (5)
 * \li "doMGM"   OBIT_bool  (1,1,1) True then find the mean gain modulus (true)
 * \li "solType" OBIT_string (4,1,1 Solution type '  ', 'L1',  (' ')
 * \li "solMode" OBIT_string (4,1,1 Solution mode: 'A&P', 'P', 'P!A', 'GCON' ('P')
 * \li "minNo"   OBIT_int   (1,1,1) Min. no. antennas. (default 4)
 * \li "antWt"   OBIT_float (*,1,1) Antenna weights. (default 1.0)
 * \li "UVR_Full"OBIT_float (2,1,1) Range of baseline lengths with full weight
 *                                  (kilolamda). If none is given then 
 *                                  derive one if possible.
 * \li "WtUV"    OBIT_float (1,1,1) Weight outside of UVRANG. (default 1.0)
 * \li "prtLv"   OBIT_int   (1,1,1) Print level (default no print)
 * \param inUV   Input UV data. 
 * \param outUV  UV with which the output  SN is to be associated
 * \param err    Error/message stack, returns if error.
 * \return Pointer to the newly created SN object which is associated with outUV.
 */
ObitTableSN* ObitUVSelfCalCal (ObitUVSelfCal *in, ObitUV *inUV, ObitUV *outUV, 
			       ObitErr *err)
{
  ObitTableSN *outSoln=NULL;
  ObitTableSNRow *row=NULL;
  gint32 dim[MAXINFOELEMDIM] = {1,1,1,1,1};
  ObitInfoType type;
  glong ver, i, nif, npoln, iAnt, numBL;
  glong nextVisBuf, cntmgm, iSNRow, numFreq, cntGood=0, cntPoss=0;
  oint numPol, numIF, numAnt, suba, refant, minno, prtlv, mode;
  gfloat solInt, snrmin, uvrang[2], wtuv, summgm;
  gint kday, khr, kmn, ksec, *refAntUse=NULL;
  glong *ant1=NULL, *ant2=NULL, *count=NULL;
  gfloat *antwt=NULL, *creal=NULL, *cimag=NULL, *cwt=NULL;
  gfloat *avgVis=NULL, *gain=NULL, *snr=NULL;
  gboolean avgpol, avgif, domgm, dol1;
  gboolean done, good, *gotAnt;
  gchar soltyp[5], solmod[5];
  gdouble timec, timex;
  gint sid, fqid;
  gfloat timei;
  ObitIOCode retCode;
  gchar *tname, *ModeStr[] = {"A&P", "P", "P!A"};
  gchar *routine = "ObitUVSelfCalCal";
  
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return outSoln;
  g_assert (ObitUVSelfCalIsA(in));
  g_assert (ObitUVIsA(inUV));
  g_assert (ObitUVIsA(outUV));
  
  /* open UV data if not already open */
  if ((inUV->myStatus==OBIT_Inactive) || (inUV->myStatus==OBIT_Defined)) {
    retCode = ObitUVOpen (inUV, OBIT_IO_ReadOnly, err);
    if (err->error) Obit_traceback_val (err, routine, inUV->name, outSoln);
  }
  
  /* Update frequency tables on inUV */
  if (!inUV->myDesc->freqArr) ObitUVGetFreq (inUV, err);
  if (err->error) Obit_traceback_val (err, routine, inUV->name, outSoln);
  /* Need array information */
  if (!inUV->myDesc->numAnt)   ObitUVGetSubA (inUV, err);
  if (err->error) Obit_traceback_val (err, routine, inUV->name, outSoln);
  
  /* Create output - force new SN table */
  tname = g_strconcat ("SN Calibration for: ", outUV->name, NULL);
  ver = 0;
  if (inUV->myDesc->jlocs>=0)
    numPol = MIN (2, inUV->myDesc->inaxes[inUV->myDesc->jlocs]);
  else numPol = 1;
  if (inUV->myDesc->jlocif>=0)
    numIF  = inUV->myDesc->inaxes[inUV->myDesc->jlocif];
  else numIF  = 1;
  outSoln = newObitTableSNValue(tname, (ObitData*)outUV, &ver, OBIT_IO_WriteOnly, 
				numPol, numIF, err);
  g_free (tname);
  if (err->error) Obit_traceback_val (err, routine, inUV->name, outSoln);
  if (inUV->myDesc->jlocf>=0)
    numFreq  = inUV->myDesc->inaxes[inUV->myDesc->jlocf];
  else numFreq  = 1;
 
  /* Init mean gain modulus statistics */
  summgm = 0.0;
  cntmgm = 0;
  
  /* Which subarray? */
  suba = 1;
  ObitInfoListGetTest(in->info, "subA", &type, dim, &suba);
  /* Can only do one */
  Obit_retval_if_fail((suba>0 && suba<=inUV->myDesc->numSubA), err, outSoln,
		      "%s: MUST specify a single subarray for %s", 
		      routine, inUV->name);
  
  /* Create arrays */
  numAnt = inUV->myDesc->numAnt[suba-1];
  gain   = g_malloc0(2*numAnt*sizeof(gfloat));
  snr    = g_malloc0(numAnt*sizeof(gfloat));
  count  = g_malloc0(numAnt*sizeof(glong));
  antwt  = g_malloc0(numAnt*sizeof(gfloat));
  creal  = g_malloc0(numAnt*numIF*numPol*sizeof(gfloat));
  cimag  = g_malloc0(numAnt*numIF*numPol*sizeof(gfloat));
  cwt    = g_malloc0(numAnt*numIF*numPol*sizeof(gfloat));
  gotAnt = g_malloc0(numAnt*sizeof(gboolean));
  refAntUse = g_malloc0(numIF*numPol*sizeof(gint));
  numBL  = (numAnt * (numAnt-1)) / 2;
  ant1   = g_malloc0(numBL*sizeof(glong)+5);
  ant2   = g_malloc0(numBL*sizeof(glong)+5);
  
  /* Get parameters from inUV */
  solInt = 1.0 / 60.0;
  ObitInfoListGetTest(in->info, "solInt", &type, dim, &solInt);
  solInt /= 1440.0;  /* Convert to days */
  refant = 1;
  ObitInfoListGetTest(in->info, "refAnt", &type, dim, &refant);
  avgpol = FALSE;
  ObitInfoListGetTest(in->info, "avgPol", &type, dim, &avgpol);
  avgif = FALSE;
  ObitInfoListGetTest(in->info, "avgIF",  &type, dim, &avgif);
  snrmin = 5.0;
  ObitInfoListGetTest(in->info, "minSNR", &type, dim, &snrmin);
  domgm = TRUE;
  ObitInfoListGetTest(in->info, "doMGM", &type, dim, &domgm);
  soltyp[0] = soltyp[1] = soltyp[2] = soltyp[3] = '0'; soltyp[4] = 0;
  ObitInfoListGetTest(in->info, "solType", &type, dim, soltyp);
  solmod[0] = solmod[1] = solmod[2] = solmod[3] = '0'; solmod[4] = 0;
  ObitInfoListGetTest(in->info, "solMode", &type, dim, solmod);
  minno = 4;
  ObitInfoListGetTest(in->info, "minNo",  &type, dim, &minno);
  for (i=0; i<numAnt; i++) antwt[i] = 1.0;
  ObitInfoListGetTest(in->info, "antWt",  &type, dim, &antwt[0]);
  uvrang[0] = 0.0; uvrang[1] = 1.0e15;
  if (!ObitInfoListGetTest(in->info, "UVR_Full", &type, dim, &uvrang[0])
      || (uvrang[1]<=uvrang[0])) {
    /* If no explicit uv range given, derive one */
    ObitUVSelfCalBLRange (in, err);
    uvrang[0] = in->UVFullRange[0];
    uvrang[1] = in->UVFullRange[1];
  }  /* end derive uv range */
  wtuv = 1.0;
  ObitInfoListGetTest(in->info, "WtUV", &type, dim, &wtuv);
  prtlv = 0;
  ObitInfoListGetTest(in->info, "prtLv", &type, dim, &prtlv);
  
  /* Digest SOLMODE and SOLTYPE */
  dol1   = !strncmp(soltyp, "L1",2);
  mode = 1;
  if (!strncmp(solmod, "A&P",3))  mode = 0;
  if (solmod[0]=='P')             mode = 1;
  if (!strncmp(solmod, "P!A", 3)) mode = 2;
  Obit_log_error(err, OBIT_InfoErr, "Self calibrate in %s mode", ModeStr[mode]);
  ObitErrLog(err);
 
  /* Averaging of data? */
  nif = numIF;
  if (avgif) nif = 1;
  npoln = numPol;
  if (avgpol) npoln = 1;
  
  /* Allocate average visibility array */
  avgVis  = g_malloc0(3*nif*npoln*numBL*sizeof(gfloat));
  
  /* Open output table */
  retCode = ObitTableSNOpen (outSoln, OBIT_IO_WriteOnly, err);
  if (err->error) Obit_traceback_val (err, routine, outSoln->name, outSoln);
  outSoln->numAnt = numAnt;  /* Number of antennas */
  outSoln->mGMod  = 1.0;     /* initial mean gain modulus */
  
  /* Create Row */
  row = newObitTableSNRow (outSoln);
  
  /* Attach row to output buffer */
  ObitTableSNSetRow (outSoln, row, err);
  if (err->error) Obit_traceback_val (err, routine, inUV->name, outSoln);
  
  /* Initialize solution row */
  row->Time   = 0.0; 
  row->TimeI  = 0.0; 
  row->SourID = 0; 
  row->antNo  = 0; 
  row->SubA   = 0; 
  row->FreqID = 0; 
  row->IFR    = 0.0; 
  row->NodeNo = 0; 
  row->MBDelay1 = 0.0; 
  for (i=0; i<numIF; i++) {
    row->Real1[i]   = 0.0; 
    row->Imag1[i]   = 0.0; 
    row->Delay1[i]  = 0.0; 
    row->Rate1[i]   = 0.0; 
    row->Weight1[i] = 0.0; 
    row->RefAnt1[i] = 0; 
  }
  if (numPol>1) {
    row->MBDelay2 = 0.0; 
    for (i=0; i<numIF; i++) {
      row->Real2[i]   = 0.0; 
      row->Imag2[i]   = 0.0; 
      row->Delay2[i]  = 0.0; 
      row->Rate2[i]   = 0.0; 
      row->Weight2[i] = 0.0; 
      row->RefAnt2[i] = 0; 
    }
  }
  
  /* Loop until done */
  done = FALSE;
  nextVisBuf = -1;
  while (!done) {
    /* Read and average next solution interval */
    done =  NextAvg (inUV, solInt, avgif, avgpol, antwt, uvrang, wtuv, 
		     (glong)numAnt, numFreq, (glong)numIF, (glong)numPol, 
		     &timec, &timei, &sid, &fqid, avgVis, ant1, ant2, &nextVisBuf, 
		     err);
    if (err->error) Obit_traceback_val (err, routine, inUV->name, outSoln);

    /* How many possible solutions */
    cntPoss += numAnt;
    
    /* Done? 
    if (done) break; still have OK data */
    
    /* Write time if requested */
    if (prtlv >= 1) {
      kday = timec;
      timex = (timec - kday) * 24.;
      khr = timex;
      timex = (timex - khr) * 60.;
      kmn = timex;
      timex = (timex - kmn) * 60.;
      ksec = timex + 0.5;
      Obit_log_error(err, OBIT_InfoErr, " time=%d/ %3d %3d %3d,", 
		     kday, khr, kmn, ksec);
    } 
    
    /* Do solutions */
    doSolve (avgVis, ant1, ant2, numAnt, numIF, numPol, refant, avgif, avgpol, 
	     dol1, mode, minno, snrmin, prtlv, creal, cimag, cwt, refAntUse, 
	     gotAnt, gain, snr, count, err);
    
    /* Write solutions to SN table */
    /* Common values */
    row->Time   = timec; 
    row->TimeI  = timei; 
    row->SourID = sid; 
    row->SubA   = suba; 
    row->FreqID = fqid; 
    
    /* Loop over antennas */
    iSNRow = -1;
    for (iAnt= 0; iAnt<numAnt; iAnt++) {
      if (gotAnt[iAnt]) {
	good = FALSE; /* Until proven */
	row->antNo  = iAnt+1; 
	for (i=0; i<numIF; i++) {
	  row->Real1[i]   = creal[iAnt+i*numAnt]; 
	  row->Imag1[i]   = cimag[iAnt+i*numAnt]; 
	  row->Weight1[i] = cwt[iAnt+i*numAnt]; 
	  row->RefAnt1[i] = refAntUse[i]; 
	  if ( cwt[i]>0.0) good = TRUE; 
	  /* If good sum mean gain mudulus */
	  if (domgm && cwt[i]>0.0) {
	    cntmgm++;
	    summgm += sqrt(creal[i]*creal[i]+cimag[i]*cimag[i]);
	  }
	}
	if (numPol>1) {
	  row->MBDelay2 = 0.0; 
	  for (i=0; i<numIF; i++) {
	    row->Real2[i]   = creal[iAnt+(i+numIF)*numAnt]; 
	    row->Imag2[i]   = cimag[iAnt+(i+numIF)*numAnt]; 
	    row->Weight2[i] = cwt[iAnt+(i+numIF)*numAnt]; 
	    row->RefAnt2[i] = refAntUse[numIF+i];
	    if ( cwt[numAnt+i]>0.0) good = TRUE; 
	    /* If good sum mean gain modulus */
	    if (domgm && cwt[numAnt+i]>0.0) {
	      cntmgm++;
	      summgm += sqrt(creal[numAnt+i]*creal[numAnt+i]+
			     cimag[numAnt+i]*cimag[numAnt+i]);
	    }
	  }
	}
	retCode = ObitTableSNWriteRow (outSoln, iSNRow, row, err);
	if (err->error) Obit_traceback_val (err, routine, outSoln->name, outSoln);
	/* How many good solutions */
	if (good) cntGood++;
      } /* end if gotant */
    }
  } /* end loop processing data */
  
  /* Close input uv data */
  ObitUVClose (inUV, err);
  if (err->error) Obit_traceback_val (err, routine, inUV->name, outSoln);
  
  /* Save mean gain modulus if requested */
  if (domgm  &&  (cntmgm > 0)  &&  ((mode == 0) || (mode == 3))) {
    /* Update */
    outSoln->mGMod  = summgm / cntmgm;
  }
  
  /* If suba>1 mark as unsorted */
  if (suba == 1) {  /* IF subarray 1 the sort is time, antenna */
    outSoln->myDesc->sort[0] = outSoln->TimeCol+1;
    outSoln->myDesc->sort[1] = outSoln->antNoCol+1; 
  } else { /* otherwise unsorted */
    outSoln->myDesc->sort[0] = 0;
    outSoln->myDesc->sort[1] = 0;
  }

  /* Close output table */
  retCode = ObitTableSNClose (outSoln, err);
  if (err->error) Obit_traceback_val (err, routine, outSoln->name, outSoln);
  
  /* Give success rate */
  Obit_log_error(err, OBIT_InfoErr, "%ld of %ld possible solutions found",
		 cntGood, cntPoss);
  
  /* Require at least 10% */
  Obit_retval_if_fail((cntGood >= 0.1*cntPoss), err, outSoln,
		      "%s: TOO FEW Successful selfcal solutions for  %s", 
		      routine, inUV->name);
  
  /* Cleanup */
  row = ObitTableSNUnref(row);
  if (gain)   g_free(gain);
  if (snr)    g_free(snr);
  if (count)  g_free(count);
  if (antwt)  g_free(antwt);
  if (creal)  g_free(creal);
  if (cimag)  g_free(cimag);
  if (cwt)    g_free(cwt);
  if (gotAnt) g_free(gotAnt);
  if (avgVis) g_free(avgVis);
  if (ant1)   g_free(ant1);
  if (ant2)   g_free(ant2);
  if (refAntUse) g_free(refAntUse);
  
  return outSoln;
} /* end ObitUVSelfCalCal */

/**
 * References the phases to a common reference antenna in a  
 * polarization coherent fashion.  
 * Leaves the output table sorted in time-antenna order.  
 * Routine translated from the AIPSish UVUTIL.FOR/SLFREF  
 * \param in      Input self cal object. 
 * \param SNTab  SN table object 
 * \param isuba  Desired subarray, 0=> 1 
 * \param refant Reference antenna, if 0 then the most commonly used 
 *               reference antenna is picked. 
 * \param err    Error/message stack, returns if error.
 */
void ObitUVSelfCalRefAnt (ObitUVSelfCal *in, ObitTableSN *SNTab, gint isuba, 
			  gint* refant, ObitErr* err) 
{
  ObitIOCode retCode;
  gint   ant, iif, isub, numif, numpol, numant, numtime, iref, crefa, *antuse=NULL;
  gfloat *wrkTime=NULL, *work1=NULL, *work2=NULL, *work3=NULL, *work4=NULL, *work5=NULL;
  gchar msgtxt[81];
  gchar *routine = "ObitUVSelfCalRefAnt";
  
  /* Error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return ;  /* previous error? */
  g_assert (ObitUVSelfCalIsA(in));
  g_assert(ObitTableSNIsA(SNTab));
 
  /* Subarray */
  isub = MAX (1, isuba);
  
  /* Must be time order */
  if (SNTab->myDesc->sort[0]!=(SNTab->TimeOff+1)) {
    retCode = ObitTableUtilSort ((ObitTable*)SNTab, "TIME    ", FALSE, err);
    if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
  }

  /* Open table */
  retCode = ObitTableSNOpen (SNTab, OBIT_IO_ReadWrite, err);
  if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
 
  /* Get descriptive info */
  numif  = SNTab->numIF;
  numpol = SNTab->numPol;
  numant = SNTab->numAnt;

  /* Determine which antennas used as reference antennas, number of times */
  antuse = refCount (SNTab, isub, &numtime, err);
  if (err->error) Obit_traceback_msg (err, routine, SNTab->name);

  /* Create work arrays */
  wrkTime = g_malloc0(numtime*sizeof(gfloat));
  work1   = g_malloc0(numtime*sizeof(gfloat));
  work2   = g_malloc0(numtime*sizeof(gfloat));
  work3   = g_malloc0(numtime*sizeof(gfloat));
  work4   = g_malloc0(numtime*sizeof(gfloat));
  work5   = g_malloc0(numtime*sizeof(gfloat));

  /* Determine reference antenna if  necessary. */
  if (*refant <= 0) {
    (*refant) = 1;
    crefa = antuse[*refant-1];
    /* Pick most commonly used */
    for (ant=1; ant<numant; ant++) { /* loop 30 */
      if (antuse[ant] > crefa) {
	*refant = ant+1;
	crefa = antuse[(*refant)-1];
      } 
    } /* end loop  L30:  */;
  } /* end picking reference antenna */
  iref = *refant;

  /* Message about rereferencing. */
  g_snprintf (msgtxt,80, "Rereferencing phases to antenna %3d", *refant);
  Obit_log_error(err, OBIT_InfoErr, msgtxt);

  /* Loop through antennas used as  secondary reference antennas. */
  for (ant= 1; ant<=numant; ant++) { /* loop 500 */
    if ((antuse[ant-1] <= 0)  ||  (ant == *refant)) continue; /*goto L500;*/
    for (iif=0; iif<numif; iif++) { /* loop 300 */

      refPhase (SNTab, isub, iif, iref, ant, numtime, wrkTime, 
		work1, work2, work3, work4, work5, err);
      if (err->error) {
	Obit_log_error(err, OBIT_InfoErr,
		       "%s: ERROR ref. ant %3d phases to antenna %3d in %s", 
		       routine, ant, *refant, SNTab->name);
	return;
      }
    } /* end IF loop  L300: */;
  } /* end of antenna loop  L500: */;
  
  /* Close table */
  retCode = ObitTableSNClose (SNTab, err);
  if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
  
  /* Cleanup */
  if (antuse)  g_free (antuse);
  if (wrkTime) g_free (wrkTime);
  if (work1)   g_free (work1);
  if (work2)   g_free (work2);
  if (work3)   g_free (work3);
  if (work4)   g_free (work4);
  if (work5)   g_free (work5);

} /* end of routine ObitUVSelfCalRefAnt */ 

/**
 * Smooths the SN table which should already be referenced to a single  
 * reference antenna.  Failed solutions are interpolated.  
 * Leaves the output table sorted in antenna-time order.  
 * Routine translated from the AIPSish UVUTIL.FOR/SLFSMO  
 * \param in      Input self cal object. 
 * \li smoType   OBIT_string (4,1,1) smoothing type 'MWF' 
 *               (median window filter), 
 *               "GAUS' (Gaussian) else "BOX", 
 * \li smoAmp    Amplitude smoothing time in min. 
 * \li smoPhase  Phase smoothing time in min. (0 => fix failed only
 * \param inUV   UV data object must contain 
 * \param SNTab  SN table object 
 * \param isuba  Desired subarray, 0=> 1 
 * \param err    Error/message stack, returns if error.
 */
void ObitUVSelfSNSmo (ObitUVSelfCal *in, ObitUV *inUV, ObitTableSN *SNTab, gint isuba, 
		      ObitErr* err) 
{
  ObitIOCode retCode;
  gint32 dim[UV_MAXDIM];
  ObitInfoType type;
  gchar  smtype[5];
  gint   iif, isub, numant, numpol, numif;
  gfloat smtamp, smtphs, gncnt, gnsum;
  gint   mxtime, ntmp, *antuse;
  gfloat *work1=NULL, *work2=NULL;
  gchar *routine = "ObitUVSelfSNSmo";

  /* Error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;  /* previous error? */
  g_assert (ObitUVSelfCalIsA(in));
  g_assert(ObitUVIsA(inUV));
  g_assert(ObitTableSNIsA(SNTab));

  /* Info from uv data */
  if(!ObitInfoListGet(in->info, "smoType", &type, dim, smtype, err)) 
    Obit_traceback_msg (err, routine, inUV->name);
  smtype[dim[0]] = 0;
  if(!ObitInfoListGet(in->info, "smoAmp", &type, dim, &smtamp, err)) 
    Obit_traceback_msg (err, routine, inUV->name);
  if(!ObitInfoListGet(in->info, "smoPhase", &type, dim, &smtphs, err)) 
    Obit_traceback_msg (err, routine, inUV->name);

  /* Subarray */
  isub = MAX (1, isuba);
  
  /* MUST be time order */
  if (SNTab->myDesc->sort[0]!=SNTab->TimeOff) {
    retCode = ObitTableUtilSort ((ObitTable*)SNTab, "TIME    ", FALSE, err);
    if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
  }

  /* Open table */
  retCode = ObitTableSNOpen (SNTab, OBIT_IO_ReadWrite, err);
  if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
 
  /* Get descriptive info */
  numif  = SNTab->numIF;
  numpol = SNTab->numPol;
  numant = SNTab->numAnt;

  /* Determine which antennas used as reference antennas, number of times */
  antuse = refCount (SNTab, isub, &mxtime, err);
  g_free(antuse);  /* don't need */
  if (err->error) Obit_traceback_msg (err, routine, SNTab->name);

  work1 = g_malloc(10*mxtime*sizeof(gfloat));
  /* How many secondary work arrays */
  if (!strncmp(smtype, "GAUS", 4)) ntmp = 3;
  else if (!strncmp(smtype, "MWF", 3)) ntmp = 4;
  else ntmp = 2;
  work2 = g_malloc(ntmp*mxtime*sizeof(gfloat));

  /* Reset mean gain modulus info  */
  gncnt = 0.0;
  gnsum = 0.0;
  for (iif= 1; iif<=numif; iif++) { /* loop over IFs 300 */
    /* Smooth this IF */
    SNSmooth (SNTab, smtype, 0.5, smtamp, smtphs, iif, isub, 
	      &gncnt, &gnsum, mxtime, work1, work2, err);
    if (err->error) {
      g_free(work1); g_free(work2);  /* Cleanup on error */
      Obit_traceback_msg (err, routine, SNTab->name);
    }
  } /* end loop  L300: */;

  /* Save mean gain modulus if requested */
  if ((fabs (SNTab->mGMod-1.0) > 1.0e-5)  &&  (gncnt > 0.1)) {
    SNTab->mGMod  = gnsum / gncnt;
  } 

  /* Close output table */
  retCode = ObitTableSNClose (SNTab, err);
    if (err->error) {
      g_free(work1); g_free(work2);
      Obit_traceback_msg (err, routine, SNTab->name);
    }

    /* Cleanup */
    if (work1) g_free(work1);
    if (work2) g_free(work2);
  
} /* end of routine ObitUVSelfSNSmo */ 

/**
 * Routine to deselect records in an SN table if they match a given
 * subarray, have a selected FQ id, appear on a list of antennas and
 * are in a given timerange.
 * \param in         Input self cal object. 
 * \param SNTab      SN table object 
 * \param isub       Subarray number, <=0 -> any
 * \param fqid       Selected FQ id, <=0 -> any
 * \param nantf      Number of antennas in ants
 * \param ants       List of antennas, NULL or 0 in first -> flag all
 * \param timerange  Timerange to flag, 0s -> all
 * \param err        Error/message stack, returns if error.
 */
void ObitUVSelfCalDeselSN (ObitUVSelfCal *in, ObitTableSN *SNTab, 
			   gint isuba, gint fqid, gint nantf, gint *ants, 
			   gfloat timerange[2], ObitErr* err)
{
  ObitIOCode retCode;
  ObitTableSNRow *row=NULL;
  gint   i;
  gboolean allAnt, flagIt;
  gfloat tr[2];
  glong  loop;
  gchar  *routine = " ObitUVSelfCalDeselSN";
  
  /* Error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;  /* previous error? */
  g_assert (ObitUVSelfCalIsA(in));
  g_assert(ObitTableSNIsA(SNTab));
 
 /* Open table */
  retCode = ObitTableSNOpen (SNTab, OBIT_IO_ReadWrite, err);
  if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
 
  /* All antennas to be flagged? */
  allAnt = ((ants==NULL) || (ants[0]<0));

  /* Timerange */
  tr[0] = timerange[0];
  tr[1] = timerange[1];
  if ((tr[0]==0.0) && (tr[1]==0.0)) {
    tr[0] = -1.0e20;
    tr[1] =  1.0e20;
  }

  /* Create Row */
  row = newObitTableSNRow (SNTab);
  /* Loop through table */
  for (loop=1; loop<=SNTab->myDesc->nrow; loop++) { /* loop 20 */

    retCode = ObitTableSNReadRow (SNTab, loop, row, err);
    if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
    if (row->status<0) continue;  /* Skip deselected record */

    /* Flag this one? */
    flagIt = (row->SubA==isuba) || (isuba<=0);             /* by subarray */
    flagIt = flagIt || (row->FreqID==fqid) || (fqid<=0);      /* by FQ id */
    flagIt = flagIt || ((row->Time<tr[0]) || (row->Time>tr[1])); /* by time */
    flagIt = flagIt || allAnt;                          /* Check Antenna */
    if (!flagIt) for (i=0; i<nantf; i++) {
      if (row->antNo==ants[i]) {flagIt = TRUE; break;}
    }

    if (flagIt) { /* flag */
      /* Rewrite record flagged */
      row->status = -1;
      retCode = ObitTableSNWriteRow (SNTab, loop, row, err);
      if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
    } /* end if flag */
  } /* End loop over table */

  /* Close table */
  retCode = ObitTableSNClose (SNTab, err);
  if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
  row = ObitTableSNRowUnref (row);  /* Cleanup */
} /* end ObitUVSelfCalDeselSN */

/**
 * Determine the flux density histogram of amp vs baseline length.
 * Histogram forced to be monitonically decreasing with baseline.
 * \param in    Input Self Cal
 * \param inUV  UV data to examine
 * \param err   Error stack, returns if not empty.
 */
void ObitUVSelfCalFluxHist (ObitUVSelfCal *in, ObitUV *inUV, ObitErr *err)
{
  ObitIOCode retCode=OBIT_IO_SpecErr;
  gint i, j, k, inc, icell;
  gfloat MaxBL, MaxW, *count, bl, *u, *v, *w, *vis, icells, last, amp;
  gchar *routine = "ObitUVSelfCalBLRange";
 
   /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitUVSelfCalIsA(in));
  g_assert (ObitUVIsA(inUV));

  /* Get baseline range */
  ObitUVUtilUVWExtrema (inUV, &MaxBL, &MaxW, err);
  if (err->error) Obit_traceback_msg (err, routine, inUV->name);

  /* Create histogram, count arrays */
  /* Size of histogram Max (1000, 1% of vis) */
  in->numHist = MIN (1000, (glong)(0.01*inUV->myDesc->nvis)); 
  in->HistInc = MaxBL / in->numHist;  /* Increment between cells */
  in->hist    = ObitMemAlloc0Name(in->numHist*sizeof(gfloat),"Flux Histogram");
  count       = ObitMemAlloc0Name(in->numHist*sizeof(gfloat),"Histogram Counts");
  for (i=0; i<in->numHist; i++) {in->hist[i] = 0.0; count[i]=0.0;}
  icells = 1.0 / in->HistInc;  /* inverse of cell spacing */

  /* increment in frequency array = number of Stokes */
  inc = inUV->myDesc->inaxes[inUV->myDesc->jlocs];  

  /* Open uv data if not already open */
  if (inUV->myStatus==OBIT_Inactive) {
    retCode = ObitUVOpen (inUV, OBIT_IO_ReadOnly, err);
    if (err->error) Obit_traceback_msg (err, routine, inUV->name);
  }
  
  /* Loop through data */
  while (retCode==OBIT_IO_OK) {
    /* read buffer full */
    retCode = ObitUVRead (inUV, NULL, err);
    if (err->error) Obit_traceback_msg (err, routine, inUV->name);
    
    /* initialize data pointers */
    u   = inUV->buffer+inUV->myDesc->ilocu;
    v   = inUV->buffer+inUV->myDesc->ilocv;
    w   = inUV->buffer+inUV->myDesc->ilocw;
    vis = inUV->buffer+inUV->myDesc->nrparm;
    for (i=0; i<inUV->myDesc->numVisBuff; i++) { /* loop over buffer */
      
      /* Get statistics */
      bl = sqrt ((*u)*(*u) + (*v)*(*v));

      /* Loop over correlations */
      for (j=0; j<inUV->myDesc->ncorr; j++) {
	k = j / inc;  /* Index in frequency scaling array */
	if (vis[j*3+2]>0.0) {
	  icell = bl * icells * inUV->myDesc->fscale[k] + 0.5;
	  icell = MAX (0, MIN (icell, in->numHist-1));
	  amp = sqrt (vis[j*3]*vis[j*3] + vis[j*3+1]*vis[j*3+1]);
	  count[icell]++;
	  in->hist[icell] += amp;
	} /* end if valid data */
      } /* end loop over correlations */
      
      /* update data pointers */
      u += inUV->myDesc->lrec;
      v += inUV->myDesc->lrec;
      w += inUV->myDesc->lrec;
      vis += inUV->myDesc->lrec;
    } /* end loop over buffer */
  } /* end loop over file */
  
    /* Close */
  retCode = ObitUVClose (inUV, err);
  if (err->error) Obit_traceback_msg (err, routine, inUV->name);

  /* Normalize */
  last = 1.0e20;
  for (i=0; i<in->numHist; i++) {
    if (count[i] > 0.0) {
      in->hist[i] /= count[i];
      last = in->hist[i];
    }  else in->hist[i] = last;
  }

  /* Make sure monitonic - work backwards through list */
  last = 0.0;
  for (i=0; i<in->numHist; i++) {
    j = in->numHist - i - 1;
    in->hist[j] = MAX (in->hist[j], last);
    last = in->hist[j];
  }
  /* Cleanup */
  count     = ObitMemFree(count);
} /* end  ObitUVSelfCalFluxHist */

/**
 * Find UV range that encloses a given flux density..
 * Returns a uv range from the shortest baseline whose average flux 
 * amplitude equals the sum of the clean components in the mosaic 
 * member of the SkyModel member to infinity.
 * Results saved in the UVFullRange member.
 * If there is no histogram all baselines are accepted.
 * \param in       Input Self Cal
 * \param inUV     UV data to examine
 * \param err      Error stack, returns if not empty.
 */
void ObitUVSelfCalBLRange (ObitUVSelfCal *in, ObitErr *err)
{
  glong i;
  /*gchar *routine = "ObitUVSelfCalBLRange";*/

   /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitUVSelfCalIsA(in));

  /* If no histogram take all */
  if (in->numHist<=0) {
    in->UVFullRange[0] = 0.0;
    in->UVFullRange[1] = 1.0e20;
    return;
  }

  /* Find first element in hist < in->sumCC */
  for (i=0; i<in->numHist; i++) {
    if (in->hist[i]<in->sumCC) break;
  }

  /* Save result */
  in->UVFullRange[0] = i * in->HistInc;
  in->UVFullRange[1] = 1.0e20;

  /* Tell about it */
  Obit_log_error(err, OBIT_InfoErr, "Full weight  from %g klambda in SelfCal", 
		 in->UVFullRange[0]*0.001);

} /* end ObitUVSelfCalBLRange */

/**
 * Initialize global ClassInfo Structure.
 */
void ObitUVSelfCalClassInit (void)
{
  const ObitClassInfo *ParentClass;
  
  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;
  
  /* Initialize (recursively) parent class first */
  ParentClass = ObitGetClass();
  ObitClassInit();  /* Initialize parent class if needed */
  
  /* function pointers etc. for this class */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = (ObitClassInitFP)ObitUVSelfCalClassInit;
  myClassInfo.newObit       = (newObitFP)newObitUVSelfCal;
  myClassInfo.ObitCopy      = (ObitCopyFP)ObitUVSelfCalCopy;
  myClassInfo.ObitClone     = NULL;
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitUVSelfCalClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitUVSelfCalInit;
  myClassInfo.ObitUVSelfCalCreate = (ObitUVSelfCalCreateFP)ObitUVSelfCalCreate;
} /* end ObitUVSelfCalClassInit */

/*---------------Private functions--------------------------*/

/**
 * Creates empty member objects, initialize reference count.
 * Parent classes portions are (recursively) initialized first
 * \param inn Pointer to the object to initialize.
 */
void ObitUVSelfCalInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitUVSelfCal *in = inn;

  /* error checks */
  g_assert (in != NULL);

  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->thread    = newObitThread();
  in->info      = newObitInfoList(); 
  in->modelMode = OBIT_SkyModel_Fastest;
  in->skyModel  = NULL;
  in->SCData    = NULL;
  in->numHist   = 0;
  in->HistInc   = 1.0;
  in->hist      = NULL;
  in->UVFullRange[0] = 0.0;
  in->UVFullRange[1] = 0.0;

} /* end ObitUVSelfCalInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * \param  inn Pointer to the object to deallocate.
 *           Actually it should be an ObitUVSelfCal* cast to an Obit*.
 */
void ObitUVSelfCalClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitUVSelfCal *in = inn;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* delete this class members */
  in->thread   = ObitThreadUnref(in->thread);
  in->info     = ObitInfoListUnref(in->info);
  in->skyModel = ObitSkyModelUnref(in->skyModel);
  in->SCData   = ObitUVUnref(in->SCData);
  in->hist     = ObitMemFree(in->hist);
  
  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);
  
} /* end ObitUVSelfCalClear */

/**
 * Average next solution interval
 *  Data is averaged until one of several conditions is met:
 *  \li the time exceeds the initial time plus the specified interval.
 *  \li the source id (if present) changes
 *  \li the FQ id (if present) changes. 
 * Routine translated from the AIPSish UVUTIL.FOR/NXTAVG
 * \param inUV    Input UV data. 
 * \param interv  Desired time interval of average in days. 
 * \param avgif   If true average in IF 
 * \param avgpol  If true average in polarization (Stokes 'I')
 * \param antwt   Extra weights to antennas (>=0 => 1.0)
 * \param uvrang  Range of baseline lengths with full weight (kilolamda). 
 *                0s => all baselines 
 * \param wtuv    Weight outside of UVRANG. (No default)
 * \param numAnt  Highest antenna number (NOT number of antennas)
 * \param numIF   Maximum number of Frequencies 
 * \param numIF   Maximum number of IFs 
 * \param numPol  Maximum number of polarizations
 * \param timec   [out] Center time of observations (days)
 * \param timei   [out] Actual time interval (days)
 * \param sid     [out] Source Id if present else -1. 
 * \param fqid    [out] FQ id if present else -1.
 * \param avgVis  [out] [3,baseline,IF,pol]
 * \param ant1    [out] Array of first antenna numbers (1-rel) on a baseline
 * \param ant2    [out] Array of second antenna numbers (1-rel) on a baseline
 * \param nextVisBuf [in/out] next vis (0-rel) to read in buffer, 
 *                   -1=> read, -999 done
 * \param err    Error/message stack, returns if error.
 * \return TRUE if all data read, else FALSE
 */
static gboolean 
NextAvg (ObitUV* inUV, gfloat interv, 
	 gboolean avgif, gboolean avgpol, gfloat* antwt, gfloat uvrang[2], 
	 gfloat wtuv, glong numAnt, glong numFreq, glong numIF, glong numPol, 
	 gdouble* timec, gfloat* timei, gint* sid, gint* fqid, gfloat* avgVis, 
	 glong *ant1, glong *ant2, glong *nextVisBuf, ObitErr* err)
{
  gboolean done=FALSE;
  ObitIOCode retCode= OBIT_IO_OK;
  gfloat linterv, ctime, cbase, stime, ltime=0, weight, wt, bl, *visPnt;
  gint i, j, csid, cfqid, a1, a2, *blLookup=NULL, blIndex, visIndex;
  gint iFreq, iIF, iStok, jBL, jIF, jStok, jncs, jncif, mPol, mIF, offset, numBL;
  gdouble timeSum;
  glong timeCount, accumIndex;
  glong maxAllow; /* DEBUG */
  /* glong maxShit=0; DEBUG */
  gchar *routine = "ObitUVSelfCal:NextAvg";
  
  /* Error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return done;  /* previous error? */
  g_assert(ObitUVIsA(inUV));
  
  /* Did we hit the end last time? */
  if (*nextVisBuf==-999) return TRUE;

  /* Shave a bit off the interval */
  linterv = 0.99*interv;
  
  /* Increments in accumulator */  
  jncif = 3*(numAnt*(numAnt-1))/2;
  if (avgpol) jncs  = jncif;
  else jncs  = jncif * numIF;
  
  /* Numbers of poln and IFs in accumulator */
  if (avgpol) mPol = 1;
  else mPol = numPol;
  if (avgif) mIF = 1;
  else mIF = numIF;

  maxAllow = 3*((numAnt*(numAnt-1))/2)*mPol*mIF; /* DEBUG */
  
  /* Baseline lookup table */
  blLookup = g_malloc0(numAnt*sizeof(gint));
  blLookup[0] = 0;
  for (i=1; i<numAnt; i++) blLookup[i] = blLookup[i-1] + numAnt-i;
  
  /* Fill in antenna numbers in ant* arrays - assume a1<a2 */
  jBL = 0;
  for (i=0; i<numAnt; i++) {
    for (j=i+1; j<numAnt; j++) {
      ant1[jBL] = i+1;
      ant2[jBL] = j+1;
      jBL++;
    }
  }
  
  /* Zero accumulations */
  numBL = (numAnt*(numAnt-1)) / 2;
  for (jBL=0; jBL<numBL; jBL++) {         /* Loop over baseline */
    for (jStok=0; jStok<mPol; jStok++) {  /* Loop over Pol */
      for (jIF=0; jIF<mIF; jIF++) {       /* Loop over IF */
	/* Accumulator index */
	accumIndex = jBL*3 + jStok*jncs + jIF*jncif;
	avgVis[accumIndex]   = 0.0;
	avgVis[accumIndex+1] = 0.0;
	avgVis[accumIndex+2] = 0.0;
      } /* end loop over IF */
    } /* end loop over Pol */
  } /* end loop over baseline */
  *sid  = -1;
  *fqid = -1;
  stime = -1.0e20;
  timeSum = 0.0;
  timeCount = 0;
  
  /* Loop reading and averaging data */
  while (!done) {
    
    /* Need to read? */
    if ((*nextVisBuf<0) || (*nextVisBuf>=inUV->myDesc->numVisBuff)) {
      retCode = ObitUVRead (inUV, NULL, err);
      if (err->error) Obit_traceback_val (err, routine, inUV->name, done);
      /* Finished? */
      if (retCode==OBIT_IO_EOF) {
	done = TRUE;
	*nextVisBuf = -999;
	break;
      }
      *nextVisBuf = 0;
    }

    /* Visibility pointer */
    visPnt = inUV->buffer + (*nextVisBuf) * inUV->myDesc->lrec;
    /* Vis data */
    ctime = visPnt[inUV->myDesc->iloct]; /* Time */
    if (stime<-1000.0) stime = ctime;  /* Set start time from first vis */
    if (inUV->myDesc->ilocsu>=0) csid  = (gint)visPnt[inUV->myDesc->ilocsu]; /* Source */
    else csid  = 0;
    if (*sid<0) *sid = csid;  /* Set output source id first vis */
    if (inUV->myDesc->ilocfq>=0) cfqid = (gint)visPnt[inUV->myDesc->ilocfq]; /* FQid */
    else cfqid  = 0;
    if (*fqid<0) *fqid = cfqid;  /* Set output fq id first vis */
    cbase = visPnt[inUV->myDesc->ilocb]; /* Baseline */
    
    /* Is this integration done? */
    if ((*sid!=csid) || (*fqid!=cfqid) || (ctime-stime>=linterv)) break;
    
    /* Sum time */
    timeSum += ctime;
    timeCount++;
    ltime = ctime;   /* Last time in accumulation */
    
    /* crack Baseline */
    a1 = (cbase / 256.0) + 0.001;
    a2 = (cbase - a1 * 256) + 0.001;

    /* DEBUG */
    if ((a1==0) || (a2==0)) {
      fprintf (stderr, "Fuckit all: %d \n",retCode);
    }
    /* Check that antenna numbers in range */
    Obit_retval_if_fail(((a1>0) && (a2>0) && (a1<=numAnt)&& (a2<=numAnt)), err, done,
			"%s: Bad antenna number, %d or %d not in [1,%ld] in %s", 
			routine, a1, a2, numAnt, inUV->name);

    /* Ignore autocorrelations */
    if (a1==a2) continue;
   
    /* Set extra weighting factors */
    weight = antwt[a1-1]*antwt[a2-1];
    
    /* Check baseline length - posibly modify weight */
    bl = sqrt (visPnt[inUV->myDesc->ilocu]*visPnt[inUV->myDesc->ilocu] + 
	       visPnt[inUV->myDesc->ilocv]*visPnt[inUV->myDesc->ilocv]);
    if ((bl<uvrang[0]) || (bl>uvrang[1])) weight *= wtuv;
    
    /* Baseline index this assumes a1<a2 always */
    blIndex =  blLookup[a1-1] + a2-a1-1;
    
    /* Accumulate */
    offset = 3*blIndex; /* Offset in accumulator */
    /* Loop over polarization */
    for (iStok = 0; iStok<numPol; iStok++) {
      jStok = iStok;      /* Stokes in accumulation */
      if (avgpol) jStok = 0;
      /* Loop over IF */
      for (iIF = 0; iIF<numIF; iIF++) {
	jIF = iIF;        /* IF in accumulation */
	if (avgif) jIF = 0;
	
	/* Loop over frequency */
	for (iFreq = 0; iFreq<numFreq; iFreq++) {

	  /* Visibity index */
	  visIndex = inUV->myDesc->nrparm + iStok*inUV->myDesc->incs + 
	    iIF*inUV->myDesc->incif + iFreq*inUV->myDesc->incf;
	  
	  /* Accumulator index */
	  accumIndex = offset + jStok*jncs + jIF*jncif;
	  
	  /* Accumulate */
	  if (visPnt[visIndex+2]>0.0) {
	    wt = weight * visPnt[visIndex+2];
	    /* maxShit = MAX (maxShit, accumIndex);  DEBUG */
	    /* DEBUGif (accumIndex>maxAllow) { 
	       fprintf (stderr,"bad accum %ld a1 %d a2 %d jStok %d jIF %d\n", 
	       accumIndex, a1, a2, jStok, jIF);
	       }  end DEBUG */
	    avgVis[accumIndex]   += wt * visPnt[visIndex];
	    avgVis[accumIndex+1] += wt * visPnt[visIndex+1];
	    avgVis[accumIndex+2] += wt;
	  } /* End vis valid */
	} /* Loop over Freq */
      } /* Loop over IF */
    } /* Loop over Stokes */
    
    (*nextVisBuf)++;  /* Increment vis being processed */
  } /* End loop summing */
  
  /* Normalize by sum of weights */
  for (jBL=0; jBL<numBL; jBL++) {         /* Loop over baseline */
    for (jStok=0; jStok<mPol; jStok++) {  /* Loop over Pol */
      for (jIF=0; jIF<mIF; jIF++) {       /* Loop over IF */
	/* Accumulator index */
	accumIndex = jBL*3 + jStok*jncs + jIF*jncif;
	if (avgVis[accumIndex+2]>0) {
	  avgVis[accumIndex]  /= avgVis[accumIndex+2];
	  avgVis[accumIndex+1]/= avgVis[accumIndex+2];
	}
      } /* end loop over IF */
    } /* end loop over Pol */
  } /* end loop over baseline */
  
  /* Average time and interval for output */
  if (timeCount>0) *timec = timeSum/timeCount;
  else *timec = 0.0;
  *timei = (ltime - stime);
  
  /* DEBUG
  fprintf (stderr,"max accumIndex %ld\n", maxShit); */

  /* Cleanup */
  if (blLookup) g_free(blLookup);
  return done;
} /* end NextAvg */

/**
 *   Determine antenna gains.
 *   Does least squares solutions for phase and optionally 
 *   amplitude.  Three methods are available, "normal", "L1" and 
 *   "amplitude constrained". 
 *      All frequencies in each IF and at all times are assumed to have 
 *   been averaged.  If avgif is true then the data is assumed to have 
 *   been averaged in frequency and the solution found for the first IF 
 *   is copied to all IFs. 
 *   If avgpol  is true then the data is assumed to have been averaged in 
 *   polarization  and the data is copied to the second 
 * Routine translated from the AIPSish UVUTIL.FOR/SLFPA
 * \param vobs    Array of observed data [3,baseline,IF,pol]
 * \param ant1    Array of first antenna numbers (1-rel) on a baseline
 * \param ant2    Array of second antenna numbers (1-rel) on a baseline
 * \param numAnt  Maximum antenna number (NOT number of antennas)
 * \param numIF   Maximum number of IFs 
 * \param numPol  Maximum number of polarizations
 * \param numif   Number of IFs 
 * \param numpol  Number of Poln. in initial data (in SN)
 * \param refant  Reference antenna to use.
 * \param avgif   If true average in IF 
 * \param avgpol  If true average in polarization (Stokes 'I')
 * \param dol1    If true, use L1 solution
 * \param mode    Solution mode; 0= full gain, 1=phase  2=phase(ignore amp), 
 *                3=full, constrain amplitude.
 * \param minno   Minimum number of antannas allowed 
 * \param snrmin  Minimum SNR allowed.
 * \param prtlv   Print level, .ge. 2 gives some print.
 * \param creal   [out] (ant,if,pol) Real part of solution
 * \param cimag   [out] (ant,if,pol) Imag part of solution 
 * \param cwt     [out] (ant,if,pol) Weights = SNR
 * \param refan   [out] (if,pol,)     Reference antennas used
 * \param gotant  [out] (ant) If true corresponding antenna has data.
 * \param gain    Work array (2,ant)
 * \param snr     Work array (ant)
 * \param count   Work array (ant)
 * \param err    Error/message stack, returns if error.
 */
static void 
doSolve (gfloat* vobs, glong *ant1, glong *ant2, glong numAnt, glong numIF, 
	 glong numPol, gint refant, gboolean avgif, 
	 gboolean avgpol, gboolean dol1, gint mode, gint minno, gfloat snrmin, 
	 gint prtlv, float* creal, gfloat* cimag, gfloat* cwt, gint* refan, 
	 gboolean* gotant, gfloat *gain, gfloat *snr, glong *count, ObitErr *err)
{
  glong iif, ist, iant, iBL, numBL, BLIndex, mPol, mIF;
  gint lprtlv, iref, ierr;
  gfloat amp, time=0.0, fblank =  ObitMagicF();
  /* No printout from CLBSNR */
  gfloat closer[2][2] = {{1.0e10,1.0e10},{1.0e10,1.0e10}};
  gchar prtsou[17] = "                ";
  
  /* Error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;  /* previous error? */
  
  /* Initialize output */
  /* (Loop) over Stokes' type */
  for (ist=0; ist<numPol; ist++) {
    /* Loop over IF */
    for (iif=0; iif<numIF; iif++) {
      refan[iif+ist*numIF] = 0;
      for (iant=0; iant<numAnt-1; iant++) {
	creal[iant+(iif+ist*numIF)*numAnt] = fblank;
	cimag[iant+(iif+ist*numIF)*numAnt] = fblank;
	cwt[iant+(iif+ist*numIF)*numAnt]   = 0.0;
      } /* end antenna loop */
    } /* end IF loop */
  } /* End stokes loop */
  /* No antennas found yet */
  for (iant=0; iant<numAnt; iant++) gotant[iant] = FALSE;
  
  numBL = (numAnt*(numAnt-1)) / 2;  /* Number of baselines */
  
  /* (Loop) over Stokes' type */
  BLIndex = 0;
  mPol = numPol;
  if (avgpol) mPol = 1;
  mIF  = numIF;
  if (avgif)  mIF  = 1;
  for (ist=0; ist<mPol; ist++) { /* loop 600 */
    /* Loop over IF */
    for (iif=0; iif<mIF; iif++) { /* loop 500 */
      /* Check if antennas have any data */
      for (iBL=0; iBL<numBL; iBL++) {
	if (vobs[BLIndex+iBL*3+2]>0.0) {
	  gotant[ant1[iBL]-1] = TRUE;
	  gotant[ant2[iBL]-1] = TRUE;	  
	}
      }
      iref = refant;
      
      /* Do solution */
      
      if (dol1) {/* L1 solution */
	gainCalcL1 (&vobs[BLIndex], ant1, ant2, numBL, numAnt,
		    refant, mode, minno, gain, &iref, prtlv, &ierr, err);
	
      } else { /* Normal */
	gainCalc (&vobs[BLIndex], ant1, ant2, numBL, numAnt,
		  refant, mode, minno, gain, &iref, prtlv, &ierr, err);
      } 
      
      if (ierr != 0) { /* Solution failed */
	for (iant=0; iant<numAnt; iant++) cwt[iant+(iif+ist*numIF)*numAnt] = -1.0;
	continue;
      } 
      
      /* Convert amplitude to 1/amp * to correct data. */
      for (iant=0; iant<numAnt; iant++) { /* loop 110 */
	amp = gain[iant*2]*gain[iant*2] + gain[iant*2+1]*gain[iant*2+1];
	if (amp < 1.0e-20) amp = 1.0;
	gain[iant*2]   = gain[iant*2]   / amp;
	gain[iant*2+1] = gain[iant*2+1] / amp;
      } /* end loop  L110: */;
      
      /* Compute SNRs */
      lprtlv = prtlv;
      if (lprtlv > 0)  lprtlv += 3;
      calcSNR (&vobs[BLIndex], ant1, ant2, numBL, 
	       numAnt, gain, snr, closer, snrmin, time, iif, ist, count, 
	       lprtlv, prtsou, err);
      
      /* Save results */
      for (iant=0; iant<numAnt; iant++) { /* loop 150 */
	if (snr[iant] > snrmin) {
	  creal[iant+(iif+ist*numIF)*numAnt] = gain[iant*2];
	  cimag[iant+(iif+ist*numIF)*numAnt] = gain[iant*2+1];
	  cwt[iant+(iif+ist*numIF)*numAnt]   = snr[iant];
	} 
      } /* end loop  L150: */;
      refan[iif+ist*numIF] = iref;  /* Actual reference antenna */
      
      /* Averaging in poln? */
      if (avgpol) {
	for (iant=0; iant<numAnt; iant++) { /* loop 200 */
	  creal[iant+(iif+(ist+1)*numIF)*numAnt] = creal[iant+(iif+ist*numIF)*numAnt];
	  cimag[iant+(iif+(ist+1)*numIF)*numAnt] = cimag[iant+(iif+ist*numIF)*numAnt];
	  cwt[iant+(iif+(ist+1)*numIF)*numAnt]   = cwt[iant+(iif+ist*numIF)*numAnt];
	} /* end loop  L200: */;
	refan[iif+numIF] = refan[iif];
      } 
      BLIndex += (+numBL)*3;  /* Index into data array for this IF/Poln */
    } /* end loop over IF  L500: */;
  } /* end loop over Stokes' type L600: */;

  /* If averaging in IF copy soln. */
  if (avgif) {
    for (iif= 1; iif<numIF; iif++) { /* loop 620 */
      for (iant=0; iant<numAnt; iant++) { /* loop 610 */
	creal[iant+(iif+ist*numIF)*numAnt] = creal[iant];
	cimag[iant+(iif+ist*numIF)*numAnt] = cimag[iant];
	cwt[iant+(iif+ist*numIF)*numAnt]   = cwt[iant];
	if (numPol>1) { /* Other poln */
	  creal[iant+(iif+numIF)*numAnt] = creal[iant+numIF*numAnt];
	  cimag[iant+(iif+numIF)*numAnt] = cimag[iant+numIF*numAnt];
	  cwt[iant+(iif+numIF)*numAnt]   = cwt[iant+numIF*numAnt];
	}
      } /* end loop  L610: */;
      refan[iif] = refan[0];
      if (numPol>1) refan[iif+numIF] = refan[numIF];
    } /* end loop  L620: */;
  } 
} /* end doSolve */

/**
 * calcSNR computes antenna based signal-to-noise ratioes (SNR) based  
 * on phase residuals from a gain model.  The approximation used is  
 * that SNR = 1.0 / RMS phase residual.  
 * If the values in closer are greater than 0 then any values exceeding 
 * these limits will be printed under control of prtlv  
 * Does a weighted solution for the SNR.  If insufficient data is  
 * present to compute an RMS but at least one observation exists, the  
 * SNR is set to 6.0.  
 * Routine translated from the AIPSish CLBSNR.FOR/CLBSNR
 * \param vobs    Normalized visibility [3,baseline,IF,pol]
 *                Zero value assumed invalid on input. 
 *                On return real part is replaced with the phase difference. 
 * \param ant1    Array of first antenna numbers. 
 * \param ant2    Array of second antenna numbers. 
 * \param numBL   Number of observations (baselines) 
 * \param numAnt  Number of antennas. 
 * \param gain    Antenna gains to be applied (real, imaginary) 
 * \param snr     [out] Signal to noise ratio for each antenna. 
 *                If an antenna is used in fewer than 3 baselines 
 *                the returned value is SNRMIN + 1.0 
 * \param closer  (i,j) If (j=1/2) amplitude/phase closure errors 
 *                exceed these limits on average (i=1) or individually (i=2) 
 *                they will be printed 
 * \param snrmin  Minimum SNR allowed. 
 * \param time    Time in days of data, used for labeling. 
 * \param iif     IF number used for labeling only.
 * \param ist     Stokes parameter of soln. 1-5 => R,L,R,L,I. 
 * \param count   A work array used for the counts for each 
 *                antenna, must be at least MAXANT in size. 
 * \param prtlv   Print level: 0 none, 1 statistics of failures, 
 *                2 individual failures, 3 the antenna SNRs 
 * \param prtsou  Current source name. 
 * \param err     Error/message stack, returns if error.
 */
static void 
calcSNR (gfloat* vobs, glong *ant1, glong *ant2, glong numBL, 
	 glong numAnt, gfloat* gain, gfloat* snr, gfloat closer[2][2], 
	 gfloat snrmin, gdouble time, gint iif, gint ist, glong* count, 
	 gint prtlv, gchar* prtsou, ObitErr *err) 
{
  gchar pol[5][5] = {"Rpol","Lpol","Rpol","Lpol","Ipol"};
  gint   loop, ii, jj, nprt, id, ih, im, ls, blprt[3][3], ne;
  gboolean   doclos, msgdun, docls1, docls2;
  gfloat      zr, zi, zzr, zzi, prtsnr, tmtemp, phsq, ae, pe, blrprt[3];
  gfloat *sumwt=NULL;
  gint   *error=NULL;
  gchar msgtxt[81];
  
  /* Error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;  /* previous error? */
  
  /* What messages are wanted? */
  docls1 = (closer[0][0] * closer[1][0] > 1.0e-20)  &&  
    (closer[0][0] * closer[1][0] < 1.0e20)  &&  (prtlv > 0);
  docls2 = ((closer[0][1] * closer[1][1] > 1.0e-20)  &&  
	    (closer[0][1] * closer[1][1] < 1.0e20))  &&  (prtlv > 0);
  doclos = (docls1)  ||  (docls2);
  msgdun = FALSE;
  
  /* Label for any closure errors: */
  if (doclos) {
    id = time;
    tmtemp = (time - id) * 24.0;
    ih = tmtemp;
    tmtemp = (tmtemp - ih) * 60.0;
    im = tmtemp;
    tmtemp = (tmtemp - im) * 60.0;
    ls = tmtemp;
    if (ls == 60) {
      ls = 0;
      im = im + 1;
      if (im == 60)  {
	im = 0;
	ih = ih + 1;
	if (ih == 24) {
	  ih = 0;
	  id = id + 1;
	} 
      } 
    } 
    g_snprintf (msgtxt,80,"closure errors at %3d/%2.2d:%2.2d:%2.2d %s IF %3d %s", 
		id, ih, im, ls, prtsou, iif, pol[ist-1]);
  } 

  /* Create work arrays */
  sumwt = g_malloc0(numAnt*sizeof(gfloat));
  error = g_malloc0(numAnt*sizeof(gint));
 
  /* Zero sums, counts etc. */
  nprt = 0;
  for (loop=0; loop<numAnt; loop++) { /* loop 10 */
    snr[loop]   = 1.0e-6;
    sumwt[loop] = 0.0;
    count[loop] = 0;
    error[loop] = 0;
  } /* end loop  L10:  */
  
  /* Determine phase residuals. */
  ne = 0;
  ae = 0.0;
  pe = 0.0;
  for (loop=0; loop<numBL; loop++) { /* loop 30 */
    if (vobs[loop*3+2]>1.0e-20) {
      ii = ant1[loop]-1;
      jj = ant2[loop]-1;
      zr = gain[ii*2] * gain[jj*2] + gain[ii*2+1] * gain[jj*2+1];
      zi = gain[ii*2] * gain[jj*2+1] - gain[ii*2+1] * gain[jj*2];
      zzr = vobs[loop*3] * zr - vobs[loop*3+1] * zi;
      zzi = vobs[loop*3] * zi + vobs[loop*3+1] * zr;
      vobs[loop*3+1] = sqrt (zzr*zzr + zzi*zzi);
      if (vobs[loop*3+1]*vobs[loop*3+2] > 1.0e-20) {
	vobs[loop*3] = atan2 (zzi, zzr);
	if (docls1) {
	  ne = ne + 1;
	  pe = pe + fabs (vobs[loop*3]);
	  ae = ae + fabs (log10 (fabs (vobs[loop*3+1]+1.e-20)));
	} 
      } 
    }
  } /* end loop  L30:  */
  
  /* Statistical failure */
  if ((docls1)  &&  (ne > 0)) {
    pe = pe / ne;
    ae = ae / ne;
    ae = pow (10.0, ae) - 1.0;
    /* print header, message */
    if ((ae > closer[0][0])  ||  (pe > closer[1][0])) {
      Obit_log_error(err, OBIT_InfoErr, "%s", msgtxt);
      msgdun = TRUE;
      ae = ae * 100.0;
      pe = pe * 57.296;
      Obit_log_error(err, OBIT_InfoErr, "Average closure error %10.3f %8.2f d", 
		     ae, pe);
    } 
  } 
  
  /* Sum square residuals */
  ne = 0;
  for (loop=0; loop<numBL; loop++) { /* loop 50 */
    if (vobs[loop*3+1]*vobs[loop*3+2] >= 1.0e-20) {
      phsq = vobs[loop*3] * vobs[loop*3] * vobs[loop*3+2];
      ii = ant1[loop]-1;
      count[ii]++;
      snr[ii] += phsq;
      sumwt[ii] += vobs[loop*3+2];
      jj = ant2[loop]-1;
      count[jj]++;
      snr[jj] += phsq;
      sumwt[jj] += vobs[loop*3+2];
      
      /* check closure error */
      if (docls2) {
	pe = fabs (vobs[loop*3]);
	ae = vobs[loop*3+1];
	if ((ae < 1.0)  &&  (ae > 0.0)) ae = 1.0 / ae;
	ae = ae - 1.0;
	if ((ae > closer[0][1])  ||  (pe > closer[1][1])) {
	  error[ii]++;
	  error[jj]++;
	  ne = ne + 1;
	  /* individual messages */
	  if (prtlv >= 2) {
	    /* Print header message */
	    if (!msgdun) {
              Obit_log_error(err, OBIT_InfoErr, "%s", msgtxt);
	      msgdun = TRUE;
	    } 
	    /* Flush buffer if full */
	    if (nprt >= 3) {
              Obit_log_error(err, OBIT_InfoErr, 
		   "%4.2d-%2.2d %7.1f %5d %4.2d-%2.2d %7.1f %5d %4.2d-%2.2d %7.1f %5d ", 
			     blprt[0][0], blprt[0][1],  blrprt[0], blprt[0][2],
			     blprt[1][0], blprt[1][1],  blrprt[1], blprt[1][2],
			     blprt[2][0], blprt[2][1],  blrprt[1], blprt[3][2]);
	      nprt = 0;
	    } 
	    /* New entry */
	    blprt[nprt][0] = ii;
	    blprt[nprt][1] = jj;
	    blprt[nprt][2] = pe * 57.296 + 0.5;
	    blrprt[nprt] = MAX (-9999., MIN (9999., ae*100.));
	    nprt = nprt + 1;
	  } 
	} 
      } 
    } 
  } /* end loop  L50:  */;
  
  /* Flush closure message buffer */
  if (nprt > 0) {
    Obit_log_error(err, OBIT_InfoErr, 
		   "%4.2d-%2.2d %7.1f %5d %4.2d-%2.2d %7.1f %5d %4.2d-%2.2d %7.1f %5d ", 
		   blprt[0][0], blprt[0][1],  blrprt[0], blprt[0][2],
		   blprt[1][0], blprt[1][1],  blrprt[1], blprt[1][2],
		   blprt[2][0], blprt[2][1],  blrprt[1], blprt[3][2]);
    nprt = 0;
  } 
  
  /* summary by antenna */
  if (ne > 0) {
    if (!msgdun) {
      Obit_log_error(err, OBIT_InfoErr, "%s", msgtxt);
      msgdun = TRUE;
    } 
    for (loop=0; loop<numAnt; loop++) { /* loop 60 */
      if (error[loop] > 0) {
	Obit_log_error(err, OBIT_InfoErr, "Antenna %2d had %6d excess closure errors", 
		       loop+1, error[loop]);
      } 
    } /* end loop  L60:  */;
  } 

  /* Convert to SNRs */
  for (loop=0; loop<numAnt; loop++) { /* loop 70 */
    if (count[loop] <= 0) snr[loop] = 0.0;
    /* For insufficient data (but  some) use SNRMIN + 1.0 */
    if ((count[loop] >= 1)  &&  (count[loop] <= 2)) snr[loop] = snrmin + 1.0;
    if ((count[loop] >= 3)  &&  (snr[loop] > 1.0e-20)) 
      snr[loop] = 1.0 / sqrt (snr[loop] / ((count[loop]-1) * sumwt[loop]));
    
    /* Print result if desired. */
    if ((count[loop] >= 1)  &&  (prtlv >= 3)) {
      prtsnr = MIN (9999.999, snr[loop]);
      Obit_log_error(err, OBIT_InfoErr, "antenna(%2d) %3ld obs, snr = %10.3f", 
		     loop+1, count[loop], prtsnr);
    }
  } /* end loop  L70:  */;

  /* Cleanup */
  if (sumwt) g_free(sumwt);
  if (error) g_free(error);
} /* end of routine calcSNR */ 

/**
 * Computes antenna gains given visibilities divided by the  
 * model values.  
 * Routine translated from the AIPSish GCALC.FOR/GCALC  
 * \param vobs    Complex normalized visibility. 
 * \param is      Array of first antenna numbers. 
 * \param js      Array of second antenna numbers. 
 * \param wt      Array of visibility weights. 
 * \param numBL   Number of observations. 
 * \param numAnt  Maximum antenna number (NOT number of antennas)
 * \param refant  Desired reference antenna. 
 * \param mode    Solution mode: 0 = full gain soln. 
 *                1 = phase only keep ampl. info. 
 *                2 = phase only discard ampl. info. 
 * \param minno   Min. number of antennas acceptable. 
 * \param g       Complex antenna gains to be applied. 
 * \param nref    Reference antenna used. 
 * \param prtlv   Print flag,    0=none, 1=soln, 2=data plus soln
 * \param ierr    Return error code 0 = OK, 1 = no valid data, 2=didn't converge, 
 *                3 = too few antennas
 * \param err     Error/message stack, returns if error.
 */
static void 
gainCalc (gfloat* vobs, glong *ant1, glong *ant2, glong numBL, glong numAnt,
	  gint refant, gint mode, gint minno, gfloat *g, gint* nref,
	  gint prtlv, gint *ierr, ObitErr* err) 
{
  gfloat gng[2], z[2], zr, zi, amp, gd, ph, qq, rms, s, sumwt, tol, w=0.0, x, xx, xxx, 
    yy, wx, arg, xfail, xfail1, xfail2;
  glong  i, ii, it, j, jj, k, nt, ntd, ntm1, itmax, nfail, iref;
  gboolean   convgd;
  gfloat *gn=NULL, *glast=NULL, *swt=NULL, tempR, tempI;
  gchar  msgtxt[81];

  /* Error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return ;  /* previous error? */

  itmax = 60;
  tol = 5.0e-6;
  nt = 0;
  ntd = 0;
  sumwt = 0.0;
  *ierr = 0;

  /* Any data? */
  if (numBL <= 0) {*ierr = 1; return; } 

  /* Create work arrays */
  gn    = g_malloc0(2*numAnt*sizeof(gfloat));
  glast = g_malloc0(2*numAnt*sizeof(gfloat));
  swt   = g_malloc0(numAnt*sizeof(gfloat));
  for (i=0; i<numAnt; i++) swt[i] = 0.0;

  /* Print data if nec. */
  if (prtlv >= 2) {
    for (k=0; k<numBL; k++) { /* loop 20 */
      if (vobs[k*3+2] > 1.0e-20) {
	amp = sqrt (vobs[k*3]*vobs[k*3] + vobs[k*3+1]*vobs[k*3+1]);
	ph = 57.296 * atan2 (vobs[k*3+1], vobs[k*3]+1.0e-20);
	g_snprintf (msgtxt,80,"%4ld amp =%12.3e phase =%7.2f %3ld %3ld %12.3e", 
		    k+1, amp, ph, ant1[k], ant2[k], vobs[k*3+2]);
	Obit_log_error(err, OBIT_InfoErr, msgtxt);
      } 
    } /* end loop  L20:  */;
  } /* end if print */
 
  /* Find which antennas have data  and normalize to unit amplitude if requested. */
  for (k=0; k<numBL; k++) { /* loop 40 */
    i = ant1[k]-1;
    j = ant2[k]-1;
    nt = MAX (nt, ant2[k]);
    xxx = sqrt ((vobs[k*3]*vobs[k*3]) + (vobs[k*3+1]*vobs[k*3+1]));
    if (xxx <= 1.0e-20) vobs[k*3+2] = 0.0;
    if (vobs[k*3+2] > 1.0e-20) {
      swt[i] += vobs[k*3+2];
      swt[j] += vobs[k*3+2];
      sumwt  += vobs[k*3+2];
      if (mode == 2) {  /* normalize visibility */
	vobs[k*3]   /=  xxx;
	vobs[k*3+1] /=  xxx;
      } 
    } 
  } /* end loop  L40:  */;

  /* Initialize solutions */
  for (i=0; i<numAnt; i++) { /* loop 50 */
    g[i*2]       = 1.0;
    g[i*2+1]     = 0.0;
    glast[i*2]   = 1.0;
    glast[i*2+1] = 0.0;
    if (swt[i] > 1.0e-20) ntd++;
  } /* end loop  L50:  */;

  /* Any data? */
  if (sumwt <= 1.0e-20) {
    *ierr = 1; 
    if (gn)    g_free(gn);   /* Cleanup */
    if (glast) g_free(glast);
    if (swt)   g_free(swt);
    return; 
  } 

  /* Pick reference ant. if the input one not usable */
  (*nref) = refant;
  if (((*nref < 1)  ||  (*nref > numAnt)  ||  (swt[(*nref)-1] == 0.0))) { /* goto L100;*/
    /* Find antenna with highest summed weight */
    xx = 0.0;
    for (i=0; i<nt; i++) { /* loop 90 */
      if (swt[i] > xx) { /*goto L90;*/
	xx = swt[i];
	*nref = i+1;
      }
    } /* end loop  L90:  */;
  } /* L100: */

  ntm1 = nt - 1;
  /* Too few antennas? */
  if (ntd < minno) {
    *ierr = 3; 
    if (gn)    g_free(gn);   /* Cleanup */
    if (glast) g_free(glast);
    if (swt)   g_free(swt);
    return; 
  } 

  /* Print statistics */
  if (prtlv >= 2) {
    /* Sum chi squares */
    s = 0.0;
    for (k=0; k<numBL; k++) { /* loop 160 */
      if (vobs[k*3+2]>0.0) {
	ii = ant1[k];
	jj = ant2[k];
	z[0] = g[jj*2];
	z[1] = - g[jj*2+1];
	zr = g[ii*2] * z[0] - g[ii*2+1] * z[1];
	zi = g[ii*2] * z[1] + g[ii*2+1] * z[0];
	z[0] = vobs[k*3]   - zr;
	z[1] = vobs[k*3+1] - zi;
	qq = z[0] * z[0] + z[1] * z[1];
	s = s + vobs[k*3+2] * qq;
      }
    } /* end loop  L160: */;
    rms = sqrt (s/sumwt);
    it = 0;
    g_snprintf (msgtxt, 80, "iter=%5ld s=%15.5e rms=%15.5e", it, s, rms);
    Obit_log_error(err, OBIT_InfoErr, msgtxt);
  } /* end print */
 
  /* Begin solution iteration */
  for (it=1; it<=itmax; it++) { /* loop 310 */
    if (it > 15) tol = (it - 10) * 1.0e-6;

    /* Full amp and phase solution */
    if (mode <= 0) { /* goto L230;*/
      /* Following for amplitude and phase solution. */
      if (it == 1) w = 0.5;
      if (it == 2) w = 0.75;
      if (it > 2)  w = 0.9;
      if (it > 10) w = 0.5;
      if (ntd <= 6) w = 0.25;
      for (i=0; i<nt; i++) { /* loop over antennas 220 */
	if (swt[i] == 0.0) continue; /* no valid data?  goto L220;*/

	gng[0] = 0.0;  /* zero accumulators */
	gng[1] = 0.0;
	gd = 0.0;

	/* Loop over data looking for antenna i */
	for (k=0; k<numBL; k++) {
	  if (vobs[k*3+2]<0.0) continue;
	  if ((i+1) == ant1[k]) {
	    /* i as first antenna */
	    jj = ant2[k]-1;
	    z[0] = g[jj*2];
	    z[1] = g[jj*2+1];
	    qq = z[0] * z[0] + z[1] * z[1];
	    gd     += vobs[k*3+2] * qq;
	    gng[0] += vobs[k*3+2] * (g[jj*2] * vobs[k*3]   - g[jj*2+1] * vobs[k*3+1]);
	    gng[1] += vobs[k*3+2] * (g[jj*2] * vobs[k*3+1] + g[jj*2+1] * vobs[k*3]);

	    /* i as second antenna: */
	  } else if ((i+1) == ant2[k]) {
	    ii = ant1[k]-1;
	    z[0] = g[ii*2];
	    z[1] = g[ii*2+1];
	    qq = z[0] * z[0] + z[1] * z[1];
	    gd     += vobs[k*3+2] * qq;
	    gng[0] += vobs[k*3+2] * (g[ii*2]   * vobs[k*3] + g[ii*2+1] * vobs[k*3+1]);
	    gng[1] += vobs[k*3+2] * (g[ii*2+1] * vobs[k*3] - g[ii*2]   * vobs[k*3+1]);
	  }
	} /* end loop of data */

	/* corrections to gain for antenna i */
	g[i*2]   += w * (gng[0] / gd - g[i*2]);
	g[i*2+1] += w * (gng[1] / gd - g[i*2+1]);
      } /* end loop over data  L220: */

      /* End amplitude and phase */

    } else { /* Phase only L230 */
      /* Following for phase only. */
      for (i=0; i<nt; i++) { /* loop 240 */
	gn[i*2]   = 0.0;
	gn[i*2+1] = 0.0;
      } /* end loop  L240: */;

      /* Damping factor */
      w = .8;
      if (ntd <= 6) w = .25;
      for (k=0; k<numBL; k++) { /* loop over data 250 */
	if (vobs[k*3+2] > 0.0) {
	  ii = ant1[k]-1;
	  jj = ant2[k]-1;
	  /*               Z = G(II) * CONJG (G(JJ)) * CONJG (WORK(K)) */
	  zr = (g[ii*2]   * g[jj*2] + g[ii*2+1] * g[jj*2+1]);
	  zi = (g[ii*2+1] * g[jj*2] - g[ii*2]   * g[jj*2+1]);
	  /*               GN(II) = GN(II) + Z */
	  tempR = vobs[k*3+2] * (zr * vobs[k*3] + zi * vobs[k*3+1]);
	  tempI = vobs[k*3+2] * (zi * vobs[k*3] - zr * vobs[k*3+1]);
	  gn[ii*2]   += tempR;
	  gn[ii*2+1] += tempI;
	  /*               GN(JJ) = GN(JJ) + CONJG (Z) */
	  gn[jj*2]   += tempR;
	  gn[jj*2+1] -= tempI;
	} /* end of if valid */
      } /* end loop  L250: */

      /* Update solutions */
      for (i=0; i<nt; i++) { /* loop over antennas 260 */
	wx = w;
	if (swt[i] <= 0.0) wx = 0.0;
	/*               XX = ATAN2 (AIMAG (G(I)), REAL (G(I))) */
	xx = atan2 (g[i*2+1], g[i*2]);
	/*               YY = ATAN2 (AIMAG (GN(I)), REAL (GN(I))) */
	yy = atan2 (gn[i*2+1], gn[i*2]+1.0e-30);
	arg = xx - wx * yy;
	g[i*2]   = cos (arg);
	g[i*2+1] = sin (arg);
      } /* end loop  L260: */;
    } /* end phase only */

    /* Convergence test */
    convgd = TRUE; /* L265: */
    nfail = 0;
    xfail = 0.0;
    for (i=0; i<nt; i++) { /* loop 280 */
      if ((swt[i] > 0.0) && convgd) { /*goto L275;*/
      /* Use different criteria before and after  30 iterations */
	if (it <= 30) { /* before 30 goto L270;*/
	  x = tol * (5.0e-7 + sqrt (g[i*2]*g[i*2] + g[i*2+1]*g[i*2+1]));
	  if (fabs (g[i*2]  -glast[i*2])   > x) convgd = FALSE;
	  if (fabs (g[i*2+1]-glast[i*2+1]) > x) convgd = FALSE;

	} else { /* after 30  L270: */
	  xfail1 = fabs (g[i*2]   - glast[i*2]);
	  xfail2 = fabs (g[i*2+1] - glast[i*2+1]);
	  xfail1 = MAX (xfail1, xfail2);
	  x = tol * (5.0e-7 + sqrt (g[i*2]*g[i*2]+g[i*2+1]*g[i*2+1]));
	  if (xfail1 > x) { /*goto L275;*/
	    convgd = FALSE;
	    nfail = nfail + 1;
	    if ((xfail1/x) >= xfail) { /* goto L275; */
	      xfail = xfail1 / x;
	    }
	  }
	}
      } /* end if got data and still "converged" L275: */

      /* Save as last solution */
      glast[i*2]   = g[i*2];
      glast[i*2+1] = g[i*2+1];
    } /* end loop  L280: */;

    if (prtlv >= 2) {
      /* Print statistics */
      s = 0.0;
      for (k=0; k<numBL; k++) { /* loop 290 */
	if (vobs[k*3+2]>0) {
	  ii = ant1[k]-1;
	  jj = ant2[k]-1;
	  z[0] = g[jj*2];
	  z[1] = - g[jj*2+1];
	  zr = g[ii*2] * z[0] - g[ii*2+1] * z[1];
	  zi = g[ii*2] * z[1] + g[ii*2+1] * z[0];
	  z[0] = vobs[k*3] - zr;
	  z[1] = vobs[k*3+1] - zi;
	  qq = z[0] * z[0] + z[1] * z[1];
	  s += vobs[k*3+2] * qq;
	}
      } /* end loop  L290: */;
      rms = sqrt (s/sumwt);
      g_snprintf (msgtxt,80,"iter=%5ld s=%15.5e rms=%15.5e", it, s, rms);
      Obit_log_error(err, OBIT_InfoErr, msgtxt);
    } /* end print statistics */ 

    if (convgd) break;   /* Converged?  goto L400;*/
    if ((it > 30) && (xfail <= 10.0)) break;   /* Close enough goto L400; */
    if (it >= itmax) { /* goto L310; */
      /* Didn't converge */
      *ierr = 2;
      if (gn)    g_free(gn);   /* Cleanup */
      if (glast) g_free(glast);
      if (swt)   g_free(swt);
      return;
    }
  }    /* End of iteration loop */

  /* Refer solutions to reference antenna. */
  iref = *nref-1;
  xxx = 1.0 / sqrt (g[iref*2]*g[iref*2] + g[iref*2+1]*g[iref*2+1]);
  for (i=0; i<nt; i++) { /* loop 600 */
    if ((i != iref)  &&  (swt[i] > 1.0e-20)) {
      /*            G(I) = G(I) / G(NREF) * CABS (G(NREF)) */
      zr = g[i*2]   * g[iref*2] + g[i*2+1] * g[iref*2+1];
      zi = g[i*2+1] * g[iref*2] - g[i*2]   * g[iref*2+1];
      g[i*2]   = xxx * zr;
      g[i*2+1] = xxx * zi;
    } 
  } /* end loop  L600: */;

  /* adjust reference antenna gain */
  g[iref*2] = sqrt (g[iref*2]*g[iref*2] + g[iref*2+1]*g[iref*2+1]);
  g[iref*2+1] = 0.0;

  if (prtlv >= 1) {
    for (i=0; i<nt; i++) { /* loop 610 */
      if (swt[i] > 1.0e-20) {
	/* Print results. */
	amp = sqrt (g[i*2]*g[i*2] + g[i*2+1]*g[i*2+1]);
	ph = 57.2958 * atan2 (g[i*2+1], g[i*2]);
	g_snprintf (msgtxt,80,"ant= %5ld amp=%12.5f phase=%12.2f", 
		    i+1, amp, ph);
	Obit_log_error(err, OBIT_InfoErr, msgtxt);
      } 
    } /* end loop  L610: */;
  } /* end of print */ 

  *ierr = 0; /* ok */
  /* Cleanup */
  if (gn)    g_free(gn); 
  if (glast) g_free(glast);
  if (swt)   g_free(swt);
} /* end of routine gainCalc */ 


/**
 * Does an L1 type gain solution similar to gainCalc.  
 * Routine translated from the AIPSish GCALC1.FOR/GCALC1  
 * \param vobs    Complex normalized visibility. 
 * \param is      Array of first antenna numbers. 
 * \param js      Array of second antenna numbers. 
 * \param wt      Array of visibility weights. 
 * \param numBL   Number of observations. 
 * \param numAnt  Maximum antenna number (NOT number of antennas)
 * \param refant  Desired reference antenna. 
 * \param mode    Solution mode: 0 = full gain soln. 
 *                1 = phase only keep ampl. info. 
 *                2 = phase only discard ampl. info. 
 * \param minno   Min. number of antennas acceptable. 
 * \param g       Complex antenna gains to be applied. 
 * \param nref    Reference antenna used. 
 * \param prtlv   Print flag,    0=none, 1=soln, 2=data plus soln
 * \param ierr    Return error code 0 = OK, 1 = no valid data, 3 = too few antennas
 * \param err     Error/message stack, returns if error.
 */
static void 
gainCalcL1 (gfloat* vobs, glong *ant1, glong *ant2, glong numBL, 
	    glong numAnt, gint refant, gint mode, gint minno, gfloat *g, gint* nref, 
	    gint prtlv, gint *ierr, ObitErr* err) 
{
  glong   k, nt, ntd, ie, ne, it, itmax, i, ii, j, jj, iref;
  gfloat  amp, ph, xlamb, pterm;
  gdouble gd, t, eps, f, tol, s, qq, w=0.0, xx, yy, x, xrr, xrd, xrz, xrtemp, xrgng, 
    xir, xid, xiz, xitemp, xigng, d1, d2, f1, f2, rmean, t1, t2, sumwt, xxx;
  gboolean   convgd;
  gfloat  epss[3] = {5.0e-3, 5.0e-4, 5.0e-5};
  gfloat  *swt=NULL, *p1=NULL, *p2=NULL;
  gdouble *xrdg=NULL, *xidg=NULL, *xrglas=NULL, *xiglas=NULL;
  gchar msgtxt[81];

  /* Error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;  /* previous error? */

  /* Anything to do? */
  if (numBL <= 0) {*ierr=1; return;}

  /* Create work arrays */
  swt    = g_malloc0(numAnt*sizeof(gfloat));
  p1     = g_malloc0(numAnt*sizeof(gfloat));
  p2     = g_malloc0(numAnt*sizeof(gfloat));
  xrdg   = g_malloc0(numAnt*sizeof(gdouble));
  xidg   = g_malloc0(numAnt*sizeof(gdouble));
  xrglas = g_malloc0(numAnt*sizeof(gdouble));
  xiglas = g_malloc0(numAnt*sizeof(gdouble));

  /* Init values */
  xlamb = 0.0;
  pterm = 0.0;
  ne = 3;
  itmax = 60;
  tol = 5.0e-5;
  nt = 0;
  ntd = 0;
  sumwt = 0.0;
  for (i= 1; i<=numAnt; i++) { /* loop 10 */
    swt[i-1] = 0.;
  } /* end loop  L10:  */;

  /* Find which antennas have data and normalize to unit amplitude if requested. */
  for (k=0; k<numBL; k++) { /* loop 40 */
    /* Data dump if requested */
    if ((prtlv >= 2)  &&  (vobs[k*3+2] > 1.0e-20)) {
      g_snprintf (msgtxt,80,"%4ld amp =%12.3e phase =%7.3f %3ld %3ld %12.3e", 
		  k+1, vobs[k*3], vobs[k*3+1], ant1[k], ant2[k], vobs[k*3+2]);
      Obit_log_error(err, OBIT_InfoErr, msgtxt);
    } 
    if (vobs[k*3+2] > 0.0) {
      i = ant1[k]-1;
      j = ant2[k]-1;
      nt = MAX (nt, ant2[k]);
      xxx = sqrt ((vobs[k*3]*vobs[k*3]) + (vobs[k*3+1]*vobs[k*3+1]));
      if (xxx > 1.0e-20) {
	swt[i] += vobs[k*3+2];
	swt[j] += vobs[k*3+2];
	sumwt  += vobs[k*3+2];
	/* Normalize by amplitude if only solving for phase without ampl. */
	if (mode == 2) {
	  vobs[k*3]   /= xxx;
	  vobs[k*3+1] /= xxx;
	} 
      }
    } 
  } /* end loop  L40:  */

  /* Initialize solutions */
  for (i=0; i<numAnt; i++) { /* loop 50 */
    g[i*2]    = 1.0;
    g[i*2+1]  = 0.0;
    xrdg[i]   = 1.0;
    xidg[i]   = 0.0;
    xrglas[i] = 1.0;
    xiglas[i] = 0.0;
    if (swt[i] > 0.0) ntd++;
  } /* end loop  L50:  */;

  /* Any data found? */
  if (sumwt == 0.0) {
    /* No data */
    *ierr = 1;
    g_free(swt); g_free(p1); g_free(p2);  /* Cleanup */
    g_free(xrdg); g_free(xidg); g_free(xrglas); g_free(xiglas);
    return;
  }

  /* Pick reference ant. if the input one not usable */
  (*nref) = refant;
  if (((*nref < 1)  ||  (*nref > numAnt)  ||  (swt[(*nref)-1] <= 0.0))) { /* goto L100;*/
    /* no good, find antenna with highest summed weight */
    xx = 0.0;
    for (i=0; i<nt; i++) { /* loop 90 */
      if (swt[i] > xx) { /*goto L90;*/
	xx = swt[i];
	*nref = i+1;
      }
    } /* end loop  L90:  */;
  } /* L100: */

  if (ntd < minno) { /* goto L930;*/
    /* Too few antennas */
    *ierr = 3;
    g_free(swt); g_free(p1); g_free(p2); /* Cleanup */
    g_free(xrdg); g_free(xidg); g_free(xrglas); g_free(xiglas);
    return;
  }

  /* L1 outer loop */
  for (ie= 1; ie<=ne; ie++) { /* loop 320 */
    eps = epss[ie-1];
    /* Print rms residual if requested */
    if (prtlv >= 2) { /*goto L170;*/
      s = 0.0;
      for (k=0; k<numBL; k++) { /* loop 160 */
	if (vobs[k*3+2] > 0.0) { /*goto L160;*/
	  i = ant1[k]-1;
	  j = ant2[k]-1;
	  xrz = vobs[k*3]   - (xrdg[i] * xrdg[j] + xidg[i] * xidg[j]);
	  xiz = vobs[k*3+1] - (xrdg[j] * xidg[i] - xrdg[i] * xidg[j]);
	  qq = xrz * xrz + xiz * xiz;
	  s += vobs[k*3+2] * sqrt (qq + eps);
	} /* end of if data valid */
      } /* end loop  L160: */;
      rmean = s / sumwt;
      it = 0;
      g_snprintf (msgtxt,80,"iter=%5ld s=%15.5e rmean=%15.5e", it, s, rmean);
      Obit_log_error(err, OBIT_InfoErr, msgtxt);
    } /* end of if print */

    /* Inner Solution loop */
    for (it= 1; it<=itmax; it++) { /* loop  */
      if (it > 15) tol = (it-10) * 1.0e-5;
      if (mode<=0) {
	/* Section to solve for full  complex gains: */
	if (it == 1) w = 0.5;
	if (it == 2) w = 1.5;
	if (it == 3) w = 1.5;
	if (ntd <= 6) w = .25;
	for (i=0; i<nt; i++) { /* loop 220 */
	  if (swt[i] > 0.0) { /*goto L220;*/
	    xrgng = 0.0;
	    xigng = 0.0;
	    gd = 0.0;
	    for (k=0; k<numBL; k++) { /* loop 210 */
	      if (vobs[k*3+2] > 0.0) { /*goto L210; */
		ii = ant1[k]-1;
		jj = ant2[k]-1;
		if ((swt[ii] == 0.) || (swt[jj] == 0.)) continue; /* goto L210;*/
		if (i == ii) {
		  /* i as first antenna */
		  xrr = vobs[k*3]   - (xrdg[ii] * xrdg[jj] + xidg[ii] * xidg[jj]);
		  xir = vobs[k*3+1] - (xrdg[jj] * xidg[ii] - xrdg[ii] * xidg[jj]);
		  t = vobs[k*3+2] / sqrt (xrr * xrr + xir * xir + eps);
		  xrz = xrdg[jj];
		  xiz = xidg[jj];
		  gd    += t * (xrz * xrz + xiz * xiz);
		  xrgng += t * (xrdg[jj] * vobs[k*3]   - xidg[jj] * vobs[k*3+1]);
		  xigng += t * (xrdg[jj] * vobs[k*3+1] + vobs[k*3] * xidg[jj]);

		} else if (i == jj) {
		  /* i as second antenna: */
		  xrr = vobs[k*3]   - (xrdg[ii] * xrdg[jj] + xidg[ii] * xidg[jj]);
		  xir = vobs[k*3+1] - (xrdg[jj] * xidg[ii] - xrdg[ii] * xidg[jj]);
		  t = vobs[k*3+2] / sqrt (xrr * xrr + xir * xir + eps);
		  xrz = xrdg[ii];
		  xiz = xidg[ii];
		  gd    += t * (xrz * xrz + xiz * xiz);
		  xrgng += t * (xrdg[ii]  * vobs[k*3] + xidg[ii] * vobs[k*3+1]);
		  xigng += t * (vobs[k*3] * xidg[ii]  - xrdg[ii] * vobs[k*3+1]);
		} /* end i as second antenna */
	      } /* end if baseline valid data */
	    } /* end loop  L210: */;

	    xrdg[i] += w * (xrgng / gd - xrdg[i]);
	    xidg[i] += w * (xigng / gd - xidg[i]);
	  } /* end if antenna has valid data*/
	} /* end loop  L220: */;
	
      } else {
	/* Phase only solutions */
	for (i=0; i<nt; i++) { /* loop  240*/
	  p1[i] = 0.;
	  p2[i] = 0.;
	} /* end loop  L240: */;
	
	w = 0.5;        /* damping */
	if (it <= 3)  w = 0.25;
	if (ntd <= 6) w = 0.25;
	for (k=0; k<numBL; k++) { /* loop 250 */
	  if (vobs[k*3+2]>0.0) {
	    i = ant1[k]-1;
	    j = ant2[k]-1;
	    xrd = xrdg[i] * xrdg[j] + xidg[i] * xidg[j];
	    xid = xrdg[j] * xidg[i] - xrdg[i] * xidg[j];
	    xrr = vobs[k*3]   - xrd;
	    xir = vobs[k*3+1] - xid;
	    xrtemp = xrd;
	    xitemp = xid;
	    xrd = xrtemp    * vobs[k*3] + xitemp * vobs[k*3+1];
	    xid = vobs[k*3] * xitemp    - xrtemp * vobs[k*3+1];
	    qq = xrr * xrr + xir * xir;
	    f = qq + eps;
	    f1 = sqrt (f);
	    f2 = f * f1;
	    d1 = 2.0 * xid;
	    d2 = 2.0 * xrd;
	    t1 = 0.5 * vobs[k*3+2] / f1 * d1;
	    t2 = 0.5 * vobs[k*3+2] * (d2 / f1-.5 * (d1 * d1) / f2);
	    p1[i] += t1;
	    p1[j] -= t1;
	    p2[i] += t2;
	    p2[j] += t2;
	  } /* end if valid data */
	} /* end loop  L250: */

	/* Update solutions */
	for (i=0; i<nt; i++) { /* loop 260 */
	  if (swt[i] > 0.0) { /*goto L260;*/
	    xx = atan2 (xidg[i], xrdg[i]);
	    yy = atan2 (p1[i], p2[i]);
	    xrdg[i] = cos (xx - w * yy);
	    xidg[i] = sin (xx - w * yy);
	  } /* end if valid antenna data */
	} /* end loop  L260: */
      } /* end Phase only section */
	

      /* Convergence test */
      convgd = TRUE; /* L270: */
      for (i=0; i<nt; i++) { /* loop 280 */
	if ((swt[i] > 0.0)  &&  convgd) { /*goto L275;*/
	  x = tol * (5.0e-7 + sqrt (xrdg[i] * xrdg[i] + xidg[i] * xidg[i]));
	  if (fabs (xrdg[i]-xrglas[i]) > x) convgd = FALSE;
	  if (fabs (xidg[i]-xiglas[i]) > x) convgd = FALSE;
	} /* end if valid and still "converged" L275: */
	xrglas[i] = xrdg[i];
	xiglas[i] = xidg[i];
      } /* end loop  L280: */
      
      /* Print iteration value is requested */
      if (prtlv >= 2) { /*goto L300; */
	s = 0.0;
	for (k=0; k<numBL; k++) { /* loop 290 */
	  if (vobs[k*3+2] > 0.0) { /*goto L290;*/
	    i = ant1[k]-1;
	    j = ant2[k]-1;
	    xrz = vobs[k*3]   - (xrdg[i] * xrdg[j] + xidg[i] * xidg[j]);
	    xiz = vobs[k*3+1] - (xrdg[j] * xidg[i] - xrdg[i] * xidg[j]);
	    qq = xrz * xrz + xiz * xiz;
	    s += vobs[k*3+2] * sqrt (qq + eps);
	  } /* end if valid */
	} /* end loop  L290: */;
	rmean = s / sumwt;
	g_snprintf (msgtxt,80,"iter=%5ld s=%15.5e rmean=%15.5e", it, s, rmean);
	Obit_log_error(err, OBIT_InfoErr, msgtxt);
      } /* end print */

      /* Inner loop converged? */
      if (convgd) break;  /* goto L320; L300: */
      
    } /* end inner solution loop L310: */;
  } /* end outer solution loop  L320: */;

  /* Refer solutions to reference antenna. */
  iref = (*nref)-1;
  g[iref*2]   = xrdg[iref];
  g[iref*2+1] = xidg[iref];
  xx = 1.0 / sqrt (g[iref*2]*g[iref*2] + g[iref*2+1]*g[iref*2+1]);
  for (i=0; i<nt; i++) { /* loop 600 */
    g[i*2]  = xrdg[i];
    g[i*2+1] = xidg[i];
    if ((i != iref)  &&  (swt[i] > 0.0)) { /*goto L600;*/
      xrr = g[i*2]   * g[iref*2] + g[i*2+1] * g[iref*2+1];
      xir = g[i*2+1] * g[iref*2] - g[i*2]   * g[iref*2+1];
      g[i*2]   = xx * xrr;
      g[i*2+1] = xx * xir;
    } /* end if valid */
  } /* end loop  L600: */;

  /* Adjust reference solution */
  g[iref*2]   = sqrt (g[iref*2]*g[iref*2] + g[iref*2+1]*g[iref*2+1]);
  g[iref*2+1] = 0.0;
 
  /* Print final results if requested */
  if (prtlv >= 1) {
    for (i=0; i<nt; i++) { /* loop 610 */
      if (swt[i] > 0.0) { /*goto L610;*/
	amp = sqrt (g[i*2]*g[i*2] + g[i*2+1]*g[i*2+1]);
	ph = 57.2958 * atan2 (g[i*2+1], g[i*2]);
	g_snprintf (msgtxt,80,"ant= %5ld amp=%12.5f phase=%12.2f", 
		    i+1, amp, ph);
	Obit_log_error(err, OBIT_InfoErr, msgtxt);
      } /* end if valid */
    } /* end loop  L610: */;
  } /* end print */

  *ierr = 0;  /* Seems to have worked */

  /* Cleanup */
  if (swt)    g_free(swt);
  if (p1)     g_free(p1);
  if (p2)     g_free(p2);
  if (xrdg)   g_free(xrdg);
  if (xidg)   g_free(xidg);
  if (xrglas) g_free(xrglas);
  if (xiglas) g_free(xiglas);
} /* end of routine gainCalcL1 */ 

/**
 * Routine to determine antenna usage as the reference antenna in an  
 * open SN table.  All IFs are examined.  
 * All valid entries are included.  
 * Routine translated from the AIPSish REFCNT.FOR/REFCNT  
 * \param SNTab    SN table object 
 * \param isub     Subarray number, 0=>1 
 * \param numtime  [out] number of distinct times in table. 
 * \param err      Error/message stack, returns if error.
 * \return pointer to array of counts of usage as reference ant.  
 *         g_free when done.
 */
static gint *refCount (ObitTableSN *SNTab, gint isub, 
		       gint *numtime, ObitErr* err) 
{
  gint   *antuse=NULL;
  ObitIOCode retCode;
  gint   iif, iref, numif, numpol, numant=0,sub;
  glong  loop, count;
  gfloat lastTime;
  ObitTableSNRow *row=NULL;
  gchar *routine = "refCount";

  /* Error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return antuse;  /* previous error? */
  g_assert(ObitTableSNIsA(SNTab));

  /* create output array */
  antuse = g_malloc0(numant*sizeof(gint));

  /* Get descriptive info */
  numpol = SNTab->numPol;
  numif  = SNTab->numIF;
  numant = SNTab->numAnt;

  /* Subarray */
  sub = MAX (1, isub);
  lastTime = -1.0e20;
  count = 0;
  
  /* Create Row */
  row = newObitTableSNRow (SNTab);
  /* Loop through table */
  for (loop=1; loop<=SNTab->myDesc->nrow; loop++) { /* loop 20 */

    retCode = ObitTableSNReadRow (SNTab, loop, row, err);
    if (err->error) Obit_traceback_val (err, routine, SNTab->name, antuse);
    if (row->status<0) continue;  /* Skip deselected record */

    /* Count times */
    if (row->Time>lastTime) {
      lastTime = row->Time;
      count ++;
    }

    /* Right subarray? */
    if ((row->SubA!=sub) && (row->SubA>0)) continue;
    /* Loop over IFs */
    for (iif=0; iif<numif; iif++) { /* loop 10 */
      if (row->Weight1[iif] > 0.0) {
	iref =  row->RefAnt1[iif];
	if ((iref > 0)  &&  (iref <= numant)) antuse[iref-1]++;
      } 
      if ((numpol>1) && (row->Weight2[iif] > 0.0)) {
	iref =  row->RefAnt2[iif];
	if ((iref > 0)  &&  (iref <= numant)) antuse[iref-1]++;
      }
    } /* end loop  L10:  */
  } /* end loop  L20:  */

  row = ObitTableSNRowUnref(row); /* delete row object */

  /* How many times? */
  *numtime = count;
  return antuse;
} /* end of routine refCount */ 

/**
 * Routine to rereference phases in an a polarization coherent fashion;  
 * i.e. both polarizations must be present (if possible) in data used  
 * to determine the relative phase between the primary and secondary  
 * reference antennas.  This routine does one IF at a time but both  
 * polarizations (if present) are done.  
 * All valid entries are included.  
 * Note: reference antenna values in the table are modified.  
 * Routine translated from the AIPSish REFFAZ.FOR/REFFAZ  
 * \param SNTab    SN table object, should be opened and closed externally
 * \param isub     subarray number, 0=>1 
 * \param iif      0-rel IF number, 0=>1 
 * \param refa     primary reference antenna 
 * \param ant      secondary reference antenna 
 * \param mxtime   Dimension of work arrays 
 * \param wrktim   Work array 
 * \param work1    Work array 
 * \param work2    Work array 
 * \param work3    Work array 
 * \param work4    Work array 
 * \param work5    Work array 
 * \param err      Error/message stack, returns if error.
 */
static void 
refPhase (ObitTableSN *SNTab, gint isub, gint iif, gint refa, gint ant, 
	  gint mxtime, gfloat* wrktim, gfloat* work1, gfloat* work2, 
	  gfloat* work3, gfloat* work4, gfloat* work5, ObitErr* err)
{
  gint    numtim, numif, numpol, ipnt1, ipnt2, refa1, refa2, isuba, sa;
  glong   loop, numrec;
  gboolean   need2, dotwo, done;
  gfloat wt1=0.0, wt2, re1, re2, im1, im2, amp, tre, tim, smotim;
  gfloat fblank =  ObitMagicF();
  gdouble timoff=0.0, time, time1, time2;
  ObitIOCode retCode;
  ObitTableSNRow *row=NULL;
  gchar *routine = "refPhase";

  /* Error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return ;  /* previous error? */
  g_assert(ObitTableSNIsA(SNTab));
 
  /* Subarray */
  isuba = MAX (1, isub);
  
  /* Get descriptive info */
  numif  = SNTab->numIF;
  numpol = SNTab->numPol;

  /* Create Row */
  row = newObitTableSNRow (SNTab);

  /* Initially require both polarizations if present */
  need2 =  numpol>1; 
  dotwo =  numpol>1; 

  /* Loop thru table referring ant to refa. */
  done = FALSE;
  while (!done) { /* Loop trying combined and separate poln L10: */
    numtim = 0;
    for (loop=1; loop<=SNTab->myDesc->nrow; loop++) { /* loop 100 */

      retCode = ObitTableSNReadRow (SNTab, loop, row, err);
      if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
      if (row->status<0) continue;  /* Skip deselected record */
      
      /* Is this either of the antennas we're looking for? */
      if ((row->antNo!=refa) && (row->antNo!=ant)) continue;  /* Keep truckin' */
      
      /* right subarray? */
      if ((row->SubA!=isuba) && (row->SubA>0))  continue;  /* Keep truckin' */
      
      /* Find and check reference  antennas.  Must all be the  same. */
      refa1 = row->RefAnt1[iif];
      if (need2) refa2 = row->RefAnt2[iif];
      else refa2 = 0;
      
      /* Bad solution? */
      if (((row->Weight1[iif] <= 0.0)  ||  ( row->Real1[iif] == fblank)  ||  
	   (row->RefAnt1[iif] <= 0))) continue;  /* goto L100;*/
      if (need2  &&  ((numpol > 1)  &&  
		      ((row->Weight2[iif] <= 0.0) || (row->Real2[iif] == fblank)  || 
		       (row->RefAnt2[iif] <= 0)))) continue;  /* goto L100; */
      
      /* Desired antenna combination? */
      if (need2  &&  (refa1 > 0)  &&  (refa2 > 0)  &&  (refa1 != refa2)) continue; 
      
      if (refa1 < 0) refa1 = refa2;
      if ((refa1 != ant)  &&  (refa1 != refa)) continue;  /* goto L100;*/
      if (row->antNo == refa1) continue;   /* goto L100;*/
      if (numtim >= mxtime) continue;   /* goto L100;*/
      
      /* Save times */
      if (numtim == 0) timoff = row->Time;
      wrktim[numtim] = row->Time - timoff;
      
      if (refa1 != ant) {
	/* refa is reference ant */
	work2[numtim] = row->Real1[iif];
	work3[numtim] = row->Imag1[iif];
	if (dotwo) {
	  work4[numtim] = row->Real2[iif];
	  work5[numtim] = row->Imag2[iif];
	} 
      } else {
	/* ant is reference ant */
	work2[numtim] =  row->Real1[iif];
	work3[numtim] = -row->Imag1[iif];
	if (dotwo) {
	  work4[numtim] =  row->Real2[iif];
	  work5[numtim] = -row->Imag2[iif];
	} 
      } 
      numtim++;  /* count times */
    } /* end loop  L100: */;
    
    if (need2  &&  (numtim <= 0)) {
      /* Try again with only one poln. */
      need2 = FALSE;
      done  =  FALSE;  /* Need another pass to get both poln */
    } else done = TRUE;

  } /* End of loop over poln L10: */

  /* Find any? */
  if (numtim <= 0) {ObitTableSNRowUnref(row); return;}

  /* Smooth (2 sec to extrapolate) */
  smotim = 2.0 / 86400.0;
  boxsmo (smotim, wrktim, work2, numtim, work1);
  boxsmo (smotim, wrktim, work3, numtim, work2);
  if (dotwo) {
    boxsmo (smotim, wrktim, work4, numtim, work3);
    boxsmo (smotim, wrktim, work5, numtim, work4);
  } 

  /* Set up for interpolation */
  ipnt1 = 0;
  ipnt2 = 1;
  time1 = wrktim[0];
  time2 = wrktim[1];
  if (numtim == 1) {  /* Only one entry in array to interpolate? */
    ipnt2 = 0;
    time2 = time1;
  } 

  /* Loop thru table changing any data with ref=ant to ref=refa */
  numrec = SNTab->myDesc->nrow;
  for (loop=1; loop<=numrec; loop++) { /* loop 200 */
    
    retCode = ObitTableSNReadRow (SNTab, loop, row, err);
    if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
    if (row->status<0) continue;  /* Skip deselected record */
    
    /* right antenna? */
    if ((row->RefAnt1[iif] != ant)  &&  
	(dotwo  &&  (row->RefAnt2[iif] != ant))) continue;  /*goto L200;*/
    
    /* Right subarray? */
    sa = row->SubA;
    
    /* Interpolate */
    if ((sa == isuba)  ||  (sa <= 0)) {
      time = row->Time - timoff;
      done = FALSE;
      while (!done) {  /* loop until interpolation setup */
	if ((time >= time1)  &&  (time < time2)) {  /* L140: */
	  /* Between entries */
	  if (time2 != time1) {
	    wt1 = 1.0 - ((time-time1) / (time2-time1));
	  } else {
	    wt1 = 1.0;
	  } 
	  done = TRUE; 
	  
	} else if (time < time1) {/* Before first time */
	  wt1 = 1.0;
	  done = TRUE; 
	  
	} else if ((ipnt2+1) >= numtim) { /* After last time */
	  wt1 = 0.0;
	  done = TRUE; 
	  
	} else {   /* Shift in interpolation arrays */
	  ipnt1++;
	  ipnt2 = ipnt1+1;
	  time1 = wrktim[ipnt1];
	  time2 = wrktim[ipnt2];
	  done = FALSE;    /* goto L140; */
	}
      } /* end of loop until interpolation setup */
      
      /* Interpolate */
      wt2 = 1.0 - wt1;
      if (row->RefAnt1[iif] == ant) {
	/* Interpolate phase pol 1 */
	if ((row->Imag1[iif] != fblank)  &&  (row->Real1[iif] != fblank)) {
	  re1 = wt1 * work1[ipnt1] + wt2 * work1[ipnt2];
	  im1 = wt1 * work2[ipnt1] + wt2 * work2[ipnt2];
	  /* Normalize by amplitude */
	  amp = MAX (sqrt (re1*re1 + im1*im1), 1.0e-10);
	  re1 /= amp;
	  im1 /= amp;

	  /* Correct phase pol 1 */
	  tre = row->Real1[iif];
	  tim = row->Imag1[iif];
	  row->Real1[iif] = tre*re1 - tim*im1;
	  row->Imag1[iif] = tre*im1 + tim*re1;
	}  /* end data valid */
	
	/* Relabel reference antenna */
	row->RefAnt1[iif] = refa;
	
      } else if (row->RefAnt1[iif] == 0) {  /* Null ref ant */
	/* Relabel if blanked */
	if ((row->Imag1[iif] == fblank)  ||  (row->Real1[iif] == fblank)) 
	  row->RefAnt1[iif] = refa;
      }
      
      /* Second polarization */
      if (dotwo  &&  (row->RefAnt2[iif] == ant)) {
	if ((row->Imag2[iif] != fblank)  &&  (row->Real2[iif] != fblank)) {
	  re2 = wt1 * work3[ipnt1] + wt2 * work3[ipnt2];
	  im2 = wt1 * work4[ipnt1] + wt2 * work4[ipnt2];
	  /* Normalize by amplitude */
	  amp = MAX (sqrt (re2*re2 + im2*im2), 1.0e-10);
	  re2 /= amp;
	  im2 /= amp;
	  
	  /* Correct phase. pol 2 */
	  tre = row->Real2[iif];
	  tim = row->Imag2[iif];
	  row->Real2[iif] = tre*re2 - tim*im2;
	  row->Imag2[iif] = tre*im2 + tim*re2;
	} /* end data valid */
	
	/* Relabel reference antenna */
	row->RefAnt2[iif] = refa;
	
      } else if ((numpol > 1) && (row->RefAnt2[iif] == 0)) { /* Null ref ant */
	/* Relabel if blanked */
	if ((row->Imag2[iif] == fblank)  ||  (row->Real2[iif] == fblank)) 
	  row->RefAnt2[iif] = refa;
      }
      
      /* Rewrite record */
      retCode = ObitTableSNWriteRow (SNTab, loop, row, err);
      if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
    } /* end of if correct subarray */
  } /* end loop  L200: */;

  row = ObitTableSNRowUnref(row); /* delete row object */
} /* end of routine refPhase */ 

/**
 * Does a box car (running mean) smoothing of irregularly spaced points.  
 * Routine translated from the AIPSish BOXSMO.FOR/BOXSMO
 * \param width  Width of boxcar in same units as x, if <=0 just copy
 * \param x      Abscissas of points to be smoothed. 
 * \param y      Values to be smoothed. 
 * \param n      Number of points to smooth. 
 * \param ys     Smoothed values. 
 */
void boxsmo (gfloat width, gfloat* x, gfloat* y, gint n, gfloat* ys) 
{
  gint   first, count, i, current, limit;
  gfloat xlast, xfirst, sum, hwidth, avg;

  if (n <= 0) return;

  /* Actual smoothing? */
  if (width <= 0.0) { /* No */
    for (i=0; i<n; i++) ys[i] = y[i];
    return;
  }

  /* Initialize. */
  first   = 0;
  current = 0;
  xlast = x[0] + width;
  hwidth = width * 0.5;
  sum   = 0.0;
  count = 0;

  /* Average window at start. */
  i = 0;
  while ((i<=n) && (x[i] <= xlast)) {
    sum += y[i++];
    count++;
  }

  /* Average in Window. */
  avg = sum / MAX (1, count);

  /* Fill in average value within half width of start */
  xlast = x[0] + hwidth;
  while (x[current] <= xlast) {
    if (current >=  n) return;  /* done? */
    ys[current] = avg;
    current++;  /* next */
  }

  /* Begin main loop. */
  xlast = x[current] + hwidth;
  while (xlast  <  x[n-1]) {
    if (current >=  n) return;  /* done? */
    xfirst = x[current] - hwidth;
    xlast  = x[current] + hwidth;
    
    /* Check if running into window at the end. */
    if (xlast  >=  x[n-1]) break;  /* goto L200;*/

    /* Init sum. */
    sum   = 0.0;
    count = 0;
    limit = first;
    for (i=limit; i<n; i++) { /* loop 150 */
      /* Check if point too early. */
      if (x[i] < xfirst) { /*goto L120;*/
	/* Too early, reset FIRST. */
	first = i + 1;

	/* Check if out of window? */
      } else if (x[i] > xlast) break; /*goto L160; */

      else { /* Do sum */
	sum += y[i];
	count++;
      }
    } /* end loop  L150: */;

    /* Average window. */
    ys[current] = sum / MAX (1, count);  /* L160: */
    current++;  /* next */
  } /* End main loop goto L100; */

  /* Sum over end window. */
  count = 0;
  sum = 0.0;	
  xfirst = x[n-1]- width;
  
  for (i= first; i<n; i++) { /* loop 210 */
    if (x[i] >= xfirst) { /*goto L210;*/
      sum += y[i];
      count++;
    }
  } /* end loop  L210: */
  
  avg = sum / MAX (1, count);  /* average in last window */
  
  /* write average to last points */
  for (i=current; i<n; i++) { /* loop 250 */
    ys[i] = avg;
  } /* end loop  L250: */;
} /* end of routine boxsmo */ 


/**
 * Routine to smooth amplitudes and/or phases rates in an open SN  
 * table.  All poln present are smoothed but only one IF.  The KOL  
 * pointers are presumed to point at the desired IF.  An error is  
 * returned if there are any non-zero delays, rates, or multi-band  
 * delays.  If the reference antenna changes and phase is being  
 * smoothed, an error is returned.  
 * Input table must be in time order.  
 * Routine translated from the AIPSish SNSMOO.FOR/SNSMOO 
 * \param SNTab  SN table object; must be opened/closed externally
 * \param intType   Type of smoothing function: 'MWF', 'GAUS', else BOX 
 * \param alpha    Alpha clip for MWF (0 -> box, 1 -> pure MWF) 
 * \param stamp    Amplitude smoothing time (days) < 0 => none, 0 => 
 *                 fill in for blanked only. 
 * \param stph     Phase smoothing time (days) 
 * \param iif      Desired IF (0-rel)
 * \param sub      Desired subarray (1-rel)
 * \param gncnt    [In/Out] Count for gain normalization 
 * \param gnsum    [In/Out] Sum of gain modulii 
 * \param nxt      Number of times allowed in WRK 
 * \param work1    Work buffer (NXT*10) 
 * \param work2    Work area >= (NXT*m)  (m=2 BOX, 3 GAUS, 4 MWF) 
 * \param err    Error/message stack, returns if error.
 */
static void 
SNSmooth (ObitTableSN *SNTab, gchar* intType, gfloat alpha, gfloat stamp, gfloat stph, 
	  gint iif, gint sub, gfloat* gncnt, gfloat* gnsum, 
	  gint nxt, gfloat* work1, gfloat* work2, ObitErr* err) 
{
  gint   loopr, loopa, numtim, ant, numrec, nleft, isnrno, itime, 
    refa1, refa2, n1good, n2good, i, numif, numpol, numant;
  gfloat    amp, fblank =  ObitMagicF();
  gboolean  need2;
  gdouble   timoff=0.0;
  ObitIOCode retCode;
  ObitTableSNRow *row=NULL;
  gchar *routine = "SNSmooth";
  
  /* Error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return ;  /* previous error? */
  g_assert(ObitTableSNIsA(SNTab));
  
  /* Get number of records in table */
  numrec = SNTab->myDesc->nrow;
  if (numrec <= 0) return;   /* bail if nothing */
  
  /* Get descriptive info */
  numif  = SNTab->numIF;
  numpol = SNTab->numPol;
  numant = SNTab->numAnt;
  
  /* Create Row */
  row = newObitTableSNRow (SNTab);
  
  /* Are there 2 polarizations? */
  need2 = numpol>1;
  refa1 = 0;
  refa2 = 0;
  
  /* Loop over antenna */
  for (loopa= 1; loopa<=numant; loopa++) { /* loop 600 */
    ant = loopa;
    /* Set pointers, counters */
    numtim = 0;
    nleft = numrec;
    n1good = 0;
    n2good = 0;
    /* Loop in time, reading */
    for (loopr=1; loopr<=nleft; loopr++) { /* loop 100 */
      isnrno = loopr;
      retCode = ObitTableSNReadRow (SNTab, isnrno, row, err);
      if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
      if (row->status<0) continue;  /* Skip deselected record */
      
      /* right antenna? */
      if (row->antNo > ant) continue;  /* goto L110;*/
      
      /* Want this record */
      if ((row->SubA == sub)  &&  (row->antNo == ant)) {
	/* Check for vlb etc. - cannot do  */
	if (((row->MBDelay1    != fblank)  &&  (row->MBDelay1    != 0.0)) ||
	    ((row->Delay1[iif] != fblank)  &&  (row->Delay1[iif] != 0.0)) ||
	    ((row->Rate1[iif]  != fblank)  &&  (row->Rate1[iif]  != 0.0))) {
	  /* goto L985; */
	  Obit_log_error(err, OBIT_Error, 
			 "%s: Non-zero rates or delays, cannot smooth %s", 
			 routine, SNTab->name);
	  row = ObitTableSNRowUnref(row); /* delete row object */
	  return;
	}
	
	/* take first reference antenna and check that it never changes */
	if (refa1 <= 0) {
	  refa1 = row->RefAnt1[iif];
	} else {
	  if (row->RefAnt1[iif] == 0) row->RefAnt1[iif] = refa1;
	} 
	if (refa1 != row->RefAnt1[iif]) { /* goto L980;*/
	  Obit_log_error(err, OBIT_Error, 
			 "%s: Reference antenna varies, cannot smooth %s", 
			 routine, SNTab->name);
	  row = ObitTableSNRowUnref(row); /* delete row object */
	  return;
	}
	
	if (need2) {  /* Get second polarization */
	  /* Check for vlb etc. - cannot do  */
	  if (((row->MBDelay2    != fblank)  &&  (row->MBDelay2    != 0.0)) ||
	      ((row->Delay2[iif] != fblank)  &&  (row->Delay2[iif] != 0.0)) ||
	      ((row->Rate2[iif]  != fblank)  &&  (row->Rate2[iif]  != 0.0))) {
	    /* goto L985; */
	    Obit_log_error(err, OBIT_Error, 
			   "%s: Non-zero rates or delays, cannot smooth %s", 
			   routine, SNTab->name);
	    row = ObitTableSNRowUnref(row); /* delete row object */
	    return;
	  }
	  
	  
	  /* take first reference antenna and check that it never changes */
	  if (refa2 <= 0) {
	    refa2 = row->RefAnt2[iif];
	  } else {
	    if (row->RefAnt2[iif] == 0) row->RefAnt2[iif] = refa2;
	  } 
	  if (refa2 != row->RefAnt2[iif]) { /* goto L980;*/
	    Obit_log_error(err, OBIT_Error, 
			   "%s: Reference antenna varies, cannot smooth %s", 
			   routine, SNTab->name);
	    row = ObitTableSNRowUnref(row); /* delete row object */
	    return;
	  }
	}  /* end get second polarization */ 
	
	/* Put in buffer */
	if (numtim >= nxt) {
	  Obit_log_error(err, OBIT_Error, 
			 "%s: Exceed time limit of %d for %s", 
			 routine, nxt, SNTab->name);
	  row = ObitTableSNRowUnref(row); /* delete row object */
	  return;
	} 
	/* Work1 usage :
	   0 = Real pol 1
	   1 = Imag pol 1
	   2 = Amplitude pol 1
	   3 = Weight pol 1
	   4 = Real pol 2
	   5 = Imag pol 2
	   6 = Amplitude pol 2
	   7 = Weight pol 2
	   8 = Time(day) relative to first
	   9 = row number
	*/
	
	if (numtim == 0) timoff = row->Time;  /* First time */
	work1[8*nxt+numtim] = row->Time - timoff;
	work1[9*nxt+numtim] = (gfloat)isnrno;
	if ((row->Weight1[iif] > 0.0)  &&  (row->Weight1[iif] != fblank)) {
	  work1[2*nxt+numtim] = sqrt (row->Real1[iif]*row->Real1[iif] + 
				      row->Imag1[iif]*row->Imag1[iif]);
	} else {
	  work1[2*nxt+numtim] = 0.0;
	} 
	/* First polarization */
	if (work1[2*nxt+numtim] > 0.0) {
	  work1[0*nxt+numtim] = row->Real1[iif] / work1[2*nxt+numtim];
	  work1[1*nxt+numtim] = row->Imag1[iif] / work1[2*nxt+numtim];
	  work1[3*nxt+numtim] = row->Weight1[iif];
	  n1good = n1good + 1;
	} else {
	  work1[0*nxt+numtim] = 0.0;
	  work1[1*nxt+numtim] = 0.0;
	  work1[3*nxt+numtim] = 0.0;
	}
	
	if (need2) {  /* Second polarization */
	  if ((row->Weight2[iif] > 0.)  &&  (row->Weight2[iif] != fblank)) {
	    work1[6*nxt+numtim] = sqrt (row->Real2[iif]*row->Real2[iif] + 
					row->Imag2[iif]*row->Imag2[iif]);
	  } else {
	    work1[6*nxt+numtim] = 0.0;
	  } 
	  if (work1[6*nxt+numtim] > 0.0) {
	    work1[4*nxt+numtim] = row->Real2[iif] / work1[6*nxt+numtim];
	    work1[5*nxt+numtim] = row->Imag2[iif] / work1[6*nxt+numtim];
	    work1[7*nxt+numtim] = row->Weight2[iif];
	    n2good = n2good + 1;
	  } else {
	    work1[4*nxt+numtim] = 0.0;
	    work1[5*nxt+numtim] = 0.0;
	    work1[7*nxt+numtim] = 0.0;
	  } 
	} /* end second polarization */
      } /* end if want record */ 
      numtim++;   /* count times */
    } /* end loop  L100: */
    
    /* Smooth as requested */
    if (n1good > 0) { /* First polarization */
      if (!strncmp (intType, "GAUS", 4)) {
	SmooGauss (stamp, &work1[8*nxt], &work1[2*nxt], &work1[3*nxt], numtim, 
		   &work2[0*nxt], &work2[1*nxt], &work2[2*nxt]);
	for (i=0; i<numtim; i++) work1[2*nxt+i] = work2[i];
	SmooGauss (stph, &work1[8*nxt], &work1[0*nxt], &work1[3*nxt], numtim, 
		   &work2[0*nxt], &work2[1*nxt], &work2[2*nxt]);
	for (i=0; i<numtim; i++) work1[0*nxt] = work2[i];
	SmooGauss (stph, &work1[8*nxt], &work1[1*nxt], &work1[3*nxt], numtim, 
		   &work2[0*nxt], &work2[1*nxt], &work2[2*nxt]);
	for (i=0; i<numtim; i++) work1[1*nxt+i] = work2[i];
	for (i=0; i<numtim; i++) work1[3*nxt+i] = work2[1*nxt+i];
      } else if (!strncmp (intType, "MWF", 3)) {
	SmooMWF (stamp, alpha, &work1[8*nxt], &work1[2*nxt], &work1[3*nxt], numtim, 
		 &work2[0*nxt], &work2[1*nxt], &work2[2*nxt], &work2[3*nxt]);
	for (i=0; i<numtim; i++) work1[2*nxt+i] = work2[i];
	SmooMWF (stph, alpha, &work1[8*nxt], &work1[0*nxt], &work1[3*nxt], numtim, 
		 &work2[0*nxt], &work2[1*nxt], &work2[2*nxt], &work2[3*nxt]);
	for (i=0; i<numtim; i++) work1[i] = work2[i];
	SmooMWF (stph, alpha, &work1[8*nxt], &work1[1*nxt], &work1[3*nxt], numtim, 
		 &work2[0*nxt], &work2[1*nxt], &work2[2*nxt], &work2[3*nxt]);
	for (i=0; i<numtim; i++) work1[1*nxt+i] = work2[i];
	for (i=0; i<numtim; i++) work1[3*nxt+i] = work2[1*nxt+i];
      } else {
	SmooBox (stamp, &work1[8*nxt], &work1[2*nxt], &work1[3*nxt], numtim, 
		 &work2[0*nxt], &work2[1*nxt]);
	for (i=0; i<numtim; i++) work1[2*nxt+i] = work2[i];
	SmooBox (stph, &work1[8*nxt], &work1[0*nxt], &work1[3*nxt], numtim, 
		 &work2[0*nxt], &work2[1*nxt]);
	for (i=0; i<numtim; i++) work1[i] = work2[i];
	SmooBox (stph, &work1[8*nxt], &work1[1*nxt], &work1[3*nxt], numtim, 
		 &work2[0*nxt], &work2[1*nxt]);
	for (i=0; i<numtim; i++) work1[1*nxt+i] = work2[i];
	for (i=0; i<numtim; i++) work1[3*nxt+i] = work2[1*nxt+i];
      }
      
    } /* end first polarization */
    
    if (n2good > 0) {  /* Second polarization */
      if (!strncmp (intType, "GAUS", 4)) {
	SmooGauss (stamp, &work1[8*nxt], &work1[6*nxt], &work1[7*nxt], numtim, 
		   &work2[0*nxt], &work2[1*nxt], &work2[2*nxt]);
	for (i=0; i<numtim; i++) work1[6*nxt+i] = work2[i];
	SmooGauss (stph, &work1[8*nxt], &work1[4*nxt], &work1[7*nxt], numtim, 
		   &work2[0*nxt], &work2[1*nxt], &work2[2*nxt]);
	for (i=0; i<numtim; i++) work1[4*nxt+i] = work2[i];
	SmooGauss (stph, &work1[8*nxt], &work1[5*nxt], &work1[7*nxt], numtim, 
		   &work2[0*nxt], &work2[1*nxt], &work2[2*nxt]);
	for (i=0; i<numtim; i++) work1[5*nxt+i] = work2[i];
	for (i=0; i<numtim; i++) work1[7*nxt+i] = work2[1*nxt+i];
      } else if (!strncmp (intType, "MWF", 3)) {
	SmooMWF (stamp, alpha, &work1[8*nxt], &work1[6*nxt], &work1[7*nxt], numtim, 
		 &work2[0*nxt], &work2[1*nxt], &work2[2*nxt], &work2[3*nxt]);
	for (i=0; i<numtim; i++) work1[6*nxt+i] = work2[i];
	SmooMWF (stph, alpha, &work1[8*nxt], &work1[4*nxt], &work1[7*nxt], numtim, 
		 &work2[0*nxt], &work2[1*nxt], &work2[2*nxt], &work2[3*nxt]);
	for (i=0; i<numtim; i++) work1[4*nxt+i] = work2[i];
	SmooMWF (stph, alpha, &work1[8*nxt], &work1[5*nxt], &work1[7*nxt], numtim, 
		 &work2[0*nxt], &work2[1*nxt], &work2[2*nxt], &work2[3*nxt]);
	for (i=0; i<numtim; i++) work1[5*nxt+i] = work2[i];
	for (i=0; i<numtim; i++) work1[7*nxt+i] = work2[1*nxt+i];
      } else {
	SmooBox (stamp, &work1[8*nxt], &work1[6*nxt], &work1[7*nxt], numtim, 
		 &work2[0*nxt], &work2[1*nxt]);
	for (i=0; i<numtim; i++) work1[6*nxt+i] = work2[i];
	SmooBox (stph, &work1[8*nxt], &work1[4*nxt], &work1[7*nxt], numtim, 
		 &work2[0*nxt], &work2[1*nxt]);
	for (i=0; i<numtim; i++) work1[4*nxt+i] = work2[i];
	SmooBox (stph, &work1[8*nxt], &work1[5*nxt], &work1[7*nxt], numtim, 
		 &work2[0*nxt], &work2[1*nxt]);
	for (i=0; i<numtim; i++) work1[5*nxt+i] = work2[i];
	for (i=0; i<numtim; i++) work1[7*nxt+i] = work2[1*nxt+i];
      } 
    } /* end second polarization */
    
    /* Replace with smoothed values */
    for (itime=0; itime<numtim; itime++) { /* loop 200 */
      isnrno = (gint)(work1[9*nxt+itime]+0.5);
      retCode = ObitTableSNReadRow (SNTab, isnrno, row, err);
      if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
      if (row->status<0) continue;  /* Skip deselected record */
      
      /* Update */
      if (work1[3*nxt+itime] > 0.0) {
	amp = sqrt (work1[0*nxt+itime]*work1[0*nxt+itime] + 
		    work1[1*nxt+itime]*work1[1*nxt+itime]);
	if (amp <= 0.0) amp = 1.0;
	row->Real1[iif]   = work1[0*nxt+itime] * work1[2*nxt+itime] / amp;
	row->Imag1[iif]   = work1[1*nxt+itime] * work1[2*nxt+itime] / amp;
	row->Weight1[iif] = work1[3*nxt+itime];
	(*gncnt) += + 1.0;
	(*gnsum) += work1[2*nxt+itime];
	if (row->RefAnt1[iif] == 0) row->RefAnt1[iif] = refa1;
      } 
      if (need2) {
	if (work1[7*nxt+itime] > 0.0) {
	  amp = sqrt (work1[4*nxt+itime]*work1[4*nxt+itime] + 
		      work1[5*nxt+itime]*work1[5*nxt+itime]);
	  if (amp <= 0.0) amp = 1.0;
	  row->Real2[iif]   = work1[4*nxt+itime] * work1[6*nxt+itime] / amp;
	  row->Imag2[iif]   = work1[5*nxt+itime] * work1[6*nxt+itime] / amp;
	  row->Weight2[iif] = work1[7*nxt+itime];
	  (*gncnt) += 1.0;
	  (*gnsum) += work1[6*nxt+itime];
	  if (row->RefAnt2[iif] == 0) row->RefAnt2[iif] = refa2;
	} 
      }
      
      /* Rewrite record */
      retCode = ObitTableSNWriteRow (SNTab, isnrno, row, err);
      if (err->error) Obit_traceback_msg (err, routine, SNTab->name);
    } /* end loop rewriting smoothed solutions L200: */;
    /* First SN number of next antenna */
    
    /* End of antenna loop */
  } /* end loop over antennas  L600: */;

  row = ObitTableSNRowUnref(row); /* delete row object */
} /* end of routine SNSmooth */ 

/**
 * Does a box car (running mean) smoothing of weighted  
 * irregularly spaced points possibly with blanked values.  Only  
 * returns blanked values if no valid data found.  First good value  
 * used for all previous points, last good value used for all  
 * subsequent points in which all data are blanked in the boxcar.  A  
 * datum is blanked if its weight is <= 0 or fblank.  
 * Routine translated from the AIPSish SMBOX.FOR/SMBOX  
 * \param width   Width of boxcar in same units as X: 0 => replace 
 *                blanks with interpolated closest 2, < 0 => replace 
 *                only blanks with the boxcar smoothed values (all 
 *                others remain unchanged) 
 * \param x       Abscissas of points to be smoothed in increasing 
 *                order 
 * \param y       Values to be smoothed. 
 * \param w       Weights of data. 
 * \param n       Number of points to smooth. 
 * \param ys      Smoothed values. 
 * \param ws      Smoothed weights 
 */
static void 
SmooBox (gfloat width, gfloat* x, gfloat* y, gfloat* w, gint n, 
	   gfloat* ys, gfloat* ws) 
{
  gint   i , j, k, l, i1, i2;
  gboolean wasb, onlyb, blnkd;
  gfloat  hw, d, temp, fblank =  ObitMagicF();

  if (n <= 0) return;    /* any data? */
  d = fabs (width);      /* Smoothing width */
  onlyb = width <= 0.0;  /* Only interpolate blanked values? */
  hw = d / 2.0;          /* Half width of smoothing */
  wasb = FALSE;          /* any blanked data? */

  /* width = 0.0 => interp only - here copy good and interpolate later */
  if (d <= 0.0) {
    for (i=0; i<n; i++) { /* loop 10 */
      blnkd = (y[i] == fblank)  ||  (w[i] <= 0.0)  ||  (w[i] == fblank);
      if (blnkd) {
	ys[i] = fblank;
	ws[i] = 0.0;
	wasb = TRUE;
      } else {
	ys[i] = y[i];
	ws[i] = w[i];
      } 
    } /* end loop  L10:  */;

    
  } else {  /* Smooth */
    for (i=0; i<n; i++) { /* loop 100 */
      blnkd = (y[i] == fblank)  ||  (w[i] <= 0.0)  ||  (w[i] == fblank);
      if (blnkd) {
	ys[i] = 0.0;
	ws[i] = 0.0;
      } else {
	ys[i] = y[i] * w[i];
	ws[i] = w[i];
      } 

      if ((blnkd)  ||  (!onlyb)) {  /* smoothing data */
	for (k=0; k<i; k++) { /* weighted sum of previous data loop 20 */
	  j = i - k;
	  if (fabs(x[i]-x[j]) > hw) {
	    break;   /* out of window? goto L25;*/
	  } else {
	    if ((y[j] != fblank)  &&  (w[j] > 0.0)  &&  (w[j] != fblank)) {
	      ys[i] += y[j] * w[j];
	      ws[i] += w[j];
	    } 
	  } 
	} /* end loop  L20:  */;

	for (j= i+1; j<n; j++) { /* weighted sum of subsequent data loop 30 */
	  if (fabs(x[i]-x[j]) > hw) {
	    break;   /* out of window? goto L35;*/  
	  } else {
	    if ((y[j] != fblank)  &&  (w[j] > 0.0)  &&  (w[j] != fblank)) {
	      ys[i] += y[j] * w[j];
	      ws[i] += w[j];
	    } 
	  } 
	} /* end loop  L30:  */;
      } /* end of smoothing */
      
      if (ws[i] > 0.0) {  /*  normalize by sum of weights L35: */
	ys[i] /= ws[i];
      } else {
	wasb = TRUE;
	ys[i] = fblank;
      } 
    } /* end loop  L100: */;
  } /* End of smoothing */


  /* fill in any remaining blanks */
  if (wasb) {
    /* extrapolate to ends */
    i1 = n+1;
    i2 = 0;
    for (i=0; i<n; i++) { /* loop 110 */
      if (ws[i] > 0.0) {
	i1 = MIN (i, i1);
	i2 = MAX (i, i2);
      } 
    } /* end loop  L110: */;

    if (i1 > 1) {  /* Fill to beginning */
      j = i1 - 1;
      for (l=0; l<=j; l++) 
	{ys[l] = ys[i1]; ws[l] = ws[i1];}
    } 
    
    if (i2 < n) {  /* Fill to end */
      j = n - i2 - 1;
      for (l=0; l<j; l++) 
	{ys[i2+1+l] = ys[i2]; ws[i2+1+l] = ws[i2];}
    } 

    /* interpolate others */
    for (i=0; i<n; i++) { /* loop 130 */
      if (ws[i] > 0.0) {  /* OK */
	i1 = i;           /* previous valid */
      } else {  /* Blanked, find next valid */
	for (i2=i+1; i2<n; i2++) { /* loop 120 */
	  if (ws[i2] > 0.0) break;  /* found it goto L125;*/
	} /* end loop  L120: */;

	/* interpolate */
	temp = x[i2] - x[i1]; /* L125: */
	if (temp == 0.0) temp = 1.0;
	ys[i] = ys[i1] + (x[i]-x[i1]) * (ys[i2]-ys[i1]) / temp;
	ws[i] = ws[i1] + (x[i]-x[i1]) * (ws[i2]-ws[i1]) / temp;
      } 
    } /* end loop  L130: */
  } /* end of if any blanks to interpolate */ 
} /* end of routine SmooBox */ 

/**
 * Does a Gaussian (running mean) smoothing of weighted  
 * irregularly spaced points possibly with blanked values.  Only  
 * returns blanked values if no valid data found.  First good value  
 * used for all previous points, last good value used for all  
 * subsequent points in which all data are blanked in the smoothing interval.  
 * A datum is considered blanked if its weight is <= 0 or its value fblank.  
 * Routine translated from the AIPSish SMGAUS.FOR/SMGAUS  
 * \param width   Width of boxcar in same units as X: 0 => replace 
 *                blanks with interpolated closest 2, < 0 => replace 
 *                only blanks with the b oxcar smoothed values (all 
 *                others remain unchanged) 
 * \param x       Abscissas of points to be smoothed in increasing 
 *                order 
 * \param y       Values to be smoothed. 
 * \param w       Weights of data. 
 * \param n       Number of points to smooth. 
 * \param ys      Smoothed values. 
 * \param ws      Smoothed weights 
 * \param wtsum   scratch 
 */
static void 
  SmooGauss (gfloat width, gfloat* x, gfloat* y, gfloat* w, gint n, 
	     gfloat* ys, gfloat* ws, gfloat* wtsum) 
{
  gint   i , j, k, l, i1, i2;
  gfloat      hw, d, temp, s=0.0, g, fblank =  ObitMagicF();
  gboolean   wasb, onlyb, blnkd;


  if (n <= 0) return;    /* any data? */
  d = fabs (width);      /* Smoothing width */
  onlyb = width <= 0.0;  /* Only interpolate blanked values? */
  hw = d / 2.0;          /* Half width of smoothing */
  wasb = FALSE;          /* any blanked data? */

  /* width = 0.0 => interp only - here copy good and interpolate later */
  if (d <= 0.0) {
    for (i=0; i<n; i++) { /* loop 10 */
      blnkd = (y[i] == fblank)  ||  (w[i] <= 0.0)  ||  (w[i] == fblank);
      if (blnkd) {
	ys[i] = fblank;
	ws[i] = 0.0;
	wasb = TRUE;
      } else {
	ys[i] = y[i];
	ws[i] = w[i];
      } 
    } /* end loop  L10:  */;

    
  } else {   /* Smooth */
    for (i=0; i<n; i++) { /* loop 100 */
      blnkd = (y[i] == fblank)  ||  (w[i] <= 0.0)  ||  (w[i] == fblank);
      if (blnkd) {
	ys[i] = 0.0;
	ws[i] = 0.0;
	wtsum[i] = 0.0;
      } else {
	ys[i] = y[i] * w[i];
	ws[i] = w[i];
	wtsum[i] = 1.0;
      } 

      if ((blnkd)  ||  (!onlyb)) { /* smoothing data */
	for (k=0; k<i; k++) { /* weighted sum of previous data loop 20 */
	  j = i - k;
	  if (fabs(x[i]-x[j]) > hw) {
	    break;   /* out of window? goto L25; */
	  } else {
	    if ((y[j] != fblank)  &&  (w[j] > 0.0)  &&  (w[j] != fblank)) {
	      g = exp (-((x[i]-x[j])/s)*((x[i]-x[j])/s));
	      wtsum[i] += g;
	      ys[i]    += y[j] * w[j] * g;
	      ws[i]    += w[j] * g;
	    } 
	  } 
	} /* end loop  L20:  */

	for (j= i+1; j<n; j++) { /* weighted sum of subsequent data loop  30*/
	  if (fabs(x[i]-x[j]) > hw) {
	    break;   /* out of window? goto L35;*/
	  } else {
	    if ((y[j] != fblank)  &&  (w[j] > 0.0)  &&  (w[j] != fblank)) {
	      g = exp (-((x[i]-x[j])/s)*((x[i]-x[j])/s));
	      wtsum[i] += g;
	      ys[i]    += y[j] * w[j] * g;
	      ws[i]    += w[j] * g;
	    } 
	  } 
	} /* end loop  L30:  */;
      } /* end of smoothing */ 

      if (ws[i] > 0.0) {   /*  normalize by sum of weights L35: */
	ys[i] = ys[i] / ws[i];
	if (wtsum[i] > 0.0) ws[i] = ws[i] / wtsum[i];
      } else {
	wasb = TRUE;
	ys[i] = fblank;
      } 
    } /* end loop  L100: */;
  } /* end of Smoothing */
 
  /* fill in any remaining blanks */
  if (wasb) {
    /* extrapolate to ends */
    i1 = n+1;
    i2 = 0;
    for (i=0; i<n; i++) { /* loop 110 */
      if (ws[i] > 0.0) {
	i1 = MIN (i, i1);
	i2 = MAX (i, i2);
      } 
    } /* end loop  L110: */;

    if (i1 > 1) {  /* Fill to beginning */
      j = i1 - 1;
      for (l=0; l<=j; l++) 
	{ys[l] = ys[i1]; ws[l] = ws[i1];}
    } 
    
    if (i2 < n) {  /* Fill to end */
      j = n - i2 - 1;
      for (l=0; l<j; l++) 
	{ys[i2+1+l] = ys[i2]; ws[i2+1+l] = ws[i2];}
    } 

    /* interpolate others */
    for (i=0; i<n; i++) { /* loop 130 */
      if (ws[i] > 0.0) {  /* OK */
	i1 = i;           /* previous valid */
      } else {  /* Blanked, find next valid */
	for (i2=i+1; i2<n; i2++) { /* loop 120 */
	  if (ws[i2] > 0.0) break;  /* found it goto L125;*/
	} /* end loop  L120: */;

	/* interpolate */
	temp = x[i2] - x[i1]; /* L125: */
	if (temp == 0.0) temp = 1.0;
	ys[i] = ys[i1] + (x[i]-x[i1]) * (ys[i2]-ys[i1]) / temp;
	ws[i] = ws[i1] + (x[i]-x[i1]) * (ws[i2]-ws[i1]) / temp;
      } 
    } /* end loop  L130: */;
  } /* end of if any blanks to interpolate */ 
} /* end of routine SmooGauss */ 

/**
 * Does a median window smoothing of weighted irregularly spaced  
 * points possibly with blanked values.  Only returns blanked values if  
 * no valid data found.  First good value used for all previous points,  
 * last good value used for all subsequent points in which all data are  
 * blanked in the smoothing interval.
 * A datum is considered blanked if its weight is <= 0 or its value fblank.  
 * Routine translated from the AIPSish SMMWF.FOR/SMMWF  
 * \param width   Width of boxcar in same units as X: 0 => replace 
 *                blanks with interpolated closest 2, < 0 => replace 
 *                only blanks with the b oxcar smoothed values (all 
 *                others remain unchanged) 
 * \param alpha   0 -> 1 = pure boxcar -> pure MWF (ALPHA of the 
 *                data samples are discarded and the rest averaged). 
 * \param x       Abscissas of points to be smoothed in increasing 
 *                order 
 * \param y       Values to be smoothed. 
 * \param w       Weights of data. 
 * \param n       Number of points to smooth. 
 * \param ys      Smoothed values. 
 * \param ws      Smoothed weights 
 * \param yor     Scratch 
 * \param wor     Scratch 
 */
static void 
SmooMWF (gfloat width, gfloat alpha, gfloat* x, gfloat* y, gfloat* w, gint n, 
	 gfloat* ys, gfloat* ws, gfloat* yor, gfloat* wor) 
{
  gint   i, j, k, l, i1, i2, ic;
  gfloat      hw, d, temp, beta=0.0, fblank =  ObitMagicF();
  gboolean   wasb, onlyb, blnkd;

  if (n <= 0) return;    /* any data? */
  d = fabs (width);      /* Smoothing width */
  onlyb = width <= 0.0;  /* Only interpolate blanked values? */
  hw = d / 2.0;          /* Half width of smoothing */
  wasb = FALSE;          /* any blanked data? */

  /* width = 0.0 => interp only - here copy good and interpolate later */
  if (d <= 0.0) {
    for (i=0; i<n; i++) { /* loop 10 */
      blnkd = (y[i] == fblank)  ||  (w[i] <= 0.0)  ||  (w[i] == fblank);
      if (blnkd) {
	ys[i] = fblank;
	ws[i] = 0.0;
	wasb = TRUE;
      } else {
	ys[i] = y[i];
	ws[i] = w[i];
      } 
    } /* end loop  L10:  */;
    
  } else {   /* Smooth */
    for (i=0; i<n; i++) { /* loop 100 */
      blnkd = (y[i] == fblank)  ||  (w[i] <= 0.0)  ||  (w[i] == fblank);
      if (blnkd) {
	ys[i] = 0.0;
	ws[i] = 0.0;
	ic = 0;
      } else {
	ys[i] = y[i] * w[i];
	ws[i] = w[i];
	ic = 0;
	yor[ic] = y[i];
	wor[ic] = w[i];
      }
      
      if ((blnkd)  ||  (!onlyb)) { /* smoothing data */
	for (k=1; k<i-1; k++) { /* previous values loop 30 */
	  if (fabs(x[i]-x[i-k]) > hw) {
	    break;   /* out of window? goto L35; */
	  } else {
	    if ((y[i-k] != fblank)  &&  (w[i-k] > 0.0)  &&  (w[i-k] != fblank)) {
	      /* valid datum in window, order the datum */
	      l = ic + 1;
	      for (j=0; j<ic; j++) { /* find location in ordered arrays for [i-k] loop 15 */
		if (yor[j] > y[i-k]) {l = j; break;}  /* goto L20; */
	      } /* end loop  L15:  */;
	      
	      for (j=l; j<=ic; j++) { /* shuffle loop 25 */
		i1 = ic - j + l;
		yor[i1+1] = yor[i1];
		wor[i1+1] = wor[i1];
	      } /* end loop  L25:  */;
	      yor[l] = y[i-k];  /* insert [i-k] in ordered array */
	      wor[l] = w[i-k];
	    } 
	  } 
	} /* end loop  L30:  */;
	
	for (k=i+1; k<n; k++) { /* subsequent points loop 60 */
	  if (fabs(x[i]-x[k]) > hw) {
	    break; /* out of window goto L65;*/
	  } else {
	    if ((y[k] != fblank)  &&  (w[k] > 0.0)  &&  (w[k] != fblank)) {
	      l = ic + 1;
	      for (j=0; j<ic; j++) { /* find location in ordered arrays for [k] loop 45 */
		if (yor[j] > y[k]) {l = j; break;}  /* goto L50; */
	      } /* end loop  L45:  */;
	      
	      for (j=l; j<ic; j++) { /*shuffle  loop 55 */
		i1 = ic - j + l;
		yor[i1+1] = yor[i1];
		wor[i1+1] = wor[i1];
	      } /* end loop  L55:  */;
	      yor[l-1] = y[k];   /* insert [k] in ordered array */
	      wor[l-1] = w[k];
	    } 
	  } 
	} /* end loop  L60:  */
	
	/* Now average the center set */
	ys[i] = 0.0;  /*  L65: */
	ws[i] = 0.0;
	if (ic > 0) {
	  k = beta * ic + 0.5;
	  i1 = k;
	  i2 = ic - 1 - k;
	  if (i2 < i1) {
	    i1 = MAX (0, i1-1);
	    i2 = MIN (ic-1, i2+1);
	  } 
	  for (k=i1; k<=i2; k++) { /* loop 70 */
	    ys[i] += yor[k] * wor[k];
	    ws[i] += wor[k];
	  } /* end loop  L70:  */;
	} /* end of some data to average */ 
      } /* end  smoothing data */

      /* Get smoothed datum */
      if (ws[i] > 0.0) {
	ys[i] = ys[i] / ws[i];
      } else {
	ys[i] = fblank;
	wasb = TRUE;
      } 
    } /* end smoothing loop  L100: */
  } /* end of Smooth */

  /* fill in any remaining blanks */
  if (wasb) {
    /* extrapolate to ends */
    i1 = n+1;
    i2 = 0;
    for (i=0; i<n; i++) { /* loop 110 */
      if (ws[i] > 0.0) {
	i1 = MIN (i, i1);
	i2 = MAX (i, i2);
      } 
    } /* end loop  L110: */

    if (i1 > 1) {  /* Fill to beginning */
      j = i1 - 1;
      for (l=0; l<=j; l++) 
	{ys[l] = ys[i1]; ws[l] = ws[i1];}
    } 
    
    if (i2 < n) {  /* Fill to end */
      j = n - i2 - 1;
      for (l=0; l<j; l++) 
	{ys[i2+1+l] = ys[i2]; ws[i2+1+l] = ws[i2];}
    } 

    /* interpolate others */
    for (i=0; i<n; i++) { /* loop 130 */
      if (ws[i] > 0.0) {  /* OK */
	i1 = i;           /* previous valid */
      } else {  /* Blanked, find next valid */
	for (i2=i+1; i2<n; i2++) { /* loop 120 */
	  if (ws[i2] > 0.0) break;  /* found it goto L125;*/
	} /* end loop  L120: */

	/* interpolate */
	temp = x[i2] - x[i1]; /* L125: */
	if (temp == 0.0) temp = 1.0;
	ys[i] = ys[i1] + (x[i]-x[i1]) * (ys[i2]-ys[i1]) / temp;
	ws[i] = ws[i1] + (x[i]-x[i1]) * (ws[i2]-ws[i1]) / temp;
      } 
    } /* end loop  L130: */
  } /* end of if any blanks to interpolate */ 


} /* end of routine SmooMWF */ 
