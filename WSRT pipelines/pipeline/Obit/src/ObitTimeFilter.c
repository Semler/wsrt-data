/* $Id: ObitTimeFilter.c,v 1.3 2004/12/28 14:40:50 bcotton Exp $  */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003,2004                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include <math.h>
#include "ObitTimeFilter.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitTimeFilter.c
 * ObitTimeFilter class function definitions.
 * This class is derived from the Obit base class.
 */

/** name of the class defined in this file */
static gchar *myClassName = "ObitTimeFilter";

/*--------------- File Global Variables  ----------------*/
/**
 * ClassInfo structure ObitTimeFilterClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitTimeFilterClassInfo myClassInfo = {FALSE};

/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitTimeFilterInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitTimeFilterClear (gpointer in);

/** Private: interpolate to replace blanks in a 1-D ObitFarray. */
static void  ObitTimeFilterDeblank (ObitTimeFilter *in, ObitFArray *array);

/*----------------------Public functions---------------------------*/
/**
 * Constructor.
 * Initializes class if needed on first call.
 * \param name    An optional name for the object.
 * \param nTime   Number of times in arrays to be filtered
 *                It is best to add some extra padding (10%) to allow a smooth
 *                transition from the end of the sequence back to the beginning.
 *                Remember the FFT algorithm assumes the function is periodic.
 * \param nSeries Number of time sequences to be filtered
 * \return the new object.
 */
ObitTimeFilter* newObitTimeFilter (gchar* name, gint nTime, gint nSeries)
{
  ObitTimeFilter* out;
  gint i, rank, dim[1];
  glong ndim, naxisr[1], naxisc[1], pos[1] = {0};

  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitTimeFilterClassInit();

  /* allocate/init structure */
  out = g_malloc0(sizeof(ObitTimeFilter));

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

  /* set ClassInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  ObitTimeFilterInit((gpointer)out);

  /* save inputs */
  out->nTime   = nTime;
  out->nFreq   = 1 + nTime / 2;
  out->nSeries = nSeries;

  /* Create arrays */
  out->timeData   = g_malloc0 (nSeries*sizeof(gfloat*));
  out->freqData   = g_malloc0 (nSeries*sizeof(gfloat*));
  out->times      = g_malloc0 (out->nTime*sizeof(gfloat));
  out->freqs      = g_malloc0 (out->nFreq*sizeof(gfloat));
  out->timeSeries = g_malloc0 (nSeries*sizeof(ObitFArray*));
  out->freqSeries = g_malloc0 (nSeries*sizeof(ObitCArray*));
  ndim = 1;
  naxisr[0] = out->nTime;
  naxisc[0] = out->nFreq;
  for (i=0; i<nSeries; i++) {
    out->timeSeries[i] = ObitFArrayCreate("Time", ndim, naxisr);
    out->timeData[i]   = ObitFArrayIndex (out->timeSeries[i], pos); /* save pointer to data */
    out->freqSeries[i] = ObitCArrayCreate("Freq", ndim, naxisc);
    out->freqData[i]   = ObitCArrayIndex (out->freqSeries[i], pos); /* save pointer to data */
  }

  /* Create FFT objects */
  rank = 1;
  dim[0] = out->nTime;
  out->FFTFor = newObitFFT ("Time2FreqFFT", OBIT_FFT_Forward, 
			   OBIT_FFT_HalfComplex, rank, dim);
  out->FFTRev = newObitFFT ("Freq2TimeFFT", OBIT_FFT_Reverse, 
			   OBIT_FFT_HalfComplex, rank, dim);

  /* Interpolator object */
  out->interp = newObitFInterpolateCreate ("TimeSeriesInterpolator", 
					  out->timeSeries[0], NULL, 3);

  return out;
} /* end newObitTimeFilter */

/**
 * Returns ClassInfo pointer for the class.
 * \return pointer to the class structure.
 */
gconstpointer ObitTimeFilterGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitTimeFilterClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitTimeFilterGetClass */

/**
 * Change the sizes of the arrays on the input object to nTime.
 * \param in    ObitTimeFilter to resize.
 * \param nTime Number of times in arrays to be filtered
 *              It is best to add some extra padding (10%) to allow a smooth
 *              transition from the end of the sequence back to the beginning.
 *              Remember the FFT algorithm assumes the function is periodic.
 */
void ObitTimeFilterResize (ObitTimeFilter *in, gint nTime)
{
  gint i, rank, dim[1];
  glong ndim, naxisr[1], naxisc[1], pos[1] = {0};

  /* error checks */
  g_assert (ObitTimeFilterIsA(in));

  /* Don't bother if it's the same size */
  if (nTime==in->nTime) return;

  /* Lock ObitObjects against other threads */
  ObitThreadLock(in->thread);

  /* save inputs */
  in->nTime   = nTime;
  in->nFreq   = 1 + nTime / 2;

  /* Realloc float arrays */
  in->times  = g_realloc (in->times, in->nTime*sizeof(gfloat));
  in->freqs  = g_realloc (in->freqs, in->nFreq*sizeof(gfloat));
  
  /* Loop over series reallocating */
  ndim = 1;
  naxisr[0] = in->nTime;
  naxisc[0] = in->nFreq;
  for (i=0; i<in->nSeries; i++) {
    in->timeSeries[i] = ObitFArrayRealloc(in->timeSeries[i], ndim, naxisr);
    in->timeData[i]   = ObitFArrayIndex (in->timeSeries[i], pos); /* save pointer to data */
    in->freqSeries[i] = ObitCArrayRealloc(in->freqSeries[i], ndim, naxisc);
    in->freqData[i]   = ObitCArrayIndex (in->freqSeries[i], pos); /* save pointer to data */
  } /* end loop over series */

  /* Recreate FFT objects */
  in->FFTFor = ObitFFTUnref(in->FFTFor);  /* out with the old */
  rank = 1;
  dim[0] = in->nTime;
  in->FFTFor = newObitFFT ("Time2FreqFFT", OBIT_FFT_Forward, 
			   OBIT_FFT_HalfComplex, rank, dim);
  in->FFTRev = ObitFFTUnref(in->FFTRev);  /* out with the old */
  in->FFTRev = newObitFFT ("Freq2TimeFFT", OBIT_FFT_Reverse, 
			   OBIT_FFT_HalfComplex, rank, dim);

  /* Replace array on interpolator member */
  ObitFInterpolateReplace (in->interp, in->timeSeries[0]);

  /* Unlock ObitObjects */
  ObitThreadUnlock(in->thread);
 
} /* end ObitTimeFilterResize */

/**
 * Fourier transform all time series to frequency series
 * Any blanked values in the time series are interpolated and then the time series 
 * is FFTed to the frequency domain.  A linear interpolation between the 
 * last valid point and the first valid point is made to reduce the wraparound edge
 * effects.
 * \param in  Object with TimeFilter structures.
 */
void ObitTimeFilter2Freq (ObitTimeFilter *in)
{
  gint i;

  /* error checks */
  g_assert (ObitTimeFilterIsA(in));

  /* Lock ObitObjects against other threads */
  ObitThreadLock(in->thread);

  /* Loop over series */
  for (i=0; i<in->nSeries; i++) {

    /* Interpolated any blanked values, ensure smooth wrap around */
    ObitTimeFilterDeblank(in, in->timeSeries[i]);
    
    /* Transform */
    ObitFFTR2C (in->FFTFor, in->timeSeries[i], in->freqSeries[i]);

  } /* end loop over series */

  /* Unlock ObitObjects */
  ObitThreadUnlock(in->thread);
 
} /* end ObitTimeFilter2Freq */

/**
 * Fourier transform filtered frequency series to time series
 * \param in  Object with TimeFilter structures.
 */
void ObitTimeFilter2Time (ObitTimeFilter *in)
{
  gint i, j;
  float norm;

  /* error checks */
  g_assert (ObitTimeFilterIsA(in));

  /* Lock ObitObjects against other threads */
  ObitThreadLock(in->thread);

  /* Loop over series */
  for (i=0; i<in->nSeries; i++) {

    /* Transform */
    ObitFFTC2R (in->FFTRev, in->freqSeries[i], in->timeSeries[i]);

    /* Normalize by n */
    norm = 1.0 / in->nTime;
    if (in->nTime>1) for (j=0; j<in->nTime; j++) in->timeData[i][j] *= norm;

  } /* end loop over series */

  /* Unlock ObitObjects */
  ObitThreadUnlock(in->thread);
 
} /* end ObitTimeFilter2Time */

/**
 * Apply specified filter to specified time series.
 *
 * Following Filters are supported:
 * \li OBIT_TimeFilter_LowPass - 
 *     Zeroes frequencies above a fraction, parms[0], of the highest.
 * \li OBIT_TimeFilter_HighPass - 
 *     Zeroes frequencies below a fraction, parms[0], of the highest.
 *
 * \param in       Object with TimeFilter structures.
 * \param seriesNo Which time/frequency series to apply to (0-rel), <0 => all
 * \param type     Filter type to apply
 * \param *parm    Parameters for filter, meaning depends on type.
 * \param err    Error stack
 */
void ObitTimeFilterFilter (ObitTimeFilter *in, gint seriesNo,
			   ObitTimeFilterType type, gfloat *parms, ObitErr *err)
{
  gint hi, lo, fLo, fHi, iSeries, iFreq;
  gchar *routine = "ObitTimeFilterFilter";

  /* error checks */
  g_assert (ObitTimeFilterIsA(in));
 
  /* Lock ObitObjects against other threads */
  ObitThreadLock(in->thread);

  /* Range of series to use */
  if (seriesNo>0) {
    lo = seriesNo;
    hi = seriesNo;
  } else {
    lo = 0;
    hi = in->nSeries-1;
  }
   
  /* Apply filter by type */
  switch (type) {
  case OBIT_TimeFilter_LowPass:   /* Low pass filter */
    /* Set range of frequency values to zero */
    if (!((parms[0]>=0.0) && (parms[0]<=1.0))) { /* Check range */
      Obit_log_error(err, OBIT_Error, "%s: parm[0] out of range [0,1] %g", 
		     routine, parms[0]);
      return;
    }

    fLo = (gint)((parms[0] * in->nFreq) + 0.99999);
    fLo *= 2;
    fHi = 2 * in->nFreq;

    /* Loop over series */
    for (iSeries=lo; iSeries<=hi; iSeries++) {
      for (iFreq = fLo; iFreq<fHi; iFreq++) in->freqData[iSeries][iFreq] = 0.0;
    } /* end loop over series */
    break;
  case OBIT_TimeFilter_HighPass:  /* High pass filter */
    /* Set range of frequency values to zero */
    if (!((parms[0]>=0.0) && (parms[0]<=1.0))) { /* Check range */
      Obit_log_error(err, OBIT_Error, "%s: parm[0] out of range [0,1] %g", 
		     routine, parms[0]);
      return;
    }
    fLo = 0;
    fHi = (gint)((parms[0] * in->nFreq) + 0.5) - 1;
    fHi *= 2;

    /* Loop over series */
    for (iSeries=lo; iSeries<=hi; iSeries++) {
      for (iFreq = fLo; iFreq<fHi; iFreq++) in->freqData[iSeries][iFreq] = 0.0;
    } /* end loop over series */
    break;
  default:                        /* Unknown - barf and die */
    g_assert_not_reached(); 
  }; /* end switch on filter typr */
  
  
  /* Unlock ObitObjects */
  ObitThreadUnlock(in->thread);
} /* end ObitTimeFilterFilter */

/**
 * Initialize global ClassInfo Structure.
 */
 void ObitTimeFilterClassInit (void)
{
  const ObitClassInfo *ParentClass;

  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;

  /* Initialize (recursively) parent class first */
  ParentClass = ObitGetClass();
  ObitClassInit();  /* Initialize parent class if needed */

  /* function pointers etc. for this class */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = (ObitClassInitFP)ObitTimeFilterClassInit;
  myClassInfo.newObit       = (newObitFP)newObitTimeFilter;
  myClassInfo.ObitCopy      = NULL;
  myClassInfo.ObitClone     = NULL;
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitTimeFilterClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitTimeFilterInit;
  /* New to this class */
  myClassInfo.newObitTimeFilter  = 
    (newObitTimeFilterFP)newObitTimeFilter;
  myClassInfo.ObitTimeFilter2Freq  = 
    (ObitTimeFilter2FreqFP)ObitTimeFilter2Freq;
  myClassInfo.ObitTimeFilter2Time  = 
    (ObitTimeFilter2TimeFP)ObitTimeFilter2Time;
  myClassInfo.ObitTimeFilterFilter  = 
    (ObitTimeFilterFilterFP)ObitTimeFilterFilter;
} /* end ObitTimeFilterClassInit */

/*---------------Private functions--------------------------*/

/**
 * Creates empty member objects, initialize reference count.
 * Parent classes portions are (recursively) initialized first
 * \param inn Pointer to the object to initialize.
 */
void ObitTimeFilterInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitTimeFilter *in = inn;

  /* error checks */
  g_assert (in != NULL);

  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->thread     = newObitThread();
  in->nSeries    = 0;
  in->nTime      = 0;
  in->nFreq      = 0;
  in->times      = NULL;
  in->freqs      = NULL;
  in->timeData   = NULL;
  in->freqData   = NULL;
  in->timeSeries = NULL;
  in->freqSeries = NULL;
  in->FFTFor     = NULL;
  in->FFTRev     = NULL;
  in->interp     = NULL;

} /* end ObitTimeFilterInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * For some reason this wasn't build into the GType class.
 * \param  inn Pointer to the object to deallocate.
 *         Actually it should be an ObitTimeFilter* cast to an Obit*.
 */
void ObitTimeFilterClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitTimeFilter *in = inn;
  gint i;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* delete this class members */
  in->thread    = ObitThreadUnref(in->thread);
  in->FFTFor = ObitFFTUnref(in->FFTFor);
  in->FFTRev = ObitFFTUnref(in->FFTRev);
  in->interp = ObitFInterpolateUnref(in->interp);
  if (in->times) g_free(in->times);
  if (in->freqs) g_free(in->freqs);
  for (i=0; i < in->nSeries; i++) {
    if (in->timeSeries) in->timeSeries[i] = ObitFArrayUnref(in->timeSeries[i]);
    if (in->freqSeries) in->freqSeries[i] = ObitCArrayUnref(in->freqSeries[i]);
  }
  if (in->timeData) g_free(in->timeData);
  if (in->freqData) g_free(in->freqData);
  if (in->timeSeries) g_free(in->timeSeries);
  if (in->freqSeries) g_free(in->freqSeries);

  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);
  
} /* end ObitTimeFilterClear */


/**
 * Replace blanks in an array by interpolation.
 * Valid values are extended into blanked regions that cannot be interpolated.
 * \param in    Object with Interpolation structures.
 * \param array FArray to deblank.
 */
static void  ObitTimeFilterDeblank (ObitTimeFilter *in, ObitFArray *array)
{
  gfloat *data, good, value, first, last, w1, w2, iDelta;
  gfloat fblank = ObitMagicF();
  glong i, n, iFirst, iLast, pos[1] = {0};
  gboolean blanked = FALSE;

  /* error checks */
  g_assert (ObitTimeFilterIsA(in));
  g_assert (ObitFArrayIsA(array));

  /* Get array pointer */
  data = ObitFArrayIndex (array, pos);
  n = array->naxis[0];

  /* Is there anything to do? */
  good = fblank;
  iFirst = -1;
  first = data[0];
  iLast = -1;
  last = data[n-1];
  for (i=0; i<n; i++) {
    blanked = blanked || (data[i] == fblank);
    if (data[i] != fblank) good = data[i];
    /* Find first and last good points */
    if ((iFirst<0) && (data[i]!=fblank)) {
      iFirst = i;
      first = data[i];
    }
    if (data[i]!=fblank) {
      iLast = i;
      last = data[i];
    }
  }
  if (!blanked) return;

  /* If it's all blanked, zero fill and return */
  if (good==fblank) {
    for (i=0; i<n; i++) data[i] = 0.0;
    return;
  }

  /* replace array on interpolator */
  ObitFInterpolateReplace (in->interp, array);

  /* Working value needed for smooth wrap around */
  iDelta = 1.0 / ((gfloat)MAX(1, (n-iLast+iFirst)));

  /* interpolate if possible */
  for (i=0; i<n; i++) {
    if (data[i]==fblank) { /* need to interpolate or end wrap? */
      if (i<iFirst) { /* beginning - enforce smooth transition */
	w1 = (iFirst - i) * iDelta;
	w2 = 1.0 - w1;
	value = w1 * last + w2 * first;
      } else if (i>iLast) { /* end - enforce smooth transition */
	w2 = (i-iLast) * iDelta;
	w1 = 1.0 - w2;
	value = w1 * last + w2 * first;
      } else { /* in middle - interpolate */
	value = ObitFInterpolate1D (in->interp, (gfloat)(i+1.0));
      }

      /* if it's good - use it */
      if (value!=fblank) { /* good */
	data[i] = value;
	good = value;
      } else { /* bad - use last good value */
	data[i] = good;
      }
    } else { /* this one OK on input, save as last good */
      good = data[i];
    }

  } /* end loop interpolating */

} /* end ObitTimeFilterDeblank */

