/* $Id: ObitUVWeight.c,v 1.7 2005/08/03 16:12:08 bcotton Exp $    */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include <math.h>
#include "ObitUVWeight.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitUVWeight.c
 * ObitUVWeight class function definitions.
 * This class is derived from the Obit base class.
 */

/** name of the class defined in this file */
static gchar *myClassName = "ObitUVWeight";

/** Degrees to radians factor */
#ifndef DG2RAD  
#define DG2RAD G_PI / 180.0
#endif

/**  Radians to degrees factor */
#ifndef RAD2DG  
#define RAD2DG 180.0 / G_PI
#endif

/*----------------- Macroes ---------------------------*/
/** 
 * Macro to unreference (and possibly destroy) an ObitUVWeight
 * returns a ObitUVWeight*.
 * in = object to unreference
 */
#define ObitUVWeightUnref(in) ObitUnref (in)

/** 
 * Macro to reference (update reference count) an ObitUVWeight.
 * returns a ObitUVWeight*.
 * in = object to reference
 */
#define ObitUVWeightRef(in) ObitRef (in)

/** 
 * Macro to determine if an object is the member of this or a 
 * derived class.
 * Returns TRUE if a member, else FALSE
 * in = object to reference
 */
#define ObitUVWeightIsA(in) ObitIsA (in, ObitUVWeightGetClass())

/*--------------- File Global Variables  ----------------*/
/**
 * ClassInfo structure ObitUVWeightClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitUVWeightClassInfo myClassInfo = {FALSE};

/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitUVWeightInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitUVWeightClear (gpointer in);

/** Private: Get input parameters from uvdata */
void ObitUVWeightInput (ObitUVWeight *in, ObitUV *uvdata, ObitErr *err);

/** Private: Read uv data accumulating to grid */
void ObitUVWeightReadUV (ObitUVWeight *in, ObitUV *UVin, ObitErr *err);

/** Private: Reweight uv data accumulating to grids */
void ObitUVWeightWtUV (ObitUVWeight *in, ObitUV *UVin, ObitErr *err);

/** Private: Prepare visibility data for gridding weights*/
static void PrepBuffer (ObitUVWeight* in, ObitUV *uvdata);

/** Private: convolve a uv data buffer and sum to grid */
static void GridBuffer (ObitUVWeight* in, ObitUV *uvdata);

/** Private: Process grid */
static void ProcessGrid (ObitUVWeight* in, ObitErr *err);

/** Private: Apply corrections for a buffer of data */
static void WeightBuffer (ObitUVWeight* in, ObitUV *uvdata);

/** Private: Fill convolving function table */
static void ConvFunc (ObitUVWeight* in);

/*----------------------Public functions---------------------------*/
/**
 * Constructor.
 * Initializes class if needed on first call.
 * \param name An optional name for the object.
 * \return the new object.
 */
ObitUVWeight* newObitUVWeight (gchar* name)
{
  ObitUVWeight* out;

  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitUVWeightClassInit();

  /* allocate/init structure */
  out = g_malloc0(sizeof(ObitUVWeight));

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

  /* set ClassInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  ObitUVWeightInit((gpointer)out);

 return out;
} /* end newObitUVWeight */

/**
 * Returns ClassInfo pointer for the class.
 * \return pointer to the class structure.
 */
gconstpointer ObitUVWeightGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitUVWeightClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitUVWeightGetClass */

/**
 * Convolves UV weights onto a grid to determine weighting function.
 * Then the data weights are modified by the weighting function.
 * The control parameters are attached to the ObitInfoList member info
 * on uvdata.  See ObitUVWeight class documentation for details
 */
void ObitUVWeightData (ObitUV *uvdata, ObitErr *err)
{
  ObitUVWeight *myWeight = NULL;
  gchar *outName = NULL;
  glong naxis[2];
  gboolean doUnifWt;
  gchar *routine = "ObitUVWeightData";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitUVIsA(uvdata));

  /* create object */
  outName = g_strconcat("UVWeight for: ",uvdata->name, NULL);
  myWeight = newObitUVWeight(outName);
  g_free(outName);

  /* get weighting information */
  ObitUVWeightInput (myWeight, uvdata, err);
  if (err->error) Obit_traceback_msg (err, routine, uvdata->name);

  /* frequency tables if not defined */
  if ((uvdata->myDesc->freqArr==NULL) || (uvdata->myDesc->fscale==NULL)) {
    ObitUVGetFreq (uvdata, err);
    if (err->error) Obit_traceback_msg (err, routine, uvdata->name);
  } /* end setup frequency table */

  /* Are we uniform Weighting? */
  doUnifWt = myWeight->Robust < 7;

  /* Gridding for uniform weighting */
  if (doUnifWt) {
    /* Set convolving function */
    ConvFunc(myWeight);
    
    /* Create weighting grids */
    naxis[0] = 1 + myWeight->nuGrid/2;
    naxis[1] = myWeight->nvGrid;
    myWeight->cntGrid = ObitFArrayCreate ("Count Grid", 2, naxis);
    myWeight->wtGrid  = ObitFArrayCreate ("Weight Grid", 2, naxis);
    
    /* Get/grid weights if uniform weighting */
    ObitUVWeightReadUV (myWeight, uvdata, err);
    if (err->error) Obit_traceback_msg (err, routine, uvdata->name);
    
    /* Process grid */
    ProcessGrid (myWeight, err);
    if (err->error) Obit_traceback_msg (err, routine, uvdata->name);

    /* Informative messages */
     Obit_log_error(err, OBIT_InfoErr, 
		    "Using Robust uniform weighting for %s", uvdata->name);

     /* debug
     fprintf (stderr,"Grid size %ld %ld \n",naxis[0],naxis[1]); */
  } else {
    /* Only natural weighting (and taper and power)*/
    myWeight->wtScale = 1.0;
    myWeight->temperance  = 0.0;

    /* Informative messages */
     Obit_log_error(err, OBIT_InfoErr, 
		    "Using natural weighting for %s", uvdata->name);
  } /* End of natural weighting */

  /* Modify Weights */ 
  ObitUVWeightWtUV (myWeight, uvdata, err);
  if (err->error) Obit_traceback_msg (err, routine, uvdata->name);

  /* final diagnostics */
     Obit_log_error(err, OBIT_InfoErr, 
		    "Weighting increased noise by %f for %s", 
		    myWeight->noiseFactor, uvdata->name);

  /* cleanup */
  myWeight = ObitUVWeightUnref(myWeight);
} /* end ObitUVWeightData */

/**
 * Initialize global ClassInfo Structure.
 */
 void ObitUVWeightClassInit (void)
{
  const ObitClassInfo *ParentClass;

  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;

  /* Initialize (recursively) parent class first */
  ParentClass = ObitGetClass();
  ObitClassInit();  /* Initialize parent class if needed */

  /* function pointers etc. for this class */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = 
    (ObitClassInitFP)ObitUVWeightClassInit;
  myClassInfo.newObit       = (newObitFP)newObitUVWeight;
  myClassInfo.ObitCopy      = NULL;
  myClassInfo.ObitClone     = NULL;
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitUVWeightClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitUVWeightInit;
  myClassInfo.ObitUVWeightData   = 
    (ObitUVWeightDataFP)ObitUVWeightData;
} /* end ObitUVWeightClassInit */

/*---------------Private functions--------------------------*/

/**
 * Creates empty member objects, initialize reference count.
 * Parent classes portions are (recursively) initialized first
 * \param inn Pointer to the object to initialize.
 */
void ObitUVWeightInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitUVWeight *in = inn;

  /* error checks */
  g_assert (in != NULL);

  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->cntGrid    = NULL;
  in->wtGrid     = NULL;
  in->convfn     = NULL;

} /* end ObitUVWeightInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * For some reason this wasn't build into the GType class.
 * \param  inn Pointer to the object to deallocate.
 *           Actually it should be an ObitUVWeight* cast to an Obit*.
 */
void ObitUVWeightClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitUVWeight *in = inn;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* delete this class members */
  in->cntGrid   = ObitFArrayUnref(in->cntGrid);  
  in->wtGrid    = ObitFArrayUnref(in->wtGrid);  
  in->convfn    = ObitFArrayUnref(in->convfn);
  
  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);
  
} /* end ObitUVWeightClear */

/**
 * Read input parameters from uvdata.
 * The control parameters are attached to the ObitInfoList member info
 * on uvdata.  See ObitUVWeight Class documentation for details.
 * \param uvdata ObitUV whose data is to be weighted.
 * \param err    ObitErr stack for reporting problems.
 */
void ObitUVWeightInput (ObitUVWeight *in, ObitUV *uvdata, ObitErr *err)
{
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM];
  gfloat temp, xCells, yCells, *fptr;
  gint   itemp, *iptr;
  gboolean gotIt;
  gchar *routine = "ObitUVWeightInput";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitUVIsA(uvdata));
  g_assert (ObitInfoListIsA(uvdata->info));

  /* Weighting grid size */
  itemp = 0;  
  gotIt = ObitInfoListGetP(uvdata->info, "nuGrid", &type, dim, (gpointer*)&iptr);
  if (gotIt) in->nuGrid = *iptr;
  else { /* better have "nx" */
    if (!ObitInfoListGetP(uvdata->info, "nx", &type, dim, (gpointer*)&iptr)) {
      Obit_log_error(err, OBIT_Error, 
		     "%s: MUST define grid size for %s", routine, uvdata->name);
      return;
    }
    in->nuGrid = *iptr;
  }
  /* Should be odd */
  in->nuGrid = ((in->nuGrid-1)/2) * 2 + 1;

  gotIt = ObitInfoListGetP(uvdata->info, "nvGrid", &type, dim, (gpointer*)&iptr);
  if (gotIt) in->nvGrid = *iptr;
  else { /* better have "ny" */
    if (!ObitInfoListGetP(uvdata->info, "ny", &type, dim, (gpointer*)&iptr)) {
      Obit_log_error(err, OBIT_Error, 
		     "%s: MUST define grid size for %s", routine, uvdata->name);
      return;
    }
    in->nvGrid = *iptr;
  }
  /* Should be odd */
  in->nvGrid = ((in->nvGrid-1)/2) * 2 + 1;
  
  /* Image cell size */
  if (!ObitInfoListGetP(uvdata->info, "xCells", &type, dim, (gpointer*)&fptr)) {
    Obit_log_error(err, OBIT_Error, 
		   "%s: MUST define cell spacing for %s", routine, uvdata->name);
    return;
  }
  xCells = (*fptr)/3600.0;

  if (!ObitInfoListGetP(uvdata->info, "yCells", &type, dim, (gpointer*)&fptr)) {
    Obit_log_error(err, OBIT_Error, 
		   "%s: MUST define cell spacing for %s", routine, uvdata->name);
    return;
  }
  yCells = (*fptr)/3600.0;

  /* Taper [default none] */
  temp = 0.0;
  ObitInfoListGetTest(uvdata->info, "Taper", &type, dim, &temp);
  in->sigma2u = -temp*temp;
  in->sigma2v = -temp*temp;

  /* WtBox */
  itemp = 0;
  ObitInfoListGetTest(uvdata->info, "WtBox", &type, dim, &itemp);
  in->WtBox = itemp;
 
 /* WtFunc */
  itemp = 1;
  ObitInfoListGetTest(uvdata->info, "WtFunc", &type, dim, &itemp);
  in->WtFunc = itemp;
 
 /* Robust */
  temp = 0.0;
  ObitInfoListGetTest(uvdata->info, "Robust", &type, dim, &temp);
  in->Robust = temp;
 
 /* Weight power */
  temp = 1.0;
  ObitInfoListGetTest(uvdata->info, "WtPower", &type, dim, &temp);
  in->WtPower = fabs (temp);
 
  /* baseline range */
  temp = 1.0e15;
  ObitInfoListGetTest(uvdata->info, "MaxBaseline", &type, dim, &temp);
  in->blmax = temp;
  temp = 0.0;
  ObitInfoListGetTest(uvdata->info, "MinBaseline", &type, dim, &temp);
  in->blmin = temp;

  /* set uv to cells factors */
  in->UScale = in->nuGrid * (DG2RAD * fabs(xCells));
  in->VScale = in->nvGrid * (DG2RAD * fabs(yCells));

} /* end ObitUVWeightInput */

/**
 * Read a UV data object, applying any taper or shift and accumulating to grid.
 * Buffering of data will use the buffers as defined on UVin 
 * ("nVisPIO" in info member).
 * The UVin object will be closed at the termination of this routine.
 * Requires setup by #ObitUVWeightCreate.
 * The gridding information should have been stored in the ObitInfoList on in:
 * \li "Taper" OBIT_float scalar = UV taper width in wavelengths.
 *             Default = no taper.
 * \li "Guardband" OBIT_float scalar = maximum fraction of U or v range allowed in grid.
 *             Default = 0.4.
 * \li "MaxBaseline" OBIT_float scalar = maximum baseline length in wavelengths.
 *             Default = 1.0e15.
 * \li "startChann" OBIT_int scalar = first channel (1-rel) in uv data to grid.
 *             Default = 1.
 * \li "numberChann" OBIT_int scalar = number of channels in uv data to grid.
 *             Default = all.
 * \param in      Object to initialize
 * \param UVin    Uv data object to be gridded.
 *                Should be the same as passed to previous call to 
 *                #ObitUVWeightSetup for input in.
 * \param err     ObitErr stack for reporting problems.
 */
void ObitUVWeightReadUV (ObitUVWeight *in, ObitUV *UVin, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_OK;
  gchar *routine = "ObitUVWeightReadUV";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitUVWeightIsA(in));
  g_assert (ObitUVIsA(UVin));
  g_assert (ObitUVDescIsA(UVin->myDesc));
  g_assert (UVin->myDesc->fscale!=NULL); /* frequency scaling table */

  retCode = ObitUVOpen (UVin, OBIT_IO_ReadOnly, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* loop gridding data */
  while (retCode == OBIT_IO_OK) {

    /* read buffer */
    retCode = ObitUVRead (UVin, NULL, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
    
    /* prepare data */
    PrepBuffer (in, UVin);
    
    /* grid */
    GridBuffer (in, UVin);
  } /* end loop reading/gridding data */

  /* Close data */
  retCode = ObitUVClose (UVin, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);
} /* end ObitUVWeightReadUV  */

/**
 * Apply any weighting to the uvdata in UVin and rewrite
 * Compute the increase in the noise due to the weighting.
 * \param in      Weighting object.
 * \param UVin    Uv data object to be corrected.
 * \param err     ObitErr stack for reporting problems.
 */
void ObitUVWeightWtUV (ObitUVWeight *in, ObitUV *UVin, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_OK;
  gdouble sumInWt, sumOutWt, sumO2IWt, fract;
  glong firstVis;
  gchar *routine = "ObitUVWeightWtUV";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitUVWeightIsA(in));
  g_assert (ObitUVIsA(UVin));
  g_assert (ObitUVDescIsA(UVin->myDesc));
  g_assert (UVin->myDesc->fscale!=NULL); /* frequency scaling table */

  retCode = ObitUVOpen (UVin, OBIT_IO_ReadWrite, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Init noise factor info */
  in->noiseFactor = 1.0;
  in->wtSums[0] = 0.0; /* Sum of input weights      */
  in->wtSums[1] = 0.0; /* Sum of output weights     */
  in->wtSums[2] = 0.0; /* Sum Out_wt*Out_wt / In_wt */ 
  in->numberBad = 0;   /* Number of visibilities outside of the inner 90% */ 

  /* loop correcting data */
  while (retCode == OBIT_IO_OK) {

    /* read buffer */
    retCode = ObitUVRead (UVin, NULL, err);
    if (retCode == OBIT_IO_EOF) break; /* done? */
    if (err->error) Obit_traceback_msg (err, routine, in->name);
    firstVis = UVin->myDesc->firstVis;
    
    /* Apply weighting */
    WeightBuffer (in, UVin);

    /* rewrite buffer */
    retCode = ObitUVWrite (UVin, NULL, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
    UVin->myDesc->firstVis = firstVis;  /* reset first vis in buffer */
    ((ObitUVDesc*)UVin->myIO->myDesc)->firstVis = firstVis;
    
  } /* end loop weighting data */

  /* Close data */
  retCode = ObitUVClose (UVin, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Calculate noise increase factor */
  sumInWt  = in->wtSums[0];
  sumOutWt = in->wtSums[1];
  sumO2IWt = in->wtSums[2];

  /* Make sure all data not flagged */
  if (sumOutWt<=0.0) {
    Obit_log_error(err, OBIT_Error, 
		    "ERROR: All data flagged for %s", in->name);
    return;
   }

  if (sumOutWt < 1.0e-9) sumOutWt = 1.0;
  in->noiseFactor = sumO2IWt * sumInWt / (sumOutWt*sumOutWt);
  in->noiseFactor = sqrt (MAX (0.0, in->noiseFactor));

  /* Check for excessive numbers of visibilities flagged for being outside
     the inner 90% of the grid */
  fract = in->numberBad;
  fract = 100.0 * fract / UVin->myDesc->nvis;
  if (fract > 1.0) {
      Obit_log_error(err, OBIT_InfoWarn, 
		    "WARNING: %6.1f percent of data flagged outside of inner 0.9 for %s", 
		     fract, UVin->name);
  }

} /* end ObitUVWeightWtUV  */

/**
 * Prepares a buffer load of visibility data for gridding:
 * \li Convert to cells at the reference frequency.
 * \li enforce guardband - no data near outer edges of grid 
 * \li All data should be converted to the positive V half plane.
 * \param in      Object with grid to accumulate.
 * \param uvdata  Object with uvdata in buffer.
 */
static void PrepBuffer (ObitUVWeight* in, ObitUV *uvdata)
{
  glong ivis, nvis, ifreq, nif, iif, nfreq, loFreq, hiFreq;
  gfloat *u, *v, *w, *vis, *ifvis, *vvis;
  gfloat bl2, blmax2, blmin2, wt, guardu, guardv;
  ObitUVDesc *desc;
  gboolean flip, doFlag, doPower, doOne;

  /* error checks */
  g_assert (ObitUVWeightIsA(in));
  g_assert (ObitUVIsA(uvdata));
  g_assert (uvdata->myDesc != NULL);
  g_assert (uvdata->buffer != NULL);

  /* how much data? */
  desc  = uvdata->myDesc;
  nvis  = desc->numVisBuff;
  if (nvis<=0) return; /* need something */
  nfreq = desc->inaxes[desc->jlocf];
  nif = 1;
  if (desc->jlocif>=0) nif = desc->inaxes[desc->jlocif];
  
  /* range of channels (0-rel) */
  loFreq = 0;
  hiFreq = nfreq-1;

 /* initialize data pointers */
  u   = uvdata->buffer+desc->ilocu;
  v   = uvdata->buffer+desc->ilocv;
  w   = uvdata->buffer+desc->ilocw;
  vis = uvdata->buffer+desc->nrparm;

  /* what needed */
  /* Raising weight to a power? */
  doPower = (fabs (in->WtPower-1.0) > 0.01) && (in->WtPower > 0.01);
  /* Replacing weights with 1.0? */
  doOne = (in->WtPower < 0.01);

 /* Baseline max, min values */
  blmax2 = in->blmax * in->blmax;
  blmin2 = in->blmin * in->blmin;

  /* guardband (90%)in wavelengths */
  guardu = (0.9 * (gfloat)in->wtGrid->naxis[0]) / fabs(in->UScale);
  guardv = (0.9 * ((gfloat)in->wtGrid->naxis[1])/2) / fabs(in->VScale);

  /* Loop over visibilities */
  for (ivis=0; ivis<nvis; ivis++) {

    /* check extrema */
    bl2 = (*u)*(*u) + (*v)*(*v);
    doFlag = ((bl2<blmin2) || (bl2>blmax2));
    /* enforce guardband */
    doFlag = doFlag || ((fabs(*u)>guardu) || (fabs(*v)>guardv));

    /* in the correct half plane? */
    flip = (*u) <= 0.0;

    /* loop over IFs */
    ifvis = vis;
    for (iif = 0; iif<nif; iif++) {

      /* loop over frequencies */
      vvis = ifvis;
      for (ifreq = loFreq; ifreq<=hiFreq; ifreq++) {

	/* is this one wanted? */
	if (doFlag)  vvis[2] = 0.0;  /* baseline out of range? */
	
	wt = vvis[2];                /* data weight */
	if (wt <= 0.0) continue;
	
	/* Replacing weights with one? */
	if (doOne) vis[3] = 1.0;

	/* Weights to a power? */
	if (doPower && (vis[3]>0.0)) vis[3] = pow (vis[3], in->WtPower);
	
	vvis += desc->incf; /* visibility pointer */
      } /* end loop over frequencies */
      ifvis += desc->incif; /* visibility pointer */
    } /* Loop over IFs */

    /* Scale u,v to cells at reference frequency */
    if (flip) { /* put in other half plane */
      *u = -((*u) * in->UScale);
      *v = -((*v) * in->VScale);
    } else { /* no flip */
      *u = (*u) * in->UScale;
      *v = (*v) * in->VScale;
    }

    /* update data pointers */
    u += desc->lrec;
    v += desc->lrec;
    w += desc->lrec;
    vis += desc->lrec;
  } /* end loop over visibilities */
} /* end PrepBuffer */

/**
 * Convolves weights in buffer on uvdata onto the grid member of in.
 * Rows in the grid are in U and the data should have all been converted to the 
 * positive U half plane.
 * U, V, should be in cells and data not to be included on the grid should 
 * have zero weight.  Convolution functions must be created.
 * This uses two grids, one for the counts of visibilities in cells and the other
 * for the sum of the weights.  These are needed for Briggs Robust weighting.
 * \param in      Object with grid to accumulate
 * \param uvdata  Object with uv data in buffer, prepared for gridding.
 */
static void GridBuffer (ObitUVWeight* in, ObitUV *uvdata)
{
  glong ivis, nvis, ifreq, nfreq, ncol, iu, iv, icu, icv, lGridRow, lGridCol, itemp;
  glong iif, ifq, nif, loFreq, hiFreq, uoff, voff, uuoff=0.0, vvoff, vConvInc, uConvInc;
  gfloat *grid, *ggrid, *cntGrid, *u, *v, *w, *vis, *vvis, *ifvis, *wt;
  gfloat *convfnp, weight, rtemp, uf, vf;
  gint fincf, fincif;
  glong pos[] = {0,0,0,0,0};
  ObitUVDesc *desc;

  /* error checks */
  g_assert (ObitUVWeightIsA(in));
  g_assert (ObitUVIsA(uvdata));
  g_assert (in->cntGrid != NULL);
  g_assert (in->wtGrid != NULL);
  g_assert (uvdata->myDesc != NULL);
  g_assert (uvdata->buffer != NULL);

  /* how much data? */
  desc  = uvdata->myDesc;
  nvis  = desc->numVisBuff;
  if (nvis<=0) return; /* need something */
  nfreq = desc->inaxes[desc->jlocf];
  nif = 1;
  if (desc->jlocif>=0) nif = desc->inaxes[desc->jlocif];
 
  /* range of channels (0-rel) */
  loFreq = 0;
  hiFreq = nfreq-1;

  /* Channel and IF increments in frequcncy scaling array */
  fincf  = desc->incf  / 3;
  fincif = desc->incif / 3;

 /* initialize data pointers */
  u   = uvdata->buffer+desc->ilocu;
  v   = uvdata->buffer+desc->ilocv;
  w   = uvdata->buffer+desc->ilocw;
  vis = uvdata->buffer+desc->nrparm;

  lGridRow = in->cntGrid->naxis[0]; /* length of row */
  lGridCol = in->cntGrid->naxis[1]; /* length of column */

  /* convolution fn pointer */
  pos[0] = 0;
  convfnp = ObitFArrayIndex (in->convfn, pos);

  /* Loop over visibilities */
  for (ivis=0; ivis<nvis; ivis++) {

    /* loop over IFs */
    ifvis = vis;
    for (iif=0; iif<nif; iif++) {

   /* loop over frequencies */
       vvis = ifvis;
      for (ifreq = loFreq; ifreq<=hiFreq; ifreq++) {
	ifq = iif*fincif + ifreq*fincf;  /* index in IF/freq table */

	/* is this one wanted? */
	wt = vvis + 2; /* data weight */
	if (*wt <= 0.0) continue;

	/* weight to grid */
	weight = (*wt);
	
	/* Scale u,v for frequency (w not used) - already in cells and correct half plane */
	uf = (*u) * desc->fscale[ifq];
	vf = (*v) * desc->fscale[ifq];
	
	/* get center cell */
	if (vf > 0.0) iv = (glong)(vf + 0.5);
	else iv = (glong)(vf - 0.5);
	iu = (glong)(uf + 0.5);

	/* Add this visibility to the count grid */
	pos[0] = iu;
	pos[1] = iv + lGridCol/2;
	cntGrid = ObitFArrayIndex (in->cntGrid, pos); /* pointer in grid */

	/* Check if datum in grid - cntGrid != NULL */
	if  (cntGrid != NULL) {
	  *cntGrid += 1.0;  /* increment count */
	
	  /* convolve weight onto the weight grid */
	  /* back off half Kernel width */
	  iu -= in->convWidth/2;
	  iv -= in->convWidth/2;
	  
	  /* Starting convolution location, table has in->convNperCell points per cell */
	  /* Determine fraction of the cell to get start location in convolving table. */
	  if (uf > 0.0) itemp = (glong)(uf + 0.5);
	  else itemp = ((glong)(uf - 0.5));
	  rtemp = in->convNperCell*(itemp - (uf) - 0.5);
	  if (rtemp > 0.0) rtemp += 0.5;
	  else rtemp -= 0.5;
	  uoff = (glong)rtemp + in->convNperCell;
	  /* Increment between u convolving */
	  uConvInc = in->convNperCell;
	  
	  /* now v convolving fn */
	  if (vf > 0.0) itemp = (glong)(vf + 0.5);
	  else itemp = ((glong)(vf - 0.5));
	  rtemp = in->convNperCell*(itemp - (vf) - 0.5);
	  if (rtemp > 0.0) rtemp += 0.5;
	  else rtemp -= 0.5;
	  voff = (glong)rtemp + in->convNperCell;
	  /* Increment between v convolving entries */
	  vConvInc = in->convWidth * in->convNperCell + 1;
	  voff = voff * vConvInc;
	  
	  /* if too close to the center, have to break up and do conjugate halves */
	  if (iu >= 0) { /* all in same half */
	    ncol = in->convWidth;
	    pos[0] = iu;
	    pos[1] = iv + lGridCol/2;
	    
	  } else { 
	    /* have to split - grid part in conjugate half */
	    pos[0] = -iu;
	    pos[1] = -iv + lGridCol/2;
	    grid = ObitFArrayIndex (in->wtGrid, pos); /* pointer in grid */
	    ncol = -iu;
	    vvoff = voff;
	    for (icv=0; icv<in->convWidth; icv++) {
	      uuoff = uoff;
	      ggrid  = grid;
	      for (icu=0; icu<=ncol; icu++) {
		*ggrid   += weight * convfnp[uuoff+vvoff];
		uuoff += uConvInc;  /* Convolution kernel pointer */
		ggrid -= 1; /* gridding pointer - opposite of normal gridding */
	      } /* end inner gridding loop */
	      vvoff += vConvInc;  /* Convolution kernel pointer */
	      grid -= lGridRow; /* gridding pointer - reverse direction for conjugate */
	    } /* end outer loop */
	    
	    /* set up for rest of grid */
	    ncol = (in->convWidth + iu); /* how many columns left? */
	    iu = 0;      /* by definition  start other half plane at iu=0 */
	    pos[0] = iu; 
	    pos[1] = iv + lGridCol/2;
	    uoff = uuoff - in->convNperCell; /* for other half in u */
	  } /* End of dealing with conjugate portion */
	  
	  /* main loop gridding */
	  grid = ObitFArrayIndex (in->wtGrid, pos); /* pointer in grid */
	  vvoff = voff;
	  for (icv=0; icv<in->convWidth; icv++) {
	    uuoff = uoff;
	    ggrid  = grid;
	    for (icu=0; icu<ncol; icu++) {
	      *ggrid   += weight * convfnp[uuoff+vvoff];
	      uuoff += uConvInc;  /* Convolution kernel pointer */
	      ggrid += 1; /* gridding pointer */
	    } /* end inner gridding loop */
	    vvoff += vConvInc;  /* Convolution kernel pointer */
	    grid += lGridRow; /* gridding pointer */
	  } /* end outer gridding loop */
	  
	  vvis += desc->incf; /* visibility pointer */
	} /* End of if datum in grid */
      } /* end loop over frequencies */
      ifvis += desc->incif; /* visibility pointer */
    } /* Loop over IFs */

    /* update data pointers */
    u += desc->lrec;
    v += desc->lrec;
    w += desc->lrec;
    vis += desc->lrec;
  } /* end loop over visibilities */
} /* end GridBuffer */

/**
 * Processes grid for Robust weighting.
 * Calculates:
 * \li temperance = factor for Briggs Robust weighting
 * \li wtScale = weighting normalization factor.
 * \param in   Object with grid.
 * \param err  Object for informative messages and errors.
 */
static void ProcessGrid (ObitUVWeight* in, ObitErr *err)
{
  gfloat sumWtCnt, sumCnt;
  /* error checks */
  g_assert (ObitUVWeightIsA(in));
  g_assert (in->cntGrid != NULL);
  g_assert (in->wtGrid != NULL);

  /* Get Sum of Weights*counts */
  sumWtCnt = ObitFArrayDot(in->cntGrid, in->wtGrid);

  /* Get sum of counts */
  sumCnt = ObitFArraySum(in->cntGrid);

  /* Robust temperance value */
  in->wtScale    = sumWtCnt / MAX (1.0, sumCnt);
  in->temperance = in->wtScale * pow (10.0, in->Robust) / 5.0;

  /* If Robust out of range turn it off */
  if ((in->Robust<-7.0) || (in->Robust>7.0)) in->temperance = 0.0;

  /* Weight scaling factor */
  in->wtScale += in->temperance;

} /* end ProcessGrid */

/**
 * Corrects data in buffer using weighting grid.
 * Adds temperance factor in->temperance and multiplies by in->wtScale
 * \param in      Object with grid to accumulate
 * \param uvdata  Object with uv data in buffer, prepared for gridding.
 */
static void WeightBuffer (ObitUVWeight* in, ObitUV *uvdata)
{
  glong ivis, nvis, ifreq, nfreq, iu, iv, lGridRow, lGridCol=0;
  glong ifq, iif, nif, loFreq, hiFreq;
  gfloat *grid, *u, *v, *w, *vis, *vvis, *ifvis, *wt;
  gfloat tape, tfact, inWt, outWt, guardu, guardv, uf, vf, minWt;
  gfloat ucell, vcell, uucell, vvcell;
  glong pos[] = {0,0,0,0,0};
  gint fincf, fincif;
  ObitUVDesc *desc;
  gboolean doPower, doOne, doTaper, doUnifWt, doFlag;
  gdouble sumInWt, sumOutWt, sumO2IWt,numberBad ;

  /* error checks */
  g_assert (ObitUVWeightIsA(in));
  g_assert (ObitUVIsA(uvdata));
  g_assert (uvdata->myDesc != NULL);
  g_assert (uvdata->buffer != NULL);

  /* initialize weighting sums */
  sumInWt  = in->wtSums[0];
  sumOutWt = in->wtSums[1];
  sumO2IWt = in->wtSums[2];
  numberBad = in->numberBad;

  /* how much data? */
  desc  = uvdata->myDesc;
  nvis  = desc->numVisBuff;
  if (nvis<=0) return; /* need something */
  nfreq = desc->inaxes[desc->jlocf];
  nif = 1;
  if (desc->jlocif>=0) nif = desc->inaxes[desc->jlocif];

  /* Minimum allowed weight */
  minWt = 1.0e-15;
  if (in->temperance>0.0) minWt = 1.0e-7 * in->temperance;
  if (minWt==0.0)  minWt = 1.0e-25;
 
  /* Channel and IF increments in frequency scaling array */
  fincf  = desc->incf  / 3;
  fincif = desc->incif / 3;

  /* range of channels (0-rel) */
  loFreq = 0;
  hiFreq = nfreq-1;

 /* initialize data pointers */
  u   = uvdata->buffer+desc->ilocu;
  v   = uvdata->buffer+desc->ilocv;
  w   = uvdata->buffer+desc->ilocw;
  vis = uvdata->buffer+desc->nrparm;

  /* what needed */
  /* Need taper? */
  doTaper = (in->sigma2u!=0.0) || (in->sigma2v!=0.0);
  /* Raising weight to a power? */
  doPower = (fabs (in->WtPower-1.0) > 0.01) && (in->WtPower > 0.01);
  /* Replacing weights with 1.0? */
  doOne = (in->WtPower < 0.01);
  /* Uniform Weighting */
  doUnifWt = in->Robust < 7;

  if (doUnifWt) {
    g_assert (in->wtGrid != NULL);

    lGridRow = in->wtGrid->naxis[0]; /* length of row */
    lGridCol = in->wtGrid->naxis[1]; /* length of column */
    /* guardband (90%)in wavelengths */
    guardu = (0.9 * (gfloat)in->wtGrid->naxis[0]) / fabs(in->UScale);
    guardv = (0.9 * ((gfloat)in->wtGrid->naxis[1])/2) / fabs(in->VScale);
  } else { /* Not uniform weighting */
    guardu = 1.0e30;
    guardv = 1.0e30;
  }

  /* Loop over visibilities */
  for (ivis=0; ivis<nvis; ivis++) {

    /* enforce guardband */
    doFlag = FALSE;
    if ((fabs(*u)>guardu) || (fabs(*v)>guardv)) {
      doFlag = TRUE;
      numberBad++;
    }

    /* Scale u,v to cells at reference frequency */
    if ((*u) <= 0.0) { /* put in other half plane */
      ucell = -((*u) * in->UScale);
      vcell = -((*v) * in->VScale);
    } else { /* no flip */
      ucell = (*u) * in->UScale;
      vcell = (*v) * in->VScale;
    }
 
    /* loop over IFs */
    ifvis = vis;
    for (iif=0; iif<nif; iif++) {

      /* loop over frequencies */
      vvis = ifvis;
      for (ifreq = loFreq; ifreq<=hiFreq; ifreq++) {
	ifq = iif*fincif + ifreq*fincf;  /* index in IF/freq table */

	/* is this one wanted? */
	wt = vvis + 2; /* data weight */
	if (doFlag) *wt = 0.0;
	if (*wt <= 0.0) continue;

	/* Input weight */
	inWt = *wt;

	/* Replacing weights with one? */
	if (doOne) vis[3] = 1.0;

	/* Weights to a power? */
	if (doPower && (vis[3]>0.0)) vis[3] = pow (vis[3], in->WtPower);
	
	/* apply any taper to the weight. */
	if (doTaper) {
	  /* Scale u,v (wavelengths) for frequency (w not used) */
	  uf = *u * desc->fscale[ifq];
	  vf = *v * desc->fscale[ifq];

	  tape = desc->fscale[ifq]* desc->fscale[ifq] * 
	    ((uf)*(uf)*in->sigma2u + (vf)*(vf)*in->sigma2v);
	  if (tape<-14.0) tfact = 0.0; /* underflow */
	  else tfact = exp(tape);
	  *wt *= tfact;
	}

	/* Doing uniform weighting? */
	if (doUnifWt) {
	  /* Scale u,v (cells) for frequency (w not used) */
	  uucell = ucell * desc->fscale[ifq];
	  vvcell = vcell * desc->fscale[ifq];

	  /* get center cell */
	  if (vvcell > 0.0) iv = (glong)(vvcell + 0.5);
	  else iv = (glong)(vvcell - 0.5);
	  iu = (glong)(uucell + 0.5);
	
	  /* location in grid */
	  pos[0] = iu; 
	  pos[1] = iv + lGridCol/2;
	  grid = ObitFArrayIndex (in->wtGrid, pos); /* pointer in weight grid */

	  /* zero weight if not in grid */
	  if (grid==NULL) {
	    *wt = 0.0;
	   } else {
	      /* Make weighting correction */
	      if (*grid >minWt) *wt *= in->wtScale / (*grid + in->temperance);
	      else *wt *= in->wtScale / (minWt + in->temperance);
	   }
	} /* end of uniform weighting correction */

	/* Output weight */
	outWt = *wt;

	/* Weighting sums for statistics */
	sumInWt  += inWt;
	sumOutWt += outWt;
	sumO2IWt += outWt * outWt / inWt;

	vvis += desc->incf; /* visibility pointer */
      } /* end loop over frequencies */
      ifvis += desc->incif; /* visibility pointer */
    } /* Loop over IFs */

    /* update data pointers */
    u += desc->lrec;
    v += desc->lrec;
    w += desc->lrec;
    vis += desc->lrec;
  } /* end loop over visibilities */

  /* save weighting sums */
  in->wtSums[0] = sumInWt;
  in->wtSums[1] = sumOutWt;
  in->wtSums[2] = sumO2IWt;
  in->numberBad = numberBad;

} /* end WeightBuffer */

/**
 * Calculates convolving function and attaches it to in.
 * Makes 2D convolving function tabulated every 1/5 of a cell.
 * \param in      Object with table to init.
 */
static void ConvFunc (ObitUVWeight* in)
{
  gfloat xinc, fwt, fu, fv=0.0, rad, radmax, xi=0.0;
  gfloat u, v, absu, absv=0.0, umax, vmax, *convfnp;
  glong i, j, k, size, lim, bias, naxis[2];
  gboolean round;
  /*gfloat shit[701]; DEBUG */

  /* error checks */
  g_assert (ObitUVWeightIsA(in));

  /* Width of convolving kernel in cells */
  in->convWidth    = 2*in->WtBox+1;  /* number of cells - should be odd */
  in->convNperCell = 5;   /* Number of tabulated points per cell in convfn */
  round = (in->WtFunc > 0) && (in->WtBox>0); /* Round or square function? */

  /* allocate array */
  lim = in->convWidth * in->convNperCell + 1;
  size = lim;
  naxis[0] = size;
  naxis[1] = size;
  in->convfn = ObitFArrayUnref(in->convfn);
  in->convfn = ObitFArrayCreate (in->name, 2L, naxis);

  /* get pointer to memory array */
  naxis[0] = 0; naxis[1] = 0;
  convfnp = ObitFArrayIndex (in->convfn, naxis);

  xinc = 1.0 / (gfloat)in->convNperCell;
  if (in->WtBox==0) {  /* if using a single cell, use everything */
    in->WtFunc = 1;    /* only use pill box for single cell */
    umax = 1.0;
    vmax = 1.0;
  } else { /* multi cell weighting function */
    umax = in->WtBox + 0.5;
    vmax = in->WtBox + 0.5;
  }
  radmax = umax * vmax;
  bias = (in->convNperCell/2) * in->convWidth;

  /*+++++++++++++++++ Pillbox ++++++++++++++++++++++++++++++++++++++++*/
  if (abs(in->WtFunc)==1) {
    /* fill function */
    k = 0;
    for (j=0; j<lim; j++) {
      /* distance in v */
      v = (j-bias) * xinc;
      absv = fabs (v);
      fv =  1.0;
      if (absv == vmax) fv = 0.5;
      else if (absv > vmax) fv = 0.0;
      for (i=0; i<lim; i++) {
	/* distance in u */
	u = (i-bias) * xinc;
	if (round) {
	  /* Use circularly symmetric version */
	  rad = u*u + v*v;
	  fwt = 1.0;
	  if (rad == radmax) fwt = 0.5;
	  else if (rad > radmax)  fwt = 0.0;
	} else {
	  /* use square version */
	  absu = fabs (u);
	  fu = 1.0;
	  if (absu == umax) fu = 0.5;
	  else if (absu > umax)  fu = 0.0;
	  fwt = fu * fv;
	}
	convfnp[k++] = fwt;  /* save it in u major order */
      }
    } 
  } /* end pillbox */
  
  else if (abs(in->WtFunc)==2) {
    /*+++++++++++++++++ Linear ++++++++++++++++++++++++++++++++++++++++*/
    /* fill function */
    xi = 1.0 / ((gfloat)in->WtBox + 1.0);
    k = 0;
    for (j=0; j<lim; j++) {
      /* distance in v */
      v = (j-bias) * xinc;
      absv = fabs (v);
      fv =  1.0 - absv*xi;
      if (absv > vmax) fv = 0.0;
      for (i=0; i<lim; i++) {
	/* distance in u */
	u = (i-bias) * xinc;
	if (round) {
	  /* Use circularly symmetric version */
	  rad = sqrt(u*u + v*v);
	  fwt = 1.0 - rad*xi;
	  if (rad > radmax)  fwt = 0.0;
	} else {
	  /* use square version */
	  absu = fabs (u);
	  fu = 1.0- absu*xi;
	  if (absu > umax)  fu = 0.0;
	  fwt = fu * fv;
	}
	convfnp[k++] = fwt;  /* save it in u major order */
      }
    }  
  } /* end linear */
  
  else if (abs(in->WtFunc)==3) {
    /*+++++++++++++++++ Exponential ++++++++++++++++++++++++++++++++++++++++*/
    /* fill function */
    k = 0;
    for (j=0; j<lim; j++) {
      /* distance in v */
      v = (j-bias) * xinc;
      if (!round) {
	absv = fabs (v);
	fv = exp(-2.0*absv*xi);
	if (absv > vmax) fv = 0.0;
      }
      for (i=0; i<lim; i++) {
	/* distance in u */
	u = (i-bias) * xinc;
	if (round) {
	  /* Use circularly symmetric version */
	  rad = sqrt(u*u + v*v);
	  fwt = exp(-2.0*rad*xi);
	  if (rad > radmax)  fwt = 0.0;
	} else {
	  /* use square version */
	  absu = fabs (u);
	  fu = exp(-2.0*absu*xi);
	  if (absu > umax) fu = 0.0;
	  fwt = fu * fv;
	}
	convfnp[k++] = fwt;  /* save it in u major order */
      }
    }
  } /* end exponential */
  
  else if (abs(in->WtFunc)==4) {
    /*+++++++++++++++++ Gaussian ++++++++++++++++++++++++++++++++++++++++*/
    /* fill function */
    k = 0;
    for (j=0; j<lim; j++) {
      /* distance in v */
      v = (j-bias) * xinc;
      if (!round) {
	absv = fabs (v);
	fv = exp(-4.0*absv*xi*absv*xi);
	if (absv > vmax) fv = 0.0;
      }
      else if (absv >  vmax) fv = 0.0;
      for (i=0; i<lim; i++) {
	/* distance in u */
	u = (i-bias) * xinc;
	if (round) {
	  /* Use circularly symmetric version */
	  rad = sqrt(u*u + v*v);
	  fwt = exp(-4.0*rad*xi*rad*xi);
	  if (rad == radmax) fwt = 0.5;
	  else if (rad > radmax)  fwt = 0.0;
	} else {
	  /* use square version */
	  absu = fabs (u);
	  fu = exp(-4.0*absu*xi*absu*xi);
	  if (absu > umax)  fu = 0.0;
	  fwt = fu * fv;
	}
	convfnp[k++] = fwt;  /* save it in u major order */
      }
    }
  } /* end computing convolving fns */
  else { /* should never get here */
    g_error("Unknown convolving function type %ld",in->WtFunc);
  }
} /* end ConvFunc */



