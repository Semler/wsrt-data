/* $Id: ObitAntennaList.c,v 1.3 2004/12/28 14:40:49 bcotton Exp $ */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003,2004                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "ObitAntennaList.h"

/*-------------- Obit: Software for the recently deceased ------------*/
/**
 * \file ObitAntennaList.c
 * ObitAntennaList class function definitions.
 *
 * This is a list of associated tables.
 */

/*------------------- File Global Variables - ----------------*/
/** name of the class defined in this file */
static gchar *myClassName = "ObitAntennaList";

/**
 * ClassInfo global structure ObitAntennaListClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitAntennaListClassInfo myClassInfo = {FALSE};

/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitAntennaListInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitAntennaListClear (gpointer in);


/*----------------------Public functions---------------------------*/
/**
 * Basic Constructor.
 * Initializes class if needed on first call.
 * \return the new object.
 */
ObitAntennaList* newObitAntennaList (gchar* name)
{
  ObitAntennaList* out;

  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitAntennaListClassInit();

  /* allocate/init structure */
  out = g_malloc0(sizeof(ObitAntennaList));

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

 /* set classInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  ObitAntennaListInit((gpointer)out);

  return out;
} /* end newObitAntennaList */

/**
 * Returns ClassInfo pointer for the class.
 * Initializes class if needed on first call.
 * \return pointer to the class structure.
 */
gconstpointer ObitAntennaListGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitAntennaListClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitGetIOClass */

/**
 * Creates an ObitAntennaList of a given size.
 * \param name       An optional name for the object.
 * \param nant       Number of antennas (actually, the highest Antenna ID).
 * \param numpolcal  Number of Polarization calibration parameters
 * \return the new object.
 */
ObitAntennaList* ObitAntennaListCreate (gchar* name, glong nant, glong numpolcal)
{
  ObitAntennaList* out;
  glong i;
  gchar sname[31];

  /* Create basic structure */
  out = newObitAntennaList (name);

  /* create data array if wanted */
  if (nant<0) return out;

  /* Save information */
  out->number = nant;

  /* create array */
  out->ANlist = g_malloc0 (nant*sizeof(ObitAntenna*));

  /* Create elements of ObitAntenna list */
  for (i=0; i<out->number; i++) {
    g_snprintf (sname, 30, "Antenna %ld", i+1);
    out->ANlist[i] = newObitAntenna (sname);
    /* Polarization calibration arrays */
    out->ANlist[i]->FeedAPCal = g_malloc0(numpolcal*sizeof(gfloat));
    out->ANlist[i]->FeedBPCal = g_malloc0(numpolcal*sizeof(gfloat));
  }

  return out;
} /* end ObitAntennaListCreate */

/**
 * Make a copy of a object.
 * Parent class members are included but any derived class info is ignored.
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \param err Obit error stack object.
 * \return pointer to the new object.
 */
ObitAntennaList* ObitAntennaListCopy  (ObitAntennaList *in, ObitAntennaList *out, 
				       ObitErr *err)
{
  const ObitClassInfo *ParentClass;
  gboolean oldExist;
  gchar *outName;
  glong i;
  gchar *routine = "ObitAntennaListCopy";
  
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return out;
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));
  
  /* Create if it doesn't exist */
  oldExist = out!=NULL;
  if (!oldExist) {
    /* derive object name */
    outName = g_strconcat ("Copy: ",in->name,NULL);
    out = newObitAntennaList(outName);
    if (outName) g_free(outName); outName = NULL;
  }
  
  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);
  
  /*  copy this class */
  /* number of elements */
  out->number = in->number;
  
  /* Copy general information */
  out->polType   = in->polType;
  out->GSTIAT0   = in->GSTIAT0;
  out->RotRate   = in->RotRate;
  out->JD        = in->JD;
  out->ut1Utc    = in->ut1Utc;
  out->dataUtc   = in->dataUtc;
  out->numPoln   = in->numPoln;
  out->numPCal   = in->numPCal;
  out->polRefAnt = in->polRefAnt;
  out->numIF     = in->numIF;
  out->FreqID    = in->FreqID;
  for (i=0; i<3; i++) out->ArrayXYZ[i] = in->ArrayXYZ[i];
  for (i=0; i<2; i++) out->PolarXY[i]  = in->PolarXY[i];
  for (i=0; i<12; i++) out->TimSys[i]  = in->TimSys[i];
  for (i=0; i<12; i++) out->ArrName[i] = in->ArrName[i];
  /* Polarization R-L Phases */
  out->RLPhaseDiff = g_realloc (out->RLPhaseDiff, out->numIF*sizeof(gfloat));
  for (i=0; i<out->numIF; i++) out->RLPhaseDiff[i] = in->RLPhaseDiff[i];
  
  /* Reallocate list if needed */
  out->ANlist = g_realloc (out->ANlist, out->number*sizeof(ObitAntenna*));
  
  /* loop through list copying elements */
  for (i=0; i<out->number; i++) 
    out->ANlist[i] = ObitAntennaCopy (in->ANlist[i], out->ANlist[i], err);
  
  if (err->error) Obit_traceback_val (err, routine, in->name, out);
  
  return out;
} /* end ObitAntennaListCopy */

/**
 * Return corresponding type code, recognizes:
 * \li 'ORI-ELP ' Orientation-ellipticity (OBIT_UVPoln_ELORI)
 * \li 'APPROX  ' R/L Linear D term approximation (OBIT_UVPoln_Approx)
 * \li 'VLBI    ' R/L Linear D term approximation for resolved sources (OBIT_UVPoln_VLBI)
 * \li 'X-Y LIN ' X/Y Linear D term approximation (OBIT_UVPoln_XYLin)
 * \param type Polarization calibration type name
 * \return code, OBIT_UVPoln_Unknown -> not recognized, 
 *               OBIT_UVPoln_NoCal   -> no code (no poln cal)
 */
ObitUVPolCalType ObitAntennaListGetPolType (gchar* type)
{
  ObitUVPolCalType out = OBIT_UVPoln_Unknown;

  if (!strncmp (type, "    ",    4)) out = OBIT_UVPoln_NoCal;
  if (!strncmp (type, "ORI-ELP", 7)) out = OBIT_UVPoln_ELORI;
  if (!strncmp (type, "APPROX",  6)) out = OBIT_UVPoln_Approx;
  if (!strncmp (type, "VLBI",    4)) out = OBIT_UVPoln_VLBI;
  if (!strncmp (type, "X-Y LIN", 7)) out = OBIT_UVPoln_XYLin;
  return out;
} /* end  */

/**
 * Initialize global ClassInfo Structure.
 */
void ObitAntennaListClassInit (void)
{
  const ObitClassInfo *ParentClass;

  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;

  /* Initialize (recursively) parent class first */
  ParentClass = ObitGetClass();
  ObitClassInit();  /* Initialize parent class if needed */

  /* function pointers etc. for this class */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = (ObitClassInitFP)ObitAntennaListClassInit;
  myClassInfo.newObit       = (newObitFP)newObitAntennaList;
  myClassInfo.ObitCopy      = (ObitCopyFP)ObitAntennaListCopy;
  myClassInfo.ObitClone     = NULL;
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitAntennaListClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitAntennaListInit;
} /* end ObitAntennaListClassInit */
/*---------------Private functions--------------------------*/

/**
 * Creates empty member objects, initialize reference count.
 * Does (recursive) initialization of base class members before 
 * this class.
 * \param inn Pointer to the object to initialize.
 */
void ObitAntennaListInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitAntennaList *in = inn;

  /* error checks */
  g_assert (in != NULL);
  
  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->ANlist      = NULL;
  in->RLPhaseDiff = NULL;
  in->number = 0;
} /* end ObitAntennaListInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * \param  inn Pointer to the object to deallocate.
 */
void ObitAntennaListClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitAntennaList *in = inn;
  glong i;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* free this class members */
  /* loop through list copying elements */
  for (i=0; i<in->number; i++) 
    in->ANlist[i] = ObitAntennaUnref (in->ANlist[i]);

  /* delete members */
  if (in->ANlist) g_free(in->ANlist); in->ANlist = NULL;
  if (in->RLPhaseDiff) g_free(in->RLPhaseDiff); in->RLPhaseDiff = NULL;
 
 /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);

} /* end ObitAntennaListClear */


  
