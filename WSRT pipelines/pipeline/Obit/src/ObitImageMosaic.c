/* $Id: ObitImageMosaic.c,v 1.31 2005/08/20 19:39:46 bcotton Exp $  */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2004-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "ObitImageMosaic.h"
#include "ObitIOImageFITS.h"
#include "ObitIOImageAIPS.h"
#include "ObitSystem.h"
#include "ObitImageUtil.h"
#include "ObitUVUtil.h"
#include "ObitAIPSDir.h"
#include "ObitUV.h"
#include "ObitSkyGeom.h"
#include "ObitTableVZ.h"
#include "ObitTableSUUtil.h"
#include "ObitPBUtil.h"
#include "ObitFFT.h"
#include "ObitMem.h"
/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitImageMosaic.c
 * ObitImageMosaic class function definitions.
 * This class is derived from the Obit base class.
 *
 * This class contains an array of associated astronomical images.
 *
 */

/** name of the class defined in this file */
static gchar *myClassName = "ObitImageMosaic";

/*--------------- File Global Variables  ----------------*/
/**
 * ClassInfo structure ObitImageMosaicClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitImageMosaicClassInfo myClassInfo = {FALSE};

/*--------------------------- Macroes ---------------------*/
#ifndef MAXFLD         /* Maxum number of fields */
#define MAXFLD 4096
#endif

/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitImageMosaicInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitImageMosaicClear (gpointer in);

/** Private: Cover specified field of view */
static gfloat
FlyEye (gfloat radius, gint imsize, gfloat cells[2], gint ovrlap,
	gfloat shift[2], double ra0, double dec0, 
	gint *nfield, gint *fldsiz, gfloat *rash, gfloat *decsh, gint *flqual,
	ObitErr *err);

/** Private: Add field to list */
static int 
AddField (gfloat shift[2], gfloat dec, gint imsize, gfloat cells[2], 
	  gint qual, gboolean check, 
	  gint *nfield, gint *fldsiz, gfloat *rash, gfloat *decsh, gint *flqual,
	  ObitErr *err);

/** Private: Bubble sort */
 static void 
 Bubble (gfloat *data, gint* indx, gint number, gint direct);

/** Private: Lookup outliers in catalog */
static void 
AddOutlier (gchar *Catalog, gint catDisk, gfloat minRad, gfloat cells[2], 
	    gfloat OutlierDist, gfloat OutlierFlux, gfloat OutlierSI, gint OutlierSize,
	    gdouble ra0, gdouble dec0, gboolean doJ2B, gdouble Freq, 
	    gint *nfield, gint *fldsiz, gfloat *rash, gfloat *decsh, gint *flqual, 
	    ObitErr *err);

/*----------------------Public functions---------------------------*/
/**
 * Constructor.
 * Initializes class if needed on first call.
 * \param name An optional name for the object.
 * \return the new object.
 */
ObitImageMosaic* newObitImageMosaic (gchar* name, gint number)
{
  ObitImageMosaic* out;

  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitImageMosaicClassInit();

  /* allocate/init structure */
  out = ObitMemAlloc0Name(sizeof(ObitImageMosaic), "ObitImageMosaic");

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

  /* set ClassInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  out->numberImages = number;
  ObitImageMosaicInit((gpointer)out);

 return out;
} /* end newObitImageMosaic */

/**
 * Returns ClassInfo pointer for the class.
 * \return pointer to the class structure.
 */
gconstpointer ObitImageMosaicGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitImageMosaicClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitGetImageClass */

/**
 * Zap (delete with underlying structures) selected image member(s).
 * Also deletes any associated beam member
 * \param in      The array of images
 * \param number  The 0-rel image number, -1=> all
 * \param err     Error stack, returns if not empty.
 */
void 
ObitImageMosaicZapImage  (ObitImageMosaic *in, gint number,
			  ObitErr *err)
{
  gint i;
  ObitImage *myBeam=NULL;
  gchar *routine="ObitImageMosaicZapImage";

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

  /* Kill 'em all? */
  if (number==-1) {
    for (i=0; i<in->numberImages; i++) {
      /* First the beam if any */
      if (in->images[i]->myBeam!=NULL) {
	myBeam = (ObitImage*)in->images[i]->myBeam;
	in->images[i]->myBeam = (Obit*)ObitImageZap(myBeam, err);
      }

      /* then the image */
      in->images[i] = ObitImageZap(in->images[i], err);
      if (err->error) Obit_traceback_msg (err, routine, in->name);
    }
    return;
  }

  /* Check that number in legal range */
  if ((number<0) || (number>in->numberImages)) {
    Obit_log_error(err, OBIT_Error, 
		   "%s: Image number %d out of range [%d %d] in %s", 
		   routine, number, 0, in->numberImages, in->name);
    return;
  }

  /* First the beam if any */
  if (in->images[number]->myBeam!=NULL) {
    myBeam = (ObitImage*)in->images[number]->myBeam;
    in->images[number]->myBeam = (Obit*)ObitImageZap(myBeam, err);
  }
  /* then the image */
  in->images[number] = ObitImageZap(in->images[number], err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);
  
  return;
} /* end ObitImageMosaicZapImage */

/**
 * Return pointer to selected image member.
 * Returned reference has been Refed.
 * \param in      The array of images
 * \param number  The 0-rel image number
 * \param err     Error stack, returns if not empty.
 * \return pointer to the selected image. NULL on failure.
 */
ObitImage* 
ObitImageMosaicGetImage  (ObitImageMosaic *in, gint number,
			  ObitErr *err)
{
  ObitImage *out=NULL;
  gchar *routine="ObitImageMosaicGetImage";

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return NULL;
  g_assert (ObitIsA(in, &myClassInfo));

  /* Check that number in legal range */
  if ((number<0) || (number>in->numberImages)) {
    Obit_log_error(err, OBIT_Error, 
		   "%s: Image number %d out of range [%d %d] in %s", 
		   routine, number, 0, in->numberImages, in->name);
    return out;
  }

  out = ObitImageRef(in->images[number]);

  return out;
} /* end ObitImageMosaicGetImage */

/**
 * Set a selected image member.
 * \param in      The array of images
 * \param number  The 0-rel image number in array
 * \param image   Image reference to add
 * \param err     Error stack, returns if not empty.
 */
void 
ObitImageMosaicSetImage  (ObitImageMosaic *in, gint number, 
			  ObitImage* image, ObitErr *err)
{
  gchar *routine="ObitImageMosaicSetImage";

  /* error checks */
  g_assert(ObitErrIsA(err));
  g_assert(ObitImageIsA(image));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

  /* Check that number in legal range */
  if ((number<0) || (number>in->numberImages)) {
    Obit_log_error(err, OBIT_Error, 
		   "%s: Image number %d out of range [%d %d] in %s", 
		   routine, number, 0, in->numberImages, in->name);
    return;
  }

  /* Unref any old image*/
  if (in->images[number]) ObitImageUnref(in->images[number]);

  in->images[number] = ObitImageRef(image);
} /* end ObitImageMosaicSetImage */


/**
 * Determine the RMS pixel value of image.
 * Ignores outer 5 pixel in image.
 * \param in      The array of images
 * \param number  The 0-rel image number in array
 * \param image   Image reference to add
 * \param err     Error stack, returns if not empty.
 */
gfloat 
ObitImageMosaicGetImageRMS  (ObitImageMosaic *in, gint number,
			     ObitErr *err)
{
  gfloat RMS = -1.0;
  ObitIOSize IOBy;
  gint32 dim[MAXINFOELEMDIM] = {1,1,1,1,1};
  gint blc[IM_MAXDIM] = {1,1,1,1,1};
  gint trc[IM_MAXDIM] = {0,0,0,0,0};
  gchar *routine="ObitImageMosaicGetImageRMS";

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return RMS;
  g_assert (ObitIsA(in, &myClassInfo));

  /* Check that number in legal range */
  if ((number<0) || (number>in->numberImages)) {
    Obit_log_error(err, OBIT_Error, 
		   "%s: Image number %d out of range [%d %d] in %s", 
		   routine, number, 0, in->numberImages, in->name);
    return RMS;
  }

  /* Read plane */
  IOBy = OBIT_IO_byPlane;
  dim[0] = 1;
  ObitInfoListPut (in->images[number]->info, "IOBy", OBIT_int, dim, 
		   (gpointer)&IOBy, err);

  /* Set blc, trc to ignore outer 5 pixels */
  blc[0] = 5;
  blc[1] = 5;
  trc[0] = in->images[number]->myDesc->inaxes[0] - 5;
  trc[1] = in->images[number]->myDesc->inaxes[1] - 5;
  dim[0] = 7;
  ObitInfoListPut (in->images[number]->info, "BLC", OBIT_int, dim, blc, err); 
  ObitInfoListPut (in->images[number]->info, "TRC", OBIT_int, dim, trc, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, RMS);
      
 /* Read image */
  ObitImageOpen (in->images[number], OBIT_IO_ReadOnly, err);
  ObitImageRead (in->images[number], NULL, err); /* Read plane */
  ObitImageClose(in->images[number], err);
  if (err->error) Obit_traceback_val (err, routine, in->name, RMS);

  /* Get RMS */
  RMS = ObitFArrayRMS (in->images[number]->image);

  /* Free image buffer */
  in->images[number]->image = ObitFArrayUnref(in->images[number]->image);

  /* Reset blc, trc */
  blc[0] = 1;
  blc[1] = 1;
  trc[0] = 0;
  trc[1] = 0;
  dim[0] = 7;
  ObitInfoListPut (in->images[number]->info, "BLC", OBIT_int, dim, blc, err); 
  ObitInfoListPut (in->images[number]->info, "TRC", OBIT_int, dim, trc, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, RMS);
      
  return RMS;
} /* end ObitImageMosaicGetImageRMS */

/**
 * Return pointer to full field image member.
 * Returned reference has been Refed.
 * \param in      The array of images
 * \param err     Error stack, returns if not empty.
 * \return pointer to the selected image. NULL on failure.
 */
ObitImage* 
ObitImageMosaicGetFullImage  (ObitImageMosaic *in, ObitErr *err)
{
  ObitImage *out=NULL;

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return NULL;
  g_assert (ObitIsA(in, &myClassInfo));

  out = ObitImageRef(in->FullField);

  return out;
} /* end ObitImageMosaicGetFullImage */

/**
 * Set the Full field image member.
 * \param in      The array of images
 * \param image   Image reference to add
 * \param err     Error stack, returns if not empty.
 */
void 
ObitImageMosaicSetFullImage  (ObitImageMosaic *in, 
			      ObitImage* image, ObitErr *err)
{

  /* error checks */
  g_assert(ObitErrIsA(err));
  g_assert(ObitImageIsA(image));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

  /* Unref any old image*/
  ObitImageUnref(in->FullField);

  in->FullField = ObitImageRef(image);
} /* end ObitImageMosaicSetFullImage */


/**
 * Make a shallow copy of input object.
 * Parent class members are included but any derived class info is ignored.
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \param err Error stack, returns if not empty.
 * \return pointer to the new object.
 */
ObitImageMosaic* ObitImageMosaicCopy (ObitImageMosaic *in, ObitImageMosaic *out, 
				      ObitErr *err)
{
  const ObitClassInfo *ParentClass;
  gboolean oldExist;
  gint i;
  gchar *outName;
  gchar *routine = "ObitImageMosaicCopy";

  /* error checks */
  if (err->error) return NULL;
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));

  /* Create if it doesn't exist */
  oldExist = out!=NULL;
  if (!oldExist) {
    /* derive object name */
    outName = g_strconcat ("Copy: ",in->name,NULL);
    out = newObitImageMosaic(outName, in->numberImages);
    g_free(outName);
  }

  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);

   /* if Old exists, check that number in legal range */
  if (oldExist) {
    if ((out->numberImages<in->numberImages)) {
      Obit_log_error(err, OBIT_Error, 
		     "%s: Too few images %d in extant output %s", 
		     routine, out->numberImages, out->name);
      return out;
    }

    /* unreference any old members */
    for (i=0; i<out->numberImages; i++)
      out->images[i] = ObitImageUnref(out->images[i]);
  }

  /* Copy other class members */
  out->xCells   = in->xCells;
  out->yCells   = in->yCells;
  out->fileType = in->fileType;
  out->imSeq    = in->imSeq;
  out->imDisk   = in->imDisk;
  if (out->imName) g_free(out->imName);
  out->imName = g_strdup(in->imName);
  if (out->imClass) g_free(out->imClass);
  out->imClass = g_strdup(in->imClass);
  out->bmaj    = in->bmaj;
  out->bmin    = in->bmin;
  out->bpa     = in->bpa;
  for (i=0; i<in->numberImages; i++) {
    ObitImageMosaicSetImage(out, i, in->images[i], err);
    out->nx[i]       = in->nx[i];
    out->ny[i]       = in->ny[i];
    out->nplane[i]   = in->nplane[i];
    out->RAShift[i]  = in->RAShift[i];
    out->DecShift[i] = in->DecShift[i];
  }
  if (err->error) Obit_traceback_val (err, routine, in->name, out);

  return out;
} /* end ObitImageMosaicCopy */

/**
 * Attach images to their underlying files.
 * For AIPS files:
 * Mosaic images have classes with the first character of the imClass
 * followed by 'M', followed by 4 digits of the field number.  
 * The Beams are the same except that the second character is 'B'.
 * The full field image has class imClass and 'F' as the 6th character.
 *
 * For FITS files:
 * Image classes are imClass+digits of the field
 * Beam classes are the same except the second character is replaced 
 * with a 'B' (unless it already is 'B' in which case 'b' is used).
 * The full field image has class imClass followed by 'Full'
 * 
 * \param in     The object with images,  Details are defined in members:
 * \li numberImage - Number of images in Mosaic
 * \li images   - Image array
 * \li fileType - Are image OBIT_IO_AIPS or OBIT_IO_FITS?
 * \li imName   - Name of Mosaic images
 * \li imClass  - imClass
 * \li imSeq    - imSeq
 * \li imDisk   - imDisk
 * \param doBeam  If true, make beam as well.
 * \param err     Error stack, returns if not empty.
 */
void ObitImageMosaicSetFiles  (ObitImageMosaic *in, gboolean doBeam, ObitErr *err) 
{
  gint i, user, cno;
  gint blc[IM_MAXDIM] = {1,1,1,1,1};
  gint trc[IM_MAXDIM] = {0,0,0,0,0};
  gboolean exist;
  ObitImage *image=NULL;
  gchar ct, strTemp[48], Aname[13], Aclass[7], Atclass[2], Atype[3] = "MA";
  gchar *routine = "ObitImageMosaicSetFiles";

 /* Create full field image if needed */
  if (in->doFull && (in->FOV>0.0) && (in->numberImages>=1)) {
    image = in->FullField;

    /* AIPS file */
    if (in->fileType==OBIT_IO_AIPS) {
      /* Get AIPS user number */
      user = ObitSystemGetAIPSuser();
      /* set class */
      sprintf(strTemp, "%s", in->imClass);
      strncpy (Aclass, strTemp,    6);  Aclass[6] = 0;
      Aclass[5] = 'F';
      strncpy (Aname,  in->imName, 12); Aname[12] = 0;
      /* allocate */
      cno = ObitAIPSDirAlloc(in->imDisk, user, Aname, Aclass, Atype, in->imSeq, &exist, err);
      /* Set info */
      ObitImageSetAIPS(image,OBIT_IO_byPlane,in->imDisk,cno,user,blc,trc,err);
      /*DEBUG */
      Obit_log_error(err, OBIT_InfoErr, "Making AIPS image %s %s %d on disk %d cno %d",
		     Aname, Aclass, in->imSeq, in->imDisk, cno);
      /* fprintf (stderr,"Making AIPS image %s %s on disk %d cno %d\n",
	 Aname, Aclass, in->imDisk, cno);*/

    /* FITS file */
    } else if (in->fileType==OBIT_IO_FITS) {
      /* set filename - derive from field */
      sprintf(strTemp, "MA%s.%sFulldseq%d", in->imName, in->imClass, in->imSeq);
      ObitImageSetFITS(image,OBIT_IO_byPlane,in->imDisk,strTemp,blc,trc,err);
    }
    if (err->error) Obit_traceback_msg (err, routine, in->name);

    /* Open and close to fully instantiate */
    ObitImageOpen(image, OBIT_IO_WriteOnly, err);
    ObitImageClose(image, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);

  } /* end do full field image */

  /* Loop over images */
  for (i=0; i<in->numberImages; i++) {

    /* Define files */
    image = in->images[i];
    if (in->fileType==OBIT_IO_AIPS) {
      /* Get AIPS user number */
      user = ObitSystemGetAIPSuser();
      /* set class */
      /* Only one character of Class allowed here*/
      Atclass[0] = in->imClass[0]; Atclass[1] = 'M'; Atclass[2] = 0;
      sprintf(strTemp, "%s%4.4d", Atclass, i+1);
      strncpy (Aclass, strTemp,    6);  Aclass[6] = 0;
      strncpy (Aname,  in->imName, 12); Aname[12] = 0;
      /* allocate */
      cno = ObitAIPSDirAlloc(in->imDisk, user, Aname, Aclass, Atype, in->imSeq, &exist, err);
      /* Set info */
      ObitImageSetAIPS(image,OBIT_IO_byPlane,in->imDisk,cno,user,blc,trc,err);
      /*DEBUG */
      Obit_log_error(err, OBIT_InfoErr, "Making AIPS image %s %s %d on disk %d cno %d",
		     Aname, Aclass, in->imSeq, in->imDisk, cno);
      /* fprintf (stderr,"Making AIPS image %s %s on disk %d cno %d\n",
	 Aname, Aclass, in->imDisk, cno);*/
    } else if (in->fileType==OBIT_IO_FITS) {
      /* set filename - derive from field */
      sprintf(strTemp, "MA%s.%s%4.4dseq%d", in->imName, in->imClass, i, in->imSeq);
      ObitImageSetFITS(image,OBIT_IO_byPlane,in->imDisk,strTemp,blc,trc,err);
    }
    /* Open and close to fully instantiate */
    ObitImageOpen(image, OBIT_IO_WriteOnly, err);
    ObitImageClose(image, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
 
    /* Doing beams ?*/
    if (doBeam) {
      image = (ObitImage*)in->images[i]->myBeam;
      /* Define files - same except second character of "class" is 'B' */
      if (in->fileType==OBIT_IO_AIPS) {
	/* Get AIPS user number */
	user = ObitSystemGetAIPSuser();
	/* set class */
	Aclass[1] = 'B';
	/* allocate */
	cno = ObitAIPSDirAlloc(in->imDisk, user, Aname, Aclass, Atype, in->imSeq, &exist, err);
	/* Set info */
	ObitImageSetAIPS(image,OBIT_IO_byPlane,in->imDisk,cno,user,blc,trc,err);
	
      } else if (in->fileType==OBIT_IO_FITS) {
	/* set filename - derive from field - insure uniqueness */
	ct = in->imClass[1];
	if (in->imClass[1] != 'B') in->imClass[1] = 'B';
	else in->imClass[1] = 'b';
	sprintf(strTemp, "MA%s.%s%4.4dseq%d", in->imName, in->imClass, i, in->imSeq);
	in->imClass[1] = ct;
	ObitImageSetFITS(image,OBIT_IO_byPlane,in->imDisk,strTemp,blc,trc,err);
      }
      /* Open and close to fully instantiate */
      ObitImageOpen(image, OBIT_IO_WriteOnly, err);
      ObitImageClose(image, err);
      if (err->error) Obit_traceback_msg (err, routine, in->name);
    } /* end Beams */
  }    /* end loop over images */
 
} /* end  ObitImageMosaicSetFiles */

/**
 * Create an Image Mosaic based on a uv data and parameters attached thereto
 * 
 * \param name    Name to be given to new object
 * \param uvData  The object to create images in,  Details are defined in info members:
 * \li FileType = Underlying file type, OBIT_IO_FITS, OBIT_IO_AIPS
 * \li Name     = Name of image, used as AIPS name or to derive FITS filename
 * \li Class    = Root of class, used as AIPS class or to derive FITS filename
 * \li Seq      = Sequence number
 * \li Disk     = Disk number for underlying files
 * \li FOV      = Field of view (deg) for Mosaic 
 *                If > 0.0 then a mosaic of images will be added to cover this region.
 *                Note: these are in addition to the NField fields added by 
 *                other parameters
 * \li doFull   = if TRUE, create full field image to cover FOV [def. FALSE]
 * \li NField   = Number of fields defined in input,
 *                if unspecified derive from data and FOV
 * \li "xCells" = Cell spacing in X (asec) for all images,
 *                if unspecified derive from data
 * \li "yCells" = Cell spacing in Y (asec) for all images,
 *                if unspecified derive from data
 * \li "BMAJ"   = OBIT_float scalar = Restoring beam major axis (asec)
 *                if = 0 then write fitted value to header
 * \li "BMIN"   = OBIT_float scalar = Restoring beam minor axis (asec)
 * \li "BPA"    = OBIT_float scalar = Restoring beam position angle (deg)
 * \li "Beam"   = OBIT_float [3] = (BMAJ, BMIN, BPA) alternate form
 * \li nx       = Minimum number of cells in X for NField images
 *                if unspecified derive from data
 * \li ny       = Minimum number of cells in Y for NField images
 *                if unspecified derive from data
 * \li RAShift  = Right ascension shift (AIPS convention) for each field
 *                if unspecified derive from FOV and data
 * \li DecShift = Declination for each field
 *                if unspecified derive from FOV and data
 * \li Catalog  =    AIPSVZ format catalog for defining outliers, 
 *                   'None'=don't use [default]
 *                   'Default' = use default catalog.
 *                   Assumed in FITSdata disk 1.
 * \li OutlierDist = Maximum distance (deg) from center to include outlier fields
 *                   from Catalog. [default 1 deg]
 * \li OutlierFlux = Minimum estimated flux density include outlier fields
 *                   from Catalog. [default 0.1 Jy ]
 * \li OutlierSI   = Spectral index to use to convert catalog flux density to observed
 *                   frequency.  [default = -0.75]
 * \li OutlierSize = Width of outlier field in pixels.  [default 50]
 * \param err     Error stack, returns if not empty.
 * \return Newly created object.
 */
ObitImageMosaic* ObitImageMosaicCreate (gchar *name, ObitUV *uvData, ObitErr *err)
{
  ObitImageMosaic *out = NULL;
  ObitInfoType type;
  ObitIOType Type;
  gint32 i, dim[MAXINFOELEMDIM];
  gint Seq, Disk, NField, nx[MAXFLD], ny[MAXFLD], catDisk;
  gint overlap, imsize, fldsiz[MAXFLD], flqual[MAXFLD];
  gfloat FOV=0.0, xCells, yCells, RAShift[MAXFLD], DecShift[MAXFLD], MaxBL, MaxW, Cells;
  gfloat Radius = 0.0;
  gfloat shift[2] = {0.0,0.0}, cells[2], bmaj, bmin, bpa, beam[3];
  gdouble ra0, dec0;
  gboolean doJ2B, doFull;
  gfloat equinox, minRad, ratio, OutlierFlux, OutlierDist, OutlierSI;
  gint  itemp, OutlierSize=0, nFlyEye = 0;
  union ObitInfoListEquiv InfoReal; 
  gchar Catalog[100], Aname[100], Aclass[100];
  gchar *routine = "ObitImageMosaicCreate";

  /* Get inputs with plausible defaults */
  NField = 0;
  ObitInfoListGetTest(uvData->info, "NField",  &type, dim,  &NField);
  ObitInfoListGet(uvData->info, "imFileType",&type, dim,  &Type,     err);
  ObitInfoListGet(uvData->info, "imName",    &type, dim,  Aname,     err);
  ObitInfoListGet(uvData->info, "imClass",   &type, dim,  Aclass,    err);
  ObitInfoListGet(uvData->info, "imSeq",     &type, dim,  &Seq,      err);
  ObitInfoListGet(uvData->info, "imDisk",    &type, dim,  &Disk,     err);
  ObitInfoListGet(uvData->info, "FOV",     &type, dim,  &InfoReal, err);
  if (type==OBIT_float) FOV = InfoReal.flt;
  else if (type==OBIT_double)  FOV = InfoReal.dbl;
  xCells = 0.0;
  ObitInfoListGetTest(uvData->info, "xCells",  &type, dim,  &xCells);
  yCells = 0.0;
  ObitInfoListGetTest(uvData->info, "yCells",  &type, dim,  &yCells);
  doFull = FALSE;
  ObitInfoListGetTest(uvData->info, "doFull", &type, dim,  &doFull);
  sprintf (Catalog, "None");
  ObitInfoListGetTest(uvData->info, "Catalog", &type, dim,  Catalog);
  if (err->error) Obit_traceback_val (err, routine, uvData->name, out);

  /* Get array inputs */
  for (i=0; i<MAXFLD; i++) nx[i] = 0;
  ObitInfoListGetTest(uvData->info, "nx",       &type, dim, nx);
  for (i=0; i<MAXFLD; i++) ny[i] = 0;
  ObitInfoListGetTest(uvData->info, "ny",       &type, dim, ny);
  for (i=0; i<MAXFLD; i++) RAShift[i] = 0.0;
  ObitInfoListGetTest(uvData->info, "RAShift",  &type, dim, RAShift);
  for (i=0; i<MAXFLD; i++) DecShift[i] = 0.0;
  ObitInfoListGetTest(uvData->info, "DecShift", &type, dim, DecShift);
  /* Optional Beam */
  bmaj = bmin = bpa = 0.0;
  ObitInfoListGetTest(uvData->info, "BMAJ", &type, dim, &bmaj);
  ObitInfoListGetTest(uvData->info, "BMIN", &type, dim, &bmin);
  ObitInfoListGetTest(uvData->info, "BPA",  &type, dim, &bpa);
  /* Try alternate form - all in beam */
  beam[0] = bmaj; beam[1] = bmin; beam[2] = bpa;
  ObitInfoListGetTest(uvData->info, "Beam",  &type, dim, beam);
  bmaj = beam[0]; bmin = beam[1]; bpa = beam[2];
  /* Beam given in asec - convert to degrees */
  bmaj /= 3600.0; bmin /=3600.0;

  /* Get extrema */
  ObitUVUtilUVWExtrema (uvData, &MaxBL, &MaxW, err);
  if (err->error) Obit_traceback_val (err, routine, uvData->name, out);
  /* Suggested cellsize and facet size */
  ObitImageUtilImagParm (MaxBL, MaxW, &Cells, &Radius);

  /* Check cell spacing if given */
  if ((fabs(xCells)>0.0) || (fabs(yCells)>0.0)) {
    ratio = fabs(Cells) / fabs(xCells);
    Obit_retval_if_fail(((ratio<10.0) && (ratio>0.1)), err, out,
			"%s: Cellsize seriously bad, suggest %g asec", 
			routine, Cells);
  }

  /* Get cells spacing and maximum undistorted radius from uv data if needed */
  if ((FOV>0.0) || (xCells<=0.0) || (yCells<=0.0)) {
    if (xCells==0.0) xCells = -Cells;
    if (yCells==0.0) yCells =  Cells;
    /* tell about it */
    Obit_log_error(err, OBIT_InfoErr, 
		   "Suggested cell spacing %f, undistorted FOV %f cells for %s",
		   Cells, Radius, uvData->name);
  } else { /* Use given FOV */
    Radius = 0.0;
  }
     
  /* Set fly's eye if needed */
  overlap = 7;
  imsize = (gint)(2.0*Radius + 0.5);
  /* Not bigger than FOV */
  imsize = MIN (imsize, ((2.0*3600.0*FOV/Cells)+10.99));
  cells[0] = -Cells; cells[1] = Cells;
  ObitUVGetRADec (uvData, &ra0, &dec0, err);
  if (err->error) Obit_traceback_val (err, routine, uvData->name, out);
  /* Copy nx to fldsiz - Make image sizes FFT friendly */
  for (i=0; i<NField; i++) fldsiz[i] = ObitFFTSuggestSize (nx[i]);
  minRad = 0.0;
  if (FOV>0.0) minRad = FlyEye(FOV, imsize, cells, overlap, shift, ra0, dec0, 
			       &NField, fldsiz, RAShift, DecShift, flqual, err); 
  if (err->error) Obit_traceback_val (err, routine, uvData->name, out);
  nFlyEye = NField;  /* Number in Fly's eye */

  /* Add outlyers from catalog if requested */
  /* Blank = default */
  if (!strncmp(Catalog, "    ", 4)) sprintf (Catalog, "Default");
  if (strncmp(Catalog, "None", 4)) {
    catDisk = 1;  /* FITS directory for catalog */
    /* Set default catalog */
     if (!strncmp(Catalog, "Default", 7)) sprintf (Catalog, "NVSSVZ.FIT");

     /* Get outlier related inputs */
     OutlierDist = 1.0;
     ObitInfoListGetTest(uvData->info, "OutlierDist",  &type, dim,  &OutlierDist);
     OutlierFlux = 0.1;
     ObitInfoListGetTest(uvData->info, "OutlierFlux",  &type, dim,  &OutlierFlux);
     OutlierSI = -0.75;
     ObitInfoListGetTest(uvData->info, "OutlierSI",    &type, dim,  &OutlierSI);
     InfoReal.itg = 50;type = OBIT_float;
     ObitInfoListGetTest(uvData->info, "OutlierSize",  &type, dim,  &InfoReal);
     if (type==OBIT_float) itemp = InfoReal.flt + 0.5;
     else itemp = InfoReal.itg;
     OutlierSize = itemp;

     /* Add to list from catalog */
     equinox = uvData->myDesc->equinox;  /* Clear up confusion in AIPS */
     if (equinox<1.0) equinox = uvData->myDesc->epoch;
     doJ2B = (equinox!=2000.0) ;  /* need to precess? */

     AddOutlier (Catalog, catDisk, minRad, cells, OutlierDist, OutlierFlux, OutlierSI, OutlierSize,
		 ra0, dec0, doJ2B, uvData->myDesc->crval[uvData->myDesc->jlocf],
		 &NField, fldsiz, RAShift, DecShift, flqual, err);
     if (err->error) Obit_traceback_val (err, routine, uvData->name, out);
  } /* end add outliers from catalog */

  /* Copy fldsiz to nx, ny */
  for (i=0; i<NField; i++) nx[i] = ny[i] = fldsiz[i];
  /*for (i=0; i<NField; i++) nx[i] = ny[i] = 1024;  debug */
      
  /* Create output object */
  out = newObitImageMosaic (name, NField);

  /* Set values on out */
  out->fileType= Type;
  Aname[12] = 0; Aclass[6]=0;
  out->imName  = g_strdup(Aname);
  out->imClass = g_strdup(Aclass);
  out->imSeq   = Seq;
  out->imDisk  = Disk;
  out->xCells  = -fabs(xCells)/3600.0;  /* to Deg*/
  out->yCells  = yCells/3600.0;         /* to deg */
  out->FOV     = FOV;
  out->Radius  = Radius;
  out->doFull  = doFull;
  out->bmaj    = bmaj;
  out->bmin    = bmin;
  out->bpa     = bpa;
  out->nFlyEye = nFlyEye;
  out->OutlierSize  = OutlierSize;
  for (i=0; i<NField; i++) {
    out->nx[i]       = nx[i];
    out->ny[i]       = ny[i];
    out->RAShift[i]  = RAShift[i]/3600.0;   /* to Deg*/
    out->DecShift[i] = DecShift[i]/3600.0;  /* to Deg*/
  }

  return out;
} /* end ObitImageMosaicCreate */

/**
 * Define images in an Image Mosaic from a UV data.
 * 
 * \param in     The object to create images in,  Details are defined in members:
 * \li numberImage - Number of images in Mosaic
 * \li images   - Image array
 * \li xCells   - Cell Spacing in X (deg)
 * \li yCells   - Cell Spacing in Y (deg)
 * \li nx       - Number of pixels in X for each image
 * \li ny       - Number of pixels in Y for each image
 * \li nplane   - Number of planes for each image
 * \li RAShift  - RA shift (asec) for each image
 * \li DecShift - Dec shift (asec) for each image
 * \li fileType - Are image OBIT_IO_AIPS or OBIT_IO_FITS?
 * \li imName   - Name of Mosaic images
 * \li imClass  - imClass
 * \li imSeq    - imSeq
 * \li imDisk   - imDisk
 * \param uvData UV data from which the images are to be made.
 * Imaging information on uvData:
 * \li "nChAvg" OBIT_int (1,1,1) number of channels to average.
 *              This is for spectral line observations and is ignored
 *              if the IF axis on the uv data has more than one IF.
 *              Default is continuum = average all freq/IFs. 0=> all.
 * \li "rotate" OBIT_float (?,1,1) Desired rotation on sky (from N thru E) in deg. [0]
 * \param doBeam  If true, make beam as well.
 * \param err     Error stack, returns if not empty.
 */
void ObitImageMosaicDefine (ObitImageMosaic *in, ObitUV *uvData, gboolean doBeam,
			    ObitErr *err)
{
  gint i, nx, ny;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM] = {1,1,1,1,1};
  gfloat *farr=NULL;
  gboolean doCalSelect;
  ObitIOAccess access;
  gchar *routine = "ObitImageMosaicDefine";

  /* error checks */
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitUVIsA(uvData));

  /* Set values on uv data */
  Obit_return_if_fail((in->numberImages>=1), err,
		      "%s: No images requested", routine);
  dim[0] = in->numberImages;
  ObitInfoListAlwaysPut (uvData->info, "nx",     OBIT_int,   dim, in->nx);
  ObitInfoListAlwaysPut (uvData->info, "nxBeam", OBIT_int,   dim, in->nx);
  ObitInfoListAlwaysPut (uvData->info, "ny",     OBIT_int,   dim, in->ny);
  ObitInfoListAlwaysPut (uvData->info, "nyBeam", OBIT_int,   dim, in->ny);
  ObitInfoListAlwaysPut (uvData->info, "xShift", OBIT_float, dim, in->RAShift);
  ObitInfoListAlwaysPut (uvData->info, "yShift", OBIT_float, dim, in->DecShift);
  farr = ObitMemAllocName(in->numberImages*sizeof(gfloat), routine);
  for (i=0; i<in->numberImages; i++) farr[i] = 3600.0*in->xCells;
  ObitInfoListAlwaysPut (uvData->info, "xCells", OBIT_float, dim, farr);
  for (i=0; i<in->numberImages; i++) farr[i] = 3600.0*in->yCells;
  ObitInfoListAlwaysPut (uvData->info, "yCells", OBIT_float, dim, farr);
  ObitMemFree(farr);

  /* Make sure UV data descriptor has proper info */
  doCalSelect = FALSE;
  ObitInfoListGetTest(uvData->info, "doCalSelect", &type, (gint32*)dim, &doCalSelect);
  if (doCalSelect) access = OBIT_IO_ReadCal;
  else access = OBIT_IO_ReadOnly;

  /* Open/Close to update descriptor */
  ObitUVOpen (uvData, access, err);
  ObitUVClose (uvData, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Loop over images */
  for (i=0; i<in->numberImages; i++) {
    in->images[i] = ObitImageUtilCreateImage(uvData, i+1, doBeam, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
  }    /* end loop over images */

 /* Create full field image if needed */
  if (in->doFull) { 
    /* Basic structure of field 1 */
    in->FullField = ObitImageUtilCreateImage(uvData, 1, FALSE, err);
    /* Replace name */
    g_free(in->FullField->name);
    in->FullField->name = g_strdup("Flattened Image");
    /* set size */
    nx = 2 * in->FOV / fabs (in->xCells);
    ny = 2 * in->FOV / fabs (in->yCells);
    in->FullField->myDesc->inaxes[0] = nx;
    in->FullField->myDesc->inaxes[1] = ny;
    /* set center pixel must be an integer pixel */
    in->FullField->myDesc->crpix[0] = 1 + nx/2;
    in->FullField->myDesc->crpix[1] = 1 + ny/2;
    /* set shift to zero */
    in->FullField->myDesc->xshift = 0.0;
    in->FullField->myDesc->yshift = 0.0;
  }

  ObitImageMosaicSetFiles (in, doBeam, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);
 
} /* end ObitImageMosaicDefine */

/**
 * Project the tiles of a Mosaic to the full field flattened image
 * Routine translated from the AIPSish 4MASS/SUB/FLATEN.FOR/FLATEN  
 * \param in     The object with images
 * \param err     Error stack, returns if not empty.
 */
void ObitImageMosaicFlatten (ObitImageMosaic *in, ObitErr *err)
{
  gint   blc[IM_MAXDIM]={1,1,1,1,1}, trc[IM_MAXDIM]={0,0,0,0,0};
  gint i, radius, rad, plane[IM_MAXDIM] = {1,1,1,1,1}, hwidth = 2;
  glong *naxis, pos1[IM_MAXDIM], pos2[IM_MAXDIM];
  gint32 dim[MAXINFOELEMDIM] = {1,1,1,1,1};
  ObitImage *out=NULL, *tout1=NULL, *tout2=NULL;
  ObitFArray *sc2=NULL, *sc1=NULL;
  ObitIOSize IOsize = OBIT_IO_byPlane;
  gfloat xpos1[IM_MAXDIM], xpos2[IM_MAXDIM];
  gboolean overlap;
  gchar *routine = "ObitImageMosaicFlatten";

  /* error checks */
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

  /* Must have output image defined */
  if (in->FullField==NULL) {
    Obit_log_error(err, OBIT_Error, "No flattened image defined");
    return;
  }

  /* Tell user */
  Obit_log_error(err, OBIT_InfoErr, "Flattening flys eye to single image");

  /* Copy beam parameters from first image to flattened */
  ObitImageOpen(in->images[0], OBIT_IO_ReadOnly, err);
  ObitImageOpen(in->FullField, OBIT_IO_ReadWrite, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);
  in->FullField->myDesc->beamMaj = in->images[0]->myDesc->beamMaj;
  in->FullField->myDesc->beamMin = in->images[0]->myDesc->beamMin;
  in->FullField->myDesc->beamPA  = in->images[0]->myDesc->beamPA;
  in->FullField->myDesc->niter = 1;

  /* Create weight scratch array (temp local pointers for output) */
  sc1 = in->FullField->image;              /* Flattened image FArray */
  out = in->FullField;
  sc2 = newObitFArray("Scratch Weight array");
  ObitFArrayClone (sc1, sc2, err);         /* weight array */
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  ObitImageClose(in->images[0], err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Working, memory only Images */
  tout1 = newObitImage ("Interpolated Image");
  tout2 = newObitImage ("Weight Image");

  /* How big do we want ? */
  /*radius = MAX (in->FOV/(3600.0*in->xCells), in->FOV/(3600.0*in->yCells));*/
  radius = in->Radius;
 
  /* Zero fill accumulations */
  ObitFArrayFill (sc1, 0.0);
  ObitFArrayFill (sc2, 0.0);

  /* Loop over tiles */
  for (i= 0; i<in->numberImages; i++) { /* loop 500 */
    /* Open full image */
    dim[0] = IM_MAXDIM;
    blc[0] = blc[1] = blc[2] = blc[4] = blc[5] = 1;
    ObitInfoListPut (in->images[i]->info, "BLC", OBIT_int, dim, blc, err); 
    trc[0] = trc[1] = trc[2] = trc[4] = trc[5] = 0;
    ObitInfoListPut (in->images[i]->info, "TRC", OBIT_int, dim, trc, err); 
    dim[0] = 1;
    ObitInfoListPut (in->images[i]->info, "IOBy", OBIT_int, dim, &IOsize, err);
    ObitImageOpen(in->images[i], OBIT_IO_ReadOnly, err);
    ObitImageClose(in->images[i], err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);

    /* Input subimage dimension  (trim edges) */
    naxis = in->images[i]->myDesc->inaxes;

    /* Fudge a bit at the edges */
    rad = radius + 3;
    if (rad > ((naxis[0]/2)-2)) rad = (naxis[0]/2) - 3;
    if (rad > ((naxis[1]/2)-2)) rad = (naxis[1]/2) - 3;
    blc[0] = (naxis[0] / 2) - rad;
    blc[0] = MAX (2, blc[0]);
    blc[1] = (naxis[1] / 2) + 1 - rad;
    blc[1] = MAX (2, blc[1]);
    trc[0] = (naxis[0] / 2) + rad;
    trc[0] = MIN (naxis[0]-1, trc[0]);
    trc[1] = (naxis[1] / 2) + 1 + rad;
    trc[1] = MIN (naxis[1]-1, trc[1]);

   /* Open/read sub window of image */
    dim[0] = IM_MAXDIM;
    ObitInfoListPut (in->images[i]->info, "BLC", OBIT_int, dim, blc, err); 
    ObitInfoListPut (in->images[i]->info, "TRC", OBIT_int, dim, trc, err); 
    dim[0] = 1;

    /* Is there some overlap with flattened image? */
    in->images[i]->extBuffer = TRUE;  /* Don't need buffer here */
    ObitImageOpen(in->images[i], OBIT_IO_ReadOnly, err);
    ObitImageClose(in->images[i], err);
    in->images[i]->extBuffer = FALSE;  /* May need buffer later */
    overlap = ObitImageDescOverlap(in->images[i]->myDesc, in->FullField->myDesc, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);

    if (overlap) {    /* Something to do */
      /* Make or resize Interpolated scratch arrays (memory only images) */
      ObitImageClone2 (in->images[i], out, tout1, err);
      ObitImageClone2 (in->images[i], out, tout2, err);

      /* debug 
      fprintf(stderr,"Interpolate tile %d %ld %ld\n",
	      i,tout1->image->naxis[0],tout1->image->naxis[1]);*/
	
      naxis = tout1->myDesc->inaxes; /* How big is output */

      /* Interpolate and weight image */
      /* reopen with windowing */
      ObitImageOpen (in->images[i], OBIT_IO_ReadOnly, err);
      ObitImageRead (in->images[i], NULL, err); /* Read plane */
      if (err->error) Obit_traceback_msg (err, routine, in->name);

      ObitImageUtilInterpolateWeight (in->images[i], tout1, tout2, TRUE, rad, 
				      plane, plane, hwidth, err);
      if (err->error) Obit_traceback_msg (err, routine, in->name);

      /* Close, deallocate buffer */
      ObitImageClose(in->images[i], err);
      in->images[i]->image = ObitFArrayUnref(in->images[i]->image); /* Free buffer */
      if (err->error) Obit_traceback_msg (err, routine, in->name);

      /* Paste into accumulation images */
      /* Weighted image - first need pixel alignment */
      pos1[0] = tout1->image->naxis[0]/2; /* Use center of tile */
      pos1[1] = tout1->image->naxis[1]/2;
      xpos1[0] = pos1[0];
      xpos1[1] = pos1[1];
      /* Find corresponding pixel in output */
      ObitImageDescCvtPixel (tout1->myDesc, in->FullField->myDesc, xpos1, xpos2, err);
      if (err->error) Obit_traceback_msg (err, routine, in->name);
      pos2[0] = (gint)(xpos2[0]+0.5);
      pos2[1] = (gint)(xpos2[1]+0.5);

      /* accumulate image*weight */
      ObitFArrayShiftAdd (sc1, pos2, tout1->image, pos1, 1.0, sc1);

      /* accumulate weight */
      ObitFArrayShiftAdd (sc2, pos2, tout2->image, pos1, 1.0, sc2);
    } /* end if overlap */
  } /* end loop  L500 over input images */

  /* DEBUG
  ObitImageUtilArray2Image ("DbugSumWI.fits", 1, sc1, err);
  ObitImageUtilArray2Image ("DbugSumWW.fits", 1, sc2, err); */

  /* Normalize */
  ObitFArrayDivClip (sc1, sc2, 0.01, out->image);

  /* Write output */
  ObitImageWrite (in->FullField, out->image->array, err); 
  ObitImageClose (in->FullField, err);
  in->FullField->image = ObitFArrayUnref(in->FullField->image); /* Free buffer */

  /* Clear BLC,TRC on images */
  dim[0] = IM_MAXDIM;
  blc[0] = blc[1] = blc[2] = blc[4] = blc[5] = 1;
  trc[0] = trc[1] = trc[2] = trc[4] = trc[5] = 0;
  ObitInfoListPut (in->FullField->info, "BLC", OBIT_int, dim, blc, err); 
  ObitInfoListPut (in->FullField->info, "TRC", OBIT_int, dim, trc, err); 
  for (i=0; i<in->numberImages; i++) {
    ObitInfoListPut (in->images[i]->info, "BLC", OBIT_int, dim, blc, err); 
    ObitInfoListPut (in->images[i]->info, "TRC", OBIT_int, dim, trc, err); 
  }

  /* Cleanup */
  sc2   = ObitFArrayUnref(sc2);
  tout1 = ObitImageUnref(tout1);
  tout2 = ObitImageUnref(tout2);

} /* end ObitImageMosaicFlatten */

/**
 * Return the Field of View of a Mosaic
 * Given value on object unless it is zero and then it works it out from the 
 * array of images and saves it on the object.
 * \param in      The object with images
 * \param err     Error stack, returns if not empty.
 * \return the full width of the field of view in deg.
 */
gfloat ObitImageMosaicFOV (ObitImageMosaic *in, ObitErr *err)
{
  gfloat FOV = 0.0;
  gfloat pixel[2], xshift, yshift, shift, rotate=0.0;
  gdouble pos[2], pos0[2];
  gint i, n;
  ObitImageDesc *desc=NULL;
  gchar *routine = "ObitImageMosaicFOV";

  /* error checks */
  if (err->error) return FOV;
  g_assert (ObitIsA(in, &myClassInfo));

  /* Do we have it already? */
  if (in->FOV>0.0) return in->FOV;

  /* No images? We're wasting time */
  if (in->numberImages<=0) return FOV;

  /* If only one, it's the size */
  if (in->numberImages==1) {
    desc = in->images[0]->myDesc;
    in->FOV = MAX ((desc->inaxes[0]*fabs(desc->cdelt[0])), 
		   (desc->inaxes[1]*fabs(desc->cdelt[1])));
    return in->FOV;
  }

  /* Use the center of the first image as the reference position */
  desc = in->images[0]->myDesc;
  FOV = 0.5 * MAX ((desc->inaxes[0]*fabs(desc->cdelt[0])), 
		   (desc->inaxes[1]*fabs(desc->cdelt[1])));
  ObitImageDescGetPos (desc, desc->crpix, pos0, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, FOV);
  
  /* Loop over images, in Fly's eye unless that size is zero, then all */
  n = in->nFlyEye;
  if (n<=0) n = in->numberImages;
  for (i=0; i<n; i++) {
    desc = in->images[i]->myDesc;
    /* All four corners */
    pixel[0] = 1; pixel[1] = 1;
    ObitImageDescGetPos (desc, pixel, pos, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, FOV);
    /* Get separation */
    ObitSkyGeomShiftXY (pos0[0], pos0[1], rotate, pos[0], pos[1], 
			&xshift, &yshift);
    shift = MAX (fabs(xshift), fabs(yshift));
    FOV = MAX (FOV, shift);

    pixel[0] = desc->inaxes[0]; pixel[1] = desc->inaxes[1];
    ObitImageDescGetPos (desc, pixel, pos, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, FOV);
    /* Get separation */
    ObitSkyGeomShiftXY (pos0[0], pos0[1], rotate, pos[0], pos[1], 
			&xshift, &yshift);
    shift = MAX (fabs(xshift), fabs(yshift));
    FOV = MAX (FOV, shift);

    pixel[0] = desc->inaxes[0]; pixel[1] = 1;
    ObitImageDescGetPos (desc, pixel, pos, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, FOV);
    /* Get separation */
    ObitSkyGeomShiftXY (pos0[0], pos0[1], rotate, pos[0], pos[1], 
			&xshift, &yshift);
    shift = MAX (fabs(xshift), fabs(yshift));
    FOV = MAX (FOV, shift);

    pixel[0] = 1; pixel[1] = desc->inaxes[1];
    ObitImageDescGetPos (desc, pixel, pos, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, FOV);
    /* Get separation */
    ObitSkyGeomShiftXY (pos0[0], pos0[1], rotate, pos[0], pos[1], 
			&xshift, &yshift);
    shift = MAX (fabs(xshift), fabs(yshift));
    FOV = MAX (FOV, shift);
  } /* end loop over images */

  in->FOV = FOV;  /* Save result */

  return in->FOV;
} /* end ObitImageMosaicFOV */

/**
 * Initialize global ClassInfo Structure.
 */
 void ObitImageMosaicClassInit (void)
{
  const ObitClassInfo *ParentClass;

  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;

  /* Initialize (recursively) parent class first */
  ParentClass = ObitGetClass();
  if ((ParentClass!=NULL) && (ParentClass->ObitClassInit!=NULL))
    ParentClass->ObitClassInit();

  /* function pointers etc. for this class */
  myClassInfo.hasScratch    = FALSE; /* Scratch files allowed */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = (ObitClassInitFP)ObitImageMosaicClassInit;
  myClassInfo.newObit       = (newObitFP)newObitImageMosaic;
  myClassInfo.ObitCopy      = (ObitCopyFP)ObitImageMosaicCopy;
  myClassInfo.ObitClone     = NULL;  /* Different call */
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitImageMosaicClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitImageMosaicInit;
} /* end ObitImageMosaicClassInit */

/*---------------Private functions--------------------------*/

/**
 * Creates empty member objects, initialize reference count.
 * Parent classes portions are (recursively) initialized first
 * \param inn Pointer to the object to initialize.
 */
void ObitImageMosaicInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  gint i;
  ObitImageMosaic *in = inn;

  /* error checks */
  g_assert (in != NULL);

  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->thread    = newObitThread();
  in->info      = newObitInfoList(); 
  in->FullField = NULL;
  in->doFull    = FALSE;
  in->images    = ObitMemAlloc0Name(in->numberImages*sizeof(ObitImage*),"ImageMosaic images");
  for (i=0; i<in->numberImages; i++) in->images[i] = NULL;
  in->nx        = ObitMemAlloc0Name(in->numberImages*sizeof(gint),"ImageMosaic nx");
  in->ny        = ObitMemAlloc0Name(in->numberImages*sizeof(gint),"ImageMosaic ny");
  in->nplane    = ObitMemAlloc0Name(in->numberImages*sizeof(gint),"ImageMosaic nplane");
  in->RAShift   = ObitMemAlloc0Name(in->numberImages*sizeof(gfloat),"ImageMosaic RAShift");
  in->DecShift  = ObitMemAlloc0Name(in->numberImages*sizeof(gfloat),"ImageMosaic DecShift");
  in->imName    = NULL;
  in->imClass   = NULL;
  in->bmaj      = 0.0;
  in->bmin      = 0.0;
  in->bpa       = 0.0;
  in->Radius    = 0.0;
  in->OutlierSize= 0;

} /* end ObitImageMosaicInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * \param  inn Pointer to the object to deallocate.
 *           Actually it should be an ObitImageMosaic* cast to an Obit*.
 */
void ObitImageMosaicClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitImageMosaic *in = inn;
  gint i;
  
  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  
  /* delete this class members */
  in->thread    = ObitThreadUnref(in->thread);
  in->info      = ObitInfoListUnref(in->info);
  in->FullField = ObitUnref(in->FullField);
  if (in->images) {
    for (i=0; i<in->numberImages; i++)
      in->images[i] = ObitUnref(in->images[i]);
    in->images = ObitMemFree(in->images); 
  }
  if (in->nx)       ObitMemFree(in->nx);
  if (in->ny)       ObitMemFree(in->ny);
  if (in->nplane)   ObitMemFree(in->nplane);
  if (in->RAShift)  ObitMemFree(in->RAShift);
  if (in->DecShift) ObitMemFree(in->DecShift);
  if (in->imName)   g_free(in->imName);
  if (in->imClass)  g_free(in->imClass);
 
  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);
  
} /* end ObitImageMosaicClear */

/**
 * Set fly's eye fields using a hexagonal pattern of overlapping, circular fields.
 * Translated from the AIPSish FLYEYE in $FOURMASS/SUB/ADDFIELDS.FOR
 * \param  radius  radius of fly's eye in degree 
 * \param  imsize  size of circular regions to be imaged 
 *                 this is the size of the undistorted region. 
 * \param  cells   pixel size (asec) 
 * \param  overlap number pixels of overlap 
 * \param  shift   shift all coordinates 
 * \param  ra0     center ra in degrees 
 * \param  dec0    center dec in degrees 
 * \param  nfield  (output) number fields already added 
 * \param  fldsiz  (output) circular image field sizes (pixels) 
 * \param  rash    (output) ra shifts (asec) 
 * \param  decsh   (output) declination shifts (asec) 
 * \param  flqual  (output) field quality (crowding) code, -1 => ignore 
 * \param  err     Error stack, returns if not empty.
 * \return radius of zone of avoidence for externals (deg)
 */
static gfloat
FlyEye (gfloat radius, gint imsize, gfloat cells[2], gint overlap,
	gfloat shift[2], gdouble ra0, gdouble dec0, 
	gint *nfield, gint *fldsiz, gfloat *rash, gfloat *decsh, gint *flqual,
	ObitErr *err) 
{
  gfloat out = 0.0;
  gdouble cosdec, sindec, cosfld, sinfld, ra[MAXFLD], dec[MAXFLD], dx, dy, 
    drad, xra, xdec, xd,  xsh[2], ll, mm, maxrad, ra0r, dec0r, dradd;
  gfloat      dist[MAXFLD], mrotat, rxsh[2];
  gboolean   this1, warn;
  gint   ifield, mfield, ii, jj, indx[MAXFLD], i1, i2, j1, j2, jerr, nfini, mxfld;
  gchar *routine = "FlyEye";

  /* error checks */
  if (err->error) return out;

  for (ii=0; ii<MAXFLD; ii++) dist[ii] = -1.0;
  maxrad = 0.0;
  cosdec = cos (dec0*DG2RAD);
  sindec = sin (dec0*DG2RAD);
  ra0r   = ra0 * DG2RAD;
  dec0r  = dec0 * DG2RAD;
  nfini  = *nfield;
  warn   = FALSE;

  /* shift to degrees */
  xsh[1] = shift[1] / 3.6e3;
  xsh[0] = shift[0] / 3.6e3 / cosdec;

  /* spacing of imaging region in  rad.   */
  drad = 0.5 * AS2RAD * fabs(cells[0]) * (imsize - overlap);
  dx = 1.5 * drad;
  dy = cos (30.0*DG2RAD) * drad;

  /* field size in deg. */
  dradd = drad * RAD2DG / sqrt (2.0);

  /* collect list of fields to add */
  ifield = *nfield;

  /* maximum number of fields to add, only use inscribed circle in square array. */
  mfield = 2.0 * sqrt (MAXFLD / G_PI);

  /* don't go overboard - only twice  radius */
  mxfld = 0.5 + 2.0 * (radius) / dradd;
  mxfld = MAX (1, mxfld);
  mfield = MIN (mfield, mxfld);
  i2 = mfield;
  i1 = -i2;
  j2 = mfield/2 + 1;
  j1 = -j2;
  this1 = FALSE;

  /* See if one field is enough? */
  if (radius < (0.5*imsize*fabs(cells[0])/3600.0)) {  /* Yes one will do */
    i1 = i2 = 0;
    maxrad = radius;
  } /* end of one field is enough */

 /* Loop over declination */
  for (ii= i1; ii<=i2; ii++) { /* loop 20 */
    mm = ii * dy;

    /* Loop over RA */
    for (jj= i1; jj<=i2; jj++) { /* loop 10 */
      ll = jj * dx;
      /* do every other possibility to get hexagonal pattern */
      this1 = !this1;
      if (this1) {

	/* Convert to RA, Dec (sine projection)  */
	ObitSkyGeomNewPos (OBIT_SkyGeom_SIN, ra0r, dec0r, ll, mm, &xra, &xdec, &jerr);
	if (jerr != 0) {
	  /* angle too large for sine projection. */
	  xra  = ra0 + 2.0 * (radius);
	  xdec = dec0 - 2.0 * (radius);
	  xd   = 1000.0 * (fabs(radius)+dradd);
	} else {
	  /* ok - convert to deg. */
	  xra  = xra*RAD2DG  + xsh[0];
	  xdec = xdec*RAD2DG + xsh[1];
	  /* distance from center */
	  cosfld = cos (xdec*DG2RAD);
	  sinfld = sin (xdec*DG2RAD);
	  xd = sinfld * sindec + cosfld * cosdec * cos (xra*DG2RAD - ra0r);
	  if (xd > 1.0) {
	    xd = 0.0;
	  } else {
	    xd = RAD2DG * acos (xd);
	  } 
	} 

	/* Add field to list if close enough  */
	/* debug 
	fprintf (stderr,"%d %lf %lf %lf\n",ifield,ll,mm,xd);*/
	if (xd < (fabs(radius)+dradd)) {
	  if (ifield < MAXFLD) {
	    ra[ifield]     = xra;
	    dec[ifield]    = xdec;
	    dist[ifield]   = xd;
	    fldsiz[ifield] = imsize;
	    flqual[ifield] = 0;
	    indx[ifield]   = ifield;
	    ifield++;
	    maxrad = MAX (maxrad, xd);
	  } else {
	    warn = TRUE;
	  } 
	} /* end add field that's close enough */
      } /* end pick every other entry to get hexagonal pattern */
    } /* end loop  L10 over RA:  */
  } /* end loop  L20 over declination:  */

  mfield = ifield;

  /* sort on distances */
  Bubble (dist, indx, ifield, 1);

  /* save to output */
  if (radius > 0.0) {
    
    mrotat = 0.0;/* assume no systematic rotation */
    for (ii = 0; ii<mfield; ii++) { /* loop 30 */
      ObitSkyGeomShiftXY (ra0, dec0, mrotat, ra[indx[ii]], dec[indx[ii]], 
			  &rxsh[0], &rxsh[1]);
      AddField (rxsh, dec[indx[ii]], imsize, cells, -1, FALSE, 
		nfield, fldsiz, rash, decsh, flqual, err);
    } /* end loop  L30:  */
  } 

  /* tell how many */
  mfield = *nfield - nfini;
  Obit_log_error(err, OBIT_InfoErr, "%s: added %d flys eye fields", routine, mfield);

  /* If no fields complain and die */
  if ((*nfield)<=0) {
  Obit_log_error(err, OBIT_Error, "%s: NO fields to be imaged ",routine);
  }

  /* too many? */
  if (warn) 
    Obit_log_error(err, OBIT_InfoWarn, 
		   "%s: Exceeded field limit - full field will not be imaged", 
		   routine);

  /* want confusing sources outside  of this */
  out = maxrad;
  return out;
} /* end of routine FlyEye */ 


/**
 * Add a field the the field list if there is room and if it not within 3/4 
 * the radius of another field.
 * Translated from the AIPSish ADFLDX in $FOURMASS/SUB/ADDFIELDS.FOR
 * \param   shift    x,y shift in deg 
 * \param   dec      declination of new field (deg) 
 * \param   imsize   desired size as diameter in pixels of field 
 * \param   cells    cell size in arc sec 
 * \param   qual     input field quality (crowding) code. 
 * \param   check    if true check if this is in another field 
 * \param   nfield   (output) number fields already added 
 * \param   fldsiz   (output) circular image field sizes (pixels) 
 * \param   rash     (output) ra shifts (asec) 
 * \param   decsh    (output) declination shifts (asec) 
 * \param   flqual   (output) field quality (crowding) code, -1 => ignore 
 * \param   err      Error stack, returns if not empty.
 * \return 0 if added, 1=matches previous, 2=table full
 */
static int 
AddField (gfloat shift[2], gfloat dec, gint imsize, gfloat cells[2], 
	  gint qual, gboolean check, 
	  gint *nfield, gint *fldsiz, gfloat *rash, gfloat *decsh, gint *flqual,
	  ObitErr *err) 
{
  gint   i;
  gfloat      dist, maxdis, cosdec, xsh, ysh, xxsh, yysh;
  gchar *routine = "AddField";
  
  /* error checks */
  if (err->error) return 1;

  /* is this in a previous field? */
  if (check) {
    cosdec = cos (dec*DG2RAD);
    xsh = shift[0]*3600.0;
    ysh = shift[1]*3600.0;
    for (i = 0; i<(*nfield); i++) { /* loop 100 */
      xxsh = rash[i]/3600.0;
      yysh = decsh[i]/3600.0;
      /* work in pixels**2 */
      dist = (cosdec * (xxsh - xsh) / cells[0]) * 
	(cosdec * (xxsh - xsh) / cells[0]) + 
	((yysh - ysh) / cells[1]) * ((yysh - ysh) / cells[1]);
      maxdis = (0.5 * 0.75 * fldsiz[i]) * (0.5 * 0.75 * fldsiz[i]);
      if (dist < maxdis) {
	Obit_log_error(err, OBIT_InfoWarn, 
		       "%s: Rejecting additional field at %g %g, in previous field %d", 
		       routine, shift[0], shift[1], i+1);
	return 1;
      } /* end of already in another field */
    } /* end loop  L100: */
  } 

  /* if you got here - it must be ok */
  if ((*nfield) < MAXFLD) {
    (*nfield)++;
    fldsiz[(*nfield)-1] = ObitFFTSuggestSize (imsize); /* Make it FFT friendly */
    rash[(*nfield)-1]   = shift[0]*3600.0;
    decsh[(*nfield)-1]  = shift[1]*3600.0;
    flqual[(*nfield)-1] = qual;
  } else {
    /* can't fit it */
    return 2;
  } 
  return 0;
} /* end of routine  AddField */ 

/**
 * In place bubble sort with tracking of swap indices
 * \param data    data array to sort, returned sorted 
 * \param indx    [out] indices (0-rel) for sorted data.
 * \param number  No. elements in data, indx
 * \param direct  sort ordet, 1=ascending, -1=descending
 */
 static void 
 Bubble (gfloat *data, gint* indx, gint number, gint direct)
{
  gint   i, j, it;
  gfloat      rt;
  gboolean   done;
  
  done = FALSE;
  i = 1;
  
  /* descending order sort */
  if (direct == -1) {
    while ((i < number)  &&  (!done)) {
      done = TRUE;
      for (j= number; j>=i+1; j--) { /* loop 20 */
	if (data[j-1] > data[j-2]) {
	  rt = data[j-1];
	  data[j-1] = data[j-2];
	  data[j-2] = rt;
	  it = indx[j-1];
	  indx[j-1] = indx[j-2];
	  indx[j-2] = it;
	  done = FALSE;
	} 
      } /* end loop  L20:  */;
      i++;
    } /* end while loop */
    
    /* ascending order sort */
  } else {
    while ((i < number)  &&  (!done)) {
      done = TRUE;
      for (j= number; j>=i+1; j--) { /* loop 40 */
	if (data[j-1] < data[j-2]) {
	  rt = data[j-1];
	  data[j-1] = data[j-2];
	  data[j-2] = rt;
	  it = indx[j-1];
	  indx[j-1] = indx[j-2];
	  indx[j-2] = it;
	  done = FALSE;
	} 
      } /* end loop  L40:  */;
      i++;
    } /* end while loop */
  } 

} /* end of routine Bubble */ 

/**
 * Searches Catalog for sources in the desired ra, dec, and flux 
 * range taking into account the estimated single-dish beam 
 * Translated from the AIPSish ADNVSS in $FOURMASS/SUB/ADDFIELDS.FOR
 * \param Catalog      FITS AIPSVZ format catalog file name
 * \param catDisk      FITS sidk number for Catalog
 * \param minRad       Minimum distance from field center to include
 * \param cells        pixel size (asec)
 * \param OutlierDist  Maximum distance (deg) from center - if zero return
 * \param OutlierFlux  Minimum estimated flux density 
 * \param OutlierSI    Spectral index to use to convert catalog flux density
 * \param OutlierSize  Width of outlier field in pixels
 * \param ra0          center ra in degrees 
 * \param dec0         center dec in degrees 
 * \param doJ2B        If True precess catalog from J2000 to B1950
 * \param Freq         Frequency of data in Hz
 * \param nfield       (output) number fields already added
 * \param fldsiz       (output) circular image field sizes (pixels) 
 * \param rash         (output) ra shifts (asec)
 * \param decsh        (output) declination shifts (asec) 
 * \param flqual       (output) field quality (crowding) code, -1 => ignore 
 * \param err          Error stack, returns if not empty.
 */
static void 
AddOutlier (gchar *Catalog, gint catDisk, gfloat minRad, gfloat cells[2], 
	    gfloat OutlierDist, gfloat OutlierFlux, gfloat OutlierSI, gint OutlierSize,
	    gdouble ra0, gdouble dec0, gboolean doJ2B, gdouble Freq, 
	    gint *nfield, gint *fldsiz, gfloat *rash, gfloat *decsh, gint *flqual, 
	    ObitErr *err) 
{
  gint count, imsize, jerr, nfini, qual;
  gdouble ra, dec, ra2000, dc2000;
  gdouble xx, yy, zz, dist, refreq;
  gfloat radius, minflx, asize, alpha, pbf;
  gfloat flux, scale, xsh[2];
  gboolean wanted, doJinc, warn;
  gint blc[IM_MAXDIM] = {1,1,1,1,1};
  gint trc[IM_MAXDIM] = {0,0,0,0,0};
  glong ver, nrows, irow;
  ObitIOCode retCode;
  ObitImage *VZImage=NULL;
  ObitTableVZ *VZTable=NULL;
  ObitTableVZRow *VZRow=NULL;
  gchar *routine = "AddOutlier";
  
  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;

  /* Really needed? */
  if (OutlierDist<=1.0e-10) return;
  
  /* get control parameters */
  radius = OutlierDist;
  minflx = OutlierFlux;
  alpha  = OutlierSI;
  imsize = OutlierSize;
  if (imsize <= 0) imsize = 50;
  asize = 25.0;      /* antenna diameter in meters */
  nfini = *nfield;
  warn = TRUE;

  /* get j2000 position to lookup in Catalog in radians */
  ra2000 = DG2RAD * ra0;
  dc2000 = DG2RAD * dec0;
  if (doJ2B) ObitSkyGeomBtoJ (&ra2000, &dc2000);

  /* set defaults. */
  if (radius <= 0.0) radius = 15.0;
  if (asize <= 0.0)  asize  = 25.0;
  if (alpha == 0.0)  alpha  = -0.75;

  /* which beam model to use */
  doJinc = (Freq >= 1.0e9);

  /* Open Catalog (VZ table on an image) */
  VZImage = newObitImage("Catalog image");
  ObitImageSetFITS(VZImage, OBIT_IO_byPlane, catDisk, Catalog, blc, trc, err);

  /* Open to fully instantiate */
  ObitImageOpen(VZImage, OBIT_IO_ReadOnly, err);
  if (err->error) Obit_traceback_msg (err, routine, VZImage->name);

  /* Now get VZ table */
  ver = 1;
  VZTable =  newObitTableVZValue("Catalog table", (ObitData*)VZImage, &ver, 
				 OBIT_IO_ReadOnly, err);
  ObitTableVZOpen(VZTable, OBIT_IO_ReadOnly, err);
  VZRow =  newObitTableVZRow (VZTable);  /* Table row */
  if (err->error) Obit_traceback_msg (err, routine, VZTable->name);

  /* Get table info */
  refreq = VZTable->refFreq;
  nrows  = VZTable->myDesc->nrow;

  /* frequency scaling */
  scale = pow ((Freq / refreq), alpha);

  /* loop through table */
  count = 0;
  warn = FALSE;
  for (irow= 1; irow<=nrows; irow++) { /* loop 500 */
    /* read */
    retCode = ObitTableVZReadRow (VZTable, irow, VZRow, err);
    if (err->error) Obit_traceback_msg (err, routine, VZTable->name);
   
    /* spectral scaling of flux density */
    flux = VZRow->PeakInt * scale;

    /* position, etc */
    ra   = VZRow->Ra2000;
    dec  = VZRow->Dec2000;
    qual = VZRow->Quality;

    /* select (crude) */
    if ((fabs(dec0-dec) <= radius)  &&  (flux >= minflx)) {
      /* separation from pointing center */
      xx = DG2RAD * ra;
      yy = DG2RAD * dec;
      zz = sin (yy) * sin (dc2000) + cos (yy) * cos (dc2000) * cos (xx-ra2000);
      dist = acos (zz) * RAD2DG;

      /* primary beam correction to flux density */
      if (doJinc) {
	pbf = ObitPBUtilJinc (dist, Freq, asize);
      } else {
	pbf = ObitPBUtilPoly (dist, Freq);
      } 
      flux = flux * pbf;
      
      /* select (fine) */
      wanted = ((flux >= minflx)  &&  (dist <= radius))  && (dist > minRad);
      if (wanted) {
	if (doJ2B) {  /* precess if necessary */
	  ra  *= DG2RAD;
	  dec *= DG2RAD;
	  ObitSkyGeomBtoJ (&ra, &dec);
	  ra  *= RAD2DG;
	  dec *= RAD2DG;
	} 

	/* get shift needed */
	ObitSkyGeomShiftXY (ra0, dec0, 0.0, ra, dec, &xsh[0], &xsh[1]);

	/* add it if you can */
	jerr = AddField (xsh, dec, imsize, cells, qual, TRUE, 
			 nfield, fldsiz, rash, decsh, flqual, err);
	warn = warn  ||  (jerr != 0);
       
	/* debug */
	if (jerr==0) Obit_log_error(err, OBIT_InfoErr, "Field %d is %f %f", *nfield, ra, dec);
      } /* end if wanted */ 
    } /* end crude selection */
  } /* end loop over table */

  /* tell how many */
  count = *nfield - nfini;
  Obit_log_error(err, OBIT_InfoErr, "%s: Added %d outlying fields", routine, count);

  /* too many? */
  if ((*nfield)>=MAXFLD) Obit_log_error(err, OBIT_InfoWarn, 
					"%s: Hit field limit - some outliers may be ignored", 
					routine);
  /* Close up */
  ObitImageClose(VZImage, err);
  retCode = ObitTableVZClose(VZTable, err);
  if (err->error) Obit_traceback_msg (err, routine, VZTable->name);
  
  /* clean up */
  VZImage = ObitImageUnref(VZImage);
  VZTable = ObitTableUnref(VZTable);
  VZRow   = ObitTableRowUnref(VZRow);
  
} /* end of routine AddOutlier */ 

