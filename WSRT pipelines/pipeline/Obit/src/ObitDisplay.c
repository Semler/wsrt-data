/* $Id: ObitDisplay.c,v 1.5 2005/08/07 18:46:41 bcotton Exp $    */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2005                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "ObitAIPSDir.h"
#include "ObitFITS.h"
#include "ObitDisplay.h"
#include "ObitImageMosaic.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitDisplay.c
 * ObitDisplay class function definitions.
 * This class is derived from the Obit base class.
 */

/** name of the class defined in this file */
static gchar *myClassName = "ObitDisplay";

/**
 * ClassInfo structure ObitDisplayClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitDisplayClassInfo myClassInfo = {FALSE};

/*--------------- File Global Variables  ----------------*/


/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitDisplayInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitDisplayClear (gpointer in);

/** Private: ping server. */
static gboolean pingServer (ObitDisplay* display, ObitErr *err);

/** Private: Load Image. */
static gboolean LoadImage (ObitDisplay* display, ObitImage *image, 
			   gint field, gint nfield, ObitInfoList **request, 
			   ObitErr *err);

/** Private: Edit Window. */
static gboolean EditWindow (ObitDisplay* display, ObitDConCleanWindow *window, 
			    gint field, ObitInfoList **request, ObitErr *err);

/**  Private: Turn Image info into an ObitXML */
static ObitXML* getImageInfo (ObitImage *image, gint field, gint nfield,
			      ObitErr *err);

/*----------------------Public functions---------------------------*/
/**
 * Constructor.
 * Initializes class if needed on first call.
 * \param name An optional name for the object.
 * \return the new object.
 */
ObitDisplay* newObitDisplay (gchar* name)
{
  ObitDisplay* out;

  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitDisplayClassInit();

  /* allocate/init structure */
  out = g_malloc0(sizeof(ObitDisplay));

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

  /* set ClassInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  ObitDisplayInit((gpointer)out);

 return out;
} /* end newObitDisplay */

/**
 * Returns ClassInfo pointer for the class.
 * \return pointer to the class structure.
 */
gconstpointer ObitDisplayGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitDisplayClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitDisplayGetClass */

/**
 * Creates an ObitDisplay
 * \param name      An optional name for the object.
 * \param ServerURL URL of display server, 
 *        NULL defaults to "http://localhost:8765/RPC2"
 * \param err       Obit Error message
 * \return the new object.
 */
ObitDisplay* ObitDisplayCreate (gchar* name, gchar *ServerURL, ObitErr *err)
{
  ObitDisplay* out;
  gchar *routine = "ObitDisplayCreate";

  /* Only allow one */
  Obit_retval_if_fail((myClassInfo.numberDisplay<1), err, NULL,
		      "%s: ONLY One XMLDisplay  allowed", routine);
  myClassInfo.numberDisplay++;

  /* Create basic structure */
  out = newObitDisplay (name);
  out->client = ObitRPCCreateClient (name, err);
  if ((ServerURL!=NULL) && (strncmp(ServerURL,"      ",6)) && 
      (strncmp(ServerURL,"ObitView",8)))
    out->ServerURL = g_strdup(ServerURL);
  else if ((ServerURL!=NULL) && (!strncmp(ServerURL,"ObitView",8)))
    out->ServerURL = g_strdup("http://localhost:8765/RPC2");
  else out->ServerURL = g_strdup("http://localhost:8765/RPC2");
  
  return out;
} /* end ObitDisplayCreate */

/**
 * Display an image with optional CLEAN window editing
 * Either a single image or an image mosaic can be passed.
 * For a mosaic, the user can request other images from the mosaic.
 * \param display    ObitDisplay object
 * \param image      ObitImage or Image Mosaic
 * \param window     if nonNULL window corresponding to image
 *                   possible edited by user. 
 *                   This MUST correspond to image.
 * \param field      If image= an ImageMosaic then this is the 
 *                   1-rel field number
 * \param err        Obit Error message
 * \return TRUE if user wants to quit
 */
gboolean ObitDisplayShow (ObitDisplay* display, Obit *image, 
		      ObitDConCleanWindow *window, 
		      gint field, ObitErr *err)
{
  gboolean out = FALSE;
  ObitImage *curImage=NULL;
  ObitImageDesc *desc=NULL;
  ObitImageMosaic *mosaic=NULL;
  gboolean isMosaic, doEdit=FALSE;
  gint32 dim[MAXINFOELEMDIM];
  ObitInfoType infoType;
  ObitInfoList *request=NULL;
  gint req, ifield, jfield, nfield=1;
  gchar *routine = "ObitObitDisplayShow";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return out;
  g_assert (ObitDisplayIsA(display));
  if (display->turnedOff) return out;  /* display turned off? */
  if (window) g_assert (ObitDConCleanWindowIsA(window));
  g_assert (ObitImageIsA(image) || ObitImageMosaicIsA(image));

  isMosaic = ObitImageMosaicIsA(image);
  if (isMosaic) {
    mosaic = (ObitImageMosaic*)image;
    nfield = mosaic->numberImages;

    /* If window given check for compatability */
    if (window) {
      doEdit = TRUE;
      Obit_retval_if_fail((mosaic->numberImages==window->nfield), err, out,
			  "%s: Mosaic and windows incompatible, No. field %d != %ld",
			  routine, mosaic->numberImages, window->nfield);
    }
  } else {  /* Image */
    curImage = (ObitImage*)image;
    doEdit = window!=NULL;
  }
  
  /* Progress Report to show and clear pending messages */
  ObitErrLog(err);

 /* Check server availability (ping) */
  if (!pingServer(display, err)) {
    /* works but server temporarily unavailable */
    Obit_log_error(err, OBIT_InfoWarn, "%s: Display unavailable",
		   routine);
    return out;
  }
  
  /* Loop until no more requests */
  ifield = field;
  while (1) {
    
    /* check field number */
    Obit_retval_if_fail(((ifield>0) && (ifield<=nfield)), err, out,
			"%s: field %d out of range [1,%d]",
			routine, ifield, nfield);
    if (mosaic) curImage = mosaic->images[ifield-1];

    /* Load image */
    if (!LoadImage (display, curImage, ifield, nfield, &request, err)) {
      /* Failed */
      Obit_log_error(err, OBIT_InfoWarn, "%s: Image load failed",
		     routine);
      return out;
    }
    
    /* Request? */
    req = OBIT_Request_Continue;
    if (request) {
      if (ObitInfoListGetTest (request, "Request", &infoType, dim, &req)) {
	/*fprintf (stdout, "Request from edit %d\n",req);*/
	if (ObitInfoListGetTest (request, "Field", &infoType, dim, &jfield)) {
	  /*fprintf (stdout, "Request field %d\n",field);*/
	}
      }
      request = ObitInfoListUnref(request);
    } /* End request handling */

    /* Edit windows if given */
    if (window && doEdit) {
      
      /* Check compatibility */
      desc = curImage->myDesc;
      Obit_retval_if_fail(((desc->inaxes[0]==window->naxis[ifield-1][0]) && 
			   (desc->inaxes[1]==window->naxis[ifield-1][1])), err, out,
			  "%s: Image and window incompatible [%ld,%ld] [%ld,%ld]",
			  routine, desc->inaxes[0], desc->inaxes[1], 
			  window->naxis[ifield-1][0], window->naxis[ifield-1][1]);
      
      
      /* Do edit */
      if (!EditWindow (display, window, ifield, &request, err)) {
	/* Failed */
	Obit_log_error(err, OBIT_InfoWarn, "%s: Image load failed",
		       routine);
	return out;
      }
      
      /* Request? */
      if (request) {
	if (ObitInfoListGetTest (request, "Request", &infoType, dim, &req)) {
	  /*fprintf (stdout, "Request from edit %d\n",req);*/
	  if (ObitInfoListGetTest (request, "Field", &infoType, dim, &jfield)) {
	    /*fprintf (stdout, "Request field %d\n",jfield);*/
	  }
	}
	request = ObitInfoListUnref(request);
      }
    } /* end editing windows */
    
    /* Handle any request */
    if (req==OBIT_Request_Abort) {
      Obit_log_error(err, OBIT_Error, "User requests Abort");
      return out;
    } else if (req==OBIT_Request_Quit) {
      Obit_log_error(err, OBIT_InfoWarn, "User requests Quit");
      out = TRUE;
      return out;
    } else if (req==OBIT_Request_Edit) {
      ifield = jfield;
      doEdit = window!=NULL;
    } else if (req==OBIT_Request_View) {
      ifield = jfield;
      doEdit = TRUE;
    } else if (req==OBIT_Request_NoTV) {
      ObitDisplayTurnOff (display);
      Obit_log_error(err, OBIT_InfoWarn, "User requests no further displays");
      break;
    } else if (req==OBIT_Request_Continue) {
      break;
    } else break;/* No request - exit loop */
    
  } /* end loop over requests */
  return out;
} /* end ObitDisplayShow */

/**
 * Enable display (default initial condition)
 * \param display    ObitDisplay object
 */
void ObitDisplayTurnOn (ObitDisplay* display)
{
  /* error check */
  g_assert (ObitDisplayIsA(display));

  display->turnedOff = FALSE;
} /* end ObitDisplayTurnOn */

/**
 * Disable display 
 * \param display    ObitDisplay object
 */
void ObitDisplayTurnOff (ObitDisplay* display)
{
  /* error check */
  g_assert (ObitDisplayIsA(display));

  display->turnedOff = TRUE;
} /* end ObitDisplayTurnOff */


/**
 * Initialize global ClassInfo Structure.
 */
void ObitDisplayClassInit (void)
{
  const ObitClassInfo *ParentClass;
  
  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;
  
  /* Initialize (recursively) parent class first */
  ParentClass = ObitGetClass();
  ObitClassInit();  /* Initialize parent class if needed */
  
  /* function pointers etc. for this class */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = (ObitClassInitFP)ObitDisplayClassInit;
  myClassInfo.newObit       = (newObitFP)newObitDisplay;
  myClassInfo.ObitCopy      = NULL;
  myClassInfo.ObitClone     = NULL;
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitDisplayClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitDisplayInit;
  myClassInfo.ObitDisplayCreate = 
    (ObitDisplayCreateFP)ObitDisplayCreate;
  myClassInfo.ObitDisplayShow = 
    (ObitDisplayShowFP)ObitDisplayShow;
  myClassInfo.ObitDisplayTurnOn = 
    (ObitDisplayTurnOnFP)ObitDisplayTurnOn;
  myClassInfo.ObitDisplayTurnOff = 
    (ObitDisplayTurnOffFP)ObitDisplayTurnOff;
  myClassInfo.numberDisplay  = 0;
} /* end ObitDisplayClassInit */

/*---------------Private functions--------------------------*/

/**
 * Creates empty member objects, initialize reference count.
 * Parent classes portions are (recursively) initialized first
 * \param inn Pointer to the object to initialize.
 */
void ObitDisplayInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitDisplay *in = inn;

  /* error checks */
  g_assert (in != NULL);

  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->thread    = newObitThread();
  in->turnedOff = FALSE;
  in->ServerURL = NULL;
} /* end ObitDisplayInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * \param  inn Pointer to the object to deallocate.
 *           Actually it should be an ObitDisplay* cast to an Obit*.
 */
void ObitDisplayClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitDisplay *in = inn;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* delete this class members */
  in->thread  = ObitThreadUnref(in->thread);
  in->client  = ObitRPCUnref(in->client);
  if (in->ServerURL) g_free(in->ServerURL); in->ServerURL = NULL;
  myClassInfo.numberDisplay--;

  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);
  
} /* end ObitDisplayClear */

/**
 * Ping server
 * Returns TRUE is the ping was successful and the server is available.
 * A communitations failure will result in the display being "turned Off"
 * and err set
 * \param display   ObitDisplay object
 * \param err       Obit Error message
 * \return TRUE if available
 */
static gboolean pingServer (ObitDisplay* display, ObitErr *err)
{
  gboolean out = FALSE;
  ObitXML *xml=NULL, *result=NULL;
  ObitInfoList *status=NULL, *request=NULL;
  gint32 dim[MAXINFOELEMDIM];
  ObitInfoType infoType;
  gint retCode;
  gchar *reason=NULL;
  gchar *routine = "ObitObitDisplay:pingServer";

  /* existing error? */
  if (err->error) return out;

  xml = ObitXMLPing2XML(err);
  result = ObitRPCCall (display->client, display->ServerURL, xml, &status, &request, err);
  /* If something goes wrong with communications, turn off */
  if (err->error) {
    ObitDisplayTurnOff (display);
    ObitErrClearErr (err);  /* ignore error */
    return out;
  }

  /* Check Status */
  retCode = -1;
  if (status) {
    ObitInfoListGet (status, "code", &infoType, dim, (gpointer)&retCode, err);
    ObitInfoListGetP (status, "reason", &infoType, dim, (gpointer*)&reason);
  }
  if (err->error) {
    ObitDisplayTurnOff (display);
    Obit_traceback_val (err, routine, display->name, out);
  }

  /* Did it work? */
  if (retCode!=0) {
    Obit_log_error(err, OBIT_InfoWarn, "%s: Could not talk to Display, code %d",
		   routine, retCode);
    Obit_log_error(err, OBIT_InfoWarn, "   because: %s",reason);
    return out;
  }

  /* Cleanup from load Image */
  status  = ObitInfoListUnref(status);
  request = ObitInfoListUnref(request);
  xml     = ObitXMLUnref(xml);
  result  = ObitXMLUnref(result);

  /* Must be OK */
  out = TRUE;

  return out;
} /* end pingServer */

/**
 * Send request to display server to load an image
 * The image must be visible as described to the server.
 * May return a request for further client action. 
 * Returns TRUE is the ping was successful and the server is available.
 * A communitations failure will result in the display being "turned Off"
 * and err set
 * \param display   ObitDisplay object
 * \param image     Image to display (1st plane)
 * \param field  Field number
 * \param nfield Total number of images in mosaic (1 if single)
 * \param request   [out] request details if any
 * \param err       Obit Error message
 * \return TRUE if successful
 */
static gboolean LoadImage (ObitDisplay* display, ObitImage *image, 
			   gint field, gint nfield, ObitInfoList **request, 
			   ObitErr *err)
{
  gboolean out = FALSE;
  ObitXML *xml=NULL, *result=NULL;
  ObitInfoList *status=NULL;
  gint32 dim[MAXINFOELEMDIM];
  ObitInfoType infoType;
  gint retCode;
  gchar *reason=NULL;
  gchar *routine = "ObitObitDisplay:LoadImage";

  /* existing error? */
  if (err->error) return out;

  /* Get image info  */
  xml = getImageInfo (image, field, nfield, err);

  /* Load Image */
  result = ObitRPCCall (display->client, display->ServerURL, xml, &status, request, err);
  /* If something goes wrong with communications, turn off */
  if (err->error) {
    ObitDisplayTurnOff (display);
    ObitErrClearErr (err);  /* ignore error */
    return out;
  }

  /* Check Status */
  retCode = -1;
  if (status) {
    ObitInfoListGet (status, "code", &infoType, dim, &retCode, err);
    ObitInfoListGetP (status, "reason", &infoType, dim, (gpointer*)&reason);
  }
  if (err->error) {
    ObitDisplayTurnOff (display);
    Obit_traceback_val (err, routine, display->name, out);
  }

  /* Did it work? */
  if (retCode!=0) {
    Obit_log_error(err, OBIT_InfoWarn, "%s: Could not talk to Display, code %d",
		   routine, retCode);
    Obit_log_error(err, OBIT_InfoWarn, "   because: %s",reason);
    return out;
  }

  /* Cleanup from load Image */
  status  = ObitInfoListUnref(status);
  xml     = ObitXMLUnref(xml);
  result  = ObitXMLUnref(result);

  /* Must be OK */
  out = TRUE;

  return out;
} /* end LoadImage */

/**
 * Send selected window to display server and allow user editing
 * Field window information is replaces in window with edited version.
 * May return a request for further client action. 
 * Returns TRUE is the ping was successful and the server is available.
 * A communitations failure will result in the display being "turned Off"
 * and err set
 * \param display   ObitDisplay object
 * \param image     Image to display (1st plane)
 * \param field     Field number (1-rel) to edit
 * \param request   [out] request details if any
 * \param err       Obit Error message
 * \return TRUE if successful
 */
static gboolean EditWindow (ObitDisplay* display, ObitDConCleanWindow *window, 
			   gint field, ObitInfoList **request, ObitErr *err)
{
  gboolean out = FALSE;
  ObitXML *xml=NULL, *result=NULL;
  ObitDConCleanWindow *newWindow=NULL;
  ObitInfoList *status=NULL;
  gint32 dim[MAXINFOELEMDIM];
  ObitInfoType infoType;
  gint retCode;
  gchar *reason=NULL;
  gchar *routine = "ObitObitDisplay:LoadImage";

  /* existing error? */
  if (err->error) return out;

  /* Convert window to ObitXML */
  xml = ObitXMLWindow2XML (window, (glong)field, err);

  /* Load Image */
  result = ObitRPCCall (display->client, display->ServerURL, xml, &status, request, err);
  /* If something goes wrong with communications, turn off */
  if (err->error) {
    ObitDisplayTurnOff (display);
    ObitErrClearErr (err);  /* ignore error */
    return out;
  }

  /* Check Status */
  retCode = -1;
  if (status) {
    ObitInfoListGet (status, "code", &infoType, dim, &retCode, err);
    ObitInfoListGetP (status, "reason", &infoType, dim, (gpointer*)&reason);
  }
  if (err->error) {
    ObitDisplayTurnOff (display);
    Obit_traceback_val (err, routine, display->name, out);
  }

  /* Did it work? */
  if (retCode!=0) {
    Obit_log_error(err, OBIT_InfoWarn, "%s: Could not talk to Display, code %d",
		   routine, retCode);
    Obit_log_error(err, OBIT_InfoWarn, "   because: %s",reason);
    return out;
  }

  /* Replace edited version in original window */
  newWindow = ObitXMLXML2Window (result, err);
  ObitDConCleanWindowReplaceField (newWindow, 1, window, field, err);
  if (err->error) Obit_traceback_val (err, routine, display->name, out);

  /* Cleanup from load Image */
  newWindow = ObitDConCleanWindowUnref(newWindow);
  status    = ObitInfoListUnref(status);
  xml       = ObitXMLUnref(xml);
  result    = ObitXMLUnref(result);

  /* Must be OK */
  out = TRUE;

  return out;
} /* end EditWindow  */

/**
 * Send selected window to display server and allow user editing
 * \param image image to be described
 * \param field  Field number
 * \param nfield Total number of images in mosaic (1 if single)
 * \param err    Obit Error message
 * \return ObitXML description of the file to send display server 
 */
static ObitXML* getImageInfo (ObitImage *image, gint field, gint nfield,
			      ObitErr *err)
{
  ObitXML *out = NULL;
  ObitIOType FileType;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM];
  gint i, disk, UserId=0, CNO, ASeq=0;
  gchar *name=NULL, *filenameP, filename[201], *AClass=NULL, *ADir=NULL;
  ObitAIPSDirCatEntry *catEntry=NULL;
  gchar *routine = "ObitDisplay:getImageInfo";

  /* existing error? */
  if (err->error) return out;

  /* Get FileType */
  if (!ObitInfoListGet(image->info, "FileType", &type, (gint32*)&dim, 
		       (gpointer)&FileType, err)) {
    Obit_log_error(err, OBIT_Error, 
		   "%s: entry FileType not in InfoList Object %s",
		   routine, image->name);
    return out;
  }

  /* Other info by type */
  if (FileType==OBIT_IO_FITS) {    /* FITS file */
    if(!ObitInfoListGet(image->info, "Disk", &type, dim, &disk, err))
      Obit_traceback_val (err, routine, image->name, out);
    
    if(!ObitInfoListGetP(image->info, "FileName", &type, dim, 
			 (gpointer*)&filenameP)) 
       Obit_traceback_val (err, routine, image->name, out);
    for (i=0; i<MIN (201, dim[0]); i++) filename[i] = filenameP[i];
    filename[i] = 0;
       
    /* Full name */
    name = ObitFITSFilename (disk, filename, err);
    if (err->error) Obit_traceback_val (err, routine, image->name, out);

  } else if (FileType==OBIT_IO_AIPS) { /* AIPS file */
    if(!ObitInfoListGet(image->info, "Disk", &type, dim, &disk, err)) 
      Obit_traceback_val (err, routine, image->name, out);
    
    if(!ObitInfoListGet(image->info, "User", &type, dim, &UserId, err)) 
      Obit_traceback_val (err, routine, image->name, out);
    
    if(!ObitInfoListGet(image->info, "CNO", &type, dim, &CNO, err))
      Obit_traceback_val (err, routine, image->name, out);

    ADir = ObitAIPSDirname(disk, err);
    if (err->error) Obit_traceback_val (err, routine, image->name, out);

    catEntry = ObitAIPSDirGetEntry(disk, UserId, CNO, err);
    if (err->error) Obit_traceback_val (err, routine, image->name, out);

    name = g_malloc(13*sizeof(gchar));
    for (i=0; i<12; i++) name[i] = catEntry->name[i]; name[i] = 0;
    AClass = g_malloc(7*sizeof(gchar));
    for (i=0; i<6; i++) AClass[i] = catEntry->class[i]; AClass[i] = 0;
    ASeq = catEntry->seq;

  } else if (FileType==OBIT_IO_MEM) {  /* Memory resident only */
    /* Can't cope with this one */
    Obit_log_error(err, OBIT_InfoErr, 
		   "%s: Can't send memory resident images to display",
		   routine);
    return out;
  }

  /* Construct XML */
  out = ObitXMLFileInfo2XML (FileType, name, AClass, ADir, ASeq, UserId, 
			     field, nfield, err);
  if (err->error) Obit_traceback_val (err, routine, image->name, out);

  /* Cleanup */
  if (name)     g_free(name);
  if (AClass)   g_free(AClass);
  if (catEntry) g_free(catEntry);

  return out;
} /* end getImageInfo */
