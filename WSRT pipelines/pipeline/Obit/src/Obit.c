/* $Id: Obit.c,v 1.6 2005/07/07 13:24:41 bcotton Exp $            */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2002-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "Obit.h"
#include "ObitMem.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 *  \file Obit.c
 * Obit class function definitions.
 */

/*--------------- file global data ----------------*/
/** name of the class defined in this file */
static gchar *myClassName = "Obit";

/**
 * ClassInfo structure ObitClassInfo.
 * This structure is used by class objects to access class functions.
 */
ObitClassInfo myClassInfo = {FALSE};

/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
static void  ObitInit  (gpointer in);

/** Private: Deallocate members. */
static void  ObitClear (gpointer in);

/*----------------------Public functions---------------------------*/
/**
 *  Constructor.
 * Initializes class if needed on first call.
 * \param name An optional name for the object.
 * \return the new object.
 */
Obit* newObit (gchar* name)
{
  Obit* out;

  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitClassInit();

  /* allocate/init structure */
  out = g_malloc0(sizeof(Obit));

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

  /* set ClassInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  ObitInit((gpointer)out);

  return out;
} /* end newObit */

/**
 * Returns ClassInfo pointer for the class.
 * This method MUST be included in each derived class to ensure
 * proper linking and class initialization.
 * Initializes class if needed on first call.
 * \return pointer to the class structure.
 */
gconstpointer ObitGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitGetClass */

/**
 * Make a deep copy of input object.
 * Copies are made of complex members such as files; these will be copied
 * applying whatever selection is associated with the input.
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \param err Error stack, returns if not empty.
 * \return pointer to the new (existing) object.
 */
Obit* ObitCopy (Obit *in, Obit *out, ObitErr *err)
{
  gboolean oldExist;

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return NULL;
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));

  /* Create if it doesn't exist */
   oldExist = out!=NULL;
   if (!out) out = newObit(NULL);

  /* deep copy this class members if newly created */
   if (!oldExist) {
   }

  return out;
} /* end ObitCopy */

/**
 * Make a shallow copy of an object.
 * The result will have pointers to the more complex members.
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \return pointer to the new object.
 */
Obit* ObitClone  (Obit *in, Obit *out)
{

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));

  /* Create if it doesn't exist */
  if (!out) out = newObit(NULL);

  return out;
} /* end ObitClone */

/**
 * To reference a pointer, incrementing reference count and returning 
 * the pointer.
 * This function should always be used to copy pointers as this 
 * will ensure a proper reference count.
 * Should also work for derived classes
 * \param in Pointer to object to link, if Null, just return.
 * \return the pointer to in.
 */
gpointer ObitRef (gpointer in)
{
  gpointer out = in;
  if (in==NULL) return in;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* increment reference count */
  ((Obit*)in)->ReferenceCount++;

  return out;
} /* end ObitRef */

/**
 * Always use this function to dismiss an object as it will
 * ensure that the object is only deleted when there are no more 
 * pointers to it.
 * if the input pointer is NULL, the reference count is already <=0 
 * or the object is not a valid Obit Object, it simply returns.
 * \param  in Pointer to object to unreference.
 * \return NULL pointer.
 */
gpointer ObitUnref (gpointer inn)
{
  ObitClassInfo *MyClass;
  Obit *in = (Obit*)inn;

  /* Ignore NULL pointers */
  if (in==NULL) return NULL;

  /* Do nothing if not valid Obit Object */
  if (in->ObitId != OBIT_ID) return NULL;

  /* Do nothing is reference count already <=0 */
  if (in->ReferenceCount<=0)  return NULL;

  /* error check */
  g_assert (ObitIsA(in, &myClassInfo));

  /* decrement ref count */
  in->ReferenceCount--;

  /* Deallocate if it goes negative */
  if (in->ReferenceCount<1) {

    /* Free members */
    /* Who is the guy? */
    MyClass = in->ClassInfo;
    if ((MyClass!=NULL) && (MyClass->ObitClear!=NULL)) 
      MyClass->ObitClear (in);

    /* free structure - may be ObitMem allocation */
    if (ObitMemValid (in)) ObitMemFree (in);
    else g_free (in);
  } /* end deallocate */

  return NULL; /* new value for pointer */
} /* end ObitUnref */

/**
 * Determine if the input object is a member of the class whose
 * ClassInfo is pointed to by class,  or of a derived class.
 * Should also work for derived classes.
 * \param in Pointer to object to test.
 * \param class Pointer to ClassInfo structure of the class to be
 *              tested.
 * \return TRUE if member of test or a derived class, else FALSE.
 */
gboolean ObitIsA (gpointer in, gconstpointer class)
{
  const ObitClassInfo *inClass;

  /* if either input is null then it's not a match */
  if (in==NULL) return FALSE;
  if (class==NULL) return FALSE;

  /* Check ObitString */
  if (((Obit*)in)->ObitId != OBIT_ID) return FALSE;

  /* Loop back through the inheritance comparing ClassInfo 
     pointers */
  inClass = ((Obit*)in)->ClassInfo;
  while (inClass!=NULL) {
    if (inClass==class) return TRUE;  /* Found it */
    inClass = inClass->ParentClass;
  }
  return FALSE; /* if it got here it must not match */
} /* end ObitIsA  */

/**
 * Returns Obit magic blanking float value
 * This is adopted from AIPS and correcponds to the string 'INDE'
 * \return float magic value
 */
gfloat ObitMagicF (void)
{
  static union FBLANKequiv {
    gchar string[4];
    gfloat fblank;
  } FBLANK;
  FBLANK.string[0] = 'I'; 
  FBLANK.string[1] = 'N'; 
  FBLANK.string[2] = 'D'; 
  FBLANK.string[3] = 'E'; 
  
  return FBLANK.fblank;
} /* end ObitMagicF */

/**
 * Initialize global ClassInfo Structure.
 */
void ObitClassInit (void)
{
  ObitClassInfo *ParentClass;

  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;

  /* Initialize (recursively) parent class first */
  ParentClass = NULL; /* ObitxxxGetClass() for derived classes */

  /* function pointers etc. */
  myClassInfo.hasScratch    = FALSE; /* No scratch files */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = (ObitClassInitFP)ObitClassInit;
  myClassInfo.newObit       = (newObitFP)newObit;
  myClassInfo.ObitCopy      = (ObitCopyFP)ObitCopy;
  myClassInfo.ObitClone     = (ObitCloneFP)ObitClone;
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitInit;
} /* end ObitClassInit */
/*---------------Private functions--------------------------*/

/**
 * Creates empty member objects, initialize reference count.
 * Recursively initialized base classes before this class.
 * \param in Pointer to the object to initialize.
 */
static void ObitInit  (gpointer inn)
{
  Obit *in = inn;

  /* error checks */
  g_assert (in != NULL);
  
  /* set members in this class */
  in->ObitId = OBIT_ID;
  in->ReferenceCount = 1;

} /* end ObitInit */

/**
 * Deallocates member objects.
 * \param  in Pointer to the object to deallocate.
 */
static void ObitClear (gpointer inn)
{
  Obit *in = inn;

  /* error check */
  g_assert (ObitIsA(in, &myClassInfo));

  /* delete this class members */
  if (in->name){ g_free (in->name); in->name = NULL;}

} /* end ObitClear */


