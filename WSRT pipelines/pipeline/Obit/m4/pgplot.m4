AC_DEFUN([AM_PATH_PGPLOT], [
         AC_REQUIRE([AC_F77_LIBRARY_LDFLAGS])dnl
         AC_REQUIRE([AC_PATH_XTRA])dnl

         AC_ARG_WITH(pgplot,
                     AC_HELP_STRING([--with-pgplot=DIR],
                                    [search for PGPLOT in DIR]),
                     [for dir in `echo "$withval" | tr : ' '`; do
    if test -d $dir; then
      PGPLOT_CPPFLAGS="$PGPLOT_CPPFLAGS -I$dir"
      PGPLOT_LDFLAGS="$PGPLOT_LDFLAGS -L$dir"
    fi
  done[]])
ac_pgplot_saved_CPPFLAGS="$CPPFLAGS"
ac_pgplot_saved_CFLAGS="$CFLAGS"
ac_pgplot_saved_LDFLAGS="$LDFLAGS"
ac_pgplot_saved_LIBS="$LIBS"
PGPLOT_CFLAGS="$PGPLOT_CFLAGS $X_CFLAGS"
PGPLOT_LIBS="$PGPLOT_LIBS $X_LIBS $X_PRE_LIBS -lX11 $X_EXTRA_LIBS"
CPPFLAGS="$CPPFLAGS $PGPLOT_CPPFLAGS"
CFLAGS="$CFLAGS $PGPLOT_CFLAGS"
LDFLAGS="$LDFLAGS $PGPLOT_LDFLAGS"
LIBS="$PGPLOT_LIBS $FLIBS"
ac_have_pgplot=yes
	 AC_CHECK_LIB(pgplot, main, [], [ac_have_pgplot=no
                      AC_MSG_WARN([cannot find PGPLOT library])])
	 AC_CHECK_LIB(cpgplot, cpgenv, [], [ac_have_pgplot=no
                      AC_MSG_WARN([cannot find PGPLOT C library])])
	 AC_CHECK_HEADER(cpgplot.h, [], [ac_have_pgplot=no,
                      AC_MSG_WARN([cannot find PGPLOT headers])])
if test $ac_have_pgplot = yes; then
	 AC_DEFINE(HAVE_PGPLOT, 1, [Define to 1 if you have PGPLOT.])
fi
PGPLOT_LIBS="-lcpgplot -lpgplot $PGPLOT_LIBS"
CPPFLAGS="$ac_pgplot_saved_CPPFLAGS"
CFLAGS="$ac_pgplot_saved_CFLAGS"
LDFLAGS="$ac_pgplot_saved_LDFLAGS"
LIBS="$ac_pgplot_saved_LIBS"
	 AC_SUBST(PGPLOT_CPPFLAGS)
	 AC_SUBST(PGPLOT_CFLAGS)
	 AC_SUBST(PGPLOT_LDFLAGS)
	 AC_SUBST(PGPLOT_LIBS)
])
