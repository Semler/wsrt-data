AC_DEFUN([AC_PATH_FFTW], [
	AC_ARG_WITH(fftw,
                    AC_HELP_STRING([--with-fftw=DIR],
                                 [search for FFTW in DIR/include and DIR/lib]),
                    [for dir in `echo "$withval" | tr : ' '`; do
    if test -d $dir/include; then
      FFTW_CPPFLAGS="$FFTW_CPPFLAGS -I$dir/include"
    fi
    if test -d $dir/lib; then
      FFTW_LDFLAGS="$FFTW_LDFLAGS -L$dir/lib"
    fi
  done[]])

        AC_ARG_WITH(fftw-includes,
                    AC_HELP_STRING([--with-fftw-includes=DIR],
	                           [search for FFTW includes in DIR]),
	            [for dir in `echo "$withval" | tr : ' '`; do
    if test -d $dir; then
      FFTW_CPPFLAGS="$FFTW_CPPFLAGS -I$dir"
    fi
  done[]])

ac_fftw_saved_CPPFLAGS="$CPPFLAGS"
ac_fftw_saved_LDFLAGS="$LDFLAGS"
ac_fftw_saved_LIBS="$LIBS"
CPPFLAGS="$CPPFLAGS $FFTW_CPPFLAGS"
LDFLAGS="$LDFLAGS $FFTW_LDFLAGS"
ac_have_fftw=yes
        AC_SEARCH_LIBS(fftw, [fftw sfftw], [], [ac_have_fftw=no
                       AC_MSG_WARN([cannot find FFTW library])])
        AC_SEARCH_LIBS(rfftw, [rfftw srfftw], [], [ac_have_fftw=no
                       AC_MSG_WARN([cannot find FFTW library])])
        AC_CHECK_HEADERS([fftw.h rfftw.h], [], [ac_have_fftw=no
                         AC_MSG_WARN([cannot find FFTW headers])])
if test $ac_have_fftw = yes; then
	AC_DEFINE(HAVE_FFTW, 1, [Define to 1 if FFTW is available.])
fi
FFTW_LIBS="$LIBS"
CPPFLAGS="$ac_fftw_saved_CPPFLAGS"
LDFLAGS="$ac_fftw_saved_LDFLAGS"
LIBS="$ac_fftw_saved_LIBS"
	AC_SUBST(FFTW_CPPFLAGS)
	AC_SUBST(FFTW_LDFLAGS)
	AC_SUBST(FFTW_LIBS)
])
