AC_DEFUN([AC_PATH_GSL], [
	AC_ARG_WITH(gsl,
                    AC_HELP_STRING([--with-gsl=DIR],
                                 [search for GSL in DIR/include and DIR/lib]),
                    [for dir in `echo "$withval" | tr : ' '`; do
    if test -d $dir/include; then
      GSL_CFLAGS="$GSL_CFLAGS -I$dir/include"
    fi
    if test -d $dir/lib; then
      GSL_LDFLAGS="$GSL_LDFLAGS -L$dir/lib"
    fi
  done[]])

        AC_ARG_WITH(gsl-includes,
                    AC_HELP_STRING([--with-gsl-includes=DIR],
	                           [search for GSL includes in DIR]),
	            [for dir in `echo "$withval" | tr : ' '`; do
    if test -d $dir; then
      GSL_CFLAGS="$GSL_CFLAGS -I$dir"
    fi
  done[]])

ac_gsl_saved_CFLAGS="$CFLAGS"
ac_gsl_saved_LDFLAGS="$LDFLAGS"
ac_gsl_saved_LIBS="$LIBS"
CFLAGS="$CFLAGS $GSL_CFLAGS"
LDFLAGS="$LDFLAGS $GSL_LDFLAGS"
ac_have_gsl=yes
        AC_SEARCH_LIBS(cblas_isamax, [gslcblas], [], [ac_have_gsl=no
                       AC_MSG_WARN([cannot find GSL library])])
        AC_SEARCH_LIBS(gsl_fit_linear, [gsl], [], [ac_have_gsl=no
                       AC_MSG_WARN([cannot find GSL library ])], [-lgslcblas])
        AC_CHECK_HEADERS([gsl/gsl_blas.h], [], [ac_have_gsl=no
                         AC_MSG_WARN([cannot find GSL headers])])
if test $ac_have_gsl = yes; then
	AC_DEFINE(HAVE_GSL, 1, [Define to 1 if GSL is available.])
fi
GSL_LIBS="$LIBS"
CFLAGS="$ac_gsl_saved_CFLAGS"
LDFLAGS="$ac_gsl_saved_LDFLAGS"
LIBS="$ac_gsl_saved_LIBS"
	AC_SUBST(GSL_CFLAGS)
	AC_SUBST(GSL_LDFLAGS)
	AC_SUBST(GSL_LIBS)
])
