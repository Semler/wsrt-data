/* $Id: ObitPosition.h,v 1.1.1.1 2004/07/19 16:42:43 bcotton Exp $                            */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 1996,1997,2004                                     */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
#ifndef OBITPOSITION_H 
#define OBITPOSITION_H 

#include <math.h>
#include <string.h>
#include <stdio.h>
#include "ObitImageDesc.h"

/*-------- Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitPosition.h
 * ObitPosition class definition.
 *
 * Utility routines related to celestial position
 * There are no objects of this class.
 */

/*---------------Public functions---------------------------*/
/** Public: Routine to determine accurate position for pixel coordinates */
gint ObitPositionWorldPos(gfloat pixel[2], ObitImageDesc *desc,
			  gdouble coord[2]);

/** Public: Routine to determine accurate pixel coordinates for an RA and Dec
 * offsets from the reference position. */
gint ObitPositionXYpix(gdouble coord[2], ObitImageDesc *desc,
		       gfloat pixel[2]);

/** Public: Routine to determine accurate position for pixel coordinates from 
 * offsets from the reference position. */
gint ObitPositionWorldPosLM(gdouble doff[2], ObitImageDesc *desc,
			    gdouble coord[2]);

/** Public: Routine to determine accurate coordinate offsets for an RA and Dec
 * offsets from the reference position.*/
gint ObitPositionXYpixLM(gdouble coord[2], ObitImageDesc *desc,
			 gdouble doff[2]);

#endif /* OBITPOSITION_H  */
