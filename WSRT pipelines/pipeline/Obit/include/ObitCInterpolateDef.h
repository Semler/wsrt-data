/* $Id: ObitCInterpolateDef.h,v 1.2 2004/12/10 22:34:19 bcotton Exp $   */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2004                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
/*  Define the basic components of the ObitCInterpolate structure     */
/*  This is intended to be included in a class structure definition   */
/**
 * \file ObitCInterpolateDef.h
 * ObitInterpolate structure members for derived classes.
 */
#include "ObitDef.h"  /* Parent class definitions */
/** Threading info member object  */
ObitThread *thread;
/** Linked list of arrays of data.  */
ObitInfoList *info;
/** Image descriptor to give relation between pixels and coordinates */
ObitImageDesc *myDesc;
/** Array to be interpolated */
ObitCArray *myArray;
/** Pointer to data in myArray */
gfloat *array;
/** Dimension of plane in myArray */
glong nx, ny;
/** Number of conjugate (neg V) Columns in myArray  */
glong numConjCol;
/** Table of interpolation kernals, every kernalSpace of a pixel */
ObitFArray *myKernal;
/** Spacing of kernal tabulation */
gfloat kernalSpace;
/** Number of tabulations in myKernal per cell = 1/kernalSpace */
glong numKTab;
/** Half width of interpolation kernal */
glong hwidth;
/** Target "X" Pixel */
gfloat xPixel;
/** Target "Y" Pixel */
gfloat yPixel;
/** Convolving "X" start pixel number in convolution */
glong xStart;
/** Convolving "Y" start pixel number in convolution */
glong yStart;
/** Number of  "X" terms in convolution */
glong xNterm;
/** Number of  "Y" terms in convolution  */
glong yNterm;
/** "X" Convolving kernal (pointer in kernalTable) */
gfloat *xKernal;
/** "Y" Convolving kernal (pointer in kernalTable)*/
gfloat *yKernal;
/** Reciprocals of Lagrangian denominators */
gfloat denom[40];
