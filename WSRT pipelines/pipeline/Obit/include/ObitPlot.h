/* $Id: ObitPlot.h,v 1.6 2005/08/12 10:42:54 bcotton Exp $                            */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
#ifndef OBITPLOT_H 
#define OBITPLOT_H 
#include <glib.h>
/* Disable pgplot if not needed */
#ifdef HAVE_PGPLOT
#include <cpgplot.h>
#endif /* HAVE_ PGPLOT*/
#include "Obit.h"
#include "ObitErr.h"
#include "ObitThread.h"
#include "ObitInfoList.h"

/*-------- Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitPlot.h
 * ObitPlot Obit graphics class definition.
 * This class is derived from the Obit class.
 * This contains information about the observations and the size and 
 * structure of the data.
 *
 * \section ObitPlotUsage Usage
 * Instances can be obtained using the #newObitPlot constructor
 * the #ObitPlotCopy copy constructor or a pointer duplicated using 
 * the #ObitPlotRef function.
 * When an instance is no longer needed, use the #ObitPlotUnref macro
 * to release it.
 */

/*----------------- Macroes ---------------------------*/
/** 
 * Macro to unreference (and possibly destroy) an ObitPlot
 * returns a ObitPlot* (NULL).
 * \li in = object to unreference.
 */
#define ObitPlotUnref(in) ObitUnref (in)

/** 
 * Macro to reference (update reference count) an ObitPlot.
 * returns a ObitPlot*.
 * in = object to reference
 */
#define ObitPlotRef(in) ObitRef (in)

/** 
 * Macro to determine if an object is the member of this or a 
 * derived class.
 * Returns TRUE if a member, else FALSE
 * in = object to reference
 */
#define ObitPlotIsA(in) ObitIsA (in, ObitPlotGetClass())

/*--------------Class definitions-------------------------------------*/
/**
 * ObitPlot Class structure.
 *
 * This class contains descriptions of interferometric visibility data.
 */  
typedef struct {
#include "ObitPlotDef.h"  /* Actual definitions */
} ObitPlot;

/*---------------Public functions---------------------------*/
/** Public: Class initializer. */
void ObitPlotClassInit (void);

/** Public: Constructor. */
ObitPlot* newObitPlot (gchar *name);

/** Public: Initialize plot. */
void ObitPlotInitPlot (ObitPlot* in, char *output, ObitErr *err);

/** Public: Copy Plot */
ObitPlot* ObitPlotCopy (ObitPlot* in, ObitPlot* out,
			    ObitErr *err);

/** Public: Return class pointer. */
gconstpointer ObitPlotGetClass (void);

/** Public: Simple X-Y plot. */
void ObitPlotXYPlot (ObitPlot* in, gint symbol, 
		     gint n, gfloat *x, gfloat *y, ObitErr *err);

/** Public:  set window and viewport and draw labeled frame */
void  ObitPlotCpgenv (ObitPlot* in, 
		      gfloat xmin, gfloat xmax, gfloat ymin, gfloat ymax, 
		      gint just, gint axis, ObitErr *err);

/** Public: write labels for x-axis, y-axis, and top of plot*/
void  ObitPlotCpglab (ObitPlot* in, gchar *xlabel, gchar *ylabel, gchar *title,
		      ObitErr *err);

/** Public: draw labeled frame around viewport */
void  ObitPlotCpgbox (ObitPlot* in, 
		      gchar *xopt, gfloat xtick, gint nxsub, 
		      gchar *yopt,  gfloat ytick, gint nysub, 
		      ObitErr *err);

/** Public: Scaling for characters */
void  ObitPlotCpgsch (ObitPlot* in, gfloat cscale, ObitErr *err);

/** Public: Set line width */
void  ObitPlotCpgslw (ObitPlot* in, gint lwidth, ObitErr *err);

/** Public: Write text */
void  ObitPlotCpgmtxt (ObitPlot* in, gchar *side, gfloat disp, 
		       gfloat coord, gfloat fjust, gchar *text,
		       ObitErr *err);

/**  Public: Front end to pgplot routine cpgmove.*/
void  ObitPlotCpgmove (ObitPlot* in, gfloat x, gfloat y, ObitErr *err);

/**  Public: Front end to pgplot routine cpgdraw. */
void  ObitPlotCpgdraw (ObitPlot* in, gfloat x, gfloat y, ObitErr *err);

/*-------------------Class Info--------------------------*/
/**
 * ClassInfo Structure.
 * Contains class name, a pointer to any parent class
 * (NULL if none) and function pointers.
 */
typedef struct  {
#include "ObitPlotClassDef.h" /* Actual definition */
} ObitPlotClassInfo; 


#endif /* OBITPLOT_H */ 

