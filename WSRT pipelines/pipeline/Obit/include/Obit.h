/* $Id: Obit.h,v 1.4 2005/10/06 20:22:54 bcotton Exp $            */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2002-2004                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
#ifndef OBIT_H 
#define OBIT_H 
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include "ObitTypes.h"
#include "ObitErr.h"

/*-------- Obit: Software for the recently deceased ------------------*/
/**
 * \file Obit.h
 * Obit base class definition.
 *
 * This is the base class for Obit data objects.
 * The structure is also defined in ObitDef.h to allow recursive definition 
 * in derived classes. 
 *
 * \section ObitUsage Usage
 * No instances should be created of this class. 
 */

/*-------------- enumerations -------------------------------------*/
/*--------------Class definitions-------------------------------------*/
/** 
 * Obit object recognition string.  
 * This is to be the first gint32 of each object.
 * The value in ascii is "Obit"
 */
#define OBIT_ID 0x4f626974

/** Obit Class structure. */
typedef struct {
#include "ObitDef.h"    /* actual CLASS definition */
} Obit;

/*---------------Public functions---------------------------*/
/** Public: Constructor. */
Obit* newObit (gchar* name);
/* define type for ClassInfo structure */
typedef gpointer (*newObitFP)(gchar* name);

/** Public: ClassInfo pointer */
gconstpointer ObitGetClass (void);

/** Public: Class initializer. */
void ObitClassInit (void);

/** Public: Copy (deep) constructor. */
Obit* ObitCopy  (Obit *in, Obit *out, ObitErr *err);
typedef gpointer (*ObitCopyFP)   (gpointer in, gpointer out, 
				  ObitErr *err);

/** Public: Copy (shallow) constructor. */
Obit* ObitClone (Obit *iObn, Obit *out);
typedef gpointer (*ObitCloneFP)  (gpointer in, gpointer out);

/** Public: Ref pointer, increment reference count, return pointer. */
gpointer ObitRef (gpointer in);
typedef  gpointer (*ObitRefFP)   (gpointer in);

/** Public: Unref pointer, decrement reference count and destroy if 0. */
gpointer ObitUnref (gpointer in);
typedef gpointer (*ObitUnrefFP) (gpointer *in);

/** Public: returns TRUE is object is a member of myClassInfo or a 
    derived class. */
gboolean ObitIsA (gpointer in, gconstpointer type);
typedef gboolean (*ObitIsAFP) (gpointer in, gconstpointer class);

/** Public: returns magic value blanking value */
gfloat ObitMagicF (void);

/** Function pointers for private functions */
typedef void (*ObitClassInitFP) (void);
typedef void (*ObitInitFP)      (gpointer in);
typedef void (*ObitClearFP)     (gpointer in);

/*----------------------- Class Info ---------------------------------*/
/**
 * ClassInfo Structure.
 * Contains class name, a pointer to any base class
 * (NULL if none) and function pointers.
 */
typedef struct  {
#include "ObitClassDef.h" /* actual definitions */
} ObitClassInfo; 


#endif /* OBIT_H */ 
