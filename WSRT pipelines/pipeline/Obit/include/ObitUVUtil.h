/* $Id: ObitUVUtil.h,v 1.7 2005/09/14 14:27:29 bcotton Exp $   */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2004-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
#ifndef OBITUVUTIL_H 
#define OBITUVUTIL_H 

#include "ObitErr.h"
#include "ObitUV.h"

/*-------- Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitImage.h
 * ObitUVUtil class definition.
 *
 * This utility class contains utility functions for uv data.
 * There are no objects of this class.
 */

/*---------------Public functions---------------------------*/
/** Public: Get UVW Extrema */
void ObitUVUtilUVWExtrema (ObitUV *inUV, gfloat *MaxBL, gfloat *MaxW,
			   ObitErr *err);

/** Public: Make zeroed copy of an ObitUV */
ObitUV* ObitUVUtilCopyZero (ObitUV *inUV, gboolean scratch, ObitUV *outUV,
			    ObitErr *err);

/** Public: Divide the visibilities in one ObitUVData by another */
void ObitUVUtilVisDivide (ObitUV *inUV1, ObitUV *inUV2, ObitUV *outUV, 
			  ObitErr *err);

/** Public: Compare the visibilities in one ObitUVData with another */
gfloat ObitUVUtilVisCompare (ObitUV *inUV1, ObitUV *inUV2, ObitErr *err);

#endif /* OBITIUVUTIL_H */ 
