/* $Id: ObitUVDescDef.h,v 1.2 2005/06/17 12:25:04 bcotton Exp $  */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003,2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
/*  Define the basic components of the ObitUVDesc structure           */
/*  This is intended to be included in a class structure definition   */
/**
 * \file ObitUVDescDef.h
 * ObitUVDesc structure members for derived classes.
 */
#include "ObitDef.h"  /* Parent class definitions */
/** Type of access to file */
  ObitIOAccess access;
  /** Number of visibilities total */
  glong nvis;
  /** Length (floats) of a vis record. */
  glong lrec;
  /** Number of random parameters.*/
  glong  nrparm;
  /** current beginning visibility (1-rel) read/write in buffer  */
  glong firstVis;
  /** number of visibilities in buffer */
  glong numVisBuff;
  /** Number of axes. */
  glong   naxis;
  /** Number of subarrays in the data */
  glong numSubA;
  /** Maximum antenna in the data */
  glong maxAnt;
  /** Dimensions of axes. */
  glong   inaxes[UV_MAXDIM];
  /** WCS labels for each dimension of array. */
  gchar   ctype[UV_MAXDIM][UVLEN_KEYWORD];
  /** WCS labels for random axes. */
  gchar   ptype[UV_MAX_RANP][UVLEN_KEYWORD];
  /** Axis coordinate increments. */
  gfloat  cdelt[UV_MAXDIM]; 
  /** Axis reference pixels (1-rel) */
  gfloat  crpix[UV_MAXDIM];
  /** Axis rotation angles (deg) */
  gfloat  crota[UV_MAXDIM];
  /** Alternate reference Pixel (frequency or velocity) */
  gfloat altCrpix;
  /** Offset in X (rotated RA) of phase center  (deg) */
  gfloat xshift;
  /** Offset in Y (rotated Dec) of phase center (deg)  */
  gfloat yshift;
  /** Data integration time in Days */
  gfloat DeltaTime;
  /** Name of object. */
  gchar   object[UVLEN_VALUE]; 
  /** Name of telescope making observation. */
  gchar   teles[UVLEN_VALUE]; 
  /** Name of instrument making observation. */
  gchar   instrument[IMLEN_VALUE]; 
  /** Observer */
  gchar   observer[IMLEN_VALUE];
  /** Observing date as yyyy-mm-dd */
  gchar   obsdat[UVLEN_VALUE];
  /** File creation date as yyyy-mm-dd */
  gchar   date[UVLEN_VALUE];
  /** Origin (software) of image. */
  gchar   origin[UVLEN_VALUE];
  /** Units of visibilities */
  gchar   bunit[UVLEN_VALUE];
  /** Epoch (years) of celestial coordinates,
   *  This is sometimes confused with equinox. */
  gfloat  epoch;
  /** Mean Equinox of celestial coordinates (e.g. 2000) */
  gfloat  equinox;
  /** Julian date of observations */
  gdouble JDObs;
  /** Axis coordinate values at reference pixel. */
  gdouble crval[UV_MAXDIM];
  /** Observed RA (deg) */
  gdouble obsra; 
  /** Observed Dec (deg) */
  gdouble obsdec;
  /** Alternate reference value (frequency or velocity) */
  gdouble altRef;
  /** Rest frequency of line (Hz)  */
  gdouble restFreq;
  /** Reference Frequency (Hz) for u,v,w */
  gdouble freq; 
  /** Array of Frequencies (Hz) in the order of Freq, IF in data 
   *  dimension is nfreq*nif */
  gdouble *freqArr;
  /** Array of differential frequency scaling factors from one frequency to the
   *  next, dimension is nfreq*nif */
  gfloat *fscale;
  /** Array of Frequencies per IF */
  gdouble *freqIF;
  /** Array of channel frequency increments per IF */
  gfloat *chIncIF;
  /** Array of sidebands, one per IF */
  gint *sideband;
  /** Number of antennas (actually max ant number) per subarray */
  oint *numAnt;
  /** Velocity reference frame: 1-3 (LSR, Helio, Observer) */
  gint VelReference;
  /** Velocity definition 0=>optical, 1=radio */
  gint VelDef;
  /** Random parameter offset: U coordinate */
  gint ilocu;
  /** Random parameter offset: V coordinate */
  gint ilocv;
  /** Random parameter offset: W coordinate */
  gint ilocw;
  /** Random parameter offset:Time */
  gint iloct;
  /** Random parameter offset: Baseline */
  gint ilocb;
  /** Random parameter offset: Source id. */
  gint ilocsu;
  /** Random parameter offset:Freq id.  */
  gint ilocfq;
  /** Random parameter offset: Integration time */
  gint ilocit;
  /** Random parameter offset: Correlation id. */
  gint ilocid;
  /** 0-rel axis order: Weight-scale parameters for compressed data */
  gint ilocws;
  /** 0-rel axis order: Complex values */
  gint jlocc;
  /** 0-rel axis order: Stokes' parameters */
  gint jlocs;
  /** 0-rel axis order: Frequency */
  gint jlocf;
  /** 0-rel axis order: RA */
  gint jlocr;
  /**0-rel axis order: declination */
  gint jlocd;
  /** 0-rel axis order: IF */
  gint jlocif;
  /** Increment in data: Stokes */
  gint incs;
  /** Increment in data: Frequency */
  gint incf;
  /** Increment in data: IF */
  gint incif;
  /** Total number of complex visibilities */
  gint ncorr;
  /** Sort order 1st 2 char meaningful. */
  gchar isort[3];
  /** InfoList for other keywords */
  ObitInfoList *info;
