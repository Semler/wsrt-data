/* $Id: ObitDConCleanPxListDef.h,v 1.4 2005/04/19 11:46:30 bcotton Exp $ */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2004                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
/*  Define the basic components of the ObitDConCleanPxList structure  */
/*  This is intended to be included in a class structure definition   */
/**
 * \file ObitDConCleanBmHistDef.h
 * ObitDConCleanPxList structure members for this and any derived classes.
 */
#include "ObitDef.h"  /* Parent class (Obit) definitions */
/** Threading info member object  */
ObitThread *thread;
/** Linked list of arrays of data.  */
ObitInfoList *info;
/** Image mosaic being deconvolved */
ObitImageMosaic *mosaic;
/** Image Clean window definition */
ObitDConCleanWindow *window;
/** Image Beam patch data,
    The beam patch array is square and 2*patch +1 x 2*patch +1
    in size.  The center pixel is the center of the dirty beam.*/
ObitFArray *BeamPatch;
/** Minimum flux density to load */
gfloat minFluxLoad;
/** Maximum abs. residual at end of CLEAN */
gfloat maxResid;
/** min. minor cycle flux for auto Window feature */
gfloat autoWinFlux ;
/** Total flux density CLEANed */
gfloat totalFlux;
/** Plane being processed, 1-rel indices of axes 3-? */
glong plane[IM_MAXDIM-2];
/** Maximum number of iterations */
glong niter;
/** current number of iterations */
glong currentIter;
/** current number of components per field */
glong *iterField;
/** CC table version per field */
glong *CCver;
/** current summed flux density per field */
gfloat *fluxField;
/** Gaussian size (circular) per field, (deg.) */
gfloat *circGaus;
/** CLEAN gain per field */
gfloat *gain;
/** Minimum flux density per field */
gfloat *minFlux;
/** Depth factor per field */
gfloat *factor;
/** Array of CCTables for writing */
ObitTableCC **CCTable;
/** maximum number of pixels */
glong maxPixel;
/** number of pixels */
glong nPixel;
/** pixel x coordinate (0-rel) */
glong *pixelX;
/** pixel y coordinate (0-rel) */
glong *pixelY;
/** pixel field */
gshort *pixelFld;
/** pixel flux density */
gfloat *pixelFlux;

