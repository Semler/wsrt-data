/* $Id: ObitImageDescDef.h,v 1.2 2005/06/17 12:25:04 bcotton Exp $ */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003,2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
/*  Define the basic components of the ObitImage structure            */
/*  This is intended to be included in a class structure definition   */
/**
 * \file ObitImageDef.h
 * ObitImage structure members for derived classes.
 */
#include "ObitDef.h"  /* Parent class definitions */
  /** Number of bits per pixel on disk. negative implies IEEE floating.*/
  gint    bitpix;
  /** Number of axes. */
  gint    naxis;
  /** Dimensions of axes. */
  glong   inaxes[IM_MAXDIM];
  /** WCS labels for each dimension of array. */
  gchar   ctype[IM_MAXDIM][IMLEN_KEYWORD];
  /** Axis coordinate increments. */
  gfloat  cdelt[IM_MAXDIM]; 
  /** Axis reference pixels (1-rel) */
  gfloat  crpix[IM_MAXDIM];
  /** Axis rotation angles (deg) */
  gfloat  crota[IM_MAXDIM];
  /** Alternate reference Pixel (frequency or velocity) */
  gfloat altCrpix;
  /** Offset in X (rotated RA) of phase center (deg)  */
  gfloat xshift;
  /** Offset in Y (rotated Dec) of phase center (deg)  */
  gfloat yshift;
  /** Convolving beam major axis in degrees  */
  gfloat beamMaj;
  /** Convolving beam minor axis in degrees   */
  gfloat beamMin;
  /**  Convolving beam position angle in degrees */
  gfloat beamPA;
  /** Name of object. */
  gchar   object[IMLEN_VALUE]; 
  /** Name of telescope making observation. */
  gchar   teles[IMLEN_VALUE]; 
  /** Name of instrument making observation. */
  gchar   instrument[IMLEN_VALUE]; 
  /** Observer */
  gchar   observer[IMLEN_VALUE];
  /** Observing date as yyyy-mm-dd */
  gchar   obsdat[IMLEN_VALUE];
  /** Image creation date as yyyy-mm-dd */
  gchar   date[IMLEN_VALUE];
  /** Origin (software) of image. */
  gchar   origin[IMLEN_VALUE];
  /** Units of image */
  gchar   bunit[IMLEN_VALUE];
  /** Epoch (years) of celestial coordinates,
   *  This is sometimes confused with equinox.
   */
  gfloat  epoch;
  /** Mean Equinox of celestial coordinates (e.g. 2000) */
  gfloat  equinox;
  /** Observed RA (deg) */
  gdouble obsra; 
  /** Observed Dec (deg) */
  gdouble obsdec;
  /** Axis coordinate values at reference pixel. */
  gdouble crval[IM_MAXDIM];
  /** Alternate reference value (frequency or velocity) */
  gdouble altRef;
  /** Rest frequency of line (Hz)  */
  gdouble restFreq;
  /** maximum value in image */
  gfloat  maxval;
  /** minimum value in image */
  gfloat  minval;
  /** Are there blanked pixels in the image? */
  gboolean  areBlanks;
  /** number of iterations of deconvolution */
  glong niter;
  /** Velocity reference frame: 1-3 (LSR, Helio, Observer) */
  gint VelReference;
  /** Velocity definition 0=>optical, 1=radio */
  gint VelDef;
  /** Coordinate type */
  ObitCoordType coordType;
  /** current row  (1-rel) read/write in buffer  */
  glong row;
  /** current plane (1-rel) read/write in buffer */
  glong plane;
  /** Access size code  */
  ObitIOSize IOsize;
  /** 0-rel axis order: RA (or longitude) , -1 means not present. */
  gint jlocr;
  /** 0-rel axis order: declination (or latitude), -1 means not present. */
  gint jlocd;
  /** 0-rel axis order: Stokes' parameters, -1 means not present. */
  gint jlocs;
  /** 0-rel axis order: Frequency. -1 means not present. */
  gint jlocf;
  /** InfoList for other keywords */
  ObitInfoList *info;
