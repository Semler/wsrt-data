/* $Id: ObitErr.inc,v 1.3 2005/04/19 19:21:20 bcotton Exp $ */  
/*--------------------------------------------------------------------*/
/* Swig module description for ObitErr type                           */
/*                                                                    */
/*;  Copyright (C) 2004                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

%{
#include "ObitErr.h"
%}

extern ObitErr* ObitErrUnref (ObitErr* in);
extern ObitErr*  ObitErrRef (ObitErr* in);
extern void ObitErrLog (ObitErr* in);
extern void ObitErrClear (ObitErr* in);
extern int ObitErrIsA (ObitErr* in);

%inline %{

extern ObitErr* ObitErrCreate (void) {
  return newObitErr();
}

extern int isError(ObitErr *in) {
   return (int)in->error;
}

extern char *OErrMsg(ObitErr *in)
{
  ObitErrCode errLevel;
  gchar *errMsg, *errLevelStr;

/*
 * Human readable versions of the ObitErr codes.
 * Should be coordinated with enum definition.
 */
gchar *ObitErrorLevelString[] = {
  "no message   ", /* OBIT_None        */
  "information  ", /* OBIT_InfoErr     */
  "warning      ", /* OBIT_InfoWarn    */
  "traceback    ", /* OBIT_Traceback   */
  "Mild error   ", /* OBIT_MildError   */
  "Error        ", /* OBIT_Error       */
  "Serious error", /* OBIT_StrongError */
  "Fatal error  ", /* OBIT_Fatal       */
};

  /* error checks */
  g_assert (ObitErrIsA(in));

  ObitErrPop (in, &errLevel, &errMsg);
  if(errMsg) {
    gchar *str;
    /* convert error level to something human readable */
    errLevelStr = ObitErrorLevelString[errLevel];
    str = g_strdup_printf("%s: %s", errLevelStr, errMsg);
    g_free(errMsg);
    errMsg = str;
  }
  return errMsg;
}

/* Force an abort as a really heavyhanded interrupt */
extern void Bomb(void) {
  char ct, *PythonSux = NULL;
  ct = PythonSux[-1000000000];
}

%}

/* Definitions for Python Shadow class */
/* A copy of the struct for c */
%{
typedef struct {
  ObitErr *me;
} OErr;
%}
/* and a copy of the struct for swig */
typedef struct {
  ObitErr *me;
} OErr;

%addmethods OErr { 
  OErr(void) {
     OErr *out;
     out = (OErr *) malloc(sizeof(OErr));
     out->me = ObitErrCreate();
    return out;
   }
  ~OErr() {
    self->me = ObitErrUnref(self->me);
    free(self);
  }
};

