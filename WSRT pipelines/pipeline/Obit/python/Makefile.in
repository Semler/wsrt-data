# $Id: Makefile.in,v 1.5 2005/08/03 12:11:42 bcotton Exp $
#-----------------------------------------------------------------------
#;  Copyright (C) 2004-2005
#;  Associated Universities, Inc. Washington DC, USA.
#;
#;  This program is free software; you can redistribute it and/or
#;  modify it under the terms of the GNU General Public License as
#;  published by the Free Software Foundation; either version 2 of
#;  the License, or (at your option) any later version.
#;
#;  This program is distributed in the hope that it will be useful,
#;  but WITHOUT ANY WARRANTY; without even the implied warranty of
#;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#;  GNU General Public License for more details.
#;
#
#;  You should have received a copy of the GNU General Public
#;  License along with this program; if not, write to the Free
#;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,
#;  MA 02139, USA.
#;
#;  Correspondence concerning this software should be addressed as follows:
#;         Internet email: bcotton@nrao.edu
#;         Postal address: W. D. Cotton
#;                         National Radio Astronomy Observatory
#;                         520 Edgemont Road
#;                         Charlottesville, VA 22903-2475 USA
#-----------------------------------------------------------------------
#    Makefile to build library routines
#    Obit software
#
#   Expects CC, CFLAGS, DOCS to be set by either environment variable 
#   or Make argument
#     CC is the compiler to use
#     CFLAGS is compile options
#     DOCS is the name of the Binary tables documentation tex file.
#     INCDIR directories with include files
#     LIB defines libraries and search paths,
#
# just do LINUX for now
ARCH = LINUX
#------------------------------------------------------------------------

top_srcdir = @top_srcdir@

CC = @CC@
#CPPFLAGS = @CPPFLAGS@
CPPFLAGS = 
CFLAGS = @CFLAGS@
LDFLAGS = @LDFLAGS@

ALL_CPPFLAGS = $(CPPFLAGS) -I$(top_srcdir)/include @CFITSIO_CPPFLAGS@ \
	@PYTHON_CPPFLAGS@ @FFTW_CPPFLAGS@ @PYTHON_CPPFLAGS@ @DEFS@
ALL_CFLAGS = $(CFLAGS) @GLIB_CFLAGS@
ALL_LDFLAGS = $(LDFLAGS) @CFITSIO_LDFLAGS@ @FFTW_LDFLAGS@

LIBS = ../lib/$(ARCH)/libObit.a @CFITSIO_LIBS@ @FFTW_LIBS@ @GLIB_LIBS@ @LIBS@

CLIENT_CPPFLAGS = $(CPPFLAGS) -I$(top_srcdir)/include @CFITSIO_CPPFLAGS@ \
        @PYTHON_CPPFLAGS@ @FFTW_CPPFLAGS@ @XMLRPC_CLIENT_CPPFLAGS@ @DEFS@
CLIENT_CFLAGS = $(CFLAGS) @GLIB_CFLAGS@ @GSL_CFLAGS@ @ZLIB_CFLAGS@
CLIENT_LDFLAGS = $(LDFLAGS) @CFITSIO_LDFLAGS@ @FFTW_LDFLAGS@ \
        @GSL_LDFLAGS@ @XMLRPC_CLIENT_LDFLAGS@ @ZLIB_LDFLAGS@ 
CLIENT_LIBS = ../lib/$(ARCH)/libObit.a \
        @CFITSIO_LIBS@ @FFTW_LIBS@ @XMLRPC_LIBS@ \
        @GLIB_LIBS@ @GSL_LIBS@ @ZLIB_LIBS@  \
        @XMLRPC_CLIENT_LIBS@ @WWWLIB_WL_RPATH@ @WWW_LIBS@

SWIG = @SWIG@

# Libraries in case they've changed
MYLIBS := $(wildcard ../lib/$(ARCH)/lib*.a)

# Do everything in one big module
TARGETS := Obit.so 

all: $(TARGETS)

# Build shared library for python interface
$(TARGETS): %.so: %_wrap.c $(MYLIBS)
	$(CC) -c $(CLIENT_CPPFLAGS) $(CLIENT_CFLAGS) $*_wrap.c 
	$(CC) -shared  $(CLIENT_CFLAGS) $(CLIENT_LDFLAGS) $*_wrap.o $(CLIENT_LIBS) -o $*.so

# Use Swig to build python/c interface if necessary
Obit_wrap.c: *.inc 
	echo "%module Obit" > Obit.i
	cat ObitTypeMaps.swig >> Obit.i
	cat *.inc >> Obit.i
	$(SWIG) -python  $(INCDIR) Obit.i

clean:
	rm -f Obit.i *.o *.so *.pyc	
