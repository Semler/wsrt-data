""" TaskWindow executes a task asynchronously

More thoughts:
- what happens when task wants to talk to user and ask input?
"""
# Task Window for running tasks asynchronously
# $Id: TaskWindow.py,v 1.2 2005/10/10 19:21:39 bcotton Exp $
#-----------------------------------------------------------------------
#  Copyright (C) 2005
#  Associated Universities, Inc. Washington DC, USA.
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,
#  MA 02139, USA.
#
#  Correspondence concerning this software should be addressed as follows:
#         Internet email: bcotton@nrao.edu.
#         Postal address: William Cotton
#                         National Radio Astronomy Observatory
#                         520 Edgemont Road
#                         Charlottesville, VA 22903-2475 USA
#-----------------------------------------------------------------------

# Task window class
import thread, threading, Tkinter, ScrolledText, tkFileDialog, time

#start up root window
root = Tkinter.Tk()
root.title("ObitTalk Root window")
Tkinter.Label(root,\
              text="ObitTalk root window do not close until finished with tasks").pack(side=Tkinter.TOP)
Tkinter.Button(root, text="Close ALL task windows",command=root.destroy).pack(side=Tkinter.BOTTOM)

class TaskWindow(threading.Thread):
    """ TaskWindow executes a task asynchronously
    
    Create a thread and execute a task.
    To use, create object and then call it's start() member
    a call to the wait() function will wait for program to finish
    Messages are displayed in a ScrolledText window
    NB: The root Tk window should not be closed until you are through with tasks
    """
    
    def __init__(self, TaskObj):
        """ Create and initialize Task Window
        
        TaskObj  = Obit or AIPS or similar task object of the task to execute
        """
        self.myTask = TaskObj
        self.number = 1
        self.myFrame    = None
        self.rootLabel  = None
        self.scrollText = None
        self.proxy      = None
        self.tid        = None
        self.done       = True
        self.Failed     = False
        self.Started    = False
        threading.Thread.__init__(self)
        # end __init__
        
    def run(self):
        """ Create Scrolled Text window, execute program and display messages
        """
        self.myFrame = Tkinter.Toplevel()
        self.myFrame.master.title("ObitTalk Root window")
        self.rootLabel = Tkinter.Label(self.myFrame.master, \
                                       text=self.myTask._name+" Message window")
        self.rootLabel.pack()
        #self.myFrame = Tkinter.Tk()
        #self.myFrame = Tkinter.Frame()
        #self.myFrame.pack()
        self.myFrame.title(self.myTask._name+" Messages")
        self.scrollText = ScrolledText.ScrolledText(self.myFrame)
        self.scrollText.pack(fill=Tkinter.BOTH,expand=1)
        self.abortButt = Tkinter.Button(self.myFrame, text="Abort",command=self.abort)
        self.abortButt.pack(side=Tkinter.RIGHT)
        self.saveButt = Tkinter.Button(self.myFrame, text="Save as",command=self.save)
        self.saveButt.pack(side=Tkinter.RIGHT)
        self.runLabel = Tkinter.Label(self.myFrame, text="Idle")
        self.runLabel.pack(side=Tkinter.LEFT)
        thread.start_new(self.go, ())  # start process in another thread
        # start event loop
        self.myFrame.mainloop()
        # end run
        
    def go(self):
        """ Start up task
        """
        self.write(["Task starts"])
        self.done       = False
        self.Started    = True
        self.runLabel.__setitem__("text","Task Running")
        (self.proxy, self.tid) = self.myTask.spawn()
        log = []
        self.failed = False
        try:
            while not self.myTask.finished(self.proxy, self.tid):
                messages = self.myTask.messages(self.proxy, self.tid)
                if messages:
                    log.extend(messages)
                    self.write(messages)
                continue
        except KeyboardInterrupt, exception:
            self.myTask.abort(self.proxy, self.tid)
            raise exception
        except:   # Aborts throw exceptions that get caught here
            self.failed = True
            #print "DEBUG in go - an exception thrown"
        #print "DEBUG in go, finished", self.myTask.finished(self.proxy, self.tid)

        if not self.failed:
            self.wait()
            self.write(["Task finished"])
            self.runLabel.__setitem__("text","Task Finished")
        self.done = True
        # Turn Abort button into Close
        self.abortButt.__setitem__("text","Close")
        self.abortButt.__setitem__("command",self.delete)
        #if AIPS.log:
        #    for message in log:
        #        AIPS.log.write('%s\n' % message)
        #        continue
        #    pass
        # end go
        
    def write(self, messages):
        """ Create Scrolled Text window, execute program and display messages

        message = message to display, a newline will be added
        """
        #print "DEBUG write",messages
        if not messages:
            return
        for message in messages:
            indx = str(float(self.number))
            if type(message)==str:
                self.scrollText.insert(indx,message+"\n")
            else:
                self.scrollText.insert(indx,message[1]+"\n")
            self.scrollText.see(indx)
            self.number += 1
        # end write
        
    def save(self):
        """ Save current messages into a file selected by a browser
        """
        filename = tkFileDialog.asksaveasfilename()
        if filename:
            alltext = self.scrollText.get(1.0, float(self.number+1))
            open(filename,'w').write(alltext)
        # end save
        
    def abort(self):
        """ Abort task
        """
        #print "in abort"
        if self.done:
            self.write([(1,"Task already finished")])
            return
        self.myTask.abort(self.proxy, self.tid)
        self.write([(1,"Task aborted")])
        self.runLabel.__setitem__("text","Task Aborted")
        self.done = True
        # Turn Abort button into Close
        self.abortButt.__setitem__("text","Close")
        self.abortButt.__setitem__("command",self.delete)
        # end abort
        
    def delete(self):
        """ delete window
        """
        #print "in delete"
        # Wait for finish
        self.wait()                # wait for task to finish
        self.myFrame.destroy()     # kill da wabbit
        self.rootLabel.destroy()   # remove label from Root window
        # end delete
        
    def wait(self):
        """ wait for task to end
        """
        # Wait for it to start first
        while not self.Started:
            time.sleep(0.2)
        time.sleep(0.1)  # Another to be sure
        #print "in wait", self.proxy, self.tid
        if self.done:
            return
        # If the task is aborted, this will throw an exception
        try:
            self.myTask.wait(self.proxy, self.tid)
        except:
            pass
        # end wait
    # end TaskWindow class
