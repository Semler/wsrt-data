# $Id: UV.py,v 1.23 2005/09/14 14:27:29 bcotton Exp $
#-----------------------------------------------------------------------
#  Copyright (C) 2004,2005
#  Associated Universities, Inc. Washington DC, USA.
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,
#  MA 02139, USA.
#
#  Correspondence concerning this software should be addressed as follows:
#         Internet email: bcotton@nrao.edu.
#         Postal address: William Cotton
#                         National Radio Astronomy Observatory
#                         520 Edgemont Road
#                         Charlottesville, VA 22903-2475 USA
#-----------------------------------------------------------------------
 
# Python shadow class to ObitUV class
import OErr, InfoList, Table, AIPSDir, OSystem
import Obit, TableList,  UVDesc

class UVPtr :
    def __init__(self,this):
        self.this = this
    def __setattr__(self,name,value):
        if name == "me" :
            # Out with the old
            Obit.UVUnref(Obit.UV_me_get(self.this))
            # In with the new
            Obit.UV_me_set(self.this,value)
            return
        self.__dict__[name] = value
    def __getattr__(self,name):
        if self.__class__ != UV:
            return
        if name == "me" : 
            return Obit.UV_me_get(self.this)
        # Functions to return members
        if name=="List":
            return PGetList(self)
        if name=="TableList":
            return PGetTableList(self)
        if name=="Desc":
            return PGetDesc(self)
        if name=="VisBuf":
            return PGetVisBuf(self)
        raise AttributeError,str(name)  # Unknown
    def __repr__(self):
        if self.__class__ != UV:
            return
        return "<C UV instance> " + Obit.UVGetName(self.me)
class UV(UVPtr):
    """ Python Obit inteferometer (UV) data class
    
    This class contains interoferometric data and allows access.
    An ObitUV is the front end to a persistent disk resident structure.
    There maybe (usually are) associated tables which either describe
    the data or contain calibration and/or editing information.
    Both FITS (as Tables) and AIPS cataloged data are supported.
    Most access to UV data is through functions as the volume of the data is
    inappropriate to be processed directly in python.
    
    UV Members with python interfaces:
    InfoList  - used to pass instructions to processing
    UVDesc - Astronomical labeling of the data
    
    Additional Functions are available in UVUtil.
    """
    def __init__(self, name) :
        self.this = Obit.new_UV(name)
    def __del__(self):
        if Obit!=None:
            Obit.delete_UV(self.this)
            
    def Zap (self, err):
        """ Delete underlying files and the basic object.
        
        inImage   = Python UV object
        err       = Python Obit Error/message stack
        """
        PZap(self, err)
        # end Zap
        
    def Open (self, access, err):
        """ Open a UV data persistent (disk) form
        
        self   = Python UV object
        access    = access READONLY (1), WRITEONLY (2), READWRITE(3)
        err       = Python Obit Error/message stack
        """
        POpen(self, access, err)
        # end Open
        
    def Close (self, err):
        """ Close a UV  persistent (disk) form
        
        self      = Python UV object
        err       = Python Obit Error/message stack
        """
        PClose (self, err)
        # end Close
        
    def NewTable (self, access, tabType, tabVer, err):
        """ Return the specified associated table
        
        self      = Python UV object
        access    = access code 1=READONLY, 2=WRITEONLY, 3=READWRITE
        tabType   = Table type, e.g. "AIPS AN"
        tabVer    = table version, if > 0 on input that table returned,
                    if 0 on input, the highest version is used.
        err       = Python Obit Error/message stack
        """
        return PNewUVTable (self, access, tabType, tabVer, err)
        # end NewTable

    def Header (self, err):
        """ Write image header on output
        
        self   = Python Obit UV object
        err    = Python Obit Error/message stack
        """
        PHeader (self, err)
        # end Header
        
    def ZapTable (self, tabType, tabVer, err):
        PZapTable (self, tabType, tabVer, err)
        # end ZapTable

    def UpdateTables (self, err):
        PUpdateTables (self, err)
        # end UpdateTables
        
    # End of class member functions (i.e. invoked by x.func())

# Symbolic names for access codes
READONLY  = 1
WRITEONLY = 2
READWRITE = 3

def newPFUV(name, filename, disk, exists, err):
    """ Create and initialize an FITS based UV structure
    
    Create, set initial access information (full image, plane at a time)
    and if exists verifies the file.
    Sets buffer to hold 1000 vis.
    Returns the Python UV object
    name     = name desired for object (labeling purposes)
    filename = name of FITS file
    disk     = FITS directory number
    exists   = if true then the file is opened and closed to verify
    err      = Python Obit Error/message stack
    """
    ################################################################
    out = UV (name)
    Obit.UVSetFITS(out.me, 1000, disk, filename, err.me)
    if exists:
        Obit.UVfullInstantiate (out.me, 1, err.me)
        if err.isErr:
            raise err
    # show any errors 
    OErr.printErrMsg(err, "newPFUV: Error verifying file")
    out.FileType = 'FITS'
    out.FileName = filename
    out.Disk     = disk
    out.Otype    = "UV"
    return out
    # end newPFUV
    
    
def newPAUV(name, Aname, Aclass, disk, seq, exists, err):
    """ Create and initialize an AIPS based UV structure
    
    Create, set initial access information (full image, plane at a time)
    and if exists verifies the file.
    Sets buffer to hold 1000 vis.
    Returns the Python UV object
    name     = name desired for object (labeling purposes)
    Aname    = AIPS name of file
    Aclass   = AIPS class of file
    seq      = AIPS sequence number of file
    disk     = FITS directory number
    exists   = if true then the file is opened and closed to verify
    err      = Python Obit Error/message stack
    """
    ################################################################
    out = UV (name)
    user = OSystem.PGetAIPSuser()
    if exists:
        cno = AIPSDir.PFindCNO(disk, user, Aname, Aclass, "UV", seq, err)
        Obit.UVSetAIPS(out.me, 1000, disk, cno, user, err.me)
        Obit.UVfullInstantiate (out.me, 1, err.me)
    else:
        cno = AIPSDir.PAlloc(disk, user, Aname, Aclass, "UV", seq, err)
        Obit.UVSetAIPS(out.me, 1000, disk, cno, user, err.me)

    if err.isErr:
        raise err
    # show any errors 
    OErr.printErrMsg(err, "newPAUV: Error verifying file")
    out.FileType = 'AIPS'
    out.Disk   = disk
    out.Aname  = Aname
    out.Aclass = Aclass
    out.Aseq   = seq 
    out.Otype  = "UV"
    out.Acno   = cno
    return out
    # end newPAUV

def PScratch (inUV, err):
    """ Create a scratch file suitable for accepting the data to be read from inUV
    
    A scratch UV is more or less the same as a normal UV except that it is
    automatically deleted on the final unreference.
    inUV      = Python UV object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    outUV    = UV("None")
    outUV.me = Obit.UVScratch (inUV.me, err.me);
    if err.isErr:
        raise err
    return outUV
    # end PScratch

def PZap (inUV, err):
    """ Delete underlying files and the basic object.
    
    inUV      = Python UV object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.UVZap (inUV.me, err.me)
    if err.isErr:
        raise err
    # end PZap

def PCopy (inUV, outUV, err):
    """ Make a deep copy of input object.

    Makes structure the same as inUV, copies data, tables
    inUV   = Python UV object to copy
    outUV  = Output Python UV object, must be defined
    err    = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not PIsA(outUV):
        raise TypeError,"outUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.UVCopy (inUV.me, outUV.me, err.me)
    if err.isErr:
        raise err
    # end PCopy

def PClone (inUV, outUV, err):
    """ Make a copy of a object but do not copy the actual data

    This is useful to create an UV similar to the input one.
    inUV   = Python UV object
    outUV  = Output Python UV object, must be defined
    err    = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not PIsA(outUV):
        raise TypeError,"outUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.UVClone (inUV.me, outUV.me, err.me)
    if err.isErr:
        raise err
    # end PClone

def PNewUVTable (inUV, access, tabType, tabVer, err):
    """ Return the specified associated table
    
    inUV      = Python UV object
    access    = access code 1=READONLY, 2=WRITEONLY, 3=READWRITE
    tabType   = Table type, e.g. "AIPS AN"
    tabVer    = table version, if > 0 on input that table returned,
                if 0 on input, the highest version is used.
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    outTable    = Table.Table("None")
    ret = Obit.newUVTable (inUV.me, access, tabType, [tabVer], err.me)
    if err.isErr:
        raise err
    # Table and version returned in a list
    outTable.me = ret[0]
    # Open and close to fully instantiate - should exist
    outTable.Open(access, err)
    outTable.Close(err)
    # Make sure that it worked - the output should be a table
    if not Table.PIsA(outTable):
        raise RuntimeError,"Failed to extract "+tabType+" table from "+PGetName(inUV)
    return outTable
    # end PNewUVTable


def POpen (inUV, access, err):
    """ Open an image persistent (disk) form

    inUV   = Python UV object
    access    = access 1=READONLY, 2=WRITEONLY, 3=READWRITE
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.UVOpen(inUV.me, access, err.me)
    if err.isErr:
        return err
    return ret
    # end POpen

def PDirty (inUV):
    """ Mark UV as needing a header update to disk file

    inUV     = Python UV object
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    #
    Obit.UVDirty (inUV.me)
    # end PDirty

def PClose (inUV, err):
    """ Close an image  persistent (disk) form

    inUV   = Python UV object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.UVClose (inUV.me, err.me)
    if err.isErr:
        raise err
    return ret
    # end PClose

def PZapTable (inUV, tabType, tabVer, err):
    """ Destroy specified table

    inUV      = Python UV object
    tabType   = Table type, e.g. "AIPS AN"
    tabVer    = table version, integer
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.UVZapTable(inUV.me, tabType, tabVer, err.me)
    if err.isErr:
        raise err
    return ret
    # end PZapTable

def PCopyTables (inUV, outUV, exclude, include, err):
    """ Copy Tabeles from one image to another

    inUV      = Python UV object
    outUV     = Output Python UV object, must be defined
    exclude   = list of table types to exclude (list of strings)
                has priority
    include   = list of table types to include (list of strings)
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not PIsA(outUV):
        raise TypeError,"outUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.UVCopyTables  (inUV.me, outUV.me, exclude, include, err.me)
    if err.isErr:
        raise err
    return ret
    # end PCopyTables

def PUpdateTables (inUV, err):
    """ Update any disk resident structures about the current tables

    inUV      = Python UV object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.UVUpdateTables (inUV.me, err.me)
    if err.isErr:
        raise err
    return ret
    # end PUpdateTables

def PFullInstantiate (inUV, access, err):
    """ Fully instantiate an UV by opening and closing

    return 0 on success, else failure
    inUV   = Python UV object
    access    = access code 1=READONLY, 2=WRITEONLY, 3=READWRITE
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.UVfullInstantiate (inUV.me, access, err.me)
    if err.isErr:
        raise err
    return ret
    # end PfullInstantiate

def PGetList (inUV):
    """ Return the member InfoList

    returns InfoList
    inUV   = Python UV object
    """
    ################################################################
     # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    #
    out    = InfoList.InfoList()
    out.me = Obit.InfoListUnref(out.me)
    out.me = Obit.UVGetList(inUV.me)
    return out
    # end PGetList

def PGetTableList (inUV):
    """ Return the member tableList

    returns tableList
    inUV   = Python UV object
    """
    ################################################################
     # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    #
    out    = TableList.TableList(PGetName(inUV))
    out.me = Obit.TableListUnref(out.me)
    out.me = Obit.UVGetTableList(inUV.me)
    return out
    # end PGetTableList


def PHeader (inUV, err):
    """ Print data descriptor

    inUV      = Python Obit UV object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV data"
    #
    # Fully instantiate
    PFullInstantiate (inUV, READONLY, err)
    # File info
    if inUV.FileType=="AIPS":
        print "AIPS UV Data Name: %12s Class: %6s seq: %8d disk: %4d" % \
              (inUV.Aname, inUV.Aclass, inUV.Aseq, inUV.Disk)
    elif inUV.FileType=="FITS":
        print "FITS UV Data Disk: %5d File Name: %s " % \
              (inUV.Disk, inUV.FileName)
    # print in ImageDesc
    UVDesc.PHeader(inUV.Desc)
    # Tables
    TL = inUV.TableList
    Tlist = TableList.PGetList(TL, err)
    Tdict = {}
    # Once to find everything
    for item in Tlist:
        Tdict[item[1]] = item[0]
    # Again to get Max
    for item in Tlist:
        count = max (Tdict[item[1]], item[0])
        Tdict[item[1]] = count
    for item,count in Tdict.items():
        print "Maximum version number of %s tables is %d " % \
              (item, count)
    # end PHeader
    

def PGetDesc (inUV):
    """ Return the member UVDesc

    returns UVDesc as a Python Dictionary
    inUV   = Python UV object
    """
    ################################################################
     # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    #
    out    = UVDesc.UVDesc("None")
    out.me = Obit.UVGetDesc(inUV.me)
    return out
    # end PGetDesc

def PGetVisBuf (inUV):
    return Obit.UVGetVisBuf(inUV.me)

def PGetHighVer (inUV, tabType):
    """ Get highest version number of a specified Table

    returns highest tabType version number, 0 if none.
    inUV   = Python UV object
    tabType   = Table type, e.g. "OTFSoln"
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    #
    return Obit.UVGetHighVer(inUV.me, tabType)
    # end PGetHighVer

def PGetSubA (inUV, err):
    """ Get Subarray information

    returns 0 on success, else 1
    inUV   = Python UV object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.UVGetSubA(inUV.me, err.me)
    if err.isErr:
        raise err
    # end PGetSubA 

def PGetFreq (inUV, err):
    """ Get Frequency information

    inUV   = Python UV object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.UVGetSubA(inUV.me, err.me)
    if err.isErr:
        raise err
    return ret
    # end PGetFreq


def PIsScratch (inUV):
    """ Tells if UV is a scratch object

    return true, false (1,0)
    inUV   = Python UV object
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    #
    return Obit.UVisScratch(inUV.me)
    # end PIsScratch

def PIsA (inUV):
    """ Tells if input really a Python Obit UV

    return true, false (1,0)
    inUV   = Python UV object
    """
    ################################################################
     # Checks
    if inUV.__class__ != UV:
        return 0
    return Obit.UVIsA(inUV.me)
    # end PIsA

def PGetName (inUV):
    """ Tells UV object name (label)

    returns name as character string
    inUV   = Python UV object
    """
    ################################################################
     # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    #
    return Obit.UVGetName(inUV.me)
    # end PGetName

#----------------------  UVUtil routines    ------------------------

def PUtilUVWExtrema (inUV, err):
    """ Get Subarray information

    returns array [0]=maximum baseline length (in U,V), [1] = maximum W
    inUV   = Python UV object
    err    = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    val = [0.0, 0.0] # create array
    Obit.UVUtil(inUV.me, err.me, val)
    if err.isErr:
        raise err
    return val
    # end PUTilUVWExtrema

def PUtilCopyZero (inUV, scratch, outUV, err):
    """ Copy a UV data set replacing data by zero, weight 1

    returns UV data object
    inUV   = Python UV object to copy
    scratch= True if this is to be a scratch file (same type as inUV)
    outUV  = Predefined UV data if scratch is False
             ignored if scratch True.
    err    = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if ((not scratch) and (not PIsA(outUV))):
        raise TypeError,"outUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    # Create output for scratch
    if scratch:
        outUV = UV("None")
        outUV.me = Obit.UVUtilCopyZero(inUV.me, scratch, outUV.me, err.me)
        if err.isErr:
            raise err
    return outUV
    # end PUTilCopyZero

def PUtilVisDivide (in1UV, in2UV, outUV, err):
    """ Divides the visibilites in in1UV by those in in2UV

    in1UV   = Numerator  Python UV object
    in2UV   = Denominator Python UV object
    outUV   = Output Python UV object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(in1UV):
        raise TypeError,"in1UV MUST be a Python Obit UV"
    if not PIsA(in2UV):
        raise TypeError,"in2UV MUST be a Python Obit UV"
    if not PIsA(outUV):
        raise TypeError,"outUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.UVUtilVisDivide(in1UV.me, in2UV.me, outUV.me, err.me)
    if err.isErr:
        raise err
    # end PUtilVisDivide

def PUtilVisCompare (in1UV, in2UV, err):
    """ Compares the visibilites in in1UV with those in in2UV

    returns RMS real, imaginary parts/amplitude
    in1UV   = Numerator  Python UV object
    in2UV   = Denominator Python UV object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(in1UV):
        raise TypeError,"in1UV MUST be a Python Obit UV"
    if not PIsA(in2UV):
        raise TypeError,"in2UV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.UVUtilVisCompare(in1UV.me, in2UV.me, err.me)
    if err.isErr:
        raise err
    return ret
    # end PUtilVisCompare

def PEditTD (inUV, outUV, err):
    """ Time-domain editing of UV data - produces FG table

    Fill flagging table with clipping by RMS values of the real and imaginary
    parts.  All correlations are clipped on each baseline if the RMS is
    larger than  the maximum.  The clipping is done independently in
    each time interval defined by timeAvg. 
       The clipping level is given by MIN (A, MAX (B,C)) where:
    A = sqrt (maxRMS[0]**2 + (avg_amp * maxRMS[1])**2)
       and avg_amp is the average amplitude on each baseline.
    B = median RMS + 3 * sigma of the RMS distribution.
    C = level corresponding to 3% of the data.
       All data on a given baseline/correlator are flagged if the RMS
    exceeds the limit.  If a fraction of bad baselines on any correlator
    exceeds maxBad, then all data to that correlator is flagged.  In
    addition, if the offending correlator is a parallel hand correlator
    then any corresponding cross hand correlations are also flagged.
    Flagging entries are written into FG table flagTab.
    control parameters on inUV info member
      "flagTab" OBIT_int    (1,1,1) FG table version number [ def. 1]
      "timeAvg" OBIT_float  (1,1,1) Time interval over which to determine 
                data to be flagged (days) [def = 1 min.]
                NB: this should be at least 2 integrations.
      "maxRMS"  OBIT_float (2,1,1) Maximum RMS allowed, constant plus 
                amplitude coefficient. 
      "maxBad"  OBIT_float (1,1,1) Fraction of allowed flagged baselines 
                [default 0.25]

    inUV   = Python UV object to clip/flag
    outUV  = UV data onto which the FG table is to be attached.
             May be the same as inUV.
    err    = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if ((not scratch) and (not PIsA(outUV))):
        raise TypeError,"outUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    # Create output for scratch
    if scratch:
        outUV = UV("None")
        outUV.me = Obit.UVEditTD(inUV.me, outUV.me, err.me)
        if err.isErr:
            raise err
    return outUV
    # end PEditTD

def PEditStokes (inUV, outUV, err):
    """ Stokes editing of UV data, FG table out

       All data on a given baseline/correlator are flagged if the 
    amplitude of the datatype "FlagStok"  exceeds maxAmp.  
    If a fraction of bad baselines on any antenna/channel/IF exceeds 
    maxBad, then all data to that correlator is flagged.  
    Flagging entries are written into FG table flagTab.
    Results are unpredictable for uncalibrated data.
    Control parameters on info member of inUV:
      "flagStok" OBIT_string (1,1,1) Stokes value to clip (I, Q, U, V, R, L)
                 default = "V"
      "flagTab" OBIT_int    (1,1,1) FG table version number [ def. 1]
                NB: this should not also being used to flag the input data!
      "timeAvg" OBIT_float  (1,1,1) Time interval over which to determine
                data to be flagged (days) [def = 1 min.]
      "maxAmp"  OBIT_float (1,1,1) Maximum VPol allowed
      "maxBad"  OBIT_float (1,1,1) Fraction of allowed flagged baselines 
                to an antenna above which all baselines are flagged.
                [default 0.25]
 
    inUV   = Python UV object to clip/flag
    outUV  = UV data onto which the FG table is to be attached.
             May be the same as inUV.
    err    = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if ((not scratch) and (not PIsA(outUV))):
        raise TypeError,"outUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    # Create output for scratch
    if scratch:
        outUV = UV("None")
        outUV.me = Obit.UVEditStokes(inUV.me, outUV.me, err.me)
        if err.isErr:
            raise err
    return outUV
    # end PEditStokes

def PEditClip (inUV, scratch, outUV, err):
    """ Clip raw visibilities

    control parameters on inUV info member
       "maxAmp" OBIT_float  (1,1,1) Maximum allowed amplitude
       "oper"   OBIT_string (4,1,1) operation type:
            "flag" flag data with amplitudes in excess of maxAmp
            "clip" clip amplitudes at maxAmp and preserve phase
               default is "flag"
    returns UV data object
    inUV   = Python UV object to clip/flag
    scratch= True if this is to be a scratch file (same type as inUV)
    outUV  = Predefined UV data if scratch is False, may be inUV
             ignored if scratch True.
    err    = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if ((not scratch) and (not PIsA(outUV))):
        raise TypeError,"outUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    # Create output for scratch
    if scratch:
        outUV = UV("None")
        outUV.me = Obit.UVEditClip(inUV.me, scratch, outUV.me, err.me)
        if err.isErr:
            raise err
    return outUV
    # end PEditClip

def PEditClipStokes (inUV, scratch, outUV, err):
    """ Flag visibilities by Stokes

    Clip a uv data set.  Data with amplitudes of the selected stokes
    in excess of maxAmp are flagged.  Optionally all correlations associated
    may be flagged.  Stokes conversion as needed for test.
    Control parameters are on the inUV info member:
      "clipStok" OBIT_string (1,1,1) Stokes value to clip (I, Q, U, V, R, L)
                  default = "I"
       "flagAll"  Obit_bool   (1,1,1) if true, flag all associated correlations
                  default = True
       "maxAmp"   OBIT_float  (1,1,1) Maximum allowed amplitude
    returns UV data object
    inUV   = Python UV object to clip/flag
    scratch= True if this is to be a scratch file (same type as inUV)
    outUV  = Predefined UV data if scratch is False, may be inUV
             ignored if scratch True.
    err    = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inUV):
        raise TypeError,"inUV MUST be a Python Obit UV"
    if ((not scratch) and (not PIsA(outUV))):
        raise TypeError,"outUV MUST be a Python Obit UV"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    # Create output for scratch
    if scratch:
        outUV = UV("None")
        outUV.me = Obit.UVEditClipStokes(inUV.me, scratch, outUV.me, err.me)
        if err.isErr:
            raise err
    return outUV
    # end PEditClipStokes

