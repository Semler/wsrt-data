# Python/Obit Astronomical Image class
# $Id: Image.py,v 1.35 2005/08/30 14:24:31 bcotton Exp $
#-----------------------------------------------------------------------
#  Copyright (C) 2004,2005
#  Associated Universities, Inc. Washington DC, USA.
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,
#  MA 02139, USA.
#
#  Correspondence concerning this software should be addressed as follows:
#         Internet email: bcotton@nrao.edu.
#         Postal address: William Cotton
#                         National Radio Astronomy Observatory
#                         520 Edgemont Road
#                         Charlottesville, VA 22903-2475 USA
#-----------------------------------------------------------------------

# Obit Image manupulation
import Obit, Table, FArray, OErr, ImageDesc, InfoList, History, AIPSDir, OSystem
import TableList, AIPSDir

# Python shadow class to ObitImage class
 
class ImagePtr :
    def __init__(self,this):
        self.this = this
    def __setattr__(self,name,value):
        if name == "me" :
            # Out with the old
            Obit.ImageUnref(Obit.Image_me_get(self.this))
            # In with the new
            Obit.Image_me_set(self.this,value)
            return
        if name=="FArray":
            return PSetFArray(self,value)
        self.__dict__[name] = value
    def __getattr__(self,name):
        if self.__class__ != Image:
            return
        if name == "me" : 
            return Obit.Image_me_get(self.this)
        # Functions to return members
        if name=="List":
            return PGetList(self)
        if name=="TableList":
            return PGetTableList(self)
        if name=="Desc":
            return PGetDesc(self)
        if name=="FArray":
            return PGetFArray(self)
        raise AttributeError,str(name)   # Unknown
    def __repr__(self):
        if self.__class__ != Image:
            return
        return "<C Image instance> " + Obit.ImageGetName(self.me)
class Image(ImagePtr):
    """ Python Obit Image class

    This class contains an astronomical image and allows access.
    An ObitImage is the front end to a persistent disk resident structure.
    Magic value blanking is supported, blanked pixels have the value
    OBIT_MAGIC (ObitImageDesc.h).
    Pixel data are kept in an FArray structure which is how Python acceses the data.
    There may be associated tables (e.g. "AIPS CC" tables).
    Both FITS and AIPS cataloged images are supported.

    Image Members with python interfaces:
    InfoList  - used to pass instructions to processing
                Member List (readonly)
    ImageDesc - Astronomical labeling of the image
                Member Desc (readonly)
    FArray    - Container used for pixel data
                Member FArray

    Additional Functions are available in ImageUtil.
    """
    def __init__(self, name) :
        self.this = Obit.new_Image(name)
    def __del__(self):
        if Obit!=None:
            Obit.delete_Image(self.this)

    def Zap (self, err):
        """ Delete underlying files and the basic object.
        
        self      = Python Image object
        err       = Python Obit Error/message stack
        """
        PZap(self,err)
        # end Zap

    def Open (self, access, err, blc=None, trc=None):
        """ Open an image persistent (disk) form
        
        self   = Python Image object
        access    = access READONLY (1), WRITEONLY (2), READWRITE(3)
        err       = Python Obit Error/message stack
        blc       = if given and a list of integers (min 2) giving
        bottom left corner (1-rel) of subimage
        trc       = if given and a list of integers (min 2) giving
        top right corner (1-rel) of subimage
        """
        POpen(self, access, err, blc=blc, trc=trc)
        # end Open

    def Close (self, err):
        """ Close an image  persistent (disk) form
        
        self      = Python Image object
        err       = Python Obit Error/message stack
        """
        PClose (self, err)
        # end Close

    def Read (self, err):
        """ Read an image  persistent (disk) form
        
        The data to be read is specified in the InfoList mamber
        Uses FArray member as buffer.
        self      = Python Image object
        err       = Python Obit Error/message stack
        """
        PRead (self, err)
        # end Read

    def Write (self, err):
        """ Write an image  persistent (disk) form
        
        The data to be written is specified in the InfoList member
        Uses FArray member as buffer.
        self      = Python Image object
        err       = Python Obit Error/message stack
        """
        PWrite (self, err)
        # end Write
    
    def ReadFA (self, array, err):
        """ Read an image  persistent (disk) form to a specified FArray

        The data to be read is specified in the InfoList member
        self   = Python Image object
        array  = Python FArray to accept data
        err    = Python Obit Error/message stack
        """
        PReadFA (self, array, err)
        # end ReadFA

    def WriteFA (self, array, err):
        """ Write an image  persistent (disk) form from a specified FArray

        The data to be written is specified in the InfoList member
        self      = Python Image object
        array     = Python FArray to write
        err       = Python Obit Error/message stack
        """
        PWriteFA (self, array, err)
        # end WriteFA

    def ReadPlane (self, err, blc=None, trc=None):
        """ Read an image plane into the FArray 
        
        Reads the plane specified by blc, trc
        into the FArray associated with the image
        self     = Python Image object
        err      = Python Obit Error/message stack
        blc      = if given and a list of integers (min 2) giving
                   bottom left corner (1-rel) of subimage
        trc      = if given and a list of integers (min 2) giving
                   top right corner (1-rel) of subimage
        returns Python  FArray from Image with data read
        """
        return PReadPlane (self, err, blc, trc)
        # end PReadPlane
   
    def WritePlane (self, imageData, err):
        """ Write an image plane.
        
        Writes the plane specified by blc, trc on image infoList
        Checks if the current FArray on Image is compatable with
        imageData.
        self      = Python Image object
        imageData = Python FArray with data to write
        err       = Python Obit Error/message stack
        """
        PWritePlane (Image, imageData, err)
        # end PWritePlane

    def GetPlane (self, array, plane, err):
        """ Read an image  persistent (disk) form to an (optional) specified FArray
        
        The data to be read is specified in the InfoList member as modified by plane
        self   = Python Image object
        array  = Python FArray to accept data, if None use inImage buffer
        plane  = array of 5 integers giving (1-rel) pixel numbers
        err    = Python Obit Error/message stack
        """
        PGetPlane (self, array, plane, err)
        # end PGetPlane

    def PutPlane (self, array, plane, err):
        """ Write an image persistent (disk) form from an (optional) specified FArray
        
        The data to be written is specified in the InfoList member as modified by plane
        self   = Python Image object
        array     = Python FArray to provide data, if None use inImage buffer
        plane     = array of 5 integers giving (1-rel) pixel numbers
        err       = Python Obit Error/message stack
        """
        PPutPlane (self, array, plane, err)
        # end PutPlane
        
    def NewTable (self, access, tabType, tabVer, err):
        """ Return the specified associated table
        
        self      = Python Image object
        access    = access code 1=READONLY, 2=WRITEONLY, 3=READWRITE
        tabType   = Table type, e.g. "AIPS CC"
        tabVer    = table version, if > 0 on input that table returned,
                    if 0 on input, the highest version is used.
        err       = Python Obit Error/message stack
        """
        return PImageGetTable (self, access, tabType, tabVer, err)
        # end NewTable

    def Header (self, err):
        """ Write image header on output
        
        self   = Python Obit Image object
        err    = Python Obit Error/message stack
        """
        PHeader (self, err)
        # end Header

    def ZapTable (self, tabType, tabVer, err):
        PZapTable (self, tabType, tabVer, err)
        # end ZapTable

    def UpdateTables (self, err):
        PUpdateTables (self, err)
        # end UpdateTables
        
    # End of class member functions (i.e. involed by x.func()(


# Commonly used, dangerous variables
dim=[1,1,1,1,1]
blc=[1,1,1,1,1,1,1]
trc=[0,0,0,0,0,0,0]
err=OErr.OErr()

# Symbolic names for access codes
READONLY  = 1
WRITEONLY = 2
READWRITE = 3

def ObitName(ObitObject):
    """Return name of an Obit object or input if not an Obit Object
    """
    ################################################################
    out = ObitObject    # in case
    if type(out) == types.StringType:
        return out
    if ObitObject.me.find("_ObitImage_p") >= 0:
        return Obit.ImageGetName(ObitObject.me)
    if ObitObject.me.find("_ObitOTF_p") >= 0:
        return Obit.OTFGetName(ObitObject.me)
    if ObitObject.me.find("_ObitTable_p") >= 0:
        return Obit.TableGetName(ObitObject.me)
    if ObitObject.me.find("_Obit_p") >= 0:
        return Obit.GetName(ObitObject.me)
    return out
    # end ObitName
        

def input(inputDict):
    """ Print the contents of an input Dictionary

    inputDict = Python Dictionary containing the parameters for a routine
    """
    ################################################################
    print 'Current values of entries'
    myList = inputDict.items()
    myList.sort()
    for k,v in myList:
        # Print name of Obit objects (or string)
        #if (type(v)==types.StringType):
        #    print '  ',k,' = ',ObitName(v)
        #else:
        print '  ',k,' = ',v
        
    # end input

def newObit(name, filename, disk, exists, err):
    """ Create and initialize an Image structure

    Create, set initial access information (full image, plane at a time)
    and if exists verifies the file.
    Returns the Python Image object
    name     = name desired for object (labeling purposes)
    filename = name of FITS file
    disk     = FITS directory number
    exists   = if true then the file is opened and closed to verify
    err      = Python Obit Error/message stack
    """
    ################################################################
    out = Image (name)
    Obit.ImageSetFITS(out.me, 2, disk, filename, blc, trc, err.me)
    if exists:
        Obit.ImagefullInstantiate (out.me, 1, err.me)
    if err.isErr:
        raise err
    # show any errors 
    #OErr.printErrMsg(err, "newObit: Error verifying file")
    out.FileType = 'FITS'
    out.FileName = filename
    out.Disk = disk
    return out      # seems OK
    # end newObit

    
def newPFImage(name, filename, disk, exists, err):
    """ Create and initialize an FITS based Image structure

    Create, set initial access information (full image, plane at a time)
    and if exists verifies the file.
    Returns the Python Image object
    name     = name desired for object (labeling purposes)
    filename = name of FITS file
    disk     = FITS directory number
    exists   = if true then the file is opened and closed to verify
    err      = Python Obit Error/message stack
    """
    ################################################################
    out = Image (name)
    Obit.ImageSetFITS(out.me, 2, disk, filename, blc, trc, err.me)
    if exists:
        Obit.ImagefullInstantiate (out.me, 1, err.me)
    if err.isErr:
        raise err
    # show any errors 
    #OErr.printErrMsg(err, "newPFImage: Error verifying file")
    out.FileType = 'FITS'
    out.FileName = filename
    out.Disk = disk
    out.Otype  = "Image"
    return out      # seems OK
    # end newPFImage

    
def newPAImage(name, Aname, Aclass, disk, seq, exists, err):
    """ Create and initialize an AIPS based Image structure

    Create, set initial access information (full image, plane at a time)
    and if exists verifies the file.
    Returns the Python Image object
    name     = name desired for object (labeling purposes)
    Aname    = AIPS name of file
    Aclass   = AIPS class of file
    seq      = AIPS sequence number of file
    disk     = FITS directory number
    exists   = if true then the file is opened and closed to verify
    err      = Python Obit Error/message stack
    """
    ################################################################
    out = Image (name)
    user = OSystem.PGetAIPSuser()
    # print "disk, aseq", disk, seq
    if exists:
        cno = AIPSDir.PFindCNO(disk, user, Aname, Aclass, "MA", seq, err)
        Obit.ImageSetAIPS(out.me, 2, disk, cno, user, blc, trc, err.me)
        Obit.ImagefullInstantiate (out.me, 1, err.me)
        #print "found",Aname,Aclass,"as",cno
    else:
        cno = AIPSDir.PAlloc(disk, user, Aname, Aclass, "MA", seq, err)
        Obit.ImageSetAIPS(out.me, 2, disk, cno, user, blc, trc, err.me)
        #print "assigned",Aname,Aclass,"to",cno

    if err.isErr:
        raise err
    # show any errors 
    #OErr.printErrMsg(err, "newObit: Error verifying file")
    # Add File info
    out.FileType = 'AIPS'
    out.Disk   = disk
    out.Aname  = Aname
    out.Aclass = Aclass
    out.Aseq   = seq 
    out.Otype  = "Image"
    out.Acno   = cno
    return out      # seems OK
    # end newPAImage

    
def newPACNO(disk, cno, exists, err):
    """ Create and initialize an AIPS based Image structure

    Create, set initial access information (full image, plane at a time)
    and if exists verifies the file.
    Returns the Python Image object
    disk     = AIPS directory number
    cno      = AIPS catalog number
    exists   = if true then the file is opened and closed to verify
    err      = Python Obit Error/message stack
    """
    ################################################################
    out = Image ("AIPS Image")
    user = OSystem.PGetAIPSuser()
    # print "disk, aseq", disk, seq
    if exists:
        Obit.ImageSetAIPS(out.me, 2, disk, cno, user, blc, trc, err.me)
        Obit.ImagefullInstantiate (out.me, 1, err.me)
    else:
        Obit.ImageSetAIPS(out.me, 2, disk, cno, user, blc, trc, err.me)

    if err.isErr:
        raise err
    # Add File info
    out.FileType = 'AIPS'
    out.Disk   = disk
    out.Acno   = cno
    # Lookup name etc
    s = AIPSDir.PInfo (disk, user, cno, err)
    # parse returned string
    Aname = s[0:12]
    Aclass = s[13:19]
    Aseq = int(s[20:25])
    Atype = s[26:28]
    out.Aname  = Aname
    out.Aclass = Aclass
    out.Aseq   = Aseq 
    out.Otype  = "Image"
    return out      # seems OK
    # end newPACNO

    
# Image utilities
def PReadPlane (inImage, err, blc=None, trc=None):
    """ Read an image plane into the FArray 

    Reads the plane specified by blc, trc
    into the FArray associated with the image
    inImage   = Python Image object
    err       = Python Obit Error/message stack
    blc       = if given and a list of integers (min 2) giving
                bottom left corner (1-rel) of subimage
    trc       = if given and a list of integers (min 2) giving
                top right corner (1-rel) of subimage
    returns Python  FArray from Image with data read
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,'inImage MUST be a Python Obit Image'
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    # Read image
    inImage.Open(READONLY, err, blc, trc)
    inImage.Read(err)
    #OErr.printErrMsg(err, "Error reading image")
    imageData = inImage.FArray         # Image FArray (data)
    inImage.Close(err)
    return imageData
    # end PReadPlane
   
def PWritePlane (Image, imageData, err):
    """ Write an image plane.

    Writes the plane specified by blc, trc on image infoList
    Checks if the current FArray on Image is compatable with
    imageData.
    Image     = Python Image object
    imageData = Python FArray with data to write
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(Image):
        raise TypeError,"Image MUST be a Python Obit Image"
    if not FArray.PIsA(imageData):
        raise TypeError,"imageData MUST be a Python Obit FArray"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    # Write image
    Image.Open(READWRITE, err)
    # Check that FArrays are compatible
    idtest = PGetFArray(Image)
    if not FArray.PIsCompatable (imageData, idtest):
        raise RuntimeError,'Images incompatable'
    Image.WriteFA(imageData, err)
    Image.Close(err)
    #OErr.printErrMsg(err, "Error writing images")
    # end PWritePlane

def PScratch (inImage, err):
    """ Create a scratch file suitable for accepting the data to be read from inImage

    A scratch Image is more or less the same as a normal Image except that it is
    automatically deleted on the final unreference.
    inImage   = Python Image object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    outImage    = Image("None")
    outImage.me = Obit.ImageScratch (inImage.me, err.me);
    if err.isErr:
        raise err
    return outImage
    # end PScratch

def PZap (inImage, err):
    """ Delete underlying files and the basic object.

    inImage   = Python Image object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.ImageZap (inImage.me, err.me)
    if err.isErr:
        raise err
    # end PZap

def PCopy (inImage, outImage, err):
    """ Make a deep copy of input object.

    Makes structure the same as inImage, copies data, tables
    inImage   = Python Image object to copy
    outImage  = Output Python Image object, must be defined
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not PIsA(outImage):
        raise TypeError,"outImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.ImageCopy (inImage.me, outImage.me, err.me)
    if err.isErr:
        raise err
    # end PCopy

def PClone (inImage, outImage, err):
    """ Make a copy of a object but do not copy the actual data

    This is useful to create an Image similar to the input one.
    inImage   = Python Image object
    outImage  = Output Python Image object, must be defined
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not PIsA(outImage):
        raise TypeError,"outImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.ImageClone (inImage.me, outImage.me, err.me)
    if err.isErr:
        raise err
    # end PClone

def PClone2 (inImage1, inImage2, outImage, err):
    """ Make a copy of a object but do not copy the actual data

    inImage1  = Python Image object to clone
    inImage2  = Python Image object whose geometry is to be used
    outImage  = Output Python Image object, must be defined,
                will be defined as Memory only
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage1):
        raise TypeError,"inImage1 MUST be a Python Obit Image"
    if not PIsA(inImage2):
        raise TypeError,"inImage2 MUST be a Python Obit Image"
    if not PIsA(outImage):
        raise TypeError,"outImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.ImageClone2 (inImage1.me, inImage2.me, outImage.me, err.me)
    if err.isErr:
        raise err
    # end PClone2

def PCloneMem (inImage, outImage, err):
    """ Make a Memory only clone of an Image structure

    This is useful for temporary structures
    inImage   = Python Image object
    outImage  = Output Python Image object, must be defined
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not PIsA(outImage):
        raise TypeError,"outImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.ImageCloneMem (inImage.me, outImage.me, err.me)
    if err.isErr:
        raise err
    # end PCloneMem

def PCopyQuantizeFITS (inImage, outImage, err, fract=0.25, inHistory=None):
    """ Make a copy of an image quantizing to a 16 or 32 bit integer
        FITS image

    inImage   = Python Image object
    outImage  = Output Python Image object, must be defined
                but not fully created
    err       = Python Obit Error/message stack
    fract     = quantization level as a fraction of the plane min. RMS
    inHistory = if given a History object to copy to the output FITS header
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not PIsA(outImage):
        raise TypeError,"outImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    # Input image info
    inImage.Open(READONLY, err)
    #OErr.printErrMsg(err, "Error opening input")
    IdescDict = inImage.Desc.Dict          # Python dict object
    Idim  = IdescDict["inaxes"]            # Dimensionality
    IData = inImage.FArray                 # image plane Data
    
    # Loop over input plane getting statistics
    inMax = -1.0e30; inMin = 1.0e30; inRMS = 1.0e30
    pos=[0,0]
    for plane in range(Idim[2]):
        planeDim = [plane+1,1,1,1,1]  # Only 3D
        inImage.GetPlane(None, planeDim, err)   # Read plane (IData)
        inMax = max (inMax, FArray.PMax(IData, pos))
        inMin = min (inMin, FArray.PMin(IData, pos))
        rms = IData.RMS
        # debug
        #print "plane",plane,"rms",rms
        if (rms>0.0):
            inRMS = min (inRMS, rms)

    # Set quantization level
    quant = fract * inRMS

    # debug
    if (quant<=0.0):
        print "PCopyQuantizeFITS, min, max, rms, quant",inMax,inMin,inRMS,quant
        quant=1.0e-6
    
    # Set bitpit
    dr = max (abs(inMax), abs(inMin)) / quant
    if dr < 32760.0:
        bitpix = 16
    elif dr < 2147483600.0:
        bitpix = 32;
    else:   # No can do
        raise RuntimeError,"Cannot quantize image, excessive precision"

    # Copy output from input

    # Modify input descriptor and use as output
    # Set quantization and bitpix on output
    Odesc = outImage.Desc
    OdescDict = inImage.Desc.Dict
    OdescDict["bitpix"] = bitpix
    OdescDict["minval"] = inMin
    OdescDict["maxval"] = inMax
    OdescDict["origin"] = "Generated by Obit"
    Odesc.Dict = OdescDict
    Olist = Odesc.List      # quant goes on infoList
    dim = [1,1,1,1,1]
    InfoList.PAlwaysPutFloat (Olist, "Quant", dim, [quant])

    # Open output
    outImage.Open(WRITEONLY, err)                # Open
    #OErr.printErrMsg(err, "Error opening output")

    # Copy history if requested
    if inHistory!=None:
        outHistory  = History.History("Output history", outImage.List, err)
        History.PCopy2Header (inHistory, outHistory, err)
        OErr.printErrMsg(err, "Error with history")
  
    # Copy data
    for plane in range(Idim[2]):
        planeDim = [plane+1,1,1,1,1]  # Only 3D
        inImage.GetPlane(None, planeDim, err)        # Read
        outImage.PutPlane(IData, planeDim, err)      # Write
        
    # Close images
    inImage.Close(err)
    outImage.Close(err)
    #OErr.printErrMsg(err, "Error closing files")
# end PCopyQuantizeFITS

def PCompare (in1Image, in2Image, err, plane=[1,1,1,1,1]):
    """ Compare a plane of two images

    returns list [max. abs in1Image, max abs difference, RMS difference]
    in1Image  = Python Image object
    in2Image  = Python Image object, on output, the FArray contains the difference.
    err       = Python Obit Error/message stack
    plane     = plane to compare
    """
    ################################################################
    # Checks
    if not PIsA(in1Image):
        raise TypeError,"in1Image MUST be a Python Obit Image"
    if not PIsA(in2Image):
        raise TypeError,"in2Image MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    # Input images, read plane 
    in1Image.Open(READONLY, err)
    #OErr.printErrMsg(err, "Error opening input")
    in1Image.GetPlane(None, plane, err)
    I1Data = in1Image.FArray 
    in2Image.Open(READONLY, err)
    #OErr.printErrMsg(err, "Error opening input")
    in2Image.GetPlane(None, plane, err)
    I2Data = in2Image.FArray
   
    # Close images
    in1Image.Close(err)
    in2Image.Close(err)
    #OErr.printErrMsg(err, "Error closing files")

    # Get difference
    FArray.PSub(I1Data, I2Data, I2Data)
    pos=[1,1]
    result = [FArray.PMaxAbs(I1Data,pos), FArray.PMaxAbs(I2Data,pos), FArray.PRMS(I2Data)]
    return result
# end PCompare

def PImageGetTable (inImage, access, tabType, tabVer, err):
    """ Return the specified associated table

    returns Python Obit Table
    inImage   = Python Image object
    access    = access code 1=READONLY, 2=WRITEONLY, 3=READWRITE
    tabType   = Table type, e.g. "OTFSoln"
    tabVer    = table version, if > 0 on input that table returned,
                if 0 on input, the highest version is used.
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    outTable    = Table.Table("None")
    ret = Obit.ImageGetTable (inImage.me, access, tabType, [tabVer], err.me)
    if err.isErr:
        raise err
    # Table and version returned in a list
    outTable.me = ret[0]
    # Open and close to fully instantiate - should exist
    outTable.Open(access, err)
    outTable.Close(err)
    # Make sure that it worked - the output should be a table
    if not Table.PIsA(outTable):
        #OErr.printErr(err)  # Any Obit messages
        raise RuntimeError,"Failed to extract "+tabType+" table from "+PGetName(inImage)
    return outTable
    # end PImageGetTable
    

def PHeader (inImage, err):
    """ Print image descriptor

    inImage   = Python Image object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    #
    # Fully instantiate
    PFullInstantiate (inImage, READONLY, err)
    # File info
    if inImage.FileType=="AIPS":
        print "AIPS Image Name: %12s Class: %6s seq: %8d disk: %4d" % \
              (inImage.Aname, inImage.Aclass, inImage.Aseq, inImage.Disk)
    elif inImage.FileType=="FITS":
        print "FITS Image Disk: %5d File Name: %s " % \
              (inImage.Disk, inImage.FileName)
    # print in ImageDesc
    ImageDesc.PHeader(inImage.Desc)
    # Tables
    TL = inImage.TableList
    Tlist = TableList.PGetList(TL, err)
    Tdict = {}
    # Once to find everything
    for item in Tlist:
        Tdict[item[1]] = item[0]
    # Again to get Max
    for item in Tlist:
        count = max (Tdict[item[1]], item[0])
        Tdict[item[1]] = count
    for item,count in Tdict.items():
        print "Maximum version number of %s tables is %d " % \
              (item, count)
    # end PHeader
    

def POpen (inImage, access, err, blc=None, trc=None):
    """ Open an image persistent (disk) form

    inImage   = Python Image object
    access    = access READONLY (1), WRITEONLY (2), READWRITE(3)
    err       = Python Obit Error/message stack
    blc       = if given and a list of integers (min 2) giving
                bottom left corner (1-rel) of subimage
    trc       = if given and a list of integers (min 2) giving
                top right corner (1-rel) of subimage
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    # Set subimage if given
    if (blc.__class__==list) | (trc.__class__==list):
        inInfo = inImage.List
        if (blc.__class__==list) & (len(blc)>1) & (blc[0].__class__==int):
            dim = [len(blc),1,1,1,1]
            InfoList.PAlwaysPutInt  (inInfo, "BLC", dim, blc)
        if (trc.__class__==list) & (len(trc)>1) & (trc[0].__class__==int):
            dim = [len(trc),1,1,1,1]
            InfoList.PAlwaysPutInt  (inInfo, "TRC", dim, trc)
    Obit.ImageOpen(inImage.me, access, err.me)
    if err.isErr:
        raise err
    # end POpen

def PClose (inImage, err):
    """ Close an image  persistent (disk) form

    inImage   = Python Image object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.ImageClose (inImage.me, err.me)
    if err.isErr:
        raise err
    # end PClose

def PDirty (inImage):
    """ Mark Image as needing a header update to disk file

    inImage     = Python Image object
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    #
    Obit.ImageDirty (inImage.me)
    # end PDirty

def PRead (inImage, err):
    """ Read an image  persistent (disk) form

    The data to be read is specified in the InfoList mamber
    Uses FArray member as buffer.
    inImage   = Python Image object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.ImageRead (inImage.me, err.me)
    if err.isErr:
        raise err
    # end PRead

def PWrite (inImage, err):
    """ Write an image  persistent (disk) form

    The data to be written is specified in the InfoList member
    Uses FArray member as buffer.
    inImage   = Python Image object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.ImageWrite (inImage.me, err.me)
    if err.isErr:
        raise err
    # end PWrite
    
def PReadFA (inImage, array, err):
    """ Read an image  persistent (disk) form to a specified FArray

    The data to be read is specified in the InfoList member
    inImage   = Python Image object
    array     = Python FArray to accept data
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not FArray.PIsA(array):
        raise TypeError,"array MUST be a Python Obit FArray"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.ImageReadFA (inImage.me, array.me, err.me)
    if err.isErr:
        raise err
    # end PReadFA

def PWriteFA (inImage, array, err):
    """ Write an image  persistent (disk) form from a specified FArray

    The data to be written is specified in the InfoList member
    inImage   = Python Image object
    array     = Python FArray to write
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not FArray.PIsA(array):
        raise TypeError,"array MUST be a Python Obit FArray"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.ImageWriteFA (inImage.me, array.me, err.me)
    if err.isErr:
        raise err
    # end PWriteFA

def PGetPlane (inImage, array, plane, err):
    """ Read an image  persistent (disk) form to an (optional) specified FArray

    The data to be read is specified in the InfoList member as modified by plane
    inImage   = Python Image object
    array     = Python FArray to accept data, if None use inImage buffer
    plane     = array of 5 integers giving (1-rel) pixel numbers
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not ((array == None) or FArray.PIsA(array)):
        raise TypeError,"array MUST be a Python Obit FArray or None"
    if len(plane) != 5:
        raise TypeError,"plane must have 5 integer elements"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    if array == None:
        tarray = inImage.FArray   # use image objects FArray
        larray = tarray.me
    else:
        larray = array.me
    Obit.ImageGetPlane (inImage.me, larray, plane, err.me)
    if err.isErr:
        raise err
    # end PGetPlane

def PPutPlane (inImage, array, plane, err):
    """ Write an image persistent (disk) form from an (optional) specified FArray

    The data to be written is specified in the InfoList member as modified by plane
    inImage   = Python Image object
    array     = Python FArray to provide data, if None use inImage buffer
    plane     = array of 5 integers giving (1-rel) pixel numbers
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not ((array == None) or FArray.PIsA(array)):
        raise TypeError,"array MUST be a Python Obit FArray or None"
    if len(plane) != 5:
        raise TypeError,"plane must have 5 integer elements"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    if array == None:
        tarray =  inImage.FArray  # use image objects FArray
        larray = tarray.me
    else:
        larray = array.me
    Obit.ImagePutPlane (inImage.me, larray, plane, err.me)
    if err.isErr:
        raise err
    # end PPutPlane

def PZapTable (inImage, tabType, tabVer, err):
    """ Destroy specified table

    inImage   = Python Image object
    tabType   = Table type, e.g. "AIPS CC"
    tabVer    = table version, integer
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.ImageZapTable(inImage.me, tabType, tabVer, err.me)
    if err.isErr:
        raise err
    # end PZapTable

def PCopyTables (inImage, outImage, exclude, include, err):
    """ Copy Tabeles from one image to another

    inImage   = Python Image object
    outImage  = Output Python Image object, must be defined
    exclude   = list of table types to exclude (list of strings)
                has priority
    include   = list of table types to include (list of strings)
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not PIsA(outImage):
        raise TypeError,"outImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.ImageCopyTables  (inImage.me, outImage.me, exclude, include, err.me)
    if err.isErr:
        raise err
    # end PCopyTables

def PUpdateTables (inImage, err):
    """ Update any disk resident structures about the current tables

    inImage   = Python Image object
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    Obit.ImageUpdateTables (inImage.me, err.me)
    if err.isErr:
        raise err
    # end PUpdateTables

def PFullInstantiate (inImage, access, err):
    """ Fully instantiate an Image by opening and closing

    return 0 on success, else failure
    inImage   = Python Image object
    access    = access code 1=READONLY, 2=WRITEONLY, 3=READWRITE
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.ImagefullInstantiate (inImage.me, access, err.me)
    if err.isErr:
        raise err
    return ret
    # end PFullInstantiate

def PFArray2Image (inArray, outImage, err):
    """ Attach an FArray to an image and write it

    Very rudimentary header attached
    inArray   = Python Image object
    outImage  = Python Image to write
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not FArray.PIsA(inArray):
        raise TypeError,"inArray MUST be a Python Obit FArray"
    if not PIsA(outImage):
        raise TypeError,"outImage MUST be a Python Obit Image"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    # Get array info
    naxis = inArray.Naxis[0:2]
    naxis = [naxis[0], naxis[1], 1, 1, 1, 1, 1]
    #    
    # Set size on image descriptor
    desc = ImageDesc.PDefault("temp")
    descDict = desc.Dict                      # Input Python dict object
    descDict["inaxes"] = naxis                # Update size
    descDict["crpix"]  = [1.0+naxis[0]*0.5, 1.0+naxis[1]*0.5, 1.0, 1.0, 1.0, 1.0, 1.0]
    descDict["bitpix"] = -32  # output floating
    descDict["object"] = "Temp"
    outImage.Desc.Dict = descDict
    #
    # Write output image
    outImage.Open(WRITEONLY, err)
    outImage.WriteFA(inArray, err)
    outImage.Close(err)
    #OErr.printErrMsg(err, "Error writing padded image for "+PGetName(outImage))
    # end PFArray2Image

def PFArray2FITS (inArray, outFile, err, outDisk=1, oDesc=None ):
    """ Write an FArray to a FITS image

    Very rudimentary header attached
    inArray   = Python Image object
    outFile   = Name of FITS file
    outDisk   = FITS disk number
    oDesc     = None or ImageDescriptor to be written
    err       = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not FArray.PIsA(inArray):
        raise TypeError,"inArray MUST be a Python Obit FArray"
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    # Create image
    outImage  = newObit(outFile, outFile, outDisk, 0, err)
    #OErr.printErrMsg(err, "Error creating FITS image "+outFile)
    #
    # Get array info
    naxis = inArray.Naxis[0:2]
    naxis = [naxis[0], naxis[1], 1, 1, 1, 1, 1]
    #
    # Set size on image descriptor
    if oDesc!=None:
        desc = oDesc
    else:
        desc = ImageDesc.PDefault("temp")
        descDict = desc.Dict                      # Input Python dict object
        descDict["inaxes"] = naxis                # Update size
        descDict["crpix"]  = [1.0+naxis[0]*0.5, 1.0+naxis[1]*0.5, 1.0, 1.0, 1.0, 1.0, 1.0]
        descDict["bitpix"] = -32  # output floating
        descDict["object"] = "Temp"
        #desc.Dict = descDict                      # Update descriptor
    outImage.Desc.Dict = descDict                 # Update descriptor on image
    #
    # Write output image
    outImage.Open(WRITEONLY, err)
    outImage.WriteFA(inArray, err)
    outImage.Close(err)
    #OErr.printErrMsg(err, "Error writing padded image for "+outFile)
    # end PFArray2FITS

def PGetList (inImage):
    """ Return the member InfoList

    returns InfoList
    inImage   = Python Image object
    """
    ################################################################
     # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    #
    out    = InfoList.InfoList()
    out.me = Obit.InfoListUnref(out.me)
    out.me = Obit.ImageGetList(inImage.me)
    return out
    # end PGetList

def PGetTableList (inImage):
    """ Return the member tableList

    returns tableList
    inUV   = Python UV object
    """
    ################################################################
     # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    #
    out    = TableList.TableList(PGetName(inImage))
    out.me = Obit.TableListUnref(out.me)
    out.me = Obit.ImageGetTableList(inImage.me)
    return out
    # end PGetTableList


def PGetDesc (inImage):
    """ Return the member ImageDesc

    returns ImageDesc as a Python Dictionary
    inImage   = Python Image object
    """
    ################################################################
     # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    #
    out    = ImageDesc.ImageDesc("None")
    out.me = Obit.ImageGetDesc(inImage.me)
    return out
    # end PGetDesc

def PGetFArray (inImage):
    """ Return FArray used to buffer Image data

    returns FArray with image pixel data
    inImage   = Python Image object
    """
    ################################################################
     # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    #
    out    = FArray.FArray("None")
    out.me = Obit.ImageGetFArray(inImage.me)
    return out
    # end PGetFArray

def PSetFArray (inImage, array):
    """ Replace the FArray on an Image

    inImage   = Python Image object
    array     = Python FArray to attach
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not FArray.PIsA(array):
        raise TypeError,"array MUST be a Python Obit FArray"
    #
    Obit.ImageSetFArray(inImage.me, array.me)
    # end PSetFArray

def PGetBeam (inImage):
    """ Return Beam attached to Image

    returns Beam with image pixel data
    inImage   = Python Image object
    """
    ################################################################
     # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    #
    out    = Image("None")
    out.me = Obit.ImageGetBeam(inImage.me)
    return out
    # end PGetBeam

def PSetBeam (inImage, beam):
    """ Replace the Beam attached to an Image

    inImage   = Python Image object
    beam      = Python Beam Image to attach
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    if not PIsA(beam):
        raise TypeError,"array MUST be a Python Obit Image"
    #
    Obit.ImageSetBeam(inImage.me, beam.me)
    # end PSetBeam

def PGetHighVer (inImage, tabType):
    """ Get highest version number of a specified Table

    returns highest tabType version number, 0 if none.
    inImage   = Python Image object
    tabType   = Table type, e.g. "OTFSoln"
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    #
    return Obit.ImageGetHighVer(inImage.me, tabType)
    # end PGetHighVer

def PIsScratch (inImage):
    """ Tells if Image is a scratch object

    return true, false (1,0)
    inImage   = Python Image object
    """
    ################################################################
    # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    return Obit.ImageisScratch(inImage.me)
    # end PIsScratch

def PIsA (inImage):
    """ Tells if input really a Python Obit Image

    return true, false (1,0)
    inImage   = Python Image object
    """
    ################################################################
     # Checks
    if inImage.__class__ != Image:
        print "Class actually is",inImage.__class__
        return 0
    return Obit.ImageIsA(inImage.me)
    # end PIsA

def PUnref (inImage):
    """ Decrement reference count

    Decrement reference count which will destroy object if it goes to zero
    Python object stays defined.
    inImage   = Python Image object
    """
    ################################################################
     # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"

    inImage.me = Obit.ImageUnref(inImage.me)
    # end PUnref

def PGetName (inImage):
    """ Tells Image object name (label)

    returns name as character string
    inImage   = Python Image object
    """
    ################################################################
     # Checks
    if not PIsA(inImage):
        raise TypeError,"inImage MUST be a Python Obit Image"
    #
    return Obit.ImageGetName(inImage.me)
    # end PGetName
