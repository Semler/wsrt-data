/* $Id: FArray.inc,v 1.12 2005/03/05 14:41:40 bcotton Exp $           */  
/*--------------------------------------------------------------------*/
/* Swig module description for ObitFarray type                        */
/*                                                                    */
/*;  Copyright (C) 2004                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

%{

#include "ObitFArray.h"
%}


%inline %{
extern ObitFArray* FArrayCreate(char* name, long ndim, long *naxis) {
   return  ObitFArrayCreate (name, ndim, naxis);
}

extern float FArrayGetVal(ObitFArray* in, long *pos) {
   float out[4];
   long *Iout;
   out[0] = *(ObitFArrayIndex (in, pos));
   // return NaN rather than Obit magic value
   if (out[0]==ObitMagicF()) {  // create a word with all bits on
        Iout = (long*)out;
        *Iout = ~0;
   }
   return out[0];
}

extern void FArraySetVal(ObitFArray* in, long *pos, float val) {
   float *off;
   off = ObitFArrayIndex (in, pos);
   *off = val;
}

extern ObitFArray* FArrayCopy  (ObitFArray *in, ObitFArray *out, ObitErr *err) {
  return ObitFArrayCopy (in, out, err);
} // end FArrayCopy 

extern ObitFArray* FArrayClone  (ObitFArray *in, ObitFArray *out, ObitErr *err) {
  ObitFArrayClone (in, out, err);
  return out;
} // end FArrayCopy 

extern ObitFArray* FArraySubArr  (ObitFArray *in, long *blc, long *trc, 
				  ObitErr *err) {
  return ObitFArraySubArr (in, blc, trc, err);
} // end FArraySubAr

extern int FArrayIsCompatable  (ObitFArray *in1, ObitFArray *in2) {
  return ObitFArrayIsCompatable(in1, in2);
}

extern ObitFArray* FArrayRealloc (ObitFArray* in, long ndim, long *naxis) {
   return  ObitFArrayRealloc(in, ndim, naxis);
}

//extern float* FArrayIndex (ObitFArray* in, long *pos) {
//   return ObitFArrayIndex (in, pos);
//}

extern float FArrayMax (ObitFArray* in, long *outValue2) {
   return ObitFArrayMax(in, outValue2);
}

extern float FArrayMaxAbs (ObitFArray* in, long *outValue2) {
   return ObitFArrayMaxAbs(in, outValue2);
}

extern float FArrayMin (ObitFArray* in, long *outValue2) {
   return ObitFArrayMin(in, outValue2);
}

extern void FArrayDeblank (ObitFArray* in, float scalar) {
   ObitFArrayDeblank (in, scalar);
}

extern float FArrayRMS (ObitFArray* in) {
   float out[4];
   long *Iout;
   out[0] = ObitFArrayRMS(in);
   // return NaN rather than Obit magic value
    if (out[0]==ObitMagicF()) {  // create a word with all bits on
        Iout = (long*)&out;
        *Iout = ~0;
   }
   return out[0];
}

extern float FArrayMode (ObitFArray* in) {
   float out[4];
   long *Iout;
   out[0] = ObitFArrayMode(in);
   // return NaN rather than Obit magic value
   if (out[0]==ObitMagicF()) {  // create a word with all bits on
        Iout = (long*)&out;
        *Iout = ~0;
   }
   return out[0];
}

extern float FArrayMean (ObitFArray* in) {
   float out[4];
   long *Iout;
   out[0] = ObitFArrayMean(in);
   // return NaN rather than Obit magic value
   if (out[0]==ObitMagicF()) {  // create a word with all bits on
        Iout = (long*)&out;
        *Iout = ~0;
   }
   return out[0];
}

extern void FArrayFill (ObitFArray* in, float scalar) {
   return ObitFArrayFill(in, scalar);
}

extern void FArrayNeg (ObitFArray* in) {
   ObitFArrayNeg(in);
}

extern float FArraySum (ObitFArray* in) {
   return ObitFArraySum(in);
}

extern long FArrayCount (ObitFArray* in) {
   return ObitFArrayCount(in);
}

extern void FArraySAdd (ObitFArray* in, float scalar) {
   ObitFArraySAdd(in, scalar);
}

extern void FArraySMul (ObitFArray* in, float scalar) {
   ObitFArraySMul(in, scalar);
}

extern void FArraySDiv (ObitFArray* in, float scalar) {
   ObitFArraySDiv(in, scalar);
}

extern void FArrayClip (ObitFArray* in, float minVal,float maxVal, float newVal) {
   ObitFArrayClip(in, minVal, maxVal, newVal);
}

extern void FArrayDivClip (ObitFArray* in1, ObitFArray* in2, float minVal, 
	ObitFArray* out) {
   ObitFArrayDivClip(in1, in2, minVal, out);
}

extern void FArrayClipBlank (ObitFArray* in, float minVal,float maxVal) {
   gfloat fblank = ObitMagicF();
   ObitFArrayClip(in, minVal, maxVal, fblank);
}

extern void FArrayBlank (ObitFArray* in1, ObitFArray* in2, ObitFArray* out) {
   ObitFArrayBlank (in1, in2, out);
}

extern void FArrayAdd (ObitFArray* in1, ObitFArray* in2, ObitFArray* out) {
   ObitFArrayAdd (in1, in2, out);
}

extern void FArraySub (ObitFArray* in1, ObitFArray* in2, ObitFArray* out) {
   ObitFArraySub (in1, in2, out);
}

extern void FArrayMul (ObitFArray* in1, ObitFArray* in2, ObitFArray* out) {
   ObitFArrayMul (in1, in2, out);
}

extern void FArrayDiv (ObitFArray* in1, ObitFArray* in2, ObitFArray* out) {
   ObitFArrayDiv (in1, in2, out);
}

extern float FArrayDot (ObitFArray* in1, ObitFArray* in2) {
	return ObitFArrayDot(in1, in2);
}

extern void FArrayMulColRow (ObitFArray* in, ObitFArray* row, ObitFArray* col,
				  ObitFArray* out) {
   ObitFArrayMulColRow (in, row, col, out);
}

extern void FArray2DCenter (ObitFArray* in) {
   ObitFArray2DCenter (in);
}

extern int FArraySymInv2D (ObitFArray* in) {
   int ierr;
   ObitFArray2DSymInv (in, &ierr);
   return ierr;
}

extern void FArrayCGauss2D (ObitFArray* in, long Cen[2], float FWHM) {
   ObitFArray2DCGauss (in, Cen, FWHM);
}

extern void FArrayEGauss2D (ObitFArray* in, float amp, float Cen[2], float GauMod[3]) {
   ObitFArray2DEGauss (in, amp, Cen, GauMod);
}

extern void FArrayShiftAdd (ObitFArray* in1, long *pos1,
				 ObitFArray* in2, long *pos2, 
				 float scalar, ObitFArray* out) {
   ObitFArrayShiftAdd (in1, pos1, in2, pos2, scalar, out);
} // end FArrayShiftAdd

extern void FArrayPad (ObitFArray* in, ObitFArray* out, float factor) {
   ObitFArrayPad (in, out, factor);
} // end FArrayPad

extern char* FArrayGetName (ObitFArray* in) {
  return in->name;
} // end  FArrayGetName

extern long FArrayGetNdim (ObitFArray* in) {
  return in->ndim;
} // end  FArrayGetNdim

// returns an array of 7 elements no matter
extern long* FArrayGetNaxis (ObitFArray* in) {
  return in->naxis;
} // end  FArrayGetNaxis

extern int FArrayIsA (ObitFArray* in) {
  return ObitFArrayIsA(in);
} // end  FArrayIsA 

ObitFArray* FArrayRef (ObitFArray* in) {
  return ObitFArrayRef (in);
} // end FArrayRef

ObitFArray* FArrayUnref (ObitFArray* in) {
  if (!ObitFArrayIsA(in)) return NULL;
  if (in && (in->ReferenceCount>0)) in = ObitFArrayUnref (in);
  return in;
} // end FArrayUnref

%}

/* Definitions for Python Shadow class */
/* A copy of the struct for c */
%{
typedef struct {
  ObitFArray *me;
} FArray;
%}
/* and a copy of the struct for swig */
typedef struct {
  ObitFArray *me;
} FArray;

%addmethods FArray { 
  FArray(char* name, long ndim, long *naxis) {
     FArray *out;
     out = (FArray *) malloc(sizeof(FArray));
     if (strcmp(name, "None")) out->me = FArrayCreate(name, ndim, naxis);
     else  out->me = NULL;
     return out;
   }
  ~FArray() {
    self->me = FArrayUnref(self->me);
    free(self);
  }
};

