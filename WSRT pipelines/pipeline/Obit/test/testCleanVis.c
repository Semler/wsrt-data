#include <stdio.h>
#include <stdlib.h>
#include "ObitSystem.h"
#include "ObitUV.h"
#include "ObitImage.h"
#include "ObitImageUtil.h"
#include "ObitImageMosaic.h"
#include "ObitUVImager.h"
#include "ObitDConCleanVis.h"

/* program to test CleanVis functionality */
int main ( int argc, char **argv )
{
  ObitSystem *mySystem;
  ObitErr *err;
  gint ierr, dim[MAXINFOELEMDIM] = {1,1,1,1,1};
  gchar *AIPSdir[] = {"../AIPSdata/"};
  gchar *FITSdir[] = {"../testIt/"};
  ObitUV *uvdata=NULL;
  ObitImage *fullField=NULL, *outImage=NULL;
  ObitDConCleanVis *myClean=NULL;
  /* data */
  gint inDisk = 1;
  /*gchar *inFile = "UVImageTestIn.uvtab";*/
  /*gchar *inFile = "WideField20UC.uvtab";*/
  gchar *inFile = "WideField20.uvtab";
  /* gchar *inFile = "WideField20Model1.uvtab";*/
  gchar *sources[] = {"C346R424"};
  gint user=103;
  ObitIOType type = OBIT_IO_FITS;  /* FOR FITS */
  /* ObitIOType type = OBIT_IO_AIPS;  FOR AIPS */
  /* gint Adisk=1, Acno;
     gchar Aname[13] = {"WideField20 "};
     gchar Aclass[7] = {"uvdata"};
     gchar Atype[3] = {"UV"};
     gint  Aseq = 2; */
  gint outDisk  = 1;
  gchar *outFile = "!testCleanVisOutC.fits";
 /*  gint  masterDisk = 1;
     gchar *masterFile  = "CleanVisMaster.fits";
     gint i;
     gfloat bmaj, bmin, bpa; */

  /* Imaging Control */
  gchar *Stokes="I   ";          /* Stokes parameter to image */
  gfloat FOV = 0.4167;           /* Field of View 25/60 = 25' */
  /*gfloat FOV = 0.1; Small for testing */
  gfloat UVRange[] = {0.0,0.0};   /* range in uv plane in klambda */
  gfloat TimeRange[] = {0.0,10.0};/* Time range in days */
  gfloat Robust = 0.0;            /* Briggs robustness */
  gfloat UVTaper[] = {0.0,0.0};   /* UV plane taper in klambda */
  gboolean doCalSelect = TRUE;    /* Edit/calibrate data? */
  gint docalib = 2;               /* Calibrate data? 2=> cal wt. */
  /* gint docalib = -1;               Calibrate data? 2=> cal wt. */
  gint gainuse = 2;               /* CL table to apply */
  gboolean doFull = TRUE;         /* do full field image? */
  gboolean doBeam = TRUE;         /* Calculate beam? */
  gboolean autoWin = TRUE;        /* automatic windowing? */

  /* Outlyer */
  gchar *Catalog = "NVSSVZ.FIT";
  gfloat OutlierDist = 1.0;   /* Maximum distance to add outlyers (deg) */
  gfloat OutlierFlux = 0.001; /* Minimum estimated outlier flux density (Jy) */
  gfloat OutlierSI   = -0.7;  /* Spectral index to estimate flux density */
  gint   OutlierSize = 50;    /* Size of outlyer field */
  gboolean Tr=TRUE, Fl=FALSE;
  gchar *name = "TstName", *class = "Class";
  gint seq=1;

  /* Clean parameters */
  gint niter    = 1000;  /* Number of CLEAN components */
  /* gint niter    = 1;  Number of CLEAN components */
  gint minpatch = 200;   /* Minimum beam patch */
  gint ccver    = 1;     /* CC table version */
  gfloat minflux=0.0001; /* Minimum clean flux density */
  gfloat gain   = 0.1;   /* CLEAN loop gain */
  /* gfloat gain  = 1.0;    CLEAN loop gain */
  

  /* Initialize Obit */
  err = newObitErr();
  ierr = 0;
  mySystem = ObitSystemStartup ("testCleanVis", 1, user, 1, AIPSdir, 1, FITSdir, 
				(oint)Tr, (oint)Fl, err);
  ObitErrLog(err); /* show any error messages on err */

  /* Create basic uv data */
  uvdata = newObitUV("input FITS UV data");

  /* setup input */
  /* FOR FITS  */
  ObitUVSetFITS(uvdata, 1000, inDisk, inFile, err);

   /* FOR AIPS
      Adisk = 1;
      Acno = ObitAIPSDirFindCNO(Adisk, user, Aname, Aclass, Atype, Aseq, err);
      if (Acno<0) Obit_log_error(err, OBIT_Error, 
      "Failure looking up input uvdata file");
      ObitUVSetAIPS(uvdata,1000,Adisk,Acno,user,err); */
  /* show any errors */
  if (err->error) ierr = 1;   ObitErrLog(err);  if (ierr!=0) return ierr;
  

 /* Set parameters on uvdata */
  dim[0] = dim[1] = 1;
  ObitInfoListAlwaysPut(uvdata->info, "doCalSelect", OBIT_bool,   dim, &doCalSelect);
  ObitInfoListAlwaysPut(uvdata->info, "doCalib",     OBIT_int,    dim, &docalib);
  ObitInfoListAlwaysPut(uvdata->info, "gainUse",     OBIT_int,    dim, &gainuse);
  ObitInfoListAlwaysPut(uvdata->info, "doBeam",      OBIT_bool,   dim, &doBeam);
  ObitInfoListAlwaysPut(uvdata->info, "imFileType",  OBIT_int,    dim, &type);
  ObitInfoListAlwaysPut(uvdata->info, "imSeq",       OBIT_int,    dim, &seq);
  ObitInfoListAlwaysPut(uvdata->info, "imDisk",      OBIT_int,    dim, &inDisk);
  ObitInfoListAlwaysPut(uvdata->info, "FOV",         OBIT_float,  dim, &FOV);
  ObitInfoListAlwaysPut(uvdata->info, "doFull",      OBIT_bool,   dim, &doFull);
  ObitInfoListAlwaysPut(uvdata->info, "OutlierDist", OBIT_float,  dim, &OutlierDist);
  ObitInfoListAlwaysPut(uvdata->info, "OutlierFlux", OBIT_float,  dim, &OutlierFlux);
  ObitInfoListAlwaysPut(uvdata->info, "OutlierSI",   OBIT_float,  dim, &OutlierSI);
  ObitInfoListAlwaysPut(uvdata->info, "OutlierSize", OBIT_int,    dim, &OutlierSize);
  ObitInfoListAlwaysPut(uvdata->info, "Robust",      OBIT_float,  dim, &Robust);
  dim[0] = 4;
  ObitInfoListAlwaysPut(uvdata->info, "Stokes",      OBIT_string, dim, Stokes);
  dim[0] = 12;
  ObitInfoListAlwaysPut(uvdata->info, "imName",        OBIT_string, dim, name);
  dim[0] = 6;
  ObitInfoListAlwaysPut(uvdata->info, "imClass",       OBIT_string, dim, class);
  dim[0] = strlen(Catalog);
  ObitInfoListAlwaysPut(uvdata->info, "Catalog",     OBIT_string, dim, Catalog);
  dim[0] = 2;
  ObitInfoListAlwaysPut(uvdata->info, "UVRange",     OBIT_float,  dim, UVRange);
  dim[0] = 2;
  ObitInfoListAlwaysPut(uvdata->info, "UVTaper",     OBIT_float,  dim, UVTaper);
  dim[0] = 2;
  ObitInfoListAlwaysPut(uvdata->info, "timeRange",   OBIT_float,  dim, TimeRange);
  dim[0] = 16;
  ObitInfoListAlwaysPut(uvdata->info, "Sources",      OBIT_string, dim, sources[0]);
  dim[0] = dim[1] = 1;
  ObitInfoListAlwaysPut(uvdata->info, "PBCor", OBIT_bool,   dim, &Tr);

  /* Make CleanVis */
  myClean = ObitDConCleanVisCreate("Clean Object", uvdata, err);
  if (err->error) ierr = 1; ObitErrLog(err); if (ierr!=0) return ierr;

  /* CLEAN parameters */
  dim[0] = dim[1] = 1;
  ObitInfoListAlwaysPut(myClean->info, "doRestore", OBIT_bool,dim, &Tr);
  ObitInfoListAlwaysPut(myClean->info, "doFlatten", OBIT_bool,dim, &Tr);
  ObitInfoListAlwaysPut(myClean->info, "Niter",     OBIT_int, dim, &niter);
  ObitInfoListAlwaysPut(myClean->info, "Gain",      OBIT_float, dim, &gain);
  ObitInfoListAlwaysPut(myClean->info, "minPatch",  OBIT_int, dim, &minpatch);
  ObitInfoListAlwaysPut(myClean->info, "CCVer",     OBIT_int, dim, &ccver);
  ObitInfoListAlwaysPut(myClean->info, "minFlux",   OBIT_float,dim, &minflux);
  ObitInfoListAlwaysPut(myClean->info, "autoWindow",OBIT_bool, dim, &autoWin);

  /* Clean */
  ObitDConCleanVisDeconvolve ((ObitDCon*)myClean, err);
  if (err->error) ierr = 1; ObitErrLog(err); if (ierr!=0) return ierr;

  /* Quantize output */
  fullField = ObitImageMosaicGetFullImage (myClean->mosaic, err);
  outImage  = ObitImageUtilQuanFITS(fullField, outFile, outDisk, 0.20, err);
  if (err->error) ierr = 1;   ObitErrLog(err);  if (ierr!=0) return ierr;

  /* Shutdown Obit */
  mySystem = ObitSystemShutdown (mySystem);
  
  return 0;
} /* end of main */

