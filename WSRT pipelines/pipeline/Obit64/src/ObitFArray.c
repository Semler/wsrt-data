/* $Id: ObitFArray.c,v 1.16 2005/08/11 22:49:36 bcotton Exp $         */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "ObitFArray.h"
#include "ObitMem.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitFArray.c
 * ObitFArray class function definitions.
 * This class is derived from the Obit base class.
 */

/** name of the class defined in this file */
static gchar *myClassName = "ObitFArray";

/**
 * ClassInfo structure ObitFArrayClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitFArrayClassInfo myClassInfo = {FALSE};

/*--------------- File Global Variables  ----------------*/


/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitFArrayInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitFArrayClear (gpointer in);

/*----------------------Public functions---------------------------*/
/**
 * Constructor.
 * Initializes class if needed on first call.
 * \param name An optional name for the object.
 * \return the new object.
 */
ObitFArray* newObitFArray (gchar* name)
{
  ObitFArray* out;

  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitFArrayClassInit();

  /* allocate/init structure */
  out = ObitMemAlloc0Name (sizeof(ObitFArray), "ObitFArray");

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

  /* set ClassInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  ObitFArrayInit((gpointer)out);

 return out;
} /* end newObitFArray */

/**
 * Returns ClassInfo pointer for the class.
 * \return pointer to the class structure.
 */
gconstpointer ObitFArrayGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitFArrayClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitFArrayGetClass */

/**
 * Make a deep copy of an ObitFArray.
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \param err Obit error stack object.
 * \return pointer to the new object.
 */
ObitFArray* ObitFArrayCopy  (ObitFArray *in, ObitFArray *out, ObitErr *err)
{
  const ObitClassInfo *ParentClass;
  gboolean oldExist;
  gchar *outName;
  glong i;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return out;
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));

  /* Create if it doesn't exist */
  oldExist = out!=NULL;
  if (!oldExist) {
    /* derive object name */
    outName = g_strconcat ("Copy: ",in->name,NULL);
    out = newObitFArray(outName);
    g_free(outName);
  }

  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);

  /*  copy this class */

  /* arrays */
  out = ObitFArrayRealloc (out, in->ndim, in->naxis);
  /* copy data */
  for (i=0; i<in->arraySize; i++) out->array[i] = in->array[i];

  return out;
} /* end ObitFArrayCopy */

/**
 * Make a copy of a object but do not copy the actual data
 * This is useful to create an FArray similar to the input one.
 * \param in  The object to copy
 * \param out An existing object pointer for output, must be defined.
 * \param err Obit error stack object.
 */
void ObitFArrayClone  (ObitFArray *in, ObitFArray *out, ObitErr *err)
{
  const ObitClassInfo *ParentClass;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitIsA(out, &myClassInfo));

  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);

  /*  copy this class */

  /* arrays */
  out = ObitFArrayRealloc (out, in->ndim, in->naxis);

} /* end ObitFArrayClone */

/**
 * Determine if the two input objects have a compatable geometry.
 * Must have same number of non degenerate dimensions and 
 * each dimension must be the same size.
 * \param in1  First object to test.
 * \param in2  Second object to test.
 * \return TRUE if compatable, else FALSE.
 */
gboolean ObitFArrayIsCompatable (ObitFArray *in1, ObitFArray *in2)
{
  glong i, ndim;

  /* test fails if either input is NULL */
  if (in1==NULL) return FALSE;
  if (in2==NULL) return FALSE;

  /* Same number of non degenerate dimensions? */
  ndim = in1->ndim;
  if (in1->ndim!=in2->ndim) {
    /* Don't bother if extra dimensions are length 1 */
    if (in1->ndim>in2->ndim) {  /* in1 has more */
      ndim = in2->ndim;
      for (i=in2->ndim; i<in1->ndim; i++)
	if (in1->naxis[i]>1) return FALSE;
    } else {  /* in2 has more */
      ndim = in1->ndim;
      for (i=in1->ndim; i<in2->ndim; i++)
	if (in2->naxis[i]>1) return FALSE;
    }
  }  /* end dimension check */

  /* check dimensions */
  for (i=0; i<ndim; i++) if (in1->naxis[i]!=in2->naxis[i]) return FALSE;

  return TRUE; /* must be OK */
} /* end ObitFArrayIsCompatable  */

/**
 * Creates an ObitFArray of a specified geometry.
 * \param name  An optional name for the object.
 * \param ndim  Number of dimensions desired, if <=0 data array not created.
 *              maximum value = MAXFARRAYDIM.
 * \param naxis Dimensionality along each axis. NULL => don't create array.
 * \return the new object.
 */
ObitFArray* ObitFArrayCreate (gchar* name, glong ndim, glong *naxis)
{
  ObitFArray* out;
  glong i, size;

  /* Create basic structure */
  out = newObitFArray (name);

  /* create data array if wanted */
  if ((ndim<0) || (naxis==NULL)) return out;
  g_assert (ndim<=MAXFARRAYDIM); /* sanity check */

  /* copy geometry */
  out->ndim = ndim;
  out->naxis = ObitMemAlloc0Name (ndim*sizeof(glong), "FArray naxis");
  size = 1;
  for (i=0; i<ndim; i++) {
    out->naxis[i] = MAX (1, naxis[i]);
    size *= out->naxis[i]; /* total size */
  }

  /* create array - add a bit extra, FFT seems to need it */
  out->array = ObitMemAlloc0Name (size*sizeof(gfloat) + 
				  out->naxis[0]*sizeof(gfloat),
				  "FArray array");
  out->arraySize = size;

  return out;
} /* end ObitFArrayCreate */

/**
 * Creates an ObitFArray of the specified subarray size of an extant
 * ObitFArray and copy the values.
 * \param in    Object with structures to subarray.
 * \param blc   (0-rel) lower index of first pixel to copy
 * \param trc   (0-rel) lower index of highest pixel to copy
 * \param err   Obit error stack object.
 * \return the new object.
 */
ObitFArray* ObitFArraySubArr (ObitFArray *in, glong *blc, glong *trc, 
			       ObitErr *err)
{
  glong ndim, idim, i1, i2, i3, i4, i5, i6, i7, i8, i9, i10;
  glong naxis[MAXFARRAYDIM], ipos[MAXFARRAYDIM], opos[MAXFARRAYDIM];
  ObitFArray *out=NULL;
  gchar *outName;
  gfloat *inp, *outp;

   /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return out;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (blc != NULL);
  g_assert (trc != NULL);

  /* Get size */
  ndim = in->ndim;
  for (idim=0; idim<MAXFARRAYDIM; idim++) naxis[idim] = 1;
  for (idim=0; idim<ndim; idim++) naxis[idim] = trc[idim]-blc[idim]+1;

  /* derive object name */
  outName = g_strconcat ("Subarray: ",in->name, NULL);
    
  /* Create output */
  out = ObitFArrayCreate (outName, ndim, naxis);
  g_free(outName); outName = NULL;

  /* Copy, loop over all possible dimensions */
  for (idim=0; idim<MAXFARRAYDIM; idim++) ipos[idim] = 0;
  for (i10=0; i10<naxis[9]; i10++) {
    opos[9] = i10;
    if(ndim>9) ipos[9] = blc[9] + i10;
    for (i9=0; i9<naxis[8]; i9++) {
      opos[8] = i9;
      if(ndim>8) ipos[8] = blc[8] + i9;
      for (i8=0; i8<naxis[7]; i8++) {
	opos[7] = i8;
	if(ndim>7) ipos[7] = blc[7] + i8;
	for (i7=0; i7<naxis[6]; i7++) {
	  opos[6] = i7;
	  if(ndim>6) ipos[6] = blc[6] + i7;
	  for (i6=0; i6<naxis[5]; i6++) {
	    opos[5] = i6;
	    if(ndim>5) ipos[5] = blc[5] + i6;
	    for (i5=0; i5<naxis[4]; i5++) {
	      opos[4] = i5;
	      if(ndim>4) ipos[4] = blc[4] + i5;
	      for (i4=0; i4<naxis[3]; i4++) {
		opos[3] = i4;
		if(ndim>3) ipos[3] = blc[3] + i4;
		for (i3=0; i3<naxis[2]; i3++) {
		  opos[2] = i3;
		  if(ndim>2) ipos[2] = blc[2] + i3;
		  for (i2=0; i2<naxis[1]; i2++) {
		    opos[1] = i2;
		    if(ndim>1) ipos[1] = blc[1] + i2;
		    opos[0] = 0;
		    ipos[0] = blc[0];
		    
		    /* Array pointers */
		    inp  = ObitFArrayIndex (in,  ipos);
		    outp = ObitFArrayIndex (out, opos);

		    /* Copy row */
		    for (i1=0; i1<naxis[0]; i1++) {
		      *outp++ = *inp++;
		    }
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
  
  return out;
} /* end  ObitFArraySubArr */

/**
 * Reallocate memory if needed, zero memory.
 * \param in    Object with structures to reallocate.
 * \param ndim  Number of dimensions desired, if <=0 data array not created.
 *              maximum value = #MAXFARRAYDIM.
 * \param naxis Dimensionality along each axis. NULL => don't create array.
 * \return the resized object.
 */
ObitFArray* ObitFArrayRealloc (ObitFArray* in, glong ndim, glong *naxis)
{
  ObitFArray* out = in;
  glong i, size;

   /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ndim<=MAXFARRAYDIM); /* sanity check */

  /* just deallocate old? */
  if ((ndim<0) || (naxis==NULL)) {
    /* just deallocate old */
    if (out->array) out->array = ObitMemFree(out->array);
    out->arraySize = 0;
    if (out->naxis) out->naxis = ObitMemFree(out->naxis);
    out->ndim = 0;
    return out;
  }
 
  /* different geometry? */
  if (out->ndim != ndim) {
    out->ndim = ndim;
    if (out->naxis!=NULL) out->naxis = ObitMemFree(out->naxis);
    out->naxis = ObitMemAlloc0Name(ndim*sizeof(glong), "FArray naxis");
  }

  /* set dimensions, find output size */
  size = 1;
  for (i=0; i<ndim; i++) {
    out->naxis[i] = MAX (1, naxis[i]);
    size *= out->naxis[i]; /* total size */
  }

  /* resize array if needed */
  if (size != out->arraySize) {
    out->array = ObitMemRealloc(out->array, 
			   size*sizeof(gfloat)+out->naxis[0]*sizeof(gfloat));
    out->arraySize = size;
  }

  /* zero fill memory */
  memset (out->array, 0, size*sizeof(gfloat));

  return out;
} /* end ObitFArrayRealloc */

/**
 * Calculate offset for a given pixel location and return pointer.
 * Subsequent data are stored in order of increasing dimension 
 * (rows, then columns...).
 * \param in      Object with data
 * \param pos     array of 0-rel pixel numbers on each axis
 * \return pointer to specified cell; NULL if illegal pixel.
 */
gfloat*  ObitFArrayIndex (ObitFArray *in, glong *pos)
{
  gfloat *out = NULL;
  glong i, indx, previous;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (pos != NULL);
  g_assert (in->array != NULL);
  g_assert (in->naxis != NULL);

  /* Calculate offset */
  previous = 1;  /* size of previous dimension */
  indx = 0;
  for (i=0; i<in->ndim; i++) {
    /* check legality */
    if ((pos[i]<0) || (pos[i]>=in->naxis[i])) return NULL;
    indx += pos[i] * previous;
    previous *= in->naxis[i];
  }

  /* pointer to array */
  out = in->array + indx;

  return out;
} /* end ObitFArrayIndex  */

/**
 * Find maximum pixel value.
 * Return value and location in pos.
 * \param in      Object with data
 * \param pos     (out) array of 0-rel pixel numbers on each axis
 * \return maximum value.
 */
gfloat ObitFArrayMax (ObitFArray *in, glong *pos)
{
  glong i, temp, maxCell;
  gfloat maxVal, fblank = ObitMagicF();

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (pos != NULL);

  /* Loop over array */
  maxCell = -1;
  maxVal = -1.0E25;
  for (i=0; i<in->arraySize; i++) 
    {
      if ((in->array[i]!=fblank) && (in->array[i]>maxVal)) {
	maxCell = i;
	maxVal = in->array[i];
      }
    }

  /* Convert cell to index */
  temp = maxCell;
  for (i=0; i<in->ndim; i++) {
    pos[i] = temp % in->naxis[i];
    temp = (temp - pos[i]) / in->naxis[i];
  }

  return maxVal;
} /* end  ObitFArrayMax */

/**
 * Find maximum pixel value.
 * Return value and location in pos.
 * \param in      Object with data
 * \param pos     (out) array of 0-rel pixel numbers on each axis
 * \return maximum absolute (signed) value.
 */
gfloat ObitFArrayMaxAbs (ObitFArray *in, glong *pos)
{
  glong i, temp, maxCell;
  gfloat maxAVal, maxVal, *data, val, fblank = ObitMagicF();

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (pos != NULL);

  /* Loop over array */
  maxCell = -1;
  maxAVal = -1.0E25;
  maxVal = 0.0;
  data = in->array;
  for (i=0; i<in->arraySize; i++) 
    {
      val = data[i];
      if ((val!=fblank) && (fabs(val)>maxAVal)) {
	maxCell = i;
	maxAVal = fabs(val);
	maxVal  = val;
      }
    }

  /* Convert cell to index */
  temp = maxCell;
  for (i=0; i<in->ndim; i++) {
    pos[i] = temp % in->naxis[i];
    temp = (temp - pos[i]) / in->naxis[i];
  }

  return maxVal;
} /* end  ObitFArrayMaxAbs */

/**
 * Find minimum pixel value.
 * Return value and location in pos.
 * \param in      Object with data
 * \param pos     (out) array of 0-rel pixel numbers on each axis
 * \return minimum value.
 */
gfloat ObitFArrayMin (ObitFArray *in, glong *pos)
{
  glong i, temp, minCell;
  gfloat minVal, fblank = ObitMagicF();

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (pos != NULL);

  /* Loop over array */
  minCell = -1;
  minVal = 1.0E25;
  for (i=0; i<in->arraySize; i++) 
    {
      if ((in->array[i]!=fblank) && (in->array[i]<minVal)) {
	minCell = i;
	minVal = in->array[i];
      }
    }

  /* Convert cell to index */
  temp = minCell;
  for (i=0; i<in->ndim; i++) {
    pos[i] = temp % in->naxis[i];
    temp = (temp - pos[i]) / in->naxis[i];
  }

  return minVal;
} /* end  ObitFArrayMin */

/**
 * replace any magic value blanks with scalar
 * \param in      Object with data to deblank
 * \param scalar  Value to replace blanks.
 */
void ObitFArrayDeblank (ObitFArray *in, gfloat scalar)
{
  glong i;
  gfloat fblank = ObitMagicF();

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* Loop over array */
  for (i=0; i<in->arraySize; i++) 
      if (in->array[i]==fblank)in->array[i] = scalar;

} /* end  ObitFArrayDeblank */

/**
 *  Determine RMS noise in array.
 *  Value is based on a histogram analysis and is determined from 
 *  the width of the peak around the mode.
 *  out =  RMS (in.)
 * \param in Input object with data
 * \return rms of element distribution (-1 on error)
 */
gfloat ObitFArrayRMS (ObitFArray* in)
{
  glong i, icell, modeCell=0, imHalf=0, ipHalf=0, numCell, pos[MAXFARRAYDIM];
  glong i1, i2, ic, infcount;
  gfloat amax, amin, omax, omin, tmax, sum, sum2, x, count, mean, arg, cellFact=1.0;
  gfloat half, *histo = NULL;
  gfloat rawRMS, out = -1.0, fblank = ObitMagicF();
  gboolean done = FALSE;

   /* error checks */
  g_assert (ObitFArrayIsA(in));
  g_assert (in->array != NULL);

  /* get max/min */
  omax = ObitFArrayMax (in, pos);
  omin = ObitFArrayMin (in, pos);
  amax = omax;
  amin = omin;

  /* How many valid values? */
  count = 0; sum = sum2 = 0.0; 
  for (i=0; i<in->arraySize; i++) if (in->array[i]!=fblank) {
    count++;
    sum  += in->array[i];
    sum2 += in->array[i] * in->array[i];
  }
  if (count<5) return out; /* better have some */

  /* Get raw RMS */
  rawRMS = (sum2/count) - ((sum / count) * (sum / count));
  if (rawRMS>0.0) rawRMS = sqrt(rawRMS);
  if (rawRMS<(1.0e-5*MAX (fabs(omax), fabs(omin)))) return rawRMS;

  /* Make histogram size such that the average cell has 30 entries */
  numCell = count / 30;
  numCell = MAX (100, numCell);  /* but not too few */
  histo = ObitMemAllocName(numCell*sizeof(gfloat), "FArray histo");

 /* Loop until a reasonable number of values in peak of histogram */
  infcount = 0;  /* Loop to check for endless loop */
  while (!done) {

    /* Don't do this forever */
    infcount++;
    if (infcount>20) 
      {if (histo) ObitMemFree(histo);return rawRMS;}  /* bag it */
    
    /* form histogram */
    cellFact =  numCell / (amax - amin + 1.0e-20);
    for (i=0; i<numCell; i++) histo[i] = 0.0;
    for (i=0; i<in->arraySize; i++) {
      if (in->array[i]!=fblank){
	icell = 0.5 + cellFact * (in->array[i]-amin);
	icell = MIN (numCell-1, MAX(0, icell));
	histo[icell]++;
      }
    }
    
    /* Find mode cell */
    modeCell = -1;
    tmax = -1.0e20;
    for (i=1; i<numCell-1; i++) { /* ignore end cells */
      if (histo[i]>tmax) {
	tmax = histo[i];
	modeCell = i;
      }
    }
    
    
    /* find half width of peak by finding number of cells positive 
       and negative from the mode the distribution stays above tmax/2.
       If the data are quantized then some (many) cells may have zero 
       occuputation (or nearly so) and should be ignored.
    */
    imHalf = modeCell;
    for (i=modeCell; i>=1; i--) {
      if (histo[i]>0.5*tmax)  imHalf = i;
      /*else if (histo[i]>0.2*tmax) break;*/
    }
    ipHalf = modeCell;
    for (i=modeCell; i<numCell-1; i++) {
      if (histo[i]>0.5*tmax) ipHalf = i;
      /*else if (histo[i]>0.2*tmax) break;*/
    }

    /* acceptability tests */
    /* ipHalf - imHalf must be greater than 10 and less than 50 */
    if ((ipHalf-imHalf) < 10) {
      /* if peak completely unresolved */
      if ((ipHalf-imHalf)<=0) {  /* wild stab */
	half = 0.5 / cellFact; /* ~ halfwidth? 1/2 cell */
      } else { /* partly resolved */
	half = (0.5 * (ipHalf-imHalf)) / cellFact; /* ~ halfwidth */
      }
      mean = amin + (modeCell-0.5) /  cellFact;
      /* don't spread over whole histogram - try for 25 cells*/
      half *= numCell / 25.0; 
      amax = mean + half;
      amin = mean - half;
      /* amin = MAX (amin, omin);
	 amax = MIN (amax, omax);*/
      continue;  /* try it again */
    } else if ((ipHalf-imHalf) > 50) {  /* Spread too thinly? */
      mean = amin + (modeCell-0.5) /  cellFact;
      half = (0.5 * (ipHalf-imHalf)) / cellFact; /* ~ halfwidth */
      /* don't spread over whole histogram - try for 25 cells*/
      half *= numCell / 25.0;  
      amax = mean + half;
      amin = mean - half;
      continue;  /* try it again */
    }
    done = TRUE;
    break;
  } /* end loop getting acceptable histogram */

  /* determine RMS */
  out = (ipHalf - imHalf) / cellFact; /* FWHM */
  out *= 2.35 / 2.0; /* Sigma */

  /* get second moment around mode +/- 3 sigma */
  i1 = modeCell - 3 * (modeCell-imHalf);
  i1 = MAX (0, i1);
  i2 = modeCell + 3 * (ipHalf - modeCell);
  i2 = MIN (numCell, i2);

  /* Get second moment */
  sum = sum2 = 0.0;
  count = 0.0; ic = 0;
  for (i=i1; i<=i2; i++) {
    if (histo[i]!=0) {
      x      = (gfloat)(i - modeCell);
      sum   += histo[i] * x;
      sum2  += histo[i] * x * x;
      count += histo[i];
      ic++;
    }
  }
    
  /* determine RMS */
  if (ic>2.0) {
    mean = sum / count;
    arg = (sum2/count) - mean*mean;
    if (arg>0.0) out = sqrt(arg);
    else out = 0.5 * fabs (mean);
    out /= cellFact;
  } else { /* trouble - histogram bad*/
    out = fblank;
  }
 
  /* cleanup */
  ObitMemFree(histo);

  return MIN (out, rawRMS);
} /* end  ObitFArrayRMS */

/**
 *  Determine Mode of pixel value distribution in array.
 *  Value is based on a histogram analysis and is determined from 
 *  the peak in the distribution..
 *  out =  Mode (in.)
 * \param in Input object with data
 * \return mode of distribution
 */
gfloat ObitFArrayMode (ObitFArray* in)
{
  glong i, icell, modeCell, numCell, count, pos[MAXFARRAYDIM];
  gfloat amax, amin, tmax, cellFact, *histo;
  gfloat out = 0.0, fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitFArrayIsA(in));
  g_assert (in->array != NULL);

  /* get max/min */
  amax = ObitFArrayMax (in, pos);
  amin = ObitFArrayMin (in, pos);

   /* How many valid values? */
  count = 0;
  for (i=0; i<in->arraySize; i++) if (in->array[i]!=fblank) count++;
  if (count<5) return out; /* better have some */

 /* Make histogram size such that the average cell has 30 entries */
  numCell = count / 30;
  histo = ObitMemAllocName(numCell*sizeof(gfloat), "FArray histo");

  /* form histogram */
  cellFact =  numCell / (amax - amin + 1.0e-20);
  for (i=0; i<numCell; i++) histo[i] = 0.0;
  for (i=0; i<in->arraySize; i++) {
    if (in->array[i]!=fblank){
      icell = 0.5 + cellFact * (in->array[i]-amin);
      icell = MIN (numCell-1, MAX(0, icell));
      histo[icell]++;
    }
  }

  /* Find mode cell */
  modeCell = -1;
  tmax = -1.0e20;
  for (i=0; i<numCell; i++) {
    if (histo[i]>tmax) {
      tmax = histo[i];
      modeCell = i;
    }
  }

  /* convert from cell to value */
  out = amin + modeCell / cellFact;

  /* cleanup */
  ObitMemFree(histo);
  
  return out;
} /* end  ObitFArrayMode */

/**
 *  determine mean value in array
 *  out =  Mean (in.)
 * \param in Input object with data
 * \return mean of distribution
 */
gfloat ObitFArrayMean (ObitFArray* in)
{
  glong i, count;
  gfloat out = 0.0, fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitFArrayIsA(in));
  g_assert (in->array != NULL);

  out = 0.0;
  count = 0;
  for (i=0; i<in->arraySize; i++) {
    if (in->array[i]!=fblank) {
      out += in->array[i];
      count++;
    }
  }
  
  if (count>0) out /= count;
  else out = fblank;

  return out;
} /* end  ObitFArrayMean */

/**
 *  Replace each element of the array with scalar.
 * in = scalar.
 * \param in Input object with data
 * \param scalar  Scalar value
 */
void ObitFArrayFill (ObitFArray* in, gfloat scalar)
{
  glong i;

   /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (in->array != NULL);

  for (i=0; i<in->arraySize; i++) in->array[i] = scalar;
} /* end  ObitFArrayFill */

/**
 *  Negate each element of the array.
 * in = -in.
 * \param in Input object with data
 */
void ObitFArrayNeg (ObitFArray* in)
{
  glong i;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (in->array != NULL);

  for (i=0; i<in->arraySize; i++) 
    if (in->array[i]!=fblank) in->array[i] = -in->array[i];
} /* end  ObitFArrayNeg */

/**
 *  Sum each element of the array.
 * out =  Sum (in.)
 * \param in Input object with data
 * \return sum of elements 
 */
gfloat ObitFArraySum (ObitFArray* in)
{
  glong i;
  gfloat out = 0.0, fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (in->array != NULL);

  for (i=0; i<in->arraySize; i++) 
    if (in->array[i]!=fblank) out += in->array[i];

  return out;
} /* end  ObitFArraySum */

/**
 * How many valid elements are in in?
 * out =  Count of valid elements in (in.)
 * \param in Input object with data
 * \return count of valid elements 
 */
glong ObitFArrayCount (ObitFArray* in)
{
  glong i, out = 0;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (in->array != NULL);

  for (i=0; i<in->arraySize; i++) 
    if (in->array[i]!=fblank) out++;

  return out;
} /* end  ObitFArrayCount */

/**
 *  Add a scalar to each element of the array.
 * in = in + scalar
 * \param in      Input object with data
 * \param scalar  Scalar value
 */
void ObitFArraySAdd (ObitFArray* in, gfloat scalar)
{
  glong i;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (in->array != NULL);

  for (i=0; i<in->arraySize; i++) 
    if (in->array[i]!=fblank) in->array[i] += scalar;
} /* end ObitFArraySAdd */

/**
 *  Multiply each element of the array by a scalar.
 * in = in * scalar
 * \param in      Input object with data
 * \param scalar  Scalar value
 */
void ObitFArraySMul (ObitFArray* in, gfloat scalar)
{
  glong i;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (in->array != NULL);

  for (i=0; i<in->arraySize; i++) 
    if (in->array[i]!=fblank) in->array[i]  *= scalar;
} /* end ObitFArraySMul */

/**
 *  Divide each element of the array into a scalar.
 * No check for zeroes is made .
 * in = scalar / in
 * \param in      Input object with data
 * \param scalar  Scalar value
 */
void ObitFArraySDiv (ObitFArray* in, gfloat scalar)
{
  glong i;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (in->array != NULL);

  for (i=0; i<in->arraySize; i++) 
    if (in->array[i]!=fblank) in->array[i] = scalar / in->array[i];
} /* end ObitFArraySDiv */

/**
 *  Replace values outside of a given range with a new value
 * in = newVal where in < minVal or in > maxVal
 * \param in      Input object with data
 * \param minVal  Minimum allowed value
 * \param maxVal  Maximum allowed value
 * \param newVal  Value to use if out of range.
 */
void ObitFArrayClip (ObitFArray* in, gfloat minVal, gfloat maxVal, 
		     gfloat newVal)
{
  glong i;
  gfloat fblank = ObitMagicF();

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (in->array != NULL);

  for (i=0; i<in->arraySize; i++) 
    if ((in->array[i]!=fblank) && ((in->array[i]<minVal) || (in->array[i]>maxVal)))
      in->array[i] = newVal;
} /* end ObitFArrayClip */

/**
 *  Replace values inside of a given range with a new value
 * in = newVal where in >= minVal and in <= maxVal
 * \param in      Input object with data
 * \param minVal  Minimum allowed value
 * \param maxVal  Maximum allowed value
 * \param newVal  Value to use if out of range.
 */
void ObitFArrayInClip (ObitFArray* in, gfloat minVal, gfloat maxVal, 
		     gfloat newVal)
{
  glong i;
  gfloat fblank = ObitMagicF();

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (in->array != NULL);

  for (i=0; i<in->arraySize; i++) 
    if ((in->array[i]!=fblank) && ((in->array[i]>=minVal) && (in->array[i]<=maxVal)))
      in->array[i] = newVal;
} /* end ObitFArrayInClip */

/**
 *  Blank elements of array in1 where array in2 is blanked
 *  out = in1 or blank where in2 is blank
 * \param in1  Input object with data
 * \param in2  Input object with blanking
 * \param out  Output array (may be an input array).
 */
void ObitFArrayBlank (ObitFArray* in1, ObitFArray* in2, ObitFArray* out)
{
  glong i;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in1, &myClassInfo));
  g_assert (ObitIsA(in2, &myClassInfo));
  g_assert (ObitFArrayIsCompatable(in1, in2));
  g_assert (ObitFArrayIsCompatable(in1, out));

  for (i=0; i<in1->arraySize; i++) {
    if (in2->array[i]!=fblank)
      out->array[i] = in1->array[i];
    else out->array[i] = fblank;
  }
} /* end ObitFArrayBlank */

/**
 * Pick the larger nonblanked elements of two arrays.
 *  out = MAX (in1, in2) or whichever is not blanked
 * \param in1  Input object with data
 * \param in2  Input object with data
 * \param out  Output array (may be an input array).
 */
void ObitFArrayMaxArr (ObitFArray* in1, ObitFArray* in2, ObitFArray* out)
{
  glong i;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in1, &myClassInfo));
  g_assert (ObitIsA(in2, &myClassInfo));
  g_assert (ObitFArrayIsCompatable(in1, in2));
  g_assert (ObitFArrayIsCompatable(in1, out));

  for (i=0; i<in1->arraySize; i++) {
    if ((in1->array[i]!=fblank) && (in2->array[i]!=fblank)) 
      out->array[i] = MAX (in1->array[i], in2->array[i]);
    else if (in1->array[i]==fblank)  /* 1 blanked */
      out->array[i] = in2->array[i];
    else if (in2->array[i]==fblank)
      out->array[i] = in1->array[i];  /* 2 blanked */
    else out->array[i] = fblank;      /* both blanked */
  }
} /* end ObitFArrayMaxArr */

/**
 * Pick the lesser nonblanked elements of two arrays.
 *  out = MIN (in1, in2) or whichever is not blanked
 * \param in1  Input object with data
 * \param in2  Input object with data
 * \param out  Output array (may be an input array).
 */
void ObitFArrayMinArr (ObitFArray* in1, ObitFArray* in2, ObitFArray* out)
{
  glong i;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in1, &myClassInfo));
  g_assert (ObitIsA(in2, &myClassInfo));
  g_assert (ObitFArrayIsCompatable(in1, in2));
  g_assert (ObitFArrayIsCompatable(in1, out));

  for (i=0; i<in1->arraySize; i++) {
    if ((in1->array[i]!=fblank) && (in2->array[i]!=fblank)) 
      out->array[i] = MIN (in1->array[i], in2->array[i]);
    else if (in1->array[i]==fblank)  /* 1 blanked */
      out->array[i] = in2->array[i];
    else if (in2->array[i]==fblank)
      out->array[i] = in1->array[i];  /* 2 blanked */
    else out->array[i] = fblank;      /* both blanked */
  }
} /* end ObitFArrayMinArr */

/**
 * Sum nonblanked elements of two arrays.
 *  out = (in1 + in2) or whichever is not blanked
 * \param in1  Input object with data
 * \param in2  Input object with data
 * \param out  Output array (may be an input array).
 */
void ObitFArraySumArr (ObitFArray* in1, ObitFArray* in2, ObitFArray* out)
{
  glong i;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in1, &myClassInfo));
  g_assert (ObitIsA(in2, &myClassInfo));
  g_assert (ObitFArrayIsCompatable(in1, in2));
  g_assert (ObitFArrayIsCompatable(in1, out));

  for (i=0; i<in1->arraySize; i++) {
    if ((in1->array[i]!=fblank) && (in2->array[i]!=fblank)) 
      out->array[i] = (in1->array[i] + in2->array[i]);
    else if (in1->array[i]==fblank)  /* 1 blanked */
      out->array[i] = in2->array[i];
    else if (in2->array[i]==fblank)
      out->array[i] = in1->array[i];  /* 2 blanked */
    else out->array[i] = fblank;      /* both blanked */
  }
} /* end ObitFArraySumArr */

/**
 *  Add corresponding elements of two arrays.
 *  out = in1 + in2,  if either is blanked the result is blanked
 * \param in1  Input object with data
 * \param in2  Input object with data
 * \param out  Output array (may be an input array).
 */
void ObitFArrayAdd (ObitFArray* in1, ObitFArray* in2, ObitFArray* out)
{
  glong i;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in1, &myClassInfo));
  g_assert (ObitIsA(in2, &myClassInfo));
  g_assert (ObitFArrayIsCompatable(in1, in2));
  g_assert (ObitFArrayIsCompatable(in1, out));

  for (i=0; i<in1->arraySize; i++) {
    if ((in1->array[i]!=fblank) && (in2->array[i]!=fblank)) 
      out->array[i] = in1->array[i] + in2->array[i];
    else out->array[i] = fblank;
  }
} /* end ObitFArrayAdd */

/**
 *  Subtract corresponding elements of the arrays.
 *  out = in1 - in2, if either is blanked the result is blanked
 * \param in1  Input object with data
 * \param in2  Input object with data
 * \param out  Output array (may be an input array).
 */
void ObitFArraySub (ObitFArray* in1, ObitFArray* in2, ObitFArray* out)
{
  glong i;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in1, &myClassInfo));
  g_assert (ObitIsA(in2, &myClassInfo));
  g_assert (ObitFArrayIsCompatable(in1, in2));
  g_assert (ObitFArrayIsCompatable(in1, out));

  for (i=0; i<in1->arraySize; i++) {
    if ((in1->array[i]!=fblank) && (in2->array[i]!=fblank)) 
      out->array[i] = in1->array[i] - in2->array[i];
    else out->array[i] = fblank;
  }
 } /* end ObitFArraySub */

/**
 *  Multiply corresponding elements of the arrays.
 *  out = in1 * in2
 * \param in1  Input object with data
 * \param in2  Input object with data
 * \param out  Output array (may be an input array).
 */
void ObitFArrayMul (ObitFArray* in1, ObitFArray* in2, ObitFArray* out)
{
  glong i;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in1, &myClassInfo));
  g_assert (ObitIsA(in2, &myClassInfo));
  g_assert (ObitFArrayIsCompatable(in1, in2));
  g_assert (ObitFArrayIsCompatable(in1, out));

  for (i=0; i<in1->arraySize; i++) {
    if ((in1->array[i]!=fblank) && (in2->array[i]!=fblank)) 
      out->array[i] = in1->array[i] * in2->array[i];
    else out->array[i] = fblank;
  }
} /* end ObitFArrayMul */

/**
 *  Divide corresponding elements of the arrays.
 *  out = in1 / in2
 * \param in1  Input object with data
 * \param in2  Input object with data
 * \param out  Output array (may be an input array).
 */
void ObitFArrayDiv (ObitFArray* in1, ObitFArray* in2, ObitFArray* out)
{
  glong i;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in1, &myClassInfo));
  g_assert (ObitIsA(in2, &myClassInfo));
  g_assert (ObitFArrayIsCompatable(in1, in2));
  g_assert (ObitFArrayIsCompatable(in1, out));

  for (i=0; i<in1->arraySize; i++) {
    if ((in1->array[i]!=fblank) && (in2->array[i]!=fblank)) 
      out->array[i] = in1->array[i] / in2->array[i];
    else out->array[i] = fblank;
  }
} /* end ObitFArrayDiv */

/**
 *  Divide corresponding elements of the arrays with clipping.
 *  out = in1 / in2 where in2>minVal, else blanked
 * \param in1    Input object with data
 * \param in2    Input object with data
 * \param minVal minimum allowed value for in2
 * \param out    Output array (may be an input array).
 */
void ObitFArrayDivClip (ObitFArray* in1, ObitFArray* in2, gfloat minVal, ObitFArray* out)
{
  glong i;
  gfloat fblank = ObitMagicF();

   /* error checks */
  g_assert (ObitIsA(in1, &myClassInfo));
  g_assert (ObitIsA(in2, &myClassInfo));
  g_assert (ObitFArrayIsCompatable(in1, in2));
  g_assert (ObitFArrayIsCompatable(in1, out));

  for (i=0; i<in1->arraySize; i++) {
    if ((in1->array[i]!=fblank) && (in2->array[i]!=fblank) && 
	(in2->array[i]>minVal)) {
      out->array[i] = in1->array[i] / in2->array[i];
      /* debug
      if (out->array[i] >1.0e5) fprintf (stderr,"bad div %ld %g %g %g\n",
					  i,in1->array[i],in2->array[i],out->array[i]); */
    } else out->array[i] = fblank;
  }
} /* end ObitFArrayDiv */

/**
 *  Sum the products of the elements of two arrays
 *  out = Sum (in1 x in2)
 * \param in1  Input object with data
 * \param in2  Input object with data
 * \return sum of product of elements 
 */
gfloat ObitFArrayDot (ObitFArray* in1, ObitFArray* in2)
{
  glong i;
  gfloat fblank = ObitMagicF();
  gfloat out = 0.0;

   /* error checks */
  g_assert (ObitIsA(in1, &myClassInfo));
  g_assert (ObitIsA(in2, &myClassInfo));
  g_assert (ObitFArrayIsCompatable(in1, in2));

  for (i=0; i<in1->arraySize; i++) {
    if ((in1->array[i]!=fblank) && (in2->array[i]!=fblank)) 
      out += in1->array[i] * in2->array[i];
  }
  return out;
} /* end ObitFArrayDot */

/**
 *  Multiply the elements of a 2D array by the corresponding elements
 *  of a row and column vector.
 *  NOTE: this does not check for magic value blanking, this was causing 
 *  trouble in its major application - image formation - which should not 
 *  produce blanks.
 *  out[i,j] = in[i,j] * row[j] * col[i].
 * \param in   Input 2D array
 * \param row  Input row vector
 * \param col  Input column
 * \param out  Output array (may be an input array).
 */
void ObitFArrayMulColRow (ObitFArray* in, ObitFArray* row, ObitFArray* col,
			  ObitFArray* out)
{
  glong i, j, nx, ny;
  gfloat *inp, *outp, *rowp, *colp;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitIsA(row, &myClassInfo));
  g_assert (ObitIsA(col, &myClassInfo));
  g_assert (ObitIsA(out, &myClassInfo));
  g_assert (ObitFArrayIsCompatable(in, out));
  g_assert (in->ndim==2);
  g_assert (row->ndim==1);
  g_assert (col->ndim==1);
  g_assert (row->naxis[0]==in->naxis[0]);
  g_assert (col->naxis[0]==in->naxis[1]);

  /* Dimensions */
  nx = in->naxis[0];
  ny = in->naxis[1];

  /* init pointers */
  inp  = in->array;
  rowp = row->array;
  colp = col->array;
  outp = out->array;

  /* Double loop over array */
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      outp[i] = inp[i] * rowp[j] * colp[i];
    } /* end row loop */
    inp  += nx; /* Pointers for next row */
    outp += nx;
  } /* end column loop */

} /* end ObitFArrayMulColRow  */

/**
 *  In-place rearrangement of a center-at-the edges array to 
 * center at the center, or the other way around.
 * This is needed for the peculiar order of FFTs.
 * FFTs don't like blanked values.
 * \param in   1D array to reorder
 */
void ObitFArray1DCenter (ObitFArray* in)
{
  glong i, pos[1], nx;
  gfloat temp, *inp, *outp;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* ... and God sayeth unto FFT: 
     "The middle shall be first and the last shall be the middle." */

  /* swap halves */
  nx = in->naxis[0];
  pos[0] = 0;
  inp = ObitFArrayIndex (in, pos);
  pos[0] = nx/2;
  outp = ObitFArrayIndex (in, pos);

  for (i=0; i<nx/2; i++) {
    temp    = outp[i];
    outp[i] = inp[i];
    inp[i]  = temp;
  }
} /* end ObitFArray1DCenter */

/**
 *  In-place rearrangement of a center-at-the edges array to 
 * center at the center, or the other way around.
 * This is needed for the peculiar order of FFTs.
 * FFTs don't like blanked values.
 * \param in   2D array to reorder
 */
void ObitFArray2DCenter (ObitFArray* in)
{
  glong i, j, pos[2], nx, ny;
  gfloat temp, *inp, *outp;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* ... and God sayeth unto FFT: 
     "The middle shall be first and the last shall be the middle." */

  /* swap first and fourth quadrents */
  nx = in->naxis[0];
  ny = in->naxis[1];
  pos[0] = 0; pos[1] = 0;
  inp = ObitFArrayIndex (in, pos);
  pos[0] = nx/2; pos[1] = ny/2;
  outp = ObitFArrayIndex (in, pos);

  for (j=0; j<ny/2; j++) {
    for (i=0; i<nx/2; i++) {
      temp = outp[i];
      outp[i] = inp[i];
      inp[i] = temp;
    }
    /* next rwo */
    inp += nx;
    outp += nx;
  }

  /* swap second and third quadrents */
  nx = in->naxis[0];
  ny = in->naxis[1];
  pos[0] = nx/2; pos[1] = 0;
  inp = ObitFArrayIndex (in, pos);
  pos[0] = 0; pos[1] = ny/2;
  outp = ObitFArrayIndex (in, pos);

  for (j=0; j<ny/2; j++) {
    for (i=0; i<nx/2; i++) {
      temp = outp[i];
      outp[i] = inp[i];
      inp[i] = temp;
    }
    /* next rwo */
    inp += nx;
    outp += nx;
  }
  
} /* end ObitFArray2DCenter */

/**
 *  In-place inversion of a symmetric matrix in an ObitFArray
 * Adopted from AIPS SYMINV.FOR
 * Magic blanking not supported
 * \param in   2D array to invers (max dim 50x50)
 * \param ierr return code, 0=>OK, else could not invert.
 */
void ObitFArray2DSymInv (ObitFArray* in, gint *ierr)
{
#define MAXSYMINVSIZE 50
  glong  j, k=0, l, m, n;
  gfloat  ab, big, p[MAXSYMINVSIZE], q[MAXSYMINVSIZE], r[MAXSYMINVSIZE];
  gfloat *a;
 

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  *ierr = 0;
  if (in->ndim!=2) *ierr = 1;
  if (in->naxis[0]>50) *ierr = 2;
  if (in->naxis[0]!=in->naxis[1]) *ierr = 3;
  if (*ierr != 0) return;

  /* array information */
  a = in->array;
  n = in->naxis[0];

  /* make sure symmetric */
  for (j= 1; j<=n; j++) { /* loop 10 */
    for (k= 1; k<=n; k++) { /* loop 5 */
      /* not symmetric */
      if (abs (a[(k-1)*n+(j-1)]-a[(j-1)*n+(k-1)]) > abs (a[(k-1)*n+(j-1)])*1.0e-4) {
	*ierr = 4;
	return;
      } 
    } /* end loop  L5:   */;
  } /* end loop  L10:  */;
  for (m= 1; m<=n; m++) { /* loop 15 */
    r[m-1] = 1.0;
  } /* end loop  L15:  */;
  for (m= 1; m<=n; m++) { /* loop 200 */
    big = 0.0;
    for (l= 1; l<=n; l++) { /* loop 20 */
      ab = abs (a[(l-1)*n+(l-1)]);
      if ((ab > big)  &&  (r[l-1] != 0)) {
	big = ab;
	k = l;
      } 
    } /* end loop  L20:  */;
    /* inverse indeterminant? */
    if (big == 0.0) {
      *ierr = 5;
      return;
    } 
    r[k-1] = 0.0;
    q[k-1] = 1.0 / a[(k-1)*n+(k-1)];
    p[k-1] = 1.0;
    a[(k-1)*n+(k-1)] = 0.0;
    for (l= 1; l<=k-1; l++) { /* loop 30 */
      p[l-1] = a[(k-1)*n+(l-1)];
      if (r[l-1] == 0.0) {
	q[l-1] = a[(k-1)*n+(l-1)] * q[k-1];
      } else {
	q[l-1] = -a[(k-1)*n+(l-1)] * q[k-1];
      } 
      a[(k-1)*n+(l-1)] = 0.0;
    } /* end loop  L30:  */;
    for (l= k+1; l<=n; l++) { /* loop 40 */
      if (r[l-1] != 0.0) {
	p[l-1] = a[(l-1)*n+(k-1)];
      } else {
	p[l-1] = -a[(l-1)*n+(k-1)];
      } 
      q[l-1] = -a[(l-1)*n+(k-1)] * q[k-1];
      a[(l-1)*n+(k-1)] = 0.0;
    } /* end loop  L40:  */;
    for (l= 1; l<=n; l++) { /* loop 60 */
      for (k= l; k<=n; k++) { /* loop 50 */
	a[(k-1)*n+(l-1)] = a[(k-1)*n+(l-1)] + p[l-1]*q[k-1];
      } /* end loop  L50:  */;
    } /* end loop  L60:  */;
  } /* end loop  L200: */;
  /* fill in symmetric half */
  m = n + 1;
  l = n;
  for (k= 2; k<=n; k++) { /* loop 80 */
    m = m - 1;
    l = l - 1;
    for (j= 1; j<=l; j++) { /* loop 70 */
      a[(j-1)*n+(m-1)] = a[(m-1)*n+(j-1)];
    } /* end loop  L70:  */;
  } /* end loop  L80:  */;
  
 } /* end ObitFArray2DSymInv */


/**
 * Make 2-D circular Gaussian in an FArray
 * \param array  Array to fill in
 * \param Cen    0-rel center pixel
 * \param Size   FWHM of Gaussian in pixels.
 */
void ObitFArray2DCGauss (ObitFArray *array, glong Cen[2], gfloat FWHM)
{
  glong ix, iy, indx, nx, ny, size[2];
  gfloat x, y, *data, sigma, factor, arg;

  /* error checks */
  g_assert(ObitFArrayIsA(array));

  nx = array->naxis[0];
  ny = array->naxis[1];
  size[0] = size[1] = 0;
  data = ObitFArrayIndex (array, size);

  /* Loop */
  sigma = FWHM / 2.3548;
  factor = 1.0 / (2.0 * sigma * sigma);
  for (iy=0; iy<ny; iy++) {
    for (ix=0; ix<nx; ix++) {
      indx = iy*nx + ix;
      x = (gfloat)(ix - Cen[0]);
      y = (gfloat)(iy - Cen[1]);
      arg = (x*x + y*y) * factor;
      if (arg<15.0) arg = exp (-arg);
      else arg = 0.0;
      data[indx] = arg;
    }
  }

} /* end ObitFArray2DCGauss */

/**
 * Make 2-D elliptical Gaussian in an FArray
 * Model is added to previoius contents of the FArray
 * \param array   Array to fill in
 * \param amp     Peak value of Gaussian
 * \param Cen     0-rel center pixel
 * \param GauMod  Gaussian parameters, Major axis, FWHM, minor axis 
 *        FWHM (both in pixels) and rotation angle wrt "Y" axis (deg).
 */
void ObitFArray2DEGauss (ObitFArray *array, gfloat amp, gfloat Cen[2], gfloat GauMod[3])
{
  glong ix, iy, indx, nx, ny, size[2];
  gfloat *data, sigmaX, sigmaY, factorX, factorY, arg;
  gfloat CosPA, SinPA, xp, yp, x, y;

  /* error checks */
  g_assert(ObitFArrayIsA(array));

  nx = array->naxis[0];
  ny = array->naxis[1];
  size[0] = size[1] = 0;
  data = ObitFArrayIndex (array, size);

  /* Set values for calculating Gaussian */
  CosPA = cos ((GauMod[2]+90.0)*DG2RAD);
  SinPA = sin ((GauMod[2]+90.0)*DG2RAD);
  sigmaX = GauMod[1] / 2.3548;   /* X is minor */
  factorX = 1.0 / (2.0 * sigmaX * sigmaX);
  sigmaY = GauMod[0] / 2.3548;   /* Y is major */
  factorY = 1.0 / (2.0 * sigmaY * sigmaY);

  /* Loop */
  for (iy=0; iy<ny; iy++) {
    for (ix=0; ix<nx; ix++) {
      indx = iy*nx + ix;
      xp = (gfloat)(ix - Cen[0]);
      yp = (gfloat)(iy - Cen[1]);
      /* Rotate to elipse frame */
      x = CosPA*xp + yp*SinPA;
      y = CosPA*yp - xp*SinPA;
      arg = x*x*factorX + y*y*factorY;
      if (arg<15.0) {
	arg = amp * exp (-arg);
	data[indx] += arg;
      }
    }
  }

} /* end ObitFArray2DEGauss */

/**
 *  Shift and Add scaled arrays
 *  Two FArrays are aligned at specified pixels and the 
 *  corresponding pixels are added with a scalar multiplied 
 *  times the second.
 *  Only handles to 3 dimensions.
 *  If in1/out are 3D and in2 is 2D then the same plane in in2
 *  is used for all planes in in1/out.
 * NB: this works better if the alignment point is near the center of in2
 *  out = in1 + scalar x in2 in overlap, else in1
 * \param in1     First input object with data, may be blanked
 * \param pos1    Alignment pixel in in1
 * \param in2     Second input object with data, blanked pixels ignored
 * \param pos2    Alignment pixel in in2
 * \param scalar  factor to be multiplied times in2
 * \param out     Output array, may be an input array and MUST 
 *                have the same the same geometry.
 */
void ObitFArrayShiftAdd (ObitFArray* in1, glong *pos1, 
			 ObitFArray* in2, glong *pos2, 
			 gfloat scalar, ObitFArray* out)
{
  glong ix, iy, ip, np, hix, lox, hiy, loy, offx, offy;
  glong nx1, ny1, nx2, ny2, lenp1, lenp2, indx1, indx2;
  gfloat fblank = ObitMagicF();

  /* error checks */
  g_assert (ObitIsA(in1, &myClassInfo));
  g_assert (ObitIsA(in2, &myClassInfo));
  g_assert (ObitIsA(out, &myClassInfo));
  g_assert(pos1!=NULL);
  g_assert(pos2!=NULL);
  /* Check dimensionality */
  g_assert (in1->ndim==in2->ndim);
  g_assert (in1->ndim==out->ndim);
  g_assert (in1->naxis[0]==out->naxis[0]);
  g_assert (in1->naxis[1]==out->naxis[1]);

  /* Size of in1/out */
  nx1 = in1->naxis[0];
  ny1 = in1->naxis[1];
  if (in1->ndim>2) np = in1->naxis[2];
  else np = 1;
  lenp1 = nx1*ny1;
 
  /* Size of in2 */
  nx2 = in2->naxis[0];
  ny2 = in2->naxis[1];
  if (in2->ndim>2) lenp2 = nx2*ny2;
  else lenp2 = 0;

  /* determine regions of overlap in in1/out of in2 */
  offx = pos2[0] - pos1[0];
  offy = pos2[1] - pos1[1];
  lox = MAX (0, pos1[0] - in2->naxis[0]/2);
  hix = MIN (nx1-1, pos1[0] + in2->naxis[0]/2);
  loy = MAX (0, pos1[1] - in2->naxis[1]/2);
  hiy = MIN (ny1-1, pos1[1] + in2->naxis[1]/2);
  /* In case in2 not centered */
  if (lox+offx<0) lox -= lox+offx;
  if (loy+offy<0) loy -= loy+offy;
  if (hix+offx>=nx2) hix -= hix+offx-nx2+1;
  if (hiy+offy>=ny2) hiy -= hiy+offy-ny2+1;

  /* Loop over planes */
  for (ip = 0; ip<np; ip++) {

    /* Loop over rows */
    for (iy=loy; iy<=hiy; iy++) {

      /* Loop over columns */
      for (ix=lox; ix<=hix; ix++) {

	/* indices in arrays */
	indx1 = ip*lenp1 + iy*nx1 + ix;
	indx2 = ip*lenp2 + (iy+offy) * nx2 + ix + offx;
	
	/* do operation */
	if (in1->array[indx1]!=fblank) {
	  if (in2->array[indx2]!=fblank) {
	    out->array[indx1] = in1->array[indx1] + scalar * in2->array[indx2];
	    /* debug
	    if (out->array[indx1] >1.0e5) fprintf (stderr,"bad accum %ld %ld %g %g %g\n",
					       ix,iy,in1->array[indx1],in2->array[indx2],out->array[indx1]); */
	  }
	} else {
	  out->array[indx1] = fblank;
	}
      } 
    } 
  } /* end loop over planes */
 
} /* end ObitFArrayShiftAdd */

/**
 * Zero fills out and inserts in, centered and multiplied by factor.
 * Any blanks in in are replaced with zero.
 * This routine is intended for zero padding images before an FFT
 * to increase the resolution in the uv plane.
 * \param in      Object with structures to zero pad
 * \param out     Output object
 * \param factor  scaling factor for in
 */
void ObitFArrayPad (ObitFArray *in, ObitFArray* out, gfloat factor)
{
  glong ipos[2], opos[2];

   /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitIsA(out, &myClassInfo));

  /* Zero fill output */
  ObitFArrayFill (out, 0.0);

  /* Insert in into out - center as well as possible */
  ipos[0] = in->naxis[0]/2;  ipos[1] = in->naxis[1]/2;
  opos[0] = out->naxis[0]/2; opos[1] = out->naxis[1]/2;
  ObitFArrayShiftAdd (out, opos, in, ipos, factor, out);

  /* Deblank */
   ObitFArrayDeblank (out, 0.0);

} /* end  ObitFArrayPad */

/**
 * Convolves a list of points with a Gaussian and adds them to a 2-D array.
 * \param in      2-D array to add Gaussians to.
 * \param list    List of positions and fluxes of the Gaussians
 *                (x pixel, y pixel, flux)
 * \param ncomp   Number of components in list  
 *                (generally less than size of FArray).
 * \param gauss   Gaussian coefficients for (d_x*d_x, d_y*d_y, d_x*d_y)
 *                Gaussian maj = major axis FWHM, min=minor, pa = posn. angle
 *                cr=cos(pa+rotation), sr=sin(pa+rotation),
 *                cell_x, cell_y x, y cell spacing is same units as maj, min
 *                [0] = {(cr/min)^2 + ((sr/maj)^2)}*(cell_x^2)*4*log(2)
 *                [1] = {(sr/min)^2 + ((cr/maj)^2)}*(cell_y^2)*4*log(2)
 *                [2] = {(1/min)^2 - ((1/maj)^2)}*sr*cr*abs(cell_x*cell_y)*8*log(2)
 */
void  ObitFArrayConvGaus (ObitFArray* in, ObitFArray* list, glong ncomp, 
			  gfloat gauss[3])
{
  glong icomp, lrec, ix, iy, indx;
  gfloat *table, *image, dx, dy, arg, aa, bb, cc;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitIsA(list, &myClassInfo));

  /* Setup list to access */
  table = list->array;
  lrec = list->naxis[0];

  /* access to in as image */
  image = in->array;

  /* Gaussian parameters */
  aa = gauss[0];
  bb = gauss[1];
  cc = gauss[2];

  /* Loop over elements in list */
  for (icomp=0; icomp<ncomp; icomp++) {
    if (table[2]==0.0) continue;  /* ignore zero flux */
    indx = 0;  /* image array index */

    /* Loop over array convolving */
    for (iy = 0; iy<in->naxis[1]; iy++) {
      dy = iy - table[1];   /* y offset */
      for (ix = 0; ix<in->naxis[0]; ix++) {
	dx = ix - table[0];   /* x offset */
	arg = aa*dx*dx + bb*dy*dy + cc*dx*dy;
	if (arg<12.0) {
	  image[indx] += table[2] * exp(-arg);
	}
	indx++;
      }
    }
    table += lrec;
  } /* end list over components */

} /* end ObitFArrayConvGaus */

/**
 * Initialize global ClassInfo Structure.
 */
 void ObitFArrayClassInit (void)
{
  const ObitClassInfo *ParentClass;
  
  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;
  
  /* Initialize (recursively) parent class first */
  ParentClass = ObitGetClass();
  ObitClassInit();  /* Initialize parent class if needed */
  
  /* function pointers etc. for this class */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = (ObitClassInitFP)ObitFArrayClassInit;
  myClassInfo.newObit       = (newObitFP)newObitFArray;
  myClassInfo.ObitCopy      = (ObitCopyFP)ObitFArrayCopy;
  myClassInfo.ObitFArraySubArr = 
    (ObitFArraySubArrFP)ObitFArraySubArr;
  myClassInfo.ObitClone     = NULL;
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitFArrayClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitFArrayInit;
  myClassInfo.ObitFArrayCreate = (ObitFArrayCreateFP)ObitFArrayCreate;
  myClassInfo.ObitFArrayIsCompatable = 
    (ObitFArrayIsCompatableFP)ObitFArrayIsCompatable;
  myClassInfo.ObitFArrayIndex  = (ObitFArrayIndexFP)ObitFArrayIndex;
  myClassInfo.ObitFArrayFill   = (ObitFArrayFillFP)ObitFArrayFill;
  myClassInfo.ObitFArrayNeg    = (ObitFArrayNegFP)ObitFArrayNeg;
  myClassInfo.ObitFArrayMax    = (ObitFArrayMaxFP)ObitFArrayMax;
  myClassInfo.ObitFArrayMaxAbs = (ObitFArrayMaxAbsFP)ObitFArrayMaxAbs;
  myClassInfo.ObitFArrayMin    = (ObitFArrayMinFP)ObitFArrayMin;
  myClassInfo.ObitFArrayDeblank= (ObitFArrayDeblankFP)ObitFArrayDeblank;
  myClassInfo.ObitFArraySum    = (ObitFArraySumFP)ObitFArraySum;
  myClassInfo.ObitFArrayCount  = (ObitFArrayCountFP)ObitFArrayCount;
  myClassInfo.ObitFArraySAdd   = (ObitFArraySAddFP)ObitFArraySAdd;
  myClassInfo.ObitFArraySMul   = (ObitFArraySMulFP)ObitFArraySMul;
  myClassInfo.ObitFArraySDiv   = (ObitFArraySDivFP)ObitFArraySDiv;
  myClassInfo.ObitFArrayClip   = (ObitFArrayClipFP)ObitFArrayClip;
  myClassInfo.ObitFArrayInClip = (ObitFArrayInClipFP)ObitFArrayInClip;
  myClassInfo.ObitFArrayBlank  = (ObitFArrayBlankFP)ObitFArrayBlank;
  myClassInfo.ObitFArrayMaxArr = (ObitFArrayMaxArrFP)ObitFArrayMaxArr;
  myClassInfo.ObitFArrayMinArr = (ObitFArrayMinArrFP)ObitFArrayMinArr;
  myClassInfo.ObitFArraySumArr = (ObitFArraySumArrFP)ObitFArraySumArr;
  myClassInfo.ObitFArrayAdd    = (ObitFArrayAddFP)ObitFArrayAdd;
  myClassInfo.ObitFArraySub    = (ObitFArraySubFP)ObitFArraySub;
  myClassInfo.ObitFArrayMul    = (ObitFArrayMulFP)ObitFArrayMul;
  myClassInfo.ObitFArrayDiv    = (ObitFArrayDivFP)ObitFArrayDiv;
  myClassInfo.ObitFArrayDivClip= (ObitFArrayDivClipFP)ObitFArrayDivClip;
  myClassInfo.ObitFArrayDot    = (ObitFArrayDotFP)ObitFArrayDot;
  myClassInfo.ObitFArrayMulColRow = 
    (ObitFArrayMulColRowFP)ObitFArrayMulColRow;
  myClassInfo.ObitFArray1DCenter = 
    (ObitFArray1DCenterFP)ObitFArray1DCenter;
  myClassInfo.ObitFArray2DCenter = 
    (ObitFArray2DCenterFP)ObitFArray2DCenter;
  myClassInfo.ObitFArray2DSymInv = 
    (ObitFArray2DSymInvFP)ObitFArray2DSymInv;
  myClassInfo.ObitFArray2DCGauss = 
    (ObitFArray2DCGaussFP)ObitFArray2DCGauss;
  myClassInfo.ObitFArrayShiftAdd = 
    (ObitFArrayShiftAddFP)ObitFArrayShiftAdd;
  myClassInfo.ObitFArrayPad = (ObitFArrayPadFP)ObitFArrayPad;
  myClassInfo.ObitFArrayConvGaus = 
    (ObitFArrayConvGausFP)ObitFArrayConvGaus;
} /* end ObitFArrayClassInit */

/*---------------Private functions--------------------------*/

/**
 * Creates empty member objects, initialize reference count.
 * Parent classes portions are (recursively) initialized first
 * \param inn Pointer to the object to initialize.
 */
void ObitFArrayInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitFArray *in = inn;

  /* error checks */
  g_assert (in != NULL);

  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->thread       = newObitThread();
  in->info         = newObitInfoList(); 
  in->array        = NULL;
  in->arraySize    = 0;
  in->ndim         = 0;
  in->naxis        = NULL;

} /* end ObitFArrayInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * For some reason this wasn't build into the GType class.
 * \param  inn Pointer to the object to deallocate.
 *           Actually it should be an ObitFArray* cast to an Obit*.
 */
void ObitFArrayClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitFArray *in = inn;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* delete this class members */
  in->thread    = ObitThreadUnref(in->thread);
  in->info      = ObitInfoListUnref(in->info);
  if (in->array)  ObitMemFree(in->array);  in->array = NULL;
  if (in->naxis)  ObitMemFree(in->naxis); in->naxis = NULL;
  
  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);
  
} /* end ObitFArrayClear */

