/* $Id: ObitTableUtil.c,v 1.4 2005/08/03 16:12:08 bcotton Exp $                            */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2004                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include <math.h>
#include <string.h>
#include "glib/gqsort.h"
#include "ObitTableUtil.h"
#include "ObitImage.h"
#include "ObitInfoElem.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitTableUtil.c
 * ObitTableUtil class utility function definitions.
 */

/*-------------------- unions -------------------------------------*/
/** Equivalence for Sort key arrays */
  union ObitSortEquiv { 
    gint    itg;
    gfloat  flt;
    gdouble dbl;
    gchar   str;
  };
/*-------------------- structure -------------------------------------*/
/** Sort Index plus sort key */
typedef struct {
  gint index;
  union ObitSortEquiv key;
} ObitSortStruct;


/*----------------------Private functions---------------------------*/
/** Private: Form sort structure for a table */
static gpointer 
MakeSortStruct (ObitTable *in, gint which[2], gboolean desc,
		gint *size, gint *number, gint *ncomp,
		ObitInfoType *type, ObitErr *err);

/** Private: Sort comparison function for  integers */
static gint CompareInt (gconstpointer in1, gconstpointer in2, 
		     gpointer ncomp);

/** Private: Sort comparison function for floats */
static gint CompareFloat (gconstpointer in1, gconstpointer in2, 
		     gpointer ncomp);

/** Private: Sort comparison function for doubles */
static gint CompareDouble (gconstpointer in1, gconstpointer in2, 
		     gpointer ncomp);

/** Private: Sort comparison function for string */
static gint CompareString (gconstpointer in1, gconstpointer in2, 
		     gpointer ncomp);

/** Private: reorder table based on Sort structure */
static ObitIOCode 
ReorderTable(ObitTable *in, gpointer base, gint size, gint number, 
	     gint sortCol, ObitErr *err);
/*----------------------Public functions---------------------------*/


/**
 * Sort the rows of a table on a given column
 * If the table is already marked in this order then it is not resorted.
 * \param in      Table to sort
 * \param colName Column label to sort by (for now first cell)
 * \param desc    If true want descending sort, else ascent
 * \param err     ObitErr error stack.
 * \return I/O Code  OBIT_IO_OK = OK.
 */
ObitIOCode ObitTableUtilSort (ObitTable *in, gchar *colName, gboolean desc,
			     ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gint i, size, number, ncomp, which[2], colNo;
  ObitInfoType type;
  gboolean bugOut;
  gpointer SortStruct = NULL;
  gchar *routine = "ObitTableUtilSort";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitTableIsA(in));

  /* Open table */
  retCode = ObitTableOpen (in, OBIT_IO_ReadWrite, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, in->name, retCode);

  /* Which column is desired? */
  colNo = -1;
  size = strlen(colName);
  for (i=0; i<in->myDesc->nfield; i++) {
    if (!strncmp (colName, in->myDesc->FieldName[i], size)) {
      colNo = i;
      break;
    }
  }

  /* If this is already sorted, button up and go home */
  if (desc) {  /* descending sort */
    bugOut = -(colNo) == (in->myDesc->sort[0]-1);
  } else {  /* ascending sort */
    bugOut = (colNo) == (in->myDesc->sort[0]-1);
  }
  if (bugOut) {
   /* Close table */
  retCode = ObitTableClose (in, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, in->name, retCode);
  return OBIT_IO_OK;
 }

  /* Found it? */
  if (colNo<0) { /* No column by that name */
     Obit_log_error(err, OBIT_Error, 
		   "%s: NO column %s found in %s", 
		    routine, colName, in->name);
    return retCode;
 }

  /* build sort structure from table */
  which[0] = colNo+1;  /* 1-rel */
  which[1] = 1;  /* First cell for now */
  SortStruct = MakeSortStruct (in, which, desc, &size, &number, &ncomp, &type, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, retCode);

  /* Close table */
  retCode = ObitTableClose (in, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, in->name, retCode);
  
  /* Sort keys by key type */
  switch (type) { 
  case OBIT_int:
    g_qsort_with_data (SortStruct, number, size, CompareInt, &ncomp);
    break;
  case OBIT_float:
    g_qsort_with_data (SortStruct, number, size, CompareFloat, &ncomp);
    break;
  case OBIT_double:
    g_qsort_with_data (SortStruct, number, size, CompareDouble, &ncomp);
    break;
  case OBIT_string:
    g_qsort_with_data (SortStruct, number, size, CompareString, &ncomp);
    break;
  default:
    g_assert_not_reached(); /* unknown, barf */
  }; /* end switch */

  /* Reorder table */
  retCode = ReorderTable (in, SortStruct, size, number, colNo+1, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, in->name, retCode);
  
  /* Cleanup */
  if (SortStruct) g_free(SortStruct);

  return OBIT_IO_OK;
} /* end ObitTableUtilSort */


/*----------------------Private functions---------------------------*/
/**
 * Create/fill sort structure for a table
 * The sort structure has one "entry" per row (the first  dimension of 
 * a 2D array) which contains 1) a gint row number as an index (1-rel), 
 * and 2 one or more entries of the relevant type. 
 * Each valid row in the table has an entry.
 * \param in     Table to sort, assumed already open;
 * \param which  Column and element number to use as key (1-rel)
 * \param desc   If true want descending sort, else ascent
 * \param size   [out] number of bytes in entry
 * \param number [out] number of entries
 * \param type   Data type of key
 * \param ncomp Number of values to compare
 * \return sort structure, use ObitSortStruc to access
 *  should be g_freeed when done.
 */
static gpointer 
MakeSortStruct (ObitTable *in, gint which[2], gboolean desc,
	        gint *size, gint *number, gint *ncomp,
	        ObitInfoType *type, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gpointer out = NULL;
  ObitTableRow *row;
  ObitSortStruct *entry;
  glong irow, nrow, tsize, count, col, cell, byteOffset, i;
  ObitInfoType itype;
  gint32 dim[MAXINFOELEMDIM];
  /* Pointers for row data */
  gint8   *ptrint8;
  gint16  *ptrint16;
  gint    *ptrint;
  oint    *ptroint;
  glong   *ptrlong=NULL;
  guint8  *ptruint8;
  guint16 *ptruint16;
  guint   *ptruint;
  gulong  *ptrulong;
  gfloat  *ptrfloat;
  gdouble *ptrdouble;
  gchar   *ptrchar, *optr;
  gchar *routine = "MakeSortStruc";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return out;
  g_assert (ObitTableIsA(in));

  /* Get table info */
  nrow = in->myDesc->nrow;

  /* which column? Cell? */
  col  = which[0]-1;
  cell = which[1]-1;

  /* Column type */
  itype = in->myDesc->type[col];
  *type = itype;
  /* Only one integer type for sort */
  if (*type==OBIT_short)       *type = OBIT_int;
  else if (*type==OBIT_oint)   *type = OBIT_int;
  else if (*type==OBIT_long)   *type = OBIT_int;
  else if (*type==OBIT_byte)   *type = OBIT_int;
  else if (*type==OBIT_ubyte)  *type = OBIT_int;
  else if (*type==OBIT_ushort) *type = OBIT_int;
  else if (*type==OBIT_uint)   *type = OBIT_int;
  else if (*type==OBIT_ulong)  *type = OBIT_int;

  /* element size */
  dim[0] = 1; dim[1] = 1;dim[2] = 1;dim[3] = 1;dim[4] = 1;
  /* string size - one element of everything else */
  if (*type == OBIT_string) 
    dim[0] = in->myDesc->dim[col][0];
  *size = sizeof(gint) + ObitInfoElemSize(*type, dim);

  /* Total size of structure in case all rows valid */
  tsize = (*size) * (nrow);
  out = g_malloc(tsize);   /* create output structure */
  
  /* Compare 1 except for strings */
  *ncomp = 1;
  if (*type == OBIT_string) *ncomp = dim[0];

  /* Create table row */
  row = newObitTableRow (in);

  byteOffset = in->myDesc->byteOffset[col];  /* where it starts in row data */

 /* loop over table */
  irow = 0;
  count = 0;
  retCode = OBIT_IO_OK;
  while (retCode==OBIT_IO_OK) {
    irow++;
   retCode = ObitTableReadRow (in, irow, row, err);
    if (retCode == OBIT_IO_EOF) break;
    if ((retCode != OBIT_IO_OK) || (err->error)) 
      Obit_traceback_val (err, routine, in->name, out);

    /* add to structure */
    entry = (ObitSortStruct*)(out + count * (*size));  /* set pointer to entry */
    entry->index = irow;

    /* Add data by type */
    switch (itype) { 
    case OBIT_float:
      ptrfloat = (gfloat*)(row->myRowData+byteOffset);
      if (desc) entry->key.flt = -(gfloat)ptrfloat[cell];
      else entry->key.flt      =  (gfloat)ptrfloat[cell];
      break;
    case OBIT_double:
      ptrdouble = (gdouble*)(row->myRowData+byteOffset);
      if (desc) entry->key.dbl = -(gdouble)ptrdouble[cell];
      else entry->key.dbl      =  (gdouble)ptrdouble[cell];
      break;
    case OBIT_string:
      ptrchar = (gchar*)(row->myRowData+byteOffset);
      ptrchar += cell*dim[0];  /* Entry in column */
      optr = &entry->key.str;
      for (i=0; i<dim[0]; i++) { /* copy string */
	optr[i] = ptrchar[i];
      }
      break;
    case OBIT_int:
      ptrint = (gint*)(row->myRowData+byteOffset);
      if (desc) entry->key.itg = -(gint)ptrint[cell];
      else entry->key.itg      =  (gint)ptrint[cell];
      break;
    case OBIT_byte:
      ptrint8 = (gint8*)(row->myRowData+byteOffset);
      if (desc) entry->key.itg = -(gint)ptrint8[cell];
      else entry->key.itg      =  (gint)ptrint8[cell];
      break;
    case OBIT_short:
      ptrint16 = (gint16*)(row->myRowData+byteOffset);
      if (desc) entry->key.itg = -(gint)ptrint16[cell];
      else entry->key.itg      =  (gint)ptrint16[cell];
      break;
     case OBIT_oint:
      ptroint = (oint*)(row->myRowData+byteOffset);
      if (desc) entry->key.itg = -(gint)ptroint[cell];
      else entry->key.itg      =  (gint)ptroint[cell];
      break;
    case OBIT_long:
      ptrlong = (glong*)(row->myRowData+byteOffset);
      if (desc) entry->key.itg = -(gint)ptrlong[cell];
      else entry->key.itg      =  (gint)ptrlong[cell];
      break;
    case OBIT_uint:
      ptruint = (guint*)(row->myRowData+byteOffset);
      if (desc) entry->key.itg = -(gint)ptruint[cell];
      else entry->key.itg      =  (gint)ptruint[cell];
      break;
    case OBIT_ubyte:
      ptruint8 = (guint8*)(row->myRowData+byteOffset);
      if (desc) entry->key.itg = -(gint)ptruint8[cell];
      else entry->key.itg      =  (gint)ptruint8[cell];
      break;
    case OBIT_ushort:
      ptruint16 = (guint16*)(row->myRowData+byteOffset);
      if (desc) entry->key.itg = -(gint)ptruint16[cell];
      else entry->key.itg      =  (gint)ptruint16[cell];
      break;
    case OBIT_ulong:
      ptrulong = (gulong*)(row->myRowData+byteOffset);
      if (desc) entry->key.itg = -(gint)ptrlong[cell];
      else entry->key.itg      =  (gint)ptrlong[cell];
      break;
    case OBIT_complex:
    case OBIT_dcomplex:
    case OBIT_bool:
    case OBIT_bits:
    default:
      g_assert_not_reached(); /* unknown, barf */
    }; /* end switch */

    count++;  /* How many valid */
  } /* end loop over file */
  
  /* check for errors */
  if ((retCode > OBIT_IO_EOF) || (err->error))
    Obit_traceback_val (err, routine, in->name, out);
  
  /* Release table row */
  row = ObitTableRowUnref (row);
  
  /* Actual number */
  *number = count;

  return out;
} /* end MakeSortStruc */ 

/**
 * Compare two lists of integers
 * Conformant to function type GCompareDataFunc
 * \param in1   First list, preceeded by gint index
 * \param in2   Second list, preceeded by gint index
 * \param ncomp Number of values to compare
 * \return <0 -> in1 < in2; =0 -> in1 == in2; >0 -> in1 > in2; 
 */
static gint CompareInt (gconstpointer in1, gconstpointer in2, 
			gpointer ncomp)
{
  gint out = 0;
  gint *int1, *int2, nc, i;

  /* get correctly typed local values */
  int1 = (gint*)(in1 + sizeof(gint));
  int2 = (gint*)(in2 + sizeof(gint));
  nc = *(gint*)ncomp;

  /* List or single value? */
  if (nc==1) {
    out = *int1 - *int2;
  } else { /* list */
    for (i=0; i<nc; i++) {
      out = int1[i] - int2[i];
      if (out) break;   /* stop at first not equal */
    }
  }

  return out;
} /* end CompareInt */

/**
 * Compare two lists of floats
 * Conformant to function type GCompareDataFunc
 * \param in1   First list, preceeded by gint index
 * \param in2   Second list, preceeded by gint index
 * \param ncomp Number of values to compare
 * \return <0 -> in1 < in2; =0 -> in1 == in2; >0 -> in1 > in2; 
 */
static gint CompareFloat (gconstpointer in1, gconstpointer in2, 
			  gpointer ncomp)
{
  gint out = 0;
  gint nc, i;
  gfloat *float1, *float2;

  /* get correctly typed local values */
  float1 = (float*)(in1 + sizeof(gint));
  float2 = (float*)(in2 + sizeof(gint));
  nc = *(gint*)ncomp;

  /* List or single value? */
  if (nc==1) {
    if (*float1<*float2)      out = -1;
    else if (*float1>*float2) out = 1;
    else                      out = 0;
  } else { /* list */
    for (i=0; i<nc; i++) {
      if (float1[i]<float2[i])      out = -1;
      else if (float1[i]>float2[i]) out = 1;
      else                          out = 0;
      if (out) break;   /* stop at first not equal */
    }
  }

  return out;
} /* end CompareFloat */

/**
 * Compare two lists of double
 * Conformant to function type GCompareDataFunc
 * \param in1   First list, preceeded by gint index
 * \param in2   Second list, preceeded by gint index
 * \param ncomp Number of values to compare
 * \return <0 -> in1 < in2; =0 -> in1 == in2; >0 -> in1 > in2; 
 */
static gint CompareDouble (gconstpointer in1, gconstpointer in2, 
			   gpointer ncomp)
{
  gint out = 0;
  gint nc, i;
  gdouble *double1, *double2;

  /* get correctly typed local values */
  double1 = (double*)(in1 + sizeof(gint));
  double2 = (double*)(in2 + sizeof(gint));
  nc = *(gint*)ncomp;

  /* List or single value? */
  if (nc==1) {
    if (*double1<*double2)      out = -1;
    else if (*double1>*double2) out = 1;
    else                      out = 0;
  } else { /* list */
    for (i=0; i<nc; i++) {
      if (double1[i]<double2[i])      out = -1;
      else if (double1[i]>double2[i]) out = 1;
      else                          out = 0;
      if (out) break;   /* stop at first not equal */
    }
  }

  return out;
} /* end CompareDouble */

/**
 * Compare two (ascii) character strings
 * Conformant to function type GCompareDataFunc
 * \param in1   First list, preceeded by gint index
 * \param in2   Second list, preceeded by gint index
 * \param ncomp Number of characters to compare
 * \return <0 -> in1 < in2; =0 -> in1 == in2; >0 -> in1 > in2; 
 */
static gint CompareString (gconstpointer in1, gconstpointer in2, 
			   gpointer ncomp)
{
  gint out = 0;
  gint nc, i;
  gchar *string1, *string2;

  /* get correctly typed local values */
  string1 = (gchar*)(in1 + sizeof(gint));
  string2 = (gchar*)(in2 + sizeof(gint));
  nc = *(gint*)ncomp;

  /* List or single value? */
  if (nc==1) {
    out = *string1 - *string2;
  } else { /* list */
    for (i=0; i<nc; i++) {
      out = string1[i] - string2[i];
      if (out) break;   /* stop at first not equal */
    }
  }

  return out;
} /* end CompareString */

/**
 * Reorders table
 * Makes scratch image with a scratch table for the reordered
 * table and then copies over the input table.
 * \param in      Table to sort
 * \param base    Base address of sort structure
 * \param size    Size in bytes of a sort element
 * \param number  Number of sort elements
 * \param sortCol Sort column number (1-rel)
 * \param err     ObitErr error stack.
 * \return I/O Code  OBIT_IO_OK = OK.
 */
static ObitIOCode 
ReorderTable(ObitTable *in, gpointer base, gint size, gint number, 
	     gint sortCol, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitImage *tmpImage = NULL, *scrImage = NULL;
  ObitTable *scrTable = NULL;
  ObitTableRow *row;
  ObitSortStruct *entry;
  glong count, irow, orow, tabVer;
  gchar *routine = "ReorderTable";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;

  /* generate new image to use in generating scratch table */
  tmpImage = newObitImage(routine);

  /* Fake info about table file type, IO size, ... */
  tmpImage->mySel->FileType = in->mySel->FileType;
  tmpImage->myDesc->IOsize  = OBIT_IO_byPlane;

  /* scratch image */
  scrImage = newObitImageScratch (tmpImage, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, retCode);

  /* Make scratch table */
  tabVer = 0;
  scrTable = newObitImageTable(scrImage, OBIT_IO_WriteOnly, in->myDesc->TableName, 
			       &tabVer, err);
  if ((scrTable==NULL) || (err->error)) Obit_traceback_val (err, routine, in->name, retCode);

  /* Clone input table */
  scrTable = ObitTableClone (in, scrTable);
 
 /* need independent descriptors */
  scrTable->myDesc = ObitUnref(scrTable->myDesc); /* release old */
  scrTable->myDesc = ObitTableDescCopy(in->myDesc,scrTable->myDesc, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, retCode);

  /* Open input table */
  retCode = ObitTableOpen (in, OBIT_IO_ReadWrite, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, in->name, retCode);

  /* Create table row */
  row = newObitTableRow (in);

  retCode = ObitTableOpen (scrTable, OBIT_IO_WriteOnly, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, scrTable->name, retCode);
  /* loop over table */
  irow = 0;
  count = 0;
  while (retCode==OBIT_IO_OK) {
 
    /* which row - get from sort structure */
    entry = base + count * (size);  /* set pointer to entry */
    irow = entry->index;
    retCode = ObitTableReadRow (in, irow, row, err);
    if (retCode == OBIT_IO_EOF) break;
    if ((retCode != OBIT_IO_OK) || (err->error)) 
      Obit_traceback_val (err, routine, in->name, retCode);

    orow = -1;
    retCode = ObitTableWriteRow (scrTable, orow, row, err);
    if ((retCode != OBIT_IO_OK) || (err->error)) 
      Obit_traceback_val (err, routine, scrTable->name, retCode);

    count++;
  } /* end loop reordering table table */

  /* Mark table as sorted */
  scrTable->myDesc->sort[0] = sortCol;
  scrTable->myDesc->sort[1] = 0;
  ((ObitTableDesc*)scrTable->myIO->myDesc)->sort[0] = sortCol;
  ((ObitTableDesc*)scrTable->myIO->myDesc)->sort[1] = 0;

  /* Truncate input table in preparation for copy back */
  in->myDesc->nrow = 0;
  ((ObitTableDesc*)in->myIO->myDesc)->nrow = 0;
 
  retCode = ObitTableClose (in, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, in->name, retCode);
  retCode = ObitTableClose (scrTable, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, scrTable->name, retCode);

  /* Copy table back */
  ObitTableCopy (scrTable, in, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, scrTable->name, retCode);

  /* Cleanup */
  row = ObitTableRowUnref (row); /* Release table row */
  tmpImage = ObitImageUnref (tmpImage);
  scrTable = ObitTableUnref(scrTable);
  scrImage = ObitImageZap (scrImage, err);
  scrImage = ObitImageUnref (scrImage);
  if (err->error) Obit_traceback_val (err, routine, scrImage->name, retCode);
 
  return OBIT_IO_OK;
} /* end ReorderTable */
