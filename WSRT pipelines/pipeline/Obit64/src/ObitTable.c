/* $Id: ObitTable.c,v 1.17 2005/07/11 15:11:11 bcotton Exp $       */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "ObitTable.h"
#include "ObitIOTableFITS.h"
#include "ObitIOTableAIPS.h"
#include "ObitTableDesc.h"
#include "ObitTableSel.h"
#include "ObitMem.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitTable.c
 * ObitTable class function definitions.
 * This class is derived from the Obit base class.
 */

/** name of the class defined in this file */
static gchar *myClassName = "ObitTable";

/** name of the Row class defined in this file */
static gchar *myRowClassName = "ObitTableRow";

/*--------------- File Global Variables  ----------------*/
/*----------------  Table Row  ----------------------*/
/**
 * ClassInfo structure ObitTableClassInfo.
 * This structure is used by class objects to access class functions.
 */
static OBITTableRowClassInfo myRowClassInfo = {FALSE};

/*------------------  Table  ------------------------*/
/**
 * ClassInfo structure ObitTableClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitTableClassInfo myClassInfo = {FALSE};

/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated Row object. */
void  ObitTableRowInit  (gpointer in);

/** Private: Deallocate Row members. */
void  ObitTableRowClear (gpointer in);

/** Private: Initialize newly instantiated object. */
void  ObitTableInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitTableClear (gpointer in);

/** Private: Read selection parameters from ObitInfoList. */
static void ObitTableGetSelect (ObitInfoList *info, ObitTableSel *sel,
				ObitErr *err);
/*----------------------Public functions---------------------------*/
/*------------------  Table Row ------------------------*/
/**
 * Constructor.
 * Initializes Row class if needed on first call.
 * \param name An optional name for the object.
 * \return the new object.
 */
ObitTableRow* newObitTableRow (ObitTable *table)
{
  ObitTableRow* out;
  gchar *name;

  /* Class initialization if needed */
  if (!myRowClassInfo.initialized) ObitTableRowClassInit();

  /* allocate/init structure */
  name =  g_strconcat ("TRow:", table->tabType, NULL);
  out = ObitMemAlloc0Name(sizeof(ObitTableRow), name);
  g_free(name);

  /* initialize values */
  out->name = g_strdup("Table Row");

  /* set ClassInfo */
  out->ClassInfo = (gpointer)&myRowClassInfo;

  /* initialize other stuff */
  ObitTableRowInit((gpointer)out);
  out->myTable   = ObitTableRef((ObitTable*)table);

 return out;
} /* end newObitTableRow */

/**
 * Returns ClassInfo pointer for the Row class.
 * \return pointer to the Row class structure.
 */
gconstpointer ObitTableRowGetClass (void)
{
  /* Class initialization if needed */
  if (!myRowClassInfo.initialized) ObitTableRowClassInit();

  return (gconstpointer)&myRowClassInfo;
} /* end ObitTableRowGetClass */

/**
 * Initialize global ClassInfo Structure.
 */
void ObitTableRowClassInit (void)
{
  const ObitClassInfo *ParentClass;

  if (myRowClassInfo.initialized) return;  /* only once */
  myRowClassInfo.initialized = TRUE;

   /* Initialize (recursively) parent class first */
  ParentClass = ObitGetClass();
  if ((ParentClass!=NULL) && (ParentClass->ObitClassInit!=NULL))
    ParentClass->ObitClassInit();

  /* function pointers etc. for this class */
  myRowClassInfo.ClassName       = g_strdup(myRowClassName);
  myRowClassInfo.ParentClass     = ParentClass;
  myRowClassInfo.ObitClassInit   = (ObitClassInitFP)ObitTableRowClassInit;
  myRowClassInfo.newObit         = NULL;
  myRowClassInfo.newObitTableRow = (newObitTableRowFP)newObitTableRow;
  myRowClassInfo.ObitCopy        = NULL;
  myRowClassInfo.ObitClone       = NULL;  
  myRowClassInfo.ObitRef         = (ObitRefFP)ObitRef;
  myRowClassInfo.ObitUnref       = (ObitUnrefFP)ObitUnref;
  myRowClassInfo.ObitIsA         = (ObitIsAFP)ObitIsA;
  myRowClassInfo.ObitClear       = (ObitClearFP)ObitTableRowClear;
  myRowClassInfo.ObitInit        = (ObitInitFP)ObitTableRowInit;
} /* end ObitTableRowClassInit */

/*------------------  Table  ------------------------*/
/**
 * Constructor.
 * Initializes class if needed on first call.
 * \param name An optional name for the object.
 * \return the new object.
 */
ObitTable* newObitTable (gchar* name)
{
  ObitTable* out;
  gchar *lname;

  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitTableClassInit();

  /* allocate/init structure */
  lname =  g_strconcat ("Tab:", name, NULL);
  out = ObitMemAlloc0Name(sizeof(ObitTable), lname);
  g_free(lname);

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

  /* set ClassInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  ObitTableInit((gpointer)out);

 return out;
} /* end newObitTable */

/**
 * Returns ClassInfo pointer for the class.
 * \return pointer to the class structure.
 */
gconstpointer ObitTableGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitTableClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitTableGetClass */

/**
 * Delete underlying files and the basic object.
 * \param in Pointer to object to be zapped.
 * \param err ObitErr for reporting errors.
 * \return pointer for input object, NULL if deletion successful
 */
ObitTable* ObitTableZap (ObitTable *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gchar *routine = "ObitTableZap";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return in;
  if (in==NULL) return NULL;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));


  /* Open and close if needed */
  if (in->myIO==NULL) {
    in->bufferSize = -1;  /* Don't need buffer */
    retCode = ObitTableOpen (in, OBIT_IO_ReadWrite, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
      Obit_traceback_val (err, routine, in->name, in);
    retCode = ObitTableClose (in, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
      Obit_traceback_val (err, routine, in->name, in);
  }
  
  /* Delete the file */
  ObitIOZap (in->myIO, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, in);

  /* in->myIO = ObitIOUnref(in->myIO);        delete IO */
  /* in->info = ObitInfoListUnref(in->info);  delete infoList */

  /* Get memory resident bits as well */
  while (in) in = ObitTableUnref(in);

  return in;
} /* end ObitTableZap */

/**
 * Make a deep copy of input object.
 * Copies are made of complex members including disk files; these 
 * will be copied applying whatever selection is associated with the input.
 * Objects should be closed on input and will be closed on output.
 * In order for the disk file structures to be copied, the output file
 * must be sufficiently defined that it can be written.
 * The copy will be attempted but no errors will be logged until
 * both input and output have been successfully opened.
 * ObitInfoList and ObitThread members are only copied if the output object
 * didn't previously exist.
 * Parent class members are included but any derived class info is ignored.
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \param err Error stack, returns if not empty.
 * \return pointer to the new object.
 */
ObitTable* ObitTableCopy (ObitTable *in, ObitTable *out, ObitErr *err)
{
  const ObitClassInfo *ParentClass;
  ObitIOCode iretCode, oretCode;
  gboolean oldExist;
  gchar *outName;
  gchar *routine = "ObitTableCopy";

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return NULL;
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));

  /* Create if it doesn't exist */
  oldExist = out!=NULL;
  if (!oldExist) {
    /* derive object name */
    outName = g_strconcat ("Copy: ",in->name,NULL);
    out = newObitTable(outName);
    if (outName) g_free(outName); outName = NULL;
    if (out->tabType) g_free(out->tabType); out->tabType = NULL;
    if (in->tabType) out->tabType = g_strdup(in->tabType);
 }

  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);

  /* copy/set this classes additions only if out newly created */
  if (!oldExist) {
    /* copy */
    out->myDesc = (gpointer)ObitTableDescCopy(in->myDesc, out->myDesc, err);
    /* Don't copy selector */
    if (out->mySel) out->mySel = ObitUnref (out->mySel);
    out->mySel = newObitTableSel (out->name);
    out->info = ObitInfoListUnref(out->info);
    out->info = ObitInfoListRef(in->info);
    /* don't copy ObitTableSel, ObitThread */
  }

  /* If the output object was created this call it cannot be fully
     defined so we're done */
  if (!oldExist) return out;

  /* if input has file designated, copy data */
  /* test open to fully instantiate input and see if it's OK */
  iretCode = ObitTableOpen (in, OBIT_IO_ReadOnly, err);
  /* if it didn't work bail out */
  if ((iretCode!=OBIT_IO_OK) || (err->error)) {
    Obit_traceback_val (err, routine,in->name, out);
  }

  /* copy Descriptor - this time with full information */
  out->myDesc = ObitTableDescCopy(in->myDesc, out->myDesc, err);


 /* use same data buffer on input and output 
     so don't assign buffer for output */
  if (out->buffer) ObitIOFreeBuffer(out->buffer); /* free existing */
  out->buffer     = NULL;
  out->bufferSize = -1;

  /* test open output */
  oretCode = ObitTableOpen (out, OBIT_IO_WriteOnly, err);
  /* if it didn't work bail out */
  if ((oretCode!=OBIT_IO_OK) || (err->error)) {
    /* unset output buffer (may be multiply deallocated) */
    out->buffer = NULL;
    out->bufferSize = 0;
    Obit_traceback_val (err, routine,in->name, out);
  }

  /* we're in business, copy */
  while ((iretCode==OBIT_IO_OK) && (oretCode==OBIT_IO_OK)) {
    iretCode = ObitTableRead (in, -1, in->buffer, err);
    /* How many */
    out->myDesc->numRowBuff = in->myDesc->numRowBuff;
    if (iretCode!=OBIT_IO_OK) break;
    oretCode = ObitTableWrite (out, -1, in->buffer, err);
  }
  
  /* unset output buffer (may be multiply deallocated ;'{ ) */
  out->buffer = NULL;
  out->bufferSize = 0;
  
  /* check for errors */
  if ((iretCode > OBIT_IO_EOF) || (oretCode > OBIT_IO_EOF) ||
      (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,in->name, out);
  
  /* close files to be sure */
  iretCode = ObitTableClose (in, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,in->name, out);
  
  /* close files to be sure */
  oretCode = ObitTableClose (out, err);
  if ((oretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,out->name, out);
  
  return out;
} /* end ObitTableCopy */

/**
 * Make a shallow copy of a object.
 * The result will have pointers to the more complex members.
 * Parent class members are included but any derived class info is ignored.
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \return pointer to the new object.
 */
ObitTable* ObitTableClone  (ObitTable *in, ObitTable *out)
{
  const ObitClassInfo *myClass, *ParentClass;
  gboolean oldExist;
  gchar *outName;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));

  /* Create if it doesn't exist */
  oldExist = out!=NULL;
  if (!oldExist) {
    /* derive object name */
    outName = g_strconcat ("Clone: ",in->name,NULL);
    out = newObitTable(outName);
    if (outName) g_free(outName); outName = NULL;
  }

  /* shallow copy any parent class members */
   myClass     = in->ClassInfo;
   ParentClass = myClass->ParentClass;
   g_assert ((ParentClass!=NULL) && (ParentClass->ObitClone!=NULL));
   ParentClass->ObitClone ((Obit*)in, (Obit*)out);

   if (!oldExist) { /* only copy ObitInfoList if just created */
     out->info = ObitInfoListUnref(out->info);
     out->info = ObitInfoListRef(in->info);
   }
     
   /* copy/set this classes additions */
   /* don't copy ObitTableSel, ObitThread or ObitInfoList */
   out->myDesc = ObitUnref(out->myDesc); /* release old */
   out->myDesc = ObitRef(in->myDesc);    /* shellow copy */
   if (out->tabType) g_free(out->tabType); out->tabType = NULL;
   if (in->tabType) out->tabType = g_strdup(in->tabType);

  return out;
} /* end ObitTableClone */

/**
 * Copy the contents of table in to the end of table out
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \param err Error stack, returns if not empty.
 * \return pointer to the new object.
 */
void ObitTableConcat (ObitTable *in, ObitTable *out, ObitErr *err)
{
  ObitIOCode iretCode, oretCode;
  glong outRec;
  gchar *routine = "ObitTableConcat";

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));

  /* test open to fully instantiate input and see if it's OK */
  iretCode = ObitTableOpen (in, OBIT_IO_ReadOnly, err);
  /* if it didn't work bail out */
  if ((iretCode!=OBIT_IO_OK) || (err->error)) {
    Obit_traceback_msg (err, routine, in->name);
  }

 /* use same data buffer on input and output 
     so don't assign buffer for output */
  if (out->buffer) ObitIOFreeBuffer(out->buffer); /* free existing */
  out->buffer     = NULL;
  out->bufferSize = -1;

  /* test open output */
  oretCode = ObitTableOpen (out, OBIT_IO_ReadWrite, err);
  /* if it didn't work bail out */
  if ((oretCode!=OBIT_IO_OK) || (err->error)) {
    /* unset output buffer (may be multiply deallocated) */
    out->buffer = NULL;
    out->bufferSize = 0;
    Obit_traceback_msg (err, routine, in->name);
  }

  /* Check that the two tables are compatable */
  if (!ObitTableDescCompatible (in->myDesc, out->myDesc)) {
    Obit_log_error(err, OBIT_Error, 
		   "%s: Tables %s and %s incompatible", 
		   routine, in->name, out->name);
    return;
  }

  /* start writing at end */
  outRec = out->myDesc->nrow + 1;

  /* we're in business, copy */
  while ((iretCode==OBIT_IO_OK) && (oretCode==OBIT_IO_OK)) {
    iretCode = ObitTableRead (in, -1, in->buffer, err);
    /* How many */
    out->myDesc->numRowBuff = in->myDesc->numRowBuff;
    if (iretCode!=OBIT_IO_OK) break;
    oretCode = ObitTableWrite (out, outRec, in->buffer, err);
    outRec += out->myDesc->numRowBuff;
  }
  
  /* unset output buffer (may be multiply deallocated ;'{ ) */
  out->buffer = NULL;
  out->bufferSize = 0;
  
  /* check for errors */
  if ((iretCode > OBIT_IO_EOF) || (oretCode > OBIT_IO_EOF) ||
      (err->error)) /* add traceback,return */
    Obit_traceback_msg (err, routine, in->name);
  
  /* close files to be sure */
  iretCode = ObitTableClose (in, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_msg (err, routine, in->name);
  
  /* close files to be sure */
  oretCode = ObitTableClose (out, err);
  if ((oretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_msg (err, routine, out->name);
  
  return;
} /* end ObitTableConcat */

/**
 * Initialize structures and open file.
 * The image descriptor is read if OBIT_IO_ReadOnly or 
 * OBIT_IO_ReadWrite and written to disk if opened OBIT_IO_WriteOnly.
 * After the file has been opened the member, buffer is initialized
 * for reading/storing the table unless member bufferSize is <0.
 * If the requested version ("Ver" in InfoList) is 0 then the highest
 * numbered table of the same type is opened on Read or Read/Write, 
 * or a new table is created on on Write.
 * The file etc. info should have been stored in the ObitInfoList:
 * \li "FileType" OBIT_int scalar = OBIT_IO_FITS or OBIT_IO_AIPS 
 *               for file type (see class documentation for details).
 * \li "nRowPIO" OBIT_int scalar = Maximum number of table rows
 *               per transfer, this is the target size for Reads (may be 
 *               fewer) and is used to create buffers.
 * \param in     Pointer to object to be opened.
 * \param access access (OBIT_IO_ReadOnly,OBIT_IO_ReadWrite,
 *               or OBIT_IO_WriteOnly).
 *               If OBIT_IO_WriteOnly any existing data in the output file
 *               will be lost.
 * \param err ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitTableOpen (ObitTable *in, ObitIOAccess access, 
			  ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  glong need;
  gchar *routine = "ObitTableOpen";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));

  /* Same type of access on descriptor */
  in->myDesc->access = access;

  /* If the file is already open - close it  first */
  if ((in->myStatus==OBIT_Active) || (in->myStatus==OBIT_Modified)) {
    retCode = ObitTableClose (in, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
      Obit_traceback_val (err, routine, in->name, retCode);
  }

  /* get selection parameters */
  ObitTableGetSelect (in->info, in->mySel, err);
    if (err->error) /* add traceback,return on error */
      Obit_traceback_val (err, routine,in->name, retCode);

  /* create appropriate ObitIO */
  /* unlink any existing IO structure */
  in->myIO = ObitUnref (in->myIO);
  if (in->mySel->FileType==OBIT_IO_FITS) {
    in->myIO = (ObitIO*)newObitIOTableFITS(in->name, in->info, err);
    /* copy selector pointer */
    ((ObitIOTableFITS*)in->myIO)->mySel = 
      ObitUnref(((ObitIOTableFITS*)in->myIO)->mySel);
    ((ObitIOTableFITS*)in->myIO)->mySel = ObitRef(in->mySel);
    /* copy descriptor */
    ((ObitIOTableFITS*)in->myIO)->myDesc = 
      ObitTableDescCopy(in->myDesc, 
		     ((ObitIOTableFITS*)in->myIO)->myDesc, err);

  } else if (in->mySel->FileType==OBIT_IO_AIPS) {
    in->myIO = (ObitIO*)newObitIOTableAIPS(in->name, in->info, err);
    /* copy selector pointer */
    ((ObitIOTableAIPS*)in->myIO)->mySel = 
      ObitUnref(((ObitIOTableAIPS*)in->myIO)->mySel);
    ((ObitIOTableAIPS*)in->myIO)->mySel = ObitRef(in->mySel);
    /* copy descriptor */
    ((ObitIOTableAIPS*)in->myIO)->myDesc = 
      ObitTableDescCopy(in->myDesc, 
		     ((ObitIOTableAIPS*)in->myIO)->myDesc, err);
  }

  in->myIO->access = access; /* save access type */

 /* most of the instructions for the I/O are in the ObitInfoList */
  retCode = ObitIOOpen (in->myIO, access, in->info, err);
  if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, in->name, retCode);

  /* read or write Headers */
  if ((access == OBIT_IO_ReadOnly) || (access == OBIT_IO_ReadWrite)) {
    /* read header info */
    retCode = ObitIOReadDescriptor(in->myIO, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
      Obit_traceback_val (err, routine, in->name, retCode);
  } 

  if ((access == OBIT_IO_ReadWrite) || (access == OBIT_IO_WriteOnly)) {
    /* Write header info */
    retCode = ObitIOWriteDescriptor(in->myIO, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
      Obit_traceback_val (err, routine, in->name, retCode);
  }

  /* Set descriptors for the output on in to reflect the selection
     by in->mySel,  the descriptors on in->myIO will still describe
     the external representation */
  ObitTableSelSetDesc ((ObitTableDesc*)in->myIO->myDesc,
    (ObitTableSel*)in->myIO->mySel, in->myDesc, err);
  if (err->error) /* add traceback,return on error */
    Obit_traceback_val (err, routine, in->name, retCode);

  /* Allocate buffer - resize if necessary */
  /* buffer size < 0 => no buffer desired */
  if (in->bufferSize >= 0) {
    need = ObitTableSelBufferSize(in->myDesc, in->mySel);
    /* is current one big enough? */
    if ((in->buffer!=NULL) && (need>in->bufferSize)) {
      /* no - deallocate */
      if (in->buffer) ObitIOFreeBuffer(in->buffer);
      in->buffer = NULL;
      in->bufferSize = 0;
    }
    /* new one if needed */
    if (in->buffer==NULL)  
      ObitIOCreateBuffer (&in->buffer, &in->bufferSize, in->myIO, 
			  in->info, err);
  } /* end buffer allocation */

  /* init I/O */
  retCode = ObitIOSet (in->myIO, in->info, err);
  if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, in->name, retCode);

  /* Set I/O to beginning of the file */
  ((ObitTableDesc*)in->myIO->myDesc)->firstRow = 0;
  ((ObitTableDesc*)in->myIO->myDesc)->numRowBuff = 0;
  /* For WriteOnly the file is truncated.*/
  if (access == OBIT_IO_WriteOnly) {
    ((ObitTableDesc*)in->myIO->myDesc)->firstRow = 1;
    ((ObitTableDesc*)in->myIO->myDesc)->nrow = 0;
    in->myDesc->nrow = 0;
  }

  /* set Status */
  in->myStatus = OBIT_Active;

  /* save current location */
  in->myDesc->firstRow   = ((ObitTableDesc*)in->myIO->myDesc)->firstRow;
  in->myDesc->numRowBuff = ((ObitTableDesc*)in->myIO->myDesc)->numRowBuff;
  in->myDesc->nrow       = ((ObitTableDesc*)in->myIO->myDesc)->nrow;
  return retCode;
} /* end ObitTableOpen */

/**
 * Shutdown I/O.
 * \param in Pointer to object to be closed.
 * \param err ObitErr for reporting errors.
 * \return error code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitTableClose (ObitTable *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gchar *routine = "ObitTableClose";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));
  /* Something going on? */
  if (in->myStatus == OBIT_Inactive) return OBIT_IO_OK;

  /* flush buffer if writing */
  if (((in->myIO->access==OBIT_IO_ReadWrite) || 
       (in->myIO->access==OBIT_IO_WriteOnly)) &&
      (in->myStatus == OBIT_Modified)) {
    retCode = ObitIOFlush (in->myIO, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
      Obit_traceback_val (err, routine, in->name, retCode);

    /* Update descriptor on myIO */
    ObitTableDescCopyDesc(in->myDesc, (ObitTableDesc*)in->myIO->myDesc, err);
    if (err->error)
      Obit_traceback_val (err, routine, in->name, retCode);

    /* Update header on disk if writing */
    retCode = OBIT_IO_OK;
    if (in->myIO->myStatus != OBIT_Inactive)
      retCode = ObitIOWriteDescriptor(in->myIO, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
      Obit_traceback_val (err, routine, in->name, retCode);    
  }

  /* Close actual file */
  retCode = ObitIOClose (in->myIO, err);
  if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, in->name, retCode);

  /* Delete buffer */
  if (in->buffer)  ObitIOFreeBuffer(in->buffer); in->buffer = NULL;
  in->bufferSize = 0; 

  /* set Status */
  in->myStatus = OBIT_Inactive;

  return retCode;
} /* end ObitTableClose */

/**
 * Ensures full instantiation of object - basically open to read/write header
 * and verify or create file.
 * If object has previously been opened, as demonstrated by the existance
 * of its myIO member, this operation is a no-op.
 * Virtual - calls actual class member
 * \param in     Pointer to object
 * \param exist  TRUE if object should previously exist, else FALSE
 * \param err    ObitErr for reporting errors.
 * \return error code, OBIT_IO_OK=> OK
 */
void ObitTableFullInstantiate (ObitTable *in, gboolean exist, ObitErr *err)
{
  ObitIOAccess access;
  gchar *routine = "ObitTableFullInstantiate";

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

  if (in->myIO) return;  /* is this needed */

  /* Open readonly if it should exist, else writeonly */
  if (exist) access = OBIT_IO_ReadOnly;
  else access = OBIT_IO_WriteOnly;

  in->bufferSize = -1;  /* Don't need to assign buffer here */

  /* Open and close */
  ObitTableOpen(in, access, err);
  ObitTableClose(in, err);
  if (err->error)Obit_traceback_msg (err, routine, in->name);
  in->bufferSize = 0;  /* May need buffer later */
} /* end ObitTableFullInstantiate */

/**
 * Remove any previously existing rows and fully instantiate.
 * \param in     Pointer to object
 * \param err    ObitErr for reporting errors.
 * \return error code, OBIT_IO_OK=> OK
 */
void ObitTableClearRows (ObitTable *in, ObitErr *err)
{
  gchar *routine = "ObitTableClearRows";

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

  in->bufferSize = -1;  /* Don't need to assign buffer here */

  /* Open and close */
  ObitTableOpen(in, OBIT_IO_ReadWrite, err);
  if (err->error)Obit_traceback_msg (err, routine, in->name);

  /* reset count to zero */
  in->myDesc->nrow = 0;
  /* The one that counts is in the IO */
  ((ObitTableDesc*)(in->myIO->myDesc))->nrow = 0;
  /* Mark as changed */
  in->myStatus = OBIT_Modified;
  
  ObitTableClose(in, err);
  if (err->error)Obit_traceback_msg (err, routine, in->name);
  in->bufferSize = 0;  /* May need buffer later */
} /* end ObitTableClearRows */

/**
 * Read table data from disk.
 * The ObitTableDesc maintains the current location in the table.
 * The number read will be mySel->nRowPIO (until the end of the selected
 * range of rows in which case it will be smaller).
 * The first row number after a read is myDesc->firstRow
 * and the number of row is myDesc->numRowBuff.
 * If there are existing rows in the buffer marked as modified 
 * ("_status" column value =1) the buffer is rewritten to disk before 
 * the new buffer is read.
 * \param in Pointer to object to be read.
 * \param rowno Row number to start reading, -1 = next;
 * \param data pointer to buffer to write results.
 *             if NULL, use the buffer member of in.
 * \param err ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK => OK
 */
ObitIOCode ObitTableRead (ObitTable *in, glong rowno, gfloat *data, 
			  ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitIOAccess access;
  gfloat *myBuf = data;
  glong need;
  gchar *routine = "ObitTableRead";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));

 /* check and see if its open - if not attempt */
  if ((in->myStatus!=OBIT_Active) && (in->myStatus!=OBIT_Modified)) {
    access = OBIT_IO_ReadOnly;
    retCode = ObitIOOpen (in->myIO, access, in->info, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback, return */
      Obit_traceback_val (err, routine, in->name, retCode);
  }

  /* Read enabled? */
  Obit_retval_if_fail(((in->myIO->access==OBIT_IO_ReadWrite)||
		       (in->myIO->access==OBIT_IO_ReadOnly)), err, retCode,
		      "%s: Read not enabled for %s", routine, in->name);

  /* select internal or external buffer */
  if (myBuf==NULL) {
    myBuf = in->buffer;
    /* Check that internal buffer (defined in bytes) large enough */
    need = ObitTableSelBufferSize (in->myDesc, in->mySel);
    if (need > in->bufferSize) {
      Obit_log_error(err, OBIT_Error, 
		     "IO buffer (%ld) too small, need %ld for %s", 
		     in->bufferSize, need, in->name);
      return retCode;
    }
  } 
  g_assert (myBuf != NULL); /* check it */

  retCode = ObitIOReadRow (in->myIO, rowno, myBuf, err);
  if ((retCode > OBIT_IO_EOF) || (err->error)) 
    Obit_traceback_val (err, routine, in->name, retCode);

  /* save current location */
  in->myDesc->firstRow   = ((ObitTableDesc*)in->myIO->myDesc)->firstRow;
  in->myDesc->numRowBuff = ((ObitTableDesc*)in->myIO->myDesc)->numRowBuff;

  return retCode;
} /* end ObitTableRead */

/**
 * Read data from disk applying selection.
 * The number read will be mySel->nRowPIO (until the end of the selected
 * range of visibilities in which case it will be smaller).
 * The first visibility number after a read is myDesc->firstRow
 * and the number of visibilities is myDesc->numRowBuff.
 * If there are existing rows in the buffer marked as modified 
 * ("_status" column value =1) the buffer is rewritten to disk before 
 * the new buffer is read.
 * \param in Pointer to object to be read.
 * \param rowno Row number to start reading, -1 = next;
 * \param data pointer to buffer to write results.
 *             if NULL, use the buffer member of in.
 * \param err ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK => OK
 */
ObitIOCode ObitTableReadSelect (ObitTable *in, glong rowno, gfloat *data, 
				ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitIOAccess access;
  gfloat *myBuf = data;
  glong need;
  gchar *routine = "ObitTableReadSelect";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));

 /* check and see if its open - if not attempt */
  if ((in->myStatus!=OBIT_Active) && (in->myStatus!=OBIT_Modified)) {
    access = OBIT_IO_ReadOnly;
    retCode = ObitIOOpen (in->myIO, access, in->info, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback, return */
      Obit_traceback_val (err, routine, in->name, retCode);
  }

  /* Read enabled? */
  Obit_retval_if_fail(((in->myIO->access==OBIT_IO_ReadWrite)||
		       (in->myIO->access==OBIT_IO_ReadOnly)), err, retCode,
		      "%s: Read not enabled for %s", routine, in->name);

  /* select internal or external buffer */
  if (myBuf==NULL) {
    myBuf = in->buffer;
    /* Check that internal buffer ( defined in gfloats) large enough */
    need = ObitTableSelBufferSize (in->myDesc, in->mySel);
    if (need > in->bufferSize) {
      Obit_log_error(err, OBIT_Error, 
		     "IO buffer (%ld) too small, need %ld for %s", 
		     in->bufferSize, need, in->name);
      return retCode;
    }
  } 
  g_assert (myBuf != NULL); /* check it */

  retCode = ObitIOReadRowSelect (in->myIO, rowno, myBuf, err);
  if ((retCode > OBIT_IO_EOF) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, in->name, retCode);

  /* save current location */
  in->myDesc->firstRow   = ((ObitTableDesc*)in->myIO->myDesc)->firstRow;
  in->myDesc->numRowBuff = ((ObitTableDesc*)in->myIO->myDesc)->numRowBuff;

  return retCode;
} /* end ObitTableReadSelect */

/**
 * Write information to disk.
 * The data in the buffer will be written starting at visibility
 * myDesc->firstRow and the number written will be myDesc->numRowBuff
 * which should not exceed mySel->nRowPIO if the internal buffer is used.
 * myDesc->firstRow will be maintained and need not be changed for
 * sequential writing.
 * \param in Pointer to object to be written.
 * \param rowno Row number (1-rel) to start reading, -1 = next;
 * \param data pointer to buffer containing input data.
 *             if NULL, use the buffer member of in.
 * \param err ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitTableWrite (ObitTable *in, glong rowno, gfloat *data, 
			   ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitIOAccess access;
  gfloat *myBuf = data;
  glong need;
  gchar *routine = "ObitTableWrite";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));

  /* check and see if its open - if not attempt */
  if ((in->myStatus!=OBIT_Modified) && (in->myStatus!=OBIT_Active)) {
    access = OBIT_IO_WriteOnly;
    retCode = ObitIOOpen (in->myIO, access, in->info, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
      Obit_traceback_val (err, routine, in->name, retCode);
  }

  /* Write enabled? */
  Obit_retval_if_fail(((in->myIO->access==OBIT_IO_ReadWrite)||
		       (in->myIO->access==OBIT_IO_WriteOnly)), err, retCode,
		      "%s: Write not enabled for %s", routine, in->name);

  /* select internal or external buffer */
  if (myBuf==NULL) {
    myBuf = in->buffer;
    /* Check that internal buffer ( defined in gfloats) large enough */
    need = ObitTableSelBufferSize (in->myDesc, in->mySel);
    if (need > in->bufferSize) {
      Obit_log_error(err, OBIT_Error, 
		     "IO buffer (%ld) too small, need %ld for %s", 
		     in->bufferSize, need, in->name);
      return retCode;
    }
  } 
  g_assert (myBuf != NULL); /* check it */

  /* set number and location to write on myIO descriptor */
  ((ObitTableDesc*)in->myIO->myDesc)->firstRow   = in->myDesc->firstRow;
  ((ObitTableDesc*)in->myIO->myDesc)->numRowBuff = in->myDesc->numRowBuff;

  /* most of the instructions for the I/O are in the ObitInfoList */
  retCode = ObitIOWriteRow (in->myIO, rowno, myBuf, err);
  if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, in->name, retCode);

  /* set Status */
  in->myStatus = OBIT_Modified;

  /* save current location */
  if (rowno>0) {
    if (((ObitTableDesc*)in->myIO->myDesc)->nrow < rowno)
      ((ObitTableDesc*)in->myIO->myDesc)->nrow = rowno;
  } else ((ObitTableDesc*)in->myIO->myDesc)->nrow++;
  in->myDesc->firstRow   = ((ObitTableDesc*)in->myIO->myDesc)->firstRow;
  in->myDesc->numRowBuff = ((ObitTableDesc*)in->myIO->myDesc)->numRowBuff;
  in->myDesc->nrow       = ((ObitTableDesc*)in->myIO->myDesc)->nrow;

  return retCode;
} /* end ObitTableWrite */

/**
 * Convert table to a derived type
 * \param in  Pointer to object to be converted still exists after call.
 * \param err ObitErr for reporting errors.
 * \return converted table
 */
ObitTable* ObitTableConvert (ObitTable *in)
{
  ObitTable *out = NULL;
  const ObitTableClassInfo *myClass;

 /* error checks */
  g_assert (ObitIsA((Obit*)in, &myClassInfo));

  /* This only is defined if this is a derived object */
   myClass = in->ClassInfo;
   if ((gconstpointer)myClass != (gconstpointer)&myClassInfo) {
     
     /* Is function defined in derived class? */
     g_assert (myClass->ObitTableConvert != NULL);

     /* call actual function */
     out = myClass->ObitTableConvert (in);
   }
  
  return out;
} /* end ObitTableConvert */

/**
 * Read one row of table data from disk.
 * The ObitTableDesc maintains the current location in the table.
 * If there are existing rows in the buffer marked as modified 
 * ("_status" column value =1) the buffer is rewritten to disk before 
 * the new buffer is read.
 * \param in    Pointer to object to be read.
 * \param rowno Row number to start reading, -1 = next;
 * \param row   pointer to Table row Structure to accept data.
 * \param err   ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK => OK
 */
ObitIOCode ObitTableReadRow (ObitTable *in, glong rowno, ObitTableRow *row,
			  ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gchar *routine = "ObitTableReadRow";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));
  g_assert (ObitTableRowIsA(row));

  /* Only one row */
  in->mySel->nRowPIO = 1;

  /* read row rowno */
  retCode = ObitTableRead ((ObitTable*)in, rowno, NULL,  err);
  if (err->error) 
    Obit_traceback_val (err, routine, in->name, retCode);

  /* set pointer to buffer */
  row->myRowData = (gpointer)in->buffer;

  return retCode;
} /* end ObitTableReadRow */

/**
 * Attach an ObitTableRow to the buffer of an ObitTable.
 * This is only useful prior to filling a row structure in preparation .
 * for a WriteRow operation.  Array members of the Row structure are .
 * pointers to independently allocated memory, this routine allows using .
 * the table IO buffer instead of allocating yet more memory..
 * This routine need only be called once to initialize a Row structure for write..
 * \param in  Table with buffer to be written 
 * \param row Table Row structure to attach 
 * \param err ObitErr for reporting errors.
 */
void 
ObitTableSetRow  (ObitTable *in, ObitTableRow *row,
		  ObitErr *err)
{
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitIsA(row, &myRowClassInfo));

  if (in->myStatus == OBIT_Inactive) {
    Obit_log_error(err, OBIT_Error,
		   "Table is inactive for  %s ", in->name);
    return;
 }

  /* set pointer to buffer */
  row->myRowData = (gpointer)in->buffer;

} /*  end ObitTableSetRow */

/**
 * Write one row of table data to disk.
 * \param in    Pointer to object to be read.
 * \param rowno Row number to start reading, -1 = next;
 * \param row   pointer to Table row Structure to accept data.
 * \param err   ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK => OK
 */
ObitIOCode ObitTableWriteRow (ObitTable *in, glong rowno, ObitTableRow *row,
			  ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gchar *routine = "ObitTableWriteRow";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));
  g_assert (ObitTableRowIsA(row));

  /* Only one row */
  in->mySel->nRowPIO = 1;
  in->myDesc->numRowBuff = 1;

  /* write row rowno */
  retCode = ObitTableWrite ((ObitTable*)in, rowno, (gfloat*)row->myRowData,  
			    err);
  if (err->error) 
    Obit_traceback_val (err, routine, in->name, retCode);

   in->myStatus = OBIT_Modified;  /* Modified */

  /* Mark as unsorted */
  in->myDesc->sort[0] = 0;
  in->myDesc->sort[1] = 0;
  ((ObitTableDesc*)in->myIO->myDesc)->sort[0] = 0;
  ((ObitTableDesc*)in->myIO->myDesc)->sort[1] = 0;

  return retCode;
} /* end ObitTableWriteRow */

/**
 * Determine table type (name of type, e.g. "AIPS AN")
 * \param in    Pointer to object to be read.
 * \param err   ObitErr for reporting errors.
 * \return pointer to string
 */
gchar* ObitTableGetType (ObitTable *in, ObitErr *err)
{
  return in->tabType;
} /* end ObitTableGetType */

/**
 * Determine table version number (1-rel)
 * \param in    Pointer to object to be read.
 * \param err   ObitErr for reporting errors.
 * \return version number
 */
glong ObitTableGetVersion (ObitTable *in, ObitErr *err)
{
  return in->tabVer;
} /* end ObitTableGetVersion */

/**
 * Initialize global ClassInfo Structure.
 */
void ObitTableClassInit (void)
{
  const ObitClassInfo *ParentClass;

  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;

  /* Initialize (recursively) parent class first */
  ParentClass = ObitGetClass();
  if ((ParentClass!=NULL) && (ParentClass->ObitClassInit!=NULL))
    ParentClass->ObitClassInit();

  /* function pointers etc. for this class */
  myClassInfo.hasScratch    = TRUE; /* Scratch files allowed */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = (ObitClassInitFP)ObitTableClassInit;
  myClassInfo.newObit       = (newObitFP)newObitTable;
  myClassInfo.ObitTableZap  = (ObitTableZapFP)ObitTableZap;
  myClassInfo.ObitCopy      = (ObitCopyFP)ObitTableCopy;
  myClassInfo.ObitClone     = (ObitCloneFP)ObitTableClone;
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitTableClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitTableInit;
  myClassInfo.ObitTableConvert = (ObitTableConvertFP)ObitTableConvert;
  myClassInfo.ObitTableOpen    = (ObitTableOpenFP)ObitTableOpen;
  myClassInfo.ObitTableClose   = (ObitTableCloseFP)ObitTableClose;
  myClassInfo.ObitTableRead    = (ObitTableReadFP)ObitTableRead;
  myClassInfo.ObitTableClearRows= (ObitTableClearRowsFP)ObitTableClearRows;
  myClassInfo.ObitTableFullInstantiate = 
    (ObitTableFullInstantiateFP)ObitTableFullInstantiate;
  myClassInfo.ObitTableReadSelect = 
    (ObitTableReadSelectFP)ObitTableReadSelect;
  myClassInfo.ObitTableWrite = 
    (ObitTableWriteFP)ObitTableWrite;
  myClassInfo.ObitTableReadRow = 
    (ObitTableReadRowFP)ObitTableReadRow;
  myClassInfo.ObitTableWriteRow = 
    (ObitTableWriteRowFP)ObitTableWriteRow;
  myClassInfo.ObitTableSetRow = 
    (ObitTableSetRowFP)ObitTableSetRow;
  myClassInfo.ObitTableGetType = 
    (ObitTableGetTypeFP)ObitTableGetType;
  myClassInfo.ObitTableGetVersion = 
    (ObitTableGetVersionFP)ObitTableGetVersion;
} /* end ObitTableClassInit */

/*---------------Private functions--------------------------*/
/*----------------  Table Row  ----------------------*/
/**
 * Creates empty member objects, initialize reference count.
 * Parent classes portions are (recursively) initialized first
 * \param inn Pointer to the object to initialize.
 */
void ObitTableRowInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitTableRow *in = inn;

  /* error checks */
  g_assert (in != NULL);

  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myRowClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->myTable   = NULL;
  in->myRowData = NULL;

} /* end ObitTableRowInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * For some reason this wasn't build into the GType class.
 * \param  inn Pointer to the object to deallocate.
 *           Actually it should be an ObitTableRow* cast to an Obit*.
 */
void ObitTableRowClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitTableRow *in = inn;

  /* error checks */
  g_assert (ObitIsA(in, &myRowClassInfo));

  /* delete this class members */
  in->myTable = ObitTableUnref(in->myTable);
  
  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);
  
} /* end ObitTableRowClear */


/*------------------  Table  ------------------------*/
/**
 * Creates empty member objects, initialize reference count.
 * Parent classes portions are (recursively) initialized first
 * \param inn Pointer to the object to initialize.
 */
void ObitTableInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitTable *in = inn;

  /* error checks */
  g_assert (in != NULL);

  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->thread    = newObitThread();
  in->info      = newObitInfoList(); 
  in->myIO      = NULL;
  in->myDesc    = newObitTableDesc(in->name);
  in->mySel     = newObitTableSel(in->name);
  in->myStatus  = OBIT_Inactive;
  in->buffer    = NULL;
  in->bufferSize= 0;
  in->tabVer    = -1;
  in->tabType   = NULL;

} /* end ObitTableInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * For some reason this wasn't build into the GType class.
 * \param  inn Pointer to the object to deallocate.
 *           Actually it should be an ObitTable* cast to an Obit*.
 */
void ObitTableClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitTable *in = inn;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* delete this class members */
  in->info   = ObitInfoListUnref(in->info);
  in->thread = ObitThreadUnref(in->thread);
  in->myIO   = ObitUnref(in->myIO);
  in->myDesc = ObitTableDescUnref(in->myDesc);
  in->mySel  = ObitTableSelUnref(in->mySel);
  if (in->buffer)  ObitIOFreeBuffer(in->buffer); 
  if (in->tabType) g_free(in->tabType); in->tabType = NULL;
 
  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);
  
} /* end ObitTableClear */

/**
 * Get requested information from the ObitInfoList
 * \param info Pointer to InfoList
 * \param sel  pointer to uvdata selector to update.
 * \param err  ObitErr for reporting errors.
 */
static void ObitTableGetSelect (ObitInfoList *info, ObitTableSel *sel,
				ObitErr *err)
{
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM];

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitInfoListIsA(info));
  g_assert (ObitIsA(sel, ObitTableSelGetClass()));

  /* what type of underlying file? */
  if (!ObitInfoListGet(info, "FileType", &type, (gint32*)&dim, 
		       (gpointer)&sel->FileType, err)) {
    /* couldn't find it - add message to err and return */
    Obit_log_error(err, OBIT_Error, 
		"ObitTableGetSelect: entry FileType not in InfoList Object %s",
		sel->name);
  }

  /* Maximum number of visibilities per read/write? [default 1] */
  sel->nRowPIO = 1;
  ObitInfoListGetTest(info, "nRowPIO", &type, (gint32*)dim, &sel->nRowPIO);
  sel->nRowPIO = MAX (1, sel->nRowPIO); /* no fewer than 1 */


} /* end ObitTableGetSelect */


