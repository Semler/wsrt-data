/* $Id: ObitPlot.c,v 1.7 2005/08/12 10:42:54 bcotton Exp $        */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
#include "Obit.h"
#include "ObitPlot.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitPlot.c
 * ObitPlot Obit Graphics class definition.
 * 
 * This implementation uses pgplot which is in Fortran and requires 
 * the use of a Fortran linker to buuild executables.
 * The bulk of the routines are merely front ends to pgplot routines
 * or groupings of routines.
 */

/*--------------- File Global Variables  ----------------*/
/** name of the class defined in this file */
static gchar *myClassName = "ObitPlot";

/**
 * ClassInfo global structure ObitIOClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitPlotClassInfo myClassInfo = {FALSE};

/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitPlotInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitPlotClear (gpointer in);

/*---------------Public functions---------------------------*/
/**
 * Construct Object.
 * \return pointer to object created.
 */
ObitPlot* newObitPlot (gchar *name)
{
  ObitPlot *out;

   /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitPlotClassInit();

  /* allocate structure */
  out = g_malloc0(sizeof(ObitPlot));

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

 /* set classInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  ObitPlotInit((gpointer)out);

 return out;
} /* end newObitPlot */

/**
 * Returns ClassInfo pointer for the class.
 * Initializes class if needed on first call.
 * \return pointer to the class structure.
 */
gconstpointer ObitPlotGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitPlotClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitPlotGetClass */

/**
 * Initialize plot specifying output device
 * \param in      Pointer to object to be copied.
 * \param output  name and type of output device:
 * \li      NULL, interactive prompt
 * \li      myPlot.ps/PS for postscript
 * \li      xterm  for an xterm
 * \param err ObitErr error stack
 */
void ObitPlotInitPlot (ObitPlot* in, gchar *output, ObitErr *err)
{
#ifdef HAVE_PGPLOT  /* Only if pgplot available */
  gint istat;
  gchar *outstr;
  gchar *question="?";
  gchar *routine = "ObitPlotInitPlot";
#endif /* HAVE_ PGPLOT */

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

#ifdef HAVE_PGPLOT  /* Only if pgplot available */
  /* Create */
  if (output) outstr = output;
  else outstr = question;
  istat = cpgopen (outstr);
  if (istat<=0) {
    Obit_log_error(err, OBIT_Error, "%s ERROR opening plot", 
		   routine);
    return;
  }

  /* Prompt for output */
  cpgask(output==NULL);
# else  /* Die if pgplot not available */
  g_error ("pgplot not available");
#endif /* HAVE_ PGPLOT */
 
} /* end ObitPlotInitPlot */

/**
 * Copy constructor.
 * \param in Pointer to object to be copied.
 * \param out Pointer to object to be written.  
 *            If NULL then a new structure is created.
 * \param err ObitErr error stack
 * \return Pointer to new object.
 */
ObitPlot* ObitPlotCopy (ObitPlot* in, ObitPlot* out, 
			    ObitErr *err)
{
  const ObitClassInfo *ParentClass;
  gboolean oldExist;
  gchar *outName;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return out;
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));
 
  /* Create if it doesn't exist */
  oldExist = out!=NULL;
  if (!oldExist) {
    /* derive object name */
    outName = g_strconcat ("Copy: ",in->name,NULL);
    out = newObitPlot(outName);
    g_free(outName);
  }

  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);

  /* initialize/copy */
  return out;
} /* end ObitPlotCopy */

/**
 * Plot X vs Y using symbol.
 * Plot should be initialized with ObitPlotInitPlot before calling.
 * \param in      Pointer to Plot object.
 * \param symbol  Symbol index to use for plotting (pgplot)
 *                values in the range [1,31] are usable 
 *                if negative, use abs value and connect points
 * \li 1 = dot
 * \li 2 = plus
 * \li 3 = *
 * \li 4 = open circle
 * \li 5 = x
 * \li 6 = open square
 * \li 7 = open triangle
 * \li 12 = open star
 * \li 13 = filled triangle
 * \li 16 = filled square
 * \li 17 = filled circle
 * \li 18 = filled star
 *
 * \param n       Number of data points in x, y
 * \param x       Independent variable, if NULL use index
 * \param y       Dependent variable
 * \param err ObitErr error stack
 *
 * Optional parameters on in->info
 * \li XMAX (float) maximum X value (defaults to actual value)
 * \li XMIN (float) minimum X value (defaults to actual value)
 * \li YMAX (float) maximum Y value (defaults to actual value)
 * \li YMIN (float) minimum Y value (defaults to actual value)
 * \li TITLE (string)  Label for the plot (defaults to none), max 120
 * \li XLABEL (string) Label for horizontal axis (defaults to none)
 * \li XOPT   (string) Options for horizontal axis (default "BCNTS")
 *                     See #ObitPlotCpgbox for details.
 * \li YLABEL (string) Label for vertical axis (defaults to none)
 * \li YOPT   (string) Options for  vertical axis (default "BCNTS")
 *                     See #ObitPlotCpgbox for details.
 * \li CSIZE  (int)    Scaling factor for characters(default = 1)
 * \li LWIDTH (int)    Line width (default = 1)
 * \li JUST   (int)    If !=0 then force X and Y axis scaling to be the same
 */
void ObitPlotXYPlot (ObitPlot* in, gint symbol, 
		     gint n, gfloat *x, gfloat *y, ObitErr *err)
{
  gint i, just, axis;
  float xmax, xmin, ymax, ymin, cscale, *xx, fblank = ObitMagicF();
  gint csize, lwidth, lsymbol;
  gint32 dim[MAXINFOELEMDIM] = {1,1,1,1,1};
  ObitInfoType type;
  gboolean gotRange, gotXMAX, gotYMAX, gotXMIN, gotYMIN, doConnect;
  gchar title[121], xlabel[121], ylabel[121], xopt[31], yopt[31];
  gchar *xopt_def="BCNTS", *yopt_def="BCNTS";
  gchar *routine="ObitPlotXYPlot";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (y!=NULL);

  /* anything to do? */
  if (n<=0) return;
  
  lsymbol = abs (symbol);
  doConnect = symbol<0;

  /* using index for x? */
  if (x!=0) xx = x;
  else { /* yes */
    xx = g_malloc0(n*sizeof(gfloat));
    for (i=0; i<n; i++) xx[i] = (float)i;
  }

  /* Get inputs from info */
  /* Plot title */
  for (i=0; i<121; i++) title[i] = 0;
  strcpy (title, "    ");
  ObitInfoListGetTest(in->info, "TITLE", &type, dim, title);
 
  /* Horizontal label */
  for (i=0; i<121; i++) xlabel[i] = 0;
  strcpy (xlabel, "    ");
  ObitInfoListGetTest(in->info, "XLABEL", &type, dim, xlabel);
 
  /* Horizontal label options */
  for (i=0; i<31; i++) xopt[i] = 0;
  strcpy (xopt, xopt_def);
  ObitInfoListGetTest(in->info, "XOPT", &type, dim, xopt);
 
  /* Vertical label */
  for (i=0; i<121; i++) ylabel[i] = 0;
  strcpy (ylabel, "    ");
  ObitInfoListGetTest(in->info, "YLABEL", &type, dim, ylabel);

  /* Vertical label options */
  for (i=0; i<31; i++) yopt[i] = 0;
  strcpy (yopt, yopt_def);
  ObitInfoListGetTest(in->info, "YOPT", &type, dim, yopt);
 
  /* Sizes */
  csize = 1;
  ObitInfoListGetTest(in->info, "CSIZE", &type, dim, (gpointer*)&csize);
  lwidth = 1;
  ObitInfoListGetTest(in->info, "LWIDTH", &type, dim, (gpointer*)&lwidth);
  just = 0;
  ObitInfoListGetTest(in->info, "JUST", &type, dim, (gpointer*)&just);
  just = (just!=0);
 
  /* Plot range */
  xmax = fblank;
  ObitInfoListGetTest(in->info, "XMAX", &type, dim, (gpointer*)&xmax);
  ymax = fblank;
  ObitInfoListGetTest(in->info, "YMAX", &type, dim, (gpointer*)&ymax);
  xmin = fblank;
  ObitInfoListGetTest(in->info, "XMIN", &type, dim, (gpointer*)&xmin);
  ymin = fblank;
  ObitInfoListGetTest(in->info, "YMIN", &type, dim, (gpointer*)&ymin);

  /* Check data for range? */
  gotXMAX = xmax != fblank;
  gotYMAX = ymax != fblank;
  gotXMIN = xmin != fblank;
  gotYMIN = ymin != fblank;
  gotRange =  gotXMAX && gotYMAX && gotXMIN && gotYMIN;
  if (!gotRange) { /* Need to check data? */
    for (i=0; i<n; i++) {
      if (xx[i]!=fblank) {
	if  (xmax == fblank) xmax = xx[i];
	else xmax = MAX (xx[i], xmax);
	if  (xmin == fblank) xmin = xx[i];
	else xmin = MIN (xx[i], xmin);
      }
      if (y[i]!=fblank) {
	if  (ymax == fblank) ymax = y[i];
	else ymax = MAX (y[i], ymax);
	if  (ymin == fblank) ymin = y[i];
	else ymin = MIN (y[i], ymin);
      }
    }
  } /* end check range */

  /* Are the range values OK? */
  if (xmax<=xmin) { /* Nope - bug out */
    Obit_log_error(err, OBIT_Error, "%s: XMAX(%g) <= XMIN(%g)", 
		   routine, xmax, xmin);
    if (x==NULL) g_free(xx);
    return;
 }

  if (ymax<=ymin) { /* Nope - bug out */
    Obit_log_error(err, OBIT_Error, "%s: YMAX (%g) <= YMIN(%g)", 
		   routine, ymax, ymin);
    if (x==NULL) g_free(xx);
    return;
 }

  /* If autosetting the range, expand a bit */
  if (!gotXMAX) xmax += 0.05 * (xmax-xmin);
  if (!gotXMIN) xmin -= 0.05 * (xmax-xmin);
  if (!gotYMAX) ymax += 0.05 * (ymax-ymin);
  if (!gotYMIN) ymin -= 0.05 * (ymax-ymin);

  /* Adjust character size */
  cscale = (gfloat)csize;
  cscale = MAX (1.0, cscale);
  ObitPlotCpgsch (in, cscale, err);

  /* set line width */
  lwidth = MAX (1, lwidth);
  ObitPlotCpgslw (in, lwidth, err);

  /* set plotting area */
  axis = -2;
  ObitPlotCpgenv (in, xmin, xmax, ymin, ymax, just, axis, err);

  /* Init - use buffering */
  cpgbbuf();

  /*  Plot label */
  ObitPlotCpglab (in, xlabel, ylabel, title, err);

  /* set ticks */
  ObitPlotCpgbox (in, xopt, 0.0, 0, yopt, 0.0, 0, err);

  /* Plot */
  cpgpt (n, xx, y, lsymbol);

  /* Connect the dots? */
  if (doConnect) {
    for (i=1; i<n; i++) {
      ObitPlotCpgmove (in, xx[i-1], y[i-1], err);
      ObitPlotCpgdraw (in, xx[i], y[i], err);
    }
  }

  /* Flush the buffer */
  cpgebuf();
  cpgupdt();
  cpgend();

  /* deallocate x index array is allocated */
  if (x==NULL) g_free(xx);

} /* end  ObitPlotXYPlot */

/** 
 * Front end to pgplot routine cpgenv.
 * Set window and viewport and draw labeled frame 
 * Set PGPLOT "Plotter Environment".  PGENV establishes the scaling
 * for subsequent calls to PGPT, PGLINE, etc.  The plotter is
 * advanced to a new page or panel, clearing the screen if necessary.
 * If the "prompt state" is ON (see PGASK), confirmation
 * is requested from the user before clearing the screen.
 * If requested, a box, axes, labels, etc. are drawn according to
 * the setting of argument AXIS.
 * \param in    Pointer to Plot object.
 * \param xmin  the world x-coordinate at the bottom left corner of the viewport.
 * \param xmax  the world x-coordinate at the top right corner of the viewport 
 *                  (note XMAX may be less than XMIN).
 * \param ymin  the world y-coordinate at the bottom left corner
 *                  of the viewport.
 * \param ymax  the world y-coordinate at the top right corner
 *                  of the viewport (note YMAX may be less than YMIN)
 * \param just  if JUST=1, the scales of the x and y axes (in
 *                  world coordinates per inch) will be equal,
 *                  otherwise they will be scaled independently.
 * \param axis  controls the plotting of axes, tick marks, etc:
 * \li axis = -2 : draw no box, axes or labels;
 * \li axis = -1 : draw box only;
 * \li axis =  0 : draw box and label it with coordinates;
 * \li axis =  1 : same as axis=0, but also draw the
 *                coordinate axes (X=0, Y=0);
 * \li axis =  2 : same as axis=1, but also draw grid lines
 *                at major increments of the coordinates;
 * \li axis = 10 : draw box and label X-axis logarithmically;
 * \li axis = 20 : draw box and label Y-axis logarithmically;
 * \li axis = 30 : draw box and label both axes logarithmically.
 * \param err   ObitErr error stack
 */
void  ObitPlotCpgenv (ObitPlot* in, 
		      gfloat xmin, gfloat xmax, gfloat ymin, gfloat ymax, 
		      gint just, gint axis, ObitErr *err)
{
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

#ifdef HAVE_PGPLOT  /* Only if pgplot available */
  /* Call pgplot routine */
  cpgenv (xmin, xmax, ymin, ymax, just, axis);
# else  /* Die if pgplot not available */
  g_error ("pgplot not available");
#endif /* HAVE_ PGPLOT */
} /* end ObitPlotCpgenv */

/** 
 * Front end to pgplot routine cpglab.
 * write labels for x-axis, y-axis, and top of plot
 * Write labels outside the viewport. This routine is a simple
 * interface to PGMTXT, which should be used if PGLAB is inadequate.
 * \param in      Pointer to Plot object.
 * \param xlabel  a label for the x-axis (centered below the
 *                viewport).
 * \param ylabel  a label for the y-axis (centered to the left
 *                  of the viewport, drawn vertically)
 * \param title   a label for the entire plot (centered above the viewport)
 * \param err     ObitErr error stack
 */
void  ObitPlotCpglab (ObitPlot* in, gchar *xlabel, gchar *ylabel, gchar *title,
		      ObitErr *err)
{
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (xlabel!=NULL);
  g_assert (ylabel!=NULL);
  g_assert (title!=NULL);

#ifdef HAVE_PGPLOT  /* Only if pgplot available */
  /* Call pgplot routine */
  cpglab (xlabel, ylabel, title);
# else  /* Die if pgplot not available */
  g_error ("pgplot not available");
#endif /* HAVE_ PGPLOT */
} /* end ObitPlotCpglab */

/** 
 * Front end to pgplot routine cpgbox.
 * Draw labeled frame around viewport.
 * Annotate the viewport with frame, axes, numeric labels, etc.
 * PGBOX is called by on the user's behalf by PGENV, but may also be
 * called explicitly.
 * \param in      Pointer to Plot object.
 * \param xopt   string of options for X (horizontal) axis of
 *                  plot. Options are single letters, and may be in
 *                  any order (see below).
 * \param xtick    world coordinate interval between major tick marks
 *                  on X axis. If XTICK=0.0, the interval is chosen by
 *                  PGBOX, so that there will be at least 3 major tick
 *                  marks along the axis.
 * \param nxsub   the number of subintervals to divide the major
 *                  coordinate interval into. If XTICK=0.0 or NXSUB=0,
 *                  the number is chosen by PGBOX.
 * \param yopt   string of options for Y (vertical) axis of plot.
 *                  Coding is the same as for xopt.
 * \param ytick  like xtick for the Y axis.
 * \param nysub  like nxsub for the Y axis
 * \param err    ObitErr error stack

 * Axis options:
 * \li A : draw Axis (X axis is horizontal line Y=0, Y axis is vertical line X=0).
 * \li B : draw bottom (X) or left (Y) edge of frame.
 * \li C : draw top (X) or right (Y) edge of frame.
 * \li G : draw Grid of vertical (X) or horizontal (Y) lines
 * \li I : Invert the tick marks; ie draw them outside the viewport instead of inside.
 * \li L : label axis Logarithmically
 * \li N : write Numeric labels in the conventional location below the
 *         viewport (X) or to the left of the viewport (Y).
 * \li P : extend ("Project") major tick marks outside the box (ignored if
 *         option I is specified)
 * \li M : write numeric labels in the unconventional location above the
 *         viewport (X) or to the right of the viewport (Y).
 * \li T : draw major Tick marks at the major coordinate interval.
 * \li S : draw minor tick marks (Subticks).
 * \li V : orient numeric labels Vertically. This is only applicable to Y.
 *         The default is to write Y-labels parallel to the axis.
 * \li 1 : force decimal labelling, instead of automatic choice (see PGNUMB).
 * \li 2 : force exponential labelling, instead of automatic.
 */
void  ObitPlotCpgbox (ObitPlot* in, 
		      gchar *xopt, gfloat xtick, gint nxsub, 
		      gchar *yopt,  gfloat ytick, gint nysub, 
		      ObitErr *err)
{
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (xopt!=NULL);
  g_assert (yopt!=NULL);

#ifdef HAVE_PGPLOT  /* Only if pgplot available */
  /* Call pgplot routine */
  cpgbox (xopt, xtick, nxsub, yopt, ytick, nysub);
# else  /* Die if pgplot not available */
  g_error ("pgplot not available");
#endif /* HAVE_ PGPLOT */
} /* end ObitPlotCpgbox */

/** 
 * Front end to pgplot routine cpgsch.
 * Scaling for characters
 * Set the character size attribute. The size affects all text and graph
 * markers drawn later in the program. The default character size is
 * 1.0, corresponding to a character height about 1/40 the height of
 * the view surface.  Changing the character size also scales the length
 * of tick marks drawn by PGBOX and terminals drawn by PGERRX and PGERRY.
 * \param in      Pointer to Plot object.
 * \param cscale  new character size (dimensionless multiple of the default size).
 * \param err     ObitErr error stack
 */
void  ObitPlotCpgsch (ObitPlot* in, gfloat cscale, ObitErr *err)
{
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

#ifdef HAVE_PGPLOT  /* Only if pgplot available */
  /* Call pgplot routine */
  cpgsch (cscale);
#endif /* HAVE_ PGPLOT */
} /* end ObitPlotCpgsch */

/** 
 * Front end to pgplot routine cpgslw
 * Set line width
 * Set the line-width attribute. This attribute affects lines, graph
 * markers, and text. The line width is specified in units of 1/200 
 * (0.005) inch (about 0.13 mm) and must be an integer in the range
 * 1-201. On some devices, thick lines are generated by tracing each
 * line with multiple strokes offset in the direction perpendicular to
 * the line.
 * \param in      Pointer to Plot object.
 * \param lwidth  Width of line, in units of 0.005 inch (0.13 mm) in range 1-201.
 * \param err     ObitErr error stack
 */
void  ObitPlotCpgslw (ObitPlot* in, gint lwidth, ObitErr *err)
{
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

#ifdef HAVE_PGPLOT  /* Only if pgplot available */
  /* Call pgplot routine */
  cpgslw (lwidth);
#endif /* HAVE_ PGPLOT */
} /* end ObitPlotCpgslw */

/** 
 * Front end to pgplot routine cpgmtxt
 * Write text at position relative to viewport.
 * Write text at a position specified relative to the viewport (outside
 * or inside).  This routine is useful for annotating graphs. It is used
 * by routine PGLAB.  The text is written using the current values of
 * attributes color-index, line-width, character-height, and
 * character-font.
 * \param in      Pointer to Plot object.
 * \param side    Must include one of the characters 'B', 'L', 'T',
 *                or 'R' signifying the Bottom, Left, Top, or Right
 *                margin of the viewport. If it includes 'LV' or
 *                'RV', the string is written perpendicular to the
 *                frame rather than parallel to it.
 * \param disp    The displacement of the character string from the
 *                specified edge of the viewport, measured outwards
 *                from the viewport in units of the character
 *                height. Use a negative value to write inside the
 *                viewport, a positive value to write outside.
 * \param coord   The location of the character string along the
 *                specified edge of the viewport, as a fraction of
 *                the length of the edge.
 * \param fjust   Controls justification of the string parallel to
 *                the specified edge of the viewport. If
 *                FJUST = 0.0, the left-hand end of the string will
 *                be placed at COORD; if JUST = 0.5, the center of
 *                the string will be placed at COORD; if JUST = 1.0,
 *                the right-hand end of the string will be placed at
 *                at COORD. Other values between 0 and 1 give inter-
 *                mediate placing, but they are not very useful.
 * \param text    The text string to be plotted. Trailing spaces are
 *                ignored when justifying the string, but leading
 *                spaces are significant.
 * \param err     ObitErr error stack
 */
void  ObitPlotCpgmtxt (ObitPlot* in, gchar *side, gfloat disp, 
		      gfloat coord, gfloat fjust, gchar *text,
		      ObitErr *err)
{
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (side!=NULL);
  g_assert (text!=NULL);

#ifdef HAVE_PGPLOT  /* Only if pgplot available */
  /* Call pgplot routine */
  cpgmtxt (side, disp, coord, fjust, text);
#endif /* HAVE_ PGPLOT */
} /* end ObitPlotCpgmtxt */

/** 
 * Primitive routine to move the "pen" to the point with world
 * coordinates (X,Y). No line is drawn.
 * \param in      Pointer to Plot object.
 * \param x       (input)  : world x-coordinate of the new pen position.
 * \param y       (input)  : world y-coordinate of the new pen position.
 * \param err     ObitErr error stack
 */
void  ObitPlotCpgmove (ObitPlot* in, gfloat x, gfloat y, ObitErr *err)
{
  /* error checks */
  if (err->error) return;

#ifdef HAVE_PGPLOT  /* Only if pgplot available */
  /* Call pgplot routine */
  cpgmove (x, y);
#endif /* HAVE_ PGPLOT */
} /* end ObitPlotCpgmove */

/** 
 * Draw a line from the current pen position to the point
 * with world-coordinates (X,Y). The line is clipped at the edge of the
 * current window. The new pen position is (X,Y) in world coordinates.
 * \param in      Pointer to Plot object.
 * \param x       (input): world x-coordinate of the end point of the line.
 * \param y       (input): world y-coordinate of the end point of the line.
 * \param err     ObitErr error stack
 */
void  ObitPlotCpgdraw (ObitPlot* in, gfloat x, gfloat y, ObitErr *err)
{
  /* error checks */
  if (err->error) return;

#ifdef HAVE_PGPLOT  /* Only if pgplot available */
  /* Call pgplot routine */
  cpgdraw (x, y);
#endif /* HAVE_ PGPLOT */
} /* end ObitPlotCpgdraw */

/**
 * Initialize global ClassInfo Structure.
 */
 void ObitPlotClassInit (void)
{
  const ObitClassInfo *ParentClass;

  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;

  /* Initialize (recursively) parent class first */
  ParentClass = ObitGetClass();
  ObitClassInit();  /* Initialize parent class if needed */

  /* function pointers etc. for this class */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = 
    (ObitClassInitFP)ObitPlotClassInit;
  myClassInfo.newObit       = (newObitFP)newObitPlot;
  myClassInfo.ObitCopy      = (ObitCopyFP)ObitPlotCopy;
  myClassInfo.ObitClone     = NULL;
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitPlotClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitPlotInit;
} /* end ObitOTFSelClassInit */

/*---------------Private functions--------------------------*/
/**
 * Creates empty member objects, initialize reference count.
 * Does (recursive) initialization of base class members before 
 * this class.
 * \param inn Pointer to the object to initialize.
 */
void ObitPlotInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitPlot *in = inn;

  /* error checks */
  g_assert (in != NULL);
  
#ifndef HAVE_PGPLOT  /* Only if pgplot available */
  g_error ("pgplot not available");
#endif /* not HAVE_ PGPLOT */

  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->thread = newObitThread();
  in->info   = newObitInfoList();
} /* end ObitPlotInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * \param  inn Pointer to the object to deallocate.
 */
void ObitPlotClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitPlot *in = inn;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* free this class members */
  in->thread    = ObitThreadUnref(in->thread);
  if (in->info) ObitInfoListUnref (in->info); in->info = NULL;
  
  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);

} /* end ObitPlotClear */



