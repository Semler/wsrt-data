/* $Id: ObitFFT.c,v 1.4 2005/02/25 14:25:46 bcotton Exp $         */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include <math.h>
#include "ObitFFT.h"
#include "ObitIOUVFITS.h"
#include "ObitIOUVAIPS.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitFFT.c
 * ObitFFT class function definitions.
 * This class is derived from the Obit base class and is based on FFTW.
 */

/** name of the class defined in this file */
static gchar *myClassName = "ObitFFT";

/*--------------- File Global Variables  ----------------*/
/**
 * ClassInfo structure ObitFFTClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitFFTClassInfo myClassInfo = {FALSE};

/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitFFTInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitFFTClear (gpointer in);

/*----------------------Public functions---------------------------*/
/**
 * Constructor.
 * Initializes class if needed on first call.
 * \param name An optional name for the object.
 * \param dir  The direction of the transform OBIT_FFT_Forward (R2C)
 *             or OBIT_FFT_Reverse (C2R).
 * \param type Whether OBIT_FFT_FullComplex (full C2C) or
 *             OBIT_FFT_HalfComplex (R2C or C2R).
 * \param rank of matrix range [1,7]
 * \param dim  dimensionality of each axis in column major (Fortran) order.
 *             If real/half complex is being used, then dim[0] should be
 *             the number of reals.
 * \return the new object.
 */
ObitFFT* newObitFFT (gchar* name, ObitFFTdir dir, ObitFFTtype type, 
		     gint rank, gint *dim)
{
  ObitFFT* out;
  gint i;

  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitFFTClassInit();

  /* allocate/init structure */
  out = g_malloc0(sizeof(ObitFFT));

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");
  out->dir  = dir;
  out->type = type;
  out->rank = rank;
  g_assert (rank<=7); /* Not too many dimensions */
  for (i=0; i<rank; i++) out->dim[i] = dim[i];

  /* set ClassInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  ObitFFTInit((gpointer)out);

 return out;
} /* end newObitFFT */

/**
 * Returns ClassInfo pointer for the class.
 * \return pointer to the class structure.
 */
gconstpointer ObitFFTGetClass (void)
{
  return (gconstpointer)&myClassInfo;
} /* end ObitFFTGetClass */

/**
 * Given a length of data, suggest a larger size that will have an efficient FFT.
 * \param  length number of values to be transformed
 * \return a number equal or larger than length that will have an efficient 
 *  transform. 
 */
gint ObitFFTSuggestSize (gint length)
{
  gint i, out = length;
  /* Array of good values , even powers of 2, 3, 5 and small multiples */
  /* including odd ones  gint good[] = {
    2,3,4,5,6,8,9,10,12,16,
    20,24,25,27,32,40,48,64,80,81,
    96,125,128,160,192,243,256,320,384,400,432,480,512,576,600,
    625,640,729,768,800,864,960,1024,1280,1536,1600,1920,2048,2187,2560,
    3072,3125,3840,4096,5120,6144,6480,6561,8192,10240,12288,15625,
    16384,19683,20480,24576,32768,40960,49152,59049,65536,78125,
    81920,98304,131072,163840,177147,196608,234375,262144,
    327680,390625,393216,524288,531441,655360,703125,786432,1048576,
    -1}; */
  /* really they should be even */
  gint good[] = {
    2,4,6,8,10,12,16,
    20,24,32,40,48,64,80,
    96,128,160,192,256,320,384,400,432,480,512,576,600,
    640,768,800,864,960,1024,1280,1536,1600,1920,2048,2560,
    3072,3840,4096,5120,6144,6480,7680,8192,10240,12288,
    16000,16384,20480,24576,32768,40960,49152,65536,
    81920,98304,131072,163840,196608,262144,
    327680,393216,524288,655360,786432,1048576,
    -1};

  /* loop over acceptable values looking for next largest */
  i = 0;
  while (good[i]>0) {
    if (good[i]>=length) return good[i];
    i++;
  }

  /* No luck - return input value */
  return out;
} /* end ObitFFTSuggestSize */

/**
 * Do full real to half complex transform.
 * Must have been created with dir = OBIT_FFT_Forward and
 * type = OBIT_FFT_HalfComplex and have same geometry as constructor call.
 * \param in       Object with FFT structures.
 * \param inArray  Array to be transformed (undisturbed on output).
 * \param outArray Output array
 */
void ObitFFTR2C (ObitFFT *in, ObitFArray *inArray, ObitCArray *outArray)
{
  /* error checks */
  g_assert (ObitFFTIsA(in));
  g_assert (ObitFArrayIsA(inArray));
  g_assert (ObitCArrayIsA(outArray));
  g_assert (in->RPlan!=NULL);
  g_assert (in->type == OBIT_FFT_HalfComplex);
  g_assert (in->dir  == OBIT_FFT_Forward);
  g_assert (in->rank == inArray->ndim);
  g_assert (in->dim[0] <= inArray->naxis[0]); /* Check two axes */
  g_assert (in->dim[1] <= inArray->naxis[1]); 
  g_assert (inArray->ndim == outArray->ndim);
  g_assert (inArray->naxis[0] <= 2*(outArray->naxis[0]-1));
  g_assert (inArray->naxis[1] <= outArray->naxis[1]);
  /* FFTW compiled in single precision?*/
  g_assert (sizeof(fftw_real) == sizeof(gfloat));

  /* Lock ObitObjects aginst other threads */
  ObitThreadLock(in->thread);
  ObitThreadLock(inArray->thread);
  ObitThreadLock(outArray->thread);
   
  /* do transform */
#ifdef HAVE_FFTW
  rfftwnd_one_real_to_complex (in->RPlan, (fftw_real*)inArray->array, 
			       (fftw_complex*)outArray->array);
#endif /* HAVE_FFTW */
  /* Unlock ObitObjects */
  ObitThreadUnlock(outArray->thread);
  ObitThreadUnlock(inArray->thread);
  ObitThreadUnlock(in->thread);
 
} /* end ObitFFTR2C */

/**
 * Do half complex to full real transform.
 * Must have been created with dir = OBIT_FFT_Reverse and
 * type = OBIT_FFT_HalfComplex and have same geometry as constructor call.
 * \param in       Object with FFT structures.
 * \param inArray  Array to be transformed (disturbed on output).
 * \param outArray Output array
 */
void ObitFFTC2R (ObitFFT *in, ObitCArray *inArray, ObitFArray *outArray)
{
  /* error checks */
  g_assert (ObitFFTIsA(in));
  g_assert (ObitCArrayIsA(inArray));
  g_assert (ObitFArrayIsA(outArray));
  g_assert (in->RPlan!=NULL);
  g_assert (in->type == OBIT_FFT_HalfComplex);
  g_assert (in->dir  == OBIT_FFT_Reverse);
  g_assert (in->rank == inArray->ndim);
  g_assert (in->dim[0] <= 2*(inArray->naxis[0]-1)); /* Check two axes */
  g_assert (in->dim[1] <= inArray->naxis[1]); 
  g_assert (inArray->ndim == outArray->ndim); 
  g_assert (2*(inArray->naxis[0]-1) == outArray->naxis[0]); 
  g_assert (inArray->naxis[1] == outArray->naxis[1]); 
  /* FFTW compiled in single precision?*/
  g_assert (sizeof(fftw_real) == sizeof(gfloat));
 
  /* Lock ObitObjects aginst other threads */
  ObitThreadLock(in->thread);
  ObitThreadLock(inArray->thread);
  ObitThreadLock(outArray->thread);
   
  /* do transform */
#ifdef HAVE_FFTW
  rfftwnd_one_complex_to_real (in->RPlan, (fftw_complex*)inArray->array, 
			       (fftw_real*)outArray->array);
#endif /* HAVE_FFTW */

  /* Unlock ObitObjects */
  ObitThreadUnlock(outArray->thread);
  ObitThreadUnlock(inArray->thread);
  ObitThreadUnlock(in->thread);
 } /* end ObitFFTC2R */

/**
 * Do full complex to complex transform in directions specified in in..
 * Must have been created with dir = OBIT_FFT_Reverse have same geometry as 
 * constructor call.
 * Transform is in the direction specified in constructor call.
 * \param in       Object with FFT structures.
 * \param inArray  Array to be transformed (disturbed on output).
 * \param outArray Output array
 */
/** Public: Full Complex to Complex. */
void ObitFFTC2C (ObitFFT *in, ObitCArray *inArray, ObitCArray *outArray)
{
  /* error checks */
  g_assert (ObitFFTIsA(in));
  g_assert (ObitCArrayIsA(inArray));
  g_assert (ObitCArrayIsA(outArray));
  g_assert (in->CPlan!=NULL);
  g_assert (in->type == OBIT_FFT_FullComplex);
  g_assert (in->rank == inArray->ndim);
  g_assert (in->dim[0] <= inArray->naxis[0]); /* Check two axes */
  g_assert (in->dim[1] <= inArray->naxis[1]); 
  g_assert (inArray->ndim == outArray->ndim);
  g_assert (inArray->naxis[0] <= outArray->naxis[0]);
  g_assert (inArray->naxis[1] <= outArray->naxis[1]);
  /* FFTW compiled in single precision?*/
  g_assert(sizeof(fftw_real) == sizeof(gfloat));

  /* Lock ObitObjects aginst other threads */
  ObitThreadLock(in->thread);
  ObitThreadLock(inArray->thread);
  ObitThreadLock(outArray->thread);
   
  /* do transform */
#ifdef HAVE_FFTW
  fftwnd_one (in->CPlan, (fftw_complex*)inArray->array, 
    (fftw_complex*)outArray->array);
#endif /* HAVE_FFTW */

  /* Unlock ObitObjects */
  ObitThreadUnlock(outArray->thread);
  ObitThreadUnlock(inArray->thread);
  ObitThreadUnlock(in->thread);
 } /* end ObitFFTC2C */

/**
 * Initialize global ClassInfo Structure.
 */
void ObitFFTClassInit (void)
{
  const ObitClassInfo *ParentClass;

  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;

  /* Initialize (recursively) parent class first */
  ParentClass = ObitGetClass();
  ObitClassInit();  /* Initialize parent class if needed */

  /* function pointers etc. for this class */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = (ObitClassInitFP)ObitFFTClassInit;
  myClassInfo.newObit       = (newObitFP)newObitFFT;
  myClassInfo.ObitCopy      = NULL;
  myClassInfo.ObitClone     = NULL;
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitFFTClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitFFTInit;
  /* New to this class */
  myClassInfo.newObitFFT  = (newObitFFTFP)newObitFFT;
  myClassInfo.ObitFFTR2C  = (ObitFFTR2CFP)ObitFFTR2C;
  myClassInfo.ObitFFTC2R  = (ObitFFTC2RFP)ObitFFTC2R;
  myClassInfo.ObitFFTC2C  = (ObitFFTC2CFP)ObitFFTC2C;
} /* end ObitFFTClassInit */

/*---------------Private functions--------------------------*/

/**
 * Creates empty member objects, initialize reference count.
 * Parent classes portions are (recursively) initialized first
 * \param inn Pointer to the object to initialize.
 */
void ObitFFTInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitFFT *in = inn;
  gint i, dim[7];
#ifdef HAVE_FFTW
  fftw_direction dir;
  gint flag;
#endif /* HAVE_FFTW */

  /* error checks */
  g_assert (in != NULL);

  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->thread       = newObitThread();
  in->CPlan        = NULL;
  in->RPlan        = NULL;

  /* reverse order of dimensions since FFTW uses row major and Obit
     uses column major */
  for (i=0; i<in->rank; i++) dim[in->rank-i-1] = in->dim[i];

  /* initialize FFT Plan */
#ifdef HAVE_FFTW
  flag = FFTW_ESTIMATE; /* may only be done once - use cheap method */
  if (in->type==OBIT_FFT_FullComplex) {
    if (in->dir==OBIT_FFT_Forward) dir = FFTW_FORWARD;
    else dir = FFTW_BACKWARD;
    in->CPlan = fftwnd_create_plan(in->rank, dim, dir, flag);
  } else if (in->type==OBIT_FFT_HalfComplex) {
    if (in->dir==OBIT_FFT_Forward) dir = FFTW_REAL_TO_COMPLEX;
    else dir = FFTW_COMPLEX_TO_REAL;
    in->RPlan = rfftwnd_create_plan(in->rank, dim, dir, flag);
  }
#else  /* Die horrible death if attempt to use */
  g_error("ObitFFTInit: FFTW NOT implemented");
#endif /* HAVE_FFTW */
} /* end ObitFFTInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * \param  inn Pointer to the object to deallocate.
 *           Actually it should be an ObitFFT* cast to an Obit*.
 */
void ObitFFTClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitFFT *in = inn;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* delete this class members */
  in->thread    = ObitThreadUnref(in->thread);
#ifdef HAVE_FFTW
  if (in->CPlan)  fftwnd_destroy_plan(in->CPlan); in->CPlan = NULL;
  if (in->RPlan) rfftwnd_destroy_plan(in->RPlan); in->RPlan = NULL;
#endif /* HAVE_FFTW */
  
  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);
  
} /* end ObitFFTClear */

