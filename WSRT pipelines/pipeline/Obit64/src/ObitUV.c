/* $Id: ObitUV.c,v 1.32 2005/09/21 14:12:41 bcotton Exp $          */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "ObitUV.h"
#include "ObitIOUVFITS.h"
#include "ObitIOUVAIPS.h"
#include "ObitUVDesc.h"
#include "ObitUVSel.h"
#include "ObitTableFQ.h"
#include "ObitTableANUtil.h"
#include "ObitTableBP.h"
#include "ObitTableBL.h"
#include "ObitTableCL.h"
#include "ObitTableCQ.h"
#include "ObitTableSN.h"
#include "ObitTableFG.h"
#include "ObitTableNX.h"
#include "ObitTableSNUtil.h"
#include "ObitTableCLUtil.h"
#include "ObitTableFQUtil.h"
#include "ObitTableSUUtil.h"
#include "ObitSystem.h"
#include "ObitMem.h"
#include "ObitHistory.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitUV.c
 * ObitUV class function definitions.
 * This class is derived from the ObitData base class.
 */

/** name of the class defined in this file */
static gchar *myClassName = "ObitUV";

/*--------------- File Global Variables  ----------------*/
/**
 * ClassInfo structure ObitUVClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitUVClassInfo myClassInfo = {FALSE};

/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitUVInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitUVClear (gpointer in);

/** Private: Read selection parameters from ObitInfoList. */
static void ObitUVGetSelect (ObitUV *in, ObitInfoList *info, ObitUVSel *sel,
			     ObitErr *err);

/** Private: Setup for calibration */
static void ObitUVSetupCal (ObitUV *in, ObitErr *err);

/** Private: Assign myIO object */
static void ObitUVSetupIO (ObitUV *in, ObitErr *err);

/** Private: Copy tables with selection */
static ObitIOCode CopyTablesSelect (ObitUV *inUV, ObitUV *outUV, ObitErr *err);

/*----------------------Public functions---------------------------*/
/**
 * Constructor.
 * Initializes class if needed on first call.
 * \param name An optional name for the object.
 * \return the new object.
 */
ObitUV* newObitUV (gchar* name)
{
  ObitUV* out;

  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitUVClassInit();

  /* allocate/init structure */
  out = g_malloc0(sizeof(ObitUV));

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

  /* set ClassInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  ObitUVInit((gpointer)out);

 return out;
} /* end newObitUV */

/**
 * Create a scratch file suitable for accepting the data to be read from in.
 * A scratch UV is more or less the same as a normal UV except that it is
 * automatically deleted on the final unreference.
 * The output will have the underlying files of the same type as in already 
 * allocated.
 * The object is defined but the underlying structures are not created.
 * \param in  The object to copy
 * \param err Error stack, returns if not empty.
 * \return pointer to the new object.
 */
ObitUV* newObitUVScratch (ObitUV *in, ObitErr *err)
{
  const ObitClassInfo *ParentClass;
  ObitUV *out=NULL;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM] = {1,1,1,1,1};
  gchar *outName;
  gint NPIO;
  /* Don't copy Cal and Soln tables */
  /*gchar *exclude[]={"AIPS UV", "AIPS CL", 
    "AIPS SN", "AIPS NX", "AIPS HI", "AIPS PL", "AIPS SL", NULL};*/
  gchar *routine = "newObitUVScratch";

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return NULL;
  g_assert (ObitIsA(in, &myClassInfo));

  /* Delete IO to be sure all selection etc. set */
  in->myIO = ObitIOUnref(in->myIO);

  /* Ensure in fully instantiated */
  ObitUVFullInstantiate (in, TRUE, err);
  if (err->error)Obit_traceback_val (err, routine, in->name, out);

  /* Create - derive object name */
  outName = g_strconcat ("Scratch Copy: ",in->name,NULL);
  out = newObitUV(outName);
  g_free(outName);

  /* Mark as scratch */
  out->isScratch = TRUE;

   /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);

 /* Copy descriptor */
  out->myDesc = (gpointer)ObitUVDescCopy(in->myDesc, out->myDesc, err);
  out->myDesc->nvis = 0;  /* no data yet */
 
   /* Copy number of records per IO */
  ObitInfoListGet (in->info, "nVisPIO", &type, dim,  (gpointer)&NPIO, err);
  ObitInfoListPut (out->info, "nVisPIO", type, dim,  (gpointer)&NPIO, err);

  /* Allocate underlying file */
  ObitSystemGetScratch (in->mySel->FileType, "UV", out->info, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, in);
  
  /* Register in the scratch file list */
  ObitSystemAddScratch ((Obit*)out, err);

  /* File may not be completely properly defined here, defer instantiation */
 return out;
} /* end newObitUVScratch */

/**
 * Returns ClassInfo pointer for the class.
 * \return pointer to the class structure.
 */
gconstpointer ObitUVGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitUVClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitUVGetClass */

/**
 * Test if two ObitUVs have the same underlying structures.
 * This test is done using values entered into the #ObitInfoList
 * in case the object has not yet been opened.
 * \param in1 First object to compare
 * \param in2 Second object to compare
 * \param err ObitErr for reporting errors.
 * \return TRUE if to objects have the same underlying structures
 * else FALSE
 */
gboolean ObitUVSame (ObitUV *in1, ObitUV *in2, ObitErr *err )
{
  /* Call ObitData function */
  return ObitDataSame ((ObitData*)in1, (ObitData*)in2, err);
} /* end ObitUVSame */

/**
 * Delete underlying files and the basic object.
 * \param in Pointer to object to be zapped.
 * \param err ObitErr for reporting errors.
 * \return pointer for input object, NULL if deletion successful
 */
ObitUV* ObitUVZap (ObitUV *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitTable *table=NULL;
  glong i, ver;
  gchar *name;
  gchar *routine = "ObitUVZap";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return in;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));

  /* Close if still active */
  if ((in->myStatus == OBIT_Active) || (in->myStatus == OBIT_Modified)){
   retCode = ObitIOClose(in->myIO, err);
   if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
     Obit_traceback_val (err, routine, in->name, in);    
  }

  /* Ensure in fully instantiated */
  ObitErrLog(err); /* Show any pending messages as they may get lost */
  ObitUVFullInstantiate (in, TRUE, err);
  /* If this fails, clear errors and assume it doesn't exist */
  if (err->error) { 
    ObitErrClearErr(err); 
    return ObitUVUnref(in); 
  }

  /* Loop over table list zapping tables */
   for (i=in->tableList->number; i>=1; i--) {
     ObitTableListGetNumber (in->tableList, i, &name, &ver, &table, err);
     table = ObitTableUnref(table);
     ObitUVZapTable (in, name, ver, err);
     g_free(name);
   } /* end loop over tables */

  /* Delete the file */
  ObitIOZap (in->myIO, err);
  if (err->error)  Obit_traceback_val (err, routine, in->name, in);

  /* If it's scratch remove from list */
  if (in->isScratch) ObitSystemFreeScratch ((Obit*)in, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, in);

  /* Delete object */
  in->isScratch = 0; /* Already deleted underlying structures */
  while (in) in = ObitUVUnref(in); 

  return in;
} /* end ObitUVZap */

/**
 * Make a deep copy of input object.
 * Copies are made of complex members including disk files; these 
 * will be copied applying whatever selection is associated with the input.
 * Objects should be closed on input and will be closed on output.
 * In order for the disk file structures to be copied, the output file
 * must be sufficiently defined that it can be written; the copy does
 * not apply any selection/calibration/translation.
 * The copy will be attempted but no errors will be logged until
 * both input and output have been successfully opened.
 * If the contents of the uv data are copied, all associated tables are 
 * copied first.
 * ObitInfoList and ObitThread members are only copied if the output object
 * didn't previously exist.
 * Parent class members are included but any derived class info is ignored.
 * The file etc. info should have been stored in the ObitInfoList:
 * \li "doCalSelect" OBIT_boolean scalar if TRUE, calibrate/select/edit input data.
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \param err Error stack, returns if not empty.
 * \return pointer to the new object.
 */
ObitUV* ObitUVCopy (ObitUV *in, ObitUV *out, ObitErr *err)
{
  const ObitClassInfo *ParentClass;
  ObitIOCode iretCode, oretCode;
  gboolean oldExist, doCalSelect;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM];
  ObitHistory *inHist=NULL, *outHist=NULL;
  glong count;
  ObitIOAccess access;
  gchar *outName=NULL;
  gchar *routine = "ObitUVCopy";
 
  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return NULL;
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));

  /* Create if it doesn't exist */
  oldExist = out!=NULL;
  if (!oldExist) {
    /* derive object name */
    outName = g_strconcat ("Copy: ",in->name,NULL);
    out = newObitUV(outName);
    g_free(outName);
  }

  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);

  /* copy/set this classes other additions only if out newly created */
  if (!oldExist) {
    /* copy */
    out->myDesc = (gpointer)ObitUVDescCopy(in->myDesc, out->myDesc, err);
    /* Don't copy selector */
    if (out->mySel) out->mySel = ObitUnref (out->mySel);
    out->mySel = newObitUVSel (out->name);
    /* Don't copy info */
    /*out->info = ObitInfoListUnref(out->info);*/
    /*out->info = ObitInfoListRef(in->info);*/
    /* Output will initially have no associated tables */
    out->tableList = ObitTableListUnref(out->tableList);
    out->tableList = newObitTableList(out->name);
    /* don't copy ObitUVSel, ObitThread */
  }

  /* If the output object was created this call it cannot be fully
     defined so we're done */
  if (!oldExist) return out;

  doCalSelect = FALSE;
  ObitInfoListGetTest(in->info, "doCalSelect", &type, (gint32*)dim, &doCalSelect);
  if (doCalSelect) access = OBIT_IO_ReadCal;
  else access = OBIT_IO_ReadOnly;

  /* if output has file designated, copy data */
  /* test open to fully instantiate input and see if it's OK */
  iretCode = ObitUVOpen (in, access, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,in->name, out);

  /* copy Descriptor - this time with full information */
  out->myDesc = ObitUVDescCopy(in->myDesc, out->myDesc, err);

  /* use same data buffer on input and output 
     so don't assign buffer for output */
  if (out->buffer) ObitIOFreeBuffer(out->buffer); /* free existing */
  out->buffer = in->buffer;
  out->bufferSize = -1;

  /* test open output */
  oretCode = ObitUVOpen (out, OBIT_IO_WriteOnly, err);
  /* If this didn't work try OBIT_IO_ReadWrite */
  if ((oretCode!=OBIT_IO_OK) || (err->error)) {
    ObitErrClear(err);
    oretCode = ObitUVOpen (out, OBIT_IO_ReadWrite, err);
  }
  /* if it didn't work bail out */
  if ((oretCode!=OBIT_IO_OK) || (err->error)) {
    /* unset output buffer (may be multiply deallocated) */
    out->buffer = NULL;
    out->bufferSize = 0;
    return out;
  }

  /* Copy any history  unless Scratch */
  if (!in->isScratch && !out->isScratch) {
    inHist  = newObitDataHistory((ObitData*)in, OBIT_IO_ReadOnly, err);
    outHist = newObitDataHistory((ObitData*)out, OBIT_IO_WriteOnly, err);
    outHist = ObitHistoryCopy (inHist, outHist, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, out);
    inHist  = ObitHistoryUnref(inHist);
    outHist = ObitHistoryUnref(outHist);
  }

  /* Copy tables before data */
  iretCode = CopyTablesSelect (in, out, err);
  if (err->error) {/* add traceback,return */
    out->buffer = NULL;
    out->bufferSize = 0;
    Obit_traceback_val (err, routine,in->name, out);
  }

  /* reset to beginning of uv data */
  iretCode = ObitIOSet (in->myIO, in->info, err);
  oretCode = ObitIOSet (out->myIO, in->info, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, out);

  /* Close and reopen input to init calibration which will have been disturbed 
     by the table copy */
  iretCode = ObitUVClose (in, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,in->name, out);

  iretCode = ObitUVOpen (in, access, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,in->name, out);

  /* we're in business, copy */
  count = 0;
  while ((iretCode==OBIT_IO_OK) && (oretCode==OBIT_IO_OK)) {
    if (doCalSelect) iretCode = ObitUVReadSelect (in, in->buffer, err);
    else iretCode = ObitUVRead (in, in->buffer, err);
    if (iretCode!=OBIT_IO_OK) break;
   /* How many */
    out->myDesc->numVisBuff = in->myDesc->numVisBuff;
    count += out->myDesc->numVisBuff;
    oretCode = ObitUVWrite (out, in->buffer, err);
  }
  
  /* check for errors */
  if ((iretCode > OBIT_IO_EOF) || (oretCode > OBIT_IO_EOF) ||
      (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,in->name, out);
  
  /* unset input buffer (may be multiply deallocated ;'{ ) */
  out->buffer = NULL;
  out->bufferSize = 0;
  
  /* close files to be sure */
  iretCode = ObitUVClose (in, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,in->name, out);
  
  /* close files to be sure */
  oretCode = ObitUVClose (out, err);
  if ((oretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine,out->name, out);

  /* Make sure something copied */
  Obit_retval_if_fail((count>0), err, out,
 		      "%s: NO Data copied for %s", routine, in->name);
  
  return out;
} /* end ObitUVCopy */

/**
 * Make a copy of a object but do not copy the actual data
 * This is useful to create a UV object similar to the input one.
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \param err Error stack, returns if not empty.
 */
void ObitUVClone  (ObitUV *in, ObitUV *out, ObitErr *err)
{
  const ObitClassInfo *ParentClass;
  ObitIOCode iretCode, oretCode;
  ObitHistory *inHist=NULL, *outHist=NULL;
  gchar *routine = "ObitUVClone";
 
  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitIsA(out, &myClassInfo));

  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);

  /* copy/set this classes other additions  */
  /* Don't copy selector */
  if (out->mySel) out->mySel = ObitUnref (out->mySel);
  out->mySel = newObitUVSel (out->name);
  /* Output will initially have no associated tables */
  out->tableList = ObitTableListUnref(out->tableList);
  out->tableList = newObitTableList(out->name);
  /* don't copy ObitUVSel, ObitThread */

  /* Open to fully instantiate input and see if it's OK */
  iretCode = ObitUVOpen (in, OBIT_IO_ReadWrite, err);
  if ((iretCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_msg (err, routine,in->name);

  /* copy Descriptor - this time with full information */
  out->myDesc = ObitUVDescCopy(in->myDesc, out->myDesc, err);

  /* Open output */
  oretCode = ObitUVOpen (out, OBIT_IO_WriteOnly, err);
  /* If this didn't work try OBIT_IO_ReadWrite */
  if ((oretCode!=OBIT_IO_OK) || (err->error)) {
    ObitErrClear(err);
    oretCode = ObitUVOpen (out, OBIT_IO_ReadWrite, err);
  }
  /* if it didn't work bail out */
  if ((oretCode!=OBIT_IO_OK) || (err->error)) {
    Obit_traceback_msg (err, routine, out->name);
  }

  /* Copy any history unless Scratch */
  if (!in->isScratch && !out->isScratch) {
    inHist  = newObitDataHistory((ObitData*)in, OBIT_IO_ReadOnly, err);
    outHist = newObitDataHistory((ObitData*)out, OBIT_IO_WriteOnly, err);
    outHist = ObitHistoryCopy (inHist, outHist, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
    inHist  = ObitHistoryUnref(inHist);
    outHist = ObitHistoryUnref(outHist);
  }

  /* Copy tables */
  iretCode = CopyTablesSelect (in, out, err);
  if (err->error) Obit_traceback_msg (err, routine,in->name);

  /* Close files */
  ObitUVClose (in, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  ObitUVClose (out, err);
  if (err->error) Obit_traceback_msg (err, routine, out->name);
} /* end ObitUVClone */

/**
 * Initialize structures and open file.
 * The image descriptor is read if OBIT_IO_ReadOnly, OBIT_IO_ReadCal or 
 * OBIT_IO_ReadWrite and written to disk if opened OBIT_IO_WriteOnly.
 * If access is OBIT_IO_ReadCal then the calibration/selection/editing
 * needed is initialized.
 * See the #ObitUVSel class for a description of the selection and 
 * calibration parameters.
 * After the file has been opened the member, buffer is initialized
 * for reading/storing the data unless member bufferSize is <0.
 * The file etc. info should have been stored in the ObitInfoList:
 * \li "FileType" OBIT_int scalar = OBIT_IO_FITS or OBIT_IO_AIPS 
 *               for file type.
 * \li "nVisPIO" OBIT_int scalar = Maximum number of visibilities
 *               per transfer, this is the target size for Reads (may be 
 *               fewer) and is used to create buffers.
 * \li "Compress" Obit_bool scalar = TRUE indicates output is to be 
 *               in compressed format. (access=OBIT_IO_WriteOnly only).
 * \param in Pointer to object to be opened.
 * \param access access (OBIT_IO_ReadOnly,OBIT_IO_ReadWrite,
 *               OBIT_IO_ReadCal or OBIT_IO_WriteOnly).
 *               If OBIT_IO_WriteOnly any existing data in the output file
 *               will be lost.
 * \param err ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitUVOpen (ObitUV *in, ObitIOAccess access, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  glong need;
  gdouble ra, dec;
  gchar *routine = "ObitUVOpen";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));

  /* Same type of access on descriptor */
  in->myDesc->access = access;

  /* If the file is already open - close it  first */
  if ((in->myStatus==OBIT_Active) || (in->myStatus==OBIT_Modified)) {
    retCode = ObitUVClose (in, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
      Obit_traceback_val (err, routine, in->name, retCode);
  }

  /* set Status */
  in->myStatus = OBIT_Active;

  ObitUVGetSelect (in, in->info, in->mySel, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, retCode);

  /* create appropriate ObitIO */
  ObitUVSetupIO (in, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, retCode);

  /* Add reference to tableList */
  in->myIO->tableList = (Obit*)ObitUnref(in->myIO->tableList);
  in->myIO->tableList = (Obit*)ObitRef(in->tableList);

  in->myIO->access = access; /* save access type */

  /*+++++++++++++++++ Actual file open ++++++++++++++++++*/
  /* most of the instructions for the I/O are in the ObitInfoList */
  retCode = ObitIOOpen (in->myIO, access, in->info, err);
  if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, in->name, retCode);

  /* read or write Headers */
  if ((access == OBIT_IO_ReadOnly) || (access == OBIT_IO_ReadCal) || 
      (access == OBIT_IO_ReadWrite)) {
    /* read header info */
    retCode = ObitIOReadDescriptor(in->myIO, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) Obit_traceback_val (err, routine, in->name, retCode);

    /* Set descriptors for the output on in to reflect the selection
       by in->mySel,  the descriptors on in->myIO will still describe
       the external representation */
    ObitUVSelSetDesc ((ObitUVDesc*)in->myIO->myDesc,(ObitUVSel*)in->myIO->mySel, in->myDesc, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, retCode);

    /* If single source selected, get position in header */
    if (in->mySel->selectSources && (in->mySel->numberSourcesList==1) && 
	(access == OBIT_IO_ReadCal)) {
      ObitUVGetRADec (in, &ra, &dec, err);
      ((ObitUVDesc*)in->myIO->myDesc)->crval[((ObitUVDesc*)in->myIO->myDesc)->jlocr] = ra;
      ((ObitUVDesc*)in->myIO->myDesc)->crval[((ObitUVDesc*)in->myIO->myDesc)->jlocd] = dec;
    }

   /* Output */
  } else if (access == OBIT_IO_WriteOnly) {
    /* Set descriptors for the output on in to reflect the selection
       by in->mySel,  the descriptors on in->myIO will still describe
       the external representation */
    ObitUVSelGetDesc (in->myDesc, (ObitUVSel*)in->myIO->mySel, (ObitUVDesc*)in->myIO->myDesc, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, retCode);
    
    /* Write header info */
    retCode = ObitIOWriteDescriptor(in->myIO, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
      Obit_traceback_val (err, routine, in->name, retCode);
  }

  /* initialize any Calibration needed - complete output Descriptor, Selector*/
  /* get selection parameters for Read/cal/select */
  if ((access==OBIT_IO_ReadOnly) || (access==OBIT_IO_ReadWrite) || 
      (access==OBIT_IO_ReadCal)) {
    ObitUVSetupCal (in, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, retCode);
  }
  
  /* Allocate buffer - resize if necessary */
  /* buffer size < 0 => no buffer desired */
  if (in->bufferSize >= 0) {
    need = ObitUVSelBufferSize(in->myDesc, in->mySel);
    /* is current one big enough? */
    if ((in->buffer!=NULL) && (need>in->bufferSize)) {
      /* no - deallocate */
      if (in->buffer) ObitIOFreeBuffer(in->buffer);
      in->buffer = NULL;
      in->bufferSize = 0;
    }
    /* new one if needed */
    if (in->buffer==NULL)  
      ObitIOCreateBuffer (&in->buffer, &in->bufferSize, in->myIO, 
			  in->info, err);

    /* Check if valid memory */
    if (!ObitMemValid (in->buffer)) {
       Obit_log_error(err, OBIT_Error, 
		     "%s: IO buffer not in Valid memory for %s", 
		     routine, in->name);
      return retCode;
    }
  } /* end buffer allocation */

  /* init I/O */
  retCode = ObitIOSet (in->myIO, in->info, err);
  if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, in->name, retCode);

  /* Set I/O to beginning of the file */
  ((ObitUVDesc*)in->myIO->myDesc)->firstVis = 0;
  /* For WriteOnly the file is truncated.*/
  if (access == OBIT_IO_WriteOnly) {
    ((ObitUVDesc*)in->myIO->myDesc)->firstVis = 1;
    ((ObitUVDesc*)in->myIO->myDesc)->nvis = 0;
    in->myDesc->nvis = 0;
  }

  /* save current location */
  in->myDesc->firstVis   = ((ObitUVDesc*)in->myIO->myDesc)->firstVis;
  in->myDesc->numVisBuff = ((ObitUVDesc*)in->myIO->myDesc)->numVisBuff;

  return retCode;
} /* end ObitUVOpen */

/**
 * Shutdown I/O.
 * \param in Pointer to object to be closed.
 * \param err ObitErr for reporting errors.
 * \return error code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitUVClose (ObitUV *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gchar *routine = "ObitUVClose";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));
  /* Something going on? */
  if (in->myStatus == OBIT_Inactive) return OBIT_IO_OK;

  /* flush buffer if writing */
  if (((in->myIO->access==OBIT_IO_ReadWrite) || 
       (in->myIO->access==OBIT_IO_WriteOnly)) &&
      (in->myStatus == OBIT_Modified)) {
    retCode = ObitIOFlush (in->myIO, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
      Obit_traceback_val (err, routine, in->name, retCode);
 
   /* Update descriptor on myIO */
    ObitUVDescCopyDesc(in->myDesc, (ObitUVDesc*)in->myIO->myDesc, err);
    if (err->error)
      Obit_traceback_val (err, routine, in->name, retCode);

    /* Update header on disk if writing */
    retCode = OBIT_IO_OK;
    if (in->myIO->myStatus != OBIT_Inactive)
      retCode = ObitIOWriteDescriptor(in->myIO, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
      Obit_traceback_val (err, routine, in->name, retCode);    
  }

  /* Close actual file */
  retCode = ObitIOClose (in->myIO, err);
  if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, in->name, retCode);

  /* shutdown any calibration, indexing */
  if (in->myIO->access==OBIT_IO_ReadCal) {
    ObitUVCalShutdown((ObitUVCal*)in->myIO->myCal, err);
    if (err->error)  Obit_traceback_val (err, routine, in->name, retCode);
    ObitUVSelShutdown(in->myIO->mySel, err);
    if (err->error)  Obit_traceback_val (err, routine, in->name, retCode);
  }

  /* Free buffer */
  ObitIOFreeBuffer(in->buffer);
  in->buffer = NULL;

  /* set Status */
  in->myStatus = OBIT_Inactive;

  return retCode;
} /* end ObitUVClose */

/**
 * Ensures full instantiation of object - basically open to read/write header
 * and verify or create file.
 * If object has previously been opened, as demonstrated by the existance
 * of its myIO member, this operation is a no-op.
 * Virtual - calls actual class member
 * \param in     Pointer to object
 * \param exist  TRUE if object should previously exist, else FALSE
 * \param err    ObitErr for reporting errors.
 * \return error code, OBIT_IO_OK=> OK
 */
void ObitUVFullInstantiate (ObitUV *in, gboolean exist, ObitErr *err)
{
  ObitIOAccess access;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM];
  gboolean doCalSelect;
  gchar *routine = "ObitUVFullInstantiate";

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

  if (in->myIO) return;  /* is this needed? */

  /* Open readonly if it should exist, else writeonly */
  if (exist) {
    /* If doCalSelect set use ReadCal */
    doCalSelect = FALSE;
    ObitInfoListGetTest(in->info, "doCalSelect", &type, (gint32*)dim, &doCalSelect);
    if (doCalSelect) access = OBIT_IO_ReadCal;
    else access = OBIT_IO_ReadOnly;
  }  else access = OBIT_IO_WriteOnly;

  in->bufferSize = -1;  /* Don't need to assign buffer here */
  
  /* Open and close */
  ObitUVOpen(in, access, err);
  ObitUVClose(in, err);
  if (err->error)Obit_traceback_msg (err, routine, in->name);
  in->bufferSize = 0;  /* May need buffer later */
} /* end ObitUVFullInstantiate */

/**
 * Read uv data data from disk.
 * The ObitUVDesc maintains the current location in the file.
 * The number read will be mySel->nVisPIO (until the end of the selected
 * range of visibilities in which case it will be smaller).
 * The first visibility number after a read is myDesc->firstVis
 * and the number of visibilities is myDesc->numVisBuff.
 * \param in Pointer to object to be read.
 * \param data pointer to buffer to write results.
 *             if NULL, use the buffer member of in.
 * \param err ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK => OK
 */
ObitIOCode ObitUVRead (ObitUV *in, gfloat *data, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitIOAccess access;
  gfloat *myBuf = data;
  glong need;
  gchar *routine = "ObitUVRead";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));

 /* check and see if its open - if not attempt */
  if ((in->myStatus!=OBIT_Active) && (in->myStatus!=OBIT_Modified)) {
    access = OBIT_IO_ReadOnly;
    retCode = ObitIOOpen (in->myIO, access, in->info, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback, return */
      Obit_traceback_val (err, routine, in->name, retCode);
  }

  /* select internal or external buffer */
  if (myBuf==NULL) {
    myBuf = in->buffer;
    /* Check that internal buffer ( defined in gfloats) large enough */
    need = in->mySel->nVisPIO*in->myDesc->lrec;
    if (need > in->bufferSize) {
      Obit_log_error(err, OBIT_Error, 
		     "IO buffer (%ld) too small, need %ld for %s", 
		     in->bufferSize, need, in->name);
      return retCode;
    }
  } 
  g_assert (myBuf != NULL); /* check it */

  retCode = ObitIORead (in->myIO, myBuf, err);
  if ((retCode > OBIT_IO_EOF) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, in->name, retCode);

  /* save current location */
  in->myDesc->firstVis   = ((ObitUVDesc*)in->myIO->myDesc)->firstVis;
  in->myDesc->numVisBuff = ((ObitUVDesc*)in->myIO->myDesc)->numVisBuff;

  return retCode;
} /* end ObitUVRead */

/**
 * Read data from disk applying selection.
 * The number read will be mySel->nVisPIO (until the end of the selected
 * range of visibilities in which case it will be smaller).
 * The first visibility number after a read is myDesc->firstVis
 * and the number of visibilities is myDesc->numVisBuff.
 * \param in Pointer to object to be read.
 * \param data pointer to buffer to write results.
 *             if NULL, use the buffer member of in.
 * \param err ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK => OK
 */
ObitIOCode ObitUVReadSelect (ObitUV *in, gfloat *data, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitIOAccess access;
  gfloat *myBuf = data;
  glong need;
  gchar *routine = "ObitUVReadSelect";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));

 /* check and see if its open - if not attempt */
  if ((in->myStatus!=OBIT_Active) && (in->myStatus!=OBIT_Modified)) {
    access = OBIT_IO_ReadCal;
    retCode = ObitIOOpen (in->myIO, access, in->info, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback, return */
      Obit_traceback_val (err, routine, in->name, retCode);
  }

  /* select internal or external buffer */
  if (myBuf==NULL) {
    myBuf = in->buffer;
    /* Check that internal buffer ( defined in gfloats) large enough */
    need = in->mySel->nVisPIO*in->myDesc->lrec;
    if (need > in->bufferSize) {
      Obit_log_error(err, OBIT_Error, 
		     "IO buffer (%ld) too small, need %ld for %s", 
		     in->bufferSize, need, in->name);
      return retCode;
    }
  } 
  g_assert (myBuf != NULL); /* check it */

  retCode = ObitIOReadSelect (in->myIO, myBuf, err);
  if ((retCode > OBIT_IO_EOF) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, in->name, retCode);

  /* save current location */
  in->myDesc->firstVis   = ((ObitUVDesc*)in->myIO->myDesc)->firstVis;
  in->myDesc->numVisBuff = ((ObitUVDesc*)in->myIO->myDesc)->numVisBuff;

  return retCode;
} /* end ObitUVReadSelect */

/**
 * Write information to disk.
 * The data in the buffer will be written starting at visibility
 * myDesc->firstVis and the number written will be myDesc->numVisBuff
 * which should not exceed mySel->nVisPIO if the internal buffer is used.
 * myDesc->firstVis will be maintained and need not be changed for
 * sequential writing.
 * \param in Pointer to object to be written.
 * \param data pointer to buffer containing input data.
 *             if NULL, use the buffer member of in.
 * \param err ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitUVWrite (ObitUV *in, gfloat *data, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitIOAccess access;
  gfloat *myBuf = data;
  glong need;
  gchar *routine = "ObitUVWrite";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));

  /* check and see if its open - if not attempt */
  if ((in->myStatus!=OBIT_Modified) && (in->myStatus!=OBIT_Active)) {
    access = OBIT_IO_WriteOnly;
    retCode = ObitIOOpen (in->myIO, access, in->info, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
      Obit_traceback_val (err, routine, in->name, retCode);
  }

  /* select internal or external buffer */
  if (myBuf==NULL) {
    myBuf = in->buffer;
    /* Check that internal buffer ( defined in gfloats) large enough */
    need = in->mySel->nVisPIO*in->myDesc->lrec;
    if (need > in->bufferSize) {
      Obit_log_error(err, OBIT_Error, 
		     "IO buffer (%ld) too small, need %ld for %s", 
		     in->bufferSize, need, in->name);
      return retCode;
    }
  } 
  g_assert (myBuf != NULL); /* check it */

  /* set number and location to write on myIO descriptor */
  in->myDesc->firstVis = MAX (1, in->myDesc->firstVis);
  ((ObitUVDesc*)in->myIO->myDesc)->firstVis   = in->myDesc->firstVis;
  ((ObitUVDesc*)in->myIO->myDesc)->numVisBuff = in->myDesc->numVisBuff;

  /* most of the instructions for the I/O are in the ObitInfoList */
  retCode = ObitIOWrite (in->myIO, myBuf, err);
  if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, in->name, retCode);

  /* set Status */
  in->myStatus = OBIT_Modified;

  /* save current location */
  in->myDesc->firstVis   = ((ObitUVDesc*)in->myIO->myDesc)->firstVis;
  in->myDesc->numVisBuff = ((ObitUVDesc*)in->myIO->myDesc)->numVisBuff;
  in->myDesc->nvis       = ((ObitUVDesc*)in->myIO->myDesc)->nvis;

  return retCode;
} /* end ObitUVWrite */

/**
 * Return a ObitTable Object to a specified table associated with
 * the input ObitUV.  
 * If such an object exists, a reference to it is returned,
 * else a new object is created and entered in the ObitTableList.
 * \param in       Pointer to object with associated tables.
 *                 This MUST have been opened before this call.
 * \param access   access (OBIT_IO_ReadOnly,OBIT_IO_ReadWrite,
 *                 or OBIT_IO_WriteOnly).
 *                 This is used to determine defaulted version number
 *                 and a different value may be used for the actual 
 *                 Open.
 * \param tabType  The table type (e.g. "AIPS CC").
 * \param tabVer   Desired version number, may be zero in which case
 *                 the highest extant version is returned for read
 *                 and the highest+1 for write.
 * \param err      ObitErr for reporting errors.
 * \return pointer to created ObitTable, NULL on failure.
 */
ObitTable* 
newObitUVTable (ObitUV *in, ObitIOAccess access, 
		gchar *tabType, glong *tabVer, ObitErr *err)
{
  /* Call ObitData function */
  return newObitDataTable ((ObitData*)in, access, tabType, tabVer, err);
} /* end newObitUVTable */

/**
 * Destroy a specified table(s) associated with the input ObitUV.  
 * The table is removed from the ObitTableList but the external form
 * may not be updated.
 * \param in       Pointer to object with associated tables.
 * \param tabType  The table type (e.g. "AIPS CC").
 * \param tabVer   Desired version number, may be zero in which case
 *                 the highest extant version is returned for read
 *                 and the highest+1 for write.
 *                 -1 => all versions of tabType
 * \param err      ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitUVZapTable (ObitUV *in, gchar *tabType, glong tabVer, 
			   ObitErr *err)
{
  /* Call ObitData function */
  return ObitDataZapTable ((ObitData*)in, tabType, tabVer, err);
} /* end ObitUVZapTable */

/**
 * Copies the associated tables from one ObitUV to another.
 * \param in      The ObitUV with tables to copy.
 * \param out     An ObitUV to copy the tables to, old ones replaced.
 * \param exclude a NULL termimated list of table types NOT to copy.
 *                If NULL, use include
 * \param include a NULL termimated list of table types to copy.
 *                ignored if exclude nonNULL.
 * \param err     ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitUVCopyTables (ObitUV *in, ObitUV *out, gchar **exclude,
			     gchar **include, ObitErr *err)
{
  /* Call ObitData function */
  return ObitDataCopyTables ((ObitData*)in, (ObitData*)out, 
			     exclude, include, err);
} /* end ObitUVCopyTables */

/**
 * Update any disk resident structures about the current tables.
 * \param in   Pointer to object to be updated.
 * \param err ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitUVUpdateTables (ObitUV *in, ObitErr *err)
{
  /* Call ObitData function */
  return ObitDataUpdateTables ((ObitData*)in, err);
} /* end ObitUVUpdateTables */

/**
 * Fills in frequency information in Descriptor from header and FQ table.
 * These are the myDesc->freqArr and myDesc->fscale array members.
 * \param in      The ObitUV with descriptor to update.
 * \param err     ObitErr for reporting errors.
 */
void ObitUVGetFreq (ObitUV* in, ObitErr *err) 
{
  ObitUVDesc* desc = NULL;
  ObitTable *tab = NULL;
  ObitTableFQ *fqtab = NULL;
  glong ver;
  gchar *routine = "ObitUVGetFreq";
  
  /* error check */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

  /* get FQ table */
  ver = 1;
  tab = newObitUVTable (in, OBIT_IO_ReadOnly, "AIPS FQ", &ver, err);
  if (tab==NULL) {
       Obit_log_error(err, OBIT_Error, 
		      "%s: No AIPS FQ table found for %s", routine, in->name);
      return;
 }
  fqtab = ObitTableFQConvert(tab);
  tab = ObitTableUnref(tab);
  if (err->error) Obit_traceback_msg (err, routine, in->name);
  
  /* update descriptors */
  desc = in->myDesc;
  ObitUVDescGetFreq (desc, (Obit*)fqtab, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Also need it on the IO descriptor */
  if (in->myIO) {
    desc = in->myIO->myDesc;
    ObitUVDescGetFreq (desc, (Obit*)fqtab, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
  }

  /* done with fq table */
  fqtab = ObitTableUnref(fqtab);

} /* end  ObitUVGetFreq */

/**
 * Determine subarray information for an ObitUV and add to the descriptors.
 * \param in    Object to obtain data from and with descriptors to update.
 *              Updated on in->myDesc and in->myIO->myDesc:
 * \li numSubA  Number of subarrays, always >0
 * \li numAnt   Array of maximum antenna numbers per subarray
 *              will be allocaded here and must be gfreeed when done
 *              NULL returned if no AN tables
 * \li maxAnt   Maximum antenna number in numAnt.  0 if no AN tables.
 * \param *err  ObitErr error stack.
 * \return I/O Code  OBIT_IO_OK = OK.
 */
ObitIOCode ObitUVGetSubA (ObitUV *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  glong highANver, iANver;
  ObitTableAN *ANTable = NULL;
  ObitUVDesc  *desc = NULL, *IOdesc = NULL;
  gchar *routine = "ObitUVGetSubA";
 
  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitUVIsA(in));
  g_assert (ObitIOIsA(in->myIO));
  g_assert (ObitUVDescIsA(in->myIO->myDesc));

  /* pointer to descriptors */
  desc   = (ObitUVDesc*)in->myDesc;
  IOdesc = (ObitUVDesc*)in->myIO->myDesc;

  /* Default outputs */
  desc->numSubA = 1;
  desc->maxAnt  = 0;
  IOdesc->numSubA = 1;
  IOdesc->maxAnt  = 0;

  /* How many AN tables (subarrays) */
  highANver = ObitTableListGetHigh (in->tableList, "AIPS AN");

  /* are there any? */
  if (highANver <= 0) return OBIT_IO_OK;

  /* allocate arrays */
  desc->numSubA = highANver;
  if (desc->numAnt) g_free(desc->numAnt);
  desc->numAnt = g_malloc0(highANver*sizeof(oint*));
  IOdesc->numSubA = highANver;
  if (IOdesc->numAnt) g_free(IOdesc->numAnt);
  IOdesc->numAnt = g_malloc0(highANver*sizeof(oint*));

  /* Loop over AN tables */
  for (iANver=1; iANver<=highANver; iANver++) {

    /* Get table */
    ANTable = 
      newObitTableANValue (in->name, (ObitData*)in, &iANver, OBIT_IO_ReadOnly, 0, 0, err);
    if ((err->error) || (ANTable==NULL)) 
      Obit_traceback_val (err, routine, in->name, retCode);

    /* Get info from table */
    retCode = ObitTableANGetInfo (ANTable, &(desc->numAnt[iANver-1]), NULL, NULL, err);
    if ((err->error) || (retCode!=OBIT_IO_OK)) 
      Obit_traceback_val (err, routine, in->name, retCode);
 
    /* save to I/O descriptor */
    IOdesc->numAnt[iANver-1] = desc->numAnt[iANver-1];

    /* max antenna number */
    desc->maxAnt = MAX (desc->maxAnt, desc->numAnt[iANver-1]);

    /* release table object */
    ANTable = ObitTableANUnref(ANTable);
  }

  /* save to I/O descriptor */
  IOdesc->maxAnt = desc->maxAnt;

  return retCode;
} /* end ObitUVGetSubA  */

/**
 * Reposition IO to beginning of file
 * \param in   Pointer to object to be rewound.
 * \param err  ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitUVIOSet (ObitUV *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gchar *routine = "ObitUVIOSet";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));

  in->myDesc->firstVis   = 1;
  in->myDesc->numVisBuff = 0;

  /* Barf and die if inactive */
  Obit_retval_if_fail((in->myStatus!=OBIT_Inactive), err, retCode,
 		      "%s: IO inactive for %s", routine, in->name);
  
  return ObitIOSet (in->myIO, in->info, err);
} /* end ObitUVIOSet */

/**
 * Get source position.  
 * If single source file get from uvDesc, 
 * if multisource read from SU table
 * Checks that only one source selected.
 * Also fill in position like information in the descriptor 
 * for multi-source datasets
 * \param  uvdata  Data object from which position sought
 * \param  ra      [out] RA at mean epoch (deg)
 * \param  dec     [out] Dec at mean epoch (deg)
 * \param  err     Error stack, returns if not empty.
 */
void  ObitUVGetRADec (ObitUV *uvdata, gdouble *ra, gdouble *dec, 
		      ObitErr *err)
{
  ObitSourceList *sList=NULL;
  ObitSource     *source=NULL;
  ObitTable      *tmpTable;
  ObitTableSU    *SUTable=NULL;
  ObitUVSel      *sel;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM];
  glong i, j, SourID, ver, count;
  gboolean found;
  gchar *sptr, *tabType = "AIPS SU";
  gchar *routine = "ObitUVUtil:GetRADec";

  /* error checks */
  g_assert(ObitUVIsA(uvdata));
  g_assert(ObitErrIsA(err));
  if (err->error) return;

  /* descriptor ilocsu > 0 indicates multisource */
  if (uvdata->myDesc->ilocsu >= 0) { /* multisource */
    sel = uvdata->mySel; /* Selector */

    /* Get SU table */
    ver = 1;
    tmpTable = newObitUVTable (uvdata, OBIT_IO_ReadWrite, tabType, &ver, err);
    SUTable = ObitTableSUConvert(tmpTable);
    tmpTable = ObitTableUnref(tmpTable);
    if (err->error) Obit_traceback_msg (err, routine, uvdata->name);

    /* Make sure selector initialized */
    if (!sel->sources) {

      /* Get selected sources */
      if (ObitInfoListGetP(uvdata->info, "Sources", &type, dim, (gpointer)&sptr)) {
	/* Count actual entries in source list */
	count = 0;  j = 0;
	for (i=0; i<dim[1]; i++) {
	  if ((sptr[j]!=' ') || (sptr[j+1]!=' ')) count++;
	  j += dim[0];
	}
	sel->numberSourcesList = count;
	sel->sources = g_realloc(sel->sources, sel->numberSourcesList*sizeof(gint));
      } else { /* Trouble - no sources selected */
	Obit_log_error(err, OBIT_Error,"%s: Sources not specified on %s",
		       routine, uvdata->name);
	SUTable = ObitTableSUUnref(SUTable);   /* Done with table */
	return;
      }

      ObitTableSULookup (SUTable, dim, sptr, sel->sources, &sel->selectSources, err);
      if(err->error)  Obit_traceback_msg (err, routine, SUTable->name);
    }

    /* There must be exactly one source selected */
    if (uvdata->mySel->numberSourcesList!=1) {
      Obit_log_error(err, OBIT_Error,"%s: Exactly one source must be selected in %s",
		   routine, uvdata->name);
      SUTable = ObitTableSUUnref(SUTable);   /* Done with table */
      return;
    }

    /* Which source is it? */
    SourID = uvdata->mySel->sources[0];

    /* Get source list */
    sList = ObitTableSUGetList (SUTable, err);
    SUTable = ObitTableSUUnref(SUTable);   /* Done with table */
    if (err->error) Obit_traceback_msg (err, routine, SUTable->name);

    /* Look up source in table */
    found = FALSE;
    for (i=0; i<sList->number; i++) {
      source = sList->SUlist[i];
      if (source->SourID == SourID) {
	*ra  = source->RAMean;
	*dec = source->DecMean;
	/* Set equinox on descriptor as well */
	uvdata->myDesc->equinox = source->equinox;
	/* Observed RA, dec if not given */
	if (uvdata->myDesc->obsra==0.0)  uvdata->myDesc->obsra  = source->RAMean;
	if (uvdata->myDesc->obsdec==0.0) uvdata->myDesc->obsdec = source->DecMean;
	/* Just in case */
	uvdata->myDesc->crval[uvdata->myDesc->jlocr] = source->RAMean;
	uvdata->myDesc->crval[uvdata->myDesc->jlocd] = source->DecMean;

	found = TRUE;
      }
    }
    sList = ObitSourceListUnref(sList);  /* Done with list */

    /* Check if found */
    if (!found) {
      Obit_log_error(err, OBIT_Error,"%s: Selected source %ld not found in list %s",
		   routine, SourID, uvdata->name);
      return;
    }

  } else { /* single source */
    *ra  = uvdata->myDesc->crval[uvdata->myDesc->jlocr];
    *dec = uvdata->myDesc->crval[uvdata->myDesc->jlocd];
  }
} /* end ObitUVGetRADec */
/*-------Private functions called by ObitData class ------*/
/** Private:  Copy Constructor for scratch file*/
static ObitData* newObitDataUVScratch (ObitData *in, ObitErr *err)
{
  return (ObitData*) newObitUVScratch ((ObitUV*)in, err);
} /* end newObitDataUVScratch  */

/** Private: Copy (deep) constructor.  */
static ObitData* ObitDataUVCopy  (ObitData *in, ObitData *out, 
				  ObitErr *err)
{
  return (ObitData*) ObitUVCopy ((ObitUV*)in, (ObitUV*)out, err);
} /* end  ObitDataUVCopy*/

/** Private: Copy structure */
static void ObitDataUVClone (ObitData *in, ObitData *out, ObitErr *err)
{
  ObitUVClone ((ObitUV*)in, (ObitUV*)out, err);
} /* end ObitDataUVClone */

/** Private: Zap */
static ObitData* ObitDataUVZap (ObitData *in, ObitErr *err)
{
  return (ObitData*)ObitUVZap ((ObitUV*)in, err);
} /* end ObitDataUVClone */

/** Private: Open */
static ObitIOCode ObitDataUVOpen (ObitData *in, ObitIOAccess access, 
				  ObitErr *err)
{
  return ObitUVOpen ((ObitUV*)in, access, err);
} /* end ObitUDataUVOpen */

/** Private: Close  */
static ObitIOCode ObitDataUVClose (ObitData *in, ObitErr *err)
{
  return ObitUVClose ((ObitUV*)in, err);
} /* end ObitDataUVClose */

/** Private:  Reset IO to start of file  */
static ObitIOCode ObitDataUVIOSet (ObitData *in, ObitErr *err)
{
  return ObitUVIOSet ((ObitUV*)in, err);
} /* end  ObitDataUVIOSet */

/** Private: Assign myIO object */
static void ObitDataUVSetupIO (ObitData *in, ObitErr *err)
{
  ObitUVSetupIO ((ObitUV*)in, err);
} /* end ObitDataUVSetupIO */

/** Private: full instantiation */
static void ObitDataUVFullInstantiate (ObitData *in, gboolean exist, 
				       ObitErr *err)
{
  ObitUVFullInstantiate ((ObitUV*)in, exist, err);
} /* end ObitDataUCFullInstantiate */

/**
 * Initialize global ClassInfo Structure.
 */
 void ObitUVClassInit (void)
{
  const ObitClassInfo *ParentClass;

  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;

  /* Initialize (recursively) parent class first */
  ParentClass = ObitDataGetClass();
  ObitDataClassInit();  /* Initialize parent class if needed */

  /* function pointers etc. for this class */
  myClassInfo.hasScratch    = TRUE; /* Scratch files allowed */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = (ObitClassInitFP)ObitUVClassInit;
  myClassInfo.newObit       = (newObitFP)newObitUV;
  myClassInfo.newObitUVScratch  = (newObitUVScratchFP)newObitUVScratch;
  myClassInfo.ObitUVSame    = (ObitUVSameFP)ObitUVSame;
  myClassInfo.ObitUVZap     = (ObitUVZapFP)ObitUVZap;
  myClassInfo.ObitCopy      = (ObitCopyFP)ObitUVCopy;
  myClassInfo.ObitClone     = NULL;  /* Different call */
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitUVClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitUVInit;
  myClassInfo.ObitUVOpen    = (ObitUVOpenFP)ObitUVOpen;
  myClassInfo.ObitUVClose   = (ObitUVCloseFP)ObitUVClose;
  myClassInfo.ObitUVIOSet   = (ObitUVIOSetFP)ObitUVIOSet;
  myClassInfo.ObitUVRead    = (ObitUVReadFP)ObitUVRead;
  myClassInfo.ObitUVReadSelect = (ObitUVReadSelectFP)ObitUVReadSelect;
  myClassInfo.ObitUVWrite   = (ObitUVWriteFP)ObitUVWrite;
  myClassInfo.newObitUVTable= (newObitUVTableFP)newObitUVTable;
  myClassInfo.ObitUVZapTable= (ObitUVZapTableFP)ObitUVZapTable;
  myClassInfo.ObitUVFullInstantiate= 
    (ObitUVFullInstantiateFP)ObitUVFullInstantiate;
  myClassInfo.ObitUVCopyTables= 
    (ObitUVCopyTablesFP)ObitUVCopyTables;
  myClassInfo.ObitUVUpdateTables= 
    (ObitUVUpdateTablesFP)ObitUVUpdateTables;
  /* Function pointers referenced from ObitData class */
  myClassInfo.newObitDataScratch  = (newObitDataScratchFP)newObitDataUVScratch;
  myClassInfo.ObitDataZap     = (ObitDataZapFP)ObitDataUVZap;
  myClassInfo.ObitDataClone   = (ObitDataCloneFP)ObitDataUVClone;
  myClassInfo.ObitDataCopy    = (ObitDataCopyFP)ObitDataUVCopy;
  myClassInfo.ObitDataOpen    = (ObitDataOpenFP)ObitDataUVOpen;
  myClassInfo.ObitDataClose   = (ObitDataCloseFP)ObitDataUVClose;
  myClassInfo.ObitDataIOSet   = (ObitDataIOSetFP)ObitDataUVIOSet;
  myClassInfo.ObitDataSetupIO = (ObitDataSetupIOFP)ObitDataUVSetupIO;
  myClassInfo.ObitDataFullInstantiate= 
    (ObitDataFullInstantiateFP)ObitDataUVFullInstantiate;
 } /* end ObitUVClassInit */

/*---------------Private functions--------------------------*/

/**
 * Creates empty member objects, initialize reference count.
 * Parent classes portions are (recursively) initialized first
 * \param inn Pointer to the object to initialize.
 */
void ObitUVInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitUV *in = inn;

  /* error checks */
  g_assert (in != NULL);

  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->myIO      = NULL;
  in->myDesc    = newObitUVDesc(in->name);
  in->mySel     = newObitUVSel(in->name);
  in->myStatus  = OBIT_Inactive;
  in->buffer    = NULL;
  in->bufferSize= 0;
  in->isScratch = FALSE;

} /* end ObitUVInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * For some reason this wasn't build into the GType class.
 * \param  inn Pointer to the object to deallocate.
 *           Actually it should be an ObitUV* cast to an Obit*.
 */
void ObitUVClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitUV *in = inn;
  ObitErr *err;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* Delete underlying files if isScratch */
  if (in->isScratch) {
    err = newObitErr();     /* for possible messages */
    /* Remove from ObitSystem list */
    ObitSystemFreeScratch ((Obit*)in, err);
    in->isScratch = FALSE;  /* avoid infinite recursion */
    ObitUVZap (in, err);    /* delete files */
    ObitErrLog(err);
    err = ObitErrUnref(err);
  }

  /* delete this class members */
  in->thread    = ObitThreadUnref(in->thread);
  in->info      = ObitInfoListUnref(in->info);
  in->myIO      = ObitUnref(in->myIO);
  in->myDesc    = ObitUVDescUnref(in->myDesc);
  in->mySel     = ObitUVSelUnref(in->mySel);
  in->tableList = ObitUnref(in->tableList);
  if (in->buffer) ObitIOFreeBuffer(in->buffer); 
  
  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);
  
} /* end ObitUVClear */

/**
 * Get requested information from the ObitInfoList
 * \param in   UVdata.
 * \param info Pointer to InfoList
 * \param sel  pointer to uvdata selector to update.
 * \param err  ObitErr for reporting errors.
 */
static void ObitUVGetSelect (ObitUV *in, ObitInfoList *info, ObitUVSel *sel,
			     ObitErr *err)
{
  ObitInfoType type;
  gint32 i, dim[MAXINFOELEMDIM];
  gint itemp, *iptr;
  glong iver, j, count;
  gfloat ftempArr[10];
  ObitTableSU *SUTable=NULL;
  union ObitInfoListEquiv InfoReal; 
  gchar tempStr[5], *sptr;
  gchar *routine = "ObitUVGetSelect";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitInfoListIsA(info));
  g_assert (ObitUVSelIsA(sel));

  /* what type of underlying file? */
  if (!ObitInfoListGet(info, "FileType", &type, (gint32*)&dim, 
		       (gpointer)&sel->FileType, err)) {
    /* couldn't find it - add message to err and return */
    Obit_log_error(err, OBIT_Error, 
		"ObitUVGetSelect: entry FileType not in InfoList Object %s",
		sel->name);
  }

  /* Maximum number of visibilities per read/write? [default 100] */
  sel->nVisPIO = 100;
  ObitInfoListGetTest(info, "nVisPIO", &type, (gint32*)dim, &sel->nVisPIO);
  sel->nVisPIO = MAX (1, sel->nVisPIO); /* no fewer than 1 */

  /* Compress output? */
  sel->Compress = FALSE;
  ObitInfoListGetTest(info, "Compress", &type, (gint32*)dim, &sel->Compress);

  /* Following only needed for ReadCal */
  if (in->myDesc->access != OBIT_IO_ReadCal) {
    /* Default selection */
    sel->SubA         = 0;
    sel->FreqID       = 0;
    sel->startChann   = 1;
    sel->numberChann  = in->myDesc->inaxes[in->myDesc->jlocf];
    sel->startIF      = 1;
    sel->numberIF     = in->myDesc->inaxes[in->myDesc->jlocif];
    sel->numberPoln   = in->myDesc->inaxes[in->myDesc->jlocs];
    sel->doPolCal     = FALSE;
    sel->timeRange[0] = -1.0e20; sel->timeRange[1] = 1.0e20;
    return; 
  }

  /* Calibrate/select/edit output? */
  sel->doCalSelect = FALSE;
  ObitInfoListGetTest(info, "doCalSelect", &type, (gint32*)dim, &sel->doCalSelect);

  /* Selection */
  InfoReal.itg = 0; type = OBIT_oint;
  ObitInfoListGetTest(info, "Subarray", &type, (gint32*)dim, &InfoReal);
  if (type==OBIT_float) itemp = InfoReal.flt + 0.5;
  else itemp = InfoReal.itg;
  sel->SubA  = itemp;

  InfoReal.itg = 0;type = OBIT_oint;
  ObitInfoListGetTest(info, "freqID", &type, (gint32*)dim, &InfoReal);
  if (type==OBIT_float) itemp = InfoReal.flt + 0.5;
  else itemp = InfoReal.itg;
  sel->FreqID = itemp;

  InfoReal.itg = 0;type = OBIT_oint;
  ObitInfoListGetTest(info, "BChan", &type, (gint32*)dim, &InfoReal);
  if (type==OBIT_float) itemp = InfoReal.flt + 0.5;
  else itemp = InfoReal.itg;
  sel->startChann  = itemp;

  InfoReal.itg = 0; type = OBIT_oint;
  ObitInfoListGetTest(info, "EChan", &type, (gint32*)dim, &InfoReal);
  if (type==OBIT_float) itemp = InfoReal.flt + 0.5;
  else itemp = InfoReal.itg;
  if (itemp>0) sel->numberChann = itemp - sel->startChann+1;
  else  sel->numberChann = 0;

  InfoReal.itg = 0; type = OBIT_oint;
  ObitInfoListGetTest(info, "BIF", &type, (gint32*)dim, &InfoReal);
  if (type==OBIT_float) itemp = InfoReal.flt + 0.5;
  else itemp = InfoReal.itg;
  sel->startIF  = itemp;

  InfoReal.itg = 0; type = OBIT_oint;
  ObitInfoListGetTest(info, "EIF", &type, (gint32*)dim, &InfoReal);
  if (type==OBIT_float) itemp = InfoReal.flt + 0.5;
  else itemp = InfoReal.itg;
  if (itemp>0) sel->numberIF = itemp - sel->startIF+1;
  else sel->numberIF = 0;

  for (i=0; i<4; i++) tempStr[i] = ' '; tempStr[4] = 0;
  ObitInfoListGetTest(info, "Stokes", &type, (gint32*)dim, &tempStr);
  for (i=0; i<4; i++) sel->Stokes[i] = tempStr[i]; sel->Stokes[4] = 0;

  /* Polarization calibration */
  InfoReal.itg = 0; type = OBIT_oint;
  ObitInfoListGetTest(info, "doPol", &type, (gint32*)dim, &InfoReal);
  if (type==OBIT_float) itemp = InfoReal.flt + 0.5;
  else itemp = InfoReal.itg;
  sel->doPolCal = itemp > 0;

  /* amp/phase/delay/rate Calibration */
  InfoReal.itg = 0; type = OBIT_oint;
  ObitInfoListGetTest(info, "doCalib", &type, (gint32*)dim, &InfoReal);
  if (type==OBIT_float) itemp = InfoReal.flt + 0.5;
  else itemp = InfoReal.itg;
  sel->doCal = itemp > 0;
  sel->doCalWt = itemp > 1;
  InfoReal.itg = 0; type = OBIT_oint;
  ObitInfoListGetTest(info, "gainUse", &type, (gint32*)dim, &InfoReal);
  sel->calVersion = InfoReal.itg;

  /* Flagging */
  InfoReal.itg = -1; type = OBIT_oint;
  ObitInfoListGetTest(info, "flagVer", &type, (gint32*)dim, &InfoReal);
  if (type==OBIT_float) itemp = InfoReal.flt + 0.5;
  else itemp = InfoReal.itg;
  sel->doFlag = itemp >= 0;
  sel->FGversion = itemp;

  /* Correlation type */
  InfoReal.itg = 0; type = OBIT_oint;
  ObitInfoListGetTest(info, "corrType", &type, (gint32*)dim, &InfoReal);
  if (type==OBIT_float) itemp = InfoReal.flt + 0.5;
  else itemp = InfoReal.itg;
  sel->corrType = itemp;

  /* Baseline dependent calibration */
  InfoReal.itg = 0; type = OBIT_oint;
  ObitInfoListGetTest(info, "BLVer", &type, (gint32*)dim, &InfoReal);
  if (type==OBIT_float) itemp = InfoReal.flt + 0.5;
  else itemp = InfoReal.itg;
  sel->doBLCal = itemp > 0;
  sel->BPversion = itemp;

  /* Bandpass calibration */
  InfoReal.itg = 0; type = OBIT_oint;
  ObitInfoListGetTest(info, "doBand", &type, (gint32*)dim, &InfoReal);
  if (type==OBIT_float) itemp = InfoReal.flt + 0.5;
  else itemp = InfoReal.itg;
  sel->doBPCal = itemp > 0;
  sel->doBand = itemp;
  itemp = 0;
  ObitInfoListGetTest(info, "BPVer", &type, (gint32*)dim, &InfoReal);
  sel->BPversion = itemp;

  /* Spectral smoothing */
  for (i=0; i<3; i++) ftempArr[i] = 0.0; 
  ObitInfoListGetTest(info, "Smooth", &type, (gint32*)dim, &ftempArr);
  for (i=0; i<3; i++) sel->smooth[i] = ftempArr[i];

  /* Time Range */
  ftempArr[0] = -1.0e20; ftempArr[1] = 1.0e20; 
  ObitInfoListGetTest(info, "timeRange", &type, (gint32*)dim, &ftempArr);
  for (i=0; i<2; i++) sel->timeRange[i] = ftempArr[i];
  /* If both=0.0 take all */
  if ((sel->timeRange[0]==0.0) && (sel->timeRange[1]==0.0)) {
    sel->timeRange[0] = -1.0e20; sel->timeRange[1] = 1.0e20;
  }

  /* UV Range */
  ftempArr[0] = 0.0; ftempArr[1] = 1.0e20; 
  ObitInfoListGetTest(info, "UVRange", &type, (gint32*)dim, &ftempArr);
  for (i=0; i<2; i++) sel->UVRange[i] = ftempArr[i];

  /* Selected antennas */
  if (ObitInfoListGetP(info, "Antennas", &type, (gint32*)dim, (gpointer)&iptr)) {
    sel->numberAntList = dim[0];
    sel->ants = g_realloc(sel->ants, sel->numberAntList*sizeof(gint));
    /* loop copying, checking for deselection */
    sel->selectAnts = FALSE;
    for (i=0; i<sel->numberAntList; i++) {
      sel->ants[i] = abs (iptr[i]);
      sel->selectAnts = sel->selectAnts || (sel->ants[i]>0);
    }
  } else {
    sel->selectAnts = FALSE;
    sel->numberAntList = 0;
  }

  /* Selected sources */
  if (ObitInfoListGetP(info, "Sources", &type, (gint32*)dim, (gpointer)&sptr)) {
    sel->numberSourcesList = count;
    /* Count actual entries in source list */
    count = 0;  j = 0;
    for (i=0; i<dim[1]; i++) {
      if ((sptr[j]!=' ') || (sptr[j+1]!=' ')) count++;
      j += dim[0];
    }
    sel->numberSourcesList = count;
    sel->sources = g_realloc(sel->sources, sel->numberSourcesList*sizeof(gint));
    /* have to lookup sources - need SU table for this. */
    iver = 1;
    SUTable = newObitTableSUValue (in->name, (ObitData*)in, &iver, OBIT_IO_ReadOnly, 0, err);
    if (SUTable==NULL) {  /* No source table - only one source and it is selected */
      sel->numberSourcesList = 0;
    } else { /* Lookup sources to get numbers */
      /* Do lookup */
      ObitTableSULookup (SUTable, dim, sptr, sel->sources, &sel->selectSources, err);
      if(err->error)  Obit_traceback_msg (err, routine, in->name);
      SUTable = ObitTableSUUnref(SUTable); /* release table */
    }
    if (err->error) Obit_traceback_msg (err, routine, in->name);
  } else { /* no "Sources" specified */
    sel->numberSourcesList = 0; /* everything selected */
  }

  /* Data selection parameters */
  sel->numberVis   = 1;
  sel->numberPoln  = 1;

} /* end ObitUVGetSelect */

/**
 * Do various operation that have to be done in the ObitUV class
 * rather than the ObitUVCal class.  This mostly involves setups
 * of tables since the ObitUV cannot be visible from the ObitUVCal class.
 * \param in   UV object with UVCal to prepare for calibration
 * \param err  ObitErr for reporting errors.
 */
static void ObitUVSetupCal (ObitUV *in, ObitErr *err)
{
  ObitUVSel *sel = NULL;
  ObitUVCal *cal = NULL;
  glong highVer, iVer, useVer;
  gchar *routine = "ObitUVSetupCal";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitUVIsA(in));
  g_assert (ObitIOIsA(in->myIO));
  g_assert (ObitUVSelIsA(in->mySel));

  /* Need frequency information */
  ObitUVGetFreq (in, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);
  /* Need array information */
  ObitUVGetSubA (in, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Following only needed for ReadCal */
  if (in->myDesc->access != OBIT_IO_ReadCal) return; 

  /* Create/initialize Calibrator if needed */
  if (in->myIO->myCal==NULL) in->myIO->myCal = (gpointer)newObitUVCal(in->name);
  cal = (ObitUVCal*)in->myIO->myCal;

  /* Setup tables as needed for calibration */
  sel = in->myIO->mySel;

  /* Need all AN tables for Polarization calibration and VLBI
  if (sel->doPolCal || sel->doCal) { ALWAYS NEED ? */
    /* How many AN tables (subarrays) */
    highVer = ObitTableListGetHigh (in->tableList, "AIPS AN");
    cal->numANTable = highVer;

    /* allocate */
    if (cal->ANTables) g_free(cal->ANTables);
    cal->ANTables = g_malloc0(highVer*sizeof(Obit*));

    /* Loop over AN tables */
    for (iVer=0; iVer<highVer; iVer++) {

      /* Get table */
      useVer = iVer+1;
      cal->ANTables[iVer] =
	(Obit*)newObitTableANValue (in->name, (ObitData*)in, &useVer, OBIT_IO_ReadOnly, 0, 0, err);
      if (err->error) {
	Obit_log_error(err, OBIT_Error, 
		       "%s: NO Antenna AN table %ld for %s", routine, useVer, in->name);
	return;
      }
    }
 /* }  end AN table setup */
  if (err->error) Obit_traceback_msg (err, routine, in->name);
  
  /* Get any SU table */
  highVer = ObitTableListGetHigh (in->tableList, "AIPS SU");
  useVer = 1;
  if (highVer>=1)
    cal->SUTable =
      (Obit*) newObitTableSUValue (in->name, (ObitData*)in, &useVer, OBIT_IO_ReadOnly, 0, err);
  else cal->SUTable = ObitTableSUUnref(cal->SUTable);
  if (err->error) Obit_traceback_msg (err, routine, in->name);
  
  /* BL table for Baseline dependent calibration */
  if (sel->doBLCal) {
    /* if sel->BLversion ==0 use highest */
    highVer = ObitTableListGetHigh (in->tableList, "AIPS BL");
    if (sel->BLversion==0) useVer = highVer;
    else useVer = sel->BLversion;
    cal->BLTable = 
      (Obit*)newObitTableBLValue (in->name, (ObitData*)in, &useVer, OBIT_IO_ReadOnly, 0, 0, err);
    if (cal->BLTable==NULL) {
      Obit_log_error(err, OBIT_Error, 
		     "%s: NO Baseline BL table %ld for %s", routine, useVer, in->name);
      return;
    }
  } /* end BL table setup */
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* BP table for Bandpass calibration */
  if (sel->doBPCal) {
    /* if sel->BLversion ==0 use highest */
    highVer = ObitTableListGetHigh (in->tableList, "AIPS BP");
    if (sel->BPversion==0) useVer = highVer;
    else useVer = sel->BPversion;
    cal->BPTable = 
      (Obit*)newObitTableBPValue (in->name, (ObitData*)in, &useVer, OBIT_IO_ReadOnly, 0, 0, 0, err);
    if (cal->BPTable==NULL) {
	Obit_log_error(err, OBIT_Error, 
		       "%s: NO Bandpass BP table %ld for %s", routine, useVer, in->name);
	return;
    }
  } /* end BP table setup */
  if (err->error) Obit_traceback_msg (err, routine, in->name);
  
  /* FG table for Flagging */
  if (sel->doFlag) {
    /* if sel->FGversion ==0 use highest */
    highVer = ObitTableListGetHigh (in->tableList, "AIPS FG");
    if (sel->FGversion==0) useVer = highVer;
    else useVer = MIN (sel->FGversion, highVer);

    if (useVer>0) { /* have one - use */
      cal->FGTable = 
	(Obit*)newObitTableFGValue (in->name, (ObitData*)in, &useVer, OBIT_IO_ReadOnly, err);
      if (cal->FGTable==NULL) {
	Obit_log_error(err, OBIT_Error, 
		       "%s: NO Flagging FG table %ld for %s", routine, useVer, in->name);
	return;
      }
    } else {
      /* no flag table - ignore */
      sel->doFlag = FALSE;
      cal->FGTable = ObitTableFGUnref(cal->FGTable);
    }
  } /* end FG table setup */
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Applying amp/phase/delay/rate calibration */
  if (sel->doCal) {

    /* Use CL table if one exists */
    highVer = ObitTableListGetHigh (in->tableList, "AIPS CL");
    if (highVer > 0) {
      /* if calVersion ==0 use highest */
      if (sel->calVersion==0) useVer = highVer;
      else useVer = sel->calVersion;
      cal->CLTable =
	(Obit*) newObitTableCLValue (in->name, (ObitData*)in, &useVer, OBIT_IO_ReadOnly, 0, 0, 0, err);
      if (cal->CLTable==NULL) {
	Obit_log_error(err, OBIT_Error, 
		       "%s: NO calibration CL table %ld for %s", routine, useVer, in->name);
	return;
      }
    } else {

      /* No CL table - Use SN table */
      highVer = ObitTableListGetHigh (in->tableList, "AIPS SN");
      if (sel->calVersion==0) useVer = highVer;
      else useVer = sel->calVersion;
      cal->SNTable =
	(Obit*) newObitTableSNValue (in->name, (ObitData*)in, &useVer, OBIT_IO_ReadOnly, 0, 0, err);
      if (cal->SNTable==NULL) { /* Couldn't open table */
	    Obit_log_error(err, OBIT_Error, 
		"%s: NO calibration SN table %ld for %s", routine, useVer, in->name);
	    return;
      }
    } /* end get cal table */
    if (err->error) Obit_traceback_msg (err, routine, in->name);

    /* Get any CQ table */
    highVer = ObitTableListGetHigh (in->tableList, "AIPS CQ");
    useVer = 1;
    if (highVer>=1)
      cal->CQTable =
	(Obit*) newObitTableCQValue (in->name, (ObitData*)in, &useVer, OBIT_IO_ReadOnly, 0, err);
    else cal->CQTable = ObitTableCQUnref(cal->CQTable);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
    
  } /* end of tables setup for amp/phase/delay/rate calibration */

  /* Initialize indexing of the uv data if an NX table exists */
  highVer = ObitTableListGetHigh (in->tableList, "AIPS NX");
  useVer = 1;
  if (highVer>=1)
    sel->NXTable =
      (Obit*) newObitTableNXValue (in->name, (ObitData*)in, &useVer, OBIT_IO_ReadOnly, err);
  else sel->NXTable = ObitTableNXUnref(sel->NXTable);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  if (highVer>=1)
    ObitUVSelNextInit (sel, (ObitUVDesc*)in->myIO->myDesc, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);
  
  /* Start up calibration - finish output Descriptor, and Selector */
  ObitUVCalStart ((ObitUVCal*)in->myIO->myCal, (ObitUVSel*)in->myIO->mySel, 
		  (ObitUVDesc*)in->myIO->myDesc, in->myDesc, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);
} /* end ObitUVSetupCal */

/**
 * Create myIO object depending on value of FileType in in->info.
 * This is the principle place where the underlying file type is known.
 * \param in   UV object to attach myIO
 * \param err  ObitErr for reporting errors.
 */
static void ObitUVSetupIO (ObitUV *in, ObitErr *err)
{
  ObitIOType FileType;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM];
  gchar *routine = "ObitUVSetupIO";

  /* error checks */
  if (err->error) return;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));

  /* Get FileType */
  if (!ObitInfoListGet(in->info, "FileType", &type, (gint32*)&dim, 
		       (gpointer)&FileType, err)) {
    /* couldn't find it - add message to err and return */
    Obit_log_error(err, OBIT_Error, 
		   "%s: entry FileType not in InfoList Object %s",
		   routine, in->name);
    return;
  }

  /* unlink any existing IO structure */
  in->myIO = ObitUnref (in->myIO);
  if (FileType==OBIT_IO_FITS) {
    in->myIO = (ObitIO*)newObitIOUVFITS(in->name, in->info, err);
    /* copy selector pointer - use same object for UV and IO */
    ((ObitIOUVFITS*)in->myIO)->mySel = ObitUnref(((ObitIOUVFITS*)in->myIO)->mySel);
    ((ObitIOUVFITS*)in->myIO)->mySel = ObitRef(in->mySel);
    /* copy descriptor */
    ((ObitIOUVFITS*)in->myIO)->myDesc = ObitUVDescCopy(in->myDesc, 
		     ((ObitIOUVFITS*)in->myIO)->myDesc, err);

  } else if (FileType==OBIT_IO_AIPS) {
    in->myIO = (ObitIO*)newObitIOUVAIPS(in->name, in->info, err);
    /* copy selector pointer - use same object for UV and IO */
    ((ObitIOUVAIPS*)in->myIO)->mySel = ObitUnref(((ObitIOUVAIPS*)in->myIO)->mySel);
    ((ObitIOUVAIPS*)in->myIO)->mySel = ObitRef(in->mySel);
    /* copy descriptor */
    ((ObitIOUVAIPS*)in->myIO)->myDesc = ObitUVDescCopy(in->myDesc, 
		     ((ObitIOUVAIPS*)in->myIO)->myDesc, err);
  }

} /* end ObitUVSetupIO */

/**
 * Copy standard UV data tables and then selected tables with selection 
 * specified on inUV.
 * Tables with selection:
 * \li FQ
 * The FQ table is always copied selecting by IF and FQID
 * Frequency offsets relative to first selected IF.
 * \li AN
 * All AN Tables are copied with selection by IF and antenna.
 * If polarization cal (doPol) is being applied then polarization
 * calibration information is reset
 * \li SN
 * If no calibration (doCalib) is applied then all SN tables are copied
 * selecting by time, source, antenna, freqID, IF, poln
 * \li CL
 * If no calibration (doCalib) is applied then all CL tables are copied
 * selecting by time, source, antenna, freqID, IF, poln
 *
 * \param inUV  The object to copy from, defines selection
 * \param outUV An existing object pointer for output or NULL if none exists.
 * \param err Error stack, returns if not empty.
 * \return I/O Code  OBIT_IO_OK = OK.
 */
static ObitIOCode CopyTablesSelect (ObitUV *inUV, ObitUV *outUV, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gchar *exclude[]={"AIPS CL","AIPS SN","AIPS FG","AIPS FQ","AIPS SU",
		    "AIPS AN","AIPS CT","AIPS OB","AIPS IM","AIPS MC",
		    "AIPS PC","AIPS NX","AIPS TY","AIPS GC","AIPS CQ",
		    "AIPS WX","AIPS AT",
		    "AIPS HI","AIPS PL","AIPS SL", 
		    NULL};
  gchar *sourceInclude[] = {"AIPS SU", NULL};
  gchar *routine = "ObitUV:CopyTablesSelect";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitUVIsA(inUV));
  g_assert (ObitUVIsA(outUV));

  /* Copy standard tables */
  retCode = ObitUVCopyTables (inUV, outUV, exclude, NULL, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, inUV->name, retCode);

  /* If multisource out then copy SU table, multiple sources selected or
   sources deselected suggest MS out */
  if ((inUV->mySel->numberSourcesList>1) || (!inUV->mySel->selectSources))
  retCode = ObitUVCopyTables (inUV, outUV, NULL, sourceInclude, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, inUV->name, retCode);

  /* Copy FQ Table */
  retCode =  ObitTableFQSelect (inUV, outUV, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, inUV->name, retCode);

  /* Copy AN Tables */
  retCode =  ObitTableANSelect (inUV, outUV, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, inUV->name, retCode);

  /* Copy SN Tables */
  retCode =  ObitTableSNSelect (inUV, outUV, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, inUV->name, retCode);

  /* Copy CL Tables */
  retCode =  ObitTableCLSelect (inUV, outUV, err);
  if ((retCode != OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, inUV->name, retCode);

  return retCode;
} /* end CopyTablesSelect */
