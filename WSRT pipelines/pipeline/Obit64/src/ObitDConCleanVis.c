/* $Id: ObitDConCleanVis.c,v 1.19 2005/08/05 12:13:03 bcotton Exp $  */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2005                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "ObitDConCleanVis.h"
#include "ObitMem.h"
#include "ObitFFT.h"
#include "ObitTableCCUtil.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitDConCleanVis.c
 * ObitDConCleanVis class function definitions.
 * Image based CLEAN class.
 * This class is derived from the ObitDCon class.
 */

/** name of the class defined in this file */
static gchar *myClassName = "ObitDConCleanVis";

/**
 * ClassInfo structure ObitDConCleanVisClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitDConCleanVisClassInfo myClassInfo = {FALSE};

/*--------------- File Global Variables  ----------------*/


/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitDConCleanVisInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitDConCleanVisClear (gpointer in);

/** Private: (re)make residuals. */
static void  MakeResidual (ObitDConCleanVis *in, glong field, 
			   gboolean doBeam, ObitErr *err);

/** Private: (re)make all residuals. */
static void  MakeAllResiduals (ObitDConCleanVis *in, ObitErr *err);

/** Private: Find best residual image. */
static void  WhosBest (ObitDConCleanVis *in, glong *best, glong *second);

/*----------------------Public functions---------------------------*/
/**
 * Constructor.
 * Initializes class if needed on first call.
 * \param name An optional name for the object.
 * \return the new object.
 */
ObitDConCleanVis* newObitDConCleanVis (gchar* name)
{
  ObitDConCleanVis* out;
  /*gchar *routine = "newObitDConCleanVis";*/

  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitDConCleanVisClassInit();

  /* allocate/init structure */
  out = g_malloc0(sizeof(ObitDConCleanVis));

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

  /* set ClassInfo */
  out->ClassInfo = (gpointer)&myClassInfo;

  /* initialize other stuff */
  ObitDConCleanVisInit((gpointer)out);

 return out;
} /* end newObitDConCleanVis */

/**
 * Returns ClassInfo pointer for the class.
 * \return pointer to the class structure.
 */
gconstpointer ObitDConCleanVisGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitDConCleanVisClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitDConCleanVisGetClass */

/**
 * Make a deep copy of an ObitDConCleanVis.
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \param err Obit error stack object.
 * \return pointer to the new object.
 */
ObitDConCleanVis* ObitDConCleanVisCopy  (ObitDConCleanVis *in, 
					     ObitDConCleanVis *out, ObitErr *err)
{
  const ObitClassInfo *ParentClass;
  glong i, nfield;
  gboolean oldExist;
  gchar *outName;
  gchar *routine = "ObitDConCleanVisCopy";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return out;
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));

  /* Create if it doesn't exist */
  oldExist = out!=NULL;
  if (!oldExist) {
    /* derive object name */
    outName = g_strconcat ("Copy: ",in->name,NULL);
    out = newObitDConCleanVis(outName);
    g_free(outName);
  }

  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, out);

  /*  copy this class */
  out->imager   = ObitUVImagerUnref(out->imager);
  out->skyModel = ObitSkyModelUnref(out->skyModel);
  out->display  = ObitDisplayUnref(out->display);
  out->imager   = ObitUVImagerRef(in->imager);
  out->skyModel = ObitSkyModelRef(in->skyModel);
  out->display  = ObitDisplayRef(in->display);

  /* Arrays */
  /* out with the old */
  out->quality = ObitMemFree (out->quality);

  /* In with the new */
  nfield       = in->nfield;
  out->quality = ObitMemAlloc0Name(nfield*sizeof(gfloat),"Clean quality");
  for (i=0; i<nfield; i++) {
    out->quality[i]    = in->quality[i];
  }

  return out;
} /* end ObitDConCleanVisCopy */

/**
 * Make a copy of a object but do not copy the actual data
 * This is useful to create an DConCleanVis similar to the input one.
 * \param in  The object to copy
 * \param out An existing object pointer for output, must be defined.
 * \param err Obit error stack object.
 */
void ObitDConCleanVisClone  (ObitDConCleanVis *in, ObitDConCleanVis *out, ObitErr *err)
{
  const ObitClassInfo *ParentClass;
  glong i, nfield;
  gchar *routine = "ObitDConCleanVisClone";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitIsA(out, &myClassInfo));

  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /*  copy this class */
  out->imager   = ObitUVImagerUnref(out->imager);
  out->skyModel = ObitSkyModelUnref(out->skyModel);
  out->display  = ObitDisplayUnref(out->display);
  out->imager   = ObitUVImagerRef(in->imager);
  out->skyModel = ObitSkyModelRef(in->skyModel);
  out->display  = ObitDisplayRef(in->display);

  /* Arrays */
  /* out with the old */
  out->quality = ObitMemFree (out->quality);

  /* In with the new */
  nfield       = in->nfield;
  out->quality = ObitMemAlloc0Name(nfield*sizeof(gfloat),"Clean quality");
  for (i=0; i<nfield; i++) {
    out->quality[i]    = in->quality[i];
  }
} /* end ObitDConCleanVisClone */

/**
 * Creates an ObitDConCleanVis 
 * defined for convenience of derived classes 
 * \param name   An optional name for the object.
 * \param uvdata from which to create object, should have all control
                 information defined on info member.
 * \param err    Obit error stack object.
 * \return the new object.
 */
ObitDConCleanVis* ObitDConCleanVisCreate (gchar* name, ObitUV *uvdata,  
					  ObitErr *err)
{
  glong nfield, i;
  ObitDConCleanVis* out=NULL;
  gchar *routine = "ObitDConCleanVisCreate";

 /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return out;
  g_assert (ObitUVIsA(uvdata));

  /* Create basic structure */
  out = newObitDConCleanVis (name);

  /* Create UV imager and its ImageMosaic */
  out->imager = ObitUVImagerCreate("UVImager", uvdata, err);
  if (err->error) Obit_traceback_val (err, routine, name, out);

  /* Save uv Mosaic reference */
  out->mosaic = ObitUVImagerGetMosaic(out->imager, err);

  /* Create SkyModel object */
  out->skyModel = ObitSkyModelCreate ("SkyModel", out->mosaic);

   /* Copy control info to SkyModel */
  ObitInfoListCopyData(uvdata->info, out->skyModel->info);
  
  /* Window object */
  out->window = ObitDConCleanWindowCreate ("CleanWindow", out->mosaic, err);
  if (err->error) Obit_traceback_val (err, routine, name, out);

  /* Arrays per field - including those in parent classes */
  nfield =  out->mosaic->numberImages;
  out->nfield  = nfield;
  out->gain    = ObitMemAlloc0Name(nfield*sizeof(gfloat),"Clean Loop gain");
  out->minFlux = ObitMemAlloc0Name(nfield*sizeof(gfloat),"Clean minFlux");
  out->factor  = ObitMemAlloc0Name(nfield*sizeof(gfloat),"Clean factor");
  out->quality = ObitMemAlloc0Name(nfield*sizeof(gfloat),"Clean quality");
  out->maxAbsRes  = ObitMemAlloc0Name(nfield*sizeof(gfloat),"Clean max res");
  out->avgRes  = ObitMemAlloc0Name(nfield*sizeof(gfloat),"Clean avg res");
  for (i=0; i<nfield; i++) {
    out->maxAbsRes[i] = -1.0;
    out->avgRes[i]    = -1.0;
    out->quality[i]   = -1.0;
  }

  return out;
} /* end ObitDConCleanVisCreate */

/**
 * Do deconvolution, uses function on class pointer
 * Does final flatten if FullField member of mosaic member is defined.
 * CLEAN control parameters are in the ObitInfoList member:
 * \li "Niter"   OBIT_int scalar   = Maximum number of CLEAN iterations
 * \li "maxPixel" OBIT_int scalar  = Maximum number of residuals [def 20000]
 * \li "minPatch" OBIT_int scalar  = Minimum beam patch in pixels [def 50]
 * \li "BMAJ"    OBIT_float scalar = Restoring beam major axis (deg)
 * \li "BMIN"    OBIT_float scalar = Restoring beam minor axis (deg)
 * \li "BPA"     OBIT_float scalar = Restoring beam position angle (deg)
 * \li "Beam"    OBIT_float array[3]= (BMAJ, BMIN, BPA) alternate form
 * \li "CCVer"   OBIT_int array    = CLEAN table version for all fields
 * \li "Gain"    OBIT_float array  = CLEAN loop gain per field
 * \li "minFlux" OBIT_float array  = Minimun flux density (Jy)  per field
 * \li "Factor"  OBIT_float array  = CLEAN depth factor per field
 * \li "Plane"   OBIT_int array    = Plane being processed, 1-rel indices of axes 3-?
 * \param in   The object to deconvolve
 * \param err Obit error stack object.
 */
void ObitDConCleanVisDeconvolve (ObitDCon *inn, ObitErr *err)
{
  ObitDConCleanVis *in;
  gboolean done, fin, quit;
  gint jtemp, i;
  gint32 dim[MAXINFOELEMDIM] = {1,1,1,1,1};
  const ObitDConCleanVisClassInfo *inClass;
  gchar *routine = "ObitDConCleanVisDeconvolve";

  /* DEBUG
  gint boom[2], xxcnt = 0; */
 
  /* Cast input to this type */
  in = (ObitDConCleanVis*)inn;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitDConCleanVisIsA(in));

  inClass = (ObitDConCleanVisClassInfo*)in->ClassInfo; /* class structure */

  /* Get parameters */
  inClass->ObitDConGetParms(inn, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Visibility selection and weighting */
  if (in->doWeight) ObitUVImagerWeight (in->imager, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Create Pixel List if needed */
  if (!in->Pixels) {
    in->Pixels = ObitDConCleanPxListCreate("Pixel List", in->mosaic, 
					   in->maxPixel, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
  }

  /* Copy control info to PixelList */
  ObitInfoListCopyData(in->info, in->Pixels->info);

  /* Reset/Init Pixel list */
  ObitDConCleanPxListReset (in->Pixels, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Reset/Init SkyModel */
  ObitSkyModelInitMod(in->skyModel);

  /* Be sure to (re)generate residuals */
  for (i=0; i<in->nfield; i++) in->maxAbsRes[i] = -1.0;

  /* Save actual CC version if not specified */
  if (in->CCver<=0) {
    if (in->Pixels->CCver[0]>0) in->CCver = in->Pixels->CCver[0];
    jtemp = in->CCver;
    ObitInfoListAlwaysPut(in->info, "CCVer", OBIT_int, dim, &jtemp);
  }

  /* Loop until Deconvolution done */
  done = FALSE;
  while (!done) {
    /* Decide which field to do next, tells if finished CLEAN */
    done = ObitDConCleanVisPickNext((ObitDConCleanVis*)in, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);

    ObitErrLog(err);  /* Progress Report */
    if (done) break;

    /* Display/edit windows if enabled */
    if (in->display) {
      quit = ObitDisplayShow (in->display, (Obit*)in->mosaic, in->window, 
			      in->currentField, err);
      if (err->error) Obit_traceback_msg (err, routine, in->name);
      if (quit) {done=TRUE; break;}
    }

    /* Get image/beam statistics needed for this cycle */
    inClass->ObitDConCleanPixelStats((ObitDConClean*)in, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);

    /* Pick components for this major cycle */
    fin = inClass->ObitDConCleanSelect((ObitDConClean*)in, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);

    /* Progress Report */
    ObitErrLog(err);

    /* Update quality list for new max value on field just CLEANed */
    if (fin) in->maxAbsRes[in->currentField-1] = 0.0;
    else in->maxAbsRes[in->currentField-1] = in->Pixels->maxResid;
    in->quality[in->currentField-1] = 
      ObitDConCleanVisQuality((ObitDConCleanVis*)in, in->currentField, err);

    /* Subtract components from visibility data */
    inClass->ObitDConCleanSub((ObitDConClean*)in, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);

  } /* end clean loop */
  ObitErrLog(err);  /* Progress Report */

  /* Make final residuals */
  MakeAllResiduals (in, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);
  ObitErrLog(err);  /* Progress Report */

  /* Final residuals and Restore */
  if (in->doRestore) {

    inClass->ObitDConCleanRestore((ObitDConClean*)in, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
    ObitErrLog(err);  /* Progress Report */
  
    /* Cross Restore if multiple overlapping fields */
    inClass->ObitDConCleanXRestore((ObitDConClean*)in, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
    ObitErrLog(err);  /* Progress Report */
  }

  /* Flatten if needed */
  if (in->doFlatten) {
    inClass->ObitDConCleanFlatten((ObitDConClean*)in, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
    ObitErrLog(err);  /* Progress Report */
    
    /* Display flattened image if enabled */
    if (in->display) 
      ObitDisplayShow (in->display, (Obit*)in->mosaic->FullField, NULL, 
		       1, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
  }

} /* end ObitDConCleanVisDeconvolve */

/**
 * Read any base class parameters and then
 * read CLEAN control parameters from the ObitInfoList member:
 * \li "Mode"      OBIT_int scalar = Model mode (ObitSkyModelMode) [def OBIT_SkyModel_Fastest]
 * \li "doRestore" OBIT_bool       = Restore image when done? [def TRUE]
 * \li "doFlatten" OBIT_bool       = Flatten image when done? [def TRUE]
 * \li "doWeight"  OBIT_bool       = Weight UV data before imaging? [def TRUE]
 *
 * From Parent classes:
 * \li "Niter"   OBIT_int scalar   = Maximum number of CLEAN iterations
 * \li "maxPixel" OBIT_int scalar  = Maximum number of residuals [def 20000]
 * \li "minPatch" OBIT_int scalar  = Minimum beam patch in pixels [def 100]
 * \li "BMAJ"    OBIT_float scalar = Restoring beam major axis (deg)
 * \li "BMIN"    OBIT_float scalar = Restoring beam minor axis (deg)
 * \li "BPA"     OBIT_float scalar = Restoring beam position angle (deg)
 * \li "Beam"    OBIT_float array[3]= (BMAJ, BMIN, BPA) alternate form  (",", deg)
 * \li "CCVer"   OBIT_int array    = CLEAN table version per field
 * \li "Gain"    OBIT_float array  = CLEAN loop gain per field
 * \li "minFlux" OBIT_float array  = Minimun flux density (Jy)  per field
 * \li "Factor"  OBIT_float array  = CLEAN depth factor per field
 * \li "Plane"   OBIT_int array    = Plane being processed, 1-rel indices of axes 3-?
 * \li "autoWindow" OBIT_boolean scalar = True if autoWindow feature wanted.
 * \li "dispURL" OBIT_string scalar = URL of display server
 * \param in  The CLEAN object as base class
 * \param err Obit error stack object.
 */
void  ObitDConCleanVisGetParms (ObitDCon *inn, ObitErr *err)
{
  ObitDConCleanVis *in = (ObitDConCleanVis*)inn;  /* as this class */
  ObitDConClassInfo *ParentClass;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM];
  gint i;
  union ObitInfoListEquiv InfoReal;
  gchar *dispURL=NULL, tname[129];
  gchar *routine = "ObitDConCleanVisGetParms";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

  /* Read any parent class parameters */
  ParentClass = (ObitDConClassInfo*)myClassInfo.ParentClass;
  ParentClass->ObitDConGetParms(inn, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Sky model type */
  InfoReal.itg = (gint)OBIT_SkyModel_Fastest; type = OBIT_int;
  ObitInfoListGetTest(in->info, "Mode", &type, dim, &InfoReal);
  in->modelMode = InfoReal.itg;

  /* Restore image when done? */
  ObitInfoListGetTest(in->info, "doRestore", &type, dim, &in->doRestore);

  /* Flatten image when done? */
  ObitInfoListGetTest(in->info, "doFlatten", &type, dim, &in->doFlatten);

  /* Weight data? */
  ObitInfoListGetTest(in->info, "doWeight", &type, dim, &in->doWeight);

  /* Image display? */
  if (!in->display) {
    ObitInfoListGetP(in->info, "dispURL", &type, dim, (gpointer)&dispURL);
    /* dispURL not NULL terminated */
    if (dispURL) {for (i=0; i<dim[0]; i++) tname[i] = dispURL[i]; tname[i]=0;}
    if (dispURL && (strncmp(tname, "None", 4))) 
      in->display = ObitDisplayCreate("Display", tname, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
  }

} /* end ObitDConCleanVisGetParms */

/**
 * Set default CLEAN windows in mosaic
 * If mosaic member  Radius>0 then make round boxes on Fly's eye field
 * with this radius, else use rectangular box including all but outer 5 pixels
 * On outlier fields, use rectangular box of width OutlierSize.
 * If CLEANBox defined in in->info then its contents are used for field 1.
 * Assumes all images in mosaic have descriptors defined.
 * Uses base class function.
 * \param in   The CLEAN object
 * \param err Obit error stack object.
 */
void ObitDConCleanVisDefWindow(ObitDConClean *inn, ObitErr *err)
{
  ObitDConCleanVis *in;
  const ObitDConCleanVisClassInfo *inClass;
  gchar *routine = "ObitDConCleanVisDefWindow";

  /* Cast input to this type */
  in = (ObitDConCleanVis*)inn;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitDConCleanVisIsA(in));

  inClass = (ObitDConCleanVisClassInfo*)in->ClassInfo; /* class structure */

  /* Call actual function */
  inClass->ObitDConCleanDefWindow((ObitDConClean*)in, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

} /* end ObitDConCleanVisDefWindow */

/**
 * Subtract components from uv data.
 * \param in   The object to deconvolve
 * \param err Obit error stack object.
 */
void ObitDConCleanVisSub(ObitDConCleanVis *in, ObitErr *err)
{
  glong i;
  ObitSkyModelType modelType = OBIT_SkyModel_Comps;
  gint32 dim[MAXINFOELEMDIM];
  gint *itemp, jtemp, nfield;
  gchar *routine = "ObitDConCleanVisSub";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitDConCleanVisIsA(in));

  /* Setup SkyModel parameters */
  dim[0] = dim[1] = dim[2] = dim[3] = dim[4] = 1;
  ObitInfoListAlwaysPut(in->skyModel->info, "Mode", OBIT_int, dim, &in->modelMode);
  ObitInfoListAlwaysPut(in->skyModel->info, "ModelType", OBIT_int, dim, &modelType);
  jtemp = in->CCver;
  ObitInfoListAlwaysPut(in->skyModel->info, "CCVer", OBIT_int, dim, &jtemp);

  nfield = in->mosaic->numberImages;
  itemp = ObitMemAlloc(nfield*sizeof(gint));  /* temp. array */
  dim[0] = nfield;
  for (i=0; i<nfield; i++) itemp[i] = in->skyModel->startComp[i];
  ObitInfoListAlwaysPut(in->skyModel->info, "BComp", OBIT_int, dim, itemp);
  for (i=0; i<nfield; i++) itemp[i] = in->Pixels->iterField[i];
  ObitInfoListAlwaysPut(in->skyModel->info, "EComp", OBIT_int, dim, itemp);
  itemp = ObitMemFree(itemp);  /* Deallocate */

  /* Subtract Current model */
  ObitSkyModelSubUV(in->skyModel, in->imager->uvwork, in->imager->uvwork, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Update CC counts */
  for (i=0; i<in->mosaic->numberImages; i++) {
    in->skyModel->startComp[i] = in->skyModel->endComp[i]+1;
  }

} /* end ObitDConCleanVisSub */

/**
 * Pick next field to clean and create residual image in mosaic member
 * The selected field is in->currentField.
 * Adopted from the AIPSish CLBSTF (QOOP:QCLEAN.FOR)
 * \param in   The object to deconvolve
 * \param err Obit error stack object.
 * \return TRUE iff reached minimum flux density or max. number  comp.
 */
gboolean ObitDConCleanVisPickNext(ObitDConCleanVis *in, ObitErr *err)
{
  glong i, best, second, loopCheck;
  gboolean *fresh, done=TRUE;
  gchar *routine = "ObitDConCleanVisPickNext";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return done;
  g_assert (ObitDConCleanVisIsA(in));

  /* Check if reached max number of components */
  if (in->Pixels->currentIter >= in->Pixels->niter) return done;

  fresh = ObitMemAlloc0(in->nfield*sizeof(gboolean));
  for (i=0; i<in->nfield; i++) fresh[i] = FALSE;

  /* Make sure all fields initialized */
  for (i=0; i<in->nfield; i++) {
    if (in->maxAbsRes[i] < 0.0) {
      /* Make residual image - get statistics */
      MakeResidual(in, i+1, TRUE, err);
      if (err->error) Obit_traceback_val (err, routine, in->name, done);
      fresh[i] = TRUE;
    }
  } /* end loop initializing fields */

  /* Ignore fields already known to be finished, see if all done */
  done = TRUE;
  for (i=0; i<in->nfield; i++) {
    if (in->maxAbsRes[i] <= in->minFlux[i]) in->quality[i] = 0.0;
    else done = FALSE;
  }
  if (done) return done;  /* anything left? */

  /* Find current estimates best and second best field field */
  WhosBest (in, &best, &second);

  /* Verify that it's still best which may require iteration */
  done = FALSE;
  loopCheck = 0;
  while (!done) {
    /* If not just created recalculate */
    if (!fresh[best]) {

      /* Don't loop forever */
      loopCheck++;
      if (loopCheck>2*in->nfield) break;

      /* Make residual image - get statistics */
      MakeResidual(in, best+1, FALSE, err);
      if (err->error) Obit_traceback_val (err, routine, in->name, done);
      fresh[best] = TRUE;
      /* Ignore fields with max residual less than min. */
      if (in->maxAbsRes[best] < in->minFlux[best]) in->quality[best] = 0.0;
    } /* end remake */

    if (in->nfield==1) break; /* If only one, the decision is easy */

    /* Still best? */
    if (in->quality[best] >= in->quality[second]) break;

    /* try again */
    if (in->quality[best]>0.0) 
      Obit_log_error(err, OBIT_InfoWarn, 
		     "%s: field %ld (%g) not as good as second %g",
		     routine, best+1, in->quality[best],  in->quality[second]);

    /* Find current estimates best and second best field field */
    WhosBest (in, &best, &second);

  } /* end loop finding best field */
  
  /* publish decision */
  in->currentField = best + 1;

  /* cleanup */
  fresh = ObitMemFree( fresh);

  /* We're done if best field has maxAbsRes<minFlux */
  done = in->maxAbsRes[best] <= in->minFlux[best];
  return done;
} /* end ObitDConCleanVisPickNext */

/**
 * Determine desirability of field for clean
 * Adopted from the AIPSish CLOFNB (QOOP:QCLEAN.FOR)
 * \param in   The object to deconvolve
 * \param field field number (1-rel) to test
 * \param err Obit error stack object.
 * \return quality measure, higher is more desirable
 */
gfloat ObitDConCleanVisQuality(ObitDConCleanVis *in, glong field, 
			       ObitErr *err)
{
  gfloat out = -1.0;
  gchar *routine = "ObitDConCleanVisQuality";

  /* error checks */
  if (err->error) return out;
  if ((field<=0) || (field>in->nfield)) {
    Obit_log_error(err, OBIT_Error,"%s field %ld out of range 1-%ld in %s",
                   routine, field, in->nfield, in->name);
    return out;
  }
  /* Get value */
  out = 0.95 * in->maxAbsRes[field-1] + 0.05*in->avgRes[field-1];

  return out;
} /* end ObitDConCleanVisQuality */

/**
 * Initialize global ClassInfo Structure.
 */
 void ObitDConCleanVisClassInit (void)
{
  const ObitClassInfo *ParentClass;
  
  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;
  
  /* Initialize (recursively) parent class first */
  ParentClass = ObitDConCleanGetClass();
  ObitDConCleanClassInit();  /* Initialize parent class if needed */
  
  /* function pointers etc. for this class */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = (ObitClassInitFP)ObitDConCleanVisClassInit;
  myClassInfo.newObit       = (newObitFP)newObitDConCleanVis;
  myClassInfo.ObitCopy      = (ObitCopyFP)ObitDConCleanVisCopy;
  myClassInfo.ObitClone     = NULL;
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitDConCleanVisClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitDConCleanVisInit;
  myClassInfo.ObitDConGetParms        = (ObitDConGetParmsFP)ObitDConCleanVisGetParms;
  myClassInfo.ObitDConDeconvolve      = (ObitDConDeconvolveFP)ObitDConCleanVisDeconvolve;
  myClassInfo.ObitDConCleanDefWindow  = (ObitDConCleanDefWindowFP)ObitDConCleanDefWindow;
  myClassInfo.ObitDConCleanPixelStats = (ObitDConCleanPixelStatsFP)ObitDConCleanPixelStats;
  myClassInfo.ObitDConCleanImageStats = (ObitDConCleanImageStatsFP)ObitDConCleanImageStats;
  myClassInfo.ObitDConCleanSelect     = (ObitDConCleanSelectFP)ObitDConCleanSelect;
  myClassInfo.ObitDConCleanSub        = (ObitDConCleanSubFP)ObitDConCleanVisSub;
  myClassInfo.ObitDConCleanRestore    = (ObitDConCleanRestoreFP)ObitDConCleanRestore;
  myClassInfo.ObitDConCleanFlatten    = (ObitDConCleanFlattenFP)ObitDConCleanFlatten;
  myClassInfo.ObitDConCleanXRestore   = (ObitDConCleanXRestoreFP)ObitDConCleanXRestore;
  myClassInfo.ObitDConCleanAutoWindow   = (ObitDConCleanAutoWindowFP)ObitDConCleanAutoWindow;
  myClassInfo.ObitDConCleanVisPickNext  = (ObitDConCleanVisPickNextFP)ObitDConCleanVisPickNext;
  myClassInfo.ObitDConCleanVisQuality   = (ObitDConCleanVisQualityFP)ObitDConCleanVisQuality;
} /* end ObitDConCleanVisClassInit */

/*---------------Private functions--------------------------*/

/**
 * Creates empty member objects, initialize reference count.
 * Parent classes portions are (recursively) initialized first
 * \param inn Pointer to the object to initialize.
 */
void ObitDConCleanVisInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitDConCleanVis *in = inn;

  /* error checks */
  g_assert (in != NULL);

  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->imager   = NULL;
  in->skyModel = NULL;
  in->modelMode = OBIT_SkyModel_Fastest;
  in->doRestore = TRUE;
  in->doFlatten = TRUE;
  in->doWeight  = TRUE;
  in->quality   = NULL;
  in->display   = NULL;
} /* end ObitDConCleanVisInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * \param  inn Pointer to the object to deallocate.
 *           Actually it should be an ObitDConCleanVis* cast to an Obit*.
 */
void ObitDConCleanVisClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitDConCleanVis *in = inn;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* delete this class members */
  in->imager    = ObitUVImagerUnref(in->imager);
  in->skyModel  = ObitSkyModelUnref(in->skyModel);
  in->quality   = ObitMemFree(in->quality);
  in->display   = ObitDisplayUnref(in->display);
 
  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);
  
} /* end ObitDConCleanVisClear */

/**
 * Make selected residual image and get statistics
 * \param in     The Clean object
 * \param field  Which (1-rel) field
 * \param doBeam If TRUE also make beam
 * \param err    Obit error stack object.
 */
static void  MakeResidual (ObitDConCleanVis *in, glong field, 
			   gboolean doBeam, ObitErr *err)
{
  gboolean doWeight, doFlatten;
  const ObitDConCleanVisClassInfo *inClass;
  gchar *routine = "MakeResidual";

  inClass = (ObitDConCleanVisClassInfo*)in->ClassInfo; /* class structure */

  /* Are residuals fresh? */
  doWeight  = FALSE;
  doFlatten = FALSE;

  /* Make residual image */
  ObitUVImagerImage (in->imager, field, doWeight, doBeam, doFlatten, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);
  /* Get statistics */
  inClass->ObitDConCleanImageStats ((ObitDConClean*)in, field, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);
  
  /* Quality measure */
  in->quality[field-1] = ObitDConCleanVisQuality((ObitDConCleanVis*)in, field, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);
} /* end MakeResidual */

/**
 * Make all residual images and get statistics
 * \param in     The Clean object
 * \param err    Obit error stack object.
 */
static void  MakeAllResiduals (ObitDConCleanVis *in, ObitErr *err)
{
  gboolean doBeam= FALSE;
  ObitImage *theBeam=NULL;
  gint i;
  gfloat sumwts;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM] = {1,1,1,1,1};
  gchar *routine = "MakeAllResiduals";

  /* Loop over fields */
  for (i=0; i<in->nfield; i++) {
    /* Need to make beam? */
    theBeam = (ObitImage*)in->imager->mosaic->images[i]->myBeam;
    doBeam = (theBeam==NULL) ||
      !ObitInfoListGetTest(theBeam->info, "SUMWTS", &type, dim, (gpointer)&sumwts);
    /* Make residual image - get statistics */
    MakeResidual(in, i+1, doBeam, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
  } /* end loop over field */
} /* end MakeAllRestestiduals */

/**
 * Find current estimates best and second field
 * \param in     The Clean object
 * \param err    Obit error stack object.
 * \param best   [out] 0-rel best field
 * \param second [out] 0-rel second best field
 */
static void WhosBest (ObitDConCleanVis *in, glong *bbest, glong *ssecond)
{
  gfloat testBest, testSecond;
  glong i, best, second;;
  
  best = second = -1; 
  testBest = testSecond = -1.0e20;
  for (i=0; i<in->nfield; i++) {
    /* First best ?*/
    if (in->quality[i]>testBest) {
      /* Move old one to second place? */
      if (testBest>testSecond) {
	testSecond = testBest;
	second = best;
      }
      testBest = in->quality[i];
      best = i;
    } else if (in->quality[i]>testSecond) {
      testSecond = in->quality[i];
      second = i;
    }
    /* Second Best */
  } /* end loop finding current best */
  
  /* results */
  *bbest   = best;
  *ssecond = second;
} /* end WhosBest */
