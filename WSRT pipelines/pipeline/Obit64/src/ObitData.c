/* $Id: ObitData.c,v 1.10 2005/09/15 13:01:28 bcotton Exp $          */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2005                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "ObitData.h"
#include "ObitSystem.h"
#include "ObitMem.h"
#include "ObitIOHistoryAIPS.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitData.c
 * ObitData class function definitions.
 * This class is derived from the Obit base class.
 */

/** name of the class defined in this file */
static gchar *myClassName = "ObitData";

/*--------------- File Global Variables  ----------------*/
/**
 * ClassInfo structure ObitDataClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitDataClassInfo myClassInfo = {FALSE};

/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitDataInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitDataClear (gpointer in);

/*----------------------Public functions---------------------------*/
/**
 * Virtual Constructor.  THIS SHOULD NEVER BE CALLED
 * Initializes class if needed on first call.
 * \param name An optional name for the object.
 * \return the new object.
 */
ObitData* newObitData (gchar* name)
{
  ObitData* out = NULL;
  gchar *routine = "newObitData";

  g_error("%s: Virtual routine - should not be called",routine);
 return out;
} /* end newObitData */

/**
 * Create a scratch file suitable for accepting the data to be read from in.
 * A scratch Data is more or less the same as a normal Data except that it is
 * automatically deleted on the final unreference.
 * The output will have the underlying files of the same type as in already 
 * allocated.
 * Virtual - calls actual class member
 * \param in  The object to copy
 * \param err Error stack, returns if not empty.
 * \return pointer to the new object.
 */
ObitData* newObitDataScratch (ObitData *in, ObitErr *err)
{
  const ObitDataClassInfo *myClass;

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return NULL;
  g_assert (ObitIsA(in, &myClassInfo));

  myClass = in->ClassInfo;
  return myClass->newObitDataScratch (in, err);

} /* end newObitDataScratch */

/**
 * Returns ClassInfo pointer for the class.
 * \return pointer to the class structure.
 */
gconstpointer ObitDataGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitDataClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitDataGetClass */

/**
 * Test if two ObitDatas have the same underlying structures.
 * This test is done using values entered into the #ObitInfoList
 * in case the object has not yet been opened.
 * \param in1 First object to compare
 * \param in2 Second object to compare
 * \param err ObitErr for reporting errors.
 * \return TRUE if to objects have the same underlying structures
 * else FALSE
 */
gboolean ObitDataSame (ObitData *in1, ObitData *in2, ObitErr *err )
{
  gboolean same = FALSE;
  gchar *routine = "ObitDataSame";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return same;
  g_assert (ObitIsA(in1, &myClassInfo));
  g_assert (ObitIsA(in2, &myClassInfo));

  /* If pointers are the same they must be the same */
  if (in1 == in2) return TRUE;

  /* Get IO if needed */
  if (!in1->myIO) ObitDataSetupIO (in1, err);
  if (err->error) Obit_traceback_val (err, routine, in1->name, FALSE);
  if (!in2->myIO) ObitDataSetupIO (in2, err);
  if (err->error) Obit_traceback_val (err, routine, in2->name, FALSE);

  /* Ask IO */
  same = ObitIOSame(in1->myIO, in1->info, in2->info, err);
  if (err->error) Obit_traceback_val (err, routine, in1->name, FALSE);

  return same;
} /* end ObitDataSame */

/**
 * Delete underlying files and the basic object.
 * Virtual - calls actual class member
 * \param in Pointer to object to be zapped.
 * \param err ObitErr for reporting errors.
 * \return pointer for input object, NULL if deletion successful
 */
ObitData* ObitDataZap (ObitData *in, ObitErr *err)
{
  const ObitDataClassInfo *myClass;

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return NULL;
  g_assert (ObitIsA(in, &myClassInfo));

  myClass = in->ClassInfo;
  return myClass->ObitDataZap (in, err);
} /* end ObitDataZap */

/**
 * Make a deep copy of input object.
 * Copies are made of complex members including disk files; these 
 * will be copied applying whatever selection is associated with the input.
 * Objects should be closed on input and will be closed on output.
 * In order for the disk file structures to be copied, the output file
 * must be sufficiently defined that it can be written.
 * The copy will be attempted but no errors will be logged until
 * both input and output have been successfully opened.
 * If the contents of the data are copied, all associated tables are 
 * copied first.
 * ObitInfoList and ObitThread members are only copied if the output object
 * didn't previously exist.
 * Parent class members are included but any derived class info is ignored.
 * Virtual - calls actual class member
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \param err Error stack, returns if not empty.
 * \return pointer to the new object.
 */
ObitData* ObitDataCopy (ObitData *in, ObitData *out, ObitErr *err)
{
  const ObitDataClassInfo *myClass;

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return NULL;
  g_assert (ObitIsA(in, &myClassInfo));

  myClass = in->ClassInfo;
  return myClass->ObitDataCopy (in, out, err);

} /* end ObitDataCopy */

/**
 * Make a copy of a object but do not copy the actual data
 * This is useful to create a data object similar to the input one.
 * Virtual - calls actual class member
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \param err Error stack, returns if not empty.
 */
void ObitDataClone  (ObitData *in, ObitData *out, ObitErr *err)
{
  const ObitDataClassInfo *myClass;

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

  myClass = in->ClassInfo;
  myClass->ObitDataClone (in, out, err);

} /* end ObitDataClone */

/**
 * Initialize structures and open file.
 * Virtual - calls actual class member
 * \param in Pointer to object to be opened.
 * \param access access (OBIT_IO_ReadOnly,OBIT_IO_ReadWrite,
 *               OBIT_IO_ReadCal or OBIT_IO_WriteOnly).
 *               If OBIT_IO_WriteOnly any existing data in the output file
 *               will be lost.
 * \param err ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitDataOpen (ObitData *in, ObitIOAccess access, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  const ObitDataClassInfo *myClass;

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));

  myClass = in->ClassInfo;
  retCode = myClass->ObitDataOpen (in, access, err);

  /* set Status */
  in->myStatus = OBIT_Active;

  return retCode;
} /* end ObitDataOpen */

/**
 * Shutdown I/O.
 * Virtual - calls actual class member
 * \param in Pointer to object to be closed.
 * \param err ObitErr for reporting errors.
 * \return error code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitDataClose (ObitData *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  const ObitDataClassInfo *myClass;

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));

  myClass = in->ClassInfo;
  retCode = myClass->ObitDataClose (in, err);

  /* set Status */
  in->myStatus = OBIT_Inactive;

  return retCode;
} /* end ObitDataClose */

/**
 * Ensures full instantiation of object - basically open to read/write header
 * and verify or create file.
 * If object has previously been opened, as demonstrated by the existance
 * of its myIO member, this operation is a no-op.
 * Virtual - calls actual class member
 * \param in     Pointer to object
 * \param exist  TRUE if object should previously exist, else FALSE
 * \param err    ObitErr for reporting errors.
 * \return error code, OBIT_IO_OK=> OK
 */
void ObitDataFullInstantiate (ObitData *in, gboolean exist, ObitErr *err)
{
  const ObitDataClassInfo *myClass;

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

  if (in->myIO) return;  /* is this needed */

  myClass = in->ClassInfo;
  myClass->ObitDataFullInstantiate (in, exist, err);
} /* end ObitDataFullInstantiate */

/**
 * Return a ObitTable Object to a specified table associated with
 * the input ObitData.  
 * If such an object exists, a reference to it is returned,
 * else a new object is created and entered in the ObitTableList.
 * \param in       Pointer to object with associated tables.
 *                 This MUST have been opened before this call.
 * \param access   access (OBIT_IO_ReadOnly,OBIT_IO_ReadWrite,
 *                 or OBIT_IO_WriteOnly).
 *                 This is used to determine defaulted version number
 *                 and a different value may be used for the actual 
 *                 Open.
 * \param tabType  The table type (e.g. "AIPS CC").
 * \param tabVer   Desired version number, may be zero in which case
 *                 the highest extant version is returned for read
 *                 and the highest+1 for write.
 * \param err      ObitErr for reporting errors.
 * \return pointer to created ObitTable, NULL on failure.
 */
ObitTable* 
newObitDataTable (ObitData *in, ObitIOAccess access, 
		  gchar *tabType, glong *tabVer, ObitErr *err)
{
  ObitTable *out;
  gboolean doClose;
  gchar *routine = "newObitDataTable";

  /* error check */
  g_assert (ObitErrIsA(err));
  if (err->error) return NULL;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));
  g_assert(tabType!=NULL);
  g_assert(tabVer!=NULL);

  /* the IO object must be present - create if necessary */
  if (in->myIO==NULL) {
    ObitDataSetupIO (in, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, NULL);

    /* add table list reference */
    in->myIO->tableList = (Obit*)ObitUnref(in->myIO->tableList);
    in->myIO->tableList = (Obit*)ObitRef(in->tableList);
  }

  /* details depend on underlying file type,
     pass it down to ObitIO to call relevant routine */
  out = (ObitTable*)newObitIOTable(in->myIO, access, tabType, 
				   tabVer, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, NULL);

  /* Find it? */
  if (out==NULL) {
    Obit_log_error(err, OBIT_InfoErr, "Could not find %s %s table %ld",in->name, 
		       tabType, *tabVer); 
    return out;
  }

  /* set Status unless readonly or not active */
  if (access!=OBIT_IO_ReadOnly) {
    /* Open if not */
    if (in->myStatus==OBIT_Inactive) {
      doClose = TRUE;
      ObitDataOpen(in, OBIT_IO_ReadWrite, err);
    } else doClose = FALSE;
    if (in->myStatus != OBIT_Inactive) in->myStatus = OBIT_Modified;
    if (doClose) ObitDataClose(in, err);
  }
  if (err->error) Obit_traceback_val (err, routine, in->name, NULL);

  /* Add info to table */
  out->tabType = g_strdup(tabType);
  out->tabVer  = *tabVer;

  return out;
} /* end newObitDataTable */

/**
 * Destroy a specified table(s) associated with the input ObitData.  
 * The table is removed from the ObitTableList but in is not updated.
 * A call to ObitDataUpdateTables to update disk structures.
 * \param in       Pointer to object with associated tables.
 * \param tabType  The table type (e.g. "AIPS CC").
 * \param tabVer   Desired version number, may be zero in which case
 *                 the highest extant version is returned for read
 *                 and the highest+1 for write.
 *                 -1 => all versions of tabType
 * \param err      ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitDataZapTable (ObitData *in, gchar *tabType, glong tabVer, 
			     ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitTable *out=NULL;
  glong highVer, ver, iver;
  gchar *routine = "ObitDataZapTable";

  /* error check */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));
  g_assert(tabType!=NULL);

  /* Details depend on underlying file type,
     pass it down to ObitIO to call relevant routine */
  ver = tabVer;
  if (ver<0) ver = 0;
 
  /* Delete table or tables */
  retCode = OBIT_IO_DeleteErr;
  if (tabVer>=0) {             /* fixed version number or highest */
    out = (ObitTable*)newObitIOTable(in->myIO, OBIT_IO_ReadOnly, tabType, 
				     &ver, err);

    /* Does it exist? */
    if (out==NULL) return  OBIT_IO_OK;

    /* Zap */
    out = ObitTableZap (out, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, retCode);
    /* remove from table list */
    ObitTableListRemove (in->tableList, tabType, ver);  
    if (!in->isScratch)  /* No messages if a scratch object */
      /*Obit_log_error(err, OBIT_InfoErr, "Deleted %s %s table %ld",in->name, 
		     tabType, ver); */
    if (in->myStatus!=OBIT_Inactive) in->myStatus = OBIT_Modified;

  } else if (tabVer<0) {              /* all */
    highVer = ObitTableListGetHigh (in->tableList, tabType);
    for (iver = 1; iver<=highVer; iver++) {
      out = (ObitTable*)newObitIOTable(in->myIO, OBIT_IO_ReadWrite, tabType, 
				       &iver, err);
      /* Zap */
      out = ObitTableZap (out, err);
      if (err->error) Obit_traceback_val (err, routine, in->name, retCode);
      /* remove from table list */
      ObitTableListRemove (in->tableList, tabType, iver);  
      if (!in->isScratch)  /* No messages if a scratch object */
	/*Obit_log_error(err, OBIT_InfoErr, "Deleted %s %s table %ld",in->name, 
		       tabType, iver); */
      if (in->myStatus!=OBIT_Inactive) in->myStatus = OBIT_Modified;
    }
  }
  
  retCode = OBIT_IO_OK;
  return retCode;
} /* end ObitDataZapTable */

/**
 * Return a ObitHistory Object to the history associated with
 * the input ObitData.  
 * If such an object exists, a reference to it is returned,
 * else a new object is created and if access=OBIT_IO_WriteOnly
 * entered in the ObitTableList if appropriate (AIPS)
 * \param in       Pointer to object with associated tables.
 *                 This MUST have been opened before this call.
 * \param access   access (OBIT_IO_ReadOnly,OBIT_IO_ReadWrite,
 *                 or OBIT_IO_WriteOnly).
 * \param err      ObitErr for reporting errors.
 * \return pointer to created ObitTable, NULL on failure.
 */
ObitHistory* 
newObitDataHistory (ObitData *in, ObitIOAccess access, ObitErr *err)
{
  ObitHistory *out;
  gboolean doClose;
  glong ver;
  gchar *name;
  ObitIOStatus myStatus;
  ObitIOCode retCode;
  gchar *routine = "newObitDataHistory";

  /* error check */
  g_assert (ObitErrIsA(err));
  if (err->error) return NULL;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));

  /* Create output */
  name = g_strconcat ("History of: ",in->name, NULL);
  out = newObitHistoryValue (name, in->info, err);
  g_free(name);
  if (err->error) Obit_traceback_val (err, routine, in->name, NULL);

  /* If WriteOnly, the IO object must be present - create if necessary */
  myStatus = in->myStatus;  /* May not change */
  if ((access==OBIT_IO_WriteOnly) && (out->myIO==NULL)) {

    /* Open and close to fully instantiate */
    retCode =  ObitHistoryOpen (out, access, err);
    retCode =  ObitHistoryClose (out, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, NULL);
  }

  /* If AIPS and WriteOnly pretend it's a table */
  if ((access==OBIT_IO_WriteOnly) && ObitIOHistoryAIPSIsA(out->myIO)) {
    ver = 1;
    ObitTableListPut(in->tableList, "AIPS HI", &ver, NULL, err);
    if (err->error) Obit_traceback_val (err, routine, in->name, out);
    myStatus = OBIT_Modified;
  }

  /* set Status unless readonly or not active to force disk header update */
  if ((access!=OBIT_IO_ReadOnly) && ( myStatus==OBIT_Modified)) {
    /* Open if not */
    if (in->myStatus==OBIT_Inactive) {
      doClose = TRUE;
      ObitDataOpen(in, OBIT_IO_ReadWrite, err);
    } else doClose = FALSE;
    if (myStatus != OBIT_Inactive) in->myStatus = myStatus;
    if (doClose) ObitDataClose(in, err);
  }
  if (err->error) Obit_traceback_val (err, routine, in->name, NULL);

  return out;
} /* end newObitDataHistory */

/**
 * Copies the associated tables from one ObitData to another.
 * \param in      The ObitData with tables to copy.
 * \param out     An ObitData to copy the tables to, old ones replaced.
 * \param exclude a NULL termimated list of table types NOT to copy.
 *                If NULL, use include
 * \param include a NULL termimated list of table types to copy.
 *                ignored if exclude nonNULL.
 * \param err     ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitDataCopyTables (ObitData *in, ObitData *out, gchar **exclude,
			       gchar **include, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  glong i, j, n, version;
  gchar *tabType;
  ObitTable *intable = NULL, *outtable=NULL;
  gboolean doit;
  gchar *routine = "ObitDataCopyTables";

  /* error check */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitIsA(out, &myClassInfo));

  retCode = OBIT_IO_ReadErr; /* in case of trouble */
  /* Loop through tables on in TableList */
  n = in->tableList->number;
  for (i=1; i<=n; i++) {
    /* Get info */
    ObitTableListGetNumber (in->tableList, i, &tabType, &version, 
			    &intable, err);

    /* are we looking for inclusions or exclusions? */
    if (exclude!=NULL) {
      /* Is this in the exclusion list? */
      doit = TRUE;
      j = 0;
      while (exclude[j]!=NULL) {
	doit = doit && strcmp (tabType, exclude[j]);
	j++;
      }
    } else {
      /* check the inclusion list */
      g_assert(include!=NULL);
      doit = FALSE;
     j = 0;
       while (include[j]!=NULL) {
	doit = doit || (!strcmp (tabType, include[j]));
	j++;
      }
   }

    /* copy if wanted */
    if (doit) {
      /* setup input table if not instantiated */
      if (intable==NULL) {
 	intable = newObitDataTable (in, OBIT_IO_ReadOnly, tabType, 
				  &version, err);
	if (intable==NULL) {
	  Obit_log_error(err, OBIT_Error, 
			 "%s: No %s table found for %s", 
			 routine, tabType, in->name);
	  return retCode;
	}
	if (err ->error)
	  Obit_traceback_val (err, routine, in->name, retCode);
      } /*  end init intable */
   
      /* setup output table */
      outtable = newObitDataTable (out, OBIT_IO_ReadWrite, tabType, 
				  &version, err);
      if (err ->error)
	Obit_traceback_val (err, routine, in->name, retCode);
      
      /* copy */
      outtable = ObitTableCopy (intable, outtable, err);
      if (err ->error)
	Obit_traceback_val (err, routine, in->name, retCode);
      
    } /* end of copy tables section */
      
    /* cleanup */
    if (tabType) g_free(tabType);
    intable  = ObitTableUnref (intable);
    outtable = ObitTableUnref (outtable);    
    if (out->myStatus != OBIT_Inactive) out->myStatus = OBIT_Modified;

  } /* end loop over tables on input */
  
  return OBIT_IO_OK;
} /* end ObitDataCopyTables */

/**
 * Update any disk resident structures about the current tables.
 * \param in   Pointer to object to be updated.
 * \param err ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitDataUpdateTables (ObitData *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gboolean openClose;
  gchar *routine="ObitDataUpdateTables";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));

  /* Need to open and close? */
  openClose = !((in->myStatus==OBIT_Active) || 
		(in->myStatus==OBIT_Modified));

  if (openClose) {
    retCode = ObitIOOpen (in->myIO, OBIT_IO_ReadWrite, in->info, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback on error */
      Obit_traceback_val (err, routine, in->name, retCode);
  }
 
  /* do update */
  retCode = ObitIOUpdateTables(in->myIO, in->info, err);
  if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback on error */
    Obit_traceback_val (err, routine, in->name, retCode);

  if (openClose) {
    retCode = ObitIOClose (in->myIO, err);
    if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback on error */
      Obit_traceback_val (err, routine, in->name, retCode);
  }
 
  return retCode;
} /* end ObitDataUpdateTables */

/**
 * Copies an associated table from one ObitData to another.
 * Any previous data in the output table will be lost.
 * \param in      The ObitData with tables to copy.
 * \param out     An ObitData to copy the tables to, old ones replaced.
 * \param tabType Table type, e.g. "AIPS CC"
 * \param inver   Input table version number, 0=>highest, actual returned
 * \param outver  Output table version number, 0=>new,  actual returned
 * \param err     ObitErr for reporting errors.
 */
void ObitDataCopyTable (ObitData *in, ObitData *out, gchar *tabType, 
			      glong *inver, glong *outver, ObitErr *err)
{
  ObitTable *inTab=NULL, *outTab=NULL;
  gchar *routine="ObitDataCopyTable";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitIsA(out, &myClassInfo));

  /* Get input table */
  inTab = newObitDataTable (in, OBIT_IO_ReadOnly, tabType, inver, err);
  if (err->error) Obit_traceback_msg (err, routine, in->name);

  /* Get output table */
  outTab = newObitDataTable (out, OBIT_IO_WriteOnly, tabType, outver, err);
  if (err->error) {
    inTab = ObitTableUnref(inTab);
    Obit_traceback_msg (err, routine, out->name);
  }

  /* Copy descriptor */
  outTab->myDesc = ObitTableDescCopy (inTab->myDesc, outTab->myDesc, err);
  if (err->error) {
    inTab = ObitTableUnref(inTab);
    outTab = ObitTableUnref(outTab);
    Obit_traceback_msg (err, routine, out->name);
  }

  /* Instantiate */
  ObitTableFullInstantiate (outTab, FALSE, err);
  if (err->error) Obit_traceback_msg (err, routine, out->name);

  /* Truncate output table */
  ObitTableClearRows (outTab, err);
  if (err->error) {
    inTab = ObitTableUnref(inTab);
    outTab = ObitTableUnref(outTab);
    Obit_traceback_msg (err, routine, out->name);
  }

  /* Copy */
  outTab = ObitTableCopy (inTab, outTab, err);
  if (err->error) {
    inTab = ObitTableUnref(inTab);
    outTab = ObitTableUnref(outTab);
    Obit_traceback_msg (err, routine, in->name);
  }

  /* Cleanup */
  inTab = ObitTableUnref(inTab);
  outTab = ObitTableUnref(outTab);
  
  return;
} /* end ObitDataCopyTable */

/**
 * Reposition IO to beginning of file
 * Virtual - calls actual class member
 * \param in   Pointer to object to be rewound.
 * \param err  ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitDataIOSet (ObitData *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  const ObitDataClassInfo *myClass;

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));

  myClass = in->ClassInfo;
  retCode = myClass->ObitDataIOSet (in, err);

  return retCode;
} /* end ObitDataIOSet */

/**
 * Create myIO object depending on value of FileType in in->info.
 * This is the principle place where the underlying file type is known.
 * Virtual - calls actual class member
 * \param in   object to attach myIO
 * \param err  ObitErr for reporting errors.
 */
void ObitDataSetupIO (ObitData *in, ObitErr *err)
{
  const ObitDataClassInfo *myClass;

  /* error checks */
  g_assert(ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

  myClass = in->ClassInfo;
  myClass->ObitDataSetupIO (in, err);
} /* end ObitDataSetupIO */

/**
 * Initialize global ClassInfo Structure.
 */
 void ObitDataClassInit (void)
{
  const ObitClassInfo *ParentClass;

  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;

  /* Initialize (recursively) parent class first */
  ParentClass = ObitGetClass();
  ObitClassInit();  /* Initialize parent class if needed */

  /* function pointers etc. for this class, including parent classes */
  myClassInfo.hasScratch      = TRUE; /* Scratch files allowed */
  myClassInfo.ClassName       = g_strdup(myClassName);
  myClassInfo.ParentClass     = ParentClass;
  myClassInfo.ObitClassInit   = (ObitClassInitFP)ObitDataClassInit;
  myClassInfo.newObit         = (newObitFP)newObitData;
  myClassInfo.newObitDataScratch  = (newObitDataScratchFP)newObitDataScratch;
  myClassInfo.ObitDataSame    = (ObitDataSameFP)ObitDataSame;
  myClassInfo.ObitDataSetupIO = (ObitDataSetupIOFP)ObitDataSetupIO;
  myClassInfo.ObitDataZap     = (ObitDataZapFP)ObitDataZap;
  myClassInfo.ObitCopy        = (ObitCopyFP)ObitCopy;
  myClassInfo.ObitClone       = NULL;  /* Different call */
  myClassInfo.ObitRef         = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref       = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA         = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear       = (ObitClearFP)ObitDataClear;
  myClassInfo.ObitInit        = (ObitInitFP)ObitDataInit;
  myClassInfo.ObitDataCopy    = (ObitDataCopyFP)ObitDataCopy;
  myClassInfo.ObitDataClone   = (ObitDataCloneFP)ObitDataClone;
  myClassInfo.ObitDataOpen    = (ObitDataOpenFP)ObitDataOpen;
  myClassInfo.ObitDataClose   = (ObitDataCloseFP)ObitDataClose;
  myClassInfo.ObitDataIOSet   = (ObitDataIOSetFP)ObitDataIOSet;
  myClassInfo.newObitDataTable= (newObitDataTableFP)newObitDataTable;
  myClassInfo.ObitDataZapTable= (ObitDataZapTableFP)ObitDataZapTable;
  myClassInfo.newObitDataHistory= 
    (newObitDataHistoryFP)newObitDataHistory;
  myClassInfo.ObitDataFullInstantiate= 
    (ObitDataFullInstantiateFP)ObitDataFullInstantiate;
  myClassInfo.ObitDataCopyTables= 
    (ObitDataCopyTablesFP)ObitDataCopyTables;
  myClassInfo.ObitDataUpdateTables= 
    (ObitDataUpdateTablesFP)ObitDataUpdateTables;
  myClassInfo.ObitDataCopyTable= 
    (ObitDataCopyTableFP)ObitDataCopyTable;
} /* end ObitDataClassInit */

/*---------------Private functions--------------------------*/

/**
 * Creates empty member objects, initialize reference count.
 * Parent classes portions are (recursively) initialized first
 * \param inn Pointer to the object to initialize.
 */
void ObitDataInit  (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitData *in = inn;

  /* error checks */
  g_assert (in != NULL);

  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && ( ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->thread    = newObitThread();
  in->info      = newObitInfoList(); 
  in->myIO      = NULL;
  in->tableList = newObitTableList(in->name);
  in->isScratch = FALSE;

} /* end ObitDataInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * For some reason this wasn't build into the GType class.
 * \param  inn Pointer to the object to deallocate.
 *             Actually it should be an ObitData* cast to an Obit*.
 */
void ObitDataClear (gpointer inn)
{
  ObitClassInfo *ParentClass;
  ObitData *in = inn;
  ObitErr *err;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* Delete underlying files if isScratch */
  if (in->isScratch) {
    err = newObitErr();     /* for possible messages */
    /* Remove from ObitSystem list */
    ObitSystemFreeScratch ((Obit*)in, err);
    in->isScratch = FALSE;  /* avoid infinite recursion */
    ObitDataZap (in, err);    /* delete files */
    ObitErrLog(err);
    err = ObitErrUnref(err);
  }

  /* delete this class members */
  in->thread    = ObitThreadUnref(in->thread);
  in->info      = ObitInfoListUnref(in->info);
  in->myIO      = ObitUnref(in->myIO);
  in->tableList = ObitUnref(in->tableList);
  
  /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && ( ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);
  
} /* end ObitDataClear */

