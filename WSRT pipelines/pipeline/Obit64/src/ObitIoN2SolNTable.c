/* $Id: ObitIoN2SolNTable.c,v 1.3 2005/08/03 16:12:07 bcotton Exp $                            */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003,2004                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "ObitIoN2SolNTable.h"
#include "ObitTableANUtil.h"
#include "ObitZernike.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitIoN2SolNTable.c
 * ObitIoN2SolNTable function definitions.
 */

/*---------------Private function prototypes----------------*/
/** Private: Evaluate ionospheric model in direction */
static void 
ObitIoN2SolNTableModel(ObitTableNI *NITable, ObitTableNIRow *NIRow, 
		       ObitAntennaList *Ant, gint numIF, gint numPol, gdouble *freqIF,
		       gdouble ra, gdouble dec, gdouble off[2], 
		       ObitTableSNRow *SNRow, ObitErr *err);

/** Private: Simpleminded uvw calculator */
static void
ObitIoN2SolNTableUVW (const gfloat b[3], gdouble dec, gfloat ha, gfloat uvw[3]);
/*----------------------Public functions---------------------------*/

/**
 * Convert an IoN table into a SolutioN table in a given direction.
 * Output SN table will be associated with inUV.
 * \param inUV     Input uv data. 
 * \param NITable  Input IonTable
 * \param outSN    if nonNULL a pointer to a previously existing Table
 * \param off      RA and Dec offsets(deg) in which NITable to be evaluated.
 * \param err      Error stack, returns if not empty.
 * \return Pointer to the newly created ObitTableSN or outSN if nonNULL
 */
ObitTableSN* ObitIoN2SolNTableConvert (ObitUV *inUV, ObitTableNI *NITable,
				       ObitTableSN *outSN, gdouble off[2], ObitErr *err)
{
  ObitIOCode retCode;
  ObitTableSNRow *SNRow   = NULL;
  ObitTableNIRow *NIRow   = NULL;
  ObitTableAN    *ANTable = NULL;
  ObitAntennaList **antennaLists;
  gint numPol, numIF, numSubA, iif, ipol=0, i;
  glong ver, numNIRow, iSNRow=0, iNIRow, iANver, highANver;
  gchar *tname;
  gboolean bad;
  gfloat  fblank =  ObitMagicF();
  gdouble ra, dec;
  gchar *routine = "ObitIoN2SolNTableConvert";
 
   /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return outSN;
  g_assert (ObitUVIsA(inUV));
  g_assert (ObitTableNIIsA(NITable));

  /* create output table if needed */
  if (inUV->myDesc->jlocs>=0) numPol = MAX (2, inUV->myDesc->inaxes[inUV->myDesc->jlocs]);
  else numPol = 1;
  if (inUV->myDesc->jlocif>=0) numIF  = inUV->myDesc->inaxes[inUV->myDesc->jlocif];
  else numIF = 1;
  if (outSN==NULL) {
    tname = g_strconcat ("Calibration for: ",inUV->name, NULL);
    ver = 0;
    outSN = newObitTableSNValue (tname, (ObitData*)inUV, &ver, OBIT_IO_WriteOnly,  
				 numPol, numIF, err);
    g_free (tname);
    if (err->error) Obit_traceback_val (err, routine, inUV->name, outSN);
  }

  /* Get source position from header (high accuracy not needed) */
  ra  = DG2RAD * inUV->myDesc->crval[inUV->myDesc->jlocr];
  dec = DG2RAD * inUV->myDesc->crval[inUV->myDesc->jlocd];

  /* Open IoN table for read */
  retCode = ObitTableNIOpen (NITable, OBIT_IO_ReadOnly, err);
  if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, outSN->name, outSN);

  /* create row structure */
  NIRow = newObitTableNIRow(NITable);
  
  /* how many rows? */
  numNIRow = NITable->myDesc->nrow;

 /* Open SN table for write */
  retCode = ObitTableSNOpen (outSN, OBIT_IO_WriteOnly, err);
  if ((retCode!=OBIT_IO_OK) || (err->error)) /* add traceback,return */
    Obit_traceback_val (err, routine, outSN->name, outSN);

  /* create row structure */
  SNRow = newObitTableSNRow(outSN);

   /* Attach row to output buffer */
  ObitTableSNSetRow (outSN, SNRow, err);
  if (err->error) Obit_traceback_val (err, routine, outSN->name, outSN);

  outSN->numAnt = 1;
 /* Initialize SN Row structure */
  SNRow->Time   = 0.0;
  SNRow->TimeI  = 0.0;
  SNRow->SourID = 0;
  SNRow->antNo  = 0;
  SNRow->SubA   = 0;
  SNRow->FreqID = 0;
  SNRow->IFR    = 0.0;
  SNRow->NodeNo = 1;
  SNRow->MBDelay1 = 0.0;
  SNRow->MBDelay2 = 0.0;
  SNRow->status = 1;
  for (iif = 0; iif < numIF; iif++) {
    SNRow->Real1[iif]   = 0.0;
    SNRow->Imag1[iif]   = 0.0;
    SNRow->Delay1[iif]  = 0.0;
    SNRow->Rate1[iif]   = 0.0;
    SNRow->Weight1[iif] = 0.0;
    SNRow->RefAnt1[iif] = 1;
    if (ipol>1) { /* Second polarization */
      SNRow->Real2[iif]   = 0.0;
      SNRow->Imag2[iif]   = 0.0;
      SNRow->Delay2[iif]  = 0.0;
      SNRow->Rate2[iif]   = 0.0;
      SNRow->Weight2[iif] = 0.0;
      SNRow->RefAnt2[iif] = 1;
    }
  }
  
  /* Get antenna information */
  /* How many AN tables (subarrays) */
  highANver = ObitTableListGetHigh (inUV->tableList, "AIPS AN");
  numSubA = highANver;

  /* Create AntennaLists */
  antennaLists = g_malloc0(numSubA*sizeof(ObitAntennaList*));

  /* Read Info from AN tables  */
  for (i=0; i<numSubA; i++) {
    iANver = i;
    /* Get table */
    ANTable = newObitTableANValue (inUV->name, (ObitData*)inUV, &iANver, OBIT_IO_ReadOnly, 0, 0, err);
    if ((err->error) || (ANTable==NULL)) 
      Obit_traceback_val (err, routine, inUV->name, outSN);
    
    antennaLists[i] = ObitTableANGetList (ANTable, err);
    if (err->error) Obit_traceback_val (err, routine, inUV->name, outSN);

    /* Maximum antenna number */
    outSN->numAnt = MAX (outSN->numAnt, antennaLists[i]->number);

    /* release table object */
    ANTable = ObitTableANUnref(ANTable);
  } /* End loop over subarrays */

  /* Frequency info */
  if (inUV->myDesc->freqIF==NULL) {
    ObitUVGetFreq (inUV, err);
    if (err->error) Obit_traceback_val (err, routine, inUV->name, outSN);    
  }

  /* Loop over NI table converting */
  for (iNIRow = 0; iNIRow<numNIRow; iNIRow++) {
    /* read NI table */
    retCode = ObitTableNIReadRow (NITable, iNIRow, NIRow, err);
    if (err->error) Obit_traceback_val (err, routine, NITable->name, outSN);

    /* If entry is flagged don't bother */
    if (NIRow->status < 0) continue;

    /* Set antenna invariant values */
    SNRow->Time   = NIRow->Time;
    SNRow->TimeI  = NIRow->TimeI;
    SNRow->SourID = NIRow->SourId;
    SNRow->antNo  = SNRow->antNo;
    SNRow->SubA   = NIRow->SubA;

    /* If bad solution flag */
    bad = NIRow->weight <0.0;
    if (bad) { /* bad solution */
      for (iif = 0; iif < numIF; iif++) {
	SNRow->Real1[iif]   = fblank;
	SNRow->Imag1[iif]   = fblank;
	SNRow->Weight1[iif] = 0.0;
	if (ipol>1) { /* Second polarization */
	  SNRow->Real2[iif]   = fblank;
	  SNRow->Imag2[iif]   = fblank;
	  SNRow->Weight2[iif] = 0.0;
	}
      }

    } else { /* soln OK */
      for (iif = 0; iif < numIF; iif++) {
	SNRow->Weight1[iif] = 1.0;
	if (ipol>1) { /* Second polarization */
	  SNRow->Weight2[iif] = 1.0;
	}
      }
    }

    /* Are values per antenna or global? */
    if (NIRow->antNo>0) { /* one per antenna */
      SNRow->antNo  = NIRow->antNo;

      /* if this soln bad write */
      if (bad) {
	/* Write solution at beginning and end of interval */
	SNRow->Time   = NIRow->Time - 0.49 * NIRow->TimeI;
	retCode = ObitTableSNWriteRow (outSN, iSNRow, SNRow, err);
	SNRow->Time   = NIRow->Time + 0.49 * NIRow->TimeI;
	retCode = ObitTableSNWriteRow (outSN, iSNRow, SNRow, err);
      } else { /* Solution OK, calculate */

	/* convert NI model to SN */
	ObitIoN2SolNTableModel (NITable, NIRow, antennaLists[NIRow->SubA-1], 
				numIF, numPol, inUV->myDesc->freqIF, ra, dec, off, SNRow, err);

	/* write it */
 	retCode = ObitTableSNReadRow (outSN, iSNRow, SNRow, err);
      }

   } else { /* one entry for all antennas */
      /* if this soln bad write */
      if (bad) {
	/* Write solution at beginning and end of interval for each antenna */
	iSNRow = -1;
	for (i=0; i<antennaLists[NIRow->SubA-1]->number; i++) {
	  SNRow->antNo  = antennaLists[NIRow->SubA-1]->ANlist[i]->AntID;
	  SNRow->Time   = NIRow->Time - 0.49 * NIRow->TimeI;
	  retCode = ObitTableSNWriteRow (outSN, iSNRow, SNRow, err);
	  SNRow->Time   = NIRow->Time + 0.49 * NIRow->TimeI;
	  retCode = ObitTableSNWriteRow (outSN, iSNRow, SNRow, err);
	}
      } else { /* soln good - calculate antenna values */
 	for (i=0; i<antennaLists[NIRow->SubA-1]->number; i++) {
	  SNRow->antNo  = antennaLists[NIRow->SubA-1]->ANlist[i]->AntID;
	  SNRow->Time   = NIRow->Time;

 	  /* convert NI model to SN */
	  ObitIoN2SolNTableModel (NITable, NIRow, antennaLists[NIRow->SubA-1], 
				  numIF, numPol, inUV->myDesc->freqIF, ra, dec, off, SNRow, err);

	  /* write it */
	  iSNRow = -1;
	  retCode = ObitTableSNWriteRow (outSN, iSNRow, SNRow, err);
	}
     }
      if (err->error) Obit_traceback_val (err, routine, outSN->name, outSN);
    } /* end of one entry for all antennas */

 } /* end loop over NI Table */

  /* Close tables */
  retCode = ObitTableSNClose (outSN, err);
  if ((retCode!=OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, outSN->name, outSN);
  retCode = ObitTableNIClose (NITable, err);
  if ((retCode!=OBIT_IO_OK) || (err->error))
    Obit_traceback_val (err, routine, outSN->name, outSN);

  /* deallocate Row structures */
  NIRow = ObitTableNIRowUnref(NIRow);
  SNRow = ObitTableSNRowUnref(SNRow);

  return outSN;
} /* end ObitIoN2SolNTableConvert  */

/**
 * Evaluate ionspheric model in offset direction off
 * \param NITable  Input IonTable
 * \param NIRow    IonTable row
 * \param Ant      Antenna list
 * \param ra       RA of field center (rad)
 * \param dec      Dec of field center (rad)
 * \param off      RA and Dec offsets(deg) in which NITable to be evaluated.
 * \param SNRow    Output SN table row, input time and antenna used.
 * \param err      Error stack, returns if not empty.
 * \return Pointer to the newly created ObitTableSN or outSN if nonNULL
 */
static void 
ObitIoN2SolNTableModel(ObitTableNI *NITable, ObitTableNIRow *NIRow, 
		       ObitAntennaList *Ant, gint numIF, gint numPol, gdouble *freqIF,
		       gdouble ra, gdouble dec, gdouble off[2], 
		       ObitTableSNRow *SNRow, ObitErr *err)
{
  /* Speed of light */
#ifndef VELIGHT
#define VELIGHT 2.997924562e8
#endif
  /* CI = 1/speed of light */
#ifndef CI
#define CI 1.0 / VELIGHT
#endif
/* RADSEC = earth rot rate in  rad/sec. */
#ifndef RADSEC
#define RADSEC 3.1415926535897932384 / 43200.0
#endif

  gint   j, itemp, iif;
  gfloat phase, el, chad, shad, ora, odec, cra, cdec, b[3], uvw[3], psize;
  gfloat phsze, dphs, dphs0, zra, zdec, coefac;
  gdouble wavel, cir, x, y, z, delayc, ratec, fqfac, ddec, dra, delay, rate, pdly, dpdly, sind, cosd;
  gdouble ArrLong, ArrLat, AntLst, HrAng, cosdec, sindec, darg;

  cir = CI * RADSEC;

  /* get local hour angle */
  
  ArrLong = Ant->ANlist[SNRow->antNo-1]->AntLong;
  ArrLat  = Ant->ANlist[SNRow->antNo-1]->AntLat;
  AntLst = Ant->GSTIAT0 + ArrLong + NIRow->Time*Ant->RotRate;
  HrAng = AntLst - ra;

  /* Source elevation */
  cosdec = cos (dec);
  sindec = sin (dec);
  darg = sin (ArrLat) * sindec + cos (ArrLat) * cosdec * cos (HrAng);
  el = (1.570796327 - acos (darg));

  /* assume for vla */
  chad = cos (HrAng);
  shad = sin (HrAng);

  /* antenna coordinates: */
  b[0] = Ant->ANlist[SNRow->antNo-1]->AntXYZ[0];
  b[1] = Ant->ANlist[SNRow->antNo-1]->AntXYZ[1];
  b[2] = Ant->ANlist[SNRow->antNo-1]->AntXYZ[2];

  /* project onto normal to source */
  ObitIoN2SolNTableUVW (b, dec, HrAng, uvw);

  /* correct ionospheric puncture  point for the offset of the 
     antenna from the array center. */
  /* size of screen */
  psize = (NITable->heightIon / sin (el)) * sin (DG2RAD * 10.0);

  /* offsets in screen units */
  cra  = 2.0 * uvw[0] / psize;
  cdec = 2.0 * uvw[1] / psize;

  /* delay and rate in sec and  sec/sec. (want corrections). 
     the formulae are written  for the right hand coordinate  system. */
  /* uncorrected delay and rate */
  delay = CI * ( (x * chad - y * shad) * cosdec + z * sindec);
  rate  = cir * (-x * shad - y * chad) * cosdec;

  /* position shift in this direction  */
  dra  =  0.0;
  ddec =  0.0;

  /* evaluate zernike model */
  ora  = (off[0] * 0.1) + cra;
  odec = (off[1] * 0.1) + cdec;
  for (j= 1; j<=NITable->numCoef; j++) { /* loop 100 */
    dra  = dra  + NIRow->coef[j-1]*ObitZernikeGradX(j+1, ora, odec);
    ddec = ddec + NIRow->coef[j-1]*ObitZernikeGradY(j+1, ora, odec);
  } /* end loop  L100: */;

  /* calculate phase directly  */
  ora  = (off[0] * 0.1) + cra;
  odec = (off[1] * 0.1) + cdec;
  zra  = (off[0]) * 0.1;
  zdec = (off[1]) * 0.1;
  dphs = 0.0;
  dphs0 = 0.0;
  for (j= 1; j<=NITable->numCoef; j++) { /* loop 110 */
    dphs  = dphs  + NIRow->coef[j-1]*ObitZernike(j+1, ora, odec);
    dphs0 = dphs0 + NIRow->coef[j-1]*ObitZernike(j+1, zra, zdec);
  } /* end loop  L110: */;

  /* phase delay correction */
  wavel = VELIGHT / (NITable->refFreq);
  coefac = 0.109662 * 0.5 * psize / wavel;
  /* phase in turns */
  phsze = -(dphs-dphs0) * coefac / 2.0*G_PI;

  /* source position error in RA direction at the image plane */
  if (cosdec  !=  0.0) {
    dra = -dra * DG2RAD / cosdec;
  } else {
    dra = 0.0;
  } 

  /* source position error in declination direction */
  ddec = -ddec * DG2RAD ;

  /* correct hour angle */
  chad = cos (HrAng - dra);
  shad = sin (HrAng - dra);

  /* correct declination */
  sind = sin (dec + ddec);
  cosd = cos (dec + ddec);

  /* corrected delay and rate */
  delayc = CI * ( (x * chad - y * shad) * cosd + z * sind);
  ratec  = cir * (-x * shad - y * chad) * cosd;

  /* correction of delay and rate */
  pdly = delayc - delay;
  dpdly = ratec - rate;

  /* set corrections */
  for (iif = 0; iif < numIF; iif++) {
    fqfac = phsze  * (NITable->refFreq/freqIF[iif]);
    itemp = fqfac;
    phase = 2.0*G_PI * (fqfac - itemp);
    SNRow->Real1[iif]   = cos (phase);
    SNRow->Imag1[iif]   = sin (phase);
    SNRow->Delay1[iif]  = -2.0*fqfac * CI;
    SNRow->Rate1[iif]   = 0.0;
    if (numPol>1) { /* Second polarization */
      SNRow->Real2[iif]   = cos (phase);
      SNRow->Imag2[iif]   = sin (phase);
      SNRow->Delay2[iif]  = -2.0*fqfac * CI;
      SNRow->Rate2[iif]   = 0.0;
    }
  }

} /* end ObitIoN2SolNTableModel */

/**
 * UVW for given baseline and geometry
 * \param b   Baseline vector, (VLA Convention)
 * \param dec Source declination (radians)
 * \param ha  Source hour angle (radians)
 * \param uvw U, V, W in same units as B
 */
static void
ObitIoN2SolNTableUVW (const gfloat b[3], gdouble dec, gfloat ha, gfloat uvw[3])
{
  gdouble cosdec, sindec;
  gfloat     sinha, cosha, vw;

  cosdec = cos (dec);
  sindec = sin (dec);
  cosha = cos (ha);
  sinha = sin (ha);
  vw = b[0]*cosha - b[1]*sinha;
  uvw[0] =  b[0]*sinha + b[1]*cosha;
  uvw[1] = -vw*sindec + b[2]*cosdec;
  uvw[2] =  vw*cosdec + b[2]*sindec;
}
/* end ObitIoN2SolNTableUVW */
