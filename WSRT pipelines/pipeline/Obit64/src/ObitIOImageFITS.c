/* $Id: ObitIOImageFITS.c,v 1.18 2005/09/20 20:57:33 bcotton Exp $    */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

#include "Obit.h"
#include "ObitIOImageFITS.h"
#include "ObitImageSel.h"
#include "ObitTableList.h"
#include "ObitFile.h"
#include "ObitFITS.h"
#include "ObitMem.h"

/*----------------Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitIOImageFITS.c
 * ObitIOImageFITS class function definitions.
 * This class is derived from the ObitIO class.
 *
 */

/*--------------- File Global Variables  ----------------*/
/** name of the class defined in this file */
static gchar *myClassName = "ObitIOImageFITS";

/**
 * ClassInfo global structure ObitIOClassInfo.
 * This structure is used by class objects to access class functions.
 */
static ObitIOImageFITSClassInfo myClassInfo = {FALSE};

/*---------------Private function prototypes----------------*/
/** Private: Initialize newly instantiated object. */
void  ObitIOImageFITSInit  (gpointer in);

/** Private: Deallocate members. */
void  ObitIOImageFITSClear (gpointer in);

/** Private: Read AIPS CLEAN parameters. */
void  ObitIOImageAIPSCLEANRead(ObitIOImageFITS *in, gint *status);

/** Private: Write AIPS CLEAN parameters. */
void  ObitIOImageAIPSCLEANWrite (ObitIOImageFITS *in, gint *status);

/** Private: Copy other header keywords. */
void  ObitIOImageKeysOtherRead(ObitIOImageFITS *in, gint *status, 
			       ObitErr *err);

/** Private: Purge HISTORY AIPS keywords. */
static void PurgeAIPSHistory(ObitIOImageFITS *in, gint *status);
/*----------------------Public functions---------------------------*/
/**
 * Basic Constructor.
 * Initializes class on the first call.
 * \param name An optional name for the object.
 * \param info if non-NULL it is used to initialize the new object.
 * \param err  ObitErr for error messages.
 * \return the new object.
 */
ObitIOImageFITS* newObitIOImageFITS (gchar *name, ObitInfoList *info,
				     ObitErr *err)
{
  ObitIOImageFITS* out;
  gint32 dim[IM_MAXDIM];
  ObitInfoType type;
  gchar tempStr[201];
  gchar *routine = "newObitIOImageFITS";

  /* Class initialization if needed */
  if (!myClassInfo.initialized) 
    ObitIOImageFITSClassInit();

  /* allocate structure */
  out = ObitMemAlloc0Name(sizeof(ObitIOImageFITS), "ObitIOImageFITS");

  /* initialize values */
  if (name!=NULL) out->name = g_strdup(name);
  else out->name = g_strdup("Noname");

  /* set classInfo */
  out->ClassInfo = (gpointer)&myClassInfo;
  
  /* initialize other stuff */
  ObitIOImageFITSInit((gpointer)out);

  /* Get any info from info input */
  if (info!=NULL) {
    if(!ObitInfoListGet(info, "Disk", &type, (gint32*)dim, 
			&out->disk, err)) /* add traceback on error */
      Obit_traceback_val (err, routine, name, out);
    
    if(!ObitInfoListGet(info, "FileName", &type, (gint32*)dim, 
			tempStr, err)) /* add traceback on error */
      Obit_traceback_val (err, routine, name, out);
    
    /* form file name for file */
    /* fetch file name from temporary buffer, null terminate. */
    tempStr[dim[0]] = 0;
    if (out->FileName) g_free(out->FileName); /* release old */
    out->FileName = ObitFITSFilename (out->disk, tempStr, err);
    if (err->error) Obit_traceback_val (err, routine, name, out);
  }

  return out;
} /* end newObitIOImageFITS */

/**
 * Returns ClassInfo pointer for the class.
 * Initializes class if needed on first call.
 * \return pointer to the class structure.
 */
gconstpointer ObiIOImageFITSGetClass (void)
{
  /* Class initialization if needed */
  if (!myClassInfo.initialized) ObitIOImageFITSClassInit();

  return (gconstpointer)&myClassInfo;
} /* end ObitIOImageFITSGetClass */

/**
 * Check if underlying files are the same.
 * This test is done using values entered into the #ObitInfoList
 * in case the object has not yet been opened.
 * \param in  ObitIO for test
 * \param in1 ObitInfoList for first object to be tested
 * \param in2 ObitInfoList for second object to be tested
 * \param err ObitErr for reporting errors.
 * \return TRUE if to objects have the same underlying structures
 * else FALSE
 */
gboolean ObitIOImageFITSSame (ObitIO *in, ObitInfoList *in1, 
				ObitInfoList *in2, ObitErr *err)
{
  gint disk1, disk2;
  gchar *filename1, *filename2;
  ObitInfoType type;
  gint32 dim[MAXINFOELEMDIM];
  gboolean same = FALSE;
  gchar *routine = " ObitIOImageFITSSame";

  /* error checks */
  if (err->error) return same;

  /* get file from info */
  if(!ObitInfoListGet(in1, "Disk", &type, dim, &disk1, err))
    Obit_traceback_val (err, routine, in->name, same);

  if (!ObitInfoListGetP(in1, "FileName", &type, (gint32*)&dim, 
		       (gpointer)&filename1)) {
    /* couldn't find it - add message to err and return */
    Obit_log_error(err, OBIT_Error, 
		   "%s: entry FileType not in InfoList Object %s",
		   routine, in->name);
  }

  if(!ObitInfoListGet(in2, "Disk", &type, dim, &disk2, err))
    Obit_traceback_val (err, routine, in->name, same);

  if (!ObitInfoListGetP(in2, "FileName", &type, (gint32*)&dim, 
		       (gpointer)&filename2)) {
    /* couldn't find it - add message to err and return */
    Obit_log_error(err, OBIT_Error, 
		   "%s: entry FileType not in InfoList Object %s",
		   routine, in->name);
  }

  /* Compare */
  same = (disk1==disk2) && 
    !strncmp (filename1,filename2, 200);
 
  return same;
} /* end ObitIOImageFITSSame */

/**
 * Delete underlying files.
 * Delete the whole FITS file.
 * \param in Pointer to object to be zapped.
 * \param err ObitErr for reporting errors.
 */
void ObitIOImageFITSZap (ObitIOImageFITS *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gint status = 0;
  gchar tempStr[201];
  gchar *routine = "ObitIOImageFITSZap";

   /* error check */
  g_assert (ObitErrIsA(err));
  if (err->error) return;
  g_assert (ObitIsA(in, &myClassInfo));

  /* Close if still open */
  if ((in->myStatus==OBIT_Modified) || (in->myStatus==OBIT_Active)) {
    retCode = ObitIOImageFITSClose (in, err);
    if (err->error) Obit_traceback_msg (err, routine, in->name);
  }

  /* Destroy using cfitsio */
  if (in->FileName[0]=='!') strncpy (tempStr, (gchar*)&in->FileName[1], 200);
  else strncpy (tempStr, in->FileName, 200);
  fits_open_file(&(in->myFptr), tempStr, READWRITE, &status);
  fits_delete_file(in->myFptr, &status);
  if (status!=0) {
    Obit_log_error(err, OBIT_Error, 
		   "ERROR %d deleting FITS file %s", 
		   status, in->FileName);
    Obit_cfitsio_error(err); /* copy cfitsio error stack */
  }

  /* FITS tables are in the same file - delete table list */
  in->tableList = ObitTableListUnref(in->tableList);

  return;
} /* end ObitIOImageFITSZap */

/**
 * Make a copy of a object.
 * The result will have pointers to the more complex members.
 * Parent class members are included but any derived class info is ignored.
 * \param in  The object to copy
 * \param out An existing object pointer for output or NULL if none exists.
 * \param err Obit error stack object.
 * \return pointer to the new object.
 */
ObitIOImageFITS* ObitIOImageFITSCopy  (ObitIOImageFITS *in, 
				       ObitIOImageFITS *out, ObitErr *err)
{
  const ObitIOClassInfo *ParentClass;
  gboolean oldExist;
  gchar *outName;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return out;
  g_assert (ObitIsA(in, &myClassInfo));
  if (out) g_assert (ObitIsA(out, &myClassInfo));

  /* Create if it doesn't exist */
  oldExist = out!=NULL;
  if (!oldExist) {
    /* derive object name */
    outName = g_strconcat ("Copy: ",in->name,NULL);
    out = newObitIOImageFITS(outName, NULL, err);
    g_free(outName);
  }

  /* deep copy any base class members */
  ParentClass = myClassInfo.ParentClass;
  g_assert ((ParentClass!=NULL) && (ParentClass->ObitCopy!=NULL));
  ParentClass->ObitCopy (in, out, err);

  /* copy this class */
  if (out->FileName!=NULL) g_free(out->FileName);
  out->FileName = g_strdup(in->FileName);

  return out;
} /* end ObitIOImageFITSCopy */

/**
 * Initialize structures and open file.
 * The file etc. info should have been stored in the ObitInfoList.
 * The image descriptor is read if ReadOnly or ReadOnly and
 * written to disk if opened WriteOnly.
 * For accessing FITS files the following entries in the ObitInfoList 
 * are used:
 * \li "Disk"     OBIT_int (1,1,1) FITS "disk" number.
 * \li "FileName" OBIT_string (?,1,1) FITS file name.
 * \param in Pointer to object to be opened.
 * \param in Pointer to object to be opened.
 * \param access access (OBIT_IO_ReadOnly,OBIT_IO_ReadWrite)
 * \param info ObitInfoList with instructions for opening
 * \param err ObitErr for reporting errors.
 * \return return code, 0=> OK
 */
ObitIOCode ObitIOImageFITSOpen (ObitIOImageFITS *in, ObitIOAccess access, 
				ObitInfoList *info, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gint i, status = 0, simple, extend;
  gint32 dim[IM_MAXDIM];
  glong pcount, gcount;
  ObitInfoType type;
  gchar tempStr[201];
  ObitImageDesc* desc;
  ObitImageSel* sel;
  gchar *routine = "ObitIOImageFITSOpen";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (ObitInfoListIsA (info));
  g_assert (in->myDesc != NULL);
  g_assert (in->mySel != NULL);

  desc = in->myDesc; /* descriptor pointer */
  sel  = in->mySel;  /* selector pointer */


  /* get instructions from info */
  if(!ObitInfoListGet(info, "Disk", &type, (gint32*)dim, 
		      &in->disk, err)) /* add traceback on error */
    Obit_traceback_val (err, routine, in->name, retCode);
 
  /* set defaults */
  desc->IOsize = OBIT_IO_byRow;
  ObitInfoListGetTest(info, "IOBy", &type, (gint32*)dim, 
		      &desc->IOsize);
  if(!ObitInfoListGet(info, "FileName", &type, (gint32*)dim, 
		      tempStr, err)) /* add traceback on error */
    Obit_traceback_val (err, routine, in->name, retCode);

  /* form file name for file */
  /* fetch file name from temporary buffer, null terminate. */
  tempStr[dim[0]] = 0;
  if (in->FileName) g_free(in->FileName); /* release old */
  in->FileName = ObitFITSFilename (in->disk, tempStr, err);
  if (err->error) Obit_traceback_val (err, routine, in->FileName, retCode);
  
  /* open file by access type */
  /*------------------------ Read Only ---------------------------------*/
  if (access == OBIT_IO_ReadOnly) {
    /* must strip any leading "!" for read/write */
    if (in->FileName[0]=='!') strncpy (tempStr, (gchar*)&in->FileName[1], 200);
    else strncpy (tempStr, in->FileName, 200);

    /* cfitsio refuses to open a file readwrite after it has been opened
       readonly so first try opening readwrite even if requested ReadOnly.
       If that fails try readonly. */
    /* Test open readwrite */
    fits_open_file(&(in->myFptr), tempStr, READWRITE, &status);
    if ((status==FILE_NOT_OPENED) || (status==READONLY_FILE)) { 
      /* Failed - try readonly */
      status = 0;
      fits_clear_errmsg();   /* Clear cfitsio error stack */
      if (fits_open_file(&(in->myFptr), tempStr, READONLY, &status) ) {
	Obit_log_error(err, OBIT_Error, 
		       "ERROR %d opening input FITS file %s", status, in->FileName);
	Obit_cfitsio_error(err); /* copy cfitsio error stack */
	
	retCode = OBIT_IO_OpenErr;
	return retCode;
      }
    }

    if (fits_read_imghdr (in->myFptr, IM_MAXDIM, &simple, &desc->bitpix, 
			  &desc->naxis, desc->inaxes,
			  &pcount, &gcount, &extend, &status)) {
      Obit_log_error(err, OBIT_Error, 
		     "ERROR reading input required keywords in FITS file %s", 
		     in->FileName);
      Obit_cfitsio_error(err); /* copy cfitsio error stack */
      retCode = OBIT_IO_OpenErr;
      return retCode;
    }

  /*------------------------ Read/Write ---------------------------------*/
  } else if (access == OBIT_IO_ReadWrite) {
    /* Initialize output file */
    /* must strip any leading "!" for read/write */
    if (in->FileName[0]=='!') strncpy (tempStr, (gchar*)&in->FileName[1], 200);
    else strncpy (tempStr, in->FileName, 200);
    if ( fits_open_file(&(in->myFptr), tempStr, READWRITE, &status) ) {
      Obit_log_error(err, OBIT_Error, "ERROR opening output FITS file %s", 
		     in->FileName);
      Obit_cfitsio_error(err); /* copy cfitsio error stack */
      retCode = OBIT_IO_OpenErr;
      return retCode;
    }
    /* get header required keywords */
    if (fits_read_imghdr (in->myFptr, IM_MAXDIM, &simple, &desc->bitpix, 
			  &desc->naxis, desc->inaxes,
			  &pcount, &gcount, &extend, &status)) {
      Obit_log_error(err, OBIT_Error, 
		 "ERROR readinging input required keywords in FITS file %s", 
		  in->FileName);
      Obit_cfitsio_error(err); /* copy cfitsio error stack */
      retCode = OBIT_IO_OpenErr;
      return retCode;
    }

  /*------------------------ Write Only ---------------------------------*/
  } else if (access == OBIT_IO_WriteOnly) {
    /* Initialize output file */
    /* Output file may already exist test open */
    /* must strip any leading "!" for read/write */
    if (in->FileName[0]=='!') strncpy (tempStr, (gchar*)&in->FileName[1], 200);
    else strncpy (tempStr, in->FileName, 200);

    /* CURSE YOU CFITSIO!!! */
    /* Open read/write to see if it's there */
    fits_open_file(&(in->myFptr), tempStr, READWRITE, &status);
    if (status==0) { /* IF OK force required keywords */
      if (fits_resize_img (in->myFptr, desc->bitpix, desc->naxis, 
			   desc->inaxes, &status)) {
 	Obit_log_error(err, OBIT_Error, 
		       "ERROR resizing output FITS file %s", in->FileName);
	Obit_cfitsio_error(err); /* copy cfitsio error stack */
	retCode = OBIT_IO_OpenErr;
	return retCode;
     }
    }

    /* If it doesn't exist then create */
    if ((status==FILE_NOT_OPENED) || (status==END_OF_FILE)) {
      /* If the file exists but has zero size then use name with '!' */
      if (in->FileName[0]!='!') { /* Add a '!' */
	tempStr[0]='!';
	strncpy (tempStr+1, in->FileName, 200);
      } else {
	strncpy (tempStr, in->FileName, 200);
      }

      status = 0;
      fits_clear_errmsg();   /* Clear error stack */
      if (fits_create_file(&(in->myFptr), tempStr, &status)) {
	Obit_log_error(err, OBIT_Error, 
		       "ERROR opening output FITS file %s", in->FileName);
	Obit_cfitsio_error(err); /* copy cfitsio error stack */
	retCode = OBIT_IO_OpenErr;
	return retCode;
	
      } else if (status!=0) { /* error */
	Obit_log_error(err, OBIT_Error, 
		       "ERROR opening output FITS file %s", in->FileName);
	Obit_cfitsio_error(err); /* copy cfitsio error stack */
	retCode = OBIT_IO_OpenErr;
	return retCode;
      }
      
      /* create image */
      if (fits_create_img (in->myFptr, desc->bitpix, desc->naxis, 
			   desc->inaxes, &status) ) {
	Obit_log_error(err, OBIT_Error, 
		       "ERROR opening output FITS image %s", in->FileName);
	Obit_cfitsio_error(err); /* copy cfitsio error stack */
	retCode = OBIT_IO_OpenErr;
	return retCode;
      }
    } else if (status!=0) { /* other errors */
      Obit_log_error(err, OBIT_Error, 
		     "ERROR opening output FITS file %s", in->FileName);
      Obit_cfitsio_error(err); /* copy cfitsio error stack */
      retCode = OBIT_IO_OpenErr;
      return retCode;
    } /* End create file/image */

    /* If it got all the way to here and has an '!' at the start of the 
       file name then remove it as it may cause trouble later */
    if(!ObitInfoListGet(info, "FileName", &type, (gint32*)dim, tempStr, err))
      Obit_traceback_val (err, routine, in->name, retCode);
    tempStr[dim[0]] = 0;  /* null terminate */
    if (tempStr[0]=='!') {
      for (i=1; i<dim[0]; i++) tempStr[i-1] = tempStr[i];
      tempStr[i-1] = 0;
    }
    dim[0] = strlen(tempStr); dim[1] = 1;
    ObitInfoListAlwaysPut(info, "FileName", OBIT_string, dim, tempStr);

  } else {
    /* should never get here */
    g_assert_not_reached(); 
  }

  /* save information */
  in->access = access;
  in->myStatus = OBIT_Active;
  desc->areBlanks = FALSE;
  /* ???  desc->maxval = -1.0e20;*/
  /* ???  desc->minval =  1.0e20;*/

  /* initialize location in image */
  desc->row   = 0;
  desc->plane = 0;

  retCode = OBIT_IO_OK;
  return retCode;
} /* end ObitIOImageFITSOpen */

/**
 * Shutdown I/O.
 * \param in Pointer to object to be closed.
 * \param err ObitErr for reporting errors.
 * \return error code, 0=> OK
 */
ObitIOCode ObitIOImageFITSClose (ObitIOImageFITS *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gint status = 0;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));
  /* don't bother if it's not open */
  if ((in->myStatus!=OBIT_Modified) && (in->myStatus!=OBIT_Active)) 
    return OBIT_IO_OK;

  fits_close_file (in->myFptr, &status);
  if (status !=0) {
    Obit_log_error(err, OBIT_Error, "ERROR closing FITS file");
    Obit_cfitsio_error(err); /* copy cfitsio error stack */
    retCode = OBIT_IO_CloseErr;
    return retCode;
  }

  in->myStatus = OBIT_Inactive;
  retCode = OBIT_IO_OK;
  return retCode;
} /* end ObitIOImageFITSClose */

/**
 * initialize I/O - position to beginning of image.
 * \param in Pointer to object to be accessed.
 * \param info ObitInfoList with instructions
 * \param err ObitErr for reporting errors.
 * \return return code, 0=> OK
 */
ObitIOCode ObitIOImageFITSSet (ObitIOImageFITS *in, ObitInfoList *info, 
			       ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_OK;
  gint hdutype, status = 0;

  /* Position to HDU 1 */
  fits_movabs_hdu (in->myFptr, 1, &hdutype, &status);
  if ((status !=0) || (hdutype!=IMAGE_HDU)) {
    Obit_log_error(err, OBIT_Error, "ERROR positioning FITS file");
    Obit_cfitsio_error(err); /* copy cfitsio error stack */
    if ((in->access==OBIT_IO_ReadOnly) ||
	(in->access==OBIT_IO_ReadWrite)) retCode = OBIT_IO_ReadErr;
    else  retCode = OBIT_IO_WriteErr;
    return retCode;
  }

  /* Reset values in descriptor */
  ((ObitImageDesc*)in->myDesc)->plane = 0;
  ((ObitImageDesc*)in->myDesc)->row   = 0;

  return retCode;
} /* end ObitIOImageFITSSet */

/**
 * Read image data from disk.
 * Reads row in->myDesc->row + 1; plane in->myDesc->plane + 1
 * When OBIT_IO_EOF is returned all data has been read (then is no new
 * data in data) and the I/O has been closed.
 * \param in Pointer to object to be read.
 * \param data pointer to buffer to write results.
 * \param err ObitErr for reporting errors.
 * \return return code, 0(OBIT_IO_OK)=> OK,
 *          OBIT_IO_EOF => image finished.
 */
ObitIOCode ObitIOImageFITSRead (ObitIOImageFITS *in, gfloat *data, 
				ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  glong incs[IM_MAXDIM]={1,1,1,1,1,1,1}, bblc[IM_MAXDIM], ttrc[IM_MAXDIM];
  glong row, plane;
  gint group=0, i, anyf, status = 0;
  ObitImageDesc* desc;
  ObitImageSel* sel;
  gfloat fblank = ObitMagicF();

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (data != NULL);
  g_assert (in->myDesc != NULL);

  Obit_retval_if_fail (((in->myStatus==OBIT_Active) || (in->myStatus==OBIT_Modified)) , 
		       err, retCode,
		       "Cannot read, I/O not currently active");
  
  desc = in->myDesc; /* descriptor pointer */
  sel  = in->mySel;  /* selector pointer */

  row   = MAX (desc->row, sel->blc[1]-1);
  plane = MAX (desc->plane, sel->blc[2]-1);
  /* set current request by desc->IOsize */
  if (desc->IOsize==OBIT_IO_byRow) {

    plane = MAX (1, plane);
   /* increment row */
    row++;
    if (row>sel->trc[1]) { /* next plane */
      row = sel->blc[1];
      plane++;
    }

    /* Set window */
    bblc[0] = sel->blc[0];
    bblc[1] = row;
    ttrc[0] = sel->trc[0];
    ttrc[1] = row;
  } else if (desc->IOsize==OBIT_IO_byPlane) {
    row = sel->blc[1];
    /* increment plane */
    plane++;
    /* Set window */
    bblc[0] = sel->blc[0];
    bblc[1] = sel->blc[1];
    ttrc[0] = sel->trc[0];
    ttrc[1] = sel->trc[1];
  }
  desc->row   = row;
  desc->plane = plane;

  /* check if done - starting on the plane past the highest. */
  if (plane > sel->trc[2]) {
    /* ObitIOImageFITSClose (in, err); Close */
    return OBIT_IO_EOF;
  }

  /* set plane */
  bblc[2] = plane;
  ttrc[2] = plane;
  for (i=3;i<desc->naxis;i++) {ttrc[i] = 1; bblc[i] = 1;}

  /*  Read selected portion of input */
  if (fits_read_subset_flt (in->myFptr, group, desc->naxis, 
			    desc->inaxes, 
			    bblc, ttrc, incs, fblank, data, 
			    &anyf, &status)) {
    Obit_log_error(err, OBIT_Error, 
		   "ERROR reading input FITS file %s plane %ld row %ld", 
		   in->FileName, plane, row);
    Obit_cfitsio_error(err); /* copy cfitsio error stack */
    retCode = OBIT_IO_ReadErr;
    return retCode;
  }

  /* keep track of blanking */
  desc->areBlanks = desc->areBlanks || anyf;

  retCode = OBIT_IO_OK;
  return retCode;
} /* end ObitIOImageFITSRead */

/**
 * Write information to disk.
 * Writes row in->myDesc->row + 1; plane in->myDesc->plane + 1
 * Writing partial images is only supported in row at a time mode.
 * When OBIT_IO_EOF is returned the image has been written,
 * data in data is ignored and the I/O is closed.
 * \param in Pointer to object to be written.
 * \param data pointer to buffer containing input data.
 * \param err ObitErr for reporting errors.
 * \return return code, 0(OBIT_IO_OK)=> OK
 *          OBIT_IO_EOF => image finished.
 */
ObitIOCode ObitIOImageFITSWrite (ObitIOImageFITS *in, gfloat *data, 
				 ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  ObitImageDesc* desc;
  ObitImageSel* sel;
  glong size, fpixel[IM_MAXDIM]={1,1,1,1,1,1,1};
  glong i, offset, len, iRow, nRows, row, plane;
  gint status = 0;
  gfloat val, fblank = ObitMagicF();
  gboolean windowed;
  gchar *routine = "ObitIOImageFITSWrite";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (in->myDesc != NULL);
  g_assert (data != NULL);
  /* is I/O active */
  Obit_retval_if_fail(((in->myStatus==OBIT_Modified) ||
		       (in->myStatus==OBIT_Active)), 
		      err, retCode, 
		      "Cannot write, I/O not currently active");

  desc = in->myDesc; /* descriptor pointer */
  sel  = in->mySel;  /* selector pointer */

  /* If windowing, writes of a plane at a time have to be done by row */
  windowed = (sel->blc[0]>1) || (sel->blc[2]>1) ||
    (sel->trc[0]!=desc->inaxes[0]) || 
    (sel->trc[1]!=desc->inaxes[1]);

  /* set cfitsio request parameters */
  row = MAX (desc->row, sel->blc[1]-1);
  plane = MAX (desc->plane, sel->blc[2]-1);

  /* set current request by desc->IOsize */
  if (desc->IOsize==OBIT_IO_byRow) {
    plane = MAX (1, plane);
    row++; /* increment row */
    nRows = 1;
    if (row>sel->trc[1]) { /* next plane */
      row = sel->blc[1];
      plane++;
    }

    len = sel->trc[0] - sel->blc[0]+1;   /* size of a transfer (row) */

  } else if (desc->IOsize==OBIT_IO_byPlane) {

    /* increment plane */
    plane++;
    row = sel->blc[1]; /* set row */

     if (windowed) {
      nRows = sel->trc[1] - sel->blc[1] + 1;
      len = sel->trc[0] - sel->blc[0]+1;   /* size of a transfer (row) */
    } else {   /* all at once */
      nRows = 1;
      len = desc->inaxes[0] * desc->inaxes[1]; /* whole plane */
    }
   }

  /* Set first pixel, size */
  fpixel[0] = sel->blc[0];
  fpixel[1] = row;
  fpixel[2] = plane;

  desc->row   = row;
  desc->plane = plane;

   /* check if done - starting on the plane past the highest. */
  if (plane > sel->trc[2]) {
    /* ObitIOImageFITSClose (in, err); Close */
    Obit_log_error(err, OBIT_Error, 
		   "%s: Attempt to write past end of %s", 
		   routine, in->FileName);
    return OBIT_IO_EOF;
  }

  size = len;           /* transfer size in floats */

  offset = 0; /* offset in output buffer */

  /* write file one row/plane at a time - loop for windowed planes */
  for (iRow=0; iRow<nRows; iRow++) {
    /* write image data */
    if (fits_write_pixnull (in->myFptr, TFLOAT, fpixel, size,
			    (void*)&data[offset], (void*)&fblank, &status)) {
      Obit_log_error(err, OBIT_Error, 
		     "ERROR writing output FITS file %s plane %ld row %ld", 
		     in->FileName, plane, row);
      Obit_cfitsio_error(err); /* copy cfitsio error stack */
      return OBIT_IO_ReadErr;
    }

    /* keep track on max/min/blanking */
    for (i=0; i<size; i++) {
      val = data[offset+i];
      if (val==fblank) {
	desc->areBlanks = TRUE;
      } else { /* OK */
	desc->maxval = MAX (desc->maxval, val);
	desc->minval = MIN (desc->minval, val);
      }
    }

    row ++;            /* next row */
    fpixel[1] = row;
    offset   += len;   /* offset in data buffer */
  } /* end loop writing */

  in->myStatus = OBIT_Modified;
  retCode = OBIT_IO_OK;
  return retCode;
} /* end ObitIOImageFITSWrite */

/**
 * Read image Descriptor data from disk.
 * \param in Pointer to object with ObitImageDesc to be read.
 * \param err ObitErr for reporting errors.
 * \return return code, 0=> OK
 */
ObitIOCode ObitIOImageFITSReadDescriptor (ObitIOImageFITS *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gchar commnt[FLEN_COMMENT];
  gchar cdata[IM_MAXDIM][FLEN_CARD], sdata[FLEN_CARD], *cdum[IM_MAXDIM];
  gint i, nfound, nhdu, hdutype, status = 0;
  glong extver, temp;
  ObitImageDesc* desc;
  ObitImageSel* sel;
  ObitTableList* tableList;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (in->myDesc != NULL);

  desc = in->myDesc; /* descriptor pointer */
  sel  = in->mySel;  /* selector pointer */
  tableList = (ObitTableList*)in->tableList;

  /* Index tables in file and update TableList if not already done*/
  if (tableList->number <= 0) {
    fits_get_num_hdus (in->myFptr, &nhdu, &status);
    for (i=1; i<=nhdu; i++) {
      fits_movabs_hdu (in->myFptr, i, &hdutype, &status);
      if (hdutype==BINARY_TBL) { /* If it's a table enter it in the list */
	/* table name */
	fits_read_key_str (in->myFptr, "EXTNAME", sdata, commnt, &status);
	/* version number */
	fits_read_key_lng (in->myFptr, "EXTVER", &extver, commnt, &status);
	if (status==0) { /* Add to TableList */
	  ObitTableListPut (tableList, sdata, &extver, NULL, err);
	  if (err->error)
	    Obit_traceback_val (err, "ObitIOImageFITSReadDescriptor", 
				tableList->name, OBIT_IO_OpenErr);
	}
      }
    } /* end loop indexing file */
  } /* end update Table List */

  /* Position to HDU 1, the image 1 */
  fits_movabs_hdu (in->myFptr, 1, &hdutype, &status);

  /* Read keyword values, use default where possible */
  fits_read_key_flt (in->myFptr, "DATAMAX", &desc->maxval, commnt, &status);
  if (status==KEY_NO_EXIST) status = 0; 

  fits_read_key_flt (in->myFptr, "DATAMIN", &desc->minval, commnt, &status);
  if (status==KEY_NO_EXIST) status = 0;

  strncpy (desc->teles, "        ", 9); 
  fits_read_key_str (in->myFptr, "TELESCOP", cdata[0], commnt, &status);
  if (status==0) strncpy (desc->teles, cdata[0], IMLEN_VALUE);
  if (status==KEY_NO_EXIST) status = 0;

  strncpy (desc->instrument, "        ", 9); 
  fits_read_key_str (in->myFptr, "INSTRUME", cdata[0], commnt, &status);
  if (status==0) strncpy (desc->instrument, cdata[0], IMLEN_VALUE);
  if (status==KEY_NO_EXIST) status = 0;

  strncpy (desc->observer, "        ", 9); 
  fits_read_key_str (in->myFptr, "OBSERVER", cdata[0], commnt, &status);
  if (status==0) strncpy (desc->observer, cdata[0], IMLEN_VALUE);
  if (status==KEY_NO_EXIST) status = 0;

  strncpy (desc->object, "        ", 9);
  fits_read_key_str (in->myFptr, "OBJECT", cdata[0], commnt, &status);
  if (status==0) strncpy (desc->object, cdata[0], IMLEN_VALUE);
  if (status==KEY_NO_EXIST) status = 0;

  strncpy (desc->bunit, "        ", 9); 
  fits_read_key_str (in->myFptr, "BUNIT", cdata[0], commnt, &status);
  if (status==0) strncpy (desc->bunit, cdata[0], IMLEN_VALUE);
  if (status==KEY_NO_EXIST) status = 0;

  strncpy (desc->obsdat, "        ", 9); 
  fits_read_key_str (in->myFptr, "DATE-OBS", cdata[0], commnt, &status);
  if (status==0)  strncpy (desc->obsdat, cdata[0], IMLEN_VALUE); 
  if (status==KEY_NO_EXIST) status = 0;

  strncpy (desc->origin, "        ", 9); 
  fits_read_key_str (in->myFptr, "ORIGIN", cdata[0], commnt, &status);
  if (status==0)  strncpy (desc->origin, cdata[0], IMLEN_VALUE); 
  if (status==KEY_NO_EXIST) status = 0;

  for (i=0; i<IM_MAXDIM; i++) strncpy (desc->ctype[i], "        ", 9);
  for (i=0; i<IM_MAXDIM; i++) cdum[i] = &cdata[i][0];
  fits_read_keys_str (in->myFptr, "CTYPE", 1, IM_MAXDIM, cdum, &nfound, 
		      &status);
    if (status==0) {
      for (i=0; i<nfound; i++) strncpy (desc->ctype[i], cdata[i], 9);
    }
  if (status==KEY_NO_EXIST) status = 0;

  desc->epoch = 0.0;
  fits_read_key_flt (in->myFptr, "EPOCH", &desc->epoch, commnt, &status);
  if (status==KEY_NO_EXIST) status = 0;

  for (i=0; i<IM_MAXDIM; i++) desc->cdelt[i] = 0.0;
  fits_read_keys_flt (in->myFptr, "CDELT", 1, IM_MAXDIM, desc->cdelt, 
		      &nfound, &status);
  if (status==KEY_NO_EXIST) status = 0;

  for (i=0; i<IM_MAXDIM; i++) desc->crpix[i] = 1.0;
  fits_read_keys_flt (in->myFptr, "CRPIX", 1, IM_MAXDIM, desc->crpix, 
		      &nfound, &status);
  if (status==KEY_NO_EXIST) status = 0;
      
  for (i=0; i<IM_MAXDIM; i++) desc->crota[i] = 0.0;
  fits_read_keys_flt (in->myFptr, "CROTA", 1, IM_MAXDIM, desc->crota, 
		      &nfound, &status);
  if (status==KEY_NO_EXIST) status = 0;

  for (i=0; i<IM_MAXDIM; i++) desc->crval[i] = 0.0;
  fits_read_keys_dbl (in->myFptr, "CRVAL", 1, IM_MAXDIM, desc->crval, 
		      &nfound, &status);
  if (status==KEY_NO_EXIST) status = 0;

  desc->obsra = 0.0;
  fits_read_key_dbl (in->myFptr, "OBSRA", &desc->obsra, commnt, &status);
  if (status==KEY_NO_EXIST) status = 0;

  desc->obsdec = 0.0;
  fits_read_key_dbl (in->myFptr, "OBSDEC", &desc->obsdec, commnt, &status);
  if (status==KEY_NO_EXIST) status = 0;

  /*----------------new --------------------------*/
  desc->altRef = 0.0;
  fits_read_key_dbl (in->myFptr, "ALTRVAL", &desc->altRef, commnt, &status);
  if (status==KEY_NO_EXIST) status = 0;

  desc->altCrpix = 0.0;
  fits_read_key_flt (in->myFptr, "ALTRPIX", &desc->altCrpix, commnt, &status);
  if (status==KEY_NO_EXIST) status = 0;

  temp = 0;
  fits_read_key_lng (in->myFptr, "VELREF", &temp, commnt, &status);
  if (status==KEY_NO_EXIST) status = 0;
  desc->VelDef = temp / 256;
  desc->VelReference = temp - 256*desc->VelDef;

  desc->restFreq = 0.0;
  fits_read_key_dbl (in->myFptr, "RESTFREQ", &desc->restFreq, commnt, &status);
  if (status==KEY_NO_EXIST) status = 0;

  desc->xshift = 0.0;
  fits_read_key_flt (in->myFptr, "XSHIFT", &desc->xshift, commnt, &status);
  if (status==KEY_NO_EXIST) status = 0;

  desc->yshift = 0.0;
  fits_read_key_flt (in->myFptr, "YSHIFT", &desc->yshift, commnt, &status);
  if (status==KEY_NO_EXIST) status = 0;

  /* AIPS Clean parameters */
  ObitIOImageAIPSCLEANRead (in, &status);

  /* Look for anything else and add it to the InfoList on desc */
  ObitIOImageKeysOtherRead(in, &status, err);
  if (err->error)  /* add trace and return on error */
    Obit_traceback_val (err, "ObitIOImageFITSReadDescriptor", 
			in->name, retCode);

  /* was there an error? */
  if (status!=0) {
    Obit_log_error(err, OBIT_Error, "ERROR reading input FITS file header");
    Obit_cfitsio_error(err); /* copy cfitsio error stack */
    retCode = OBIT_IO_ReadErr;
    return retCode;
  }

  /* enforce defaults */
  ObitImageSelDefault(desc, sel);

  retCode = OBIT_IO_OK;
  return retCode;
} /* end ObitIOImageFITSReadDescriptor */

/**
 * Write Descriptor information to disk.
 * \param in Pointer to object with ObitImageDesc to be written.
 *           If infoList member contains "Quant" entry and it is
 *           > 0.0 and an integer output (Bitpix 16, 32) is specified 
 *           then the output will be quantized at this level.  
 * \param err ObitErr for reporting errors.
 * \return return code, 0=> OK
 */
ObitIOCode 
ObitIOImageFITSWriteDescriptor (ObitIOImageFITS *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;
  gchar keywrd[FLEN_KEYWORD], commnt[FLEN_COMMENT+1];
  gint i, status = 0, magic;
  glong temp;
  ObitImageDesc* desc;
  ObitImageSel* sel;
  gfloat bscale = -1.0, bzero = 0.0, quant = 0.0, rx, rn;
  gchar keyName[FLEN_KEYWORD];
  ObitInfoType keyType;
  gint32 nkey, dim[ MAXINFOELEMDIM];
  union blobEquiv {
    gchar    s[201];
    double   d;
    float    f;
    gboolean b;
    glong    i;
  } blob;
  gchar *routine = "ObitIOImageFITSWriteDescriptor";

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));
  g_assert (in->myDesc != NULL);

  desc = in->myDesc; /* descriptor pointer */
  sel  = in->mySel;  /* selector pointer */

  /* enforce defaults */
  ObitImageSelDefault(desc, sel);

  /*  Write keyword values */
  for (i=0; i<desc->naxis; i++) {
    strncpy (commnt, "Axis type ", FLEN_COMMENT);
    sprintf (keywrd, "CTYPE%d",i+1);
    fits_update_key_str (in->myFptr, keywrd, desc->ctype[i], commnt, 
			 &status);
    strncpy (commnt, "Axis coordinate increment", FLEN_COMMENT);
    sprintf (keywrd, "CDELT%d",i+1);
    fits_update_key_flt (in->myFptr, keywrd, desc->cdelt[i], 6, commnt, 
			 &status);
    strncpy (commnt, "Axis coordinate reference pixel", FLEN_COMMENT);
    sprintf (keywrd, "CRPIX%d",i+1);
    fits_update_key_flt (in->myFptr, keywrd, desc->crpix[i], 6, commnt, 
			 &status);
    strncpy (commnt, "Axis coordinate rotation", FLEN_COMMENT);
    sprintf (keywrd, "CROTA%d",i+1);
    fits_update_key_flt (in->myFptr, keywrd, desc->crota[i], 6, commnt, 
			 &status);
    strncpy (commnt, "Axis coordinate value at CRPIX", FLEN_COMMENT);
    sprintf (keywrd, "CRVAL%d",i+1);
    fits_update_key_dbl (in->myFptr, keywrd, desc->crval[i], 12, commnt, 
			 &status);
  }
  strncpy (commnt, "Observed Right Ascension", FLEN_COMMENT);
  fits_update_key_dbl (in->myFptr, "OBSRA", desc->obsra, 12, commnt, 
		       &status);
  strncpy (commnt, "Observed declination ", FLEN_COMMENT);
  fits_update_key_dbl (in->myFptr, "OBSDEC", desc->obsdec, 12, commnt, 
		       &status);
  strncpy (commnt, "Name of object", FLEN_COMMENT);
  fits_update_key_str (in->myFptr, "OBJECT", desc->object,  commnt, 
		       &status);
  strncpy (commnt, "Telescope used", FLEN_COMMENT);
  fits_update_key_str (in->myFptr, "TELESCOP", desc->teles,  commnt, 
		       &status);
  strncpy (commnt, "Instrument used", FLEN_COMMENT);
  fits_update_key_str (in->myFptr, "INSTRUME", desc->instrument,  commnt, 
		       &status);
  strncpy (commnt, "Observer/project", FLEN_COMMENT);
  fits_update_key_str (in->myFptr, "OBSERVER", desc->observer,  commnt, 
		       &status);
  strncpy (commnt, "Date (yyyy-mm-dd) of observation", FLEN_COMMENT);
  fits_update_key_str (in->myFptr, "DATE-OBS", desc->obsdat, commnt, 
		       &status);
  strncpy (commnt, "Software last writing file", FLEN_COMMENT);
  fits_update_key_str (in->myFptr, "ORIGIN", desc->origin, commnt, 
		       &status);
  strncpy (commnt, "Celestial coordiate equinox", FLEN_COMMENT);
  fits_update_key_flt (in->myFptr, "EPOCH", desc->epoch, 6,  commnt, 
		       &status);
  strncpy (commnt, "Maximum in data array", FLEN_COMMENT);
  fits_update_key_flt (in->myFptr, "DATAMAX", desc->maxval, 8,  commnt, 
		       &status);
  strncpy (commnt, "Minimum in data array", FLEN_COMMENT);
  fits_update_key_flt (in->myFptr, "DATAMIN", desc->minval, 8,  commnt, 
		       &status);
  strncpy (commnt, "Image pixel units", FLEN_COMMENT);
  fits_update_key_str (in->myFptr, "BUNIT", desc->bunit, commnt, &status);
  
  if (desc->altRef != 0.0 ) {
    strncpy (commnt, "Alternate reference value", FLEN_COMMENT);
    fits_update_key_dbl (in->myFptr, "ALTRVAL", desc->altRef, 12, commnt, 
		       &status);
  }

  if (desc->altCrpix != 0.0) {
    strncpy (commnt, "Alternate reference pixel", FLEN_COMMENT);
    fits_update_key_flt (in->myFptr, "ALTRPIX", desc->altCrpix, 6, commnt, 
		       &status);
  }

  temp = desc->VelReference + 256*desc->VelDef;
  if (temp!=0) {
     strncpy (commnt, ">256 radio, 1 LSR, 2 Hel, 3 Obs", FLEN_COMMENT);
    fits_update_key_lng (in->myFptr, "VELREF", temp, commnt, 
		       &status);
  }

  if (desc->restFreq != 0.0) {
    strncpy (commnt, "Line rest frequency (Hz)", FLEN_COMMENT);
    fits_update_key_dbl (in->myFptr, "RESTFREQ", desc->restFreq, 12, commnt, 
		       &status);
  }

  if (desc->xshift != 0.0) {
     strncpy (commnt, "Net shift of Phase center in x", FLEN_COMMENT);
     fits_update_key_flt (in->myFptr, "XSHIFT", desc->xshift, 6, commnt, 
			  &status);
  }

  if (desc->yshift != 0.0) {
     strncpy (commnt, "Net shift of Phase center in y", FLEN_COMMENT);
     fits_update_key_flt (in->myFptr, "YSHIFT", desc->yshift, 6, commnt, 
			  &status);
  }

  /* AIPS Clean parameters */
  ObitIOImageAIPSCLEANWrite (in, &status);

  /* Write other keywords from descriptor */
  if (desc->info) nkey = desc->info->number; /* How many keywords? */
  else nkey = 0;
  retCode = OBIT_IO_WriteErr;
  strncpy (commnt, "             ", FLEN_COMMENT);
  for (i=0; i<nkey; i++) {
    /* Read from ObitInfoList */
    ObitInfoListGetNumber(desc->info, i, &keyName[0], &keyType, dim, 
			  blob.s, err);
    if (err->error)  /* add trace and return on error */
      Obit_traceback_val (err, routine, in->name, retCode);
    /* write by type */
    if (keyType==OBIT_double) {
      fits_update_key_dbl (in->myFptr, keyName, blob.d, 12, commnt, 
			   &status);
    } else if (keyType==OBIT_float) { 
      fits_update_key_flt (in->myFptr, keyName, blob.f, 6, commnt, 
			   &status);
    } else if (keyType==OBIT_string) { 
      blob.s[dim[0]] = 0; /* may not be null terminated */
      fits_update_key_str (in->myFptr, keyName, blob.s, commnt, 
			   &status);
    } else if (keyType==OBIT_long) { 
      fits_update_key_lng (in->myFptr, keyName, blob.i, commnt, 
			   &status);
    } else if (keyType==OBIT_oint) { 
      fits_update_key_lng (in->myFptr, keyName, (gint)blob.i, commnt, 
			   &status);
    } else if (keyType==OBIT_bool) { 
      fits_update_key_log (in->myFptr, keyName, blob.b, commnt, 
			   &status);
    }
  } /* end loop writing additional keywords */

  /* if output is an integer type and meaningful max/min - 
     set scaling, blanking */
  if ((desc->bitpix>0) && (desc->maxval > desc->minval)) {

    /* See if Quantization desired */
    ObitInfoListGetTest(desc->info, "Quant", &keyType, dim, (gpointer*)&quant);
    if (quant>0.0) {  /* Tell about it */
      Obit_log_error(err, OBIT_InfoErr, "%s: quantizing output at %f",routine,quant);
    }

    /* scaling for integer types */
    switch (desc->bitpix) {
    case 8:
      bscale = (desc->maxval-desc->minval) /  254.0;
      bzero = desc->minval;
      magic = 255;
      break;
    case 16:
      bscale = (desc->maxval-desc->minval) / 32760.0;
      magic = -32765;
      bzero = desc->minval;
      /* quantizing? */
      if (quant>0.0) { 
	bzero = 0.0;
	bscale = quant;
	rx = fabs(desc->maxval) / quant;
	rn = fabs(desc->minval) / quant;
	rx = MAX(rx, rn);
	if (rx > 32760.0) { /* can't fit in 16 bits */
	  Obit_log_error(err, OBIT_InfoErr, 
			 "%s: quantization %f too fine for 16 bits",
			 routine, quant);
	  return OBIT_IO_SpecErr;
	}
      }
      break;
    case 32:
      bscale =  (desc->maxval-desc->minval) / 2147483600.0;
      bzero = desc->minval;
      magic = -2147483605;
      /* quantizing? */
      if (quant>0.0) { 
	bzero = 0.0;
	bscale = quant;
	rx = fabs(desc->maxval) / quant;
	rn = fabs(desc->minval) / quant;
	rx = MAX(rx, rn);
	if (rx > 2147483600.0) { /* can't fit in 32 bits */
	  Obit_log_error(err, OBIT_InfoErr, 
			 "%s: quantization %f too fine for 32 bits",
			 routine, quant);
	  return OBIT_IO_SpecErr;
	}
      }
      break;
    default:
      break;
    } /* end switch on bitpix */

    strncpy (commnt, "Blanking value", FLEN_COMMENT);
    fits_update_key (in->myFptr, TLONG, "BLANK", (void*)&magic, commnt, 
		     &status);

    /* set data scaling */
    strncpy (commnt, "Data scaling value", FLEN_COMMENT);
    fits_update_key (in->myFptr, TFLOAT, "BSCALE", (void*)&bscale, commnt, 
		     &status);
    strncpy (commnt, "Data offset value", FLEN_COMMENT);
    fits_update_key (in->myFptr, TFLOAT, "BZERO", (void*)&bzero, commnt, 
		     &status);
  } /* end special handling for integers */

  /* an error? */
  if (status!=0) {
    Obit_log_error(err, OBIT_Error, "ERROR writing output FITS file header");
    Obit_cfitsio_error(err); /* copy cfitsio error stack */
    retCode = OBIT_IO_WriteErr;
    return retCode;
  }

  retCode = OBIT_IO_OK;
  return retCode;
} /* end ObitIOImageFITSWriteDescriptor */

/**
 * Flush I/O buffer if necessary 
 * \param in Pointer to object to be accessed.
 * \param err ObitErr for reporting errors.
 * \return return code, 0=> OK
 */
ObitIOCode ObitIOImageFITSFlush (ObitIOImageFITS *in, ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_SpecErr;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));

  /* cfitsio does the buffer flushing on close */

  retCode = OBIT_IO_OK;
  return retCode;
} /* end ObitIOImageFITSFlush */

/**
 * Create buffer approptiate for I/O request.
 * Not actually used for Images.
 * Should be called after ObitIO is opened.
 * \param data (output) pointer to data array
 * \param size (output) size of data array in floats.
 * \param in Pointer to object to be accessed.
 * \param info ObitInfoList with instructions
 * \param err ObitErr for reporting errors.
 */
void 
ObitIOImageFITSCreateBuffer (gfloat **data, glong *size, 
			     ObitIOImageFITS *in, ObitInfoList *info, 
			     ObitErr *err)
{
  /* just return - never called */
} /* end ObitIOImageFITSCreateBuffer */

/**
 * Return a ObitTable Object to a specified table associated with
 * the input ObitIO.  
 * If such an object exists, a reference to it is returned,
 * else a new object is created and entered in the ObitTableList.
 * Returned object is typed an Obit to prevent circular definitions
 * between the ObitTable and the ObitIO classes.
 * \param in       Pointer to object with associated tables.
 *                 This MUST have been opened before this call.
 * \param access   access (OBIT_IO_ReadOnly,OBIT_IO_ReadWrite,
 *                 or OBIT_IO_WriteOnly).
 *                 This is used to determine defaulted version number
 *                 and a different value may be used for the actual 
 *                 Open.
 * \param tabType  The table type (e.g. "AIPS CC").
 * \param tabVer   Desired version number, may be zero in which case
 *                 the highest extant version is returned for read
 *                 and the highest+1 for OBIT_IO_WriteOnly.
 * \param err      ObitErr for reporting errors.
 * \return pointer to created ObitTable, NULL on failure.
 */
Obit* 
newObitIOImageFITSTable (ObitIOImageFITS *in, ObitIOAccess access, 
			 gchar *tabType, glong *tabVer, ObitErr *err)
{
  ObitTable *out;
  glong version;
  gboolean gotIt;
  gchar *outName, tabName[51];

  /* error check */
  g_assert (ObitErrIsA(err));
  if (err->error) return NULL;
  g_assert (ObitIsA((Obit*)in, &myClassInfo));
  g_assert(tabType!=NULL);
  g_assert(tabVer!=NULL);

  /* the Tablelist object must be present */
  if (in->tableList==NULL) {
      Obit_log_error(err, OBIT_Error, 
		     "my tableList member is NULL, open %s first", 
		     in->name);
      return NULL;
  }

  /* Do we already have this one? */
  version = *tabVer;
  gotIt = ObitTableListGet ((ObitTableList*)in->tableList, tabType, &version, 
			    &out, err);
  if (err->error)
    Obit_traceback_val (err, "newObitIOImageFITSTable", in->name, NULL);

  /* Check if we're forcing a new table */
  if ((access==OBIT_IO_WriteOnly) && (*tabVer <= 0)) {
    version++;
    out = ObitTableUnref(out);
  }

  /* Set output table version */
  *tabVer = version;
  
  if (gotIt && (out!=NULL)) return (Obit*)out; /* that was easy */

   /* If it doesn't exist and request is read only - return NULL */
  if ((!gotIt) && (access==OBIT_IO_ReadOnly)) return NULL;

  /* Create one - make descriptive name */
  g_snprintf (tabName, 50, "%s table %ld for ",tabType, *tabVer);
  outName =  g_strconcat (tabName, in->name, NULL);
  out = newObitTable (outName);
  g_free(outName);

  /* Setup info needed for access */
  ObitTableSetFITS(out, -1, in->FileName, tabType, version, 
		  25, err);
 if (err->error)
   Obit_traceback_val (err, "newObitIOImageFITSTable", in->name, NULL);
 
 
 /* register it in the TableList */
 ObitTableListPut ((ObitTableList*)in->tableList, tabType, &version, 
		   out, err);
 if (err->error)
   Obit_traceback_val (err, "newObitIOImageFITSTable", in->name, NULL);

 return (Obit*)out;
} /* end newObitIOImageFITSTable */

/**
 * Update any disk resident structures about the current tables.
 * Nothing is needed for FITS files.
 * \param in   Pointer to object to be updated.
 * \param info ObitInfoList of parent object (not used here).
 * \param err  ObitErr for reporting errors.
 * \return return code, OBIT_IO_OK=> OK
 */
ObitIOCode ObitIOImageFITSUpdateTables (ObitIOImageFITS *in, ObitInfoList *info, 
					ObitErr *err)
{
  ObitIOCode retCode = OBIT_IO_OK;

  /* error checks */
  g_assert (ObitErrIsA(err));
  if (err->error) return retCode;
  g_assert (ObitIsA(in, &myClassInfo));

  return retCode;
} /* end ObitIOImageFITSUpdateTables */

/**
 * Initialize global ClassInfo Structure.
 */
void ObitIOImageFITSClassInit (void)
{
  const ObitClassInfo *ParentClass;

  if (myClassInfo.initialized) return;  /* only once */
  myClassInfo.initialized = TRUE;

  /* Initialize (recursively) parent class first */
  ParentClass = ObitIOGetClass();
  ObitIOClassInit();  /* Initialize parent class if needed */

  /* function pointers etc. for this class */
  myClassInfo.ClassName     = g_strdup(myClassName);
  myClassInfo.ParentClass   = ParentClass;
  myClassInfo.ObitClassInit = 
    (ObitClassInitFP)ObitIOImageFITSClassInit;
  myClassInfo.newObit       = NULL;
  myClassInfo.newObitIO     = (newObitIOFP)newObitIOImageFITS;
  myClassInfo.ObitIOSame    = (ObitIOSameFP)ObitIOImageFITSSame;
  myClassInfo.ObitIOZap     = (ObitIOZapFP)ObitIOImageFITSZap;
  myClassInfo.ObitCopy      = (ObitCopyFP)ObitIOImageFITSCopy;
  myClassInfo.ObitClone     = NULL;
  myClassInfo.ObitRef       = (ObitRefFP)ObitRef;
  myClassInfo.ObitUnref     = (ObitUnrefFP)ObitUnref;
  myClassInfo.ObitIsA       = (ObitIsAFP)ObitIsA;
  myClassInfo.ObitClear     = (ObitClearFP)ObitIOImageFITSClear;
  myClassInfo.ObitInit      = (ObitInitFP)ObitIOImageFITSInit;
  myClassInfo.ObitIOOpen    = (ObitIOOpenFP)ObitIOImageFITSOpen;
  myClassInfo.ObitIOClose   = (ObitIOCloseFP)ObitIOImageFITSClose;
  myClassInfo.ObitIOSet     = (ObitIOSetFP)ObitIOImageFITSSet;
  myClassInfo.ObitIORead    = (ObitIOReadFP)ObitIOImageFITSRead;
  myClassInfo.ObitIOReadSelect = (ObitIOReadSelectFP)ObitIOImageFITSRead;
  myClassInfo.ObitIOWrite   = (ObitIOWriteFP)ObitIOImageFITSWrite;
  myClassInfo.ObitIOFlush   = (ObitIOFlushFP)ObitIOImageFITSFlush;
  myClassInfo.ObitIOReadDescriptor  = 
    (ObitIOReadDescriptorFP)ObitIOImageFITSReadDescriptor;
  myClassInfo.ObitIOWriteDescriptor = 
    (ObitIOWriteDescriptorFP)ObitIOImageFITSWriteDescriptor;
  myClassInfo.ObitIOCreateBuffer = 
    (ObitIOCreateBufferFP)ObitIOImageFITSCreateBuffer;
  myClassInfo.ObitIOFreeBuffer   = 
    (ObitIOFreeBufferFP)ObitIOFreeBuffer;
  myClassInfo.newObitIOTable   = 
    (newObitIOTableFP)newObitIOImageFITSTable; 
  myClassInfo.ObitIOUpdateTables   = 
    (ObitIOUpdateTablesFP)ObitIOImageFITSUpdateTables;
} /* end ObitIOImageFITSClassInit */
/*--------------- Private functions --------------------------*/
/**
 * Creates empty member objects.
 * The GType constructors will call the corresponding routines
 * for each parent class.
 * \param inn Pointer to the object to initialize.
 */
void ObitIOImageFITSInit  (gpointer inn)
{
  ObitIOImageFITS *in = inn;
  const ObitClassInfo *ParentClass;

  /* error checks */
  g_assert (in != NULL);
  
  /* recursively initialize parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  if ((ParentClass!=NULL) && (ParentClass->ObitInit!=NULL)) 
    ParentClass->ObitInit (inn);

  /* set members in this class */
  in->FileName = NULL;

} /* end ObitIOImageFITSInit */

/**
 * Deallocates member objects.
 * Does (recursive) deallocation of parent class members.
 * For some reason this wasn't build into the GType class.
 * \param  inn Pointer to the object to deallocate.
 */
void ObitIOImageFITSClear (gpointer inn)
{
  ObitIOImageFITS *in = inn;
  const ObitClassInfo *ParentClass;
  ObitErr *err;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* close I/O if still active */
  if ((in->myStatus==OBIT_Active) ||(in->myStatus==OBIT_Modified)) {
    err = newObitErr();
    ObitIOImageFITSClose (in, err);
    if (err->error) ObitErrLog(err);
    err = ObitErrUnref(err);
  }

  /* delete this class members */
  if (in->FileName) g_free(in->FileName);
  in->FileName = NULL;

 /* unlink parent class members */
  ParentClass = (ObitClassInfo*)(myClassInfo.ParentClass);
  /* delete parent class members */
  if ((ParentClass!=NULL) && (ParentClass->ObitClear!=NULL)) 
    ParentClass->ObitClear (inn);

} /* end ObitIOImageFITSClear */


/**
 * Look for rational keywords for the CLEAN parameters and
 * failing this, look in AIPS history keywords.
 * Descriptor values niter, beamMaj, beamMin, beamPA
 * \param in Pointer to ObitIOImageFITS.
 * \param status (Output) cfitsio status.
 * \return return code, 0=> OK
 */
void  ObitIOImageAIPSCLEANRead(ObitIOImageFITS *in, gint *status)
{
  gchar commnt[FLEN_COMMENT], card[FLEN_COMMENT], temp[FLEN_COMMENT];
  gint i, j, k, keys, morekeys;
  gboolean gotBeam=FALSE, gotNiter=FALSE;
  ObitImageDesc *desc;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  if (*status) return;  /* existing cfitsio error? */

  desc = in->myDesc; /* set descriptor */

  /* Attempt rational keywords */
  desc->beamMaj = -1.0;
  fits_read_key_flt (in->myFptr, "CLEANBMJ", &desc->beamMaj, 
		     commnt, status);
  gotBeam = gotBeam || (*status != KEY_NO_EXIST);
  if (*status==KEY_NO_EXIST) *status = 0;

  desc->beamMin = -1.0;
  fits_read_key_flt (in->myFptr, "CLEANBMN", &desc->beamMin, 
		     commnt, status);
  gotBeam = gotBeam || (*status != KEY_NO_EXIST);
  if (*status==KEY_NO_EXIST) *status = 0;

  desc->beamPA = -1.0;
  fits_read_key_flt (in->myFptr, "CLEANBPA", &desc->beamPA, 
		     commnt, status);
  gotBeam = gotBeam || (*status != KEY_NO_EXIST);
  if (*status==KEY_NO_EXIST) *status = 0;

  desc->niter = -1;
  fits_read_key_lng (in->myFptr, "CLEANNIT", &desc->niter, 
		     commnt, status);
  gotNiter = gotNiter || (*status != KEY_NO_EXIST);
  if (*status==KEY_NO_EXIST) *status = 0;

  /* If this worked, we're done */
  if (gotBeam && gotNiter) return;

  /* Oh Well, parse all the header cards looking for: 
          1         2         3         4         5         6
0123456789012345678901234567890123456789012345678901234567890123456789
HISTORY AIPS   CLEAN BMAJ=  1.3432E-07 BMIN=  4.3621E-08 BPA= -43.11  
HISTORY AIPS   CLEAN NITER=     1000 PRODUCT=1   / NORMAL   
  */

  /* how many keywords to look at? */
  fits_get_hdrspace (in->myFptr, &keys, &morekeys, status);
  for (k=1; k<=keys; k++) {
    fits_read_record (in->myFptr, k, card, status);
    if (*status==0) {
      if (!strncmp ("HISTORY AIPS   CLEAN BMAJ", card, 25)) {
	/* Parse card */
	for (j=0,i=26; i<38; i++) temp[j++] = card[i]; temp[j] = 0;
	sscanf (temp, "%f", &desc->beamMaj);
	for (j=0,i=44; i<56; i++) temp[j++] = card[i]; temp[j] = 0;
	sscanf (temp, "%f", &desc->beamMin);
	for (j=0,i=61; i<68; i++) temp[j++] = card[i]; temp[j] = 0;
	sscanf (temp, "%f", &desc->beamPA);
	gotBeam = TRUE;
      } else if (!strncmp ("HISTORY AIPS   CLEAN NITER", card, 26)) {
	for (j=0,i=27; i<36; i++) temp[j++] = card[i]; temp[j] = 0;
	sscanf (temp, "%ld", &desc->niter);
      }
      /* Are we there yet? */
      if (gotBeam && gotNiter) return;
    }
  }

} /* end ObitIOImageAIPSCLEANRead */

/**
 * Write both rational keywords and AIPS HISTORY cards
 * Descriptor values niter, beamMaj, beamMin, beamPA
 * \param in Pointer to ObitIOImageFITS.
 * \param status (Output) cfitsio status.
 * \return return code, 0=> OK
 */
void  ObitIOImageAIPSCLEANWrite (ObitIOImageFITS *in, gint *status)
{
  gchar commnt[FLEN_COMMENT+1], card[FLEN_CARD+1];
  ObitImageDesc *desc;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  if (*status) return;  /* existing error? */

  desc = in->myDesc; /* set descriptor */

  /* Rational keywords */
  if (desc->beamMaj > 0.0) {
     strncpy (commnt, "Convolving Gaussian major axis FWHM (deg)", 
	      FLEN_COMMENT);
     fits_update_key_flt (in->myFptr, "CLEANBMJ", desc->beamMaj, 
			  6, commnt,  status);
     strncpy (commnt, "Convolving Gaussian minor axis FWHM (deg)", 
	      FLEN_COMMENT);
     fits_update_key_flt (in->myFptr, "CLEANBMN", desc->beamMin,  
			  6, commnt, status);
     strncpy (commnt, "Convolving Gaussian position angle (deg)", 
	      FLEN_COMMENT);
     fits_update_key_flt (in->myFptr, "CLEANBPA", desc->beamPA,  
			  6, commnt, status);
  }
  if (desc->niter > 0) {
     strncpy (commnt, "Number of Clean iterations", FLEN_COMMENT);
     fits_update_key_lng (in->myFptr, "CLEANNIT", desc->niter,  
			  commnt, status);
  }

  /* Purge previous version */
  PurgeAIPSHistory (in, status);

  /* Hide 'em where AIPS can find them */
  if (desc->beamMaj > 0.0) {
    g_snprintf (card, FLEN_COMMENT,
		"AIPS   CLEAN BMAJ=%12.4e BMIN=%12.4e BPA=%7.2f",
		desc->beamMaj, desc->beamMin, desc->beamPA);
    fits_write_history (in->myFptr, card, status);
  }

   if (desc->niter > 0) {
    g_snprintf (card, FLEN_COMMENT,
		"AIPS   CLEAN NITER=%9ld", desc->niter);
    fits_write_history (in->myFptr, card, status);
  }
} /* end ObitIOImageAIPSCLEANWrite */

/**
 * Look for additional descriptive keywords, any that are not 
 * on the exclusion list are copied to the descriptor InfoList.
 * \param in      Pointer to ObitIOImageFITS.
 * \param status (Output) cfitsio status.
 * \param err    ObitErr stack.
 * \return return code, 0=> OK
 */
void  ObitIOImageKeysOtherRead(ObitIOImageFITS *in, gint *status, 
			       ObitErr *err)
{
  gchar keywrd[FLEN_KEYWORD], value[FLEN_VALUE], commnt[FLEN_COMMENT+1];
  gchar *first, *last, *anF, *aT, dtype, svalue[FLEN_VALUE];
  gint i, j, k, keys, morekeys;
  glong ivalue;
  gint32 dim[MAXINFOELEMDIM] = {1,1,1,1,1};
  double dvalue;
  ObitImageDesc *desc;
  gchar *exclude[] = 
  {"SIMPLE", "BITPIX", "EXTEND", "HISTORY", "COMMENT", "BLANK", "        ",
   "BSCALE", "BZERO", "NAXIS", "BLOCKED", 
   "CTYPE", "CDELT", "CRPIX", "CROTA", "CRVAL", "OBSRA", "OBSDEC", 
   "OBJECT", "TELESCOP", "DATE", "EPOCH", "DATAMAX", "DATAMIN", "BUNIT", 
   "ALTRVAL", "ALTRPIX", "VELREF", "RESTFREQ", "XSHIFT", "YSHIFT", 
   "CLEAN", NULL};
  gint number, *len;
  gboolean bvalue, bad;
  gchar *routine = "ObitIOImageKeysOtherRead";

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));

  /* delete old InfoList and restart */
  ((ObitImageDesc*)in->myDesc)->info = ObitInfoListUnref (((ObitImageDesc*)in->myDesc)->info);
  ((ObitImageDesc*)in->myDesc)->info = (gpointer)newObitInfoList ();
  desc = in->myDesc; /* set descriptor */

  /* get number and length of exclusion strings */
  number = 0;
  i = 0;
  while (exclude[i]!=NULL) {
    number++;
    i++;
  }
  len = ObitMemAlloc0Name(number*sizeof(gint), routine);
  for (i=0; i<number; i++) len[i] = strlen(exclude[i]);

  /* how many keywords to look at? */
  fits_get_hdrspace (in->myFptr, &keys, &morekeys, status);
  for (k=1; k<=keys; k++) {
    fits_read_keyn (in->myFptr, k, keywrd, value, commnt, status);
    if (*status==0) {
      /* Is this on the list? */
      for (j=0; j<number; j++) {
	bad = !strncmp (keywrd, exclude[j], len[j]);
	bad = bad || (strlen(keywrd)<=0); /* blank keyword */
	if (bad) break;
      } /* end loop over exclusions */
      /* want this one? */

      if (!bad) {
	/* ask cfitsio what it is */
	fits_get_keytype (value, &dtype, status);
	switch (dtype) { 
	case 'C':  /* Character string */
	  first = index (value,'\'')+1; /* a string? */
	  last = rindex(value,'\'')-1;
	  g_memmove(svalue, first, (last-first+1));
	  svalue[last-first+1] = 0; /* null terminate */
	  /* add to InfoList */
	  dim[0] = strlen(svalue);
	  ObitInfoListPut(desc->info, keywrd, OBIT_string, dim, 
			  (gconstpointer)svalue, err);
	  
	  break;
	case 'L':  /* logical 'T', 'F' */
	  anF   = index (value,'F'); /* Logical */
	  aT    = index (value,'T'); /* Logical */
	  bvalue = FALSE;
	  if (aT!=NULL) bvalue = TRUE;
	  /* add to InfoList */
	  dim[0] = 1;
	  ObitInfoListPut(desc->info, keywrd, OBIT_bool, dim, 
			  (gconstpointer)&bvalue, err);
	  break;
	case 'I':  /* Integer */
	  ivalue = strtol(value, NULL, 10);
	  /* add to InfoList */
	  dim[0] = 1;
	  ObitInfoListPut(desc->info, keywrd, OBIT_oint, dim, 
			  (gconstpointer)&ivalue, err);
	  break;
	case 'F':  /* Float - use double */
	  dvalue = strtod(value, &last);
	  /* add to InfoList */
	  dim[0] = 1;
	  ObitInfoListPut(desc->info, keywrd, OBIT_double, dim, 
			  (gconstpointer)&dvalue, err);
	  break;
	case 'X':  /* Complex - can't handle */
	default:
	  g_assert_not_reached(); /* unknown, barf */
	}; /* end switch on type */
	
	/* error check */
	if (err->error)  {/* add trace and return on error */
	  ObitMemFree(len);
	  Obit_traceback_msg (err, routine, in->name);
	}
      }
    }
  } /* end loop over keywords */

  /* cleanup */
  ObitMemFree(len);

} /* end ObitIOImageKeysOtherRead */

/**
 * Delete selected "HISTORY AIPS" keywords from history
 * \param in      Pointer to ObitIOImageFITS.
 * \param status (Output) cfitsio status.
 * \return return code, 0=> OK
 */
static void PurgeAIPSHistory(ObitIOImageFITS *in, gint *status)
{
  gchar card[FLEN_CARD+1];
  gint k, keys, morekeys;

  /* error checks */
  g_assert (ObitIsA(in, &myClassInfo));
  if (*status) return;  /* existing cfitsio error? */

  /* Oh Well, parse all the header cards looking for: 
          1         2         3         4         5         6
0123456789012345678901234567890123456789012345678901234567890123456789
HISTORY AIPS   CLEAN BMAJ=  1.3432E-07 BMIN=  4.3621E-08 BPA= -43.11  
HISTORY AIPS   CLEAN NITER=     1000 PRODUCT=1   / NORMAL   
  */

  /* how many keywords to look at? */
  fits_get_hdrspace (in->myFptr, &keys, &morekeys, status);
  for (k=keys; k>=1; k--) {
    fits_read_record (in->myFptr, k, card, status);
    if (*status==0) {
      if (!strncmp ("HISTORY AIPS   CLEAN BMAJ", card, 25)) {
	/* Delete card */
	fits_delete_record (in->myFptr, k, status);
      } else if (!strncmp ("HISTORY AIPS   CLEAN NITER", card, 26)) {
 	/* Delete card */
	fits_delete_record (in->myFptr, k, status);
     }
    }
  }

} /* end  PurgeAIPSHistory */
