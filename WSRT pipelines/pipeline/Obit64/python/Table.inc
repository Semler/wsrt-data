/* $Id: Table.inc,v 1.10 2005/06/21 10:30:37 mketteni Exp $   */  
/*--------------------------------------------------------------------*/
/* Swig module description for Table type                             */
/*                                                                    */
/*;  Copyright (C) 2004,2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

%{
#include "ObitTable.h"
#include "ObitTableUtil.h"
%}

%inline %{
// Routine to remove trailing blanks from a string 
static void TableDeeBlank (gchar *in) 
{
  gint i;

  for (i=strlen(in)-1; i>=0; i--) {
     if (in[i]==' ') in[i] = 0;
     else if (in[i]!=' ') break;
  }

} // end TableDeeBlank

extern ObitTable* TableCreate (gchar* name) {
  return newObitTable (name);
} // end  TableCreate

extern ObitTable* TableZap  (ObitTable *in, ObitErr *err) {
  return ObitTableZap (in, err);
} // end TableZap

extern ObitTable* TableCopy  (ObitTable *in, ObitTable *out, 
			      ObitErr *err) {
  return ObitTableCopy (in, out, err);
} // end  TableCopy

extern ObitTable* TableClone (ObitTable *in, ObitTable *out) {
   return  ObitTableClone (in, out);
} // end  TableClone

extern void TableConcat  (ObitTable *in, ObitTable *out, ObitErr *err) {
  ObitTableConcat (in, out, err);
} // end  TableCopy

// Open and close to fully instantiate
// access 1=READONLY, 2=WRITEONLY, 3=READWRITE
extern int TablefullInstantiate (ObitTable* in, int access, ObitErr *err) {
  ObitIOCode ret;
  ObitIOAccess laccess;

  laccess = OBIT_IO_ReadOnly;
  if (access==2) laccess = OBIT_IO_WriteOnly;
  else if (access==3) laccess = OBIT_IO_ReadWrite;
  ret = ObitTableOpen (in, laccess, err);
  ret = ObitTableClose (in, err);
  if ((err->error) || (ret!=OBIT_IO_OK)) return 1;
  else return 0;
} // end TablefullInstantiate

extern int TableOpen (ObitTable *in, int access, ObitErr *err) {
  ObitIOCode ret;
  ObitIOAccess laccess;

  laccess = OBIT_IO_ReadOnly;
  if (access==2) laccess = OBIT_IO_WriteOnly;
  else if (access==3) laccess = OBIT_IO_ReadWrite;
  ret = ObitTableOpen (in, laccess, err);
  if (ret==OBIT_IO_OK) return 0;
  else return 1;
} // end Open

extern int TableClose (ObitTable *in, ObitErr *err) {
  ObitIOCode ret;
  ret =  ObitTableClose (in, err);
  if (ret==OBIT_IO_OK) return 0;
  else return 1;
} // end Close

extern  PyObject *TableReadRow (ObitTable *in, int rowno,
                         ObitErr *err) {
  ObitIOCode ret = OBIT_IO_SpecErr;
  ObitTableDesc *desc = in->myDesc;
  glong i, j, k, lrowno = rowno;
  ObitTableRow* row = newObitTableRow (in);
  PyObject *outDict = PyDict_New();
  PyObject *list;
  gshort   *idata;
  gint     *jdata;
  oint     *kdata;
  glong    *ldata;
  gchar    *cdata, *ctemp;
  gboolean *bdata;
  gfloat   *fdata;
  gdouble  *ddata;
  gchar *routine = "TableReadRow";

  if (err->error) return outDict;

  ret = ObitTableReadRow (in, lrowno, row, err);
  if (ret==OBIT_IO_OK) {
  // Convert row to python dict
  /* Table name */
  PyDict_SetItemString(outDict, "Table name", 
	PyString_InternFromString(desc->TableName));
  /* number of fields  */
  PyDict_SetItemString(outDict, "NumFields", 
	PyInt_FromLong(desc->nfield));
  /* Loop over fields */
  for (i=0; i<in->myDesc->nfield; i++) {
    if (desc->type[i] == OBIT_string)
      list = PyList_New(desc->repeat[i]/MAX (1,desc->dim[i][0]));
    else
      list = PyList_New(desc->repeat[i]);
    /* Fill list by type */
  switch (desc->type[i]) { 
    case OBIT_short:
      idata = ((gshort*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        PyList_SetItem(list, j, PyInt_FromLong((long)idata[j]));
      break;
    case OBIT_int:
      jdata = ((gint*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        PyList_SetItem(list, j, PyInt_FromLong((long)jdata[j]));
      break;
    case OBIT_oint:
      kdata = ((oint*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        PyList_SetItem(list, j, PyInt_FromLong((long)kdata[j]));
      break;
    case OBIT_long:
      ldata = ((glong*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        PyList_SetItem(list, j, PyInt_FromLong(ldata[j]));
      break;
    case OBIT_float:
      fdata = ((gfloat*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        PyList_SetItem(list, j, PyFloat_FromDouble((double)fdata[j]));
      break;
    case OBIT_double:
      ddata = ((gdouble*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        PyList_SetItem(list, j, PyFloat_FromDouble(ddata[j]));
      break;
    case OBIT_string:
      cdata = ((gchar*)row->myRowData)+desc->offset[i];
      /* null terminate string */
      ctemp = g_malloc0(desc->dim[i][0]+1);
      for (j=0; j<desc->repeat[i]/MAX (1,desc->dim[i][0]); j++) {
        for (k=0; k<desc->dim[i][0]; k++) ctemp[k] = cdata[k];  ctemp[k] = 0;
        PyList_SetItem(list, j, PyString_InternFromString(ctemp));
	cdata += desc->dim[i][0];
      }
      g_free (ctemp);
      break;
    case OBIT_bool:
      bdata = ((gboolean*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        PyList_SetItem(list, j, PyInt_FromLong((long)bdata[j]));
      break;
    default:
      /* Cannot deal with type */
      Obit_log_error(err, OBIT_Error, 
		   "%s: Cannot deal with Table row data type %d in %s", 
		   routine, desc->type[i], in->name);
    }; /* end switch by type */

    /* Set field in output dict */
    ctemp =  g_strdup(desc->FieldName[i]);
    TableDeeBlank(ctemp);
    PyDict_SetItemString(outDict, ctemp, list);
    g_free (ctemp);
  } /* End loop over fields */

 } /* end of read OK */

  row = ObitTableRowUnref(row);
  return outDict;	
} // end TableReadRow

extern int TableWriteRow (ObitTable *in, int rowno, PyObject *inDict,
                          ObitErr *err) {
  ObitIOCode ret = OBIT_IO_SpecErr;
  ObitTableDesc *desc = in->myDesc;
  glong i, j, k, lrowno = rowno;
  ObitTableRow* row = newObitTableRow (in);
  PyObject *list = NULL, *TabName=NULL;
  gshort   *idata;
  gint     *jdata;
  oint     *kdata;
  glong    *ldata, ltemp;
  gchar    *cdata, *ctemp, *tstr;
  gboolean *bdata, bad;
  gfloat   *fdata;
  gdouble  *ddata;
  gchar *routine = "TableWriteRow";

  if (err->error) return 1;

  if (!PyDict_Check(inDict)) {
	PyErr_SetString(PyExc_TypeError,"Input not a Dict");
        return 1;
  }

  /* Check that correct table type */
  TabName = PyDict_GetItemString(inDict, "Table name");
  if (TabName!=NULL)
    tstr = PyString_AsString(TabName);
  if (TabName==NULL || tstr==NULL) {  /* Found it? */
    Obit_log_error(err, OBIT_Error, "%s: Table Name not given", routine);
    return 1;
  }
  if (strncmp (tstr, desc->TableName, strlen(tstr))) {
    Obit_log_error(err, OBIT_Error, "%s: Table type '%s' NOT '%s'",
                   routine, tstr, desc->TableName);
    return 1;
  }
  /* Check number of fields */
  ltemp = PyInt_AsLong(PyDict_GetItemString(inDict, "NumFields"));
  if (ltemp!=desc->nfield) {
    Obit_log_error(err, OBIT_Error, "%s: no. columns %ld NOT %ld",
                   routine, ltemp, desc->nfield);
    return 1;
  }

  /* attach row to  output buffer */
  ObitTableSetRow (in, row, err);
  if (err->error) Obit_traceback_val (err, routine, in->name, 1);

  // Convert python dict to row
  /* Loop over fields */
  for (i=0; i<in->myDesc->nfield; i++) {
    bad = FALSE;  /* Check for field size */
    ctemp =  g_strdup(desc->FieldName[i]);
    TableDeeBlank(ctemp);
    list = PyDict_GetItemString(inDict, ctemp);
    if (list==NULL) {
      Obit_log_error(err, OBIT_Error, "%s: %s not given", routine, ctemp);
      return 1;
    }
    /* Is this really a list */
    if (!PyList_Check(list)) {
      Obit_log_error(err, OBIT_Error, "%s: %s member not a PyList",
                     routine, ctemp);
      return 1;
    }
    g_free (ctemp);
    /* Fill list by type */
  switch (desc->type[i]) { 
    case OBIT_short:
      bad = desc->repeat[i] != PyList_Size(list);
      idata = ((gshort*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        idata[j] = (gshort)PyInt_AsLong(PyList_GetItem(list, j));
      break;
    case OBIT_int:
      bad = desc->repeat[i] != PyList_Size(list);
      jdata = ((gint*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        jdata[j] = (gint)PyInt_AsLong(PyList_GetItem(list, j));
      break;
    case OBIT_oint:
      bad = desc->repeat[i] != PyList_Size(list);
      kdata = ((oint*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        kdata[j] = (oint)PyInt_AsLong(PyList_GetItem(list, j));
      break;
    case OBIT_long:
      bad = desc->repeat[i] != PyList_Size(list);
      ldata = ((glong*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        ldata[j] = (glong)PyInt_AsLong(PyList_GetItem(list, j));
      break;
    case OBIT_float:
      bad = desc->repeat[i] != PyList_Size(list);
      fdata = ((gfloat*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        fdata[j] =  (gfloat)PyFloat_AsDouble(PyList_GetItem(list, j));
      break;
    case OBIT_double:
      bad = desc->repeat[i] != PyList_Size(list);
      ddata = ((gdouble*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        ddata[j] =  (gdouble)PyFloat_AsDouble(PyList_GetItem(list, j));
      break;
    case OBIT_string:
      bad = (desc->repeat[i]/MIN (1,desc->dim[i][0])) != PyList_Size(list);
      cdata = ((gchar*)row->myRowData)+desc->offset[i];
      /* null terminate string */
      for (j=0; j<desc->repeat[i]/MIN (1,desc->dim[i][0]); j++) {
        ctemp = PyString_AsString(PyList_GetItem(list, j));
        for (k=0; k<strlen(ctemp); k++) cdata[k] = ctemp[k];
	cdata += desc->dim[i][0];
      }
      break;
    case OBIT_bool:
      bad = desc->repeat[i] != PyList_Size(list);
      bdata = ((gboolean*)row->myRowData)+desc->offset[i];
      for (j=0; j<desc->repeat[i]; j++) 
        bdata[j] = (gboolean)PyInt_AsLong(PyList_GetItem(list, j));
      break;
    default:
      /* Cannot deal with type */
      Obit_log_error(err, OBIT_Error, 
		   "%s: Cannot deal with Table row data type %d in %s", 
		   routine, desc->type[i], in->name);
      return 1;
  }; /* end switch by type */

  /* Check if sizes compatible */
  if (bad) {
      Obit_log_error(err, OBIT_Error, 
		   "%s: wrong size %ld %d", 
		   routine, desc->repeat[i], PyList_Size(list));
      return 1;
  }

  } /* End loop over fields */

  ret =  ObitTableWriteRow (in, lrowno, row, err);
  row = ObitTableRowUnref(row);
  if (ret==OBIT_IO_OK) return 0;
  else return 1;
} // end TableWriteRow

extern ObitTable* TableUnref (ObitTable* in) {
  if (!ObitTableIsA(in)) return NULL;
  return ObitTableUnref(in);
}

extern ObitTable*  TableRef (ObitTable* in) {
  return ObitTableRef(in);
}

extern ObitInfoList* TableGetList (ObitTable* in) {
  return ObitInfoListRef(in->info);
}

extern ObitInfoList* TableGetIOList (ObitTable* in) {
  ObitInfoList *info=NULL;
  if (in->myIO!=NULL) info = ((ObitTableDesc*)(in->myIO->myDesc))->info;
  return ObitInfoListRef(info);
}

extern ObitTableDesc* TableGetDesc (ObitTable* in) {
  return ObitTableDescRef(in->myDesc);
}

extern ObitTableDesc* TableGetIODesc (ObitTable* in) {
  ObitTableDesc *desc=NULL;
  if (in->myIO!=NULL) desc = (ObitTableDesc*)(in->myIO->myDesc);
  return ObitTableDescRef(desc);
}

extern long TableGetVer (ObitTable* in) {
  return in->myDesc->version;
}

extern int TableIsA (ObitTable* in) {
  return ObitTableIsA(in);
}

extern char* TableGetName (ObitTable* in) {
  if (ObitTableIsA(in)) {
    return in->name;
  } else {
    return NULL;
  }
}

// Table utilities 
extern int  TableUtilSort (ObitTable* in, char *colName, int desc, ObitErr *err) {
  ObitIOCode ret;
  gboolean ldesc;
  ldesc = desc != 0;
  ret =  ObitTableUtilSort (in, colName, ldesc, err);
  if (ret==OBIT_IO_OK) return 0;
  else return 1;
}

%}

/* Definitions for Python Shadow class */
/* A copy of the struct for c */
%{
typedef struct {
  ObitTable *me;
} Table;
%}
/* and a copy of the struct for swig */
typedef struct {
  ObitTable *me;
} Table;

%addmethods Table { 
  Table(char *name) {
     Table *out;
     out = (Table *) malloc(sizeof(Table));
     if (strcmp(name, "None")) out->me = TableCreate(name);
     else out->me = NULL;
     return out;
   }
  ~Table() {
    self->me = TableUnref(self->me);
    free(self);
  }
};

