# $Id: AIPSDir.py,v 1.8 2005/09/05 14:07:01 bcotton Exp $
#-----------------------------------------------------------------------
#  Copyright (C) 2004
#  Associated Universities, Inc. Washington DC, USA.
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,
#  MA 02139, USA.
#
#  Correspondence concerning this software should be addressed as follows:
#         Internet email: bcotton@nrao.edu.
#         Postal address: William Cotton
#                         National Radio Astronomy Observatory
#                         520 Edgemont Road
#                         Charlottesville, VA 22903-2475 USA
#-----------------------------------------------------------------------

# Python interface to AIPS directory utilities
import Obit, OSystem, OErr, pydoc, string

# Catalog types
AllType = 0
MAType  = 1
UVType  = 2

def PFindCNO(disk, user, Aname, Aclass, Atype, seq, err):
    """ Lookup AIPS catalog slot number

    returns AIPS cno
    disk     = AIPS disk number
    user     = AIPS user number
    Aname    = AIPS file name
    Aclass   = AIPS class name
    Atype    = 'MA' or 'UV' for image or uv data
    seq      = AIPS sequence number
    err      = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.AIPSDirFindCNO(disk, user, Aname, Aclass, Atype, seq, err.me)
    if err.isErr:
        raise err
    return ret
    # end PFindCNO


def PAlloc(disk, user, Aname, Aclass, Atype, seq, err):
    """ Allocate AIPS catalog slot number

    returns AIPS cno
    disk     = AIPS disk number
    user     = AIPS user number
    Aname    = AIPS file name
    Aclass   = AIPS class name
    Atype    = 'MA' or 'UV' for image or uv data
    seq      = AIPS sequence number
    err      = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.AIPSDirAlloc(disk, user, Aname, Aclass, Atype, seq, err.me)
    if err.isErr:
        raise err
    return ret
    # end PAlloc

def PRemove(disk, user, cno, err):
    """ Deallocate AIPS catalog slot number

    disk     = AIPS disk number
    user     = AIPS user number
    cno      = AIPS catalog slot to deassign
    err      = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.AIPSDirRemoveEntry(disk, user, cno, err.me)
    if err.isErr:
        raise err
    return ret
    # end PRemove
    
def PNumber(disk, user, err):
    """ Return highest current allowed AIPS catalog Slot number

    disk     = AIPS disk number
    user     = AIPS user number
    err      = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.AIPSDirNumber(disk, user, err.me)
    if err.isErr:
        raise err
    return ret
    # end PNumber

def PInfo(disk, user, cno, err):
    """ Get information for a  AIPS catalog slot number

    Returned string s:
    Aname  = s[0:12]
    Aclass = s[13:19]
    Aseq   = int(s[20:25])
    Atype  = s[26:28]

    returns string describing entry, None=>empty
    disk     = AIPS disk number
    user     = AIPS user number
    cno      = AIPS catalog slot to deassign
    err      = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.AIPSDirInfo(disk, user, cno, err.me)
    if err.isErr:
        raise err
    ssret = ret;
    if (ret!=None):
        Obit.AIPSDirInfoClean(ret)  # Deallocate string in c
    return ssret
    # end PInfo

def PSetDir(disk, newName, err):
    """ Set the directory name for a given AIPS directory

    disk     = AIPS disk number
    newName  = new directory path
    err      = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    ret = Obit.ObitAIPSSetDirname(disk, newName, err.me)
    if err.isErr:
        raise err
    # end PSetDir


def PListDir(disk, err, type = AllType, first=1, last=1000):
    """ List AIPS directory

    disk     = AIPS disk number
    err      = Python Obit Error/message stack
    type     = optional file type
    first    = optional first slot number (1-rel)
    last     = optional last slot number
    """
    ################################################################
    # Checks
    if not OErr.OErrIsA(err):
        raise TypeError,"err MUST be an OErr"
    #
    user = OSystem.PGetAIPSuser()
    ncno = PNumber (disk, user, err)
    OErr.printErrMsg(err, "Error getting number of cnos")

    mincno = first;
    maxcno = min (ncno, last)
    dirlist = "AIPS Directory listing for disk "+str(disk)+"\n"
    for cno in range(mincno, maxcno):
        line=PInfo(disk, user, cno, err);
        if (line!=None):
            OErr.printErrMsg(err, "Error reading entry")
            if (type==MAType) and (line[26:28]=='MA'):
                dirlist = dirlist+string.rjust(str(cno),3)+" "+line+"\n"
            elif (type==UVType) and (line[26:28]=='UV'):
                dirlist = dirlist+string.rjust(str(cno),3)+" "+line+"\n"
            elif (type==AllType):
                dirlist = dirlist+string.rjust(str(cno),3)+" "+line+"\n"
    if err.isErr:
        raise err
    # User pager
    pydoc.ttypager(dirlist)
    # end PListDir

