# $Id: InfoList.py,v 1.9 2005/08/07 18:22:51 bcotton Exp $
#-----------------------------------------------------------------------
#  Copyright (C) 2004,2005
#  Associated Universities, Inc. Washington DC, USA.
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,
#  MA 02139, USA.
#
#  Correspondence concerning this software should be addressed as follows:
#         Internet email: bcotton@nrao.edu.
#         Postal address: William Cotton
#                         National Radio Astronomy Observatory
#                         520 Edgemont Road
#                         Charlottesville, VA 22903-2475 USA
#-----------------------------------------------------------------------

# Python shadow class to ObitInfoList class
import Obit, OErr

class InfoListPtr :
    def __init__(self,this):
        self.this = this
    def __setattr__(self,name,value):
        if name == "me" :
            Obit.InfoList_me_set(self.this,value)
            return
        self.__dict__[name] = value
    def __getattr__(self,name):
        if name == "me" : 
            return Obit.InfoList_me_get(self.this)
        raise AttributeError,name
    def __repr__(self):
        return "<C InfoList instance>"
class InfoList(InfoListPtr):
    """ Python Obit InfoList class

    ObitInfoList Linked list of labeled items class.
    This facility allows storing arrays of values of the same (native) 
    data type and retrieving them by name or order number in the list.
    This container class is similar to a Python dictionary and is used
    extensively in Obit to pass control information.
    Most Obit objects contain an InfoList member.
    """
    def __init__(self) :
        self.this = Obit.new_InfoList()
    def __del__(self):
        if Obit!=None:
            Obit.delete_InfoList(self.this)

# Commonly used, dangerous variables
dim=[1,1,1,1,1]

def PCopy (inList):
    """ Copy list

    return copy of input InfoList
    inList    = input Python InfoList
    """
    ################################################################
    out = InfoList()
    out.me = Obit.InfoListUnref(out.me)
    out.me = Obit.InfoListCopy(inList.me)
    return out
# end PCopy 

def PCopyData (inList, outList):
    """ Copy all entries from inList to outList

    inList    = input Python InfoList
    outList   = output Python InfoList, previously exists
    """
    ################################################################
    Obit.InfoListCopyData(inList.me, outList.me)
    # end PCopyData

def PRemove (inList, name):
    """ Removes item name fro list

    inList    = input Python InfoList
    name      = name of desired entry
    """
    ################################################################
    Obit.InfoListRemove (inList.me, name)
    # end PRemove

def PItemResize (inList, name, type, dim):
    """ 

    inList   = input Python InfoList
    name     = name of desired entry
    type     = data type of object
               int=1, oint=3, long=4, float=9, double=10, string=13, boolean=14
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
    """
    ################################################################
    Obit.InfoListItemResize(inList, name, type, dim)
    # end PItemResize

def PIsA (inList):
    """ Tells if input really is InfoList

    returns true, false (1,0)
    inList    = input Python InfoList
    """
    ################################################################
     # Checks
    if inList.__class__ != InfoList:
        return 0
    return Obit.InfoListIsA(inList.me)
    # end PIsA

def PPutInt (inList, name, dim, data, err):
    """ Add an integer entry, error if conflict

    inList   = input Python InfoList
    name     = name of desired entry
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
               MUST have 5 entries
    data     = data as a 1-D array of integers
    err      = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if data[0].__class__ != int:
        print "class is" , data[0].__class__
        raise TypeError,"data MUST be int"
    if len(dim) < 5:
        raise RuntimeError,"dim has fewer then 5 entries"
    prod = 1
    for x in dim:
        if (x>0):
            prod = prod*x
    if prod < len(data):
         raise RuntimeError,"more data than defined in dim"
    Obit.InfoListPutInt(inList.me, name, dim, data, err.me)
    if err.isErr:
        raise err
    # end PPutInt

def PAlwaysPutInt (inList, name, dim, data):
    """ Add an integer entry, changing type/dim of entry if needed

    inList   = input Python InfoList
    name     = name of desired entry
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
               MUST have 5 entries
    data     = data as a 1-D array of integers
    """
    ################################################################
    # Checks
    if data[0].__class__  != int:
        print "class is" , data[0].__class__
        raise TypeError,"data MUST be int"
    if len(dim) < 5:
        raise RuntimeError,"dim has fewer then 5 entries"
    prod = 1
    for x in dim:
        if (x>0):
            prod = prod*x
    if prod < len(data):
         raise RuntimeError,"more data than defined in dim"
    Obit.InfoListAlwaysPutInt(inList.me, name, dim, data)
    # end PAlwaysPutInt

def PPutLong (inList, name, dim, data, err):
    """ Add an long entry, error if conflict

    inList   = input Python InfoList
    name     = name of desired entry
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
               MUST have 5 entries
    data     = data as a 1-D array of integers
    err      = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if data[0].__class__ != int:
        raise TypeError,"data MUST be int"
    if len(dim) < 5:
        print "class is" , data[0].__class__
        raise RuntimeError,"dim has fewer then 5 entries"
    prod = 1
    for x in dim:
        if (x>0):
            prod = prod*x
    if prod < len(data):
         raise RuntimeError,"more data than defined in dim"
    Obit.InfoListPutLong(inList.me, name, dim, data, err.me)
    if err.isErr:
        raise err
    # end PPutLong

def PAlwaysPutLong (inList, name, dim, data):
    """ Add an long entry, changing type/dim of entry if needed

    inList   = input Python InfoList
    name     = name of desired entry
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
               MUST have 5 entries
    data     = data as a 1-D array of integers
    """
    ################################################################
    # Checks
    if data[0].__class__ != int:
        print "class is" , data[0].__class__
        raise TypeError,"data MUST be int"
    if len(dim) < 5:
        raise RuntimeError,"dim has fewer then 5 entries"
    prod = 1
    for x in dim:
        if (x>0):
            prod = prod*x
    if prod < len(data):
         raise RuntimeError,"more data than defined in dim"
    Obit.InfoListAlwaysPutLong(inList.me, name, dim, data)
    # end PAlwaysPutLong

def PPutFloat (inList, name, dim, data, err):
    """ Add an float entry, error if conflict

    inList   = input Python InfoList
    name     = name of desired entry
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
               MUST have 5 entries
    data     = data as a 1-D array of float
    err      = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if data[0].__class__ != float:
        raise TypeError,"data MUST be float"
    if len(dim) < 5:
        raise RuntimeError,"dim has fewer then 5 entries"
    prod = 1
    for x in dim:
        if (x>0):
            prod = prod*x
    if prod < len(data):
        print "data, size",prod,data
        raise RuntimeError,"more data than defined in dim"
    Obit.InfoListPutFloat(inList.me, name, dim, data, err.me)
    if err.isErr:
        raise err
    # end PPutFloat

def PAlwaysPutFloat (inList, name, dim, data):
    """ Add an float entry, changing type/dim of entry if needed

    inList   = input Python InfoList
    name     = name of desired entry
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
               MUST have 5 entries
    data     = data as a 1-D array of float
    """
    ################################################################
    # Checks
    if data[0].__class__ != float:
        print "class is" , data[0].__class__
        raise TypeError,"data MUST be float"
    if len(dim) < 5:
        raise RuntimeError,"dim has fewer then 5 entries"
    prod = 1
    for x in dim:
        if (x>0):
            prod = prod*x
    if prod < len(data):
         raise RuntimeError,"more data than defined in dim"
    Obit.InfoListAlwaysPutFloat(inList.me, name, dim, data)
    # end PAlwaysPutFloat

def PPutDouble (inList, name, dim, data, err):
    """ Add an double entry, error if conflict

    inList   = input Python InfoList
    name     = name of desired entry
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
               MUST have 5 entries
    data     = data as a 1-D array of double
    err      = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if data[0].__class__ != float:
        print "class is" , data[0].__class__
        raise TypeError,"data MUST be float"
    if len(dim) < 5:
        raise RuntimeError,"dim has fewer then 5 entries"
    prod = 1
    for x in dim:
        if (x>0):
            prod = prod*x
    if prod < len(data):
         raise RuntimeError,"more data than defined in dim"
    Obit.InfoListPutDouble(inList.me, name, dim, data, err.me)
    if err.isErr:
        raise err
    # end  PPutDouble

def PAlwaysPutDouble (inList, name, dim, data):
    """ Add an integer entry, changing type/dim of entry if needed

    inList   = input Python InfoList
    name     = name of desired entry
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
               MUST have 5 entries
    data     = data as a 1-D array of double
    """
    ################################################################
    # Checks
    if data[0].__class__ != float:
        print "class is" , data[0].__class__
        raise TypeError,"data MUST be float"
    if len(dim) < 5:
        raise RuntimeError,"dim has fewer then 5 entries"
    prod = 1
    for x in dim:
        if (x>0):
            prod = prod*x
    if prod < len(data):
         raise RuntimeError,"more data than defined in dim"
    Obit.InfoListAlwaysPutDouble(inList.me, name, dim, data)
    # end PAlwaysPutDouble

def PPutBoolean (inList, name, dim, data, err):
    """ Add an boolean entry (1,0), error if conflict

    inList   = input Python InfoList
    name     = name of desired entry
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
               MUST have 5 entries
    data     = data as a 1-D array of boolean (1,0)
    err      = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if data[0].__class__ != bool:
        print "class is" , data[0].__class__
        raise TypeError,"data MUST be bool"
    if len(dim) < 5:
        raise RuntimeError,"dim has fewer then 5 entries"
    prod = 1
    for x in dim:
        if (x>0):
            prod = prod*x
    if prod < len(data):
         raise RuntimeError,"more data than defined in dim"
    Obit.InfoListPutBoolean(inList.me, name, dim, data, err.me)
    if err.isErr:
        raise err
    # end PPutBoolean

def PAlwaysPutBoolean (inList, name, dim, data):
    """ Add an boolean entry, changing type/dim of entry if needed

    inList   = input Python InfoList
    name     = name of desired entry
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
               MUST have 5 entries
    data     = data as a 1-D array of boolean (1,0)
    """
    ################################################################
    # Checks
    if data[0].__class__ != bool:
        print "class is" , data[0].__class__
        raise TypeError,"data MUST be bool"
    if len(dim) < 5:
        raise RuntimeError,"dim has fewer then 5 entries"
    prod = 1
    for x in dim:
        if (x>0):
            prod = prod*x
    if prod < len(data):
        raise RuntimeError,"more data than defined in dim"
    Obit.InfoListAlwaysPutBoolean(inList.me, name, dim, data)
    # end PAlwaysPutBoolean

def PPutString (inList, name, dim, data, err):
    """ Add an string entry, error if conflict

    inList   = input Python InfoList
    name     = name of desired entry
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
               MUST have 5 entries
    data     = data as a 1-D array of strings (rectangular char array)
    err      = Python Obit Error/message stack
    """
    ################################################################
    # Checks
    if data[0].__class__ != str:
        print "class is" , data[0].__class__
        raise TypeError,"data MUST be a string"
    if len(dim) < 5:
        raise RuntimeError,"dim has fewer then 5 entries"
    prod = 1
    for x in dim:
        if (x>0):
            prod = prod*x
    if prod < len(data):
         raise RuntimeError,"more data than defined in dim"
    # Call depends on whether single string, 1 or 2 D arrays
    if (dim[1]>1):    # array of strings
        Obit.InfoListPutString(inList.me, name, dim, data, err.me)
    else:             # single
        Obit.InfoListPutSString(inList.me, name, dim, data, err.me)
    if err.isErr:
        raise err
    # end  PPutString

def PAlwaysPutString (inList, name, dim, data):
    """ Add an String entry, changing type/dim of entry if needed

    inList   = input Python InfoList
    name     = name of desired entry
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
               MUST have 5 entries
    data     = data as a 1-D array of (rectangular char array)
    """
    ################################################################
    # Checks
    if data[0].__class__ != str:
        print "class is" , data[0].__class__
        raise TypeError,"data MUST be a string"
    if len(dim) < 5:
        raise RuntimeError,"dim has fewer then 5 entries"
    prod = 1
    for x in dim:
        if (x>0):
            prod = prod*x
    if prod < len(data):
        raise RuntimeError,"more data than defined in dim"
    # Call depends on whether single string, 1 or 2 D arrays
    if (dim[1]>1):    # array of strings
        Obit.InfoListAlwaysPutString(inList.me, name, dim, data)
    else:             # single
        Obit.InfoListAlwaysPutSString(inList.me, name, dim, data)
     # end PAlwaysPutString

def PGet (inList, name):
    """ 
    returns list containing data:
       0 - return code, 0=OK else failed
       1 - name
       2 - type
       3 -  dimension array
       4 - data array
    inList    = input Python InfoList
    type     = data type of object
               int=1, oint=3, long=4, float=9, double=10, string=13, boolean=14
    dim      = dimensionality of array as list, e.g. [1,1,1,1,1] for scalar
    data     = data as a 1-D array of (rectangular char array)
    """
    ################################################################
    # Checks
    if not PIsA(inList):
        print "Actually ",inList.__class__
        raise TypeError,'inList MUST be a Python Obit InfoList'
    
    # Dummy third argument
    blob = Obit.makeInfoListBlob()
    return Obit.InfoListGet(inList.me, name, blob)
    # end PGet

def PIsA (inList):
    """ Tells if input really a Python Obit InfoList

    return true, false (1,0)
    inList   = Python InfoList object
    """
    ################################################################
     # Checks
    if inList.__class__ != InfoList:
        return 0
    return Obit.InfoListIsA(inList.me)
    # end PIsA

def PUnref (inList):
    """ Decrement reference count

    Decrement reference count which will destroy object if it goes to zero
    Python object stays defined.
    inList   = Python InfoList object
    """
    ################################################################
     # Checks
    if not PIsA(inList):
        raise TypeError,"inList MUST be a Python Obit InfoList"

    inList.me = Obit.InfoListUnref(inList.me)
    # end PUnref

