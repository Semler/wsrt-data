"""
Utility package for use in ObitTalk

   ObitTalk is derived from the ParselTongue project at JIVE and
provides a scripting and interactive command line interface to
astronomical data and processing software.  In particular, AIPS and
FITS data structures as used in the AIPS and Obit software packages
are supported as well as AIPS tasks and Obit tasks and other python
enabled software.

This utility  package facilitates the access to data and images from
python as well as various interactive features.  The details of the
functions in this package are given later in this help.  Many of
these functions have equivalents in POPS although adapted to python.

   AIPS tasks will use the AIPS XAS TV which must be started separately
(Haven't checked that this works).  Obit tasks and ObitTalk use the
ObitView image display which must also be started independently.

   ObitTalk can start tasks either locally or on a remote machine
which has an ObitTalkServer process running.  Some data access is
supported through the AIPSUVData, AIPSImage, FITSUVData and FITSImage
classes.  Currently other python functions only work locally.

   Both Tasks and more detailed access to and manipulation of data
are available.  These are described breifly below and methods of
obtaining more detailed descriptions are described.

Obit Basics
-----------
   Obit consists of a class libraries and a number of prepackaged
tasks similar to AIPS tasks.  The classes are implemented in c but
there are python bindings to much of the high-level functionality
allowing python scripts a high degree of flexibility in accessing
and manipulating data.
   Obit can support multiple physical data formats as long as they are
uniquely mapable to a common data model.  Above a data access level,
the underlying physical data representation is (mostly) hidden.
Currently, AIPS and FITS (as practiced by AIPS) are supported.
Only FITS format OTF data is supported.
   Control parameters to Obit routines are largely passed in an
InfoList structure (a type of associative array) but many of the
python interface routines take care of this detail and their
parameters are passed through a python dictionary.

Tasks:
-----------

AIPS tasks:
All AIPS tasks

ObitTasks:
AutoFlag  Radio interferometry data editing software
Feather   Task to Feather together images
HGeom     Task to make an image consistent with another image
Imager    Radio interferometry imaging task
Squish    Compress image cube along third axis
SubImage  Task to copy a sub region of an image
Template  Task to print the mean, rms and extrema in an image
UVSub     Task to subtract a clean model from a uv data base


   To see task documentation either a python task object may first
be created is its documentation viewed or more directly:
AIPSHelp("AIPS_task_name)
or
ObitHelp("Obit_task_name)

   To create a task object:
>>> im=AIPSTask("IMEAN")
to create an AIPS task object for task IMEAN, or

>>> fe=ObitTask("Feather")
to create an Obit Task object for Feather
Note the names of the objects are arbitrary.

Task parameters can be set using the form object.parameter=value:
>>> im.inname="MY FILE"
where the parameter names are subject to minimum match.
Array values are given in square brackest "[  ]", the usual form
for a python list.  AIPS array values are indexed 1-relative and Obit
arrays 0-relative but this is largely transparent.
Note: unlike POPS, ALL strings are case sensitive.

Task parameters can be reviewed using the inputs() function:
>>> im.inputs()
or
>>> inputs(im)
Note: there is NO minimum match on functions and you must give
the parentheses.

POPS style help can be viewed:
>>> im.help()
or
>>> help(im)

or EXPLAIN (if available) by:
>>> im.explain()
or
>>> explain(im)

Tasks can be run using the go function:
>>> log=im.go()
The go function currently runs synchronously and does not return
until the task finishes.  The go function returns a list of the
task messages which in the above example are stored in the variable
log as a list of strings.

After as task is run which generates output values, these can be
viewed using the outputs function:
>> im.outputs()
and the values can be accessed through the task parameter.

The task functions work for both AIPS and Obit tasks.

ObitTalk DATA Classes
---------------------
   The ObitTalk classes  AIPSUVData, AIPSImage, FITSUVData and
FITSImage allow local or remote access to AIPS and FITS Images
and UV data.  Details of these classes interfaces can be viewed
using:
>>> help(AIPSUVData)
>>> help(AIPSImage)
>>> help(FITSUVData)
>>> help(FITSImage)

Obit classes and utility packages with python interfaces:
---------------------------------------------------------
   There are a number of Obit functions with high level python
interfaces.  To see more details import and view the help for each:

>>> import History
>>> help(History)
   

Obit/AIPS/Radio Interferometry/Image classes and utilities
AIPSDir        AIPS directory class
CArray         Complex array class
CleanImage     Image CLEAN
CleanVis       Visibility based CLEAN
FArray         float array class
FeatherUtil    Image feathering utilities
FFT            Fast Fourier Transform class
FInterpolate   Float array interpolator
History        History class
ImageDesc      Image Descriptor (header)
ImageMosaic    Image Mosaic class
Image          Image class
ImageUtil      Image utilities
InfoList       Obit associative array for control info
MosaicUtil     Image mosaicing utilities
ODisplay       Interface to ObitView display
OErr           Obit message/error class
OSystem        Obit System class
OWindow        (CLEAN) image window class
SkyModel       Sky model class
TableDesc      Table descriptor (header) class
TableList      Table list for data object (Image, UVData, OTF)
Table          Table class
UVDesc         UV data descriptor (header)
UVImager       UV data imager class
UV             UV data class
UVSelfCal      UV Self calibration class

               Single dish/OTF imaging classes and utilities
CleanOTF       Single dish (Hogbom) CLEAN
FArrayUtil     FArray utilities
GBTDCROTF      Convert GBD DCR data to OTF format
OTFDesc        OTF Descriptor
OTFGetAtmCor   OTF Atmospheric correction utilities
OTFGetSoln     OTF calibration solution utilities
OTF            OTF ("On the Fly) data
OTFSoln2Cal    Utilities to convert OTF solutiion to calibration tables
OTFUtil        OTF Utilities

Obit Python Example

   Accessing and manipulating data through Obit requires an initialization
of the Obit System which is done when this (OTObit) module is loaded into
python so is not described here.
   The following example reads an AIPS image and writes a integerized FITS
image with the pixel values truncated at a set fraction of the RMS "noise"
in the image.  This operation creates an image which is more compressible
but with a controlled loss of precision.

# Specify input and output
inDisk   = 1
Aname    = "INPUT IMAGE"
Aclass   = "CLASS"
Aseq     = 1
outDisk  = 1
outFile  = "Quantized.fits"

# Create Images
inImage   = Image.newPAImage("Input image", Aname, Aclass, inDisk, Aseq, True, err)
# Note: inImage can also be created using getname(cno,disk)
outImage  = Image.newPFImage("Output image",   outFile,  outDisk,  False, err)
Image.PClone(inImage, outImage, err)   # Same structure etc.
OErr.printErrMsg(err, "Error initializing")

# Fraction of RMS if given
fract = 0.25

# Copy to quantized integer image with history
inHistory  = History.History("history", inImage.List, err)
Image.PCopyQuantizeFITS (inImage, outImage, err, fract=fract, inHistory=inHistory)
OErr.printErrMsg(err, "Writing to FITS")



"""
# Interactive routines to Obit use from ObitTalk
# $Id: OTObit.py,v 1.4 2005/10/10 15:15:03 bcotton Exp $
#-----------------------------------------------------------------------
#  Copyright (C) 2005
#  Associated Universities, Inc. Washington DC, USA.
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,
#  MA 02139, USA.
#
#  Correspondence concerning this software should be addressed as follows:
#         Internet email: bcotton@nrao.edu.
#         Postal address: William Cotton
#                         National Radio Astronomy Observatory
#                         520 Edgemont Road
#                         Charlottesville, VA 22903-2475 USA
#-----------------------------------------------------------------------
import Obit, Table, FArray, OErr, InfoList, History, AIPSDir, OSystem
import Image, ImageDesc, TableList, ODisplay, UV, OWindow
import TaskWindow
import os, AIPS, pickle
# ObitTalk classes
import AIPSData, FITSData, AIPSTask, ObitTask
from AIPSTask import AIPSTask
from ObitTask import ObitTask
# from OTObit import *
# ObitStart()

#global Adisk

err=OErr.OErr()
ObitSys=None
Adisk = 1

# Display connection
disp = ODisplay.ODisplay("ObitView", "ObitView", err)

#Initialize Obit system
# Get list of FITS disks
FITSdisks = []
for dsk in ["FITS","FITS01","FITS02","FITS03","FITS04","FITS05","FITS06"]:
    dir = os.getenv(dsk)
    if dir:
        FITSdisks.append(dir)
        nFITS = len(FITSdisks)
        
# Get list of AIPS disks
AIPSdisks = []
for dsk in ["DA01","DA02","DA03","DA04","DA05","DA06","DA07","DA08","DA09","DA10"]:
    dir = os.getenv(dsk)
    if dir:
        AIPSdisks.append(dir)
        nAIPS = len(AIPSdisks)
        
# Init Obit
userno =  AIPS.AIPS.userno
popsno = 1
ObitSys=OSystem.OSystem ("Interactive", popsno, userno, nAIPS, AIPSdisks, \
                         nFITS, FITSdisks, True, False, err)
OErr.printErrMsg(err, "Error with Obit startup")

def ShowErr(err=err):
    """ Print any errors and clear stack
    
    err  = Python Obit Error/message stack, default of OTObit version
    """
    ################################################################
    OErr.printErrMsg(err, "Error")
    # end ShowErr

def ClearErr(err=err):
    """ Print any errors and clear stack
    
    err  = Python Obit Error/message stack, default of OTObit version
    """
    ################################################################
    OErr.printErrMsg(err, "Error")
    # end ClearErr

def Acat(disk=Adisk, first=1, last=1000):
    """ Catalog listing of AIPS files on disk disk

    The class remembers the last disk accessed
    disk      = AIPS disk number to list
    first     = lowest slot number to list
    last      =highest slot number to list
    """
    ################################################################
    Adisk = disk
    AIPSDir.PListDir(disk, err, first=first, last=last)
    OErr.printErrMsg(err, "Error with AIPS catalog")
    # end Acat

def AMcat(disk=Adisk, first=1, last=1000):
    """ Catalog listing of AIPS Image files on disk disk

    disk      = AIPS disk number to list
    first     = lowest slot number to list
    last      =highest slot number to list
    """
    ################################################################
    Adisk = disk
    AIPSDir.PListDir(disk, err, type=AIPSDir.MAType, first=first, last=last)
    OErr.printErrMsg(err, "Error with AIPS catalog")
    # end AMcat

def AUcat(disk=Adisk, first=1, last=1000):
    """ Catalog listing of AIPS UV data files on disk disk

    disk      = AIPS disk number to list
    first     = lowest slot number to list
    last      = highest slot number to list
    """
    ################################################################
    Adisk = disk
    AIPSDir.PListDir(disk, err, type=AIPSDir.UVType, first=first, last=last)
    OErr.printErrMsg(err, "Error with AIPS catalog")
    # end AUcat

def getname(cno, disk=Adisk):
    """ Return Obit object for AIPS file in cno on disk

    cno       = AIPS catalog slot number 
    disk      = AIPS disk number
    """
    ################################################################
    Adisk = disk
    user =  AIPS.AIPS.userno
    s = AIPSDir.PInfo(disk, user, cno, err)
    OErr.printErrMsg(err, "Error with AIPS catalog")
    # parse returned string
    Aname = s[0:12]
    Aclass = s[13:19]
    Aseq = int(s[20:25])
    Atype = s[26:28]
    if Atype == 'MA':
        out = Image.newPAImage("AIPS image", Aname, Aclass, disk, Aseq, True, err)
        print "AIPS Image",Aname, Aclass, disk, Aseq
    elif Atype == 'UV':
        out = UV.newPAUV("AIPS UV data", Aname, Aclass, disk, Aseq, True, err)
        print "AIPS UV",Aname, Aclass, disk, Aseq
    out.Aname  = Aname
    out.Aclass = Aclass
    out.Aseq   = Aseq 
    out.Atype  = Atype
    out.Disk   = disk
    out.Acno   = cno
    return out
    # end getname

def getFITS(file, disk=Adisk, Ftype='Image'):
    """ Return Obit object for FITS file in file on disk

    file      = FITS file name
    disk      = FITS disk number
    Ftype     = FITS data type: 'Image', 'UV'
    """
    ################################################################
    if Ftype == 'Image':
        out = Image.newPFImage("FITS image", file, disk, True, err)
    elif Ftype == 'UV':
        out = UV.newPFUV("FITS UV data", file, disk, True, err)
    out.Fname  = file
    out.Disk   = disk 
    out.Otype  = Ftype
    return out
    # end getFITS

def tvlod(image, window=None):
    """ display image

    image  = Obit Image, created with getname, getFITS
    window = Optional window for image to edit
    """
    ################################################################
    if Image.PIsA(image):
        # Obit/Image
        ODisplay.PImage(disp, image, err, window=window)
    elif image.__class__==AIPSData.AIPSImage:
        # AIPS Image
        tmp = Image.newPAImage("AIPS Image",image.name, image.klass, image.disk, \
                               image.seq, True, err)
        ODisplay.PImage(disp, tmp, err, window=window)
        del tmp
    elif image.__class__==FITSData.FITSImage:
        # FITS Image
        tmp = Image.newPFImage("FITS Image",image.filename, image.disk, True, err)
        ODisplay.PImage(disp, tmp, err, window=window)
        del tmp
    # end tvlod

def window (image):
    """ Make a window object for an image

    Returns OWindow object
    image  = Obit image object
    """
    ################################################################
    if Image.PIsA(image):
        # Obit/Image
        naxis = image.Desc.Dict["inaxes"][0:2]
    elif image.__class__==AIPSData.AIPSImage:
        # AIPS Image
        tmp = Image.newPAImage("AIPS Image",image.name, image.klass, image.disk, \
                               image.seq, True, err)
        naxis = tmp.Desc.Dict["inaxes"][0:2]
        del tmp
    elif image.__class__==FITSData.FITSImage:
        # FITS Image
        tmp = Image.newPFImage("FITS Image",image.filename, image.disk, True, err)
        naxis = tmp.Desc.Dict["inaxes"][0:2]
        del tmp
    return OWindow.PCreate1("Window", naxis, err)
    # end window

def go (TaskObj):
    """ Execute task

    Returns TaskWindow object if run asynchronously (doWait=True)
    or the task message log if run synchronously (doWait=False)
    The wait() function on the TaskWindow will hang until the task finishes
    TaskObj    = Task object to execute
                 If doWait member is true run synchronously,
                 else run with messages in a separate Message window
    """
    ################################################################
    if TaskObj.doWait:
        return TaskObj.go()
    else:
        tw = TaskWindow.TaskWindow(TaskObj)
        tw.start()
        return tw
    # end go
   
def inputs (TaskObj):
    """ List task inputs

    TaskObj    = Task object whose inputs to list
    """
    ################################################################
    TaskObj.inputs()
    # end inputs
   
def explain (TaskObj):
    """ Give explanation for a task if available

    TaskObj    = Task object whose inputs to list
    """
    ################################################################
    TaskObj.explain()
    # end explain
   
def AIPSHelp (Task):
    """ Give Help for AIPS task Task

    Task    = AIPSTask name to give (e.g. "IMEAN")
    """
    ################################################################
    t=AIPSTask(Task)
    t.help()
    # end  AIPSHelp
   
def ObitHelp (Task):
    """ Give Help for OBIT task Task

    Task    = ObitTask name to give (e.g. "Feather")
    """
    ################################################################
    t=ObitTask(Task)
    t.help()
    # end  ObitHelp
   
def imhead (ObitObj):
    """ List header

    ObitObj    = Obit or ObitTalk data object
    """
    ################################################################
    if ObitObj.__class__==AIPSData.AIPSImage:
        # AIPS Image
        tmp = Image.newPAImage("AIPS Image",ObitObj.name, ObitObj.klass, ObitObj.disk, \
                               ObitObj.seq, True, err)
        tmp.Header(err)
        del tmp
    elif ObitObj.__class__==FITSData.FITSImage:
        # FITS Image
        tmp = Image.newPFImage("FITS Image",ObitObj.filename, ObitObj.disk, True, err)
        tmp.Header(err)
        del tmp
    elif ObitObj.__class__==AIPSData.AIPSUVData:
        # AIPS UVData
        tmp = UV.newPAImage("AIPS UVData",ObitObj.name, ObitObj.klass, ObitObj.disk, \
                               ObitObj.seq, True, err)
        tmp.Header(err)
        del tmp
    elif ObitObj.__class__==FITSData.FITSUVData:
        # FITS UVData
        tmp = UV.newPFImage("FITS UVData",ObitObj.filename, ObitObj.disk, True, err)
        tmp.Header(err)
        del tmp
    else:
        # Presume it's an Obit object
        ObitObj.Header(err)
    # end imhead
   
def setname (inn, out):
    """ Copy file definition from inn to out as in...

    Supports both FITS and AIPS
    Copies Data type and file name, disk, class etc
    inn  = Obit data object, created with getname, getFITS
    out  = ObitTask object,
    """
    ################################################################
    # AIPS or Obit?
    if out.__class__ == ObitTask:
        out.DataType = inn.FileType
        out.inDisk   = inn.Disk
        if inn.FileType == 'FITS':
            out.inFile = inn.Fname
        else:   # AIPS
            out.inName  = inn.Aname
            out.inClass = inn.Aclass
            out.inSeq   = inn.Aseq
    else:  # AIPS
        out.inname  = inn.Aname
        out.inclass = inn.Aclass
        out.inseq   = inn.Aseq
        out.indisk  = inn.Disk
    # end setname
   
def set2name (in2, out):
    """ Copy file definition from in2 to out as in2...

    Supports both FITS and AIPS
    Copies Data type and file name, disk, class etc
    in2  = Obit data object, created with getname, getFITS
    out  = ObitTask object,
    """
    ################################################################
    # AIPS or Obit?
    if out.__class__ == ObitTask:
        out.DataType  = in2.FileType
        out.in2Disk   = in2.Disk
        if in2.FileType == 'FITS':
            out.in2File = in2.Fname
        else:   # AIPS
            out.in2Name  = in2.Aname
            out.in2Class = in2.Aclass
            out.in2Seq   = in2.Aseq
    else: # AIPS
        out.in2name  = inn.Aname
        out.in2class = inn.Aclass
        out.in2seq   = inn.Aseq
        out.in2disk  = inn.Disk
    # end set2name
   
def setoname (inn, out):
    """ Copy file definition from inn to out as outdisk...

    Supports both FITS and AIPS
    Copies Data type and file name, disk, class etc
    inn  = Obit data object, created with getname, getFITS
    out  = ObitTask object,
    """
    ################################################################
    # AIPS or Obit?
    if out.__class__ == ObitTask:
        out.DataType  = inn.FileType
        out.outDisk   = inn.Disk
        if inn.FileType == 'FITS':
            out.outFile = inn.Fname
        else:   # AIPS
            out.outName  = inn.Aname
            out.outClass = inn.Aclass
            out.outSeq   = inn.Aseq
    else:  # AIPS
        out.outname  = inn.Aname
        out.outclass = inn.Aclass
        out.outseq   = inn.Aseq
        out.outdisk  = inn.Disk
    # end setoname
   
def setwindow (w, out):
    """ Set BLC and TRC members on out from OWindow w

    Uses first window in first field on w which must be a rectangle
    This may be set interactively using tvlod
    w    = OWindow object
    out  = ObitTask object, BLC and TRC members [0] and [1] are modified
    """
    ################################################################
    # Must be rectangle
    l =  OWindow.PGetList(w, 1, err)
    if l[0][1] !=0:
        raise TypeError,"Window MUST be a rectangle"
    # AIPS or Obit?
    if out.__class__ == ObitTask:
        out.BLC[0] = l[0][2]+1  # make 1-rel
        out.BLC[1] = l[0][3]+1  
        out.TRC[0] = l[0][4]+1  
        out.TRC[1] = l[0][5]+1
    else:  # AIPS
        out.blc[1] = l[0][2]+1  # make 1-rel
        out.blc[2] = l[0][3]+1  
        out.trc[1] = l[0][4]+1  
        out.trc[2] = l[0][5]+1
       
    # end setwindow 
   
def zap (o):
    """ Zap object o

    Removes all external components (files)
    o    = Obit Data object to delete
    """
    ################################################################
    o.Zap(err)
    # end zap
   
def tput (to, file=None):
    """ save task object

    save values in task object
    to    = task object to save
    file  = optional file name, the default is <task_name>.pickle
            in the current working directory
    """
    ################################################################
    saveit = {}
    for adverb in to._input_list:
        value = to._retype(to.__dict__[adverb])
        saveit[adverb] = value

    # Save type
    saveit["TaskType"] = to.__class__
    
    tfile = file
    if file==None:
        tfile = to._name+".pickle"
    fd = open(tfile, "w")
    pickle.dump(saveit, fd)
    fd.close()
    # end tput
   
def tget (inn, file=None):
    """ Restore task object from disk

    Restore values in task object
    inn   = task name, or a task object of the desired type
            in the latter case, the input object will NOT be modified
    file  = optional file name, the default is <task_name>.pickle
            in the current working directory
    """
    ################################################################
    # Get name
    if type(inn)==str:
        name = inn
    else:
        # it better be a task object
        name = inn._name

    # unpickle file
    tfile = file
    if file==None:
        tfile = name+".pickle"
    fd = open(tfile, "r")
    saveit = pickle.load(fd)
    fd.close()

    myType = saveit["TaskType"]
    if myType == AIPSTask:
        to = AIPSTask(name)
    else:
        to = ObitTask(name)
    for adverb in saveit:
        if adverb == "TaskType":
            pass
        else:
            to.__dict__[adverb] = saveit[adverb]

    return to
    # end tget
   
def imstat (inImage):
    """ Set region in an image using the display and tell mean, rms

    Returns dictionary with statistics of selected region with entries:
        Mean    = Mean value
        RMS     = RMS value
        Max     = maximum value
        MaxPos  = pixel of maximum value
        Min     = minimum value
        MinPos  = pixel of minimum value
    inImage   = Python Image object, created with getname, getFITS
    """
    ################################################################
    # Get window
    w = window(inImage)
    tvlod(inImage,w)
    ShowErr()
    # Must be rectangle
    l =  OWindow.PGetList(w, 1, err)
    if l[0][1] !=0:
        raise TypeError,"Window MUST be a single rectangle"
    blc = [min(l[0][2]+1, l[0][4]+1), min(l[0][3]+1, l[0][5]+1)]
    trc = [max(l[0][2]+1, l[0][4]+1), max(l[0][3]+1, l[0][5]+1)]
    
    # Read plane
    p=Image.PReadPlane(inImage,err,blc=blc,trc=trc)
    ShowErr()

    # Get statistics
    Mean = p.Mean
    RMS  = p.RMS
    MaxPos=[0,0]
    Max = FArray.PMax(p, MaxPos)
    MaxPos[0] = MaxPos[0]+blc[0]
    MaxPos[1] = MaxPos[1]+blc[1]
    MinPos=[0,0]
    Min = FArray.PMin(p, MinPos)
    MinPos[0] = MinPos[0]+blc[0]
    MinPos[1] = MinPos[1]+blc[1]
    print "Region Mean %f, RMS %f" % (Mean, RMS)
    print "  Max %f @ pixel " % Max, MaxPos
    print "  Min %f @ pixel " % Min, MinPos
    
    # Reset BLC, TRC
    blc = [1,1,1,1,1]
    trc = [0,0,0,0,0]
    Image.POpen(inImage, Image.READONLY, err, blc=blc, trc=trc)
    Image.PClose (inImage, err)
    ShowErr()
    
    del w, p, blc, trc
    return {"Mean":Mean,"RMS":RMS,"Max":Max,"MaxPos":MaxPos,"Min":Min,"MinPos":MinPos}
    # end imstat
   
