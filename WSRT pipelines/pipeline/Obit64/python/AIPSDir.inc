/* $Id: AIPSDir.inc,v 1.3 2005/08/28 01:40:05 bcotton Exp $                            */  
/*--------------------------------------------------------------------*/
/* Swig module description for AIPS directory utilities               */
/*                                                                    */
/*;  Copyright (C) 2004                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/

%{
#include "ObitAIPSDir.h"
%}



%inline %{
extern int AIPSDirFindCNO(int disk, int user,  char *Aname, char *Aclass, 
    		          char *Atype, int seq, ObitErr *err)
{
  gchar LAname[13], LAclass[7], LAtype[3];
  gint i, l;

  /* Init AIPS fixed strings - blank fill until end */
  for (i=0; i<12; i++) LAname[i]  = ' ';LAname[i]  = 0;
  for (i=0; i<6; i++)  LAclass[i] = ' ';LAclass[i] = 0;
  for (i=0; i<2; i++)  LAtype[i]  = ' ';LAtype[i]  = 0;

  /* Copy string input into AIPS fixed strings */
  l = MIN (12, strlen(Aname));
  for (i=0; i<l; i++) LAname[i]  = Aname[i];
  l = MIN (6, strlen(Aclass));
  for (i=0; i<l; i++)  LAclass[i] = Aclass[i];
  l = MIN (2, strlen(Atype));
  for (i=0; i<l; i++)  LAtype[i] = Atype[i];

  return ObitAIPSDirFindCNO(disk, user, LAname, LAclass, LAtype, seq, err);
} /* end AIPSDirFindCNO */


extern int AIPSDirAlloc(int disk, int user,  char *Aname, char *Aclass, 
		        char *Atype, int seq, ObitErr *err)
{
  gchar LAname[13], LAclass[7], LAtype[3];
  gboolean exist;
  gint i, l;

  /* Init AIPS fixed strings - blank fill until end */
  for (i=0; i<12; i++) LAname[i]  = ' ';LAname[i]  = 0;
  for (i=0; i<6; i++)  LAclass[i] = ' ';LAclass[i] = 0;
  for (i=0; i<2; i++)  LAtype[i]  = ' ';LAtype[i]  = 0;

  /* Copy string input into AIPS fixed strings */
  l = MIN (12, strlen(Aname));
  for (i=0; i<l; i++) LAname[i]  = Aname[i];
  l = MIN (6, strlen(Aclass));
  for (i=0; i<l; i++)  LAclass[i] = Aclass[i];
  l = MIN (2, strlen(Atype));
  for (i=0; i<l; i++)  LAtype[i] = Atype[i];

  return ObitAIPSDirAlloc(disk, user, LAname, LAclass, LAtype, seq, &exist, err);
} /* end AIPSDirAlloc */

extern void AIPSDirRemoveEntry(int disk, int user, int cno, ObitErr *err)
{
  ObitAIPSDirRemoveEntry(disk, user, cno, err);
} /* end  AIPSDirRemoveEntry */

extern int AIPSDirNumber(int disk, int user, ObitErr *err)
{
  return (int)ObitAIPSDirNumber((gint)disk, (gint)user, err);
} /* end AIPSDirNumber */

/* Cleanup output with AIPSDirInfoClean */
/* Returns NULL if no entry */
extern char* AIPSDirInfo(int disk, int user, int cno, ObitErr *err)
{
  ObitAIPSDirCatEntry *entry;
  gchar *out=NULL;
  gint nout=55, i;

  entry = ObitAIPSDirGetEntry ((gint)disk, (gint)user, (gint)cno, err);
  if (err->error) return out;
  if (entry->user!=user) {
      g_free (entry);
      return out;
  }

  out = g_malloc(nout);
  g_snprintf (out, nout, "%-12.12s.%-6.6s. %4d %-2.2s            ",
              entry->name, entry->class, entry->seq, entry->type);
  ObitAIPSDirGetAccess (entry, &out[29]);
  g_free (entry);
  return (char*)out;
} /* end AIPSDirInfo */

/* Deallocate the result of AIPSDirInfoClean */
extern void AIPSDirInfoClean(char *in)
{
  if (in) g_free(in);
} /* end AIPSDirInfoClean */
%}
