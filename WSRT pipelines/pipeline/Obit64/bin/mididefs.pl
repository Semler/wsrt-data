# This script parses the LaTeX documentation file and produces
# definition files for the FITS binary tables described.
# Output is files containg c code fragments to be included in software.
# Any previously existing version of XXX.def are renamed XXX.backup.
#
# Keys on the following macroes:
# tablename: Argument is table type, if this is 'XXX', the definition
#            file will be named XXX.def
# tablecolindex: This defines zero or more symbols to be used in keyword names
#                (first arg which should be lower case)  and the label for
#                the relevant data column (second argument).  The 1-rel
#                column number of the specified column is determined and this value
#                is substituted for the first argument in any keyword names.
# tablekey:  One per application keyword, arguments are
#             1: name
#             2: type 'I'=integer, 'E'=real, 'D'=double, 'A'=character
#             3: Description string
# 
# tablecol:  One per table column, arguments are
#             1: name
#             2: units
#             3: datatype (character)
#                I=16 bit integer, 
#                J=32 bit integer, 
#                E=real, 
#                D=double, 
#                C=Complex (32 bit each), 
#                M=double complex (64 bit each), 
#                A=character, 
#                L=logical, 
#                X=bit array, 
#                B=byte array, 
#             4: dimension string
#             5: description

require 5;

# mention subroutines
#sub WriteDescriptor;  # write descriptor file
#sub GetNextArg;       # parse next argument

#------------- Subroutines used ------------------------
# Get next argument from $line reading from SRC if necessary
sub GetNextArgument {
  local $lvalue, $start, $stop;
  $lvalue = $line;
# trim line to next '{'
  $start = index ($line, "{");
  # See if need to try next line
  while ($start<0) {
    $line = <SRC>;
    $start = index ($line, "{");
  }
  $start = $start+1;
  $stop = index ($line, "}");
  $lvalue = substr ($line, $start, $stop-$start);
  # remove any '\'
  $lvalue =~ s/\x5c//g;
  #remove leading and trailing blanks
  $lvalue =~ s/^ +//;
  $lvalue =~ s/ +$//;
  # remove from $line
  $line = substr ($line, $stop+1);
  return $lvalue;
} # end GetNextArgument

#Get FITS type code
sub GetTypeCode {
  local $ltype;
  local $larg = $_[0];
  if ($larg eq 'I') {$ltype = 'TSHORT';}
  elsif ($larg eq 'J') {$ltype = 'TLONG';}
  elsif ($larg eq 'K') {$ltype = 'TINT';}
  elsif ($larg eq 'E') {$ltype = 'TFLOAT';}
  elsif ($larg eq 'D') {$ltype = 'TDOUBLE';}
  elsif ($larg eq 'A') {$ltype = 'TSTRING';}
  elsif ($larg eq 'L') {$ltype = 'TLOGICAL';}
  elsif ($larg eq 'C') {$ltype = 'TCOMPLEX';}
  elsif ($larg eq 'M') {$ltype = 'TDBLCOMPLEX';}
  elsif ($larg eq 'X') {$ltype = 'TBIT';}
  elsif ($larg eq 'B') {$ltype = 'TBYTE';}
  else {$ltype = 'TDBLCOMPLEX';} # probably should be fixed in documentation
  return $ltype;
} # end GetTypeCode

#Get FITS column element count from dimension string
sub GetColumnCount {
  local $lcount;
  local $larg = $_[0];
  # parse dimension string
  ($f1,$f2,$f3,$f4,$f5) = split(',',  substr($larg, 1));
#        printf (" dim %s %d %d %d %d %d \n",$larg, $f1, $f2, $f3, $f4, $f5);
  if ($f1 <= 1) {$f1 = 1};
  if ($f2 <= 1) {$f2 = 1};
  if ($f3 <= 1) {$f3 = 1};
  if ($f4 <= 1) {$f4 = 1};
  if ($f5 <= 1) {$f5 = 1};
#        printf (" dim %d %d %d %d %d \n",$f1, $f2, $f3, $f4, $f5);
  $lcount = $f1 * $f2 * $f3 * $f4 * $f5;
  return $lcount;
} # end GetTypeCode

# Any adjustment of keywords for indexed symbols
sub AdjustKeywords {
  local $i, $j, $icol;
  #print ("AdjustKeywords NumIndex =",$NumIndex,".\n");
  if ($NumIndex<=0) {return;}  # only if something to do
  $icol = -1;
  for ($i=0; $i<$NumIndex; $i++) {
    for ($j=0; $j<$NumCol; $j++) {
      if ($ColName[$j]=~$IndexCol[$i]) {
	$icol = $j+1;  # 1-rel column number for FITS
	break;
      }
    } # end loop over columns
    #print ("AdjustKeywords Column ",$IndexCol[$i]," is ",$icol,".\n");
    # adjust any keywords
    if ($icol>0) {
      for ($j=0; $j<$NumKey; $j++) {
	#print ($KeyWord[$j], " ",$IndexSymb[$i], " ",$icol," to ");
	$KeyWord[$j] =~ s/$IndexSymb[$i]/$icol/;
	#print ($KeyWord[$j]," \n");
      } # end loop adjusting keywords
    } # End make adjustment
  } # end loop over indexed symbols
} # end adjust keywords

# routine to write descriptor file
sub WriteDescriptor {
  if ($Extname ne "Uninitialized") {
    # Any adjustment of keywords for index symbols
    AdjustKeywords;
    #print ("WriteDescriptor ",$Extname,".\n");
    # rename old version as backup
    # to be run from src subdirectory
    $filename = "../include/$Extname.def";
    $backupname = "../include/$Extname.backup";
    if (-f $filename) {
      rename ($filename, $backupname);
    }
    open (OUT, ">$filename") || 
      die "Cannot open $filename\n";
    printf (OUT "/*----------------------------------------------------------------------- */\n");
    printf (OUT "/* Definition of $Extname table                                           */\n");
    printf (OUT "/* This file was generated by mididefs.pl from  $DocFile        */\n");
    printf (OUT "/*------------------------------------------------------------------------*/\n");
    printf (OUT "/* types defined in #include \"fitsio.h\" */\n");
    printf (OUT "    \n");
    printf (OUT "/* table type */\n");
    printf (OUT "char* extname=\"$Extname\";\n");
    printf (OUT " \n");

    # Do Keywords names
    printf (OUT "/* Define the keywords to be used */\n");
    printf (OUT "int nkey = %d;       /* number of keywords */\n", $NumKey);
    printf (OUT "char* keywords[] = { /* Table keywords */\n");
    # print keyword values
    for ($i=0; $i<$NumKey-1; $i++) {
      printf (OUT "         \"%s\",\n",$KeyWord[$i]);
    } # end loop printing keywords names
    printf (OUT "         \"%s\"\n",$KeyWord[$NumKey-1]); # final with no trailing comma
    printf (OUT "};\n");

    # print keyword types
    printf (OUT "int keytype[] = {   /* data types of the keywords */\n");
    for ($i=0; $i<$NumKey-1; $i++) {
      $type = GetTypeCode($KeyType[$i]);  # Keytype
      printf (OUT "         %s,\n",$type);
    } # end loop printing keywords data types
      $type = GetTypeCode($KeyType[$NumKey-1]);  # Keytype
    printf (OUT "         %s\n",$type); # final with no trailing comma
    printf (OUT "};\n");

    # print keyword descriptions
    printf (OUT "char* keycom[] = {   /* Description of the keywords */\n");
    for ($i=0; $i<$NumKey-1; $i++) {
      printf (OUT "         \"%s\",\n",$KeyComm[$i]);
    } # end loop printing keywords descriptions
    printf (OUT "         \"%s\"\n",$KeyComm[$NumKey-1]); # final with no trailing comma
    printf (OUT "};\n");
    
    # Do Columns 
    printf (OUT "/* Define the  columns in the table */\n");
    printf (OUT "int ncol = %d;       /* number of columns */\n", $NumCol);
    printf (OUT "char* coltype[] = { /*  Column labels */\n");
    # print column labels
    for ($i=0; $i<$NumCol-1; $i++) {
      printf (OUT "         \"%s\",\n",$ColName[$i]);
    } # end loop printing column labels
    printf (OUT "         \"%s\"\n",$ColName[$NumCol-1]); # final with no trailing comma
    printf (OUT "};\n");

    # print column data types
    printf (OUT "int coldatatype[] = {   /* data types of the keywords */\n");
    for ($i=0; $i<$NumCol-1; $i++) {
      $type = GetTypeCode($ColType[$i]);  # Colum datatype
      printf (OUT "         %s,\n",$type);
    } # end loop printing column data types
    $type = GetTypeCode($ColType[$$NumCol-1]);  # Colum datatype
    printf (OUT "         %s\n",$type); # final with no trailing comma
    printf (OUT "};\n");

    # print units
    printf (OUT "char* colunits[] = {   /*  Units for columns */\n");
    for ($i=0; $i<$NumCol-1; $i++) {
      printf (OUT "         \"%s\",\n",$ColUnit[$i]);
    } # end loop printing units
    printf (OUT "         \"%s\"\n",$ColUnit[$NumCol-1]); # final with no trailing comma
    printf (OUT "};\n");
    
    # print element count, derive from dimensionality
    printf (OUT "long colcount[] = {   /*  Number of elements (as 1D) in the columns */\n");
    for ($i=0; $i<$NumCol-1; $i++) {
      $count = GetColumnCount($ColDim[$i]);  # parse dimension string
      printf (OUT "         %d,\n",$count);
    } # end loop printing units
      $count = GetColumnCount($ColDim[$NumCol-1]);  # parse dimension string
    printf (OUT "         %s\n",$count); # final with no trailing comma
    printf (OUT "};\n");
    
    # print dimensionality
    printf (OUT "char* coldim[] = {   /*  Dimensionality if 2D or higher */\n");
    # use blank if 1D
    for ($i=0; $i<$NumCol-1; $i++) {
      $dim = " ";
      if ($ColDim[$i]=~/\,/) {$dim = $ColDim[$i]};
      printf (OUT "         \"%s\",\n",$dim);
    } # end loop printing dimensionality
    $dim = " ";
    if ($ColDim[$NumCol-1]=~/\,/) {$dim = $ColDim[$NumCol-1]};
    printf (OUT "         \"%s\"\n",$dim); # final with no trailing comma
    printf (OUT "};\n");
    
    close OUT;
  } # end of write file section

  # initialize descriptor information
  $Extname = "Uninitialized"; # No longer any information
  $NumKey = 0;     # Number of keywords
  $NumCol = 0;     # Number of columns
  $NumIndex = 0;   # Number of keyword column indices.
  @KeyWord = 0;    # Keyword names
  @KeyType = 0;    # Keyword types
  @KeyComm = 0;    # Keyword descriptions
  @ColName = 0;    # Column titles
  @ColUnit = 0;    # Column physical units
  @ColType = 0;    # Column data type code
  @ColDim  = 0;    # Column dimensionality
  @IndexSymb=0;    # Column index symbol array
  @IndexCol =0;    # Column label value array
} # end WriteDescriptor

# begin main block
{

# initialize descriptor information
$Extname = "Uninitialized";

# set default documentation file
$DocFile = "mididoc.tex";

# use first argument if it exists as input file name
#printf ("number of args $#ARGV $ARGV[0]\n");
if ($#ARGV>=0) {$DocFile=$ARGV[0]};

#printf ("reading file %s\n", $DocFile); # debug

# open input file
if (!open (SRC, "<$DocFile")) {
    printf("Cannot open input tex file %s\n", $DocFile);
    goto EXIT;
}

# loop over file parsing
while ($line = <SRC>)   {
  # Start new cycle on "\tablename{" in line
  # '\' is \x5c which is the only way it can be said
  if ($line=~"\\x5ctablename{") {
    WriteDescriptor; # write old and clear
     #print ($line);
    # get table type
    $Extname = GetNextArgument();
  } # end start new definition

  # Any keyword column indices
  elsif ($line=~"\\x5ctablecolindex\\x5b") {
     #print ($line);
    $IndexSymb[$NumIndex] =  GetNextArgument();
     #print ($IndexSymb[$NumIndex],"\n");
    $IndexCol[$NumIndex] =  GetNextArgument();
     #print ($IndexCol[$NumIndex],"\n");
    $NumIndex++;
  } # end new keyword entry

  # Table keyword entry
  elsif ($line=~"\\x5ctablekey\\x5b") {
    $KeyWord[$NumKey] =  GetNextArgument();
    $KeyType[$NumKey] =  GetNextArgument();
# use int for integers
    if ($KeyType[$NumKey] eq "I") {$KeyType[$NumKey] = "K"};
    $KeyComm[$NumKey] =  GetNextArgument();
    $NumKey++;
  } # end new keyword entry

  # Table column entry
  elsif ($line=~"\\x5ctablecol\\x5b") {
    $ColName[$NumCol] =  GetNextArgument();
    $ColUnit[$NumCol] =  GetNextArgument();
    $ColType[$NumCol] =  GetNextArgument();
    $ColDim[$NumCol] =  GetNextArgument();
    $NumCol++;
  } # end new keyword entry
} # end loop over file


# Write any final table
WriteDescriptor;

# any cleanup operations
EXIT:
} # end main block

