AC_DEFUN([AC_PATH_CFITSIO], [
	AC_ARG_WITH(cfitsio,
                    AC_HELP_STRING([--with-cfitsio=DIR],
                             [search for CFITSIO in DIR/include and DIR/lib]),
                    [for dir in `echo "$withval" | tr : ' '`; do
    if test -d $dir/include; then
      CFITSIO_CPPFLAGS="$CFITSIO_CPPFLAGS -I$dir/include"
    fi
    if test -d $dir/lib; then
      CFITSIO_LDFLAGS="$CFITSIO_LDFLAGS -L$dir/lib"
    fi
  done[]])

        AC_ARG_WITH(cfitsio-includes,
                    AC_HELP_STRING([--with-cfitsio-includes=DIR],
	                           [search for CFITSIO includes in DIR]),
	            [for dir in `echo "$withval" | tr : ' '`; do
    if test -d $dir; then
      CFITSIO_CPPFLAGS="$CFITSIO_CPPFLAGS -I$dir"
    fi
  done[]])

ac_cfitsio_saved_CPPFLAGS="$CPPFLAGS"
ac_cfitsio_saved_LDFLAGS="$LDFLAGS"
ac_cfitsio_saved_LIBS="$LIBS"
CPPFLAGS="$CPPFLAGS $CFITSIO_CPPFLAGS"
LDFLAGS="$LDFLAGS $CFITSIO_LDFLAGS"
ac_have_cfitsio=yes
        AC_CHECK_LIB(cfitsio, ffinit, [], [ac_have_cfitsio=no
	             AC_MSG_WARN([cannot find CFITSIO library])])
        AC_CHECK_HEADER(fitsio.h, [], [ac_have_cfitsio=no
	                AC_MSG_WARN([cannot find CFITSIO headers])])
if test $ac_have_cfitsio = yes; then
	AC_DEFINE(HAVE_CFITSIO, 1, [Define to 1 if you have CFITSIO.])
fi
CFITSIO_LIBS="$LIBS"
CPPFLAGS="$ac_cfitsio_saved_CPPFLAGS"
LDFLAGS="$ac_cfitsio_saved_LDFLAGS"
LIBS="$ac_cfitsio_saved_LIBS"
	AC_SUBST(CFITSIO_CPPFLAGS)
	AC_SUBST(CFITSIO_LDFLAGS)
	AC_SUBST(CFITSIO_LIBS)
])
