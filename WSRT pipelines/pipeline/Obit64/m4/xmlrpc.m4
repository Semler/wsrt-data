AC_DEFUN([AC_PATH_XMLRPC], [
	AC_ARG_WITH(xmlrpc,
                    AC_HELP_STRING([--with-xmlrpc=DIR],
                                 [search for XMLRPC in DIR/include and DIR/lib]),
                    [for dir in `echo "$withval" | tr : ' '`; do
    if test -d $dir/include; then
      XMLRPC_CPPFLAGS="$XMLRPC_CPPFLAGS -I$dir/include"
    fi
    if test -d $dir/lib; then
      XMLRPC_LDFLAGS="$XMLRPC_LDFLAGS -L$dir/lib"
    fi
  done[]])

        AC_ARG_WITH(xmlrpc-includes,
                    AC_HELP_STRING([--with-xmlrpc-includes=DIR],
	                           [search for XMLRPC includes in DIR]),
	            [for dir in `echo "$withval" | tr : ' '`; do
    if test -d $dir; then
      XMLRPC_CPPFLAGS="$XMLRPC_CPPFLAGS -I$dir"
    fi
  done[]])

XMLRPC="$dir"
WWWLIB_WL_RPATH=" "

	AC_ARG_WITH(www,
                    AC_HELP_STRING([--with-www=DIR],
                                 [search for WWW in DIR/include and DIR/lib]),
                    [for dir in `echo "$withval" | tr : ' '`; do
    if test -d $dir/include; then
      WWW_CPPFLAGS="$WWW_CPPFLAGS -I$dir/include"
    fi
    if test -d $dir/lib; then
      WWW_LDFLAGS="$WWW_LDFLAGS -L$dir/lib"
      WWW_LIBDIR="$dir/lib"
      WWWLIB_WL_RPATH="-Wl,--rpath -Wl,$WWW_LIBDIR"
    fi
  done[]])

        AC_ARG_WITH(www-includes,
                    AC_HELP_STRING([--with-www-includes=DIR],
	                           [search for WWW includes in DIR]),
	            [for dir in `echo "$withval" | tr : ' '`; do
    if test -d $dir; then
      WWW_CPPFLAGS="$WWW_CPPFLAGS -I$dir"
    fi
  done[]])

ac_xmlrpc_saved_CPPFLAGS="$CPPFLAGS"
ac_xmlrpc_saved_LDFLAGS="$LDFLAGS"
ac_xmlrpc_saved_LIBS="$LIBS"
CPPFLAGS="$CPPFLAGS $XMLRPC_CPPFLAGS"
LDFLAGS="$LDFLAGS $XMLRPC_LDFLAGS"
WWW_LIBS="$WWW_LDFLAGS -lwwwxml $LIBWWW_WL_RPATH -lxmltok -lxmlparse -lwwwzip -lwwwinit -lwwwapp -lmd5 -lwwwhtml -lwwwtelnet  -lwwwnews -lwwwhttp -lwwwmime -lwwwgopher -lwwwftp -lwwwfile -lwwwdir -lwwwcache -lwwwstream -lwwwmux -lwwwtrans -lwwwcore -lwwwutils"
XMLRPC_LIBS="-lxmlrpc -lxmlrpc_server -lxmlrpc_xmlparse -lxmlrpc_server_abyss -lxmlrpc_abyss -lpthread "
ac_have_xmlrpc=yes
        AC_SEARCH_LIBS(xmlrpc_env_init, xmlrpc, [], [ac_have_xmlrpc=no
                       AC_MSG_WARN([cannot find XMLRPC library])],
	[$XMLRPC_LIBS $WWWLIB_LDFLAGS $WWW_LIBS])
        AC_CHECK_HEADERS([xmlrpc.h], [], [ac_have_xmlrpc=no
                         AC_MSG_WARN([cannot find XMLRPC headers])])
if test $ac_have_xmlrpc = yes; then
	AC_DEFINE(HAVE_XMLRPC, 1, [Define to 1 if XMLRPC is available.])
fi
XMLRPC=/net/lusus/export/data_1/OtherSoftware.dir/rpcxml
XMLRPC_SERVER_CPPFLAGS="$CPPFLAGS $XMLRPC_CPPFLAGS"
XMLRPC_SERVER_LDFLAGS="$LDFLAGS $XMLRPC_LDFLAGS"
XMLRPC_SERVER_LIBS="$ac_xmlrpc_saved_LIBS $XMLRPC_LIBS "

XMLRPC_CLIENT_CPPFLAGS="$CPPFLAGS $XMLRPC_CPPFLAGS"
XMLRPC_CLIENT_LDFLAGS="$LDFLAGS $XMLRPC_LDFLAGS"
XMLRPC_CLIENT_LIBS="$ac_xmlrpc_saved_LIBS -lxmlrpc_client -lcurl $XMLRPC_LIBS "
AC_SUBST(XMLRPC_SERVER_CPPFLAGS)
AC_SUBST(XMLRPC_SERVER_LDFLAGS)
AC_SUBST(XMLRPC_SERVER_LIBS)
AC_SUBST(XMLRPC_CLIENT_CPPFLAGS)
AC_SUBST(XMLRPC_CLIENT_LDFLAGS)
AC_SUBST(XMLRPC_CLIENT_LIBS)

XMLRPC_LIBS="$ac_xmlrpc_saved_LIBS $XMLRPC_LIBS "
CPPFLAGS="$ac_xmlrpc_saved_CPPFLAGS"
LDFLAGS="$ac_xmlrpc_saved_LDFLAGS"
LIBS="$ac_xmlrpc_saved_LIBS"
	AC_SUBST(XMLRPC_CPPFLAGS)
	AC_SUBST(XMLRPC_LDFLAGS)
	AC_SUBST(XMLRPC_LIBS)
	AC_SUBST(WWWLIB_WL_RPATH)
	AC_SUBST(WWW_LIBS)
])
