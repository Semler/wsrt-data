 AC_DEFUN([AC_PATH_ZLIB], [
	AC_ARG_WITH(zlib,
                    AC_HELP_STRING([--with-zlib=DIR],
                                 [search for ZLIB in DIR/include and DIR/lib]),
                    [for dir in `echo "$withval" | tr : ' '`; do
    if test -d $dir/include; then
      ZLIB_CFLAGS="$ZLIB_CFLAGS -I$dir/include"
    fi
    if test -d $dir/lib; then
      ZLIB_LDFLAGS="$ZLIB_LDFLAGS -L$dir/lib"
    fi
  done[]])

        AC_ARG_WITH(zlib-includes,
                    AC_HELP_STRING([--with-zlib-includes=DIR],
	                           [search for ZLIB includes in DIR]),
	            [for dir in `echo "$withval" | tr : ' '`; do
    if test -d $dir; then
      ZLIB_CFLAGS="$ZLIB_CFLAGS -I$dir"
    fi
  done[]])

ac_zlib_saved_CFLAGS="$CFLAGS"
ac_zlib_saved_LDFLAGS="$LDFLAGS"
ac_zlib_saved_LIBS="$LIBS"
CFLAGS="$CFLAGS $ZLIB_CFLAGS"
LDFLAGS="$LDFLAGS $ZLIB_LDFLAGS"
ac_have_zlib=yes
        AC_SEARCH_LIBS(gzopen, [z], [], [ac_have_zlib=no
                       AC_MSG_WARN([cannot find ZLIB library])])
        AC_CHECK_HEADERS([zlib.h], [], [ac_have_zlib=no
                         AC_MSG_WARN([cannot find ZLIB headers])])
if test $ac_have_zlib = yes; then
	AC_DEFINE(HAVE_ZLIB, 1, [Define to 1 if ZLIB is available.])
fi
ZLIB_LIBS="$LIBS"
CFLAGS="$ac_zlib_saved_CFLAGS"
LDFLAGS="$ac_zlib_saved_LDFLAGS"
LIBS="$ac_zlib_saved_LIBS"
	AC_SUBST(ZLIB_CFLAGS)
	AC_SUBST(ZLIB_LDFLAGS)
	AC_SUBST(ZLIB_LIBS)
])
