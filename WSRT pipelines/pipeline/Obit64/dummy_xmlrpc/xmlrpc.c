/*  Dummy version of xmlrpc source file xmlrpc.c                           */
/* Includes dummy versions of all needed xmlrpc routines                   */
/* The dummy version of this library is solely to allow compiling and      */
/* linking but not execution of these functions which are stubbed.         */
/* No claims are made about this software except that is is NOT functional */
/* A proper version may be obtained from http://xmlrpc-c.sourceforge.net   */

#include <stdio.h>
#include "xmlrpc.h"

#define CRASH_AND_BURN  \
{fprintf(stderr,"xmlrpc not implemented\n"); exit(1); } 

void xmlrpc_env_init (xmlrpc_env* env)
{
  /* NOP so the program can pretend */
}

void xmlrpc_env_clean (xmlrpc_env* env)
{
  /* NOP so the program can pretend */
}

xmlrpc_value * 
xmlrpc_build_value(xmlrpc_env * const env,
                   const char * const format, 
                   ...)
{
  CRASH_AND_BURN 
  return NULL;
}

void 
xmlrpc_decompose_value(xmlrpc_env *   const envP,
                       xmlrpc_value * const value,
                       const char *   const format, 
                       ...)
{
  CRASH_AND_BURN 
}

extern void xmlrpc_INCREF (xmlrpc_value* value)
{
  CRASH_AND_BURN 
}

extern void xmlrpc_DECREF (xmlrpc_value* value)
{
  CRASH_AND_BURN 
}

extern xmlrpc_type xmlrpc_value_type (xmlrpc_value* value)
{
  CRASH_AND_BURN 
  return XMLRPC_TYPE_DEAD;
}

xmlrpc_value *
xmlrpc_int_new(xmlrpc_env * const envP,
               int          const intValue)
{
  CRASH_AND_BURN 
  return NULL;
}

void 
xmlrpc_read_int(xmlrpc_env *         const envP,
                const xmlrpc_value * const valueP,
                int *                const intValueP)
{
  CRASH_AND_BURN 
}

xmlrpc_value *
xmlrpc_bool_new(xmlrpc_env * const envP,
                xmlrpc_bool  const boolValue)
{
  CRASH_AND_BURN 
  return NULL;
}

void
xmlrpc_read_bool(xmlrpc_env *         const envP,
                 const xmlrpc_value * const valueP,
                 xmlrpc_bool *        const boolValueP)
{
  CRASH_AND_BURN 
}

xmlrpc_value *
xmlrpc_double_new(xmlrpc_env * const envP,
                  double       const doubleValue)
{
  CRASH_AND_BURN 
  return NULL;
}

void
xmlrpc_read_double(xmlrpc_env *         const envP,
                   const xmlrpc_value * const valueP,
                   xmlrpc_double *      const doubleValueP)
{
  CRASH_AND_BURN 
}

xmlrpc_value *
xmlrpc_datetime_new_str(xmlrpc_env * const envP,
                        const char * const value)
{
  CRASH_AND_BURN 
  return NULL;
}

void
xmlrpc_read_datetime_str(xmlrpc_env *         const envP,
                         const xmlrpc_value * const valueP,
                         const char **        const stringValueP)
{
  CRASH_AND_BURN 
}

xmlrpc_value *
xmlrpc_string_new(xmlrpc_env * const envP,
                  const char * const stringValue)
{
  CRASH_AND_BURN 
  return NULL;
}

xmlrpc_value *
xmlrpc_string_new_lp(xmlrpc_env * const envP, 
                     size_t       const length,
                     const char * const stringValue)
{
  CRASH_AND_BURN 
  return NULL;
}

void
xmlrpc_read_string(xmlrpc_env *         const envP,
                   const xmlrpc_value * const valueP,
                   const char **        const stringValueP)
{
  CRASH_AND_BURN 
}


void
xmlrpc_read_string_lp(xmlrpc_env *         const envP,
                      const xmlrpc_value * const valueP,
                      size_t *             const lengthP,
                      const char **        const stringValueP)
{
  CRASH_AND_BURN 
}

xmlrpc_value *
xmlrpc_base64_new(xmlrpc_env *          const envP,
                  unsigned int          const length,
                  const unsigned char * const bytestringValue)
{
  CRASH_AND_BURN 
  return NULL;
}

void
xmlrpc_read_base64(xmlrpc_env *           const envP,
                   const xmlrpc_value *   const valueP,
                   unsigned int *         const lengthP,
                   const unsigned char ** const bytestringValueP)
{
  CRASH_AND_BURN 
}

xmlrpc_value *
xmlrpc_array_new(xmlrpc_env * const envP)
{
  CRASH_AND_BURN 
  return NULL;
}

extern void
xmlrpc_array_append_item(xmlrpc_env*   env,
			 xmlrpc_value* array,
			 xmlrpc_value* value)
{
  CRASH_AND_BURN 
}

xmlrpc_value * 
xmlrpc_array_get_item(xmlrpc_env *         const envP,
                      const xmlrpc_value * const arrayP,
                      int                  const index)
{
  CRASH_AND_BURN 
  return NULL;
}

void
xmlrpc_array_read_item(xmlrpc_env *         const envP,
                       const xmlrpc_value * const arrayP,
                       unsigned int         const index,
                       xmlrpc_value **      const valuePP)
{
  CRASH_AND_BURN 
}

xmlrpc_value *
xmlrpc_struct_new(xmlrpc_env * env)
{
  CRASH_AND_BURN 
  return NULL;
}

int
xmlrpc_struct_size (xmlrpc_env   * env, 
                    xmlrpc_value * strct)
{
  CRASH_AND_BURN 
  return 1;
}

void 
xmlrpc_struct_set_value(xmlrpc_env *   const env,
                        xmlrpc_value * const strct,
                        const char *   const key,
                        xmlrpc_value * const value)
{
  CRASH_AND_BURN 
}

int 
xmlrpc_struct_has_key(xmlrpc_env *   const envP,
                      xmlrpc_value * const strctP,
                      const char *   const key)
{
  CRASH_AND_BURN 
  return 1;
}

void
xmlrpc_struct_find_value(xmlrpc_env *    const envP,
                         xmlrpc_value *  const structP,
                         const char *    const key,
                         xmlrpc_value ** const valuePP)
{
  CRASH_AND_BURN 
}

void
xmlrpc_struct_read_value(xmlrpc_env *    const envP,
                         xmlrpc_value *  const strctP,
                         const char *    const key,
                         xmlrpc_value ** const valuePP)
{
  CRASH_AND_BURN 
}

void 
xmlrpc_struct_read_member(xmlrpc_env *    const envP,
                          xmlrpc_value *  const structP,
                          unsigned int    const index,
                          xmlrpc_value ** const keyvalP,
                          xmlrpc_value ** const valueP)

{
  CRASH_AND_BURN 
}

#define XMLRPC_CLIENT_NO_FLAGS         (0)
#define XMLRPC_CLIENT_SKIP_LIBWWW_INIT (1)

extern void
xmlrpc_client_init(int          const flags,
                   const char * const appname,
                   const char * const appversion)
{
  CRASH_AND_BURN 
}

extern void
xmlrpc_client_cleanup(void)
{
  CRASH_AND_BURN 
}

xmlrpc_value * 
xmlrpc_client_call(xmlrpc_env * const envP,
                   const char * const server_url,
                   const char * const method_name,
                   const char * const format,
                   ...)
{
  CRASH_AND_BURN 
  return NULL;
}

xmlrpc_value * 
xmlrpc_client_call_params(xmlrpc_env *   const envP,
                          const char *   const serverUrl,
                          const char *   const methodName,
                          xmlrpc_value * const paramArrayP)
{
  CRASH_AND_BURN 
  return NULL;
}

/*void 
  xmlrpc_client_call_asynch(const char * const server_url,
  const char * const method_name,
  xmlrpc_response_handler callback,
  void *       const user_data,
  const char * const format,
  ...)
  {
  CRASH_AND_BURN 
  return 1;
  }*/

extern void
xmlrpc_client_event_loop_finish_asynch(void)
{
  CRASH_AND_BURN 
}

extern void
xmlrpc_client_event_loop_finish_asynch_timeout(unsigned long milliseconds)
{
  CRASH_AND_BURN 
}

xmlrpc_registry *
xmlrpc_registry_new(xmlrpc_env * env)
{
  /* NOP so the program can pretend */
  return NULL;
}

void
xmlrpc_registry_free(xmlrpc_registry * registry)
{
  CRASH_AND_BURN 
}

void
xmlrpc_registry_add_method(xmlrpc_env *      env,
                           xmlrpc_registry * registry,
                           const char *      host,
                           const char *      method_name,
                           xmlrpc_method     method,
                           void *            user_data)
{
  /* NOP so the program can pretend */
}

void
xmlrpc_server_abyss(xmlrpc_env *                      const envP,
                    const xmlrpc_server_abyss_parms * const parms,
                    unsigned int                      const parm_size)

{
  /* NOP so the program can pretend */
}

