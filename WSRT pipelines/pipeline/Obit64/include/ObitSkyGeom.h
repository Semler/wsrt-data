/* $Id: ObitSkyGeom.h,v 1.1.1.1 2004/07/19 16:42:44 bcotton Exp $                            */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2004                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
#ifndef OBITSKYGEOM_H
#define OBITSKYGEOM_H
#include "Obit.h"
#include "ObitErr.h"

/**
 * \file ObitSkyGeom.h
 * Celestial coordinates utility class
 * 
 * This file contains utility functions  Celestial coordinates
 */

/*-------------- enumerations -------------------------------------*/
/**
 * \enum obitSkyGeomProj
 * enum for SkyGeom projection types.
 */
enum obitSkyGeomProj {
  /** -SIN  Sin projection */
  OBIT_SkyGeom_SIN = 0, 
  /** -TAN  Tan projection */
  OBIT_SkyGeom_TAN,
  /** -ARC Arc projection */
  OBIT_SkyGeom_ARC, 
  /** -NCP NCP (WSRT) projection */
  OBIT_SkyGeom_NCP, 
  /** -GLS Global sinusoid projection */
  OBIT_SkyGeom_GLS, 
  /** -MER Mercator projection */
  OBIT_SkyGeom_MER, 
  /** -AIT  Aitoff projection */
  OBIT_SkyGeom_AIT, 
  /** -STG Stereographic projection */
  OBIT_SkyGeom_STG 
}; /* end enum obitIOType */

/** typedef for enum for SkyGeom projection types. */
typedef enum obitSkyGeomProj ObitSkyGeomProj;

/*---------------Public functions---------------------------*/
/** Public: Determine shift between two positions */
void ObitSkyGeomShiftXY (gdouble ra, gdouble dec, gfloat rotate,
			gdouble shiftRA, gdouble shiftDec,
			gfloat *xShift, gfloat *yShift);

/** Public: Determine result of a shift to a position */
void ObitSkyGeomXYShift (gdouble ra, gdouble dec, 
			gfloat xShift, gfloat yShift, gfloat rotate,
			gdouble *shiftRA, gdouble *shiftDec);

/** Public: Get shift parameters for -SIN projection. */
void  
ObitSkyGeomShiftSIN (gdouble ra, gdouble dec, gfloat rotate,
		    gdouble xra, double xdec, gfloat dxyzc[3]);

/** Public: Get shift parameters for -NCP projection. */
void  
ObitSkyGeomShiftNCP (gdouble ra, gdouble dec, gfloat rotate,
		    gdouble xra, double xdec, gfloat dxyzc[3]);

/** Public: Returns astronomical coordinates given direction cosines, projection */
void 
ObitSkyGeomNewPos (ObitSkyGeomProj Proj, gdouble ra0, gdouble dec0, gdouble l, gdouble m, 
		   gdouble *raout, gdouble *decout, gint *ierr);

/** Public: accurate position for pixel coordinates */
gint 
ObitSkyGeomWorldPos(gfloat xpix, gfloat ypix, gdouble xref, gdouble yref, 
		    gfloat xrefpix, gfloat yrefpix, gfloat xinc, gfloat yinc, 
		    gfloat rot, gchar *type, gdouble *xpos, gdouble *ypos);

/** Public: Position for pixel coordinates from IRAF style CD matrix */
gint 
ObitSkyGeomCDpos(gfloat xpix, gfloat ypix, gdouble xref, gdouble yref,
		 gfloat xrefpix, gfloat yrefpix, gfloat xinc, gfloat yinc, gfloat rot,
		 gfloat cd1[2], gfloat cd2[2], gchar *type, gdouble *xpos, gdouble *ypos);

/** Public: Pixel coordinates for an RA and Dec*/
gint 
ObitSkyGeomXYpix(gdouble xpos, gdouble ypos, gdouble xref, gdouble yref, 
		 gfloat xrefpix, gfloat yrefpix, gfloat xinc, gfloat yinc, 
		 gfloat rot, gchar *type, gfloat *xpix, gfloat *ypix);

/** Public:pixel coordinates for an RA and Dec from IRAF  style CD matrix. */
gint 
ObitSkyGeomCDpix(gdouble xpos, gdouble ypos, gdouble xref, gdouble yref, 
		 gfloat xrefpix, gfloat yrefpix, gfloat xinc, gfloat yinc, gfloat rot,
		 gfloat icd1[2], gfloat icd2[2], gchar *type, 
		 gfloat *xpix, gfloat *ypix);

/** Public: Position for pixel coordinates from  offsets from the reference position.*/
gint 
ObitSkyGeomWorldPosLM(gdouble dx, gdouble dy, gdouble xref, gdouble yref, 
		      gfloat xinc, gfloat yinc, gfloat rot, gchar *type, 
		      gdouble *xpos, gdouble *ypos);

/** Public: Coordinate offsets for an RA and Dec   */
gint 
ObitSkyGeomXYPixLM(gdouble xpos, gdouble ypos, gdouble xref, gdouble yref, 
		   gfloat xinc, gfloat yinc, gfloat rot, gchar *type, 
		   gdouble *dx, gdouble *dy);

/** Public: Precess B1950 to J2000 coordinates  */
void 
ObitSkyGeomBtoJ (gdouble *ra, gdouble *dec);

/** Public: Precess J2000 to B1950 coordinates */
void 
ObitSkyGeomJtoB (gdouble *ra, gdouble *dec);

/** Public: Convert Equatorial (B1950) to Galactic coordinates  */
void ObitSkyGeomEq2Gal (gdouble *RALong, gdouble *DecLat);

/** Public: Convert Galactic to Equatorial (B1950)  */
void ObitSkyGeomGal2Eq (gdouble *RALong, gdouble *DecLat);

/** Public: Convert Equatorial to Ecliptic coordinates */
void ObitSkyGeomEq2Ec (gdouble *RALong, gdouble *DecLat, gfloat epoch);

/** Public: Convert Ecliptic to Equatorial */
void ObitSkyGeomEc2Eq (gdouble *RALong, gdouble *DecLat, gfloat epoch);

#endif  /* OBITSKYGEOM_H */
