/* $Id: ObitFArray.h,v 1.10 2005/10/06 20:22:55 bcotton Exp $   */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
#ifndef OBITFARRAY_H 
#define OBITFARRAY_H 

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include "Obit.h"
#include "ObitErr.h"
#include "ObitInfoList.h"
#include "ObitThread.h"

/*-------- Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitFArray.h
 * ObitFArray numeric array class definition.
 *
 * This class is for creating and manipulating a Array as a memory resident 
 * multidimensional rectangular array of floats.
 * Elements are stored in order of the increasing axis order (the reverse of the
 * usual c definition).
 * Except as noted, magic value blanking is supported (OBIT_MAGIC).
 * 
 * \section ObitFArrayaccess Creators and Destructors
 * An ObitFArray will usually be created using ObitFArrayCreate which allows 
 * specifying a name for the object as well as dimensionality of the array.
 *
 * A copy of a pointer to an ObitFArray should always be made using the
 * #ObitFArrayRef function which updates the reference count in the object.
 * Then whenever freeing an ObitFArray or changing a pointer, the function
 * #ObitFArrayUnref will decrement the reference count and destroy the object
 * when the reference count hits 0.
 * There is no explicit destructor.
 */

/*--------------Class definitions-------------------------------------*/
/** ObitFArray Class structure. */
typedef struct {
#include "ObitFArrayDef.h"   /* this class definition */
} ObitFArray;

/*----------------- Macroes ---------------------------*/
/** 
 * Macro to unreference (and possibly destroy) an ObitFArray
 * returns a ObitFArray*.
 * in = object to unreference
 */
#define ObitFArrayUnref(in) ObitUnref (in)

/** 
 * Macro to reference (update reference count) an ObitFArray.
 * returns a ObitFArray*.
 * in = object to reference
 */
#define ObitFArrayRef(in) ObitRef (in)

/** 
 * Macro to determine if an object is the member of this or a 
 * derived class.
 * Returns TRUE if a member, else FALSE
 * in = object to reference
 */
#define ObitFArrayIsA(in) ObitIsA (in, ObitFArrayGetClass())

/** Maximum ObitFArray number of dimensions */
#ifndef MAXFARRAYDIM
#define MAXFARRAYDIM 10
#endif

/*---------------Public functions---------------------------*/
/** Public: Class initializer. */
void ObitFArrayClassInit (void);

/** Public: Default Constructor. */
ObitFArray* newObitFArray (gchar* name);

/** Public: Create/initialize ObitFArray structures */
ObitFArray* ObitFArrayCreate (gchar* name, glong ndim, glong *naxis);
/** Typedef for definition of class pointer structure */
typedef void (*ObitFArrayCreateFP) (gchar* name, glong ndim, glong *naxis);

/** Public: ClassInfo pointer */
gconstpointer ObitFArrayGetClass (void);

/** Public: Copy (deep) constructor. */
ObitFArray* ObitFArrayCopy  (ObitFArray *in, ObitFArray *out, ObitErr *err);

/** Public: Copy structure. */
void ObitFArrayClone (ObitFArray *in, ObitFArray *out, ObitErr *err);

/** Public: Copy Subarray constructor. */
ObitFArray* ObitFArraySubArr  (ObitFArray *in, glong *blc, glong *trc, 
			       ObitErr *err);
typedef ObitFArray* (*ObitFArraySubArrFP) (ObitFArray *in, glong *blc, glong *trc, 
					   ObitErr *err);

/** Public: Are two FArrays of compatable geometry. */
gboolean ObitFArrayIsCompatable  (ObitFArray *in1, ObitFArray *in2);
typedef gboolean (*ObitFArrayIsCompatableFP) (ObitFArray *in1, ObitFArray *in2);

/** Public: Reallocate/initialize ObitFArray structures */
ObitFArray* ObitFArrayRealloc (ObitFArray* in, glong ndim, glong *naxis);
typedef void (*ObitFArrayReallocFP) (ObitFArray* in, glong ndim, glong *naxis);

/** Public: return pointer to a specified element */
gfloat* ObitFArrayIndex (ObitFArray* in, glong *pos);
typedef gfloat* (*ObitFArrayIndexFP) (ObitFArray* in, glong *pos);

/** Public: Find Maximum value in an ObitFArray */
gfloat ObitFArrayMax (ObitFArray* in, glong *pos);
typedef gfloat (*ObitFArrayMaxFP) (ObitFArray* in, glong *pos);

/** Public: Find Maximum abs value in an ObitFArray */
gfloat ObitFArrayMaxAbs (ObitFArray* in, glong *pos);
typedef gfloat (*ObitFArrayMaxAbsFP) (ObitFArray* in, glong *pos);

/** Public: Find Minimum value in an ObitFArray */
gfloat ObitFArrayMin (ObitFArray* in, glong *pos);
typedef gfloat (*ObitFArrayMinFP) (ObitFArray* in, glong *pos);

/** Public: Replace blanks in an ObitFArray */
void ObitFArrayDeblank (ObitFArray* in, gfloat scalar);
typedef void (*ObitFArrayDeblankFP) (ObitFArray* in, gfloat scalar);

/** Public: RMS of pixel distribution. */
gfloat ObitFArrayRMS (ObitFArray* in);
typedef gfloat (*ObitFArrayRMSFP) (ObitFArray* in);

/** Public: Mode of pixel distribution. */
gfloat ObitFArrayMode (ObitFArray* in);
typedef gfloat (*ObitFArrayModeFP) (ObitFArray* in);

/** Public: Mean of pixel distribution. */
gfloat ObitFArrayMean (ObitFArray* in);
typedef gfloat (*ObitFArrayMeanFP) (ObitFArray* in);

/** Public: fill elements of an FArray */
void ObitFArrayFill (ObitFArray* in, gfloat scalar);
typedef void (*ObitFArrayFillFP) (ObitFArray* in, gfloat scalar);

/** Public: negate elements of an FArray */
void ObitFArrayNeg (ObitFArray* in);
typedef void (*ObitFArrayNegFP) (ObitFArray* in);

/** Public: sum elements of an FArray */
gfloat ObitFArraySum (ObitFArray* in);
typedef gfloat (*ObitFArraySumFP) (ObitFArray* in);

/** Public: number of valid elements in an FArray */
glong ObitFArrayCount (ObitFArray* in);
typedef glong (*ObitFArrayCountFP) (ObitFArray* in);

/** Public: Add a scalar to elements of an FArray */
void ObitFArraySAdd (ObitFArray* in, gfloat scalar);
typedef void (*ObitFArraySAddFP) (ObitFArray* in, gfloat scalar);

/** Public: Multiply elements of an FArray by a scalar*/
void ObitFArraySMul (ObitFArray* in, gfloat scalar);
typedef void (*ObitFArraySMulFP) (ObitFArray* in, gfloat scalar);

/** Public: Divide elements of an FArray into a scalar*/
void ObitFArraySDiv (ObitFArray* in, gfloat scalar);
typedef void (*ObitFArraySDivFP) (ObitFArray* in, gfloat scalar);

/** Public: Clip elements of an FArray outside of a given range */
void ObitFArrayClip (ObitFArray* in, gfloat minVal,gfloat maxVal, gfloat newVal);
typedef void (*ObitFArrayClipFP) (ObitFArray* in, gfloat minVal, gfloat maxVal, 
				  gfloat newVal);

/** Public: Clip elements of an FArray inside of a given range */
void ObitFArrayInClip (ObitFArray* in, gfloat minVal,gfloat maxVal, gfloat newVal);
typedef void (*ObitFArrayInClipFP) (ObitFArray* in, gfloat minVal, gfloat maxVal, 
				    gfloat newVal);

/** Public: Blank elements of an array where another is blanked */
void ObitFArrayBlank (ObitFArray* in1, ObitFArray* in2, ObitFArray* out);
typedef void (*ObitFArrayBlankFP) (ObitFArray* in1, ObitFArray* in2, 
				  ObitFArray* out);

/** Public: Get larger elements of two FArrays */
void ObitFArrayMaxArr (ObitFArray* in1, ObitFArray* in2, ObitFArray* out);
typedef void (*ObitFArrayMaxArrFP) (ObitFArray* in1, ObitFArray* in2, 
				    ObitFArray* out);

/** Public: Get lesser elements of two FArrays */
void ObitFArrayMinArr (ObitFArray* in1, ObitFArray* in2, ObitFArray* out);
typedef void (*ObitFArrayMinArrFP) (ObitFArray* in1, ObitFArray* in2, 
				    ObitFArray* out);

/** Public: Sum nonblanked elements of two FArrays */
void ObitFArraySumArr (ObitFArray* in1, ObitFArray* in2, ObitFArray* out);
typedef void (*ObitFArraySumArrFP) (ObitFArray* in1, ObitFArray* in2, 
				    ObitFArray* out);

/** Public: Add elements of two FArrays */
void ObitFArrayAdd (ObitFArray* in1, ObitFArray* in2, ObitFArray* out);
typedef void (*ObitFArrayAddFP) (ObitFArray* in1, ObitFArray* in2, 
				  ObitFArray* out);

/** Public: Subtract elements of two FArrays */
void ObitFArraySub (ObitFArray* in1, ObitFArray* in2, ObitFArray* out);
typedef void (*ObitFArraySubFP) (ObitFArray* in1, ObitFArray* in2, 
				  ObitFArray* out);

/** Public: Multiply elements of two FArrays */
void ObitFArrayMul (ObitFArray* in1, ObitFArray* in2, ObitFArray* out);
typedef void (*ObitFArrayMulFP) (ObitFArray* in1, ObitFArray* in2, 
				  ObitFArray* out);

/** Public: Divide elements of two FArrays */
void ObitFArrayDiv (ObitFArray* in1, ObitFArray* in2, ObitFArray* out);
typedef void (*ObitFArrayDivFP) (ObitFArray* in1, ObitFArray* in2, 
				  ObitFArray* out);

/** Public: Divide elements of two FArrays with clipping*/
void ObitFArrayDivClip (ObitFArray* in1, ObitFArray* in2, gfloat minVal, ObitFArray* out);
typedef void (*ObitFArrayDivClipFP) (ObitFArray* in1, ObitFArray* in2, 
				     gfloat minVal, ObitFArray* out);

/** Public: "Dot" product to two arrays */
gfloat ObitFArrayDot (ObitFArray* in1, ObitFArray* in2);
typedef gfloat (*ObitFArrayDotFP) (ObitFArray* in1, ObitFArray* in2);

/** Public: Multiply a 2D array by a Col vector * Row vector */
void ObitFArrayMulColRow (ObitFArray* in, ObitFArray* row, ObitFArray* col,
			  ObitFArray* out);
typedef void (*ObitFArrayMulColRowFP) (ObitFArray* in, ObitFArray* row, 
				       ObitFArray* col, ObitFArray* out);

/** Public: Convert a 1D "center at edges" array to proper order */
void ObitFArray1DCenter (ObitFArray* in);
typedef void (*ObitFArray1DCenterFP) (ObitFArray* in);

/** Public: Convert a 2D "center at edges" array to proper order */
void ObitFArray2DCenter (ObitFArray* in);
typedef void (*ObitFArray2DCenterFP) (ObitFArray* in);

/** Public: inplace invert a symmetric 2D array */
void ObitFArray2DSymInv (ObitFArray* in, gint *ierr);
typedef void (*ObitFArray2DSymInvFP) (ObitFArray* in, gint *ierr);

/** Public: Make 2-D Circular Gaussian in FArray */
void ObitFArray2DCGauss (ObitFArray* in, glong Cen[2], gfloat FWHM);
typedef void (*ObitFArray2DCGaussFP) (ObitFArray* in, glong Cen[2], gfloat FWHM);

/** Public: Make 2-D Eliptical Gaussian in FArray */
void ObitFArray2DEGauss (ObitFArray* in, gfloat amp, gfloat Cen[2], gfloat GauMod[3]);
typedef void (*ObitFArray2DEGaussFP) (ObitFArray* in, gfloat amp, gfloat Cen[2], 
				      gfloat GauMod[3] );

/** Public: Shift and Add scaled array */
void ObitFArrayShiftAdd (ObitFArray* in1, glong *pos1, 
			 ObitFArray* in2, glong *pos2, 
			 gfloat scalar, ObitFArray* out);
typedef void 
(*ObitFArrayShiftAddFP) (ObitFArray* in1, glong *pos1, 
			 ObitFArray* in2, glong *pos2, 
			 gfloat scalar, ObitFArray* out);

/** Public: Zero pad an array */
void  ObitFArrayPad (ObitFArray* in, ObitFArray* out, gfloat factor);
typedef void (*ObitFArrayPadFP) (ObitFArray* in, ObitFArray* out, 
				 gfloat factor);

/** Public: Convolve a list of Gaussians onto an FArray */
void  ObitFArrayConvGaus (ObitFArray* in, ObitFArray* list, glong ncomp, 
			  gfloat gauss[3]);
typedef void (*ObitFArrayConvGausFP) (ObitFArray* in, ObitFArray* list, 
				      glong ncomp, gfloat gauss[3]);
/*----------- ClassInfo Structure -----------------------------------*/
/**
 * ClassInfo Structure.
 * Contains class name, a pointer to any parent class
 * (NULL if none) and function pointers.
 */
typedef struct  {
#include "ObitFArrayClassDef.h"
} ObitFArrayClassInfo; 

#endif /* OBITFARRAY_H */ 
