/* $Id: ObitPBUtil.h,v 1.3 2005/03/03 14:16:36 bcotton Exp $ */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2004-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
#ifndef OBITPBUTIL_H 
#define OBITPBUTIL_H 

#include "Obit.h"
#include "ObitImage.h"
#include "ObitTableCC.h"

/*-------- Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitPBUtil.h
 * Antenna primary beam shape utility class.
 *
 * There are no objects of this class.
 */


/*---------------Public functions---------------------------*/
/** Use polynomial beam shape - useful for VLA frequencies < 1.0 GHz */
gfloat ObitPBUtilPoly (gdouble Angle, gdouble Freq);

/** Use Jinc beam shape - useful for frequencies > 1.0 GHz */
gfloat ObitPBUtilJinc (gdouble Angle, gdouble Freq, gfloat antSize);

/** Function which returns relative primary beam correction */
gfloat ObitPBUtilRelPB (gdouble Angle, gint nfreq, gdouble *Freq, gfloat antSize, 
			gdouble refFreq);

/** Correct ObitTableCC for relative Primary Beam */
ObitTableCC *ObitPBUtilCCCor(ObitImage *image, glong inCCver, glong *outCCver, 
			     gint nfreq, gdouble *Freq, gfloat antSize, 
			     gdouble refFreq, glong *startCC, glong *endCC, 
			     ObitErr *err);

/** Correct Image for relative Primary Beam */
ObitFArray* ObitPBUtilImageCor(ObitImage *image, gint *inPlane, 
			       gint nfreq, gdouble *Freq, 
			       gfloat antSize, gdouble refFreq, ObitErr *err);

#endif /* OBITPBUTIL_H */ 
