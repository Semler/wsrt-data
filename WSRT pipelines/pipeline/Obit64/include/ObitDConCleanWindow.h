/* $Id: ObitDConCleanWindow.h,v 1.8 2005/10/06 20:22:54 bcotton Exp $ */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2004-2005                                          */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
#ifndef OBITDCONCLEANWINDOW_H 
#define OBITDCONCLEANWINDOW_H 

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include "Obit.h"
#include "ObitImageMosaic.h"
#include "ObitErr.h"

/*-------- Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitDConCleanWindow.h
 * ObitDConCleanWindow template for classes derived from Obit
 *
 * This class contains specifications of which pixels in the images of an
 * #ObitImageMosaic are candidates for a CLEAN component.
 * The current implementation uses a GList for each field so that the number of windows
 * is arbitrary.  However, this should be transparent outside the class.
 * Each field in an ImageMosaic has two potential sets of CLEAN windows, 
 * the traditional "inner" window with an arbitrart number of components and an
 * "outer", single window which sets the region in which the autoWindow feature
 * is allowed to place windows.
 * 
 * \section ObitDConCleanWindowaccess Creators and Destructors
 * An ObitDConCleanWindow will usually be created using ObitDConCleanWindowCreate 
 * which allows specifying a name for the object as well as dimensionality of the array.
 *
 * A copy of a pointer to an ObitDConCleanWindow should always be made using the
 * #ObitDConCleanWindowRef function which updates the reference count in the object.
 * Then whenever freeing an ObitDConCleanWindow or changing a pointer, the function
 * #ObitDConCleanWindowUnref will decrement the reference count and destroy the object
 * when the reference count hits 0.
 * There is no explicit destructor.
 *
 * The contents can be modified using the #ObitDConCleanWindowAdd and 
 * #ObitDConCleanWindowUpdate functions.
 * The presence of valid pixels in an image is indicated by
 * #ObitDConCleanWindowImage and in a row by #ObitDConCleanWindowRow this also 
 * computes a mask for the row indicating valid pixels.
 *
 * This class supports the autoWindow facility and has different behavior when the 
 * autoWindow member is TRUE or FALSE.
 * If TRUE then if no inner window is specified then all pixels are invalid.
 * If FALSE then the default is that all pixels are selected.
 */

/*--------------Class definitions-------------------------------------*/
/** ObitDConCleanWindow Class structure. */
typedef struct {
#include "ObitDConCleanWindowDef.h"   /* this class definition */
} ObitDConCleanWindow;

/*----------------- enums ---------------------------*/
/**
 * enum for window types
 * Should be coordinated with ObitErrorLevelString in ObitErr.c.
 */
enum ObitDConCleanWindowType {
  /** rectangle, specified by blc, trc corners */
  OBIT_DConCleanWindow_rectangle = 0,
  /** round, specified by radius, center x, center y pixel */
  OBIT_DConCleanWindow_round
};/* end enum ObitDConObitCleanWindowType */
/**
 * typedef for enum for window types
 */
typedef enum ObitDConCleanWindowType ObitDConCleanWindowType;

/*----------------- Macroes ---------------------------*/
/** 
 * Macro to unreference (and possibly destroy) an ObitDConCleanWindow
 * returns a ObitDConCleanWindow*.
 * in = object to unreference
 */
#define ObitDConCleanWindowUnref(in) ObitUnref (in)

/** 
 * Macro to reference (update reference count) an ObitDConCleanWindow.
 * returns a ObitDConCleanWindow*.
 * in = object to reference
 */
#define ObitDConCleanWindowRef(in) ObitRef (in)

/** 
 * Macro to determine if an object is the member of this or a 
 * derived class.
 * Returns TRUE if a member, else FALSE
 * in = object to reference
 */
#define ObitDConCleanWindowIsA(in) ObitIsA (in, ObitDConCleanWindowGetClass())

/*---------------Public functions---------------------------*/
/** Public: Class initializer. */
void ObitDConCleanWindowClassInit (void);

/** Public: Default Constructor. */
ObitDConCleanWindow* newObitDConCleanWindow (gchar* name);

/** Public: Create/initialize ObitDConCleanWindow structures */
ObitDConCleanWindow* 
ObitDConCleanWindowCreate (gchar* name, ObitImageMosaic *mosaic, ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef ObitDConCleanWindow* (*ObitDConCleanWindowCreateFP) (gchar* name, 
							     ObitImageMosaic *mosaic, 
							     ObitErr *err);

/** Public: Create/initialize ObitDConCleanWindow structure with 1 field */
ObitDConCleanWindow* 
ObitDConCleanWindowCreate1 (gchar* name, glong naxis[2], ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef ObitDConCleanWindow* 
(*ObitDConCleanWindowCreate1FP) (gchar* name, glong naxis[2], ObitErr *err);

/** Public: ClassInfo pointer */
gconstpointer ObitDConCleanWindowGetClass (void);

/** Public: Copy (deep) constructor. */
ObitDConCleanWindow* ObitDConCleanWindowCopy  (ObitDConCleanWindow *in, 
					       ObitDConCleanWindow *out, 
					       ObitErr *err);

/** Public: Copy structure. */
void ObitDConCleanWindowClone (ObitDConCleanWindow *in, 
			       ObitDConCleanWindow *out, 
			       ObitErr *err);

/** Public: Ask window definition */
gboolean ObitDConCleanWindowInfo (ObitDConCleanWindow *in, 
			      glong field, glong Id,
			      ObitDConCleanWindowType *type,
			      glong **window,  ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef gboolean (*ObitDConCleanWindowInfoFP) (ObitDConCleanWindow *in, 
					       glong field, glong Id,
					       ObitDConCleanWindowType *type,
					       glong **window, ObitErr *err);

/** Public: Search for a window near a given pixel */
glong ObitDConCleanWindowSearch (ObitDConCleanWindow *in, 
				 glong field, glong pixel[2], 
				 glong toler, gint *which, ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef glong (*ObitDConCleanWindowSearchFP) (ObitDConCleanWindow *in, 
					      glong field, glong pixel[2], 
					      glong toler, gint *which, 
					      ObitErr *err);

/** Public: Add a new window definition */
glong ObitDConCleanWindowAdd (ObitDConCleanWindow *in, 
			      glong field, ObitDConCleanWindowType type,
			      glong *window, ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef glong (*ObitDConCleanWindowAddFP) (ObitDConCleanWindow *in, 
					   glong field, 
					   ObitDConCleanWindowType type,
					   glong *window, ObitErr *err);

/** Public: Delete a window */
void ObitDConCleanWindowDel (ObitDConCleanWindow *in, 
			     glong field, glong Id, ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef void (*ObitDConCleanWindowDelFP) (ObitDConCleanWindow *in, 
					  glong field, glong Id, ObitErr *err);

/** Public: Modify an existing window */
void ObitDConCleanWindowUpdate (ObitDConCleanWindow *in,  
				glong field, glong Id, 
				ObitDConCleanWindowType type,
				glong *window, ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef void (*ObitDConCleanWindowUpdateFP) (ObitDConCleanWindow *in,  
				glong field, glong Id, 
				ObitDConCleanWindowType type,
				glong *window, ObitErr *err);

/** Public: Set outer window for a field  */
void ObitDConCleanWindowOuter (ObitDConCleanWindow *in, glong field, 
			       ObitDConCleanWindowType type,
			       glong *window, ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef void (*ObitDConCleanWindowOuterFP) (ObitDConCleanWindow *in, 
					    glong field,  
					     ObitDConCleanWindowType type,
					     glong *window, ObitErr *err);

/** Public: Are there any valid pixels in this field's image? */
gboolean ObitDConCleanWindowImage (ObitDConCleanWindow *in, glong field, 
				   ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef gboolean (*ObitDConCleanWindowImageFP) (ObitDConCleanWindow *in, 
						glong field, ObitErr *err);

/** Public: Are there any valid pixels in a specified row with inner window?  */
gboolean ObitDConCleanWindowRow (ObitDConCleanWindow *in, glong field, glong row, 
				 gboolean **mask, ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef gboolean (*ObitDConCleanWindowRowFP) (ObitDConCleanWindow *in, 
					      glong field, glong row, 
					      gboolean **mask, ObitErr *err);

/** Public: Are there any valid pixels in a specified row with outer window?  */
gboolean ObitDConCleanWindowOuterRow (ObitDConCleanWindow *in, glong field, 
				      glong row, gboolean **mask, ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef gboolean (*ObitDConCleanWindowOuterRowFP) (ObitDConCleanWindow *in, 
						   glong field, glong row, 
						   gboolean **mask, ObitErr *err);

/** Public: What is the maximum region covered in x or y?  */
glong ObitDConCleanWindowSize (ObitDConCleanWindow *in, glong field, 
				 ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef glong (*ObitDConCleanWindowSizeFP) (ObitDConCleanWindow *in, 
					      glong field, ObitErr *err);

/** Public: How many pixels are selected  */
glong ObitDConCleanWindowCount (ObitDConCleanWindow *in, glong field, 
				ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef glong (*ObitDConCleanWindowCountFP) (ObitDConCleanWindow *in, 
					     glong field, 
					     ObitDConCleanWindowType type,
					     glong *window, ObitErr *err);

/** Public: find values needed for autoWindow  */
gboolean 
ObitDConCleanWindowAutoWindow (ObitDConCleanWindow *in, 
			       glong field, ObitFArray *image,
			       gboolean doAbs,
			       gfloat *PeakIn, glong *PeakInPos,
			       gfloat *PeakOut, gfloat *RMS,
			       ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef gboolean 
(*ObitDConCleanWindowAutoWindowFP) (ObitDConCleanWindow *in, 
				    glong field, ObitFArray *image,
				    gfloat *PeakIn, glong *PeakInPos,
				    gfloat *PeakOut, gfloat *RMS,
				    ObitErr *err);

/** Public: Replace all windows for a given field with those from another window  */
void 
ObitDConCleanWindowReplaceField (ObitDConCleanWindow *in,  glong ifield, 
				 ObitDConCleanWindow *out, glong ofield,
				 ObitErr *err);
/** Typedef for definition of class pointer structure */
typedef void 
(*ObitDConCleanWindowReplaceFieldFP) (ObitDConCleanWindow *in,  glong ifield, 
				 ObitDConCleanWindow *out, glong ofield,
				 ObitErr *err);

/*----------- ClassInfo Structure -----------------------------------*/
/**
 * ClassInfo Structure.
 * Contains class name, a pointer to any parent class
 * (NULL if none) and function pointers.
 */
typedef struct  {
#include "ObitDConCleanWindowClassDef.h"
} ObitDConCleanWindowClassInfo; 

#endif /* OBITFDCONCLEANWINDOW_H */ 
