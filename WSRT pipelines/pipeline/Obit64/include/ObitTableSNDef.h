/* $Id: ObitTableSNDef.h,v 1.16 2005/10/06 20:22:56 bcotton Exp $   */
/* DO NOT EDIT - file generated by ObitTables.pl                      */
/*--------------------------------------------------------------------*/
/*;  Copyright (C)  2005                                              */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
/*  Define the basic components of the ObitTableSN  structure          */
/*  This is intended to be included in a class structure definition   */
/**
 * \file ObitTableSNDef.h
 * ObitTableSN structure members for derived classes.
 */
#include "ObitTableDef.h"  /* Parent class definitions */
/** Revision number of the table definition */
oint  revision;
/** The number of polarizations */
oint  numPol;
/** The number of IF */
oint  numIF;
/** The number of antennas in table */
oint  numAnt;
/** The number of interpolation nodes */
oint  numNodes;
/** The Mean Gain modulus */
gdouble  mGMod;
/** True if table has been applied to a CL table. */
gboolean  isApplied;
/** Column offset for The center time of the solution in table record */
glong  TimeOff;
/** Physical column number for The center time of the solution in table record */
glong  TimeCol;
/** Column offset for Solution interval. in table record */
glong  TimeIOff;
/** Physical column number for Solution interval. in table record */
glong  TimeICol;
/** Column offset for Source Identifier number in table record */
glong  SourIDOff;
/** Physical column number for Source Identifier number in table record */
glong  SourIDCol;
/** Column offset for Antenna number in table record */
glong  antNoOff;
/** Physical column number for Antenna number in table record */
glong  antNoCol;
/** Column offset for Subarray number. in table record */
glong  SubAOff;
/** Physical column number for Subarray number. in table record */
glong  SubACol;
/** Column offset for Frequency ID in table record */
glong  FreqIDOff;
/** Physical column number for Frequency ID in table record */
glong  FreqIDCol;
/** Column offset for Ionospheric Faraday Rotation in table record */
glong  IFROff;
/** Physical column number for Ionospheric Faraday Rotation in table record */
glong  IFRCol;
/** Column offset for Node number in table record */
glong  NodeNoOff;
/** Physical column number for Node number in table record */
glong  NodeNoCol;
/** Column offset for Multiband delay poln # 1 in table record */
glong  MBDelay1Off;
/** Physical column number for Multiband delay poln # 1 in table record */
glong  MBDelay1Col;
/** Column offset for Real (gain Poln # 1 ) in table record */
glong  Real1Off;
/** Physical column number for Real (gain Poln # 1 ) in table record */
glong  Real1Col;
/** Column offset for Imaginary (gain Poln # 1) in table record */
glong  Imag1Off;
/** Physical column number for Imaginary (gain Poln # 1) in table record */
glong  Imag1Col;
/** Column offset for Residual group delay Poln # 1 in table record */
glong  Delay1Off;
/** Physical column number for Residual group delay Poln # 1 in table record */
glong  Delay1Col;
/** Column offset for Residual fringe rate  Poln # 1 in table record */
glong  Rate1Off;
/** Physical column number for Residual fringe rate  Poln # 1 in table record */
glong  Rate1Col;
/** Column offset for Weight of soln. Poln # 1 in table record */
glong  Weight1Off;
/** Physical column number for Weight of soln. Poln # 1 in table record */
glong  Weight1Col;
/** Column offset for Reference antenna Poln # 1 in table record */
glong  RefAnt1Off;
/** Physical column number for Reference antenna Poln # 1 in table record */
glong  RefAnt1Col;
/** Column offset for Multiband delay poln # 2 in table record */
glong  MBDelay2Off;
/** Physical column number for Multiband delay poln # 2 in table record */
glong  MBDelay2Col;
/** Column offset for Real (gain Poln # 2 ) in table record */
glong  Real2Off;
/** Physical column number for Real (gain Poln # 2 ) in table record */
glong  Real2Col;
/** Column offset for Imaginary (gain Poln # 2) in table record */
glong  Imag2Off;
/** Physical column number for Imaginary (gain Poln # 2) in table record */
glong  Imag2Col;
/** Column offset for Residual group delay Poln # 2 in table record */
glong  Delay2Off;
/** Physical column number for Residual group delay Poln # 2 in table record */
glong  Delay2Col;
/** Column offset for Residual fringe rate  Poln # 2 in table record */
glong  Rate2Off;
/** Physical column number for Residual fringe rate  Poln # 2 in table record */
glong  Rate2Col;
/** Column offset for Weight of soln. Poln # 2 in table record */
glong  Weight2Off;
/** Physical column number for Weight of soln. Poln # 2 in table record */
glong  Weight2Col;
/** Column offset for Reference antenna Poln # 2 in table record */
glong  RefAnt2Off;
/** Physical column number for Reference antenna Poln # 2 in table record */
glong  RefAnt2Col;
