/* $Id: ObitUVWeightDef.h,v 1.1.1.1 2004/07/19 16:42:40 bcotton Exp $                            */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
/*  Define the basic components of the ObitUV structure               */
/*  This is intended to be included in a class structure definition   */
/**
 * \file ObitUVGridDef.h
 * ObitUVGrid structure members for this and any derived classes.
 */
#include "ObitDef.h"  /* Parent class definitions */
/** Weighting sums */
gdouble wtSums[3];
/** Width of convolving kernel in cells */
glong convWidth;
/** Number of of tabulated points per cell in convfn */
glong convNperCell;
/** Size of uv grid (pixels) */
glong nuGrid, nvGrid;
/** Weighting box radius */
glong WtBox;
/** Weighting function index */
glong WtFunc;
/** Number of visibilities out of the inner 90% of the grid */
glong numberBad;
/** Robust weighting parameter */
gfloat Robust;
/**  Weighting power */
gfloat WtPower;
/** Scaling from wavelength to cells for u, v at reference frequency */
gfloat UScale, VScale;
/** -sigma,u,v for taper (in cells) */
gfloat sigma2u, sigma2v;
/** max, min baseline lengths (wavelengths) */
gfloat blmax, blmin;
/** Robust temperance value */
gfloat temperance;
/** Weight scaling (normalization) factor */
gfloat wtScale;
/** Noise increase factor */
gfloat noiseFactor;
/** weight grid  */
ObitFArray *wtGrid;
/** count grid, as gfloat  */
ObitFArray *cntGrid;
/** Gridding convolution table */
ObitFArray *convfn;
