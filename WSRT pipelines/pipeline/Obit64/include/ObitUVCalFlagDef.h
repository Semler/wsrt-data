/* $Id: ObitUVCalFlagDef.h,v 1.2 2005/10/06 20:22:56 bcotton Exp $  */
/*--------------------------------------------------------------------*/
/*;  Copyright (C) 2003                                               */
/*;  Associated Universities, Inc. Washington DC, USA.                */
/*;                                                                   */
/*;  This program is free software; you can redistribute it and/or    */
/*;  modify it under the terms of the GNU General Public License as   */
/*;  published by the Free Software Foundation; either version 2 of   */
/*;  the License, or (at your option) any later version.              */
/*;                                                                   */
/*;  This program is distributed in the hope that it will be useful,  */
/*;  but WITHOUT ANY WARRANTY; without even the implied warranty of   */
/*;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    */
/*;  GNU General Public License for more details.                     */
/*;                                                                   */
/*;  You should have received a copy of the GNU General Public        */
/*;  License along with this program; if not, write to the Free       */
/*;  Software Foundation, Inc., 675 Massachusetts Ave, Cambridge,     */
/*;  MA 02139, USA.                                                   */
/*;                                                                   */
/*;  Correspondence this software should be addressed as follows:     */
/*;         Internet email: bcotton@nrao.edu.                         */
/*;         Postal address: William Cotton                            */
/*;                         National Radio Astronomy Observatory      */
/*;                         520 Edgemont Road                         */
/*;                         Charlottesville, VA 22903-2475 USA        */
/*--------------------------------------------------------------------*/
#ifndef OBITUVCALFLAGDEF_H 
#define OBITUVCALFLAGDEF_H 

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glib.h>
#include "Obit.h"
#include "ObitErr.h"
#include "ObitUVDesc.h"
#include "ObitUVSel.h"
#include "ObitUVCal.h"

/*-------- Obit: Software for the recently deceased ------------------*/
/**
 * \file ObitUVCalFlagDef.h
 * ObitUVCal utilities for applying flagging to uv data 
 */

/*--------------Structure definitions-------------------------------------*/
/** Amp/phase/delay/rate calibration structure */
typedef struct {
  /** Calibration FG table (as Obit*) */
  Obit *FGTable;
  /** FG Table Row (as Obit*) */
  Obit *FGTableRow;
  /** Number of rows in flag table */
  glong numRow;
  /** Last Row read */
  glong LastRowRead;
  /** Number of antennas in flag table (actually max antenna no). */
  gint numAnt;
  /** Number of subarrays in the data */
  gint numSubA;
  /** Number of IFs in data. */
  gint numIF;
  /** Number of Stokes' in data */
  gint numStok;
  /** First Stokes value in data */
  gint stoke0;
  /** Selected Subarray number. <=0-> all */
  glong SubA;
  /** Selected Frequency ID  number. <=0-> all */
  glong FreqID;
  /** Number of channels in data */
  gint numChan;
  /** Time (days) for which the flagging tables is current {TMFLST} */
  gfloat flagTime;
  /** Maximum number of flag table entries {MAXFLG} */
  gint maxFlag;
  /** Current number of flag table entries {NUMFLG} */
  gint numFlag;
  /** Source ID per flag entry {FLGSOU} */
  gint *flagSour;
  /** Antenna number per flag entry  {FLGANT} */
  gint *flagAnt;
  /** Baseline number (A1*256+A2) per flag entry  {FLGBAS} */
  gint *flagBase;
  /**  Subarray number per flag entry  {FLGSUB} */
  gint *flagSubA;
  /**  Freqid numbers per flag entry  {FLGFQD} */
  gint *flagFQID;
  /**  First IF per flag entry  {FLGBIF} */
  gint *flagBIF;
  /**  Highest IF per flag entry  {FLGEIF} */
  gint *flagEIF;
  /**  First channel per flag entry  {FLGBCH} */
  gint *flagBChan;
  /**  Highest channel per flag entry  {FLGECH} */
  gint *flagEChan;
  /**  Flags for the polarizations per flag entry  {FLGPOL} 
   Note: there are 4*numFlag values */
  gboolean *flagPol;
  /**  End time of vslidity per flag entry  {FLGTND} */
  gfloat *flagEndTime;
} ObitUVCalFlagS;
#endif /* OBITUVCALFLAGDEF_H */ 


