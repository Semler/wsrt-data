#!/usr/bin/env python
from WSRTrecipe import *
import davlib

class get_readme(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MeasurementSet'] = ''
        self.inputs['Origin']         = 'wop17'
        self.inputs['InspectionPath'] = '/raid/MS/info'
        self.inputs['filepath']       = '.'
        
        ## List of outputs
        self.outputs['MeasurementSet'] = ''
        self.outputs['ReadmeFile']     = ''
        
        ## Help text
        self.helptext = """
        obtains MS info from glish script"""
    
    ## Code to generate results ---------------------------------------------
    def getConfig(self, name=None): ## also using the FTP config for WebDAV as it contains the same user/password
        ## Find the config file     ## might need to change in the future
        import os.path, re
        r_field = re.compile(r'(?s)([^\n:]+): (.*?)(?=\n[^ \t]|\Z)')
        cwd = os.path.realpath(os.path.dirname(__file__))  + '/'
        config  = cwd + '.nftprc'
        self.print_debug('using FTP cofiguration in: ' + config)
        if os.path.exists(config): 
            s = open(config).read()
        else: 
            raise RecipeError ('FTP config not found')
    
        ## Parse the config file
        conf = {}
        s = s.replace('\r\n', '\n')
        s = s.replace('\r', '\n')
        for item in s.split('\n\n'):
            meta = dict(r_field.findall(item.strip()))
            if meta.has_key('name'):
                fname = meta['name']
                del meta['name']
                conf[fname] = meta
            else: raise RecipeError ('FTP config must include a name')
    
        if name is not None: 
            return conf[name]
        else: return conf

    def wd_get(self, filename):
        import binascii
        meta      = self.getConfig(self.inputs['Origin'])
        auth      = binascii.b2a_base64(meta['username'] + ':' + meta['password'])
        self.dav  = davlib.DAV(meta['host'], meta['port']) 
#        self.dav.set_debuglevel(1) # for testing
        self.dav.connect()
        response = self.dav.get(filename, {'Authorization': 'Basic %s' % auth.strip()})
        self.print_debug('Status = %s, Reason = %s, Version = %s' % (response.status, response.reason, response.version))
        readme = response.read()
        if not (response.version >= 10):
            raise RecipeError ('Unknown protocol version:' + str(response.version))
        if not (response.status == 200 or response.status == 404):
            self.print_error('Got unrecognised answer with Status = %s, Reason = %s' % (response.status, response.reason))
            raise RecipeError ('Problem logging in to WebDAV default repository: ' + meta['host']+ ':' + str(meta['port']) + self.inputs['remotedir'])
        response.close()
        self.dav.close()
        if response.status == 200:
            return readme
        else:
            return ''

    def go(self):
        ## the location is something like
        ## http://wop17.nfra.nl/raid/MS/info/2006/10600334/SUBARRAY0/TIMERANGE0/10600334.readme
        import os, shutil
        ms    = self.inputs['MeasurementSet']
        posS  = ms.find('_S')
        posT  = ms.find('_T')
        posM  = ms.find('.MS')
        ObsNr = ms[0:posS]
        seq   = ms[posS+2:posT]
        tslot = ms[posT+2:posM]
        if ms[0] == '1':
            year = '20' + ms[1:3]
        else:
            year = '19' + ms[0:2]
        filename = (self.inputs['InspectionPath'] + 
                    '/' + year +
                    '/' + ObsNr + 
                    '/SUBARRAY' + seq + 
                    '/TIMERANGE' + tslot + 
                    '/' + ObsNr + '.readme')
        readme = self.wd_get(filename)
        if readme:
             destfile = str(ObsNr + '_S' + seq + '_T' + tslot + '.readme')
             binfile  = open(self.inputs['filepath'] + '/' + destfile, 'w+')
             print binfile.name
             print len(readme)
             binfile.write(readme)
             binfile.close()
             self.outputs['ReadmeFile'] = destfile
        else:
            self.outputs['ReadmeFile'] = ''
        self.outputs['MeasurementSet'] = self.inputs['MeasurementSet']
    
## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = get_readme()
    standalone.main()
