#!/usr/bin/env python
from WSRTrecipe import *
from ingredient import *

class miriad_single_band_clean(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MiriadUvFile'] = ''
        self.inputs['DynamicRange'] = 1000.0
        self.inputs['ClipRatio']    = 10.0
        self.inputs['DoAmplitude']  = 'F'
        self.inputs['UseMfs']       = False
        self.inputs['Interval']     = 1
        self.inputs['cellsize']     = 4.0
        self.inputs['filepath']     = '.'
        
        ## List of outputs
        self.outputs['CleanMap'] = ''
        
        ## Help text
        self.helptext = """
        Script to automatically reduce single spectral line
        measurements from the WSRT. Possible Difficulty settings
        are: Hard, Standard, Easy."""
    
## Code to generate results ---------------------------------------------
    def go(self):
        import os, os.path
        BandUv   = self.inputs.copy()
        ClipStep = 2.5
        RmsFac   = 4.0
        MinClip  = 0.00075
    
        BandUv['stokes'] = ['ii']
        self.cook_recipe('miriad_invert', BandUv, BandUv)
        ## Should have created map_['stokes'] and beam
        BandUv['Map']  =  filter(lambda x: x[-6:] == 'map_ii', BandUv['MiriadImages'])[0]
        BandUv['Beam'] =  filter(lambda x: x[-4:] == 'beam', BandUv['MiriadImages'])[0]
        
        BandUv['MiriadImage'] = BandUv['Map']
        self.cook_recipe('get_image_max', BandUv, BandUv)
        BandUv['ClipLevel']    = BandUv['max'] / ClipStep 
    
        BandUv['TestMap'] = BandUv['Map']
        self.cook_recipe('miriad_clean', BandUv, BandUv)
        ## Should have created cleanmap, model, mask
    
        BandUv['TestMap']      = BandUv['CleanMap']
        BandUv['OldClipLevel'] = BandUv['ClipLevel']
        BandUv['ClipLevel']    = BandUv['ClipLevel'] / ClipStep 
        while (BandUv['ClipLevel'] < 0.95 * BandUv['OldClipLevel']):
            BandUv['MiriadImage'] = BandUv['CleanMap']
            BandUv['OldClipLevel'] = BandUv['ClipLevel']
            BandUv['ClipLevel']    = BandUv['ClipLevel'] / ClipStep 
            self.cook_recipe('get_noise', BandUv, BandUv)
            if (BandUv['ClipLevel'] < RmsFac * BandUv['noise']):
                BandUv['ClipLevel'] = RmsFac * BandUv['noise']
            if (BandUv['ClipLevel'] < BandUv['max'] / BandUv['DynamicRange']): 
                BandUv['ClipLevel'] = BandUv['max'] / BandUv['DynamicRange']
            if (BandUv['ClipLevel'] < MinClip): 
                BandUv['ClipLevel'] = MinClip
            if (BandUv['ClipLevel'] < 0.95 * BandUv['OldClipLevel']):
                self.cook_recipe('miriad_clean', BandUv, BandUv)
    
        if (BandUv['ClipRatio'] >= 0.0):
            BandUv['MiriadImage'] = BandUv['Model']
            self.cook_recipe('get_image_max', BandUv, BandUv)
            BandUv['ClipLevel'] = BandUv['max'] / BandUv['ClipRatio']
        else:
            BandUv['ClipLevel'] = BandUv['ClipRatio']
    
        self.cook_recipe('miriad_selfcal', BandUv, BandUv)
    
        self.outputs['CleanMap'] = BandUv['CleanMap']

## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = miriad_single_band_clean()
    standalone.main()

