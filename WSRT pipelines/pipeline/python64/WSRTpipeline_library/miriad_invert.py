#!/usr/bin/env python
from WSRTrecipe import *

class miriad_invert(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MiriadUvFile'] = ''
        self.inputs['stokes']       = ['ii', 'q', 'u', 'v']
        self.inputs['ImageSize']    = 2049
        self.inputs['UseMfs']       = False
        self.inputs['cellsize']     = 4.0
        self.inputs['filepath']     = '.'
        
        ## List of outputs
        self.outputs['MiriadUvFile'] = ''
        self.outputs['Beam']         = ''
        self.outputs['MiriadImages'] = []
        
        ## Help text
        self.helptext = """
        Makes images: 'map_ii', 'map_q', 'map_u', 'map_v', 'beam' uses
        invert located in $MIRLIB
        Step 2.10.2 and 2.11.3 in SWOM"""
    
## Code to generate results ---------------------------------------------
    def go(self):
        import os, string

        infile = self.inputs['filepath'] + '/' + self.inputs['MiriadUvFile']
        maps = []
        for s in self.inputs['stokes']:
            maps.append(infile + '_map_' + s)

        for m in maps:  # this should happen at a higher level ??
            self.zap(m)
        self.zap(infile + '_beam')

        ## check which files already exist, should happen after the zap's!
        AlreadyExisted = os.listdir(self.inputs['filepath'])
        if self.inputs['UseMfs']: options = 'double,mfs'
        else:                     options = 'double'

##        self.cook_system('/dop89_0/oosterloo/suse9.2-32/wsrtMiriad/bin/linux/invertMore', ' vis=' + infile + 
        self.cook_system('invert', ' vis=' + infile + 
                                   ' imsize=' + str(self.inputs['ImageSize']) + 
                                   ' robust=-1 stokes=' + string.join(self.inputs['stokes'], ',') + # maybe stokes should be build dynamically?
                                   ' cell=' + str(round(self.inputs['cellsize'], 3)) +
                                   ' slop=0.9' + 
                                   ' beam=' + infile + '_beam' + 
                                   ' map=' + string.join(maps, ',') +
                                   ' options=' + options)

        ## check to see what we created
        self.outputs['MiriadImages'] = []
        list = os.listdir(self.inputs['filepath'])
        for file in list:
            if file not in AlreadyExisted:
                self.outputs['MiriadImages'].append(file)

        if self.outputs['MiriadImages']:
            self.outputs['Beam']         = self.inputs['MiriadUvFile'] + '_beam'
            self.outputs['MiriadUvFile'] = self.inputs['MiriadUvFile']
        else:
            self.outputs['MiriadUvFile'] = ''
            self.outputs['Beam']         = ''
            raise RecipeError ('Invert failed, no images created.')

## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = miriad_invert()
    standalone.main()
