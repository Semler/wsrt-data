#!/usr/bin/env python
from WSRTrecipe import *

class miriad_selfcal(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MiriadUvFile'] = ''
        self.inputs['Model']        = ''
        self.inputs['ClipLevel']    = 0.004
        self.inputs['DoAmplitude']  = False
        self.inputs['Interval']     = 1
        self.inputs['UseMfs']       = False
        self.inputs['refant']       = 1
        self.inputs['filepath']     = '.'
        
        ## List of outputs
        self.outputs['MiriadUvFile'] = ''
        
        ## Help text
        self.helptext = """
        Selfcalibrates the Miriad uvfile using the model
        it uses selfcal and puthd found in $MIRLIB
        Step 2.11.8 in SWOM"""
    
## Code to generate results ---------------------------------------------
    def go(self):
        if self.inputs['UseMfs']: options = ',mfs'
        else:                     options = ''
        select  = ''
        if (self.inputs['DoAmplitude']): 
            options = 'amplitude' + options
        else:
            options = 'phase' + options
        vis = self.inputs['filepath'] + '/' + self.inputs['MiriadUvFile']
        self.cook_miriad('puthd', ' in=' + vis + '/obstype value=crosscorrelation-2009')    
        self.cook_miriad('selfcal', ' vis=' + self.inputs['filepath'] + '/' + self.inputs['MiriadUvFile'] + 
                                    ' model=' + self.inputs['filepath'] + '/' + self.inputs['Model'] + 
                                    ' refant=' + str(self.inputs['refant']) +
                                    ' options=' + options + 
                                    ' clip=' + str(self.inputs['ClipLevel']) + 
                                    ' interval=' + str(self.inputs['Interval']) + select)
    
        self.cook_miriad('puthd', ' in='  + self.inputs['filepath'] + '/' + self.inputs['MiriadUvFile'] + '/interval'
                                  ' value=1.0 type=double') # should be interval ??
        
        # We should only do this on succes
        self.outputs['MiriadUvFile'] = self.inputs['MiriadUvFile']
    
## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = miriad_selfcal()
    standalone.main()
