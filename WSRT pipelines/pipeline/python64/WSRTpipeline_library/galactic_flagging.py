#!/usr/bin/env python
from WSRTrecipe import *

class galactic_flagging(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MiriadUvFile'] = ''
        self.inputs['dvel']         = 300.0
        self.inputs['filepath']     = '.'
        
        ## List of outputs
        self.outputs['MiriadUvFile'] = ''
        
        ## Help text
        self.helptext = """
        Flag channels containing galactic emission"""
    
## Code to generate results ---------------------------------------------
    def go(self):
        import os, mirlib
        from constants import lightspeed, h21cm
        vis = self.inputs['filepath'] + '/' + self.inputs['MiriadUvFile']
        ## frequencies affected by galactic emission
        galactic_low  = (1.0 - 0.5*self.inputs['dvel']/lightspeed) * h21cm
        galactic_high = (1.0 + 0.5*self.inputs['dvel']/lightspeed) * h21cm
        self.print_message('Galactic emission can be found between %s and %s GHz' % (galactic_low, galactic_high))
        ## Find out the number of bands in the data
        nspect = mirlib.varvalue(vis, "nspect")[0]
        
        ## Find out the number of channels in the data
        nschan = mirlib.varvalue(vis, "nschan")

        ## Find the frequency axis of the band
        sdf   = mirlib.varvalue(vis, "sdf")
        sfreq = mirlib.varvalue(vis, "sfreq")
    
        ## Check each band
        for w in range(nspect):
            if (sdf[w] < 0.0): ## make sure frequency increases with index
                sfreq_p = sfreq[w] + nschan[w] * sdf[w]
                sdf_p   = -sdf[w]
                neg     = 1
            else:
                sfreq_p = sfreq[w]
                sdf_p   = sdf[w]
                neg     = 0
                
            index_low = int((galactic_low - sfreq_p) / sdf_p)
                          
            if (index_low > 0 and index_low < nschan[w]):
                self.print_message('Galactic emission in band %s,' % (w))
                if (neg):
                    index_low  = max(1, int((galactic_high - sfreq[w]) / sdf[w]))
                    index_high = min(nschan[w], int((galactic_low - sfreq[w]) / sdf[w]))
                else:
                    index_high = min(nschan[w], int((galactic_high - sfreq_p) / sdf_p))
                self.print_message('between channels %s and %s\n' % (index_low,index_high))
                band_str = " select=window(" + str(w+1) + ")"
                line_str = " line=channel," + str(index_high - index_low) + "," + str(index_low)

                self.cook_miriad('uvflag', ' vis=' + vis + 
                                             band_str + line_str + 
                                           ' flagval=flag')

        # We should only do this on succes
        self.outputs['MiriadUvFile'] = self.inputs['MiriadUvFile']
    
## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = galactic_flagging()
    standalone.main()

