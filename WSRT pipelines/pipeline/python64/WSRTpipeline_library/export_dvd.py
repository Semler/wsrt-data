#!/usr/bin/env python
from WSRTrecipe import *
from ingredient import *

class export_dvd(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['ExportID']        = None
        self.inputs['Jobs']            = []
        self.inputs['filepath']        = '.'
        self.inputs['Medium']          = 'DVD'
        self.inputs['StructureDetail'] = '/'
        self.inputs['ZapExisting']     = True

        ## List of outputs
        self.outputs['Inventory'] = []
        
        self.observer = WSRTingredient()

        ## Help text
        self.helptext = """
        Script to automatically copy the files identified by
        Jobs to the selected Medium."""

    ## Code to generate results ---------------------------------------------
##    def send_observer(self, observer, text):
##        """write some output to the observer pipe"""
##        pipe = open(observer, 'w')
##        pipe.write(text + '\n')
##        pipe.close()
##
    def get_observer(self, observer):
        """Read some input from the observer pipe"""
        pipe = open(observer, 'r')
        text = pipe.read()
        pipe.close()
        return text

    def create_jobdir(self):
        """Create the job directory, delete a previous one if ZapExisting"""
        import os, os.path
        Dir = self.inputs['filepath'] + '/' + str(self.inputs['ExportID'])
        if os.path.isdir(Dir):
            if self.inputs['ZapExisting']:
                self.zap(Dir)
                self.print_debug('Deleted: ' + Dir)
            else:
                raise RecipeError ('Directory already exists: ' + Dir)
        os.makedirs(Dir)
        return Dir

    def walk(self, path, subdir, target):
        """walks the path + subdir and returns total size"""
        import os
        total = 0
        if os.path.isfile(path  + '/' + subdir + '/' + target):
            s = os.stat(path  + '/' + subdir + '/' + target) ## how big is this file?
            total += s[6] # the size is the sixth element in the tuplet, should replace with s.st_size?
        else:
            for root, dirs, files in os.walk(path  + '/' + subdir + '/' + target):
                dir = root.replace(path  + '/' + subdir + '/', '')
                for f in files:
                    s = os.stat(path + '/' + subdir + '/' + dir + '/' + f) ## how big is this file?
                    total += s[6] ## the size is the sixth element in the tuplet
        return total

    def create_disklist(self, Dir):
        """parse the result pickles from the jobs we need to write"""
        import pickle, os.path, shutil
        disks = [{'size':0, 'files':[], 'targets':[], 'index':0}]
        if self.inputs['Medium'] == 'DVD2':
            disksize = 8200000000L
        elif self.inputs['Medium'] == 'DVD':
            disksize = 4300000000L
        elif self.inputs['Medium'] == 'CD':
            disksize =  630000000L

        index = 0
        for j in self.inputs['Jobs']:
            try:
                fd = open(self.inputs['filepath'] + '/' + str(j) + '/results.pickle')
                results = pickle.load(fd)
                for k in results.keys():
                    if k in ['MeasurementSets', 'UvfitsFiles', 'MiriadUvFiles', 'ScnFiles', 'ReadmeFiles']:
                        list = results[k]
                        if   k == 'MeasurementSets': type = 'aips++'
                        elif k == 'UvfitsFiles'    : type = 'UVFits'
                        elif k == 'MiriadUvFiles'  : type = 'Miriad'
                        elif k == 'ScnFiles'       : type = 'Newstar'
                        elif k == 'ReadmeFiles'    : type = 'readme'
                        for i in range(0, len(list)):
                            total = self.walk(self.inputs['filepath'],
                                              str(j) + '/' +
                                              str(results['SequenceNumber']),
                                              list[i])
                            if total > disksize:
                                raise RecipeError("Data doesn't fit on Medium")
                            if (disks[index]['size'] + total) > disksize:
                                index += 1
                                disks.append({'size':0, 'targets':[], 'index':index})
                            disks[index]['size'] += total
                            info = results['MSinfo'][i].copy() ## always using the MSinfo
                            info.update({'filedir':list[i], 'type':type,
                                         'location':self.inputs['filepath'] + '/' +
                                                    str(j) + '/' + 
                                                    str(results['SequenceNumber'])})
                            disks[index]['targets'].append(info)
            except:
                raise RecipeError ('loading and reading pickle for job failed: ' + str(j))
        return disks

    def create_links_and_volumes(self, disks, Dir):
        import os
        from string import ascii_uppercase as Capitals
        for d in disks:
            name  = d['targets'][0]['Semester'] + '-' + d['targets'][0]['PrjNr']
            name += Capitals[d['index']]
            DiskID = str(self.inputs['ExportID']) + '_' + str(d['index'])
            os.makedirs(Dir + '/' + DiskID + '/datasets')
            os.makedirs(Dir + '/' + DiskID + '/inspectionfiles')
            for t in d['targets']:
                if t['type'] == 'readme':
                    os.symlink(t['location'] + '/' + t['filedir'], 
                               Dir + '/' + DiskID + '/inspectionfiles' + '/' + t['filedir'])
                else:
                    os.symlink(t['location'] + '/' + t['filedir'], 
                               Dir + '/' + DiskID + '/datasets' + '/' + t['filedir'])
            status = self.cook_interactive('mkisofs', '-f -l -R -o ' + Dir + '/' + DiskID + '.iso -V ' + name + 
                                           ' ' + Dir + '/' + DiskID, [])
            if status:
                self.observer['text'] = 'Creating ISO iage failed, aborting...'
                self.cook_recipe('writeObserverXML', self.observer, self.observer)
                raise RecipeError ('Creating ISO failed errorcode: ' + status)

    def write_disks(self, disks, Dir):
        targets = []
        for d in disks:
            DiskID = str(self.inputs['ExportID']) + '_' + str(d['index'])
            self.observer['text']   = 'Please insert disk: ' + str(d['index'] + 1)
            self.observer['number'] = d['index'] + 1
            self.observer['action'] = '\\n' ## I don't realy care what the observer types, but need to wait until something happens.
            self.cook_recipe('writeObserverXML', self.observer, self.observer)
            self.get_observer('/users/pipeline/observer')
            self.observer['action'] = None
            self.observer['text']   = 'Received acknowledgement, now burning disk.'
            self.cook_recipe('writeObserverXML', self.observer, self.observer)
            status = self.cook_interactive('cdrecord-prodvd', '-v -sao -driveropts=burnfree -eject -speed=4 -dev=1,0,0 -fs=16384k ' + 
                                           Dir + '/' + DiskID + '.iso', [])
            if status:
                self.observer['text'] = 'Burning disk failed with error: ' + str(status) + ', aborting...'
                self.cook_recipe('writeObserverXML', self.observer, self.observer)
                raise RecipeError ('Burning CD/DVD failed errorcode: ' + str(status))
            else:
                self.observer['text'] = 'Burning disk succeeded, printing booklet.'
                self.cook_recipe('writeObserverXML', self.observer, self.observer)
            
            targets.append({'size':d['size'], 'number':d['index'] + 1, 'targets':d['targets']})
            temp = WSRTingredient()
            temp['filepath'] = Dir
            temp['XMLdata']  = d['targets']
            temp['CDindex']  = str(d['index'] + 1)
            self.cook_recipe('create_cd_booklet', temp, temp)
        return targets

    def go(self):
        import os
        Dir    = self.create_jobdir()
        disks  = self.create_disklist(Dir)
        self.create_links_and_volumes(disks, Dir)
        self.observer['exportID'] = self.inputs['ExportID']
        self.observer['medium']   = self.inputs['Medium']
        self.observer['number']   = 0
        self.observer['total']    = len(disks)
        self.observer['filepath'] = '/users/pipeline'
        CreatedDisks = self.write_disks(disks, Dir)
        self.observer['text']     = 'Finished'
        self.cook_recipe('writeObserverXML', self.observer, self.observer)
        self.outputs['Inventory'] = CreatedDisks

## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = export_dvd()
    standalone.main()

