#!/usr/bin/env python
from WSRTrecipe import *
from ingredient import *
import ftplib
## Based on code from nftp.py by
## Author: Sean B. Palmer, inamidst.com
## License: GNU GPL 2
## Date: 2003-11

class export_ftp(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['ExportID']        = None
        self.inputs['Jobs']            = []
        self.inputs['destination']     = 'localhost'
        self.inputs['filepath']        = '.'
        self.inputs['remotedir']       = ''
        self.inputs['StructureDetail'] = '/'
        
        ## List of outputs
        self.outputs['Inventory'] = []
        self.outputs['files']     = []
        
        ## Help text
        self.helptext = """
        Script to automatically move the files identified by
        JobNumbers to destination and report the moved files"""
    
## Code to generate results---------------------------------------------
    def getConfig(self, name=None): 
        ## Find the config file
        import os.path, re
        r_field = re.compile(r'(?s)([^\n:]+): (.*?)(?=\n[^ \t]|\Z)')
        cwd = os.path.realpath(os.path.dirname(__file__))  + '/'
        config  = cwd + '.nftprc'
        self.print_debug('using FTP cofiguration in: ' + config)
        if os.path.exists(config): 
            s = open(config).read()
        else: 
            raise RecipeError ('FTP config not found')
    
        ## Parse the config file
        conf = {}
        s = s.replace('\r\n', '\n')
        s = s.replace('\r', '\n')
        for item in s.split('\n\n'):
            meta = dict(r_field.findall(item.strip()))
            if meta.has_key('name'):
                fname = meta['name']
                del meta['name']
                conf[fname] = meta
            else: raise RecipeError ('FTP config must include a name')
    
        if name is not None: 
            return conf[name]
        else: return conf
    
    def getFtp(self, meta):
        """Login to the FTP host"""
        ftp = ftplib.FTP(meta['host'], meta['username'], meta['password'])
        return ftp

    def upload(self, ftp, meta, target): 
        import os.path
        path, filename = os.path.split(target[1])
        if filename.find('.readme') >= 0: #quick hack to detect readme files
            path = '/inspectionfiles/' + path
        else:
            path = '/datasets/' + path
        try: ftp.cwd(meta['remotedir'] + path)
        except ftplib.error_perm, e: 
            self.print_message('Creating folder %s' % path)
            ftp.cwd(meta['remotedir'])
            for folder in path.split('/'): 
                try: ftp.cwd(folder)
                except: 
                    ftp.mkd(folder)
                    ftp.cwd(folder)
        file = open(target[0] + '/' + target[1], 'rb')
        self.print_message('Storing %s' % target[0] + '/' + target[1])
        try:
            ## not doing anything with result ftplib seems to raise an exception if something's wrong
            result = ftp.storbinary('STOR %s' % filename, file) 
        except Exception, e:
            self.print_error('Unable to store file %s' % target[1])
            file.close()
            raise RecipeError ('FTP transfer failed with error: ' + str(e))
        file.close()

    def walk(self, path, subdir, target):
        """walks the path + subdir and returns subdir/*.*, total size in (targets, total)
        targets: source = [0]+[1], destination=[1]"""
        import os
        targets = []
        if os.path.isfile(path  + '/' + subdir + '/' + target):
            targets.append((path + '/' + subdir, target)) ## source = [0]+[1], destination=[1]
        else:
            for root, dirs, files in os.walk(path  + '/' + subdir + '/' + target):
                dir = root.replace(path  + '/' + subdir + '/', '')
                for f in files:
                    targets.append((path  + '/' + subdir, dir + '/' + f)) ## source = [0]+[1], destination=[1]
        return targets

    def go(self):
        import os.path, pickle
        targets = []
        files   = []
        for j in self.inputs['Jobs']:
            try:
                fd = open(self.inputs['filepath'] + '/' + str(j) + '/results.pickle')
                results = pickle.load(fd)
                number  = results['SequenceNumber']
                for k in results.keys():
                    if k in ['MeasurementSets', 'UvfitsFiles', 'MiriadUvFiles', 'ScnFiles', 'ReadmeFiles']:
                        for f in results[k]:
                            targets.append(str(j) + '/' + str(number) + '/' + f)
                            files.extend(self.walk(self.inputs['filepath'],
                                         str(j) + '/' +
                                         str(results['SequenceNumber']), f))
            except:
                raise RecipeError ('loading and reading pickle for job failed: ' + str(j))
        meta = self.getConfig(self.inputs['destination'])
#not sure        if meta['localdir'] != self.inputs['filepath']:
#if needed            raise RecipeError ('Incorrect local directory')
        meta['remotedir'] = self.inputs['remotedir'] # there might be a more elegant solution?
        ftp  = self.getFtp(meta)
        for f in files:
            self.upload(ftp, meta, f)

        self.outputs['Inventory'] = targets
#            self.outputs['files']     = files

## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = export_ftp()
    standalone.main()

