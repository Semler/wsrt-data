#!/usr/bin/env python
from WSRTrecipe import *

class miriad_inspect_uvdata(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
        ## List of inputs with defaults
        self.inputs['MiriadUvFile'] = ''
        self.inputs['filepath']     = '.'
        
        ## List of outputs
        self.outputs['MiriadUvFile']            = ''
        self.outputs['NumberOfChannels']        = 0
        self.outputs['NumberOfSpectralWindows'] = 0
        self.outputs['ChannelWidth']            = 0.0

        ## Help text
        self.helptext = """
        Inspect the given Miriad uv file and return it's
        number of channels (nchan), number of spectral windows
        (nspec) and channel width (sdf)."""
    
    ## Code to generate results ---------------------------------------------
    def go(self):
        import mirlib
        infile = self.inputs['filepath'] + '/' + self.inputs['MiriadUvFile']
        self.outputs['NumberOfSpectralWindows'] =     mirlib.varvalue(infile, 'nspect')[0]
        self.outputs['ChannelWidth']            = abs(mirlib.varvalue(infile, 'sdf')[0])
        self.outputs['NumberOfChannels']        =     mirlib.varvalue(infile, 'nchan')[0]
        self.outputs['MiriadUvFile']            = self.inputs['MiriadUvFile']

## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = miriad_inspect_uvdata()
    standalone.main()

