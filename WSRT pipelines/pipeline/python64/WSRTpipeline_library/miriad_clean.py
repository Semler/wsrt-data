#!/usr/bin/env python
from WSRTrecipe import *
from ingredient import *

class miriad_clean(WSRTrecipe):
    def __init__(self):
        WSRTrecipe.__init__(self)
## List of inputs with defaults
        self.inputs['Map']          = ''
        self.inputs['Beam']         = ''
        self.inputs['TestMap']      = ''
        self.inputs['ClipLevel']    = 0.0
        self.inputs['ExtendedSize'] = 700
        self.inputs['filepath']     = '.'
        
## List of outputs
        self.outputs['Model']    = ''
        self.outputs['CleanMap'] = ''

## Help text
        self.helptext = """
        Makes 'Model' and 'CleanMap'
        SWOM 2.11.4,5,7 in SWOM"""
    
## Code to generate results ---------------------------------------------
    def go(self):
        import os, shutil
        cwd = os.getcwd() + '/'
        Temp = WSRTingredient()
        os.chdir(self.inputs['filepath'])
        cutFac  = 2.5
        Temp['filepath']    = '.'
        Temp['MiriadImage'] = self.inputs['Map']
        self.cook_recipe('get_image_max', Temp, Temp)
        shutil.move(self.inputs['Map'], 'c' + self.inputs['Map'])
        if (self.inputs['TestMap'] != self.inputs['Map']):
            shutil.move(self.inputs['TestMap'], 'c' + self.inputs['TestMap'])
## maths doesn't like names starting with /, ~ or a number, or have a . in them !
## Make mask for clean, should be able to use a previous one
        if self.inputs['ClipLevel'] > Temp['max']/50:
            self.cook_miriad('maths', ' exp='  + 'c' + self.inputs['Map'] +
                                      ' out='  + 'c' + self.inputs['Map'] + '_mask' +
                                      ' mask=' + 'c' + self.inputs['TestMap'] +
                                      '.gt.' + str(self.inputs['ClipLevel']))
        else: ## do some extra stuff for extended emission
            self.cook_miriad('maths', ' exp='  + 'c' + self.inputs['Map'] +
                                      ' out='  + 'c' + self.inputs['Map'] + '_std' +
                                      ' mask=' + 'c' + self.inputs['TestMap'] +
                                      '.gt.' + str(self.inputs['ClipLevel']))
            Temp['MiriadImage'] = 'c' + self.inputs['TestMap']
            self.cook_recipe('get_image_max', Temp, Temp) ## else convol complains ?
            self.cook_miriad('convol', ' map='  + 'c' + self.inputs['TestMap'] +
                                       ' out='  + 'c' + self.inputs['TestMap'] + '_conv' +
                                       ' fwhm=45') ## should emphasize extended emission.
            self.cook_miriad('maths', ' exp='  + 'c' + self.inputs['Map'] +
                                      ' out='  + 'c' + self.inputs['Map'] + '_tmp' +
                                      ' mask=' + 'c' + self.inputs['TestMap']  + '_conv' +
                                      '.gt.' + str(float(self.inputs['ClipLevel']) * 2))
            self.cook_system('mirMaskComb', ' inLow=' + 'c' + self.inputs['Map'] + '_tmp' +
                                            ' inHig=' + 'c' + self.inputs['Map'] + '_std' +
                                            ' minsize=' + str(self.inputs['ExtendedSize']) +
                                            ' out=' + 'c' + self.inputs['Map'] + '_mask')
        shutil.move('c' + self.inputs['Map'],  self.inputs['Map'])
        if (self.inputs['TestMap'] != self.inputs['Map']):
            shutil.move('c' + self.inputs['TestMap'],  self.inputs['TestMap'])
        shutil.move('c' + self.inputs['Map'] + '_mask', self.inputs['Map'] + '_mask')
        os.chdir(cwd)
        self.zap(self.inputs['filepath'] + '/' +'c' + self.inputs['Map'] + '_std')
        self.zap(self.inputs['filepath'] + '/' +'c' + self.inputs['TestMap']  + '_conv')
        self.zap(self.inputs['filepath'] + '/' +'c' + self.inputs['Map'] + '_tmp')
## Clean
        map  = self.inputs['filepath'] + '/' + self.inputs['Map']
        tmap = self.inputs['filepath'] + '/' + self.inputs['TestMap']
        beam = self.inputs['filepath'] + '/' + self.inputs['Beam']
        self.zap(map + '_model') # not happy with this ??
        cutoff = float(self.inputs['ClipLevel']) / cutFac;
        self.cook_miriad('clean', ' map='  + map + '_mask' +
                                  ' beam=' + beam +
                                  ' out='  + map + '_model' +
                                  ' cutoff=' + str(cutoff) + 
                                  ' niters=100000')
        self.zap(map + '_mask')
## Restore
        self.zap(map + '_cleanmap') # not happy with this ??
        self.cook_miriad('restor', ' map='  + map + 
                                   ' model=' + map + '_model' +
                                   ' beam='  + beam +
                                   ' out=' + map + '_cleanmap')

        self.outputs['Model']    = self.inputs['Map'] + '_model'
        self.outputs['CleanMap'] = self.inputs['Map'] + '_cleanmap'
    
## Stand alone execution code ------------------------------------------
if __name__ == '__main__':
    standalone = miriad_clean()
    standalone.main()
