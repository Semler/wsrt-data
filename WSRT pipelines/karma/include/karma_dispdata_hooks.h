/*  karma_dispdata_hooks.h

    Header for  dispdata_  package. This file ONLY contains the hook functions

    Copyright (C) 2002-2003  Richard Gooch

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    Richard Gooch may be reached by email at  karma-request@atnf.csiro.au
    The postal address is:
      Richard Gooch, c/o ATNF, P. O. Box 76, Epping, N.S.W., 2121, Australia.
*/

/*

    This include file contains all the hook definitions for the dispdata_
    package in the Karma library.


    Written by      Richard Gooch   29-DEC-2002: Moved from
  karma_dispdata_def.h

    Last updated by Richard Gooch   10-SEP-2003

*/
#ifndef KARMA_DISPDATA_HOOKS_H
#define KARMA_DISPDATA_HOOKS_H


typedef void (*KDisplayDataFuncSetClassAttributes) (va_list argp);
typedef void (*KDisplayDataFuncDestroyDataObject) (void *dataobject);
typedef flag (*KDisplayDataFuncCopyDataAttributes) (void *dest, void *source);
typedef flag (*KDisplayDataFuncSetDataAttributes) (void *data, va_list argp);
typedef flag (*KDisplayDataFuncGetDataAttributes) (void *data, va_list argp);
typedef void *(*KDisplayDataFuncCreateWindow) (KDisplayDataWindow superclass,
					       KDisplayDataCanvas *pc,
					       KDisplayDataCanvas *rgb);
typedef void (*KDisplayDataFuncDestroyWindow) (void *window);
typedef flag (*KDisplayDataFuncSetWindowAttributes) (void *win, va_list argp);
typedef flag (*KDisplayDataFuncGetWindowAttributes) (void *win, va_list argp);
typedef void *(*KDisplayDataFuncCreateViewable) (void *data, void *window);
typedef void (*KDisplayDataFuncDestroyViewable) (void *viewable,
						 flag canvas_destroy);
typedef void *(*KDisplayDataFuncCreateBlinkState)
     (KDisplayDataClassBlinkState superclass, void *window);
typedef void (*KDisplayDataFuncDestroyBlinkState) (void *blinkstate);
typedef void (*KDisplayDataFuncActivateBlinkState) (void *blinkstate,
						    flag before_blinkentries);
typedef void *(*KDisplayDataFuncCreateBlinkEntry) (void *blinkstate,
						   void *viewable);
typedef flag (*KDisplayDataFuncDestroyBlinkEntry) (void *blinkentry);
typedef void (*KDisplayDataFuncCopyBlinkEntry) (void *dest, void *source);
typedef KWorldCanvas (*KDisplayDataFuncActivateBlinkEntry) (void *blinkentry,
							    flag *visible);
typedef flag (*KDisplayDataFuncSetBlinkEntryAttributes) (void *blinkentry,
							 va_list argp);
typedef flag (*KDisplayDataFuncGetBlinkEntryAttributes) (void *blinkentry,
							 va_list argp);


EXTERN_FUNCTION (KDisplayDataClass dispdata_create_dataclass,
		 (CONST char *name,
		  KDisplayDataFuncSetClassAttributes set_class_attributes,
		  KDisplayDataFuncDestroyDataObject destroy_dataobject,
		  KDisplayDataFuncCopyDataAttributes copy_data_attributes,
		  KDisplayDataFuncSetDataAttributes set_data_attributes,
		  KDisplayDataFuncGetDataAttributes get_data_attributes,
		  KDisplayDataFuncCreateWindow create_window,
		  KDisplayDataFuncDestroyWindow destroy_window,
		  KDisplayDataFuncSetWindowAttributes set_window_attributes,
		  KDisplayDataFuncGetWindowAttributes get_window_attributes,
		  KDisplayDataFuncCreateViewable create_viewable,
		  KDisplayDataFuncDestroyViewable destroy_viewable,
		  KDisplayDataFuncCreateBlinkState create_blinkstate,
		  KDisplayDataFuncDestroyBlinkState destroy_blinkstate,
		  KDisplayDataFuncActivateBlinkState activate_blinkstate,
		  KDisplayDataFuncCreateBlinkEntry create_blinkentry,
		  KDisplayDataFuncDestroyBlinkEntry destroy_blinkentry,
		  KDisplayDataFuncCopyBlinkEntry copy_blinkentry,
		  KDisplayDataFuncActivateBlinkEntry activate_blinkentry,
		  KDisplayDataFuncSetBlinkEntryAttributes set_be_attributes,
		  KDisplayDataFuncGetBlinkEntryAttributes get_be_attributes) );


#endif /*  KARMA_DISPDATA_HOOKS_H  */
