#!/bin/csh -f
#
#  $Prog$
#
#  $Id: getms.csh,v 1.9 2008/01/10 15:11:02 schoenma Exp $
#
#  $Purpose: Retrieve MS'es from archive mirror
#
#  $Usage:   See "getms -h"
#
#
#set echo
if ($?wsrt_archivemirror_host) then 
  set src = $wsrt_archivemirror_host
else 
  set src='wsrtarchive.astron.nl'
endif
    
#
# Check if $src is up
#
if (`uname -s` == "HP-UX") then
  set rsh='remsh'
  set myping="ping $src -n 1"
else
  set rsh='rsh'
  set myping="ping $src -c 1"
endif
set result=`$myping | grep -c " 0%"`
if ($result != 1) then
  echo "Cannot connect to archive host ${src}"
  exit 1
else
  echo Using $src as archive host
endif
#
if ("$1" == "-h") then
  cat << _EOD_
#
#  Usage:    getms  <1st seq nr> [<last seq nr>] or  getms <pattern>
#
#            getms will retrieve 1 or more MS's from the archive 
#            and store it into your current directory.
#  where
#
#  <1st seq nr>    First sequence nr of a range. A number > 9700000
#
# [<last seq nr>]  (Optional) Last sequence nr of a range.
#                  Must be larger than <1st seq nr>, but not more than a
#                  100 MS'es can be copied in one call.
#
#                  If <last seq nr> is present, both numbers should range
#                  between 970000 and current seq nr
#
#  <pattern> =     Any string containing "_", e.g. "10400400_S0" will retrieve
#                  "10400400_S0_T0.MS" and all other "S0" MS'es.
#
##  Several checks will be done:
#  - Check if $HOST & $USER are present in ~inspect/.rhosts on archive system
#  - Check if the current directory is writable for you
#  - Find out which directory contains the data
#  - Check whether the data is present at all
#  - Check what data is already present on local disk
_EOD_
  exit
endif

#
# Check if $HOST & $USER are present in ~inspect/.rhosts on archive system
#
@ cnt=`$rsh $src -l inspect uptime | grep -c user`
if ( $cnt == 0 ) then
  echo "Fatal: You don't have access to the archive host as $USER on $HOST"
  echo "Ask the WSRT system administrator to grant you access."
  exit
endif
#
#
# Test if this directory is writable
#
touch tmp$$
if (! -e tmp$$) then
  echo "You do not have permission to write in this directory\!"
  exit
endif
'rm' -f tmp$$


#
# Make a list of seq. nrs
#
set first="$1"
set last="$2"
set pattrn=`echo $1| tr -d '[0-9]'`
@ pattern=0
if ("$pattrn" != "") then
  @ pattern=1
endif
#
if ("$first" == "") then
  echo -n "Enter a start sequence nr "
  set first=($<)
  if ($first < 9700000) then
    echo "Bad value, try again"
    exit
  endif

  echo -n "Enter an end sequence nr "
  set last=($<)
  if ($first> $last) then
    echo "Bad range, try again"
    exit
  endif

  set list=""
  set n=0
  set v=$first
  while ($v < $last + 1 && $n < 100)
    set list=`echo "$list $v "`
    @ v= $v + 1
    @ n= $n + 1
  end
else
  #echo $pattern
  if (! $pattern ) then
    if ("$last" == "") then
      set last="$first"
      set list=$first
    else
      set list=""
      set n=0
      set v=$first
      while ($v < $last + 1 && $n < 100)
        set list=`echo "$list $v "`
        @ v= $v + 1
        @ n= $n + 1
      end
    endif
  else
    set list=$first
    echo Will use pattern $list
  endif
endif
#
# Actual copy. Prefer "rsync", otherwise use rcp
#
#echo Retrieving '"'$list'"'
set seq=$list[1]
if ($pattern) set seq=$1
set disks=`$rsh $src -l inspect df | grep "arch[0-9]" | awk '{print $6}' | sort -n -th +1 -r`
set dest='.'
set rsync='/usr/bin/rsync'
foreach ms ($list)

  # First try to find the measurement in the content list (most likely to be found)
#  set present=`rsh $src -l inspect grep "${ms}" /arch1/archive_content.lst`
  set present=`rsh $src -l inspect grep "${ms}" /arch16/archive_content.lst`
  if ( "$present" =~ *MS* ) then
    echo "Found files related to measurement ${ms} in archive content list"
    set dir = `echo $present[1] | awk -F/ '{print "/"$1$2}'`
    goto retrievefile
  endif

  # If not found, scan all /arch[x] directories
  echo -n "Trying $seq on "
  foreach dir ($disks)
    echo -n "${src}:$dir ... "
    set present=`$rsh $src -l inspect find $dir -maxdepth 1 -name '"'${ms}\*'" -print'`
    if ( "$present" =~ *MS* ) then
      echo "Found file in directory $dir"
      goto retrievefile
    endif
  end
  echo "Could not find file $ms in archive"
  continue

retrievefile:
  if (-x $rsync) then
#    echo "$rsync -av -e rsh inspect@${src}:${dir}/$ms\* $dest"
    if ($ms =~ *org*) then 
      $rsync -av -e rsh inspect@${src}:${dir}/$ms\* $dest
    else 
      $rsync -av -e rsh inspect@${src}:${dir}/$ms\* --exclude '*.org*' $dest
    endif
  else
      # Program rsync not present
#    echo "rcp -r inspect@${src}:${dir}/${ms}\* ${dest}"
    rcp -rp inspect@${src}:${dir}/${ms}\* ${dest}
  endif
end
exit

