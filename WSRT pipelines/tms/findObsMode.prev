#!/bin/csh -f
#
# Script to learn about the used observing mode of an MS
#
# Call as: findObsMode <MS> or findObsMode <seqnr>
#
# The exit state indicates the used mode:
# 0: Error
# 1: standard non-mosaicing mode
# 2: Position mosaic mode
# 3: Frequency mosaic mode
# 4: Position and frequency mosaic mode
#
# The MS files must be in the current directory, or the full path must be 
# entered on the commandline
#
# This is a temporary script for use by the export pipeline. It uses the MSinfo
# program to make a temporary file and parses the file for observation mode
# specific lines.
#
# A.P. Schoenmakers, 2 May 2007
#

if ($1 == "") then 
  echo "Provide full MS name or sequence number"
  exit 0;
endif

if (!(-d $1)) then 
  echo "Cannot find the MS"
  exit 0;
endif

set seqnr = `echo $1|sed 's/\/$//g'` # remove trailing /
set infofile = /tmp/${seqnr:t}.info
rm -fr ${infofile} >& /dev/null
MSinfo in=$1 > ${infofile}
if (!(-e ${infofile})) then 
  echo "Problem with output file of MSinfo"
  exit 0
endif

set r_value = 1

# Position mosaic?

set posmos = `grep "Pointing Mosaic Positions" ${infofile}`
if ($#posmos != 0) then 
  set nrofpos = `echo $posmos| awk -F= '{print $2}'`
  set r_value = 2
endif

 
# Freq. mosaic?

set freqmos = `grep "Freq. Mos. Positions" ${infofile}`
if ($#freqmos != 0) then 
  set nrofpos = `echo $freqmos| awk -F= '{print $2}'`
  if ($r_value == 2) then 
    set r_value = 4
  else 
    set r_value = 3
  endif
endif

if ($#posmos == 0 && $#freqmos == 0) then 
  set r_value = 1
endif

rm -fr ${infofile} >& /dev/null
exit ${r_value};


