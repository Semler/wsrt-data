#!/bin/csh -f
#
#  $Prog$
#
#  $Id: getms.csh,v 1.15 2010/02/03 16:21:25 schoenma Exp $
#
#  $Purpose: Retrieve MS'es from archive mirror
#
#  $Usage:   See "getms -h"
#
#
#set echo
set src='wsrtarchive.astron.nl'
    
#
# Check if $src is up
#
set ssh='ssh -x'
set myping="ping -c 1 $src"

set result=`$myping | grep -c " 0%"`
if ($result != 1) then
  echo "Cannot connect to archive host ${src}"
  exit 1
else
  echo Using $src as archive host
endif
#
if ("$1" == "-h") then
  cat << _EOD_
#
#  Usage:    getms  <1st seq nr> [<last seq nr>] or  getms <pattern>
#
#            getms will retrieve 1 or more MS's from the archive 
#            and store it into your current directory.
#  where
#
#  <1st seq nr>    First sequence nr of a range. A number > 9700000
#
# [<last seq nr>]  (Optional) Last sequence nr of a range.
#                  Must be larger than <1st seq nr>, but not more than a
#                  100 MS'es can be copied in one call.
#
#                  If <last seq nr> is present, both numbers should range
#                  between 970000 and current seq nr
#
#  <pattern> =     Any string containing "_", e.g. "10400400_S0" will retrieve
#                  "10400400_S0_T0.MS" and all other "S0" MS'es.
#
##  Several checks will be done:
#  - Check if $HOST & $USER are present in ~inspect/.rhosts on archive system
#  - Check if the current directory is writable for you
#  - Find out which directory contains the data
#  - Check whether the data is present at all
#  - Check what data is already present on local disk
_EOD_
  exit
endif

#
# Check if $HOST & $USER are present in ~inspect/.rhosts on archive system
#
@ cnt=`$ssh $src -l inspect uptime | grep -c user`
if ( $cnt == 0 ) then
  echo "Fatal: You don't have access to the archive host as $USER on $HOST"
  echo "Ask the WSRT system administrator to grant you access."
  exit
endif
#
#
# Test if this directory is writable
#
touch tmp$$
if (! -e tmp$$) then
  echo "You do not have permission to write in this directory\!"
  exit
endif
'rm' -f tmp$$


#
# Make a list of seq. nrs
#
set first="$1"
set last="$2"
set pattrn=`echo $1| tr -d '[0-9]'`
@ pattern=0
if ("$pattrn" != "") then
  @ pattern=1
endif
#
if ("$first" == "") then
  echo -n "Enter a start sequence nr "
  set first=($<)
  if ($first < 9700000) then
    echo "Bad value, try again"
    exit
  endif

  echo -n "Enter an end sequence nr "
  set last=($<)
  if ($first> $last) then
    echo "Bad range, try again"
    exit
  endif

  set list=""
  set n=0
  set v=$first
  while ($v < $last + 1 && $n < 100)
    set list=`echo "$list $v "`
    @ v= $v + 1
    @ n= $n + 1
  end
else
  #echo $pattern
  if (! $pattern ) then
    if ("$last" == "") then
      set last="$first"
      set list=$first
    else
      set list=""
      set n=0
      set v=$first
      while ($v < $last + 1 && $n < 100)
        set list=`echo "$list $v "`
        @ v= $v + 1
        @ n= $n + 1
      end
    endif
  else
    set list=$first
    echo Will use pattern $list
  endif
endif
#
# Actual copy. Prefer "rsync", otherwise use rcp
#
#echo Retrieving '"'$list'"'
set seq=$list[1]
if ($pattern) set seq=$1
set disks=`$ssh $src -l inspect df | grep "arch[0-9]" | awk '{print $6}' | sort -n -th -k2 -r`
set dest='.'
set rsync=`which rsync`
set dirs = ''
foreach ms ($list)

  # First try to find the measurement in the content list (most likely to be 
  # found). If found, also check the directory for files with the same name
  set in_list=`$ssh $src -l inspect grep "${ms}" /arch16/archive_content.lst`
  if ( "$in_list" =~ *MS* ) then
    echo "Found files related to measurement ${ms} in archive content list"
    set dir = `echo $in_list[1] | awk -F/ '{print "/"$1$2}'`
    set present=`$ssh $src -l inspect find $dir -maxdepth 1 -name '"'${ms}\*'" -print'`
    set dirs = `echo $dirs $present`
    goto retrievefile
  endif

  # If not found, scan all /arch[x] directories (backwards)
  set dirs = ''
  echo -n "Trying $seq on "
  set n = $#disks
  foreach dir ($disks)
    if ($n > 0) then 
      echo -n "${src}:$dir ... "
      set present=`$ssh $src -l inspect find $dir -maxdepth 1 -name '"'${ms}\*'" -print' | grep -v "org" `
      if ( "$present" =~ *MS* ) then
        echo "Found one or more files in directory $dir"
        set dirs = `echo $dirs $present`
	# Start mechanism that only searches one more partition (previous one)
        if ($n > 2) set n = 2
      endif
      @ n = $n - 1 
    endif
  end

  if ("$dirs" == "") then 
    echo ""
    echo "Could not find file $ms in archive"
  else
    goto retrievefile
  endif    
  continue

retrievefile:
# There is a (list of) directories in $dirs; get them all
  foreach dataset ($dirs)
    if (-x $rsync) then
      $rsync -av -e ssh inspect@${src}:${dataset} $dest
    else
      # Program rsync not present
#    echo "scp -rpq inspect@${src}:${dataset} ${dest}"
      scp -rpq inspect@${src}:${dataset} ${dest}
    endif
  end
end
exit

