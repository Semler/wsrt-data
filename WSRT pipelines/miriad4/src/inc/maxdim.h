c Warning: do not edit this file, it has possibly been generated by configure
c TELESCOPE=wsrt
c=======================================================================
	CHARACTER*(*) MIRTEL
	PARAMETER(MIRTEL='wsrt')
	INTEGER   MAXBUF
	PARAMETER(MAXBUF=40000000)
c
	INTEGER   MAXDIM, MAXDIM2
	PARAMETER(MAXDIM=65536,MAXDIM2=8192)
c
	INTEGER   MAXIANT,MAXANT,MAXANT2
	PARAMETER(MAXIANT=256,MAXANT=64,MAXANT2=28)
c		maximum number of baselines
	INTEGER   MAXBASE
c	PARAMETER(MAXBASE=((MAXANT*(MAXANT+1))/2))
	PARAMETER(MAXBASE=500)
	INTEGER   MAXBASE2
	PARAMETER(MAXBASE2=500)
c
	INTEGER   MAXCHAN
	PARAMETER(MAXCHAN=8193)
c
	INTEGER   MAXWIN
	PARAMETER(MAXWIN=16)
c
	INTEGER   MAXWIDE
	PARAMETER(MAXWIDE=18)
c=======================================================================

