# This file was created automatically by SWIG.
# Don't modify this file, modify the SWIG interface instead.
# This file is compatible with both classic and new-style classes.

import _mirMinMax

def _swig_setattr(self,class_type,name,value):
    if (name == "this"):
        if isinstance(value, class_type):
            self.__dict__[name] = value.this
            if hasattr(value,"thisown"): self.__dict__["thisown"] = value.thisown
            del value.thisown
            return
    method = class_type.__swig_setmethods__.get(name,None)
    if method: return method(self,value)
    self.__dict__[name] = value

def _swig_getattr(self,class_type,name):
    method = class_type.__swig_getmethods__.get(name,None)
    if method: return method(self)
    raise AttributeError,name

import types
try:
    _object = types.ObjectType
    _newclass = 1
except AttributeError:
    class _object : pass
    _newclass = 0
del types


class minMaxData(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, minMaxData, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, minMaxData, name)
    def __repr__(self):
        return "<C minMaxData instance at %s>" % (self.this,)
    __swig_setmethods__["min"] = _mirMinMax.minMaxData_min_set
    __swig_getmethods__["min"] = _mirMinMax.minMaxData_min_get
    if _newclass:min = property(_mirMinMax.minMaxData_min_get, _mirMinMax.minMaxData_min_set)
    __swig_setmethods__["max"] = _mirMinMax.minMaxData_max_set
    __swig_getmethods__["max"] = _mirMinMax.minMaxData_max_get
    if _newclass:max = property(_mirMinMax.minMaxData_max_get, _mirMinMax.minMaxData_max_set)
    def __init__(self, *args):
        _swig_setattr(self, minMaxData, 'this', _mirMinMax.new_minMaxData(*args))
        _swig_setattr(self, minMaxData, 'thisown', 1)
    def __del__(self, destroy=_mirMinMax.delete_minMaxData):
        try:
            if self.thisown: destroy(self)
        except: pass

class minMaxDataPtr(minMaxData):
    def __init__(self, this):
        _swig_setattr(self, minMaxData, 'this', this)
        if not hasattr(self,"thisown"): _swig_setattr(self, minMaxData, 'thisown', 0)
        _swig_setattr(self, minMaxData,self.__class__,minMaxData)
_mirMinMax.minMaxData_swigregister(minMaxDataPtr)


mirMinMax = _mirMinMax.mirMinMax

