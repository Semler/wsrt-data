# This file was created automatically by SWIG.
# Don't modify this file, modify the SWIG interface instead.
# This file is compatible with both classic and new-style classes.

import _uvio

def _swig_setattr(self,class_type,name,value):
    if (name == "this"):
        if isinstance(value, class_type):
            self.__dict__[name] = value.this
            if hasattr(value,"thisown"): self.__dict__["thisown"] = value.thisown
            del value.thisown
            return
    method = class_type.__swig_setmethods__.get(name,None)
    if method: return method(self,value)
    self.__dict__[name] = value

def _swig_getattr(self,class_type,name):
    method = class_type.__swig_getmethods__.get(name,None)
    if method: return method(self)
    raise AttributeError,name

import types
try:
    _object = types.ObjectType
    _newclass = 1
except AttributeError:
    class _object : pass
    _newclass = 0
del types



cdata = _uvio.cdata

memmove = _uvio.memmove

uvopen_c = _uvio.uvopen_c

uvread_c = _uvio.uvread_c

uvprobvr_c = _uvio.uvprobvr_c

uvgetvr_c = _uvio.uvgetvr_c

uvclose_c = _uvio.uvclose_c

hisopen_c = _uvio.hisopen_c

hiswrite_c = _uvio.hiswrite_c

hisread_c = _uvio.hisread_c

hisclose_c = _uvio.hisclose_c

hopen_c = _uvio.hopen_c

hclose_c = _uvio.hclose_c

new_intp = _uvio.new_intp

copy_intp = _uvio.copy_intp

delete_intp = _uvio.delete_intp

intp_assign = _uvio.intp_assign

intp_value = _uvio.intp_value

new_floatp = _uvio.new_floatp

copy_floatp = _uvio.copy_floatp

delete_floatp = _uvio.delete_floatp

floatp_assign = _uvio.floatp_assign

floatp_value = _uvio.floatp_value

new_doublep = _uvio.new_doublep

copy_doublep = _uvio.copy_doublep

delete_doublep = _uvio.delete_doublep

doublep_assign = _uvio.doublep_assign

doublep_value = _uvio.doublep_value

new_charp = _uvio.new_charp

copy_charp = _uvio.copy_charp

delete_charp = _uvio.delete_charp

charp_assign = _uvio.charp_assign

charp_value = _uvio.charp_value
class intArray(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, intArray, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, intArray, name)
    def __repr__(self):
        return "<C intArray instance at %s>" % (self.this,)
    def __init__(self, *args):
        _swig_setattr(self, intArray, 'this', _uvio.new_intArray(*args))
        _swig_setattr(self, intArray, 'thisown', 1)
    def __del__(self, destroy=_uvio.delete_intArray):
        try:
            if self.thisown: destroy(self)
        except: pass
    def __getitem__(*args): return _uvio.intArray___getitem__(*args)
    def __setitem__(*args): return _uvio.intArray___setitem__(*args)
    def cast(*args): return _uvio.intArray_cast(*args)
    __swig_getmethods__["frompointer"] = lambda x: _uvio.intArray_frompointer
    if _newclass:frompointer = staticmethod(_uvio.intArray_frompointer)

class intArrayPtr(intArray):
    def __init__(self, this):
        _swig_setattr(self, intArray, 'this', this)
        if not hasattr(self,"thisown"): _swig_setattr(self, intArray, 'thisown', 0)
        _swig_setattr(self, intArray,self.__class__,intArray)
_uvio.intArray_swigregister(intArrayPtr)

intArray_frompointer = _uvio.intArray_frompointer

class floatArray(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, floatArray, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, floatArray, name)
    def __repr__(self):
        return "<C floatArray instance at %s>" % (self.this,)
    def __init__(self, *args):
        _swig_setattr(self, floatArray, 'this', _uvio.new_floatArray(*args))
        _swig_setattr(self, floatArray, 'thisown', 1)
    def __del__(self, destroy=_uvio.delete_floatArray):
        try:
            if self.thisown: destroy(self)
        except: pass
    def __getitem__(*args): return _uvio.floatArray___getitem__(*args)
    def __setitem__(*args): return _uvio.floatArray___setitem__(*args)
    def cast(*args): return _uvio.floatArray_cast(*args)
    __swig_getmethods__["frompointer"] = lambda x: _uvio.floatArray_frompointer
    if _newclass:frompointer = staticmethod(_uvio.floatArray_frompointer)

class floatArrayPtr(floatArray):
    def __init__(self, this):
        _swig_setattr(self, floatArray, 'this', this)
        if not hasattr(self,"thisown"): _swig_setattr(self, floatArray, 'thisown', 0)
        _swig_setattr(self, floatArray,self.__class__,floatArray)
_uvio.floatArray_swigregister(floatArrayPtr)

floatArray_frompointer = _uvio.floatArray_frompointer

class doubleArray(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, doubleArray, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, doubleArray, name)
    def __repr__(self):
        return "<C doubleArray instance at %s>" % (self.this,)
    def __init__(self, *args):
        _swig_setattr(self, doubleArray, 'this', _uvio.new_doubleArray(*args))
        _swig_setattr(self, doubleArray, 'thisown', 1)
    def __del__(self, destroy=_uvio.delete_doubleArray):
        try:
            if self.thisown: destroy(self)
        except: pass
    def __getitem__(*args): return _uvio.doubleArray___getitem__(*args)
    def __setitem__(*args): return _uvio.doubleArray___setitem__(*args)
    def cast(*args): return _uvio.doubleArray_cast(*args)
    __swig_getmethods__["frompointer"] = lambda x: _uvio.doubleArray_frompointer
    if _newclass:frompointer = staticmethod(_uvio.doubleArray_frompointer)

class doubleArrayPtr(doubleArray):
    def __init__(self, this):
        _swig_setattr(self, doubleArray, 'this', this)
        if not hasattr(self,"thisown"): _swig_setattr(self, doubleArray, 'thisown', 0)
        _swig_setattr(self, doubleArray,self.__class__,doubleArray)
_uvio.doubleArray_swigregister(doubleArrayPtr)

doubleArray_frompointer = _uvio.doubleArray_frompointer

class charArray(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, charArray, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, charArray, name)
    def __repr__(self):
        return "<C charArray instance at %s>" % (self.this,)
    def __init__(self, *args):
        _swig_setattr(self, charArray, 'this', _uvio.new_charArray(*args))
        _swig_setattr(self, charArray, 'thisown', 1)
    def __del__(self, destroy=_uvio.delete_charArray):
        try:
            if self.thisown: destroy(self)
        except: pass
    def __getitem__(*args): return _uvio.charArray___getitem__(*args)
    def __setitem__(*args): return _uvio.charArray___setitem__(*args)
    def cast(*args): return _uvio.charArray_cast(*args)
    __swig_getmethods__["frompointer"] = lambda x: _uvio.charArray_frompointer
    if _newclass:frompointer = staticmethod(_uvio.charArray_frompointer)

class charArrayPtr(charArray):
    def __init__(self, this):
        _swig_setattr(self, charArray, 'this', this)
        if not hasattr(self,"thisown"): _swig_setattr(self, charArray, 'thisown', 0)
        _swig_setattr(self, charArray,self.__class__,charArray)
_uvio.charArray_swigregister(charArrayPtr)

charArray_frompointer = _uvio.charArray_frompointer


