# This file was created automatically by SWIG.
# Don't modify this file, modify the SWIG interface instead.
# This file is compatible with both classic and new-style classes.

import _mirStat

def _swig_setattr(self,class_type,name,value):
    if (name == "this"):
        if isinstance(value, class_type):
            self.__dict__[name] = value.this
            if hasattr(value,"thisown"): self.__dict__["thisown"] = value.thisown
            del value.thisown
            return
    method = class_type.__swig_setmethods__.get(name,None)
    if method: return method(self,value)
    self.__dict__[name] = value

def _swig_getattr(self,class_type,name):
    method = class_type.__swig_getmethods__.get(name,None)
    if method: return method(self)
    raise AttributeError,name

import types
try:
    _object = types.ObjectType
    _newclass = 1
except AttributeError:
    class _object : pass
    _newclass = 0
del types


class statData(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, statData, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, statData, name)
    def __repr__(self):
        return "<C statData instance at %s>" % (self.this,)
    __swig_setmethods__["size"] = _mirStat.statData_size_set
    __swig_getmethods__["size"] = _mirStat.statData_size_get
    if _newclass:size = property(_mirStat.statData_size_get, _mirStat.statData_size_set)
    __swig_setmethods__["noiseData"] = _mirStat.statData_noiseData_set
    __swig_getmethods__["noiseData"] = _mirStat.statData_noiseData_get
    if _newclass:noiseData = property(_mirStat.statData_noiseData_get, _mirStat.statData_noiseData_set)
    def __getitem__(*args): return _mirStat.statData___getitem__(*args)
    def __init__(self, *args):
        _swig_setattr(self, statData, 'this', _mirStat.new_statData(*args))
        _swig_setattr(self, statData, 'thisown', 1)
    def __del__(self, destroy=_mirStat.delete_statData):
        try:
            if self.thisown: destroy(self)
        except: pass

class statDataPtr(statData):
    def __init__(self, this):
        _swig_setattr(self, statData, 'this', this)
        if not hasattr(self,"thisown"): _swig_setattr(self, statData, 'thisown', 0)
        _swig_setattr(self, statData,self.__class__,statData)
_mirStat.statData_swigregister(statDataPtr)


mirStat = _mirStat.mirStat

