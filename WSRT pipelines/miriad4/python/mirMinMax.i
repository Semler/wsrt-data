/* mirMinMax */
/* Compilation: 
swig -python mirMinMax.i
gcc -c mirMinMax_wrap.c -I/usr/include/python2.3 -I../../tolib/ -I../../libmirc/
ld -shared mirMinMax_wrap.o ../../libmirc/libmirc.so -o _mirMinMax.so
ld -shared -L/dop58_1/mellema/libmirc mirMinMax_wrap.o -o _mirMinMax.so -lmirc
*/

%module mirMinMax
%{
#include "port.h"
#include "mirutil.h"
#include "miriad.h"
%}

%inline%{

typedef struct minMaxData {
 float min;
 float max;
} minMaxData;

minMaxData mirMinMax (const char *inpfilename) {

 minMaxData result;
 int   fileIn;
 int   axes[5], channel[5];
 int   naxis;
 long  numPoints, index;
 float *data;
 int   *flags;
 float  datMin, datMax;

 int   i, j;

 char *timeString;


 /* we handle a cube */
 naxis = 3;
 /* open input file */
 xyopen_c(&fileIn, inpfilename, "old", naxis, axes);


 numPoints = axes[0]*axes[1]*axes[2];


 /* allocate buffer */
 data  = (float *) malloc(axes[0]*axes[1]*axes[2]*sizeof(float));
 flags  = (int *) malloc(axes[0]*axes[1]*axes[2]*sizeof(float));

 /* read entire input file into memory */
 for (i = 1; i <= axes[2]; i++) {
   channel[0] = i;
   /* set active channel */
   xysetpl_c(fileIn, 1, channel);
   for (j = 1; j <= axes[1]; j++) {
     /* read data row by row */
     xyread_c(fileIn, j, &data[(j-1)*axes[0]+(i-1)*axes[0]*axes[1]]);
     xyflgrd_c(fileIn, j, &flags[(j-1)*axes[0]+(i-1)*axes[0]*axes[1]]);
   }
 }

 datMax = -1111111111111111111.0;
 datMin = -datMax;

 for (index = 0; index < numPoints; index++) {
   if (data[index] > datMax && flags[index] ) {
     datMax = data[index];
   }
   if (data[index] < datMin && flags[index] ) {
     datMin = data[index];
   }
 }

 /* sign off */
 xyclose_c(fileIn);

 free(data);
 free(flags);

 result.min = datMin;
 result.max = datMax;

 return result;
}


%}
