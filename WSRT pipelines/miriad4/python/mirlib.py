# A module for using functions from the Miriad Library
"""
Module to use Miriad library functions
"""

import uvio
import math

vartypes= {'a' : [1,1], 'i' : [2,4], 'r' : [4,4], 'd' : [5,8]}

lightspeed=2.99792458e5

#------------------------------------------------------------------------------
def varvalue(miruv,var):
    """
    This function returns the value of a variable var
    in a Miriad UV dataset miruv.
    """

    # Open the UV dataset
    ff=uvio.new_intp()
    uvio.uvopen_c(ff,miruv,"old")
    fh=uvio.intp_value(ff)

    # Read the first data from it (needed in order
    # to access the variables.
    maxchan=8192
    p=uvio.doubleArray(4)
    f=uvio.intArray(maxchan)
    d=uvio.floatArray(2*maxchan)
    nn=uvio.new_intp()
    uvio.uvread_c(fh,p,d,f,maxchan,nn)

    # Test if the variable exists
    vartype=uvio.new_charp()
    length=uvio.new_intp()
    upd=uvio.new_intp()
    uvio.uvprobvr_c(fh,var,vartype,length,upd)

    # Get the variable from the UV data set
    cvartype=uvio.charp_value(vartype)
    if cvartype in vartypes.keys():
        h_vartype=vartypes[cvartype][0]
        bytes=vartypes[cvartype][1]
    else: raise "Error: variable not found in UV dataset"

    length_val=uvio.intp_value(length)
    if cvartype =='i':
	varvalue_array=uvio.intArray(length_val)
        value=uvio.cdata(varvalue_array,length_val*bytes)
    elif cvartype == 'r':
	varvalue_array=uvio.floatArray(length_val)
        value=uvio.cdata(varvalue_array,length_val*bytes)
    elif cvartype == 'd':
	varvalue_array=uvio.doubleArray(length_val)
        value=uvio.cdata(varvalue_array,length_val*bytes)
    elif cvartype == 'a':
        length_val=uvio.intp_value(length)+1
        value=length_val*"a"
    else:
        print 'Vartype '+cvartype+' not implemented'
        return None

    uvio.uvgetvr_c(fh,h_vartype,var,value,length_val)

    # Close the UV dataset
    uvio.uvclose_c(fh)
    
    # Handle the result depending on the vartype of the variable
    if cvartype =='i':
        uvio.memmove(varvalue_array,value)
        varvalue=[]
        for i in range(length_val):
            varvalue.append(varvalue_array[i])
    elif cvartype == 'r':
        uvio.memmove(varvalue_array,value)
        varvalue=[]
        for i in range(length_val):
            varvalue.append(float(varvalue_array[i]))
    elif cvartype == 'd':
        uvio.memmove(varvalue_array,value)
        varvalue=[]
        for i in range(length_val):
            varvalue.append(float(varvalue_array[i]))
    elif cvartype == 'a':
        varvalue=value[0:length_val-1]
    else:
        print 'Vartype '+cvartype+' not implemented'
        return None
    
    return varvalue
    
        
#------------------------------------------------------------------------------
def prnthis(mirfile):
    """
    This function prints the history of a Miriad UV dataset miruv.
    """

    # Open the UV dataset
    ff=uvio.new_intp()
    io=uvio.new_intp()
    uvio.hopen_c(ff,mirfile,"old",io)

    if uvio.intp_value(io):
        print "Error opening file "+mirfile
    else:
        fh=uvio.intp_value(ff)

        # Open the history
        uvio.hisopen_c(fh,"read")

        # End of file pointer
        eof=uvio.new_intp()
        uvio.intp_assign(eof,0)
        
        while not uvio.intp_value(eof):
            # Define line (does the length really need to be hardcoded?)
            line=80*" "
            uvio.hisread_c(fh,line,80,eof)
            print line
            
        # Close the history
        uvio.hisclose_c(fh)

        # Close the data set
        uvio.hclose_c(fh)
    
    return

#------------------------------------------------------------------------------
def getvarfromhis(mirfile,var):
    """
    This function returns the value of a variable if it is listed in
    the history of that dataset
    """

    # Set default value
    varvalue=""
    
    # Open the dataset
    ff=uvio.new_intp()
    io=uvio.new_intp()
    uvio.hopen_c(ff,mirfile,"old",io)

    if uvio.intp_value(io):
        print "Error opening file "+mirfile

    else:
        fh=uvio.intp_value(ff)
        # Open the history
        uvio.hisopen_c(fh,"read")
        
        # End of file pointer
        eof=uvio.new_intp()
        uvio.intp_assign(eof,0)
        
        while not uvio.intp_value(eof):
            # Define line (does the length really need to be hardcoded?)
            line=80*" "
            uvio.hisread_c(fh,line,80,eof)
            if line.startswith(var):
                varvalue_str=line.split()[-1].replace("\x00","")
                if not varvalue_str.isalpha():
                    varvalue=eval(varvalue_str)
                else:
                    varvalue=varvalue_str
                    
                    
        # Close the history
        uvio.hisclose_c(fh)

        # Close the data set
        uvio.hclose_c(fh)
    
    return varvalue

#------------------------------------------------------------------------------
def addhis(mirfile,string):
    """
    This function adds a line to the history of a Miriad dataset mir.
    """

    # Open the dataset
    ff=uvio.new_intp()
    io=uvio.new_intp()
    uvio.hopen_c(ff,mirfile,"old",io)

    if uvio.intp_value(io):
        print "Error opening file "+mirfile

    else:
        fh=uvio.intp_value(ff)
        # Open the history
        uvio.hisopen_c(fh,"write")
        
        # Write to history
        uvio.hiswrite_c(fh,string)
        
        # Close the history
        uvio.hisclose_c(fh)
        
        # Close the data set
        uvio.hclose_c(fh)
        
    return
    
#------------------------------------------------------------------------------
def cellsize (miruv):
    """
    This function returns an estimate for a useful image cell size
    on the basis of the wavelength and baselines listed in the
    Miriad uv dataset miruv
    """

    antpos=varvalue(miruv,"antpos")
    num_ant=len(antpos)/3

    # Assume the antennas East-West
    # antpos is in nanoseconds (delay).
    max_base=abs(antpos[num_ant*2-1]-antpos[num_ant])*lightspeed*1e-9

    # Obtain frequency information
    sfreq=varvalue(miruv,"sfreq")
    num_bands=len(sfreq)
    ref_band=num_bands/2

    # Use middle band to define resolution
    # (frequency is in GHz)
    wavelength=lightspeed/sfreq[ref_band]/1e9

    # Resolution is 0.8*lambda/D, convert to arcseconds (180*3600/pi)
    resolution=round(0.8*wavelength/max_base/math.pi*180.*3600.)

    cellsize=resolution/3.0

    return cellsize
    
    
        
