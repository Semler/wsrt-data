/* mirStat */
 %module mirStat
 %{
#include <port.h>
#include <mirutil.h>
#include <miriad.h>
 %}
 
%inline%{

typedef struct statData {
  int size;
  float noiseData[4096];
} statData;

statData mirStat(const char *inpFilename) {

  statData result;
  char  charBuff[80];
  
  int   xlen, ylen, zlen, i;
  float sigma;

  char *timeString;

  DataCube  *inpCube;
  
  
  inpCube = newDataCube();

  /* open file */
  openDataCube(inpFilename, inpCube, "old");
  readDataCube(inpCube);
  zlen = inpCube->axes[2];

  /* open history of output file */
  hisopen_c(inpCube->fileHandle, "append");
  timeString = getTheTime();
  sprintf(charBuff, "MIRSTAT: Executed on: %s", timeString);
  hiswrite_c(inpCube->fileHandle, charBuff);
  
  
  sprintf(charBuff, "MIRSTAT: Command line inputs follow:");
  hiswrite_c(inpCube->fileHandle, charBuff);
  sprintf(charBuff, "MIRSTAT:   in=%s", inpFilename);
  hiswrite_c(inpCube->fileHandle, charBuff);


  result.size=zlen;
  for (i = 0; i < zlen; i++) {
    sigma = statChannel(inpCube, i);
    result.noiseData[i]=sigma;
    sprintf(charBuff, "MIRSTAT: rms noise in channel %d: %f", i+1, sigma);
    hiswrite_c(inpCube->fileHandle, charBuff);
  }
    
  hisclose_c(inpCube->fileHandle);

  deleteDataCube(inpCube);

  return result;
}

%}

%extend statData {
    float __getitem__(int i) {
      	int err = (i<0 || i>4095);
      	if (err)
      	    return 0;
	return self->noiseData[i];
    }
}
