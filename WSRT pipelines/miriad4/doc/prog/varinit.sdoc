%N varinit %F $MIR/src/subs/var.for
%D Initialise the copy routines.
%: uv-data
%P rjs
%B
        subroutine VarInit(tIn,linetype)

        implicit none
        character linetype*(*)
        integer tIn

  The VarCpIni routine marks (using uvtrack) a number of variables to be
  copied across by the uvcopyvr routine.
  It also marks frequency setup and system temp variables, to note
  updates to these, so that VarCopy can be used later to update them.

  Typical use would be:

    call VarInit(tIn,linetype)
    call VarOnit(tIn,tOut,linetype)
          .
          .
          .
    call uvread(tIn,...)
          .
          .
          .
    call VarCopy(tIn,tOut)
    call uvwrite(tOut,...)

  Variables whose characteristics are set by VarOnit are:
   line=channel or velocity: corr
   line=wide               : wcorr
  Variables NOT set up to be copied by VarInit are:
    preamble variables:        time, baseline, coord
    polarisation variables:    npol, pol
    data variables:            corr, wcorr, nchan, nwide
  Variables produced by the VarCopy routine:
   line=channel or velocity: nspect,nschan,ischan,sdf,sfreq,restfreq,systemp,
       xtsys,ytsys
   line=wide               : wfreq,wwidth,wsystemp.
  Variables setup for copying, and copied by VarWInit:
   wfreq,wwidth,wsystemp

  Input:
    linetype   Either 'channel', 'wide' or 'velocity'. If this is
               blank, then uvcopy does not prepare itself to update the
               frequency setup variables.
