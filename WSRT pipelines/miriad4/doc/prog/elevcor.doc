%N elevcor
%D Correct ATCA data for elevation-dependent gain.
%P dpr
%: uv analysis
%B
ElevCor corrects ATCA data for elevation-dependent gain, using a
pre-defined template. This may be useful when the target and
secondary calibrator are sufficiently distant on the sky that a
gain elevation dependence cannot be subsumed into the usual time
dependent gains.

ElevCor scales the visibilities themselves, it doesn't mess with
the gains. Actually, elevcor doesn't copy or apply existing
gain/pol/bandpass tables at all. If you want to retain
pre-existing calibration (eg. bandpass), copy them after with
gpcopy. Of course, existing amplitude gains won't be very
useful.

Currently, the only templates available are for 3cm and 3mm data.
For details of the gain elevation-dependence template, consult
Hayley Bignall.
%A vis
The names of the input uv data set. No default.
%A out
The name of the output uv data set. No default.
%A options
Extra processing options, minimum match allowed:
   simulate  Include a gain error to the data, rather
             than applying the correction. This can be
             used on data generated with uvgen to simulate
             elevation-dependence in fake uvdata.
   replace   Replace the data with the gain function.
