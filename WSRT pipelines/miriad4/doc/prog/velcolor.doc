%N velcolor
%D Make a red-green-blue image to display velocity as color.
%P mchw
%: image analysis
%B
VELCOLOR makes a 3-plane Miriad image to display the 3rd axis of
a 3-d Miriad image as color. The third axis can be represented by
color by superposing the 3 planes as red, green and blue images.
The most obvious use is to display the velocity axis as color.
The algorithm weights the channels so that the apparent intensity
of the superposed red-green-blue image planes is independent of
color. (For more details see Heiles & Jenkins, 1976, A&A 46,33)
%A in
The input image. No default.
%A region
The region of the input image to be used. The 3rd axis region
determines the range from blue to red. See documentation on region
for help of how to specify this. Only the bounding box is supported.
%A pivot
Center channel of input image for output green image.
Default is 0.4*trc+0.6*blc
%A out
The output red-green-blue image. No default.
%A clip
Two values. Exclude pixels with values in the range clip(1) to clip(2).
If only one value is given, then exclude -abs(clip) to abs(clip).
