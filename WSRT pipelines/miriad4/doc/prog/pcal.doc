%N pcal
%D Polarization calibration for circularly polarized feeds.
%P mchw
%: uv analysis
%B
PCAL is a MIRIAD task to fit source polarization and
instrumental leakage terms for circularly polarized feeds.
By default, PCAL applies the gain and bandpass calibration
before it processes the uv-data.
The fit depends on the noise and the parallactic angle coverage.
  1. Ideal case - no noise. Answers good for any range.
  2. Hatcreek Tsys=300 - for I=1 Jy, leakage is good to within 1% 
     if greater than 90 degree range in parallactic angle (chi).
  3. If I=10 Jy answers good to 1% for chi range=10 degrees;
     better than 0.5% for chi range more than 90 degrees.
%A vis
The name of the input visibility data-set. Several files can be
given, wildcarding is supported. No default.
%A select
The normal uv selection commands. See the Users manual for details.
The default is to process all data.
%A line
linetype used in fit.
        line,nchan,lstart,lwidth
where line is 'channel', 'velocity','felocity' or 'wide',
and lstart,lwidth are the line parameters.
Only the first channel is used in the fit.
The unflagged data are averaged; flagged data are discarded.
%A refant
reference antenna for leakage. Default=1.
%A leakref
complex leakage into reference antenna. Default=0.,0.
%A options
This gives extra processing options. Several options can be given,
each separated by commas. They may be abbreivated to the minimum
needed to avoid ambiguity. Possible options are:
   'nocal'      Do not apply the gains file.
   'nopass'     Do not apply bandpass corrections.
   'planet'     Use planet model for LL and RR instead of data.
