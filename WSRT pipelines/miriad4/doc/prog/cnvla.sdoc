%N cnvla %F $MIR/src/subs/cnvl.for
%D Convolve the beam with a subimage.
%P rjs
%: convolution, fft
%B
        subroutine CnvlA(handle,in,nx,ny,out,flags)

        implicit none
        integer handle,nx,ny
        character flags*(*)
        real in(nx*ny),out(*)

  This convolves, or correlates, an image by a beam pattern. The input
  image is passed in as an array. The beam pattern is passed in as a
  handle as previously returned by either the CnvlInA or CnvlInF routines.

  The output image can either be the same size as the input image (see
  flag='c' below), or the same size as the previously input beam. In the
  latter case, the output image is centered in the middle of the output
  array. In particular, if the beam is of size n1 x n2, and the input image
  is of size nx x ny, then the output image will be of size n1 x n2, and
  pixel (i,j) in the input will correspond to pixel (x0+i,y0+j) in the
  output, where
       x0 = n1/2 - nx/2
       y0 = n2/2 - ny/2

  Input:
    handle     Handle of the beam, previously returned by CnvlInA or CnvlInF.
    in         Input image to convolve.
    nx,ny      Image size.
    flags      Extra processing options:
                'c'    The output is the same size as the input. The
                       default is the output image is the size of the beam.
                'x'    Correlate with the beam. The default is to convolve
                       with the beam.
  Output:
    out        The convolved image.
