%N mirbug %F $MIR/src/scripts/mir.bug.csh
%D Send mail about bugs to appropriate person
%P bpw
%: user utility
%B
Usage: mirbug [taskname or 'general'] [file]

Mirbug allows the user to send bugreports to the appropriate persons.

[taskname] should be one of the miriad tasks/tools/scripts or the
string 'general'. This parameter is used to find out who is responsible
for the code and then the report is mailed to that person. If this
argument is not given the report is sent to some miriad central
addresses.

[file] (optional) is the name of a previously created file containing
the report. This must be the second argument.

mirbug creates a template report or prepends it to the optional file
and then starts an editor. The default editor is vi on unix and edt on
VMS. However, by including 'setenv EDITOR mem' in your .cshrc file
(unix) or 'editor:==mem' in the login.com file (VMS) it is possible to
specify which editor to use.

After exiting the editor the user is given the option of not mailing
the report by using ^C. If this is not done within 5 seconds, the
report is mailed to the person responsible, to a central address and
to the sender. If ^C is used, the report is saved.

``mirbug'' is an alias for ``mir.bug.csh bug''.
