%N uvarchdd
%D Print card catalog data values from uv dataset
%P mjs
%: uv analysis
%B
UVARCHDD lists selected uv variables in a MIRIAD dataset, to be
used in generating a card catalog of information on archived
Hat Creek data.  This task is specific to the needs of the BIMA
database, and is not intended for any other use (though no harm
will come from using it).

Though the program uses subroutine SelInput to look for `select'
keywords, the `select' keyword MUST NOT be present ... this 
reference should be removed for cleanliness someday.
%A vis
The input uv dataset name. No default.
%A proj
User-defined project name, up to 39 characters.  The default
is `-'.  Entries longer than 39 characters will be truncated.
%A machine
The machine IP number (not a locally-known alias) where the
archived data is stored.  If NCSA's Common File System (cfs),
enter `ncsa-cfs'.  39 characters maximum.  No default.
%A file
The fully-qualified filename (not a MIRIAD dataset name) of the
archived data.  1024 characters maximum (a presumed unix
MAXPATHLEN).  No member of the pathname may be longer than 16
characters.  No default.
%A size
The size in KB of the MIRIAD dataset.  Caution:  not all
machines give the same result from `du -s'.  For example, the
Cray2 (UNICOS 6.0/SYSV) returns a number that must be multiplied
by 4 to yield the size in KB.  The default is 0, implying that
no size was given.
%A cksum
Checksum, input as an integer.  The default is -1, implying that
no value was given.
%A flist
File containing the list of uv variables to be included in the
catalog.  The standard list of uv variables cataloged for Hat
Creek data is contained in file `$MIR/cat/uvctlg', and this
is the default.  Environment variables may be used in the
filename.  Maximum pathlength is 96 characters.
%A log
The list output file name. The default is the terminal.
