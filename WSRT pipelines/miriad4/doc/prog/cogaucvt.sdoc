%N cogaucvt %F $MIR/src/subs/co.for
%D Change gaussian parameters between world and pixel coords
%P rjs
%: coordinates
%B
        subroutine coGauCvt(lu,in,x1,ing,bmaj1,bmin1,bpa1,
     *                               outg,bmaj2,bmin2,bpa2)

        implicit none
        integer lu
        double precision x1(*)
        character in(*),ing*(*),outg*(*)
        real bmaj1,bmin1,bpa1,bmaj2,bmin2,bpa2

  This converts the parameters that describe a gaussian between pixel and
  world coordinate systems. The gaussian lies in the image formed by the
  first two axes of the coordinate system.

  Input:
    lu         Handle of the coordinate system.
    x1         This gives the coordinate of the centroid of the gaussian.
    in         This gives the units of the input gaussian centroid. This is
               in the same format as the "in" parameter of coCvt. For
               example, to indicate that the input coordinate consists of
               3 numbers in absolute world units, use 'aw/aw/aw'.
    ing,outg   These give the conversion to be performed. Possible values
               are:
                 'w'   Input or output is in world units.
                 'p'   Input or output is in pixel units.
    bmaj1,bmin1,bpa1  Input gaussian parameters: major axis, minor axis
               and position angle of major axis. The position angle is
               measured from "north" through "east" where north is the
               direction of increasing value along the second axis,
               and east is the direction of increasing value along the
               first axis. bmaj and bmin will either be in world or
               pixel units. bpa will be in radians.
  Output:
    bmaj2,bmin2,bpa2 Output gaussian parameters.
