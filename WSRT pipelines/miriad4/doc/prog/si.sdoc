%N si %F $MIR/src/subs/si.for
%D Sine integral
%P rjs
%: utilities
%B
      double precision function si(x)

      implicit none
      double precision x

  si computes the integral of sin(t)/t on (0,x) by means
  of Chebyshev expansions on (0,5) and (5,infinity).

  Inputs:
    x          Limit of integration.
  Output:
    si         Value of the integral.

  Accuracy:
    2 units of the 14th significant figure.

  References
    sand76-006c
    The special functions and their approximations, vol. ii, by
    Y.L. Luke. Academic Press, New York, 1969.
