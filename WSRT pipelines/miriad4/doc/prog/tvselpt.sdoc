%N tvselpt %F $MIR/src/subs/tv.for
%D Interactive point/range selection on a display device.
%P jm
%: tv, display
%B
      subroutine tvselpt(channel, type, x1, y1, x2, y2, ch)

      implicit none
      integer channel, type, x1, y1, x2, y2, ch

  TVSELPT allows interactive point or range selection on a TV screen.
  If the device is capable of creating different types of selection
  operations, then the input variable ``type'' sets that style.  If
  necessary (see ``type''), four input positions must be given.  The
  first position is registered when the left mouse button is pushed.
  Moving the mouse from that position alters the drawing according
  to ``type''.  When the left button is pushed again, the operation
  ends and returns the first and last mouse positions.  The operation
  is reset if the middle button is pushed before hitting the left
  button a second time or the mouse is moved outside of the window
  selected.  Selecting either the right button or hitting the STOP
  key (L1 on a Sun) aborts the operation.

  Input:
    channel  Tv Channel to act on.
    type     Style of selection operation (if used by the device):
               If ``type'' = 0, then a point and click selects a point.
               If ``type'' = 1, then a line from (x1,y1) to (x2,y2)
                 is drawn.
               If ``type'' = 2, then a box centered on (x1,y1) is
                 drawn.
               If ``type'' = 3, then a box with (x1,y1) at one corner
                 and (x2,y2) at the other corner is drawn.
               If ``type'' = 4, then a "V" is drawn from the start
                 point (x1,y1) to the present cursor position to
                 the end point (x2,y2).
               NOTE:  ``x1, x2, y1, y2'' MUST be input if ``type'' = 4.

  Input/Output:
    x1, y1   Coordinates of start location (not zoomed corrected).
    x2, y2   Coordinates of end location (not zoomed corrected).
    ch       Which mouse button was pushed to cause selection.
