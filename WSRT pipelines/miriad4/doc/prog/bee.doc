%N bee
%D Fit polynomials and antenna positions to the antenna gains.
%P mchw
%: uv analysis
%B
BEE is a Miriad task to fit polynomials and antenna positions
to the antenna gains. The gains can be plotted versus time, HA,
DEC, elevation, etc, and identified by source. Polynomial or
cursor drawn fits to the amplitude and phase can be written as
antenna gains and used to calibrate the data.
%A vis
The input UV dataset name. The antenna gains must first be
derived for the selected linetype by the task SELFCAL with
options=apriori,amplitude,noscale minants=3 interval=inttime
in order to fit Jy/K to individual integrations. No default.
%A log
The output log file. The default is 'bee.log'.
%A device
PGPLOT diplay device. (e.g. ? /tek /retro /sun /xw) No default.
Hardcopy plots can be created using cursor options.
%A refant
The gain of this antenna is set to cmplx(1.,0.). The
other antenna gains are relative to the reference antenna.
The default is to use the original reference antenna used to
derive the antenna gains. If refant.ne.0 then the total power
for the refant is subtracted from each of the others, and can
be plotted over the antenna phases to look for correlations.
%A refpwr
Put tpower for this reference antenna into the airtemp variable
in order to fit phase = a*tpower + b*tpower(refpwr) + c
using the ``TF'' 3-parameter fit        
%A out
The output gains file. The fits to the amplitude and phase
can be written as antenna gains and used to calibrate the data.
The default is to write the gains into the input uvdata file.
%A options
rescale      The total powers are scaled to the same range as
             the reference antenna. The default is no scaling.
