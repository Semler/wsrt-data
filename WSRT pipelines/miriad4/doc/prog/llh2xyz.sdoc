%N llh2xyz %F $MIR/src/subs/ephem.for
%D Convert from latitude/longitude/height to CIO (x,y,z).
%P rjs
%: utilities
%B
        subroutine llh2xyz(lat,long,height,x,y,z)

        implicit none
        double precision x,y,z,lat,long,height

  Convert betweena location defined in terms
  geodetic latitude, longitude, and height above the reference geoid to
  CIO coordinates.

  Reference:
    Kenneth R. Lang, "Astrophysical Formulae", pages 493-497.
    Values for flattening and equatorial radius from John Reynolds,
    who says they are the IAU 1976 values.

  Input:
   lat,long    Geodetic latitude and longitude, in radians.
   height      Height above the reference geoid (i.e. sea level), in meters.
  Output:
   x,y,z       CIO coordinates, in meters.
