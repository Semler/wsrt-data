%N aricomp %F $MIR/src/subs/ari.for
%D Parse a Fortran-like expression.
%P pjt
%: mathematics
%B
        subroutine ariComp(exp,paction,type,Buf,Buflen,RBuf,RBufLen)

        implicit none
        character exp*(*)
        integer type,BufLen,RBuflen
        integer Buf(Buflen)
        real    RBuf(RBufLen)
        external paction

  AriComp parses a FORTRAN-like expression, breaking it up into
  a sequence of reverse Polish tokens. The Fortran expression (given
  as a character string) can contain all normal FORTRAN numeric operators
  and functions, as well as constants and variables.

  AriComp calls a user written action routine, to determine information
  about "variables".

  See AriExec to see how to evaluate the expression after it has been
  parsed.

  Inputs:
    exp        The expression to parse. This should be a FORTRAN-like
               real expression.
    paction    Routine called to determine what each variable represents.
    buflen     Length of integer buffer.
    rbuflen    Length of real buffer.

  Outputs:
    type       Indicates status, and whether a scalar or vector expression
               was parsed. This can take a value of error, scalar or
               vector.
    buf        Sequence of tokens in reverse polish. The first four locations
               of buf are special, however (used by ariExec, caller
               will not be interested):
                 Buf(1)  Reserved.
                 Buf(2)  Reserved.
                 Buf(3)  Number of constants held at start of RBUF.
                 Buf(4)  Number of tokens held in BUF, including these 4.
    rbuf       Constants.

  This routine returns with a parsing error is found. However it aborts
  on buffer overflows. It has some internal buffers, which hopefully are
  big enough, but BUF and RBUF can also be too small.
