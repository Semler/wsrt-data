%N fftrc %F $MIR/src/subs/fftsubs.for
%D Real to complex 1D FFT
%: fourier-transform, fft
%P rjs
%B
        subroutine fftrc(in,out,isn,n)

        implicit none
        integer isn,n
        real in(n),out(n+2)

  This performs a 1D Fourier transform of a real sequence. There is no
  1/N scaling, and the "phase center" of the transform is the first
  element of the input array.

  Input:
    in         The input real array.
    isn        The sign of the exponent in the transform, This can be
               either 1 or -1.
    n          The number of elements to transform. This must be a power
               of 2.
  Output:
    out        The output array. Because of the conjugate symmetry of the
               FFT of a real sequence, only half of the full complex sequence
               is returned. Normally this array will be dimensioned of size
               N/2+1 complex elements. Element 1 corresponds to the "DC" term,
               element N/2+1 corresponding to the "folding frequency" term.
               This array could alternately be dimensioned as N+2 real
               elements.
