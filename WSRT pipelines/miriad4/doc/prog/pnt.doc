%N pnt
%D Display and Fit pointing data.
%P mchw
%: pointing analysis
%B
PNT is an interactive program for plotting, fitting and editing
pointing data obtained from total power measurements or voltage
pattern measurements with the interferometer.    The program is
directed by the commands below, and writes a log file  PNT.LOG,
and an ascii file of the final pointing constants, PNTFIT.date,
which is updated if the program is restarted.  This file can be
used to change the pointing constants used at Hat Creek. 
%A device
PGPLOT display device. Default is to prompt the user.
%A telescop
telescope name for telescope dependent pointing data format.
known telescopes are ATA BIMA OVRO and CARMA
default is telescop=CARMA with data format. Note telescop
is case sensitive and we use the upper case versions here.
%A pdevice
Hardcopy plot device in the format plot#/pgplot device.
# increments for sucessive plots. Default is 'plot1/ps'
%A log
Output log file. Default is 'pnt.log'
%A options
Special processing options. [Y/N]. Default=N.

COMMANDS

  IN    Read multiple files of  pointing data  written by the
        programs SPOINT or CROSS. The program starts by asking
        for the data files to be used.  Subsequent use of this
        command replaces the data to be reduced.  The data are
        edited to the pointing constants in the first pointing
        record read. Special options can suppress this editing,
        or remove the old pointing corrections from the data.

  LI    List the pointing constants, and optionally, the data.

  GR    Plot  DAZ  DEL  AZ  EL or TILT versus  AZ  EL or UT.
        The sources to be plotted may be selected and plotted
        with different symbols. The plot can be manipulated
        using CURSOR OPTIONS. 

  CO    Enter comment into log file.
  FL    Fit flux densities to radio data. (See code for format)
  HI    Histogram plot of pointing errors.
  FI    Fit pointing constants to azimuth or elevation errors.
        Points deleted in the plots are not included in the fit.
        Up to 9 parameters are fitted in a least squares sense
        to the following 4 equations. The Default is equation 3.

Equation 1 has tilt and sin(2*az), cos(2*az) terms:

daz = v(1)*cosel + v(2) + v(3)*sinel + sinel*(v(4)*sin(az)
    + v(5)*cos(az)) + cosel*(v(6)*sin(2.*az) + v(7)*cos(2.*az))

Equation 2 has sin(az) and cos(az) and 2*az terms:

daz = v(1)*cosel + v(2) + v(3)*sinel + cosel*(v(4)*sin(az)
    + v(5)*cos(az)) + cosel*(v(6)*sin(2*az) + v(7)*cos(2*az))

Equation 3 has tilt and sin(az), cos(az) terms:

daz = v(1)*cosel + v(2) + v(3)*sinel +sinel*(v(4)*sin(az)
    + v(5)*cos(az)) + cosel*(v(6)*sin(az) + v(7)*cos(az))

        The pointing constants represent the following:
        v(1) - Azimuth encoder + antenna mount offset from the
         meridian.
        v(2) - Collimation error of optical or radio axis from
        mechanical axis (orthogonal to the elevation axis).
        The optical and radio pointing will differ in this term.
        v(3) - Elevation axis misalignment w.r.t. azimuth axis.
        v(4) to v(7) - Tilt and azimuth axis errors.

Equations 1 to 3 fit the elevation pointing to:

del = v(1) +v(2)*sin(el) + v(3)*cos(el) + v(4)*sin(az)
    + v(5)*cos(az) + v(6)*sin(2*az) + v(7)*tan(1.5708-el)

        v(1) - Elevation encoder offset.
        v(2) and v(3) - Elevation errors. The radio pointing
        also includes the subreflector sag.
        v(4) and v(5) - tilt of azimuth axis.
        v(6) - Fourier analysis residual.
        v(7) - Refraction. Radio and optical pointing differ. 

Equation 4 fits tilt and sin and cos az and 2az terms
in a different order:

daz = v(1)*cosel + sinel*(v(2)*sin(az) + v(3)*cos(az))
    + v(4) + v(5)*sinel + cosel*(v(6)*sin(az) + v(7)*cos(az))
                    + cosel*(v(8)*sin(2*az) + v(9)*cos(2*az))

del = v(1) + v(2)*sin(az)) + v(3)*cos(az) + v(4)*sin(el) 
    + v(5)*cos(el) + v(6)*sin(2*az) + v(7)*cos(2az)
    + v(8)*tan(1.5708-el)

Equation 5 fits Gerry's daz = v(5) * sin(El) * cos(El) for ATA - 09mar2005

daz = v(1)*cosel + sinel*(v(2)*sin(az) + v(3)*cos(az))
    + v(4) + v(5)*sinel*cosel + cosel*(v(6)*sin(az) + v(7)*cos(az))
                    + cosel*(v(8)*sin(2*az) + v(9)*cos(2*az))

del = v(1) + v(2)*sin(az)) + v(3)*cos(az) + v(4)*sin(el) 
    + v(5)*cos(el) + v(6)*sin(2*az) + v(7)*cos(2az)
    + v(8)*tan(1.5708-el)

Although the functions in these equations are not orthogonal,
additional corrections may be added to the pointing constants
already in use provided that sufficient pointing data are taken
to separate the terms.  The correlation matrix for each fit,
indicates the degree of independence of the parameters fitted. 

Avoid OVERFITTING THE DATA. Fitting more parameters will
improve the rms for any data set but a large sample of data
points is needed to obtain significant results. After
moving the antennas only the azimuth offset and tilt should change.
The other pointing parameters should not be fitted with small 
data sets. 

  ED    Edit the pointing data, either to the constants fitted
        above, or to another set entered from the keyboard. 

SOURCE SELECTION

Plotting and fitting commands request a selection of sources.
"A" includes all sources. <CR> ends the source selection.

CURSOR OPTIONS

The plotting programs allow manipulation of the data directed
by the cursor position and typing one character to select
the option. A group of data points can be selected by L(eft),
R(ight), T(op), and B(ottom). V(anish) removes points from the
plot and subsequent fits.  A(ppear) restores all the data.
I(dentify) labels the data. W(rite) lists the labels. N(ew) sets
new limits for the plot. M(ore) adds points to a plot. S(ee)
replots the data and P(rinter) makes a printer plot. H(elp)
lists the cursor options.
