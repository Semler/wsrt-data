%N hsortar %F $MIR/src/subs/hsort.for
%D Perform a dual index heapsort on a character string array.
%P jm
%: sorting
%B
      subroutine hsortar(n, array, second, indx)

      implicit none
      integer n, indx(n)
      character*(*) array(n)
      real second(n)

  HSORTAR performs an index based heapsort on a character string array.
  If there are matching elements in the primary (character) array, the
  corresponding secondary (real) array elements are used to resolve the
  ambiguity.  The number of elements in the array and the character and
  real arrays are left unchanged.  The size of the indx array should be
  at least as large as the size of the character and real arrays.

     Input:
       n        The number of elements in the character array.
       array    The character array on which to base the primary sort.
       second   The real array on which to base the secondary sort.

     Output:
       indx     Sorted integer index array such that array(indx(1)) is
                the smallest and array(indx(n)) is the largest string.
