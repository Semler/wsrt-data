%N uvphase
%D Analyse phase statistics from a uv dataset
%P mchw
%: uv analysis
%B
uvphase computes phase statistics from a uv dataset. Either
structure function, Allan deviation, or Spectra can be calculated.
Each baseline and time interval is averaged independently.
  The default calculates the structure function, the output lists:
number of records averaged, time(days), antenna pair, uvdist
elevation(radians), [phase_rms*sqrt(sin(elevation)),
phase_rms(degrees), flag(*=bad), for i=1,nchan].
  If Allan deviations are calculated, the output lists:
number of records averaged, time(days), antenna pair, 
sampling_interval, uvdist, [amplitude_deviation,
phase_deviation(degrees), flag(*=bad), for i=1,nchan].
  If Spectra are calculated, the output lists:
number of records averaged, time(days), antenna pair, 
frequency, uvdist, [phase(degrees/Hz,
phase(degrees), flag(*=bad), for i=1,nchan].
  The Miriad task ``rmsfit'' can be used to fit power laws to
the output log file.
%A vis
The input UV dataset name. No default.
%A select
This selects the data to be processed, using the standard
uv-select format. Default is all data.
%A line
The linetype to be analysed, in the form:
    type,nchan,start,width,step
where type can be `channel' (default), `wide' or `velocity'.
The default is channel data, a maximum of 4 channels will be used.
%A interval
Two values giving the averaging time, and the maximum time interval
between samples in minutes. Default averaging time = 1 min.
The default maximum interval between samples is the averaging time.
%A base
Units and reference baseline for uvdist in meters. Default=100m.
%A options
This gives extra processing options. Several options can be given,
each separated by commas. They may be abbreviated to the minimum
needed to avoid ambiguity. Possible options are:
   'psf'        Compute the rms phase for each time interval.
   'allan'      Compute Allan deviation for each time interval.
                    sqrt{<[a(i-k)-2a(i)+a(i+k)]**2>}
                where k is the sampling interval. Minimum 10 records.
   'spect'      Compute spectra of phase for each time interval.
   'topo'       Use the topographic baseline length for uvdist.
                Default is to use the projected baseline. 
   'unwrap'     Attempt to extend phases beyond -180 to 180 degrees
   'mm'         Scale phase to path length in mm. Default is degrees.

Only one of 'psf' 'allan' and spect' can be chosen.
The default is the phase structure function 'psf'. 
%A log
The list output file name. The default is the terminal.
%A device
PGPLOT device to plot PSF. Default is no plot.
%A xrange
Plot range in the x-direction. 2 values. Default is 10m to self scale.
%A yrange
Plot range in the y-direction. 2 values. Default is to self scale.
