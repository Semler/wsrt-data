%N xyzsetup %F $MIR/src/subs/xyzio.c
%D Set up arbitrary subcube
%P bpw
%: image-i/o
%B
      subroutine xyzsetup( tno, subcube, blc, trc, viraxlen, vircubesize )
      integer       tno
      character*(*) subcube
      integer       blc(*), trc(*)
      integer       viraxlen(*), vircubesize(*)

This routine does the definitions necessary to allow reading or writing
an arbitrary subcube in a n-dimensional datacube. It is used to define
which part of an input datacube should be read or which part of an
output datacube should be written. 

The variable subcube is used to define the axes of the subcubes to be
read or written. The axes of a datacube are called 'x', 'y', 'z', 'a',
'b', ... 'subcube' consists of any non-redundant combination of these.
Zero-dimensional subcubes, i.e. single pixels, are specified by setting
'subcube' to ' '. 'subcube' can be e.g. 'z' to read/write lines in the
z-direction, 'xy' to read/write image planes or 'xyz' to read/write a
3-d subcube. Permutations (like 'zx') are also permitted. These will be
reflected in the ordering of the output array produced by subroutine
xyzread. Axis reversals are also possible, and are set up by preceding
the axis name with a '-'. Again, this will be reflected in the ordering
of elements in the output array of xyzread.
If xyzsetup is used to define an output cube, the subcube variable
should be interpreted as giving the names of the axes in the virtual
cube. E.g., for subcube='z', the first axis in the virtual cube is the
z-axis of the output cube. The second  axis then is the 'x'  axis, etc.

blc and trc give the bottom left and top right corner of the total
region of the input cube that needs to be worked on (in absolute pixels,
i.e. the bottom left of the original cube is 1,1,...), or the total
region of the output cube that should be written. If the output cube
did not yet exist and the region is smaller than the defined size of
the cube, the pixels outside the region are set to zero automatically.

viraxlen and vircubesize are provided for the convenience of the
programmer. They correspond to a virtual cube, whose axes are permuted
according to the specification of 'subcube', and whose axislengths are
given by the differences of blc and trc. This virtual (intermediate)
cube contains all the pixels on which the calling program should work,
sorted in the order in which they are needed.

With a call to xyzsetup all previous buffers are irrevocably lost; but
output buffers are flushed before that. However, all calls to xyzsetup
should be done before working on the data.

    Input:
      tno           image file handle
      subcube       a character variable defining the subcube type
      blc, trc      arrays giving the bottom left and top right corner
                     of the region in the input/output cube to work on/
                     write; the number of elements used equals the
                     dimension of the input/output cube
    Output:
      viraxlen:     length of axes of virtual cube
      vircubesize:  size of subcubes:
                     vircubesize(d) = Prod(i=1->d) viraxlen(i)                 
