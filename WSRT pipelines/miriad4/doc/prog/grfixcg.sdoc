%N grfixcg %F $MIR/src/subs/cgsubs.for
%D Fix up a grey scale range with optional bias for log taking
%P nebk
%: plotting
%B
      subroutine grfixcg (pixr, lgin, gnaxis, gsize, trfun, 
     +                    pixr2, groff, blankg)

      implicit none
      real pixr(2), pixr2(2), groff, blankg
      integer lgin, gnaxis, gsize(*)
      character trfun*(*)

  Make sure the grey scale range is valid, and take logs if
  desired.  This may require a DC bias to avoid negative 
  numbers in the image.

  Input:
    lgin     Handle for image
    gnaxis   Number of dimesions in image
    gsize    Size of dimensions
    trfun    'log', 'lin', 'heq', or 'sqr' transfer functions
  Input/Output:
    pixr     User supplied grey scale range.  Defaults filled in
             on output
  Output:
    pixr2    Grey scale range with bias and logs/sqrt taken if necessary
    groff    DC bias to avoid negatives in image if logs taken
    blankg   Value to use for blanked pixels
