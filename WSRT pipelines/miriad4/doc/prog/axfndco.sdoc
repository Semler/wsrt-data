%N axfndco %F $MIR/src/subs/cosubs.for
%D Find a specified generic axis in an image
%P nebk
%: coordinates
%B
      subroutine axfndco (lun, type, n, iax, jax)

      implicit none
      integer n, iax, jax, lun
      character*(*) type

  Find generic axis type in image.

  Input
    lun    Image handle
    type   Generic axis type to find.  The first axis encountered
           that has this type is returned.  The type should be one of:

             RA   ->  RA, LL, ELON, GLON
             DEC  ->  DEC, MM, ELAT, GLAT
             LONG ->  ELON, GLON
             LATI ->  ELAT, GLAT
             VELO ->  VELO, FELO
             FREQ ->  FREQ
             UV   ->  UU, VV
             ANGL ->  ANGLE
             RAD  ->  An axis whose increment should be in
                      radians.  These are RA, DEC, LAT, LONG, 
                      ANGL axes as described by the LHS above.
           Other types are searched for exactly as specified
    n      Number of axes to search starting from 1
    iax    SPecific axis to match if N=0
  Output
    jax    Axis number that matches "type".  0 if not present
