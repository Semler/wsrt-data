%N dayjul %F $MIR/src/subs/julday.for
%D Format a conventional calendar day into a Julian day.
%P jm
%: julian-day, date, utilities
%B
      subroutine dayjul(calday, julian)

      implicit none
      character calday*(*)
      double precision julian

  Convert to Julian date from calendar date. This is not to high
  accuracy, but it achieves the accuracy required. See "Astronomical
  Formulae for Calculators", Jean Meeus (Wiillmann-Bell Inc).
  The day is assumed to begin at 0 hours UT.

  Input:
    calday   (Gregorian) Calendar day (UT time).
             The input must be one of the following forms:
                     `yymmmdd.dd'                     (D)
             or:
                     `dd/mm/yy'                       (F)
             or:
                     `[yymmmdd:][hh[:mm[:ss.s]]]'     (H)
             or:
                     `ccyy-mm-dd[Thh[:mm[:ss.s]]]'    (T)
             or:
                     `dd-mmm-ccyy'                    (V)
             where the brackets delimit optional values.

             If dashes and the literal `T' are present, then the
             (T) format is assumed and decoded.  The presence of
             slashes (/) indicate the (F) format and the presence
             of dashes and an alpha month indicate the (V) format.
             The presence (or lack of) a period (`.') immediately
             after the `yymmmdd' string determines whether to use
             the (D) or (H) formats.  If the period is present,
             the entire decimal day string is expected to be present
             (ie. `yymmmdd.dd').  If the (H) format is entered, then
             the presence (or lack of) the `yymmmdd' string determines
             the range of the output value.  Without the `yymmmdd'
             string, the output julian date will be in the range [0, 1].
             If the `yymmmdd' portion of the string is present, then
             the full julian date is computed.  If the (H) format is
             entered but no time string is included, then the trailing
             colon may be omitted (eg. `98feb01').

             When the input string is of the (H) type, it may
             be abbreviated with the missing entries internally
             filled with a value of zero.  With the `yymmmdd' string
             included, the year, month, and day values are decoded
             and then the string is decoded for the time value.
             Without the `yymmmdd' string or after it has been
             decoded, the routine will interpret the string up to
             the next colon as `hh'; text after that colon as `mm';
             and any text after the next colon as `ss.s'.  Hence,
             an input string of `3' will be interpreted as if it were
             entered as `03:00:00.0'.  Also, if the input string was
             `3:10', it will be interpreted as `03:10:00.0'.

  Output:
    julian   Julian date (may be an offset date; see `calday' above).
