%N puthd
%D Change the value of or add a single header item
%P pjt
%: utility
%B
PUTHD is a MIRIAD task to add or modify an item in the ``header''
of an image or uv dataset. The item CANNOT be an array or any other
complex data structure, it must be a single entity. To modify
such complex data structures, specialized programs are available.

Be careful when changing certain keywords which have an implied
unit. E.g. the crvalN keywords is an angular unit, and those are
assumed to be in radians in MIRIAD, not degrees as they are in FITS.
%A in
The name of an item within a data set. This is given in the
form as in the example:
       puthd in=dataset/item
%A value
The value to be placed in the item. Note only single values can
be given, no arrays. The units are native units as defined in an
Appendix of the Users Guide.
An optional second argument can be given to cause conversion of the 
value before the item is written. Possible values for the units 
are "time", "arcmin", "arcsec", "hours", "hms" and "dms". 
Times are given in the standard Miriad form and are converted to 
Julian dates. An angular unit causes conversion to radians, except 
for "bpa" which uses degrees as its native unit.
%A type
The data type of the argument. Values can be 'integer',
'real', 'double' and 'ascii'. The default is determined from the
format of the value parameter or from the type of the item if it
was already present. Normally you can allow this parameter to default.
PUTHD will complain when you change the datatype, but otherwise
allow you to do so.
