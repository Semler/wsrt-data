%N getpb %F $MIR/src/subs/getpb.for
%D Determine the primary beam associated with a image.
%: image-data
%P mchw
%B
        subroutine GetPB(tno,name,pbfwhm)

        implicit none
        integer tno
        character name*(*)
        real pbfwhm

  Determine the primary beam size of particular telescopes. This compares
  the header parameter 'telescop' with the list of known telescopes.
  The "size" array contains the primary beam size, in units of
  arcseconds * GHz. So we must divide this number by the sky freq.

  Input:
    tno        The file handle.
    name       Name of the input file. Used for error messages only.
  Output:
    pbfwhm     The primary beam size, in arcseconds. If it is a single
               dish telescope, this is set to zero.

  References:
    VLA beam size: From AIPS LTESS, which attributes the measurement to
       a test memo by Rots and Napier.
    Hat Ck size: From Bima Users Guide - Jan89, which claims this is the
       value corresponding to a 6 meter telescope. See caveats there.
