%N llsquini %F $MIR/src/subs/lsqu1.for
%D Initalize the "large" linear least square routines.
%P rjs
%: least-squares, fitting
%B
        subroutine LlsquIni(x,B,n)

        implicit none
        integer n
        real B(n,n),x(n)

  This routine is used with the LlsquAcc and LlsquSol routines, to solve
  a least squares problem, where the number of "equations" is large, but the
  number of unknowns is comparatively small.

  The system of equations for which we are looking for a least square solution i
       f = Ax

  This routine initalises, and the LlsquAcc routine is called with a set of
  equations (and accumulates the needed statistics). Finally the LlsquSol
  routine is called to finally solve the problem.

  Inputs:
    n          Number of unknowns in the system of equations.
  Output:
    B          Matrix used to store statistics. This is initialised.
    x          This holds more statistics, and eventually the solution to
               to the unknowns.
