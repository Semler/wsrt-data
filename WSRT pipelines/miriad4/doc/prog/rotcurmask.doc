%N rotcurmask
%D Create a mask cube based on a rotation curve model
%P pjt
%: image analysis
%B
ROTCURMASK creates an output mask cube from an input data cube
based on a adaptive window around a velocities from a model 
velocity field.  The output mask cube can then be used in MOMENT 
to create a more realistic velocity field.

Warning: ROTCURMASK allocates two full MAXDIM3*MAXDIM3 maps for
faster computations. (defaulted to 2048**2 maps)
%A in
The input data cube. The fist two axes should be
spatial, the third axis should have units km/s (or something
equivalent).
No default.
%A out
The output mask cube data set. 
No default.
%A rotmod
Input rotation model velocity field.
Currently this map should have the same 2D shape as the first
two (spatial) dimension of the input cube.
This map can be made from MIRIAD's velmodel or NEMO's ccdvel program.
Units should be km/s.
%A mom2
Optional map of velocity dispersion (e.g. created from MOMENT MOM=2)
to control the window size.
If none is given, the MINSIGMA parameter (see below) will be used
as a global number. Optionally an mom2 map can also be created using
IMGEN. Units of this map should be km/s.
%A minsigma
Minimum sigma, in case mom2 has lesser (or no) value. 
Should be a number in units of km/s. Default: 5.
%A window
The multiplicative factor by which sigma is multiplied to create
a window  +/- max(MINSIGMA,MOM2) * WINDOW around the model velocity.
Data outside this windows are masked false.
Default: 3.
