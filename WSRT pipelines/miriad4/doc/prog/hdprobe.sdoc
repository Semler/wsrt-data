%N hdprobe %F $MIR/src/subs/headio.c
%D Determine characteristics of a header variable.
%P mjs
%: header-i/o
%B
FORTRAN call sequence:
        subroutine hdprobe(tno,keyword,descr,type,n)
        integer tno
        character keyword*(*),descr*(*),type*(*)
        integer n

  Determine characteristics of a particular header variable.
  Inputs:
    tno         Handle of the data set.
    keyword     Name of the header variable to probe.

  Outputs:
    descr       A formatted version of the item. For single numerics or
                short strings, this is the ascii encoding of the value. For
                large items, this is some message describing the item.
    type        One of:
                  'nonexistent'
                  'integer*2'
                  'integer*8'
                  'integer'
                  'real'
                  'double'
                  'complex'
                  'character'
                  'text'
                  'binary'
    n           Number of elements in the item. Zero implies an error. One
                implies that "descr" is the ascii encoding of the value. 
