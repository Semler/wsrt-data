%N whenigt %F $MIR/src/subs/math.for
%D Return locations greater than the integer target.
%P pjt
%: scilib
%B
        subroutine whenigt(n,array,inc,target,index,nval)

        implicit none
        integer n,inc,nval
        integer index(*)
        integer array(*),target

  Whenigt returns the all the locations in an integer array greater than
  the integer target.

  Inputs:
    n          Number of elements to be searched.
    array      The integer array to be searched.
    inc        Skip distance between elements of the searched array.
    target     Integer value to be searched for in the array.

  Output:
    index      The integer array containing the index of locations
               greater than the target.
    nval       Number of values put in the index array.

  Reference:
  See page 4-65 to 4-71 of the Cray "Library Reference Manual".
