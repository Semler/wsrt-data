%N apptrfcg %F $MIR/src/subs/cgsubs.for
%D Apply transfer function to image
%P nebk
%: plotting
%B
      subroutine apptrfcg (pixr, trfun, groff, size, nimage, image,
     +                     nbins, his, cumhis)

      implicit none
      integer nimage(*), size, nbins, his(nbins)
      real groff, image(*), pixr(2), cumhis(*)
      character trfun*3

  Apply the desired transfer function to the image

  Input:
   pixr     Intensity range with NO bias or logs/sqrt taken 
   trfun    Transfer function.  "lin", "log", "heq" or "sqr"
   groff    Bias to make image positive if necessary
   size     Size of image
   nimage   Normalization image
   nbins    Number of bins for histogram equalization
  Input/output:
   image    Image.  Transfer function applied on output. Pixels
            below pixr(1) are set equal to pixr(1)
   his      Image histogram for histogram equalization
   cumhis   Cumulative histogram for histogram equalization
            Values for each bin are the intensities assigned to 
            the image.  Thus if an image pixel ended up in 
            cumhis bin idx, then its new value is cumhis(idx)
