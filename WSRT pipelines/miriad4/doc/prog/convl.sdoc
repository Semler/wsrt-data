%N convl %F $MIR/src/subs/convl.for
%D Performs the convolution of the image with the beam.
%P rjs
%: convolution, fft
%B
        subroutine Convl(in,out,n,nx,ny,Runs,nRuns,Beam,n1,n2)

        implicit none
        integer n,nx,ny,n1,n2,nRuns,Runs(3,nRuns)
        real in(n),out(n),Beam(n2,n1/2+1)

  Convolve the input by the beam, returning it in the output. Both the
  input and output are only those portions of the image that are of
  interest. These are described by "runs" arrays, as produced by
  BoxRuns.

  Input:
    in         The input image. Only the pixels within the
               region-of-interest are included.
    n          Total number of pixels in the region-of-interest.
    nx,ny      The bounding size of the input image.
    Runs       The runs describing the region-of-interest in the input
               image.
    nRuns      The total number of runs.
    Beam       The FFT of the beam, as produced by ConvlIni.
    n1,n2      The size of the beam, in x and y.
  Output:
    out        The resultant convolved image.
