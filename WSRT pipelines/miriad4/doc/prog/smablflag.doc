%N smablflag
%D Interactive flagging task.
%P jhz
%: uv analysis
%B
SmaBLFLAG is an interactive flagger. SmaBLFLAG plots the visibilities
(e.g. time vs amplitude or antel vs systemp) either a baseline at a 
time or all at once, and allows you to flag discrepant points using 
the plotting cursor. The sources and polarization states are color-coded.
There are a few simple flagging commands, which  you enter as a single 
character at the keyboard. The following commands 
are possible:
  Left-Button  Left mouse button flags the nearest visibility.
  Right-Button Right mouse button causes SmaBLFLAG to precede to the
               next baseline.
  <CR>         Carriage-return gives help.
  ?            Help.
  a            Flag nearest visibility (same as left mouse button).
  c            Clear the flagging for this baseline, and redraw plot.
  h            Give help (same as carriage return).
  p            Define a polygonal region, and flag visibilities
               within this region. You define the vertices of the
               polygon by moving the cursor and then hitting the
               left mouse button (or a). You finish defining the
               polygon by hitting the right mouse button (or x).
               You can delete vertices with the middle mouse
               button (or d).
  q            Abort completely. This does not apply flagging.
  r            Redraw plot.
  u            Unzoom.
  x            Move to next baseline (same as right mouse button).
  z            Zoom in. You follow this by clicking the mouse on the
               left and right limits to zoom.
The visibility data are color-coded for the program sources.
%A vis
Input visibility dataset to be flagged. No default.
%A line
The normal Miriad linetype specification. SmaBLFLAG will average
all channels together before displaying them, and any flagging
that you do will be applied to all selected channels. The default
is all channels.
%A device
Normal PGPLOT plotting device. It must be interactive. No default.
%A stokes
Normal Stokes/polarisation parameter selection. The default
is `ii' (i.e. Stokes-I assuming the source is unpolarised).
NOTE SmaBLFLAG plots the average of all the selected Stokes/polarisation
quantities. Also it flags ALL quantities, regardless of whether they
were selected or not.
%A select
Normal visibility data selection. Only selected data can be
flagged. The default is to select all data.
%A axis
Two character strings, giving the X and Y axes of the plot. Possible
axis values are:
  time         (the default for the X axis)
  lst          Local apparent sidereal time.
  antel        antenna elevation in degree;
               only for systemp flagging.
  uvdistance   sqrt(u**2+v**2)
  hangle       (hour angle)
  amplitude    (the default for the Y axis)
  phase
  real
  imaginary
  systemp      (only for Y axis)
%A options
Task enrichment parameters. Several can be given, separated by
commas. Minimum match is used. Possible values are:
  nobase  Normally SmaBLFLAG plots a single baseline at a time.
          This option causes all baselines to be plotted on
          a single plot.
  selgen  Generate a file appropriate for selecting the bad
          data (via a "select" keyword). The output is a text
          file called "blflag.select".
  noapply Do not apply the flagging.
  rms     When processing spectra, blflag normally plots the
          mean value of the spectra. Using options=rms causes
          if to plot the rms value instead.
  scalar  When processing spectra, blflag normally forms an
          average value by vector averaging. The "scalar" option
          causes it to generate the scalar average. This option
          should be used with significant caution.
The following options can be used to disable calibration.
  nocal   Do not apply antenna gain calibration.
  nopass  Do not apply bandpass correction.
  nopol   Do not apply polarisation leakage correction.
