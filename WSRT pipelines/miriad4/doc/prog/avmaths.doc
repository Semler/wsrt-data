%N avmaths
%D Operate on cube with averaged plane from cube
%P nebk
%: analysis
%B
AVMATHS averages designated planes from a cube, and then 
performs some mathematical operation on the cube with the 
averaged plane. Currently subtraction, optical depth, and
replacement operations have been coded.  Undefined output
pixels are blanked.
%A in
The input image. Wild card expansion is supported. No default.
%A out
The output image. No default.
%A region
Specify the channels to average with a REGION=IMAGE
command such as
  region=image(1,5),image(120,128)
This would average channels 1:5 and 120:128 from the
cube.  No other region commands are accepted for example
spatial sub-regions will be ignored.
%A options
Task enrichment options.  Minimum match is active.

"subtract" for subtraction:   OUT(i,j,k) = IN(i,j,k) - AV(i,j)
"odepth"   for optical depth: OUT(i,j,k) = LN (AV(i,j) / IN(i,j,k))
"replace"  for replacement:   OUT(i,j) = AV(i,j)
"multiply" for multiplication OUT(i,j,k) = IN(i,j,k) * AV(i,j)

"noreduce" causes the output image to be of the same dimensions
    as the input image when REPLACEMENT is invoked.  By default,
    the REPLACED output image is reduced to two dimensions as
    there is probably no point to replicating one plane N times.
    In this case, the output third axis descriptors reflect the size
    of the bounding box of the selected region on the third axis

Pixels are blanked if the input pixel is blanked, the averaged
channel pixel is blanked, or the output is undefined.
