.TH uvfit 1
.SH NAME
uvfit - Fit point sources to a given vis file.
.SH PERSON RESPONSIBLE
rjs
.SH CATEGORIES
uv analysis
.SH PARAMETERS
.TP
\fIvis\fP
Name of the input visibility file. No default.
.TP
\fIstokes\fP
Normal Stokes/polarisation parameter (e.g. i,q,u,v,ii etc).
Only a single polarisation can be requested. The default is
`ii' (i.e. Stokes-I for an unpolarised source).
.TP
\fIline\fP
Normal line-type processing with normal defaults. However, you
must select only a single channel!!
.TP
\fIselect\fP
Normal data selection. Default is all cross-correlation data.
.TP
\fIobject\fP
This gives the object type that uvfit fits for. Several objects
can be given (the objects can be of the same type, or different),
and minimum match is supported. Possible objects are
.nf
  point       A point source
  disk        An elliptical or circular disk.
  gaussian    An elliptical or circular gaussian.
  shell       The 2D projection of a thin, spherical shell.
  ring        A face-on, thin, elliptical or circular ring
.fi
For example, to fit for a point source and gaussian, use:
`object=point,gaussian'.
.TP
\fIspar\fP
This gives initial estimates of source parameters.  For
each object given by the `object' keyword, either 3 (for
point sources) or 6 (for disks and gaussians) values should be
given. The values are as follows:
.nf
  Object Type             SPAR values
  -----------             -----------
   point                   flux,x,y
   gaussian                flux,x,y,bmaj,bmin,pa
   disk                    flux,x,y,bmaj,bmin,pa
   shell                   flux,x,y,bmaj
   ring                    flux,x,y,bmaj,bmin,pa
.fi
.PP
Here "flux" is the total flux density of the component,
"x" and "y" are the offset positions (in arcsec) of the object
relative to the observing center, "bmaj" and "bmin" are the major
and minor axes FWHM (in arcsec), and "pa" is the position angle
of an elliptical component (in degrees). The position angle is
measured from north through east.
You must give initial estimates for all parameters for each object
(this includes parameters that are redundant or meaningless,
such as "bmin" and "pa" for components that are constrained to be
circular).
.PP
The more complex the set of objects being fitted for, the more
important it is to give a good estimate of the source parameters.
Generally the estimates of the source position should be accurate
to the fundamental resolution (for point sources) or the size of
the component (for extended sources).
.TP
\fIfix\fP
This gives a set a flag parameters, one parameter per source.
Each parameter consists of a set of letters, which indicate
which source parameters of a component are to be held fixed.
These source parameters are fixed by the initial estimates
given by the `spar' parameter.
The letters corresponding to each source parameter are:
.nf
  f   The flux is fixed.
  x   The offset in RA is fixed.
  y   The offset in DEC is fixed.
  a   The major axis parameter is fixed.
  b   The minor axis parameter is fixed.
  p   The position angle parameter is fixed.
  c   The gaussian, disk or ring is circular (not elliptical).
.fi
For a source where all source parameters vary, a dash (-)
can be used for this parameter.
.PP
For example "fix=fx,fc" indicates that the flux and RA offset
is to be fixed for the first source, whereas the second source,
(which is presumably a gaussian, disk or ring) has a fixed flux, and
is circular.
.TP
\fIout\fP
The optional output data-set. The default is not to create an
output data-set. If an output dataset name is given, then
either the model or residual visibilities can be saved.
.TP
\fIoptions\fP
Extra processing options. Several can be given, separated by commas.
Minimum match is used. Possible values are:
.nf
  residual The output data-set is the residual visibilities.
           If an output is being created, the default is to make
           this the fitted model.
