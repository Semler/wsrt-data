.TH uvmodel 1
.SH NAME
uvmodel - Add, subtract, etc, a model from a uv data set.
.SH PERSON RESPONSIBLE
mchw
.SH CATEGORIES
uv analysis
.SH DESCRIPTION
UVMODEL is a MIRIAD task which modifies a visibility dataset by a model.
Allowed operations are adding, subtracting, multiplying, dividing,
replacing, and polarization calibration or simulation.
The model is specified in the image domain, so that its
Fourier transform is first computed before application to the
visibilities. The model may be either an image (e.g., a CLEAN
component image) or a point source.
.PP
An example is as follows. UVMODEL could be used to remove CLEAN
components from a visibility data file.  The residual data base could
then be examined for anomalous points, which could in turn be clipped.
UVMODEL could then be reapplied to add the CLEAN components back into
the visibility data base for re-imaging. 
.SH PARAMETERS
.TP
\fIvis\fP
Input visibility data file. No default
.TP
\fImodel\fP
Input model cube. The default is a point source model.
This will generally be a deconvolved map, formed from the visibility
data being modified. It should be made with ``channel'' linetype.
The model should have units of JY/PIXEL and be weighted by the primary
beam. The task DEMOS can be used to extract primary beam weighted
models from a mosaiced image.
.TP
\fIselect\fP
The standard uv selection subcommands. The default is all data.
.TP
\fIoptions\fP
This gives extra processing options. Several values can be given
(though many values are mutually exclusive), separated by commas.
Option values can be abbreviated to uniqueness.
Possible options are (no default):
.nf
  add       Form: out = vis + model
  subtract  Form: out = vis - model
  multiply  Form: out = vis * model
  divide    Form: out = vis / model
  replace   Form: out = model
  flag      Form: out = vis, but flag data where the difference
            between vis and model is greater than "sigma" sigmas.
  polcal    Correct polarization leakage using a total intensity model.
                out = vis - model * (polcor(q,i)+conjg(polcor(p,j)))
                where p,q are the polarization and i,j the antennas.
  poleak    Simulate polarization leakage using a total intensity model.
                out = model * (polcor(q,i)+conjg(polcor(p,j)))
                where p,q are the polarization and i,j the antennas.
  unflag    Unflag any flagged data in the output.
  autoscale Adjust the scale of the model to minimise the difference
            between the model and the visibility.
  apriori   Use flux from flux table or data file planet info.
  imhead    Much ``header'' information in the uv file is ignored -- the
            model information is used instead. In particular, the
            observing center of the uv file is taken to be the reference
            pixel of the model. By default, if the reference pixel
            and the uv data observing center are different, a phase
            shift is applied to the model visibilities to align them.
            The ``imhead'' option prevents this shifting.
  selradec  This causes UVMODEL to select only those visibilities
            whose observing center is within plus or minus three
            pixels of the model reference pixel. This is needed
            if there are multiple pointings or multiple sources in
            the input uv file. By default no observing center
            selection is performed.
  polarized The source is polarized. By default the source is
            assumed to be unpolarized. For a polarized source,
            UVMODEL cannot perform polarization conversion. That is,
            if the model is of a particular polarization, then the
            visibility file should contain that sort of polarization.
            For example, if the model is Stokes-Q, then the visibility
            file should contain Stokes-Q.
  mfs       This is used if there is a single plane in the input
            model, which is assumed to represent the data at all
            frequencies. This should also be used if the model has
            been derived using MFCLEAN.
  zero      Use the value zero for the model if it cannot be 
            calculated. This can be used to avoid flagging the 
            data in the outer parts of the u-v-plane when subtracting
            a low resolution model.
.fi
The operations add, subtract, multiply, divide, replace and flag are
mutually exclusive. The operations flag and unflag are also mutually
exclusive.
.PP
The unflag option should be used with caution. Data in the output
may still be flagged, if it was not possible to calculate the
model.
.TP
\fIpolcor\fP
The instrumental polarization for LR and RL data (or XY and YX).
Four values for each antenna, being the real and imaginary parts for
leakage into L and R polarizations respectively. See options=polcal.
The leakage table is usually obtained from the tasks PCAL, or GPCAL.
.TP
\fIclip\fP
Clip level. Pixels in the model below this level are set to zero.
The default is not to perform any clipping.
.TP
\fIflux\fP
If MODEL is blank, then the flux (Jy) of a point source model should
be specified here. Also used as the default flux in the apriori
option. The default is 1 (assuming the model parameter is not given).
.TP
\fIoffset\fP
The RA and DEC offsets (arcseconds) of the point source from the
observing center. A point source to the north and east has positive
offsets. Defaults are zero.
.TP
\fIline\fP
The visibility linetype to use, in the standard form, viz:
.nf
  type,nchan,start,width,step
.fi
Generally if there is an input model, this defaults to the linetype
parameters used to construct the map. For a point source or planet
model, the default is all channels. If you wish to override these
defaults, or if the info is not present in the header, this parameter
can be useful.
.TP
\fIsigma\fP
For options=flag, UVMODEL flags those points in the output that
differ by more than "sigma" sigmas. The default is 100.
.TP
\fIout\fP
Output visibility data file name. No default. The output file will
contain only as many channels as there are planes in the model
cube. The various uv variables which describe the windows are
adjusted accordingly.
