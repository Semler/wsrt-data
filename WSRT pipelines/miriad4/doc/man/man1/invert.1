.TH invert 1
.SH NAME
invert - Transform multi-pointing visibility data into a map
.SH PERSON RESPONSIBLE
rjs
.SH CATEGORIES
map making
.SH DESCRIPTION
INVERT is a MIRIAD task which forms images from visibilities.
INVERT can form continuum images or spectral line cubes. It
can generate images/cubes for several polarisations, as well
as handling multi-frequency synthesis and mosaicing observations.
INVERT can also form complex-valued images from non-Hermitian data
(e.g. holography data). Appropriate point-spread functions can also
be generated.
.SH PARAMETERS
.TP
\fIvis\fP
Input visibility data files. Several files can be given. No default.
.TP
\fImap\fP
Output map (image) file name. Each output file consists of a single
polarization/Stokes parameter. If several different pols/Stokes
images are being made, then several file names should be given. No
default.
.TP
\fIbeam\fP
Output beam (point-spread function) file name. The default is not
to make a beam.
.TP
\fIimsize\fP
The size of the output dataset. The default is to image out to
primary beam half power points. For options=mosaic, an image of
this size is made for each pointing before a linear mosaic
operation is performed. 
.TP
\fIcell\fP
Image cell size, in arcsec. If two values are given, they give
the RA and DEC cell sizes. If only one value is given, the cells
are made square. The default is about one third of the resolution
of the resultant images.
.TP
\fIoffset\fP
When not mosaicing, this gives the sky position to shift to the
center of the output images. The position is specified as an
offset (in arcsec) from the observing center. The default is to
perform no shifting.
.PP
When mosaicing, this gives the sky coordinate (RA and DEC) of the
reference pixel in the imaging process. The value can be given in the
form hh:mm:ss,dd:mm:ss, or as decimal hours and degrees. INVERT
applies appropriate shifts to make this location fall on a pixel.
The default is a central observing center.
.TP
\fIfwhm\fP
This determines a gaussian taper to apply to the visibility data.
It specifies the FWHM of an image-domain gaussian -- tapering the
visibility data is equivalent to convolving with this image-domain
gaussian.
.PP
Either one or two values can be given, in arcsec, being the FWHM in
the RA and DEC directions. If only one value is given, the taper is
assumed to be symmetric. The default is no taper.
.PP
The signal-to-noise ratio will be optimised in the output image if
this parameter is set to the FWHM of typical image features of
interest.
.PP
If you are more accustomed to giving this parameter in the uv plane
(as AIPS requires), then:
.nf
  fwhm(image plane) = 182 / fwhm(uv plane)
.fi
where the image plane fwhm is measured in arcseconds, and the uv plane
fwhm is measured in kilowavelengths.
.TP
\fIsup\fP
Sidelobe suppression area, given in arcseconds. This parameter
gives the area around a source where INVERT attempts to suppress
sidelobes. Two values (for the RA and DEC directions respectively)
can be given. If only one value is given, the suppression area is
made square. The default is to suppress sidelobes in an area as
large as the field being mapped.
.PP
The suppression area is essentially an alternate way of specifying
the weighting scheme being used. Suppressing sidelobes in the entire
field corresponds to uniform weighting (so the default corresponds to
uniform weighting). Natural weighting gives the best signal to noise
ratio, at the expense of no sidelobe suppression. Natural weighting
corresponds to SUP=0. Values between these extremes give a tradeoff
between signal to noise and sidelobe suppression, and roughly
correspond to AIPS ``super-uniform'' weighting.
.TP
\fIrobust\fP
Brigg's visibility weighting robustness parameter. This parameter
can be used to down-weight excessive weight being given to
visibilities in relatively sparsely filled regions of the $u-v$ plane.
Most useful settings are in the range [-2,2], with values less than
-2 corresponding to very little down-weighting, and values greater than
+2 reducing the weighting to natural weighting. 
.PP
Sidelobe levels and beam-shape degrade with increasing values of
robustness, but the theoretical noise level will also decrease.
.PP
The default is no down-weighting (robust=-infinity).
.TP
\fIline\fP
Standard "line" parameter, with the normal defaults. In particular,
the default is to image all channels. See the help on "line" for 
more information.
The "line" parameter consists of a string followed by up to 
four numbers, viz:
.PP
.nf
  linetype,nchan,start,width,step
.fi
.PP
where ``linetype'' is one of "channel", "wide", "velocity" or
"felocity".
.TP
\fIref\fP
Line type of the reference channel, specified in a similar to the
"line" parameter. Specifically, it is in the form:
.nf
  linetype,start,width
.fi
Before mapping, the visibility data are divided by the reference
channel. The default is no reference channel.
.TP
\fIselect\fP
This allows a subset of the uv data to be used in the mapping
process. See the Users Manual for information on how to specify
this parameter. The default is to use all data.
.TP
\fIstokes\fP
Standard polarisation/Stokes parameter selection. See the help
on "stokes" for more information. Several polarisations can be
given. The default is ``ii'' (i.e. Stokes-I, given the
assumption that the source is unpolarised).
.TP
\fIoptions\fP
This gives extra processing options. Several options can be
given (abbreviated to uniqueness), and separated by commas:
.nf
  nocal    Do not apply gains table calibration to the data.
  nopol    Do not apply polarisation leakage corrections.
  nopass   Do not apply bandpass table calibration to the data.
  double   Normally INVERT makes the beam patterns the same
           size as the output image. This option causes the
           beam patterns to be twice as large.
  systemp  Weight each visibility in inverse proportion to the
           noise variance. Normally visibilities are weighted in
           proportion to integration time. Weighting based on the
           noise variance optimises the signal-to-noise ratio
           (provided the measures of the system temperature are
           reliable!).
  mfs      Perform multi-frequency synthesis. The causes all the
           channel data to be used in forming a single map. The
           frequency dependence of the uv coordinate is thus used to
           give better uv coverage and/or avoid frequency
           smearing. For this option to produce useful maps, the
           intensity change over the frequency band must be small.
           You should set the ``line'' parameter to select the
           channels that you wish to grid.
  sdb      Generate the spectral dirty beam as well as the normal
           beam, when MFS processing. The default is to only create
           the normal beam. If the spectral dirty beam is created,
           this is saved as an extra plane in the beam dataset.
  mosaic   Process multiple pointings, and generate a linear
           mosaic of these pointings.
  imaginary Make imaginary image for non-Hermitian data (holography).
  amplitude Produce a image using the data amplitudes only. The
            phases of the data are set to zero.
  phase     Produce an image using the data phase only. The amplitudes
            of the data are set to 1.
.TP
\fImode\fP
.fi
This determines the algorithm to be used in imaging.
Possible values are:
.nf
  fft    The conventional grid-and-FFT approach. This is the default
         and by far the fastest.
  dft    Use a discrete Fourier transform. This avoids aliasing
         but at a hugh time penalty.
  median This uses a median approach. This is generally robust to 
         bad data and sidelobes, has a even larger time penalty
         and produces images that cannot be deconvolved.
.fi
NOTE: Dft and median modes are not supported with options=mosaic.
.TP
\fIslop\fP
NOTE: This parameter should be used with caution! See the Users
Guide for more information on its applicability.
.PP
When forming spectral cubes, INVERT normally insists
that all channels in a given visibility spectrum must be good before
accepting the spectrum for imaging. This keyword allows this rule to
be relaxed. It consists of two parts: a tolerance and a method for
replacing the bad channels.
.PP
The tolerance is a value between 0 and 1, giving the fraction of
channels that INVERT will tolerate as being bad before the spectrum
is totally discarded. The default is 0, indicating that INVERT will
not tolerate any bad channels. A value of 1 indicates that INVERT
will accept a spectrum as long as there is at least one good channel.
.PP
The replacement method is either the value `zero' or `interpolate',
indicating that the bad channels are either to be replaced with
0, or to be estimated by linear interpolation of two adjacent good
channels. See the Users Guide for the merits and evils of the two
approaches. The default is `zero'.
