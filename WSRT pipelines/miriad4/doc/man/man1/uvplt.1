.TH uvplt 1
.SH NAME
uvplt - Make plots from a UV-data base on a PGPLOT device.
.SH PERSON RESPONSIBLE
nebk
.SH CATEGORIES
uv analysis, plotting
.SH DESCRIPTION
UVPLT - Plot a variety of quantities from visibility data bases.
.nf
     Options are available to time average data (plot with optional
     error bars) and to plot different baselines on separate 
     sub-plots on each page plus many others.
.SH PARAMETERS
.TP
\fIvis\fP
.fi
The input visibility file(s). Multiple input files and wild card
card expansion are supported.    
No default
.TP
\fIline\fP
This is the normal linetype specification. See the help on "line"
for more information. The default is all channels.
.TP
\fIselect\fP
This selects which visibilities to be used. Default is all
visibilities. See the Users Guide for information about how
to specify uv data selection.
Default is all data
.TP
\fIstokes\fP
Select Stokes parameter(s) or polarization(s) from:
.nf
  xx, yy, xy, yx,  i, q, u, v, 
  rr, ll, rl, lr
.fi
Default is all polarizations or Stokes parameters present
.TP
\fIaxis\fP
Two values (minimum match active), one for each of the x 
and y axes chosen from:
.nf
  time                     [time in DD HH MM SS.S format]
  dtime                    [time in decimal days format]
  amplitude, real, imag    [natural units; Jy]
  phase                    [degrees]
  uu, vv                   [u & v in klambda]
  uc, vc                   [u,& v, -u & -v in klambda]
  uvdistance               [sqrt(u**2+v**2)]
  uvangle                  [uv pos'n angle clockwise from v axis]
  hangle                   [hour angle in HH MM SS.S]
  dhangle                  [hour angle in decimal hours]
  parang                   [parallactic angle in degrees]
.fi
.PP
Defaults are axis=time,amp  (x and y axes).
.TP
\fIxrange\fP
Plot range in the x-direction 
.nf
  If axis = uu, vv, or uvdistance [kilo-lambda;   2 values]
          unless OPTIONS=NANOSEC;     then   nanoseconds]
  If axis = uvangle               [degrees;       2 values]
  If axis = time                  [dd,hh,mm,ss.s; 8 values]
  If axis = dtime                 [decimal days;  2 values]
  If axis = amplitude, real, imag [natural units; 2 values]
  If axis = phase                 [degrees;       2 values]
  If axis = hangle                [hh,mm,ss.s;    6 values]
  If axis = dhangle               [decimal hours; 2 values]
  If axis = parang                [degrees;       2 values]
.fi
.PP
Default is to self-scale (see also OPTIONS=XIND).
.TP
\fIyrange\fP
Plot range in the y-direction as for the x axis.  The 
default is to self-scale (see also OPTIONS=YIND).
.TP
\fIaverage\fP
The averaging time in minutes (unless OPTIONS=DAYS,HOURS,SECONDS).
Averaging is reset at frequency, source, or pointing centre
changes.  Individual baselines and polarizations are averaged
separately (unless OPTIONS=AVALL).  If you have selected multiple
channels and you also ask for time averaging, then all the
selected channels are averaged together in the time interval.
If you wish to use OPTIONS=AVALL to average everything on
the one subplot (e.g. polarizations) but don't want temporal
averaging, set AVERAGE to less than one integration.
Default is no averaging.
.TP
\fIhann\fP
Hanning smoothing length (an odd integer < 15).   Is applied
after any time averaging and INC selection. Useful for amplitude
or phase, say, plotted against time.  Error bars remain unaffected
by Hanning smoothing.  Currently, the Hanning smoothing is unaware 
of source or frequency changes. Use SELECT if you have boundary 
problems. 
Default is no smoothing (hann = 1).
.TP
\fIinc\fP
Plot every INCth point (on each sub-plot) that would normally 
have been selected.   Useful if you don't want to average, but 
want to cut down on the number of plotted points.  Beware of 
increments that divide exactly into the number of baselines 
in time ordered data.  
Default is 1.
.TP
\fIoptions\fP
Task enrichment options. Minimum match is effective. 
.nf
 nocal   Do not apply the gain corrections
 nopol   Do not apply the polarization leakage corrections
 nopass  Do not apply the bandpass corrections
.fi
.PP
.nf
 nofqav  By default, uvplt averages together all channels from
         a visibility record before plotting. The nofqav option
         disables this, and causes individual channels to be
         plotted.
.fi
.PP
.nf
 nobase  Plot all baselines on the same plot, otherwise
         each baseline is plotted on a separate sub-plot.
 
 2pass   Normally uvplt makes assumptions about what it is
         expecting to find in the data with regards polarizations
         and baselines.   Under some conditions, uvplt may
         report that it has not allocated sufficient buffer
         space.  This option instructs uvplt to make two passes
         through the data, the first to accumulate precise
         information on the contents of the selected data so
         that buffer space is optimally allocated.
.fi
.PP
.nf
 scalar  Do scalar (average amplitudes or phases) rather than
         vector (average real and imaginary) averaging.
         This is useful if the visibilities are uncalibrated &
         the phase is winding over the averaging interval & you
         would like an averaged amplitude. Scalar averaged phase
         is not very meaningful in general.
 avall   If you are averaging in time, then average all data
         selected on each sub-plot together.  E.g. all selected
         polarizations, and, if OPTIONS=NOBASE, all baselines
         as well.  If you wish to average all the things on
         one subplot together but without temporal averaging,
         just set the averaging time to less than one integration.
 unwrap  When plotting phase, try to unwrap it so that
         say, if one point is 179 deg and the next -179,
         they will be plotted as 179 and 181 deg.  NOTE:
         Unwrapping noise can be VERY misleading.
.fi
.PP
.nf
 rms     Draw error bars (+/- 1 standard deviation) on the plot if 
         averaging is invoked. 
 mrms    Draw error bars (+/- 1 standard deviation in the mean)
         on the plot if averaging is invoked. 
 noerr   The automatically worked out min and max plot limits
         will NOT include the ends of the error bars.
.fi
.PP
.nf
 all     Plot flagged and unflagged visibilties
 flagged Plot only flagged visibilities
         The default is to plot only unflagged (good) visibilities
         ALL overrides  FLAGGED
.fi
.PP
.nf
 nanosec u and v are plotted in nano-seconds rather than k-lambda
 days    The averaging interval is in days rather than minutes
 hours   The averaging interval is in hours rather than minutes
 seconds The averaging time is in seconds rather than minutes
.fi
.PP
.nf
 xind    If the x-axis is self-scaled, then unless OPTIONS=NOBASE,
         setting XIND will cause each sub-plot to have the x-axis 
         self-scaled independently.  The default is that the x-range
         used is that which encompasses the ranges from all sub-plots.
 yind    The equivalent for the y-axis
.fi
.PP
.nf
 equal   Plot x and y with equal scales.  Useful only for plots
         like AXIS=UU,VV.  Does not mean the plot will necessarily
         be square
 zero    Plot the x=0 and y=0 lines
.fi
.PP
.nf
 symbols Each file is plotted with a different plot symbol
 nocolour
         Each file is plotted with the same colour (white). By
         default and when there is only one polarization, each 
         file has a separate colour.
 dots    If time averaging is invoked, plot the data with dots
         rather than filled in circles.  These plot much faster
         on hardcopy devices.
.fi
.PP
.nf
 source  Put the source name rather than the file name in the
         plot title
 inter   After the plot is drawn, you get a chance to redraw
         the plot with a different x- and y-range, and also
         on a different device.  In this way you can make a
         hard-copy without re-running the program. In this case, 
         points outside of the user specified x,y-range are
         ARE included in the plot buffer, so that if you redefine
         the ranges, those points are available for plotting.
.fi
.PP
.nf
 log     Write the values and errors (if averaging) that are 
         plotted into the log file.  No attempt to separate 
         baselines is made, except that automatically obtained 
         by not setting OPTIONS=NOBASE
.TP
\fIdevice\fP
.fi
PGPLOT plot device/type. No default.
.TP
\fInxy\fP
Number of plots in the x and y directions for when plotting
each baseline separately. Defaults try to choose something
sensible.
.TP
\fIsize\fP
PGPLOT character sizes, in units of the default size (i.e., 1)
First value is for the labels, the second is for the symbol size
Defaults depend upon the number of sub-plots. The second value 
defaults to the first.
.TP
\fIlog\fP
The output logfile name. The default is the terminal.
.TP
\fIcomment\fP
A one line comment which is written into the logfile.
