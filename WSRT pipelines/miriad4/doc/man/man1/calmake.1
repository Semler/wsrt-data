.TH calmake 1
.SH NAME
calmake - Create gcal file from observation of calibrator(s)
.SH PERSON RESPONSIBLE
pjt
.SH CATEGORIES
calibration
.SH DESCRIPTION
CALMAKE is a MIRIAD task which creates a calibration dataset.
it is needed because the calibration programs do not work
directly on standard visibility files.
.PP
CALMAKE reads in dataset(s) of amplitude/phase calibration source(s),
and creates a calibration dataset, called a ``gcal'' file. 
The data within the gcal file is taken from either the wide 
band correlation channels or some averaged value from the narrow 
band channels.
.SH PARAMETERS
.TP
\fIvis\fP
Input visibility dataset(s) to be used for calibration. 
No default.
.TP
\fIgcal\fP
The output gain calibration dataset.  
No default.
.TP
\fIline\fP
The ``line'' parameter determines the channels that are to be processed
from a uv data-set. The parameter value consists of a string
followed by up to four numbers. Defaults are used for any missing
trailing portion of the parameter value.
.PP
A uv data-set can have correlations from either (or both) a spectral
or a wideband (continuum) correlator. Both the spectral and wideband
correlators produce multiple correlations (channels). The string
part of the line parameter is used to select the spectral or wideband
channels. It may be one of:
.nf
  "channel"   Spectral channels.
  "wide"      Wideband (continuum) channels.
  "velocity"  Spectral channels, resampled at equal increments in
              velocity (using the radio definition). The resampling
              involves a weighted average of the spectral channels.
              This is useful if the source was not Doppler tracked
              correctly.
  "felocity"  Similar to "velocity", except that the parameters are
              given using the optical velocity definition.
.fi
Generally the default is "channel" if the data-set has spectral
channel data, and "wide" otherwise.
.PP
The four numbers that accompany the string give:
.PP
.nf
  nchan, start, width, step
.fi
.PP
These four numbers specify which channels to select from the input
dataset and how to combine them to produce the output channels that
the Miriad task will work on.
.PP
nchan   is the number of output channels produced. Generally it
.nf
        defaults to the number of input channels.
.fi
.PP
start   is the first channel from input dataset that is to be used.
.nf
        It defaults to 1 (i.e. first channel).
.fi
.PP
width   gives the number of input channels to average together to
.nf
        produce a single output channel. It defaults to 1.
.fi
.PP
step    gives the increment between selected input channels. It
.nf
        defaults to the value of "width".
.fi
.PP
For "velocity" linetype, the start, width and step parameters are
given in km/s. The output channels are centered on velocities:
start, start+step, start+2*step, etc.
.PP
The `line' parameter interacts with the "select=window" selection
for "channel" and "velocity"/"felocity" linetypes. See the help on
select for more information.
.PP
For example:
.PP
.nf
  line=channel,10
.fi
.PP
selects 10 output channels, being input spectral channels 1 to 10.
Similarly
.PP
.nf
  line=channel,10,8,1,2
.fi
.PP
selects 10 output channels, starting at input channel 8, and skipping
every second input channel, whereas
.PP
.nf
  line=channel,10,8,2,2
.fi
.PP
selects 10 output channels, again starting at input channel 8, but
each of the output channels consists of the average of two of the
input channels. Finally
.PP
.nf
  line=velocity,10,-10,1,1
.fi
.PP
resamples the spectral data in velocity, to give 10 channels of width
1 km/s. The channels are centered at -10.0,-9.0,-8.0, etc, km/s.
Default: ``wide,2,1,1,1'', i.e. the digital wideband data.
Note that the number of channels MUST be 2.
.TP
\fIselect\fP
This keyword selects the subset of the visibility data to be
processed. There are a number of subcommands, which may be
abbreviated. Each may be prefixed with a plus or minus sign to
indicate using or discarding the selected data. Many subcommands
can be given (separate them by commas). Subcommands include:
.PP
.nf
  time(t1,t2)
.fi
Select data between times t1 and t2 (UT). Times are in the format:
.nf
  yymmmdd.fff
.fi
or
.nf
  yymmmdd:hh:mm:ss.s
.fi
Various abbreviations are possible. If no date part is given, then
the time matchs all data (regardless of its date) for the given
time of day.
.PP
.nf
  antennae(a1,a2,...)(b1,b2...)
.fi
Select all baselines pairs formed between first and second list of
antennas. The second list is optional and defaults to all antennas.
.PP
.nf
  uvrange(uvmin,uvmax)
.fi
Select visibilities with uv radius between uvmin and uvmax (in kilo
wavelenghts). If only one value is given, uvmin is taken as zero.
.PP
.nf
  uvnrange(uvmin,uvmax)
.fi
Select visibilities with uv radius between uvmin and uvmax (in
nanoseconds). If only one value is given, uvmin is taken as zero. 
.PP
.nf
  visibility(n1,n2)
.fi
Select visibilities numbered n1 to n2 inclusive.
.PP
.nf
  increment(inc)
.fi
Select every inc'th visibility.
.PP
.nf
  ra(r1,r2)
.fi
Select visibilities whose RA is in the range r1 to r2. RAs are given
in the format
.nf
  hh:mm:ss
.fi
or
.nf
  hh.hhh
.fi
Various abbreviations are possible.
.PP
.nf
  dec(d1,d2)
.fi
Select visibilites whose DEC is in the range d1 to d2. Declinations
are given in the format
.nf
  dd:mm:ss
.fi
or
.nf
  dd.ddd
.fi
Various abbreviations are possible.
.PP
.nf
  dra(p1,p2)
.fi
Select visibilities for which the RA of the pointing center is offset
from the main pointing center by between p1 and p2 arcseconds.
.PP
.nf
  ddec(p1,p2)
.fi
Select visibilities for which the DEC of the pointing center is offset
from the main pointing center by between p1 and p2 arcseconds.
.PP
.nf
  pointing(p1,p2)
.fi
Select visibilities with the rms pointing error in the range p1 to p2
arcseconds. If only one number is given, p1 is taken as 0.
.PP
.nf
  polarization(p1,p2,p3,...)
.fi
Select visibilities on the basis of their polarization/Stokes parameter.
p1,p2,p3,... can be selected from "i,q,u,v,xx,yy,xy,yx,rr,ll,rl,lr"
Conversion between polarizations Stokes parameters (e.g. convert
linears to Stokes)  is not performed by this mechanism (see keyword stokes).
.PP
.nf
  source(SRCNAM1,SRCNAM2,...)
.fi
Select correlations from the particular source. Several sources
can be given. An asterisk in the source name is treated as a
wildcard. Note that the sourcename MUST BE IN CAPITAL CASE. 
.PP
.nf
  frequency(f1,f2)
.fi
Select correlations, where the sky frequency of the first correlation
is in the range f1 to f2 (in GHz). If only a single frequency is
given, then correlations are selected if the first correlation
is within 1% of the given frequency.  Note this selects the whole
record.  This may include several spectral-windows whose frequency
is quite different from the first correlation in the first window.
.PP
.nf
  amplitude(amplo,amphi)
.fi
Select any correlation, where the amplitude is between "amplo" and
"amphi". If only one value is given, then "amphi" is assumed to be
infinite.
.PP
.nf
  shadow(d)
.fi
Selects data that would be shadowed by an antenna of diameter "d" meters. 
If "d" is zero, then the actual diameter of the antennas (if known) is used. 
If some data is shadowed, it is advisable to use an antenna diameter value 
greater than the physical antenna size (e.g. 20% larger).
.PP
.nf
  bin(b1,b2)
.fi
Select visibilities which have pulsar bin numbers in the range
b1 to b2 inclusive. If b2 is omitted, just pulsar bin b1 is
selected.
.PP
.nf
  on
.fi
This is used with single dish observations, anbd selects based
whether the "on" variable in the data is on!
.PP
.nf
  auto
.fi
This is used with files that contain a mix of autocorrelations and
crosscorrelations. This selects just the autocorrelation data.
.PP
.nf
  window(w1,w2,...)
.fi
Select by spectral window (IF band in AIPS terminology). See
the help on window for more information.
.PP
.nf
  or
.fi
The list of subcommands before and after the 'or' are "ored"
together.
Default: all data from the input datasets.
.TP
\fIauto\fP
A logical, signalling if breakpoints are to be set automatically 
when the source name changes.  
Data before and after a breakpoint are then fit independently by 
separate polynomials in subsequent fitting programs.
Also, data observed between the calibrators on either side of the 
breakpoint will be flagged bad in calapply if flagbad=true,true. 
Default: False (does not set breakpoints automatically).
.TP
\fIfluxes\fP
List of sources and associated fluxes in Jy to be used for
calibration. The format is name of the source, followed by
its flux, and optionally more sources, e.g.
fluxes=3c273,10.3,3c84,12.3,bllac,2.2
Currently no frequency dependency can be specified.
.TP
\fIfluxtab\fP
Name of the calibrators flux table file. If no name provided, 
the system default (MIRCAT/cals.fluxes) is taken. The user
can also supply fluxes by hand (see keyword fluxes= above) in
which case the flux table derived values are ignored.
Note that reported frequencies of 0.0 means the flux was
determined from interpolation accross different frequencies.
