.TH uvflag 1
.SH NAME
uvflag - Flags or unflags uv data
.SH PERSON RESPONSIBLE
bpw
.SH CATEGORIES
calibration, uv analysis
.SH DESCRIPTION
UVFLAG is used to change flags corresponding to visibility data.
Using the keywords select, line and edge one selects a portion of a
uv-data file. For all selected correlations the flags are then set to
the value specified with the keyword flagval. If flagval equals
'flag', the uv data are flagged as bad; for flagval equal to 'unflag',
the uv data are flagged as good.
The user can control the amount of output produced using the options
keyword. Only records that fulfill all selection criteria as
specified by select, line and edge are shown and counted, not the
complete datafile.
Because the physical writing of the flags is done using a buffering
approach, the flags are only actually changed in the datafile if
one lets uvflag finish normally. I.e., if the 'q' option is used to
stop printing, the flags may or may not have been changed.
UVFLAG can also be used to inspect the uv-data. If option 'noapply'
is used, everything works as it would do normally, except that the
flags are not actually changed in the datafile. This is particularly
useful in combination with option 'full'.
.SH PARAMETERS
.TP
\fIvis\fP
This is the normal keyword for an input visibility dataset. There
is generally no default input name. Some tasks allow multiple input
datasets (with wildcards supported, such as an asterisk). Other tasks
can handle only a single dataset at a time.
Multiple input datasets are allowed.
.TP
\fIselect\fP
This keyword selects the subset of the visibility data to be
processed. There are a number of subcommands, which may be
abbreviated. Each may be prefixed with a plus or minus sign to
indicate using or discarding the selected data. Many subcommands
can be given (separate them by commas). Subcommands include:
.PP
.nf
  time(t1,t2)
.fi
Select data between times t1 and t2 (UT). Times are in the format:
.nf
  yymmmdd.fff
.fi
or
.nf
  yymmmdd:hh:mm:ss.s
.fi
Various abbreviations are possible. If no date part is given, then
the time matchs all data (regardless of its date) for the given
time of day.
.PP
.nf
  antennae(a1,a2,...)(b1,b2...)
.fi
Select all baselines pairs formed between first and second list of
antennas. The second list is optional and defaults to all antennas.
.PP
.nf
  uvrange(uvmin,uvmax)
.fi
Select visibilities with uv radius between uvmin and uvmax (in kilo
wavelenghts). If only one value is given, uvmin is taken as zero.
.PP
.nf
  uvnrange(uvmin,uvmax)
.fi
Select visibilities with uv radius between uvmin and uvmax (in
nanoseconds). If only one value is given, uvmin is taken as zero. 
.PP
.nf
  visibility(n1,n2)
.fi
Select visibilities numbered n1 to n2 inclusive.
.PP
.nf
  increment(inc)
.fi
Select every inc'th visibility.
.PP
.nf
  ra(r1,r2)
.fi
Select visibilities whose RA is in the range r1 to r2. RAs are given
in the format
.nf
  hh:mm:ss
.fi
or
.nf
  hh.hhh
.fi
Various abbreviations are possible.
.PP
.nf
  dec(d1,d2)
.fi
Select visibilites whose DEC is in the range d1 to d2. Declinations
are given in the format
.nf
  dd:mm:ss
.fi
or
.nf
  dd.ddd
.fi
Various abbreviations are possible.
.PP
.nf
  dra(p1,p2)
.fi
Select visibilities for which the RA of the pointing center is offset
from the main pointing center by between p1 and p2 arcseconds.
.PP
.nf
  ddec(p1,p2)
.fi
Select visibilities for which the DEC of the pointing center is offset
from the main pointing center by between p1 and p2 arcseconds.
.PP
.nf
  pointing(p1,p2)
.fi
Select visibilities with the rms pointing error in the range p1 to p2
arcseconds. If only one number is given, p1 is taken as 0.
.PP
.nf
  polarization(p1,p2,p3,...)
.fi
Select visibilities on the basis of their polarization/Stokes parameter.
p1,p2,p3,... can be selected from "i,q,u,v,xx,yy,xy,yx,rr,ll,rl,lr"
Conversion between polarizations Stokes parameters (e.g. convert
linears to Stokes)  is not performed by this mechanism (see keyword stokes).
.PP
.nf
  source(SRCNAM1,SRCNAM2,...)
.fi
Select correlations from the particular source. Several sources
can be given. An asterisk in the source name is treated as a
wildcard. Note that the sourcename MUST BE IN CAPITAL CASE. 
.PP
.nf
  frequency(f1,f2)
.fi
Select correlations, where the sky frequency of the first correlation
is in the range f1 to f2 (in GHz). If only a single frequency is
given, then correlations are selected if the first correlation
is within 1% of the given frequency.  Note this selects the whole
record.  This may include several spectral-windows whose frequency
is quite different from the first correlation in the first window.
.PP
.nf
  amplitude(amplo,amphi)
.fi
Select any correlation, where the amplitude is between "amplo" and
"amphi". If only one value is given, then "amphi" is assumed to be
infinite.
.PP
.nf
  shadow(d)
.fi
Selects data that would be shadowed by an antenna of diameter "d" meters. 
If "d" is zero, then the actual diameter of the antennas (if known) is used. 
If some data is shadowed, it is advisable to use an antenna diameter value 
greater than the physical antenna size (e.g. 20% larger).
.PP
.nf
  bin(b1,b2)
.fi
Select visibilities which have pulsar bin numbers in the range
b1 to b2 inclusive. If b2 is omitted, just pulsar bin b1 is
selected.
.PP
.nf
  on
.fi
This is used with single dish observations, anbd selects based
whether the "on" variable in the data is on!
.PP
.nf
  auto
.fi
This is used with files that contain a mix of autocorrelations and
crosscorrelations. This selects just the autocorrelation data.
.PP
.nf
  window(w1,w2,...)
.fi
Select by spectral window (IF band in AIPS terminology). See
the help on window for more information.
.PP
.nf
  or
.fi
The list of subcommands before and after the 'or' are "ored"
together.
.TP
\fIline\fP
The ``line'' parameter determines the channels that are to be processed
from a uv data-set. The parameter value consists of a string
followed by up to four numbers. Defaults are used for any missing
trailing portion of the parameter value.
.PP
A uv data-set can have correlations from either (or both) a spectral
or a wideband (continuum) correlator. Both the spectral and wideband
correlators produce multiple correlations (channels). The string
part of the line parameter is used to select the spectral or wideband
channels. It may be one of:
.nf
  "channel"   Spectral channels.
  "wide"      Wideband (continuum) channels.
  "velocity"  Spectral channels, resampled at equal increments in
              velocity (using the radio definition). The resampling
              involves a weighted average of the spectral channels.
              This is useful if the source was not Doppler tracked
              correctly.
  "felocity"  Similar to "velocity", except that the parameters are
              given using the optical velocity definition.
.fi
Generally the default is "channel" if the data-set has spectral
channel data, and "wide" otherwise.
.PP
The four numbers that accompany the string give:
.PP
.nf
  nchan, start, width, step
.fi
.PP
These four numbers specify which channels to select from the input
dataset and how to combine them to produce the output channels that
the Miriad task will work on.
.PP
nchan   is the number of output channels produced. Generally it
.nf
        defaults to the number of input channels.
.fi
.PP
start   is the first channel from input dataset that is to be used.
.nf
        It defaults to 1 (i.e. first channel).
.fi
.PP
width   gives the number of input channels to average together to
.nf
        produce a single output channel. It defaults to 1.
.fi
.PP
step    gives the increment between selected input channels. It
.nf
        defaults to the value of "width".
.fi
.PP
For "velocity" linetype, the start, width and step parameters are
given in km/s. The output channels are centered on velocities:
start, start+step, start+2*step, etc.
.PP
The `line' parameter interacts with the "select=window" selection
for "channel" and "velocity"/"felocity" linetypes. See the help on
select for more information.
.PP
For example:
.PP
.nf
  line=channel,10
.fi
.PP
selects 10 output channels, being input spectral channels 1 to 10.
Similarly
.PP
.nf
  line=channel,10,8,1,2
.fi
.PP
selects 10 output channels, starting at input channel 8, and skipping
every second input channel, whereas
.PP
.nf
  line=channel,10,8,2,2
.fi
.PP
selects 10 output channels, again starting at input channel 8, but
each of the output channels consists of the average of two of the
input channels. Finally
.PP
.nf
  line=velocity,10,-10,1,1
.fi
.PP
resamples the spectral data in velocity, to give 10 channels of width
1 km/s. The channels are centered at -10.0,-9.0,-8.0, etc, km/s.
Uvflag supports only types 'channel and wide'. If line is
unspecified, both the 'channel data' and the 'wideband data'
(if present) will be flagged. Also, since averaging of data
is an undefined operation when setting flags, width is
forced to be equal to 1.
.TP
\fIedge\fP
This keyword allows uvflag to work on the edges of spectral
windows. Three numbers may be given, n, m and c. Flags will
be changed only for the first n and last m channels of each
spectral window and for the central c channels.
If one value is given, the number of selected channels at
the start and end of the window is assumed to be equal.
If two values are given the first gives the number of
selected channels at the start, the second the number at
the end. In this case either one may also be 0.
The third value gives the number of channels to delete from
the center of the band. The default is 0.  The center of a
window is defined as 'startchannel+nchannels/2'.
A negative value is interpreted as meaning that the beginning,
ending, or middle 1/8th of the band must be deleted.
Use of the edge keyword forces the step parameter of the
line keyword to be equal to 1.
Edge cannot be combined with linetype 'wide'. If edge is
used, the linetype defaults to channel,0,1,1,1, i.e. all
channels.
This also works for multiple input visibility files, even
if these have different correlator modes.
[0,0,0]
.TP
\fIflagval\fP
Either 'flag' or 'unflag', which tells whether the flags for
the correlations selected by 'select', 'line' and 'edge'
have to be set or unset. May be abbreviated to 'f' or 'u'.
Exactly one of the options 'flag' or 'unflag' must be
present.
.TP
\fIoptions\fP
One or more of
.nf
  'noapply',
  'none', 'brief', 'indicative', 'full', 'noquery',
  'hms', 'decimal'.
.fi
.PP
These options can be abbreviated to uniqueness. The default
is 'brief,hms', except when a logfile is given (see keyword
log), then it becomes 'indicative,hms'.
.PP
'noapply' will go through the whole process without
actually changing the flags. Useful for checking what will
happen or for inspecting the flags.
No history comments are written.
.PP
'none', 'brief', 'indicative' and 'full' control the
amount of information returned to the user.
.nf
  - 'brief' gives an overview when UVFLAG finishes.
  - 'indicative' lists the number of good, bad and changed
     flags for each selected uv-record.
  - 'full' lists the data, the old flag and the new flag for
     each channel separately and also an overview of each record.
  - If more than 1 verbosity level is given, the lowest is taken.
  - Option 'noquery' will turn off the feature that printing
    is halted every 22 lines.
.fi
.PP
For verbosity levels 'indicative' and 'full' the format
of the time that is written is determined by 'hms' (hours,
minutes and seconds) or 'decimal' (decimal parts of a day).
.TP
\fIlog\fP
Name of a file where reported information is written.
If empty this implies the terminal screen, else the
named file. Giving a filename also sets option 'indicative'.
[terminal]
