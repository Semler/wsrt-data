.TH selfcal 1
.SH NAME
selfcal - Determine self-calibration of calibration gains.
.SH PERSON RESPONSIBLE
mchw
.SH CATEGORIES
calibration, map making
.SH DESCRIPTION
SELFCAL is a MIRIAD task to perform self-calibration of visibility data.
Either phase only or amplitude and phase calibration can be performed.
The input to SELCAL are a visibility data file, and model images.
This program then calculates the visibilities corresponding to the
model, accumulates the statistics needed to determine the antennae
solutions, and then calculates the self-cal solutions.
.PP
The output is a calibration file, ready to be applied to the
visibility data.
.SH PARAMETERS
.TP
\fIvis\fP
Name of input visibility data file. No default.
.TP
\fIselect\fP
Standard uv data selection criteria.
.TP
\fImodel\fP
Name of the input models. Several models can be given, which can
cover different channel ranges, different pointing and sources, and
different polarizations of the input visibility data. Generally
the model should be derived (by mapping and deconvolution) from the
input visibility file, so that the channels in the model correspond
to channels in the visibility file. Though the maps can be made using
any linetype, generally "channel" linetype will give best results (??).
The units of the model MUST be JY/PIXEL, rather than JY/BEAM, and
should be weighted by the primary beam. The task DEMOS can be used
to extract primary beam weighted models from a mosaiced image. If
no models are given, a point source model is assumed.
.PP
NOTE: When you give SELFCAL a model, it will, by default, select the
data associated with this model from the visibility data-set. This
includes selecting the appropriate range of channels, the appropriate
polarisation type and the appropriate pointing (if options=mosaic
is used). If you use a point source model, if it YOUR responsibility
to select the appropriate data. In particular, you may well want
to select appropriate polarisations, channels and sources.
.TP
\fIclip\fP
Clip level. For models of intensity, any pixels below the clip level
are set to zero. For models of Stokes Q,U,V, or MFS I*alpha models,
any pixels whose absolute value is below the clip level are set
to zero. Default is 0.
.TP
\fIinterval\fP
The length of time, in minutes, of a gain solution. Default is 5,
but use a larger value in cases of poor signal to noise, or
if the atmosphere and instrument is fairly stable.
.TP
\fIoptions\fP
This gives several processing options. Possible values are:
.nf
  amplitude  Perform amplitude and phase self-cal.
  phase      Perform phase only self-cal.
  smooth     Determine the solutions in such a way that they are
             smooth with time.
  polarized  The source is polarised. By default the source is
             assumed to be unpolarised. For a polarized source,
             SELFCAL cannot perform polarization conversion. That
             is, if the model is of a particular polarization, then
             the visibility file should contain that sort of
             polarization. For example, if the model is Stokes-Q,
             then the visibility file should contain Stokes-Q.
  mfs        This is used if there is a single plane in the input
             model, which is assumed to represent the image at all
             frequencies. This should also be used if the model has
             been derived from MFCLEAN.
  relax      Relax the convergence criteria. This is useful when
             selfcal'ing with a very poor model.
  apriori    This is used if there is no input model, and the
             source in the visibility data is either a planet,
             or a standard calibrator. This causes the model data
             to be scaled by the known flux of the source. For a
             planet, this flux will be a function of baseline. If
             the source is a point source, the ``apriori'' option
             is only useful if the ``amplitude'' and ``noscale''
             option are being used. For a planet, this option
             should also be used for a phase selfcal, to get the
             correct weighting of the different baselines in the
             solution.
  noscale    Do not scale the gains. By default the gains are scaled
             so that the rms gain amplitude is 1. Generally this
             option should be used with the apriori option.
             It must be used if selfcal is being used to determine
             Jy/K, and should also be used if the model is believed
             to have the correct scale.
  mosaic     This causes SELFCAL to select only those visibilities
             whose observing center is within plus or minus three
             pixels of the model pointing center. This is needed
             if there are multiple pointings or multiple sources in
             the input uv file. By default no observing center
             selection is performed.
.fi
Note that "amplitude" and "phase" are mutually exclusive.
The default is options=phase.
.TP
\fIminants\fP
Data at a given solution interval is deleted  if there are fewer than
MinAnts antennae operative during the solution interval. The default
is 3 for options=phase and 4 for options=amplitude.
.TP
\fIrefant\fP
This sets the reference antenna, which is given a phase angle of zero.
The default, for a given solution interval, is the antennae with the
greatest weight.
.TP
\fIflux\fP
If MODEL is blank, then the flux (Jy) of a point source model can
be specified here. Also used as the default flux for the apriori
option. The default is 1 (assuming the model parameter is not given)
.TP
\fIoffset\fP
This gives the offset in arcseconds of a point source model (the
offset is positive to the north and to the east). This parameter is
used if the MODEL parameter is blank. The default is 0,0. The
amplitude of the point source is chosen so that flux in the model
is the same as the visibility flux.
.TP
\fIline\fP
The visibility linetype to use, in the standard form, viz:
.nf
  type,nchan,start,width,step
.fi
Generally if there is an input model, this parameter defaults to the
linetype parameters used to construct the map. If you wish to override
this, or if the info is not in the header, or if you are using
a point source model, this parameter can be useful.
.TP
\fIout\fP
The output gains file.
The default is to write the gains into the input uvdata file.
