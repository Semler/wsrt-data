%N convlini %F $MIR/src/subs/convl.for
%D Initialize the convolution routines.
%P rjs
%: convolution, fft
%B
        subroutine ConvlIni(lu,Out,n1,n2,phat,ic,jc)

        implicit none
        integer lu,n1,n2,ic,jc
        real Out(n2,n1/2+1),phat

  The Convolution routines perform an FFT-based convolution of two
  images (usually a map and a beam). This routine initializes the 
  convolution routines, and performs an FFT of the beam.

  Input:
    n1,n2      Size of the input beam. Must be a power of 2.
    ic,jc      Reference pixel of the input beam.
    lu         Handle of the MIRIAD file containing the beam. The beam
               is assumed to be even, i.e.
                 Beam(ic+i,jc+j) .eq. Beam(ic-i,jc-j)
    phat       Prussian hat parameter.

  Output:
    Out        The output, transformed, transposed version of the input.
