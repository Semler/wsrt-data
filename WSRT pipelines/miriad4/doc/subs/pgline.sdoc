%N pgline %F /auto/local/miriad4/borrow/pgplot/src/pgline.f
%D Draw a polyline (curve defined by line-segments)
%B
      SUBROUTINE PGLINE (N, XPTS, YPTS)
      INTEGER  N
      REAL     XPTS(*), YPTS(*)

 Primitive routine to draw a Polyline. A polyline is one or more
 connected straight-line segments.  The polyline is drawn using
 the current setting of attributes color-index, line-style, and
 line-width. The polyline is clipped at the edge of the window.

 Arguments:
  N      (input)  : number of points defining the line; the line
                    consists of (N-1) straight-line segments.
                    N should be greater than 1 (if it is 1 or less,
                    nothing will be drawn).
  XPTS   (input)  : world x-coordinates of the points.
  YPTS   (input)  : world y-coordinates of the points.

 The dimension of arrays X and Y must be greater than or equal to N.
 The "pen position" is changed to (X(N),Y(N)) in world coordinates
 (if N > 1).
