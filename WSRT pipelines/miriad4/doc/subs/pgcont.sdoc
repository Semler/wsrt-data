%N pgcont %F /auto/local/miriad4/borrow/pgplot/src/pgcont.f
%D Contour map of a 2D data array (contour-following)
%B
      SUBROUTINE PGCONT (A, IDIM, JDIM, I1, I2, J1, J2, C, NC, TR)
      INTEGER IDIM, JDIM, I1, J1, I2, J2, NC
      REAL A(IDIM,JDIM), C(*), TR(6)

 Draw a contour map of an array.  The map is truncated if
 necessary at the boundaries of the viewport.  Each contour line
 is drawn with the current line attributes (color index, style, and
 width); except that if argument NC is positive (see below), the line
 style is set by PGCONT to 1 (solid) for positive contours or 2
 (dashed) for negative contours.

 Arguments:
  A      (input) : data array.
  IDIM   (input) : first dimension of A.
  JDIM   (input) : second dimension of A.
  I1, I2 (input) : range of first index to be contoured (inclusive).
  J1, J2 (input) : range of second index to be contoured (inclusive).
  C      (input) : array of NC contour levels; dimension at least NC.
  NC     (input) : +/- number of contour levels (less than or equal
                   to dimension of C). If NC is positive, it is the
                   number of contour levels, and the line-style is
                   chosen automatically as described above. If NC is
                   negative, it is minus the number of contour
                   levels, and the current setting of line-style is
                   used for all the contours.
  TR     (input) : array defining a transformation between the I,J
                   grid of the array and the world coordinates.
                   The world coordinates of the array point A(I,J)
                   are given by:
                     X = TR(1) + TR(2)*I + TR(3)*J
                     Y = TR(4) + TR(5)*I + TR(6)*J
                   Usually TR(3) and TR(5) are zero - unless the
                   coordinate transformation involves a rotation or
                   shear.
