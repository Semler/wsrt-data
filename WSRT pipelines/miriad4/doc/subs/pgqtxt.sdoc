%N pgqtxt %F /auto/local/miriad4/borrow/pgplot/src/pgqtxt.f
%D Find bounding box of text string
%B
      SUBROUTINE PGQTXT (X, Y, ANGLE, FJUST, TEXT, XBOX, YBOX)
      REAL X, Y, ANGLE, FJUST
      CHARACTER*(*) TEXT
      REAL XBOX(4), YBOX(4)

 This routine returns a bounding box for a text string. Instead
 of drawing the string as routine PGPTXT does, it returns in XBOX
 and YBOX the coordinates of the corners of a rectangle parallel
 to the string baseline that just encloses the string. The four
 corners are in the order: lower left, upper left, upper right,
 lower right (where left and right refer to the first and last
 characters in the string).

 If the string is blank or contains no drawable characters, all
 four elements of XBOX and YBOX are assigned the starting point
 of the string, (X,Y).

 Arguments:
  X, Y, ANGLE, FJUST, TEXT (input) : these arguments are the same as
                    the corrresponding arguments in PGPTXT.
  XBOX, YBOX (output) : arrays of dimension 4; on output, they
                    contain the world coordinates of the bounding
                    box in (XBOX(1), YBOX(1)), ..., (XBOX(4), YBOX(4)).
