%N shadowed %F $MIR/src/subs/shadowed.for
%P bpw
%: uv-data
%B
      integer function shadowed( tno, ants, limit )
      integer          tno
      double precision ants
      real             limit

 Shadowed returns whether shadowing occured on the baseline coded in
 ants. The coding is done in the usual manner, i.e. ant1=ants/256 and
 ant2=ants-256*ant1, or ants=ant1*256+ant2. One can use the variable
 preamble(4) returned by uvread as input.
 The check is done for the last record read with uvread.
 The returned value is 0 if there was no shadowing, ant1*256 if ant1
 was shadowed, ant2 if ant2 was shadowed and 256*ant1*ant2 if both were
 shadowed.
 The antenna positions and pointings are read from the visibility
 file to which the handle 'tno' corresponds.
 Data is considered shadowed when the projected baseline is less than
 the value given by the variable limit (in units of meters).
 If the value of limit equals 0, shadowed returns with the value 0.

 Inputs:
   tno         The handle of the uv data file
   ants        A code giving the antennas to check on shadowing.
               the code is the same as the one in the preamble(4)
               variable returned by uvread.
   limit       Projected baseline below which data is considered
               shadowed.
