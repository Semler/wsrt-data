%N xyz2llh %F $MIR/src/subs/ephem.for
%D Convert from CIO (x,y,z) to latitude/longitude/height.
%P rjs
%: utilities
%B
        subroutine xyz2llh(x,y,z,lat,long,height)

        implicit none
        double precision x,y,z,lat,long,height

  Convert betweena location defined in terms of CIO (x,y,z)
  coordinates to one in geodetic latitude, longitude, and height
  above the reference geoid.

  Reference:
    Kenneth R. Lang, "Astrophysical Formulae", pages 493-497.
    Values for flattening and equatorial radius from John Reynolds,
    who says they are the IAU 1976 values.

  Input:
   x,y,z       CIO coordinates, in meters.
  Output:
   lat,long    Geodetic latitude and longitude, in radians.
   height      Height above the reference geoid (i.e. sea level), in meters.
