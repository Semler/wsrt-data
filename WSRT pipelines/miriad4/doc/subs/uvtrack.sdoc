%N uvtrack %F $MIR/src/subs/uvio.c
%D Set flags and switches associated with a uv variable.
%P rjs
%: uv-i/o
%B
FORTRAN call sequence:
        subroutine uvtrack(tno,varname,switches)
        integer tno
        character varname*(*),switches*(*)

  UVTRACK allows the programmer to set switches and flags associated with
  a particular uv variable, to allow extra processing steps of that
  variable.

  Input:
    tno         The handle of the input uv file.
    varname     The name of the variable of interest.
    switches    This is a character string, each character of which
                causes a particular flag or switch to be turned on for
                this particular variable. Valid values are:
                 'u'  Remember if this variable gets updated, and  when
                      it gets updated, uvupdate returns .true. the next
                      time it is called.
                 'c'  Remember if this variable gets updated, and when 
                      it gets updated, cause it to be copied during the
                      next call to uvcopyvr.                            
