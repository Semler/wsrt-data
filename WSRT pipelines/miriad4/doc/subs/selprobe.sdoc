%N selprobe %F $MIR/src/subs/select.for
%D Check if a particular uv data has been selected.
%P rjs
%: uv-selection, uv-i/o
%B
        logical function SelProbe(sels,object,value)

        implicit none
        character object*(*)
        real sels(*)
        double precision value

  This routine checks whether particular uv data have been selected by
  the user. It returns .true. if so, or .false. otherwise.

  Inputs:
    sels       The intermidate form of the uv selection, passed back
               by SelInput.
    object     The type of value to check. Possible values are:
                 Object:               Units of Value:
                 'time'                Julian day.
                 'antennae'            Baseline number.
                                       One of ant1 or ant2 can be zero.
                 'uvrange'             Wavelengths.
                 'uvnrange'            Nanoseconds.
                 'visibility'          Visibility number (1 relative).
                 'dra'                 Radians.
                 'ddec'                Radians.
                 'pointing'            Arcseconds.
                 'amplitude'           Same as correlation data.
                 'window'              Window Number.
                 'on'                  On switch.
                 'polarization'        Polarization type.
                 'shadow'              Shadowing.
                 'frequency'           Frequency selection.
                 'source'              Select by source.
                 'ra'                  Select by RA.
                 'dec'                 Select by DEC.
                 'bin'                 Select on bin number.
                 'ha'                  Select on hour angle.
                 'lst'                 Select on LST.
                 'elevation'           Select on elevation.
               Note that this does not support all objects to uvselect.
               The object name may have a suffix of '?' (e.g. 'window?')
               in which case the "value" argument is ignored, and SelProbe
               checks if any selection bassed on this object has been
               performed.
               The name must be given in full (no abbreviations and case
               is significant).
    value      The value to check whether it has been selected.
  Output:
    SelProbe   This returns the value .true. if the data could possibly be
               selected. It does not guarantee that such data might exist
               in any particular data file. It also has the limitation that
               information is not present to convert "uvrange" and "uvnrange"
               calls into each other. These should be treated with caution.
