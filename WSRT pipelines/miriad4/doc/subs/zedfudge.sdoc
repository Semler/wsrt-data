%N zedfudge %F $MIR/src/subs/zed.for
%D Calculate sigma fudge factor, for Zeeman experiments.
%P nebk
%: zeeman
%B
        subroutine ZedFudge(mode,ispect,vspect,m,n1,n2,a,b,fudge,
     *                                                  rho,beam,nx,ny)

        implicit none
        character mode*(*)
        integer m,n1,n2,nx,ny
        real ispect(m,n1*n2),vspect(m,n1*n2)
        real a,b,fudge,rho,beam(nx*ny)

  This determines the fudge factor that the sigma_{alpha} estimate
  should be multiplied by to give a correct value. This is used when
  the noise is correlated.

  Inputs:
    mode       '2' indicates that we should use a two sided derivative.
    m          Number of channels.
    n1,n2      Number of pixels in x and y.
    ispect     I spectra.
    vspect     V spectra.
    a,b        Values of alpha and beta to use.
    rho        Correlation factor in the spectral dimension.
    beam       Dirty beam patch.
    nx,ny      Size of the dirty beam patch.

  Output:
    fudge      Sigma fudge factor. Sigma estimates derived by ignoring the
               noise correlation should be multiplied by this fudge factor
               to get better error estimates.
