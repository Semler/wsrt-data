%N uvdatgti %F $MIR/src/subs/uvdat.for
%D Get integer information about the uvDat routines.
%P rjs
%: uv-i/o, uv-data, uv-selection
%B
        subroutine uvDatGti(object,ival)

        implicit none
        character object*(*)
        integer ival(*)

  This returns miscellaneous information about what is going on inside
  the UVDAT routines.

  Input:
    object     This is a string describing the information to return.
               Possible values are:
                'npol'    Number of simultaneous polarisations being returned
                          by the uvDatRd routine. Zero indicates that this
                          could not be determined.
                'pols'    Returns the polarizations that the uvDatRd
                          routine returns. This is an array of "npol" values.
                          If the types could not be determined, values of
                          zero are returned.
                'pol'     The last Stokes parameter, returned by uvDatRd.
                          This may vary as each new visibility is read.
                'number'  The file number currently being processed.
                'nchan'   Number of channels.
                'nfiles'  Number of files input
                'visno'   Visibility number.
  Output:
    ival       Integer valued output.
