%N mmalloc %F $MIR/src/subs/mm.f2c
%D Allocate a block of memory.
%P rjs
%: miscellaneous
%B
FORTRAN call sequence:
        integer function mmalloc(data,size)
c
        implicit none
        integer size
        integer data(*)

  This allocates memory on the heap. This returns a FORTRAN index,
  relative to data(1) of the allocated memory. The data type is
  given by type.

  Input:
    data        The returned index is relative to data(1). That is, if
                this routine returns index "i", the allocated memory is
                at data(i).
    size        Number of integers to allocate.

  Output:
    mmAlloc     Index to the allocated data. If the allocation fails,
                an index of zero is returned.                           
