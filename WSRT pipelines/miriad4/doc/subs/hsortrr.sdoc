%N hsortrr %F $MIR/src/subs/hsort.for
%D Perform a dual index heapsort on a real array.
%P jm
%: sorting
%B
      subroutine hsortrr(n, array, second, indx)

      implicit none
      integer n, indx(n)
      real array(n), second(n)

  HSORTRR performs an index based heapsort on a real array.
  If there are matching elements in the primary (real) array, the
  corresponding secondary (real) array elements are used to resolve the
  ambiguity.  The number of elements in the array and the two real
  arrays are left unchanged.  The size of the indx array should be
  at least as large as the size of the two real arrays.

     Input:
       n        The number of elements in the array.
       array    The real array on which to base the primary sort.
       second   The real array on which to base the secondary sort.

     Output:
       indx     Sorted integer index array such that array(indx(1)) is
                the smallest and array(indx(n)) is the largest element.
