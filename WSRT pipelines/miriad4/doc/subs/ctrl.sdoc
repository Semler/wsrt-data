%N ctrl %F $MIR/src/subs/ctrl.for
%D Manipulating a control panel.
%P jm
%B
  The control panel routines (all starting with the prefix Ctrl) are
  used to open up a connection to a control panel server (perhaps on
  a remote machine); allow the caller to define a number of buttons,
  cursors, and sliders that the caller wants the user to control; and
  then lets the caller and the user interact with each other.

  Associated with each button, etc, is its current value (or state).
  A button can have several states -- the control panel will show the
  current state as the label on the button. For example "ON" and "OFF".
  The value of a button is the number of the state.  These vary from
  0 to n-1 (for a button with n states), as defined by CtrlDef.  A
  slider takes on a value from 0 to 100 (which the user can change).
  A cursor takes on two values, both ranging from 0 to 100.  Each
  button, cursor, or slider has a caller-defined name, which the
  caller uses for identification.  The caller may also inquire if a
  particular button, slider, or cursor has change its value.
