%N getants %F $MIR/src/subs/calsubs.for
%D Returns the number of antennas and renumbered antenna pairs
%: calibration, baselines
%P pjt
%B
        SUBROUTINE getants( binp, nant, a1, a2 )

        INTEGER binp, nant, a1, a2

  The antennas are re-ordered from 1 to nant, where nant is the
  actual number of antennas, instead of the highest valued antenna
  number. For example if an array has antennas [2,3,6], then with 
  binp = (256*2 + 6) it will return (nant = 3) a1 = 1 and a2 = 3.
  It is assumed that ``nbl'' and ``base(MAXBASHC)'' from calsubs.h
  have been filled in, see readset.

  Input:
       binp  -- baseline number (256*A1+A2)
  I/O
       nant -- if zero on input, then internal table initialized, 
               nant is computed and returned. if nonzero, only input.
  Outputs:
       a1    -- renumbered ordinal value of lower antenna
       a2    -- renumbered ordinal value of upper antenna
