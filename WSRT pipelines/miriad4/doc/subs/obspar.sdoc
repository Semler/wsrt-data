%N obspar %F $MIR/src/subs/obspar.for
%D Get characteristics of a particular observatory.
%: utility
%P rjs
%B
        subroutine obsPar(observ,object,value,ok)

        implicit none
        character observ*(*),object*(*)
        double precision value
        logical ok

  This returns some known characteristics of various observervatories.

  Input:
    observ     Name of the observatory. Current list is :
                 'ALMA', 'ATCA', 'CARMA', CEDUNA30M', 'CSO', 'GMRT',
                 'HATCREEK', 'HOBART26M', 'IRAM15M', 'JCMT',
                 'KITTPEAK', 'NOBEYAMA', 'NOBEYAMA45', 'ONSALA', 'OVRO',
                 'PARKES', 'PENTICTON', 'QUABBIN', 'RPA', 'SZA', 'SZA10', 'SZA6'
                 'WSRT'

    object     The parameter of the observatory of interest. Possible
               values are:
                'latitude '    Observatory latitude, in radians.
                'longitude'    Observatory longitude, in radians.
                'jyperk'       Typical system gain, in Jy/K.
                'systemp'      Typical system temperature, in K.
                'evector'      Offset angle of the feed to the local
                               vertical.
                'mount'        Telescope mount: 0 = alt-az
                                                1   equitorial
                                                3   xy-ew
                                                4   nasmyth
                'antdiam'      Antenna diameter, in meters.
                'subdiam'      Subreflector diameter.
                'height'       Height above sea level, in meters
                'ew'           Positive if the telescope is an E-W array.
                'nants'        Number of antennas normally in the array.
                'ellimit'      Elevation limit.
  Output:
    value      The value of the parameter.
    ok         True if the value was successfully found.
