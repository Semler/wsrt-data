%N whenflt %F $MIR/src/subs/math.for
%D Return locations less than the target.
%P pjt
%: scilib
%B
        subroutine whenflt(n,array,inc,target,index,nval)

        implicit none
        integer n,inc,nval
        integer index(*)
        real array(*),target

  Whenflt returns the all the locations in a real array less than the real
  target.

  Inputs:
    n          Number of elements to be searched.
    array      The real array to be searched.
    inc        Skip distance between elements of the searched array.
    target     Real value to be searched for in the array.

  Output:
    index      The integer array containing the index of locations
               less than the target.
    nval       Number of values put in the index array.

  Reference:
  See page 4-65 to 4-71 of the Cray "Library Reference Manual".
