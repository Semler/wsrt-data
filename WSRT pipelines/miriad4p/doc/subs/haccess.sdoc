%N haccess %F $MIR/src/subs/hio.c
%D Open an item of a data set for access.
%P pjt
%: low-level-i/o
%B
FORTRAN call sequence
        subroutine haccess(tno,itno,keyword,status,iostat)
        integer tno,itno,iostat
        character keyword*(*),status*(*)

  Miriad data sets consist of a collection of items. Before an item within
  a data set can be read/written, etc, it must be "opened" with the haccess
  routine.

  Input:
    tno         The handle of the data set.
    keyword     The name of the item.
    status      This can be 'read', 'write', 'append' or 'scratch'.
  Output:
    itno        The handle of the opened item. Note that item handles are
                quite distinct from data-set handles.
   iostat       I/O status indicator. 0 indicates success. Other values
                are standard system error numbers.                      
