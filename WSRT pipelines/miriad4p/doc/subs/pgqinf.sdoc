%N pgqinf %F /auto/local/miriad4/borrow/pgplot/src/pgqinf.f
%D Inquire PGPLOT general information
%B
      SUBROUTINE PGQINF (ITEM, VALUE, LENGTH)
      CHARACTER*(*) ITEM, VALUE
      INTEGER LENGTH

 This routine can be used to obtain miscellaneous information about
 the PGPLOT environment. Input is a character string defining the
 information required, and output is a character string containing the
 requested information.

 The following item codes are accepted (note that the strings must
 match exactly, except for case, but only the first 8 characters are
 significant). For items marked *, PGPLOT must be in the OPEN state
 for the inquiry to succeed. If the inquiry is unsuccessful, either
 because the item code is not recognized or because the information
 is not available, a question mark ('?') is returned.

   'VERSION'     - version of PGPLOT software in use.
   'STATE'       - status of PGPLOT ('OPEN' if a graphics device
                   is open for output, 'CLOSED' otherwise).
   'USER'        - the username associated with the calling program.
   'NOW'         - current date and time (e.g., '17-FEB-1986 10:04').
   'DEVICE'    * - current PGPLOT device or file.
   'FILE'      * - current PGPLOT device or file.
   'TYPE'      * - device-type of the current PGPLOT device.
   'DEV/TYPE'  * - current PGPLOT device and type, in a form which
                   is acceptable as an argument for PGBEG.
   'HARDCOPY'  * - is the current device a hardcopy device? ('YES' or
                   'NO').
   'TERMINAL'  * - is the current device the user's interactive
                   terminal? ('YES' or 'NO').
   'CURSOR'    * - does the current device have a graphics cursor?
                   ('YES' or 'NO').
   'SCROLL'    * - does current device have rectangle-scroll
                   capability ('YES' or 'NO'); see PGSCRL.

 Arguments:
  ITEM  (input)  : character string defining the information to
                   be returned; see above for a list of possible
                   values.
  VALUE (output) : returns a character-string containing the
                   requested information, truncated to the length 
                   of the supplied string or padded on the right with 
                   spaces if necessary.
  LENGTH (output): the number of characters returned in VALUE
                   (excluding trailing blanks).
