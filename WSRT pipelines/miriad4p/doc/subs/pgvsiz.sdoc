%N pgvsiz %F /auto/local/miriad4/borrow/pgplot/src/pgvsiz.f
%D Set viewport (inches)
%B
      SUBROUTINE PGVSIZ (XLEFT, XRIGHT, YBOT, YTOP)
      REAL XLEFT, XRIGHT, YBOT, YTOP

 Change the size and position of the viewport, specifying
 the viewport in physical device coordinates (inches).  The
 viewport is the rectangle on the view surface "through"
 which one views the graph.  All the PG routines which plot lines
 etc. plot them within the viewport, and lines are truncated at
 the edge of the viewport (except for axes, labels etc drawn with
 PGBOX or PGLAB).  The region of world space (the coordinate
 space of the graph) which is visible through the viewport is
 specified by a call to PGSWIN.  It is legal to request a
 viewport larger than the view surface; only the part which
 appears on the view surface will be plotted.

 Arguments:
  XLEFT  (input)  : x-coordinate of left hand edge of viewport, in
                    inches from left edge of view surface.
  XRIGHT (input)  : x-coordinate of right hand edge of viewport, in
                    inches from left edge of view surface.
  YBOT   (input)  : y-coordinate of bottom edge of viewport, in
                    inches from bottom of view surface.
  YTOP   (input)  : y-coordinate of top  edge of viewport, in inches
                    from bottom of view surface.
