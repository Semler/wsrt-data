%N xyzprfrd %F $MIR/src/subs/xyzio.c
%D Get a profile from a dataset
%P bpw
%: image-i/o
%B
      subroutine xyzprfrd( tno, profilenr, profile, mask, ndata )
      integer    tno
      integer    profilenr
      real       profile(*)
      logical    mask(*)
      integer    ndata

This routine provides a (little) faster version for calls to xyzs2c and
xyzread for the case that the calling program needs profiles. It should
be used after a call to xyzsetup was used to set up one-dimensional
subcubes. The calling program can then loop over all profiles (from 1 to
vircubesize(naxis)/vircubesize(1)). xyzprfrd takes care of reading the
datacube. Using this routine instead of xyzs2c and xyzread reduces the
overhead by 10% (for 256-long profiles) to 30% (for 64-long profiles).

    Input:
      tno           image file handle
      profilenr     profile nr to be read from virtual cube
    Output:
      profile       will contain the profile
      mask          FALSE values indicate undefined pixels
      ndata         number of elements read                                   
