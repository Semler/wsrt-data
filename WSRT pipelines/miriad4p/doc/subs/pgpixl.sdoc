%N pgpixl %F /auto/local/miriad4/borrow/pgplot/src/pgpixl.f
%D Draw pixels
%B
      SUBROUTINE PGPIXL (IA, IDIM, JDIM, I1, I2, J1, J2, 
     1                   X1, X2, Y1, Y2)
      INTEGER IDIM, JDIM, I1, I2, J1, J2
      INTEGER IA(IDIM,JDIM)
      REAL    X1, X2, Y1, Y2

 Draw lots of solid-filled (tiny) rectangles aligned with the
 coordinate axes. Best performance is achieved when output is
 directed to a pixel-oriented device and the rectangles coincide
 with the pixels on the device. In other cases, pixel output is
 emulated.

 The subsection of the array IA defined by indices (I1:I2, J1:J2)
 is mapped onto world-coordinate rectangle defined by X1, X2, Y1
 and Y2. This rectangle is divided into (I2 - I1 + 1) * (J2 - J1 + 1)
 small rectangles. Each of these small rectangles is solid-filled
 with the color index specified by the corresponding element of 
 IA.

 On most devices, the output region is "opaque", i.e., it obscures
 all graphical elements previously drawn in the region. But on
 devices that do not have erase capability, the background shade
 is "transparent" and allows previously-drawn graphics to show
 through.

 Arguments:
  IA     (input)  : the array to be plotted.
  IDIM   (input)  : the first dimension of array A.
  JDIM   (input)  : the second dimension of array A.
  I1, I2 (input)  : the inclusive range of the first index
                    (I) to be plotted.
  J1, J2 (input)  : the inclusive range of the second
                    index (J) to be plotted.
  X1, Y1 (input)  : world coordinates of one corner of the output
                    region
  X2, Y2 (input)  : world coordinates of the opposite corner of the
                    output region
