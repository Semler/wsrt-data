%N isrchilt %F $MIR/src/subs/math.for
%D Search integer vector for target.
%P pjt
%: scilib
%B
        integer function isrchilt(n,array,inc,target)

        implicit none
        integer n,inc
        integer array(*),target

  Isrchilt returns the first location in an integer array that is less 
  than the integer target.

  Inputs:
    n          Number of elements to be searched.
    array      The integer array to be searched.
    inc        Skip distance between elements of the searched array.
    target     Integer value searched for in the array. If target is not
               found, then the routine returns n+1.
  Output:
    isrchilt   Index of the first occurrence of something less than "target".

  Reference:
  See page 4-59 to 4-64 of the Cray "Library Reference Manual".
