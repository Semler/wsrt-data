%N cocvt %F $MIR/src/subs/co.for
%D Convert coordinates.
%P rjs
%: coordinates
%B
        subroutine coCvt(lu,in,x1,out,x2)

        implicit none
        integer lu
        character in*(*),out*(*)
        double precision x1(*),x2(*)

  Convert coordinates from one coordinate system to another.
  Input and output coordinates can be either "pixel" or "world"
  coordinates, and either "absolute" or "offset".

  "World" coordinates are the normal physical units associated with
  a coordinate. World coordinates are given in radians (for astronomical
  positions), GHz (frequencies), km/s (velocities) and lambda (U-V axes).

  Pixel coordinates are fairly conventional.

  For world coordinates, absolute and offset values differ only by the
  reference world value (crval). The exception is longitude-type axes
  (e.g. RA), where offset coordinates are offsets on the sky -- that is
  the offsets are multiplied by the cos(latitude) (cos(DEC)) term.

  For pixel coordinates, absolute and offset values differ by reference
  pixel value (crpix).

  For visibility datasets (where the axes are simply RA and DEC), "pixel
  coordinates" are defined as those that would result from imaging (using
  a conventional 2D Fourier transform algorithm) the data-set with a cell
  size of 1 radian. Additionally the reference pixel values (crpix) are set
  to 0. This means that the "absolute pixel" and "offset pixel" coordinates
  are identical, and that "offset pixel" and "offset world" coordinates
  differ only by the inaccuracy in the normal small angle approximation for
  offsets in RA and DEC.

  Input:
    in         This indicates the units of the input coordinates.
               It consists of a sequence of,
                 'op'  Offset pixel coordinate
                 'ap'  Absolute pixel coordinate
                 'ow'  Offset world coordinate
                 'aw'  Absolute world coordinate,
               one for each coordinate requiring conversion. Each
               coordinate unit specifier is separated from the other
               by a slash (/).
               For example 'ap/ap' indicates two coordinates, both in
               absolute pixels.
    x1         The input coordinates, in units as givien by the `in'
               parameter. The dimensionality must agree with the number
               of units given in `in'.
    out        This indicates the units of the output coordinates, in the
               same fashion as the `in' value. The outputs must correspond
               one-for-one with the inputs.
  Output:
    x2         The output coordinates, in units given by `out'. 
