%N matchdcd %F $MIR/src/subs/match.for
%D Check if a string occurs in a list of valid strings, with extras
%P bpw
%: strings
%B
      logical function matchdcd( input,valid_strings,nr,logsc,cum,abs )

      character*(*) input
      character*(*) valid_strings
      integer       nr
      logical       logsc
      logical       cum
      logical       abs

 matchdcd does a minimal match of the input string 'input' on the
 comma-separated list of valid strings. It also returns the string number.
 Depending on the values of the logical variables logsc, cum and abs, it allows
 the input string to be c preceded by 'log' or '>' or to be surrounded by '|',
 respectively, returning whether this was the case or not in the same logicals.
 If such special options are present, they will be stripped from the input
 string. I.e. if input was "|x|" and abs=true, on exit input will be "x".

 On exit the value of matchdcd indicates whether or not any match was found. If
 no match was produced nr is zero on exit.

 The matching is minimal, that means that if valid_strings has the value
 'mass,flux,nh' and input is 'f', a match is produced. The return value of nr
 will be 2. On exit input will be set equal to the full string that was
 matched.
 If the input string is preceded by the string 'exact:' the minimal matching
 feature is turned off and only exact matches produce a return value true.

 If on input logsc is true, it will be true on output if input was 'logmass',
 but false if input was 'mass'.
 If on input cum is true, it will be true on output if input was '>flux', but
 false if input was 'flux'.
 If on input abs is true, it will be true on output if input was '|x|', but
 false if input was 'x'.
 These three variables can be combined, i.e. the input can be 'log>|x|'. If
 logsc, cum or abs are false on input, these possibilities are disabled. If all
 three are false, a call to function match may be more useful.

 Input:
   input:         the string of which it has to be checked if it matches
   valid_strings: a comma-separated list of valid input strings
 Output:
   nr:            the string number of input; 0 if there was no match
 Input/Output:
   logsc:         if true on input: input string may be preceded by log
                  output indicates whether it was or not
                  if false on input: input string can not be preceded by log
   cum:           if true on input: input string may be preceded by >
                  output indicates whether it was or not
                  if false on input: input string can not be preceded by >
   abs:           if true on input: input string may be surrounded by |
                  output indicates whether it was or not
                  if false on input: input string can not be surrounded by |
