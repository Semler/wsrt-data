%N xyzplnwr %F $MIR/src/subs/xyzio.c
%D Write a plane to a dataset
%P bpw
%: image-i/o
%B
      subroutine xyzplnwr( tno, planenr, plane, mask, ndata )
      integer    tno
      integer    planenr
      real       plane(*)
      logical    mask(*)
      integer    ndata

This routine provides a more convenient version of calls to xyzs2c and
xyzwrite for the case that the calling program provides planes. It
should be used after a call to xyzsetup was used to set up 2-dimensional
subcubes. The calling program can then loop over all planes (from 1 to
vircubesize(naxis)/vircubesize(2)). xyzplnwr takes care of writing the
datacube. The caveat is that the calling program should have an array
that is large enough to contain the complete plane.
Using this routine instead of xyzs2c and xyzwrite reduces the overhead
by 1% (for 64**2 cubes) or less.

    Input:
      tno           image file handle
      planenr       plane nr to be read from virtual-cube
      plane         contains the plane to be written as a 1-d array
      mask          FALSE values indicate undefined pixels
      ndata         number of elements to write                               
