%N smacheck
%D Check and analyse uvVariables and flag uv-data.
%P jhz
%: data check, analysis and flagging
%B
SmaCheck is a Miriad program to check, analyse uv variables 
for SMA data and flag uv-data if variables or data are 
out of range.
%A vis
The input visibility file. No default.
%A select
This selects which visibilities to be used. Default is
all visibilities. See the Users Guide for information about
how to specify uv-data selection.
%A flagval
set to either 'flag' or 'unflag' to flag the data.
Default: don not change the flags.
%A var
Name of uv-variable to check. Default is systemp.
Print mean and rms values of variable WITHIN the selected range.
A histogram plot can be printed (options=histo).
%A range
Minimum and maximum values for uv-variable.
Flag the uv-data when the uv-variable is OUTSIDE this range.
Default range=-1E20,1E20. Note that in in order to flag
data within a selected range, first flag everything, then
unflag those outside the selected range.
%A options
Extra processing options. Possible values are:
  debug    Print more output
  histo    Print histogram
  circular for circular polarization data (RR,LL,RL,LR), 
           flag out the non-circular polarization data if
           flagval is flag or default or unflag the non-circular 
           polarization data if flagval is unflag.
%A log
The output logfile name. The default is the terminal
