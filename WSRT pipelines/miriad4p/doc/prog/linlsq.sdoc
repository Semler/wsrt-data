%N linlsq %F $MIR/src/subs/lsqu.for
%D Return parameters of a straight line fit
%: least-squares
%P bpw
%B
      subroutine linlsq( xarr,yarr,npnt, a1,b1,a2,b2, sigx,sigy,corr )

      real           xarr(*)
      real           yarr(*)
      integer        npnt
      real           a1, a2, b1, b2
      real           sigx, sigy, corr

 This routine returns the parameters of a linear least squares fit to the
 relation defined by xarr and yarr.

 Input:
   xarr:         the x values
   yarr:         the y values
   npnt:         number of elements of xarr and yarr

 Output:
   a1, b1:       coefficients of the relation y=a1*x+b1
   a2, b2:       coefficients of the relation x=a2*y+b2
   sigx, sigy:   rms values of x and y
   corr:         correlation coefficient
