%N hread %F $MIR/src/subs/hio.c
%D Hwrite -- Read and write items.
%P pjt
%: low-level-i/o
%B
FORTRAN call sequence
        subroutine hreada(itno,abuf,iostat)
        subroutine hreadb(itno,bbuf,offset,length,iostat)
        subroutine hreadj(itno,jbuf,offset,length,iostat)
        subroutine hreadi(itno,ibuf,offset,length,iostat)
        subroutine hreadr(itno,rbuf,offset,length,iostat)
        subroutine hreadd(itno,dbuf,offset,length,iostat)
        subroutine hwritea(itno,abuf,iostat)
        subroutine hwriteb(itno,bbuf,offset,length,iostat)
        subroutine hwritej(itno,jbuf,offset,length,iostat)
        subroutine hwritei(itno,ibuf,offset,length,iostat)
        subroutine hwriter(itno,rbuf,offset,length,iostat)
        subroutine hwrited(itno,dbuf,offset,length,iostat)
        integer itno,offset,length,iostat
        character abuf*(*),bbuf*(length)
        integer jbuf(*),ibuf(*)
        real rbuf(*)
        double precision dbuf(*)

  These routines read and write items of a Miriad data set. They
  differ in the sort of element that they read or write.
        hreada,hwritea  I/O on ascii text data (terminated by newline char).
        hreadb,hwriteb  I/O on ascii data.
        hreadj,hwritej  I/O on data stored externally as 16 bit integers.
        hreadi,hwritei  I/O on data stored externally as 32 bit integers.
        hreadr,hwriter  I/O on data stored externally as IEEE 32 bit reals.
        hreadd,hwrited  I/O on data stored externally as IEEE 64 bit reals.

  Note that hreada and hreadb differ in that:
    * hreada reads sequentially, terminating a read on a newline character.
      The output buffer is blank padded.
    * hreadb performs random reads. Newline characters have no special
      meaning to it. A fixed number of bytes are read, and the buffer is
      not blank padded.
   Hwritea and hwriteb differ in similar ways.

  Inputs:
    itno        The handle of the item to perform I/O on.
    offset      The byte offset into the item, where I/O is to be
                performed.
    length      The number of bytes to be read.

  "Offset" and "length" are offsets and lengths into the external file, always
  given in bytes.

  Note that "offset" and "length" must obey an alignment requirement. Both
  must be a multiple of the size of the element they are performing I/O on.
  For example, they must be a multiple of 2 for hreadj,hwritej; a multiple
  of 4 for hreadi,hwritei,hreadr,hwriter; a multiple of 8 for hreadd,hwrited.

  Inputs(hwrite) or Outputs(hread):
    abuf,bbuf,jbuf,ibuf,rbuf,dbuf The buffer containing, or to receive,
                the data.
  Outputs:
    iostat      I/O status indicator. 0 indicates success. -1 indicates
                end-of-file. Other values are standard system
                error numbers.                                          
