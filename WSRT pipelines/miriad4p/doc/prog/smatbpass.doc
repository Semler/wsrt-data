%N smatbpass
%D Removes time-dependent bandpass ripples (antenna-based).
%P jhz
%: calibration
%B
SmaTbpass is a MIRIAD task which removes time-dependent bandpass 
ripples. SmaTbpass reads multiple files (bpfile) containing 
antenna-based bandpass solved using either smamfcal or mfcal
in different time intervals. The bandpasses are interpolated
or extrapolated to a time with either linear fit from
the two nearby data points or orthogonal polynomial
fit from all the data points in the input files.

The antenna-based bandpass in each of the multiple input
files must be IDENTICAL in spectral format, i.e. they must have
the same number of channele, the same number of spectral
chunks, and the same polarizations. The data used for solving 
for bandpass in the multiple files must be observed in the same 
track as the observations of the target sources in the vis files.

The output contains the data that have been applied with
the bandpass corrections.
%A bpfile
Root name of input files. Files must be named as
bpfile_i for the ith file, e.g. mybpass_1, mybpass_2 ... mybpass_n.
Each of the n bpfiles contains a bandpass solved from an independent
time interval using either smamfcal or mfcal.  No default. 
NOTE: The calibration tables (bandpass, gains, freqs) must present 
in the input datasets. 
%A nfiles
The number of bandpass files (bpfile) to read.
%A vis
The name of the input uv data sets in which the visibility
will be applied for bandpass corrections. Several can be given (wild
cards are supported) but the spectral configuration(correlator setup)
is identical through out the files. No default.
%A bptime
The time interval, in minutes, to interpolate/extrapolate the 
bandpass solutions. The default is 60 minutes.
%A options
This gives extra processing options. Several options can be given,
each separated by commas. They may be abbreivated to the minimum
needed to avoid ambiguity. Possible options are:
   nocal       Do not apply the gains file. By default, UVAVER
               applies the gains file in copying the data.
   opolyfit    Do least-square fit to the time variations in
               bandpasses given in all the bpfile_i files with an 
               orthogonal polynomial of degree n input from 
               Keyword: polyfit; n upto 10 is supported.
               Default: linear fit from two nearby time points.
   cross       Apply the bandpass to the cross-hand polarization
               visibilities in addition to the parallel ones.
               Default: to the parallel ones only.
%A polyfit
polyfit gives a degree of orthogonal polynomial in least-sqaure
fit to the time variaton of the bandpass. Default: 3 or cubic.
polyfit upto 10 is supported.
%A out
The name of the output uv data set. No default.
