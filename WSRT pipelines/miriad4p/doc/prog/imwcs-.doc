%N imwcs-
%D Changes image header for new WCS parameters
%P pjt
%: image analysis
%B
IMWCS changes the image header variables related to the WCS
(crvalN,crpixN,cdeltN,ctypeN).

This program is a cheat, and currently assumes a purely linear
coordinate system, i.e. at pixel 'i'
             wcs_value = crval + (i-crpix)*cdelt

Only 1 axis at a time can be changed.

If multiple keywords are changed, the order in which they are
changed is: CRVAL, CRPIX, CDELT, CTYPE.
If only a single one is changed, the related one (CRVAL<->CRPIX)
will also be changed, unless auto is set to false.

See also:  IMPOS, REGRID, IMFRAME
%A in
The input image dataset. No default.
%A axis
For which axis to change a WCS keyword. Any number between 1 and
the NAXIS value of an image. No default.
%A crval
Axis reference value to change.
%A crpix
Axis reference pixel to change.
%A cdelt
Axis pixel increment to change.
%A ctype
Axis name to change.
%A auto
Boolean to signify if a single keyword is changed, the related
one should also be changed. E.g. changing crpix should also
crval in a consistent way.
Default: true
%A center
Boolean, if set and crpix is not set, it will set the reference
pixel to the center of the axis, (N+1)/2.
Note that if auto=true and crval not given, a new crval will be
computed.
Default: false
