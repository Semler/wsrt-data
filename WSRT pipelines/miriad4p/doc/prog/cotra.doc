%N cotra
%D Coordinate transformations
%P pjt
%: utility
%B
COTRA is a MIRIAD task to transform between astronomical coordinate 
systems.  The coordinate systems must be one of:
equatorial, galactic, ecliptic, super-galactic
%A radec
Input RA/DEC or longitude/latitude. RA is given in hours
(or hh:mm:ss), whereas all the others are given in degrees
(or dd:mm:ss). There is no default.
%A type
Input coordinate system. Possible values are
"b1950" (the default), "j2000", "galactic", "ecliptic"
and "super-galactic". b1950 and j2000 are equatorial coordinates
in the B1950 and J2000 frames. All other coordinates are in the
B1950 frames.
%A epoch
Epoch (in standard Miriad time format) of the coordinate. Note
that this is distinct from the equinox of the coordinate as given
above. Varying epoch changes the coordinate values by less than an
arcsecond. The default is b1950.
