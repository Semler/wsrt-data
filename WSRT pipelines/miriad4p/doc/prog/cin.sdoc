%N cin %F $MIR/src/subs/si.for
%D Cosine integral
%P rjs
%: utilities
%B
        double precision function cin(x)

        implicit none
        double precision x

     written by d.e. amos and s.l. daniel, november, 1975.
     modified by a.h. morris

     references
         sand76-0062
         the special functions and their approximations, vol. ii, by
         y.l. luke. academic press, new york, 1969.

     Abstract
         cin computes the integral of (1-cos(t))/t on (0,x) by means
         of chebyshev expansions on (0,5) and (5,infinity).

     Description of arguments

         input
           x      - limit of integration, unrestricted

         output
           cin    - value of the integral
