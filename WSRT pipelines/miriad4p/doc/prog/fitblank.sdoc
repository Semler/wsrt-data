%N fitblank %F $MIR/src/subs/fitsio.for
%D Set and check the FITS blank mode.
%P rjs
%: fits
%B
        logical function FitBlank(lu,mode)

        implicit none
        integer lu
        logical mode

  FitFlg returns the current blanking mode. If .true., this indicates
  that the FITS file (either input or output) could contain blanked pixels
  or correlations.

  For a new file, the "mode" argument can be used to indicate whether the
  output could contain blanked values. To set a value as blanked, you
  must call the appropriate "flgwr" routine. If you are going to call the
  flgwr routines, you must indicate that blanking is to be handled
  before any data is written.

  For old files, the "mode" argument is ignored.

  Input:
    mode
