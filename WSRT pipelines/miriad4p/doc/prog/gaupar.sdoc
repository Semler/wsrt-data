%N gaupar %F $MIR/src/subs/gaupar.for
%D Determine effective beam of the convolution of two images.
%P rjs
%: image analysis
%B
        subroutine GauPar(bunit1x,dx1,dy1,bmaj1,bmin1,bpa1,
     *                    bunit2x,dx2,dy2,bmaj2,bmin2,bpa2,
     *                    bunit,bmaj,bmin,bpa,fac)

        implicit none
        character bunit1x*(*),bunit2x*(*),bunit*(*)
        double precision dx1,dy1,dx2,dy2
        real bmaj1,bmin1,bpa1,bmaj2,bmin2,bpa2,bmaj,bmin,bpa,fac

  Determine the units and effective beam of the convolution of
  two images.

  Input:
    bunit1x    The units of the first image, e.g. JY/PIXEL or JY/BEAM.
    dx1,dy1    Increment in x and y.
    bmaj1,bmin1 Beam major and minor FWHM, in same units as dx1,dy1.
               May be 0.
    bpa1       Position angle of the beam, in degrees.

    Similarly for bunit2,dx2,dy2,bmaj2,bmin2,bpa2, except that its
    for the second image.

  Output:
    bunit      Units of the resultant image, e.g. JY/BEAM.
    bmaj,bmin  Effective beam major and minor FWHM.
    bpa        Effective beam position angle.
    fac        Factor to multipl the result by to convert to correct
               units.
