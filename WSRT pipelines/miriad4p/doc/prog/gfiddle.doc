%N gfiddle
%D Fiddle with a (gain) visibility dataset
%P pjt
%: calibration
%B
GFIDDLE is a MIRIAD task that allows interactive and batch
processing of a gain visibility dataset (created by GMAKE).
It can also modify the flags of a regular visibility dataset
but adding breakpoints and fits has no meaning for them.
Also see UVWIDE on how to feedback wide band flags to narrow
band flags.

You can actually fiddle with *any* visibility dataset, so the
units could be K, Jy, Jy/K, or sqrt(Jy/K), depending on the
origin of the dataset, although the reported units may not
always come out as you think.

Gains can be either antenna based or baseline based.

By default, the single sideband data are displayed.  One can
optionally run in double sideband mode (i.e. phase differences
and amplitude ratios) by using the dsb option (see below).
GMAKE, when run to compute the double sideband solution, puts
the single sideband solutions (ssb) in the first two wide
channels and the double sideband solutions (dsb) in the third
and fourth channels.  If the dsb solutions are not present
and dsb mode is still requested, then then ssb data will be
used to compute the dsb data.

Commands in cursor mode are:

    d   -- delete the nearest point (i.e. flag bad).
    a   -- add the nearest deleted point (i.e. flag good).
    x   -- zoom/unzoom toggle on the current column.
    y   -- zoom/unzoom toggle on the current row.
    z   -- zoom/unzoom toggle on the current plot.
    b   -- insert a breakpoint.
    c   -- delete the nearest breakpoint.
    m   -- switch between DSB/SSB mode (see note with ssb option below).
    0-9 -- order of the polynomial fit/number of spline nodes.
    t   -- toggle between polynomial and cubic spline fits.
    f   -- two-point fit (simple linear interpolation).
    l   -- toggle between single/all baselines (see link option below).
    i   -- info on the nearest (good) data point.
    r   -- redraw the screen (also rescales the axes).
    +   -- advance to the next page (if multiple pages).
    -   -- backup to the previous page (if multiple pages).
    <>  -- shift left (right) one panel (only in zoom mode).
    v^  -- shift down (up) one panel (only in zoom mode).
    ?,h -- this help.
    q   -- quit (no saving).
    e   -- exit (save flags, breakpoints, and fits, if modified).
    .      Toggle DEBUG mode.

NOTE: there is no confirmation on 'q' or 'e'.
%A vis
Name of the input uv dataset.  There is no default and a
dataset must be supplied.
%A line
The line to be selected. Default: wide,2
There can only be one or two channels selected; all others cause
the program to terminate with a warning.  Also, any other valid
option but the default will be permitted, but warned about.
Note that this will also affect changed flags, breakpoints, and
what is written to the optional output dataset.
This keyword is usually omitted by the user.
%A select
UV dataselection. Default: all
Note that this will also affect changed flags, breakpoints and
what is written to the optional output dataset.
This keyword is usually omitted by the user.
%A device
PGPLOT graphics device name. If none is supplied or the device
selected does not have an interactive cursor, then the program
will run in batch mode (i.e. no cursor interaction).  The default
device is /null (batch mode).
%A batch
Name of a batch file in which interactive (if the PGPLOT device
supports it) commands can be stored. By default commands are appended
to the file, but with 'options=new' this behavior can be changed.
Be careful when you re-apply batch files with commands that have a 
cumulative action (flags, breaks etc.)
%A nxy
The maximum number of plots per page in the x direction.  The
number of plots in the y direction is controlled by the program.
Be careful if you use this keyword with batch= since they must
be consistent accross calls.
The default is 3 plots in the x direction. 
%A fit
This keyword provides a way for the user to apply an initial fit
to the data.  The arguments to this keyword determines what kind
of initial fit will be applied (and possible optional parameters).
Valid options are:
  poly,<amp_poly_order>,<phase_poly_order>
  2pt
  spline,<#_of_spline_nodes>
The amp and phase polynomial orders must be in the range [0, 9].
Omitting the phase polynomial order defaults to order 2; omitting
the amplitude order defaults to order 0.
The number of spline nodes must be in the range [0, 9] (the
actual number of spline nodes is one larger).  The default for the
spline fit argument is 0 (i.e. 1 node).  The 2pt fit simply
connects the dots; there are no additional parameters to this fit.
By default, no initial fit is applied.  However, the fit values
are initialized to 1 for amplitudes and 0 for phases.
%A sample
Output sample time in minutes.  It is adviced to set the sampling 
time finer than the typical intervals in the input file.
The default is 1 minute.
%A clip
Automatically clips all amplitudes larger than this value.
The default is to perform no clipping.
%A options
Additional processing options. Multiple options, if compatible,
can be given, separated by commas.
link      While flagging points, this option applies the change
          to the corresponding points on other baselines.
          Similarly, with breakpoints.  Note that unflagging and
          clearing of breakpoints cannot be done in this mode.
          The interactive counterpart (key l) allows the user to
          switch this mode interactively.
nowrap    Do not attempt to unwrap phases.
sources   If no break point information is present in the input
          uv dataset, the program will automatically set break
          points.  Focus changes always create a break point.
          Source name changes will also create a break point
          but only if this option is used.
nobreak   Do not set breakpoint on reading the data. By default
          it will.
dsb       By default, the single sideband data are displayed.
          If this option is present, then the plots and fits will
          be done in double sideband mode (i.e. phase differences
          and amplitude ratios).  Note that this option controls
          which of the windows permits editing.  The interactive
          counterpart (key m) permits viewing of the other window,
          but no editing.
new       In batch mode an existing batch file will normally be
          processed in a special append mode: first the existing
          commands are read and applied, then new commands will
          be added to the batch file. options=new will overwrite
          the batch file and start from scratch.
debug     Debugging mode.  A lot of output to aid in providing
          the programmer with relevant info. The '.' command
          can be used as a debug toggle in interactive mode.
%A ampmax
If supplied, the amplitude scale is fixed from 0 to ampmax;
otherwise auto-scaling is done.
The default is to auto-scale the amplitudes.
%A phminmax
Range in phases to be displayed.  If supplied, two values (in
units of degrees) are needed.  If only one value is supplied,
the second value will be the negative of the first.
The default is to auto-scale the phases.
%A phase
List of (antenna,{U|L},phase_slope/hour) 3-tuples that
are used as an estimate for unwrapping the phases.  When low
signal/noise data have large phase drifts, the automatic phase
unwrapper may not be sufficient to unwrap the phase.  This
parameter is an attempt to give the user some automated way to
experiment with unwrapping phases.  Phases are given in degrees;
Antenna is an integer; and U/L is given as 1/2 (for upper or
lower sideband).
Default: not used.
%A out
The name of the output UV dataset. The default is to NOT write
out (and, hence, not save) the derived fits.

COLORS:
If the graphic device selected is capable of drawing with various
colors, then the following list will identify their meanings:
  BLUE:
    Fits drawn in blue indicate that no output uv dataset
    name was provided.
  GREEN:
    If a valid output dataset is provided, then new fits
    will be drawn in green.
  YELLOW:
    Yellow fits identify windows in which the fit is old
    for some reason.  A new fit should be generated.
