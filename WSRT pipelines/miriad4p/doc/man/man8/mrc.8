.TH mrc  8
.SH NAME
mrc  - Multi-Resolution CLEAN
.SH PERSON RESPONSIBLE
bpw
.SH CATEGORIES
deconvolution
.SH DESCRIPTION
MRC applies the Multi-Resolution CLEAN algorithm. This algorithm is an
adaptation of CLEAN particularly useful for extended sources and low
signal-to-noise maps. Maps which contain only point sources can better
be deconvolved with the standard CLEAN. Maps with sources larger than
about 4 beams and/or signal-to-noise ratios less than about 6 will be
deconvolved better with MRC.
MRC takes the input map and beam and makes a smooth and difference map
and beam. Both are separately cleaned and restored, then added
properly (see below for more details). The output is the cleaned map.
The cleaning of the smooth map concentrates on the extended structure
and low signal-to-noise areas, the separate cleaning of the difference
map allows one to keep the original resolution.
MRC allows you to save the beams and intermediate dirty maps, saving
some time when trying to rerun it. On the first run of MRC, use the in=
and beam= keywords. If you also use save=name,y,y, the beams and dirty
maps are saved. Rerunning MRC with the same keywords will then cause it
to use the saved datasets.
.PP
WARNING: MRC needs many intermediate files, which are stored on your
local directory. MRC tries to keep the running amount to a minimum,
but you might hit the limit anyway. Then the only solution is to clean
your dataset a few channels at a time, or get more diskspace.
.PP
The algorithm works in detail as follows:
a) A gaussian with integral 1 is constructed.
b) The dirty beam is smoothed with the gaussian. The smoothed beam is
.nf
   subtracted from the original beam to yield a difference dirty beam.
   The smoothed beam is scaled back to have a peak value of 1. This
   scale factor is called 's'. The difference beam is also scaled back.
.fi
c) A clean beam dataset is constructed, and treated in the same way as
.nf
   dirty beam to form a smoothed and difference clean beam. The scale
   factor is called 'r'.
.fi
d) The input map is smoothed with the gaussian (and not scaled) to get
.nf
   a smoothed map. This is subtracted from the original to get a
   difference map.
.fi
e) The smoothed map is cleaned (using the standard miriad clean) with
.nf
   the smoothed dirty beam to get 'smooth components' and a 'smooth
   residual'.The components are then restored with the smoothed clean
   beam and scaled with 's/r'. Before cleaning, the smoothed map is
   smoothed with an extra factor 1.5 to create a signal mask, i.e. the
   region in the map where the S/N is larger than about 1. This step
   improves the quality of the clean.
.fi
f) The difference map is cleaned with the difference dirty beam to get
.nf
   'difference components' and a 'difference residual'. The components
   are then restored with the difference clean beam and scaled with
   's/(s-1) * (r-1)/r'.
.fi
g) The restored and scaled smoothed and difference map and the
.nf
   residuals of both cleans are added together to get the final
   result.  
.SH PARAMETERS
.TP
\fIin\fP
.fi
The input dirty map. No default, unless 'save=' says to used saved maps.
.TP
\fIbeam\fP
The input dirty beam. No default, unless 'save=' says to use saved beams.
.TP
\fIregion\fP
Specifies the region in which to find clean components. See Users
Manual or 'doc region' for instructions on how to specify this. The
default is the inner quarter of all planes.
Unless the beam dataset has size 2n-1, in which case it is the whole plane.
.TP
\fIout\fP
The output dataset, i.e. the cleaned input map.
.TP
\fIfactor\fP
The factor with which to multiply the beamsize to get the fwhm of the
smoothing gaussian. Default 1.7, 1.5
.TP
\fIcutoff\fP
Up to three values: cutoff levels for cleaning smooth/difference map
and for creating mask, in units of rms. Defaults: cutoff=1*rms of
smoothed map for smooth map clean, 2*rms of original map for difference
map clean, 2*rms of doubly-smoothed map for mask creation.
If third cutoff < 0, use the absolute value, but remake the mask even
if a file that could be the mask exists.
.TP
\fInoise\fP
The noise level in the dirty map. Combined with cutoff= to tell CLEAN
when to stop. No default.
A second value can be given if 'clalg=2mrc' is used, giving the noise
in the smoothed map. By default this is calculated.
.TP
\fIclalg\fP
algorithm to use
- clean - use standard clean, rather than multi-resolution clean
- std   - clean smoothed map again, using standard clean
- 2mrc  - clean smoothed map again, using mrc
- reg2  - first clean smoothed map using standard clean with mask up to
.nf
          5 sigma, clean the rest using a box
.fi
- test  - follow a 2mrc point-source clean; use in=, beam=, out= to specify
.nf
          maps, set region= to a small box around the point source
.TP
\fIsave\fP
.fi
The default is not to save any intermediate results. With save they can
be saved. Save takes four arguments. The first gives a prefix for the
saved datasets. The others indicate which saves are to be done and take
the values 't', or 'f' (default is 't').
(Also allowed are 1, T, y, or Y and 0, F, N or n).
The 2nd argument refers to saving intermediate beams
The 3rd argument refers to saving intermediate dirty maps and masks
The 4th argument refers to saving intermediate results (components,
.nf
    restored components and residuals)
.fi
If the 2nd argument is 't', the dirty beams, smoothing beams and clean
beams are saved in a dataset with suffix '.beams'.
If the 3rd argument is 't', the smooth and difference dirty maps and the
signal mask are saved in datasets with suffixes '.smmap', '.dfmap' and
'.mask'.
If the 4th argument is 't', the intermediate results are saved in datasets
with suffixes '.smcmp', '.smrst', '.smres', '.dfcmp', '.dfrst' and '.dfres'.
For clalg=2mrc, the intermediate smooth clean map is saved with suffix
'.smcln', and filename for saving intermediate results of the second stage
have a ".2." added.
Using save=name,t,t allows to skip remaking the beams and dirty maps for
each run. If name.beams is present that is used preferentially over the
value given by the 'beam=' keyword. If name.smmap and name.dfmap are
present, these are used preferentially over the value given by the 'in='
keyword.
.SH FILES
 $MIR/src/scripts/mrc
