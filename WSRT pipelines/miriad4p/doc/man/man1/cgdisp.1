.TH cgdisp 1
.SH NAME
cgdisp - Displays and overlays images on a PGPLOT device
.SH PERSON RESPONSIBLE
nebk
.SH CATEGORIES
plotting
.SH DESCRIPTION
CGDISP displays/overlays images via contour plots, pixel map
representations, vectors and scaled boxes on a PGPLOT device. 
Upto 3 contour plots, one pixel map, one vector plot and one box 
display may be overlaid in multi-panel plots of multi-channel 
images.  In addition overlay locations (plotted as boxes, stars,
circles, lines or see-through) may be specified from an ascii 
text file.
.PP
Manipulation of the device colour lookup table is available
when you display with a pixel map representation (formerly
called a "grey scale")
.SH PARAMETERS
.TP
\fIin\fP
You may input up to 7 images.  Upto 3 of these can be displayed 
via contour plots and 1 can be displayed via a colour pixel map 
representation.  1 vector amplitude image and 1 vector position
angle image (degrees; positive N -> E) can together be used to
display a vector map (e.g. polarization vectors).  1 image can
be displayed as small scaled boxes (see below) and 1 image may be
used as a mask.  
.PP
The "box" image is displayed by drawing little boxes (solid and
hollow for positive and negative pixels) at the location of each
selected pixel.  The size of the box scales with the value of the
pixel.  This is a useful way to display rotation measure images 
for example. The mask image blanking mask is logically ANDed to all
the other image masks before they are displayed. The mask image 
is not displayed.
.PP
If more than one image is specified, they must have identical 
first and second dimensions.  However, you can overlay combinations
of 2-D with 3-D images (e.g. multi-channel images with a continuum 
image) provided all the 3-D images have the same third dimension. 
These images can be input in any order (see TYPE).
Wild card expansion is supported.    No default.
.TP
\fItype\fP
Specifies the type of each image, respectively, listed in the IN 
keyword. Minimum match is supported (note that "pixel" was 
formerly "grey" [which is still supported]).   Choose from:
.PP
.nf
 "contour"   (contour;            up to 3 of these)
 "pixel"     (pixel map;          up to 1 of these)
 "amplitude" (vector amplitude;   up to 1 of these)
 "angle"     (vector pos'n angle; up to 1 of these)
 "box"       (box;                up to 1 of these)
 "mask"      (mask;               up to 1 of these)
.fi
.PP
You can't give one of "amplitude" or "angle" without the other.
Default is "pixel" for one image, "contour" if more than one.
.TP
\fIregion\fP
Region of interest.  Choose only one spatial region (bounding box
only supported), but as many spectral regions (i.e., multiple 
IMAGE specifications) as you like.   Each channel (or group of 
channels; see CHAN below) is drawn on a new sub-plot.  
NOTE: the region specification applies equally to all the 
input images.
Default is full image
.TP
\fIxybin\fP
Upto 4 values.  These give the spatial increment and binning
size in pixels for the x and y axes to be applied to the selected
region.   If the binning size is not unity, it must equal the 
increment.  For example, to bin up the image by 4 pixels in 
the x direction and to pick out every third pixel in the y 
direction, set XYBIN=4,4,3,1
Defaults are 1,XYBIN(1),XYBIN(1),XYBIN(3)
.TP
\fIchan\fP
2 values. The first is the channel increment to step through the
image in, the second is the number of channels to average, for 
each sub-plot.  Thus CHAN=5,3  would average groups of 3 channels 
together, starting 5 channels apart such as: 1:3, 6:8, 11:13 ...  
The channels available are those designated by the REGION keyword.
A new group of channels (sub-plot) is started if there is a 
discontinuity in the REGION selected channels (such as 
IMAGE(10,20),IMAGE(22,30).  The combination of REGION and CHAN 
determines how many sub-plots there will be.
.PP
In the case that you have input some combination of 2-D and 3-D
images, CHAN refers to the 3-D image(s). Note that a channel
is defined to be a pixel on the third axis of a cube, regardless
of the cube's order (xyv or vxy say).
Defaults are 1,1
.TP
\fIslev\fP
Up to 3 pairs of values, one for contour image. First value is 
the type of contour level scale factor.  "p" for percentage and 
"a" for absolute.   Second value is the factor to scale LEVS by. 
Thus, SLEV=p,1  would contour levels at LEVS * 1% of the image 
peak intensity.  Similarly, SLEV=a,1.4e-2 would contour levels 
at LEVS * 1.4E-2
Default is no additional scaling of LEVS (i.e., "a",1.0)
.TP
\fIlevs1\fP
The levels to contour for the first specified contour image are 
LEVS1 times SLEV (either percentage of the image peak or absolute).
Defaults try to choose something vaguely useful.
.TP
\fIlevs2\fP
LEVS for the second contour image.
.TP
\fIlevs3\fP
LEVS for the third contour image.
.TP
\fIcols1\fP
PGPLOT colours for LEVS1 contours. 0 is background colour, 1 
foreground, others are different colours. If you give one value
it is used for all contours.
.TP
\fIrange\fP
N groups of 4 values (1 group per subplot and N is the maximum
number of channels allowed by Miriad; typically 2048). These are 
the image intensity range to display (min to max), the transfer 
function type and the colour lookup table for each subplot 
displayed.  The transfer function type can be one of "lin" 
(linear), "sqr" (square root), "log" (logarithmic), and "heq" 
(histogram equalization).  The colour lookup table is an integer 
from 1 to 8 specifying a lookup table. Valud values are 1 (b&w),
2 (rainbow), 3 (linear pseudo colour), 4 (floating zero colour 
contours), 5 (fixed zero colour contours), 6 (rgb), 7 (background)
8 (heat) and 9 (absolute b&w) .  If you enter a negative 
integer, then the reversed lookup table is displayed.  
.PP
The transfer function changes available with OPTIONS=FIDDLE 
are in addition (on top of) to the selections here, but the 
colour lookup table selections will replace those selected here.
.PP
All subplots following the last one with a specified "range"
will use the "range" settings from the previous subplot. In
this way, one group of settings can be applied to all the 
subplots if desired.  The multiple subplot capability is useful
if you have used IMCAT to put unlike images into planes of
a cube and you wish to display them together.
.PP
Default is linear between the image minimum and maximum with
a b&w lookup table.   You can default the intensity range with
zeros, viz. "range=0,0,log,-2" say.
.TP
\fIvecfac\fP
3 or 4 values.  A scale factor to multiply the vector image
lengths (or box image widths) by, the x and y increments (in
pixels) across the image at which to plot the vectors (or boxes),
and optionally the length of the scale-bar vector
(unset for no scale-bar). If you have set non unit values of
XYBIN, the increments here refer to the binned pixels.  When
VECFAC(1)=1, the vectors (boxes) are scaled so that the maximum
amplitude (width) takes 1/20 of the (sub)plot size. 
.PP
The scale-bar gives a graphical representation of the vector
lengths, which makes vector plots easier to interpret.  The
scale-bar is drawn in the corner specified by the BEAMTYP key
(defaulting to bottom-left if BEAMTYP is not specified). If
VECFAC(4)=0, the scale bar is drawn the length of the longest
vector; you can find out what this is using OPTIONS=FULL. For a
fractional polarization vector map, setting VECFAC(4)=1
corresponds to 100 per cent polarization. If VECFAC(1) >> 1, this
will give a very long vector. For polarization intensity images,
VECFAC(4) is specified in flux density.
.PP
Defaults are 1.0, 2, VECFAC(2)
Default is not to draw a scale-bar.
.TP
\fIboxfac\fP
3 values.  A scale factor to multiply the box image widths by, 
and the x and y increments (in pixels) across the image at which
to plot the boxes).  If have set non unit values of XYBIN, the 
increments here refer to the binned pixels.  When BOXFAC(1)=1, 
the boxes are scaled so that there is a little bit of space
between adjacent boxes.
Defaults are 1.0, 2, BOXFAC(2)
.TP
\fIdevice\fP
The PGPLOT plot device, such as plot.plt/ps 
No default.
.TP
\fInxy\fP
Number of sub-plots in the x and y directions on the page. 
Defaults choose something depending on your telescope.
.TP
\fIlabtyp\fP
Up to 2 values.  The spatial label type of the x and y axes.
Minimum match is active.  Select from:
.PP
.nf
 "hms"       the label is in H M S.S (e.g. for RA)
 "dms"       the label is in D M S.S (e.g. for DEC)
 "arcsec"    the label is in arcsecond offsets
 "arcmin"    the label is in arcminute offsets
 "absdeg"    the label is in degrees
 "reldeg"    the label is in degree offsets
             The above assume the pixel increment is in radians.
 "abspix"    the label is in pixels
 "relpix"    the label is in pixel offsets
 "abskms"    the label is in km/s
 "relkms"    the label is in km/s offsets
 "absghz"    the label is in GHz
 "relghz"    the label is in GHz offsets
 "absnat"    the label is in natural coordinates as defined by 
             the header. 
 "relnat"    the label is in offset natural coordinates
 "none"      no label and no numbers or ticks on the axis
.fi
.PP
All offsets are from the reference pixel.  
Defaults are "relpix", LABTYP(1)   except if LABTYP(1)="hms" when
LABTYP(2) defaults to "dms"  (to give RA and DEC)
.TP
\fIbeamtyp\fP
Up to 6 values. Set if you want a small polygon to be drawn to
represent the beam FWHM. Setting beamtyp to "b,l" is sufficient to
draw a solid beam; "b,l,4" will result in a cross-hatched
beam.  Use 'n' if you don't want a beam at all.
The six parameters are:
.PP
- Beam vertical positioning: can be "t" (top), or "b" (bottom). No
.nf
  default.
.fi
- Beam horizontal positioning: can be "l" (left), or "r"
.nf
  (right). Default "l"
.fi
.PP
The next four parameters apply only to the first image specified
with the "in" keyword.  If there are multiple, different beams to
draw (for example, if different uv data were used to produce
images with different beam shapes), all subsequent beams are drawn 
as open polygons.
.PP
- Hatching style:
.nf
   1    solid (default)
   2    outline
   3    hatched
   4    cross-hatched
.fi
- Hatching angle (default 45 degrees).
- Hatching line separation (default 1).
- Line-width for outlines, hatching and cross-hatching (default 1)
.TP
\fIoptions\fP
Task enrichment options. Minimum match of all keywords is active.
.PP
"abut" means don't leave any white space between subplots.  The
.nf
  default is to leave a little bit between subplots, and 
  OPTIONS=GAPS leaves a lot of space and labels eacg subplot
  separately.
.fi
"beamAB", where "A" is one of "b" or "t" and 
.nf
                "B" is one of "l" or "r"
  means draw the beam FWHM on the plot in the corner indicated
  by the "AB" location. This option is deprecated: use the
  keyword "beamtyp" instead.
.fi
"blacklab" means that, if the device is white-background, draw
.nf
  the axis labels in black. Default is red. 
.fi
"conlabel" means label the contour values on the actual contours.
.nf
  The PGPLOT routine that does this is not very bright. You will
  probably get too many labels.  If you bin the image up with
  keyword XYBIN, say, by a factor of 2, you will get about 1/2
  as many labels.   If desperate use the overlay facility 
  (keyword OLAY) to manually label contours.
.fi
"fiddle" means enter a routine to allow you to interactively change
.nf
  the display lookup table.  You can cycle through a variety of
  colour lookup tables, as well as alter a linear transfer function
  by the cursor location, or by selecting predefined transfer 
  functions (linear, square root, logarithmic, histogram equalization)
  
  For hard copy devices (e.g. postscript), a keyboard driven
  fiddle is offered; you can cycle through different colour tables
  and invoke the predefined transfer functions, but the linear
  fiddler is not available.   Note that if you are using "cgdisp"
  from a script, so that interactive fiddling is not appropriate,
  you can use the "range" keyword to specify the transfer
  function and colour lookup tables.
.fi
"full" means do full plot annotation with contour levels, pixel
.nf
  displa range, file names, reference values, etc.  Otherwise 
  more room for the plot is available. 
.fi
"gaps" means leave large gaps between subplots and individually
.nf
  label the axes of each subplot. By default, the subplots will 
  have a small amount of white space between each subplot and 
  they will only be labelled around the borders of the full page.  
  See also OPTIONS=ABUT to eliminate the small amount of white space.
.fi
"grid" means draw a coordinate grid on the plot rather than just ticks
"mirror" causes all specified contour levels for all images
.nf
  to be multiplied by -1 and added to the list of contours
.fi
"nodistort" means that angularly-defined overlays do not distort 
.nf
  with the coordinate grid.  If you are displaying a large area of 
  the sky, such that the non-linearities in the coordinate system 
  can be seen, then by default, the overlays (keyword OLAY) will 
  distort with the coordinate grid if you are using angular units 
  for the overlay locations and half sizes.  Thus star overlays
  will rotate and stretch, circles will distort similarly. 
  Overlays given in non-angular units will always be undistorted.
.fi
"noepoch" means don't write the epoch value into the axis labels
"noerase" means don't erase a rectangle into which the "3-axis"
.nf
  values and the overlay ID strings are written.
.fi
"nofirst" means don't write the first x-axis label on any subplots
.nf
  except for the left-most one. This may avoid label overwrite.
.fi
"corner" means only write labels in the lower left corner of any
.nf
  subplot
.fi
"relax" means issue warnings when image axis descriptors are
.nf
  inconsistent (e.g. different pixel increments) instead
  of a fatal error.  Use at your peril.
.fi
"rot90" rotates vectors by an extra 90 degrees.  Useful
.nf
  to convert E-vectors into B-vectors
.fi
"signs"  Normally, when plotting vectors, CGDISP assumes that
.nf
  North is up and East to the left.  If OPTIONS=SIGNS, then
  it assumes that E and N are in the direction of increasing
  X and Y.
.fi
"single" means that when you have selected OPTIONS=FIDDLE and you
.nf
  you have more than one subplot per page, activate the fiddle
  option after each subplot rather than the default, which is
  to fiddle only at the end.  In the latter case, the histogram
  equalization, if invoked, will have been computed with the 
  image in the last subplot only.
.fi
"solneg1" means make negative contours solid and positive 
.nf
  contours dashed for the first contour image. The default, 
  and usual convention is the reverse.
.fi
"solneg2" SOLNEG1 for the second contour image.
"solneg3" SOLNEG1 for the third contour image.
"trlab" means label the top and right axes as well as the 
.nf
  bottom and left ones.  This can be useful when non-linear 
  coordinate variation across the field makes the ticks misaligned
.fi
"unequal" means draw plots with unequal scales in x and y
.nf
  so that the plot surface is maximally filled.  The default
  is for equal scales in x and y.
.fi
"wedge" means that if you are drawing a pixel map, also draw
.nf
  and label a wedge to the right of the plot, showing the map 
  of intensity to colour.
.fi
"3pixel" means label each sub-plot with the pixel value of
.nf
  the third axis.
.fi
"3value" means label each sub-plot with the appropriate 
.nf
  value of the third axis (e.g. velocity or frequency for an
  xyv ordered cube, position for a vxy ordered cube).
  Both "3pixel" and "3value" can appear, and both will be 
  written on the plot.  They are the average values when
  the third axis is binned up with CHAN.  If the third axis
  is not velocity or frequency, the units type for "3VALUE" 
  will be chosen to be the complement of any like axis in the 
  first 2. E.g., the cube is in vxy order and LABTYP=ABSKMS,ARCSEC 
  the units for the "3VALUE" label will be arcsec.  If 
  LABTYP=ABSKMS,HMS the "3VALUE" label will be DMS (if the 
  third [y] axis is declination).  See also keyword "3format"
  where you can input the format for the "3value" labelling.
.TP
\fI3format\fP
.fi
If you ask for "3value" labelling, this keyword allows you
specify the FORTRAN format of the labelling.  I have given
up trying to invent a decent algorithm to choose this. Examples
are "1pe12.6", or "f5.2" etc   If you leave this blank cgdisp 
will try something that you probably won't like.
.TP
\fIlines\fP
Up to 6 values.  The line widths for the axes, each contour 
image (in the order of TYPE), the vector image, and any overlays.
If there are less than 3 contour images or no vector
image, the vector image/overlay line widths shift left.
Line widths must be integers.
Defaults are 1,1,1,1,1,1
.TP
\fIbreak\fP
Up to 3 values. The intensity levels for the break between
solid and dashed contours for each contour image. 
Defaults are 0.0,0.0,0.0
.TP
\fIcsize\fP
Up to 4 values.  Character sizes in units of the PGPLOT default
(which is ~ 1/40 of the view surface height) for the plot axis
labels, the velocity/channel label, the overlay ID string
(if option "write" in OLAY used) label, and the contour
value labels (see options=conlab). 
Defaults try to choose something sensible.  Use 0.0 to default
any particular value. E.g., 1.4, 0, 0, 0.5
.TP
\fIscale\fP
Up to 2 values.  Scales in natural axis units/mm with which to plot
in the  x and y directions.  For example, if the increments 
per pixel are in radians, then this number would be radians/mm
(note that for RA axes you give radians on the sky per mm).
Although this choice of unit may be cumbersome, it makes no 
assumptions about the axis type, so is more flexible.   If you 
also chose OPTIONS=EQUAL then one of your scales, if you set 
both and differently, would be over-ruled.  If you give only 
one value, the second defaults to that.  
Defaults choose scales to fill the page optimally. To default 
the first but the second, use 0.0,scale(2)
.TP
\fIolay\fP
The name of a file containing a list of overlay descriptions.
Wild card expansion is active and the default is no overlays.
.PP
Miriad task CGCURS OPTIONS=CURSOR,LOG,CGDISP  can be used to
make an overlay file.
.PP
Entries in the overlay file can be white space or comma
delimitered or both.  All lines beginning with # are ignored.
.PP
.nf
                **** DO NOT USE TABS **** 
.fi
.PP
Double quotes " are used below to indicate a string.  The "
should not be put in the file.   For all the string parameters
discussed below, you can abbreviate them with minimum match.
.PP
Each line describes an overlay and should be as follows:
.PP
.nf
 ##### The first 5 columns in each line must be
.fi
.PP
.nf
  1      2       3     4    5        Column
 --------------------------------
 OFIG  XOTYPE  YOTYPE  ID  WRITE      where
.fi
.PP
OFIG is the type of overlay; choose from
.nf
 "star"    for stars (crosses; give centre and half-sizes)
 "circle"  for a filled in circle (give centre and radius)
 "ocircle" for an open circle (give centre and radius)
 "ellipse" for a filled in ellipse (give centre, half axes and p.a.)
 "oellipse for an open ellipse (give centre, half axes and p.a.)
 "box"     for boxes (give centre and half-sizes)
 "line"    for line segments (give ends)
 "clear"   for a see-through overlay -- thus you can write the
           overlay ID string (see below) without the overlay
 "sym"     for pgplot symbol number (given centre and symbol)
.fi
.PP
XOTYPE and YOTYPE  give the units of the overlay location (and 
overlay half-sizes) contained in the file for the x- and y-
directions, respectively.  Choose from:
.nf
 "hms", "dms", "arcsec", "arcmin", "absdeg", "reldeg", "abspix",
 "relpix", "absnat", "relnat", "absghz", "relghz", 
 "abskms", & "relkms"  as described in the keyword LABTYP.  
.fi
Note that OTYPE does not depend upon what you specified for LABTYP.
.PP
ID is an identifying overlay string which can be optionally
written on the overlay; it MUST be in the overlay file whether
you write it on the plot or not).  The ID string is written in the
corner for "star" and "box", in the centre for "clear", "circle"
at the end for "line".  Note that the underscore character "_"
is treated a special case and is replaced by a blank before plotting.
In this way, you can write several words as the overlay ID; you
connect them with underscores in the overlay file, and cgdisp
strips them out before plotting.
.PP
WRITE is "yes" or "no" to specify if the overlay ID is to be 
written in the overlay figure or not.
.PP
.nf
 ##### Columns beyond number 5 depend upon OFIG, XOTYPE, and YOTYPE
.fi
.PP
.nf
 6   7    8   9  10  11  12   Logical column
 ---------------------------
 X   Y   XS  YS  CS  CE       for OFIG="box" and "star"
 X1  Y1  X2  Y2  CS  CE       for OFIG="line"
 X   Y   R   CS  CE           for "circle" and "ocircle"
 X   Y   R1  R2  PA  CS  CE   for "ellipse" and "oellipse"
 X   Y   CS  CE               for OFIG="clear"
 X   Y   SY  SS  CS  CE       for OFIG="sym"
.fi
.PP
X,Y defines the center of the overlay in the nominated OTYPE
coordinate system (X- and Y-OTYPE can be different).  
(X1,Y1) & (X2,Y2) are the end points of the line segment in the
nominated OTYPE (mixed OTYPEs are supported here too).
For %OTYPE = "abspix ", "relpix", "arcsec", "arcmin", "absdeg", 
.nf
             "reldeg", "absghz", "relghz", "abskms", "relkms", 
             "absnat" & "relnat" X,Y,X1,Y1,X2,Y2 are single numbers.
.fi
.PP
For %OTYPE = "hms" or "dms", the X and/or Y location is/are replaced
by three numbers such as  HH MM SS.S or DD MM SS.S.  Thus if
XOTYPE=hms & YOTYPE=dms then the file should have lines like
.PP
.nf
  HH MM SS.S   DD MM SS.S   XS   YS  CHAN    for OFIG="box", say
.fi
.PP
XS, YS are the overlay half-sizes in the following units.
%OTYPE = "abspix" and "relpix" in pixels
.nf
         "hms"    and "dms"    in arcseconds
         "arcsec"              in arcseconds
         "arcmin"              in arcminutes
         "absdeg" and "reldeg" in degrees
         "absghz" and "relghz" in GHz
         "abskms" and "relkms" in km/s
         "absnat" and "relnat" in natural coordinates
.fi
.PP
SY is the pgplot symbol to use for "sym"
.PP
SS is the pgplot character height to use for "sym". Default
is character height used for overlay string
.PP
R is the radius of circle overlays.  It is in the units given
in the above list according to XOTYPE only. 
.PP
R1 and R2 are the ellipse major and minor axes half-widths,
both in units according to XOTYPE. PA is the position angle
in degrees, positive  N -> E
.PP
CS to CE is the channel range (image planes) on which to put the 
overlays.  If you specify only CS than the overlay is put
on that channel.  If CS=0 then the overlays are put on all
channels. 
.PP
For OFIG="box" and "star", XS, YS are optional.  The defaults
are XS=2, YS=XS pixels.   In all cases, CS and CE  are optional
and the default is 0 (all channels)
.PP
#####  The OFFSET line
.PP
At any point in the overlay file, you can include an OFFSET
line in the format
.PP
"OFFSET"   XOFF   YOFF
.PP
where the literal "OFFSET" (without the quotes) must appear
as the first thing in the line, followed by X and Y offsets,
which are applied to all succeeding overlay file locations.
.nf
       X = X + XOFF;   Y = Y + YOFF
.fi
These offsets must be in the same units as the %OTYPE that the
succeeding line(s) has(ve).  It is intended so that your overlay
locations can be in, say, arcsec relative to some location which
is not the reference pixel of the image (which is what CGDISP
ultimately wants).   You then specify, with the OFFSET line, the
offsets between the reference pixel of the contour/pixel map
images and the actual reference location of your overlay locations.
.PP
You can have as many OFFSET lines as you like in the file.  All
succeeding lines will apply these offsets until new ones are
defined.  If the line does not appear, naturally no additional
offsets are added.
.PP
The OFFSET line is not applied to ANY position fields in succeeding
lines that have %OTYPEs that are "hms" or "dms".    I am too lazy
to code it.
