.TH tvclip 1
.SH NAME
tvclip - Interactive editing of a UV data set on a TV device.
.SH PERSON RESPONSIBLE
jm
.SH CATEGORIES
calibration, uv-data, tv, plotting, display
.SH DESCRIPTION
TVCLIP is a MIRIAD task which allows interactive baseline
editing of a UV data set.  The user should make sure,
if the display device is capable of creating menus that
the menu program has been loaded ``before'' running this
routine (eg. xpanel for the XMTV) - see the User's Guide
or Cookbook under TV Devices for details.
.PP
When this program is run, each baseline is displayed,
one at a time, with the x-axis representing channel
number (increasing to the right), the y-axis representing
the change in time (increasing upward), and the color
intensity representing the amplitude or phase (depending
on the value of the keyword ``mode'') of the visibility.
The current antenna that constitute the baseline are
labeled at the bottom of the plot (if room permits).
In addition, if room permits, a color wedge is displayed
to the right side of the visibility data and is the sum
of the data over all displayed channels.  Also, a wedge
of the sum of the data over all displayed times will
appear above (or below) the data as room permits.
.PP
The user may edit the data in one of two ways: 1) By
entering commands at the keyboard; or 2) by selecting
commands from the listed menu items.  Which method is
determined by the presence of a menu program (such as
``XPANEL'' for the Sun systems).
.PP
If no menu program is currently active, then a message
is issued to the user and all commands are prompted from
the keyboard.  Commands are entered followed by a carriage
return (<CR>).  Single letter abbreviations are used for
all commands.  The current command list, each corresponding
abbreviation, and a brief description of each command is
available by entering a question mark (``?'') as a command.
Commands requiring further interaction (eg. ``select'' and
``value'') will prompt the user when they are invoked.
.PP
If a menu program is active, then TVCLIP will attempt
to construct an assortment of menu buttons and other
items that will perform the equivalent of entering commands
at the keyboard.  Certain commands (eg. ``select'' and
``value'') require further cursor input and will prompt
the user for a particular action (for example: ``select''
will request the user to select the region to box for
further flagging commands).
.PP
The current technique of editing is to load the baseline
to be edited, zoom and pan to the desired location, and
then, select the region to be edited followed by an
editing command.  To identify a region to edit, chose
the ``select'' command (or ``channel'' or ``time'' select
options) and then use the cursor to box the desired points.
To select a desired region, move the cursor (mouse) to a
corner of the region to select; identify, to the program,
the corner (usually by pushing and holding the left mouse
button); move the pointer to the other corner (by dragging
the mouse), and then identify, again to the program, the
other corner (usually by releasing the left mouse button).
After the region to edit has been identified, the selected
region may be flagged as good or bad using the appropriate
editting command (or button).  If data is flagged improperly,
the ``undo'' command will reverse the last editing operation.
.PP
NOTE:  If the device does not support this type of ``rubber-
band'' selection (Eg. XMTV does; MXAS does not), then the
user will be asked to use the cursor to identify two opposite
corners to delimit the selected region.
.PP
To save the editing done to a particular baseline, use
the ``exit'' command (or select the ``exit'' button).
The flagging changes will be immediately applied to the
data.  To NOT save the changes made to a particular
baseline, use the ``quit'' command (or button).  After
a ``quit'' or ``exit'' operation, the data for the next
baseline is loaded onto the display and the user may continue
editing.  Selecting the ``abort'' command (button) at any
time will perform the same operation as ``quit'', but
will also terminate the program.
.PP
You can use the 'batch-mode' of tvclip if you have a lot of
similar flagging to do. Use tvclip in interactive mode to 
determine a good clip level and a sequence of DIFF and CLIP 
commands that is appropriate for your data. Then set the 
clip level and enter the commands in the 'commands' keyword. 
You can then either watch the automatic flagging on the tv, 
or switch off the display (options=notv) to speed things up. 
.SH PARAMETERS
.TP
\fIvis\fP
The name of the input UV data set.  A visibility file name
must be supplied.  Only one file may be edited at a time.
.TP
\fIserver\fP
This is used to specify the TV display device. It is giving in the
form:
.PP
.nf
  type@server
.fi
.PP
or
.PP
.nf
  type@device
.fi
.PP
The first form causes a connection to be made with a network
display server, whereas the second form uses local hardware
directly. For the network servers, the server process may have
to be started before invoking the Miriad task -- see below.
.PP
Currently supported types are:
.PP
.nf
  xmtv          Miriad X server. `server' is the servers hostname.
                (use "localhost" if you are not networked)
                Normally the xmtv server will be the same host as
                the X server. The user will normally invoke the xmtv
                server before running any Miriad TV tasks, with
.fi
.PP
.nf
                  % xmtv  
.fi
.PP
.nf
                If the X server and xmtv server are on different
                hosts, you may need to set the DISPLAY environment
                variable on the xmtv server, and use xhost on the
                X server.
.fi
.PP
.nf
  ivas          IVAS (VMS only). `device' is the VMS device name.
                You will require an IIS IVAS for this.
.fi
.PP
.nf
  ivserver      IVAS server. `server' is the servers hostname.
                Servers are currently installed on
                castor.astro.uiuc.edu and bkyast.berkeley.edu.
.fi
.PP
.nf
  msss          Miriad Sun Screen server. `server' is the serving
                Sun, which can be any Sun with a console. The user
                will normally invoke the server before running
                any Miriad TV tasks, with
.fi
.PP
.nf
                  % msss  
.fi
.PP
.nf
  mxas          Miriad X server. `server' is the servers hostname.
                This is an inferior server to xmtv.
.TP
\fItvchan\fP
.fi
An integer specifying which TV channel the data is
displayed.  The default is channel 1.
.TP
\fIrange\fP
The minimum and maximum range used in scaling the data
on the TV.  Default is to autoscale to the first image.
If this keyword is used, TWO parameters must be input.
.TP
\fItvcorn\fP
The integer device coordinate of the lower left corner
of the image.  The default is to center the image on the
display.  If this keyword is used and only the x coordinate
value is input, the y coordinate value is set to the x value.
.TP
\fIline\fP
The ``line'' parameter determines the channels that are to be processed
from a uv data-set. The parameter value consists of a string
followed by up to four numbers. Defaults are used for any missing
trailing portion of the parameter value.
.PP
A uv data-set can have correlations from either (or both) a spectral
or a wideband (continuum) correlator. Both the spectral and wideband
correlators produce multiple correlations (channels). The string
part of the line parameter is used to select the spectral or wideband
channels. It may be one of:
.nf
  "channel"   Spectral channels.
  "wide"      Wideband (continuum) channels.
  "velocity"  Spectral channels, resampled at equal increments in
              velocity (using the radio definition). The resampling
              involves a weighted average of the spectral channels.
              This is useful if the source was not Doppler tracked
              correctly.
  "felocity"  Similar to "velocity", except that the parameters are
              given using the optical velocity definition.
.fi
Generally the default is "channel" if the data-set has spectral
channel data, and "wide" otherwise.
.PP
The four numbers that accompany the string give:
.PP
.nf
  nchan, start, width, step
.fi
.PP
These four numbers specify which channels to select from the input
dataset and how to combine them to produce the output channels that
the Miriad task will work on.
.PP
nchan   is the number of output channels produced. Generally it
.nf
        defaults to the number of input channels.
.fi
.PP
start   is the first channel from input dataset that is to be used.
.nf
        It defaults to 1 (i.e. first channel).
.fi
.PP
width   gives the number of input channels to average together to
.nf
        produce a single output channel. It defaults to 1.
.fi
.PP
step    gives the increment between selected input channels. It
.nf
        defaults to the value of "width".
.fi
.PP
For "velocity" linetype, the start, width and step parameters are
given in km/s. The output channels are centered on velocities:
start, start+step, start+2*step, etc.
.PP
The `line' parameter interacts with the "select=window" selection
for "channel" and "velocity"/"felocity" linetypes. See the help on
select for more information.
.PP
For example:
.PP
.nf
  line=channel,10
.fi
.PP
selects 10 output channels, being input spectral channels 1 to 10.
Similarly
.PP
.nf
  line=channel,10,8,1,2
.fi
.PP
selects 10 output channels, starting at input channel 8, and skipping
every second input channel, whereas
.PP
.nf
  line=channel,10,8,2,2
.fi
.PP
selects 10 output channels, again starting at input channel 8, but
each of the output channels consists of the average of two of the
input channels. Finally
.PP
.nf
  line=velocity,10,-10,1,1
.fi
.PP
resamples the spectral data in velocity, to give 10 channels of width
1 km/s. The channels are centered at -10.0,-9.0,-8.0, etc, km/s.
NOTE: Here ``type'' must be `channel' or ``wide'' and the maximum 
of  both ``width'' and ``step'' must be 1.  The default is
to display all channels.
.TP
\fImode\fP
Display ``amplitude'', ``phase'', ``real'', ``imaginary''.
By default, ``amplitude'' is selected. For mode=phase, the
phase is in degrees.
.TP
\fItaver\fP
The length of time (in minutes) used to average the data when
determining the running mean. This is used with the "DIFF"
command, where the running mean of the data are subtraced off.
Two values can be given for taver, being TTOT and TGAP
respectively. If the time interval between any
two successive data points is greater than TGAP, or if the
total time between the first data point in an average
and any succeeding data point exceeds TTOT, then a new
average is started.  The default for TTOT is 5 minutes, whereas
the default TGAP is the value of TTOT.
.TP
\fIclip\fP
The clip level (in average absolute deviations, a parameter 
similar to rms, but less sensitive to outliers)
This is used with the "CLIP" command. All data more than clip 
times the av. abs. dev. from the median will be clipped. 
This clip operation is repeated until no more points are clipped.
Common values are 4,5,6. Flagging on stokes V data with clip=4
usually gets rid of most interference. The default is 5.
.TP
\fIoptions\fP
Extra processing options. Several are possible:
.nf
  'nochannel', 'notime' and 'nopixel' are clip options. 
          By default the "CLIP" command will flag channels, times and 
          individual pixels with an rms that is too far from the median. 
          These options allow you to exclude some forms of clipping.
  'notv'  display option, do not show anything on the tv.
          Speeds up non-interactive clipping. This can only be used if you 
          also fill in the commands keyword. Because with 'notv' the data 
          does not have to fit on the screen, less time averaging is needed. 
          Usually the data will be flagged at full time resolution.
  'nosrc' Do not cause a break in the display when the source
          changes. Normally TVFLAG puts a gap in the display
          whenever the source changes.
.TP
\fIcommands\fP
.fi
This allows non-interactive flagging using the "CLIP" command.
Use this to specify a sequence of flagging commands to be applied
for each baseline, e.g., commands=diff,clip. 
The "EXIT" command is implicitly added at the end of the list.
You will not be able to interact with tvclip using mouse or
keyboard if "commands" is set.
.TP
\fIselect\fP
This keyword selects the subset of the visibility data to be
processed. There are a number of subcommands, which may be
abbreviated. Each may be prefixed with a plus or minus sign to
indicate using or discarding the selected data. Many subcommands
can be given (separate them by commas). Subcommands include:
.PP
.nf
  time(t1,t2)
.fi
Select data between times t1 and t2 (UT). Times are in the format:
.nf
  yymmmdd.fff
.fi
or
.nf
  yymmmdd:hh:mm:ss.s
.fi
Various abbreviations are possible. If no date part is given, then
the time matchs all data (regardless of its date) for the given
time of day.
.PP
.nf
  antennae(a1,a2,...)(b1,b2...)
.fi
Select all baselines pairs formed between first and second list of
antennas. The second list is optional and defaults to all antennas.
.PP
.nf
  uvrange(uvmin,uvmax)
.fi
Select visibilities with uv radius between uvmin and uvmax (in kilo
wavelenghts). If only one value is given, uvmin is taken as zero.
.PP
.nf
  uvnrange(uvmin,uvmax)
.fi
Select visibilities with uv radius between uvmin and uvmax (in
nanoseconds). If only one value is given, uvmin is taken as zero. 
.PP
.nf
  visibility(n1,n2)
.fi
Select visibilities numbered n1 to n2 inclusive.
.PP
.nf
  increment(inc)
.fi
Select every inc'th visibility.
.PP
.nf
  ra(r1,r2)
.fi
Select visibilities whose RA is in the range r1 to r2. RAs are given
in the format
.nf
  hh:mm:ss
.fi
or
.nf
  hh.hhh
.fi
Various abbreviations are possible.
.PP
.nf
  dec(d1,d2)
.fi
Select visibilites whose DEC is in the range d1 to d2. Declinations
are given in the format
.nf
  dd:mm:ss
.fi
or
.nf
  dd.ddd
.fi
Various abbreviations are possible.
.PP
.nf
  dra(p1,p2)
.fi
Select visibilities for which the RA of the pointing center is offset
from the main pointing center by between p1 and p2 arcseconds.
.PP
.nf
  ddec(p1,p2)
.fi
Select visibilities for which the DEC of the pointing center is offset
from the main pointing center by between p1 and p2 arcseconds.
.PP
.nf
  pointing(p1,p2)
.fi
Select visibilities with the rms pointing error in the range p1 to p2
arcseconds. If only one number is given, p1 is taken as 0.
.PP
.nf
  polarization(p1,p2,p3,...)
.fi
Select visibilities on the basis of their polarization/Stokes parameter.
p1,p2,p3,... can be selected from "i,q,u,v,xx,yy,xy,yx,rr,ll,rl,lr"
Conversion between polarizations Stokes parameters (e.g. convert
linears to Stokes)  is not performed by this mechanism (see keyword stokes).
.PP
.nf
  source(SRCNAM1,SRCNAM2,...)
.fi
Select correlations from the particular source. Several sources
can be given. An asterisk in the source name is treated as a
wildcard. Note that the sourcename MUST BE IN CAPITAL CASE. 
.PP
.nf
  frequency(f1,f2)
.fi
Select correlations, where the sky frequency of the first correlation
is in the range f1 to f2 (in GHz). If only a single frequency is
given, then correlations are selected if the first correlation
is within 1% of the given frequency.  Note this selects the whole
record.  This may include several spectral-windows whose frequency
is quite different from the first correlation in the first window.
.PP
.nf
  amplitude(amplo,amphi)
.fi
Select any correlation, where the amplitude is between "amplo" and
"amphi". If only one value is given, then "amphi" is assumed to be
infinite.
.PP
.nf
  shadow(d)
.fi
Selects data that would be shadowed by an antenna of diameter "d" meters. 
If "d" is zero, then the actual diameter of the antennas (if known) is used. 
If some data is shadowed, it is advisable to use an antenna diameter value 
greater than the physical antenna size (e.g. 20% larger).
.PP
.nf
  bin(b1,b2)
.fi
Select visibilities which have pulsar bin numbers in the range
b1 to b2 inclusive. If b2 is omitted, just pulsar bin b1 is
selected.
.PP
.nf
  on
.fi
This is used with single dish observations, anbd selects based
whether the "on" variable in the data is on!
.PP
.nf
  auto
.fi
This is used with files that contain a mix of autocorrelations and
crosscorrelations. This selects just the autocorrelation data.
.PP
.nf
  window(w1,w2,...)
.fi
Select by spectral window (IF band in AIPS terminology). See
the help on window for more information.
.PP
.nf
  or
.fi
The list of subcommands before and after the 'or' are "ored"
together.
NOTE: The default is to use all visibilities.
