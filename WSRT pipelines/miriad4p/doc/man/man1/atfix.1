.TH atfix 1
.SH NAME
atfix - Apply various miscellaneous corrections to ATCA visibility data
.SH PERSON RESPONSIBLE
rjs
.SH CATEGORIES
uv analysis
.SH DESCRIPTION
ATFIX performs various miscellaneous corrections appropriate for
ATCA data. These corrections are typically more important for
data produced by the 12- and 3-mm systems. These systems still contain
imperfections that need to be fixed off-line. Steps that can be
performed are
.nf
  Correct for atmospheric opacity
  Apply gain/elevation effect
  Apply system temperature measurements
  Correct for incorrect baseline length
  Correct antenna table (when some antennas were off-line)
.fi
.PP
NOTE: This task will usually be used very early in the data reduction
process. It should be used before calibration. 
.SH PARAMETERS
.TP
\fIvis\fP
The names of the input uv data sets. No default.
.TP
\fIselect\fP
Standard visibility data selection. See the help on "select" for
more information. The default is to select all data.
.TP
\fIout\fP
The name of the output uv data set. No default.
.TP
\fIarray\fP
One of the flaws in the current ATCA datafiles is that antenna locations
are not recorded for antennas that are off-line (and hence not producing
data). While this might not seem a serious flaw, the off-line antennas
can still cause shadowing. This will be an issue when using the
3-antenna system in compact arrays.
.PP
By giving a value to this parameter, atfix will fill in any
antenna locations that are missing from the input visibility file.
NOTE: This just fills in missing antenna locations, it does not perform
any flagging of shadowed data.
.PP
The value given to this parameter is either an array configuration name
(e.g. EW352 or 750A) or a list of six station names (e.g. W106 or N3).
When giving the station names, these must be in the order of the antennas
(i.e. CA01, CA02, CA03 etc).
.PP
NOTE: When antennas are in a shuffled order, or for arrays using
the north spur, you should generally give the list of station
names, as the standard array configuration names assume the
standard antenna order (not the shuffled order).
.PP
If in doubt, see the on-line history of configurations:
.nf
  http://www.narrabri.atnf.csiro.au/operations/array_configurations/config_hist.
.TP
\fIdantpos\fP
.fi
For poorly understood reasons, the effective locations of the antennas
appear to be a function of frequency. Currently the on-line system uses
the antenna locations derived from centimetre wavelength observations.
If millimetre wavelength solutions for the baseline lengths are available,
you will want to correct your data to account for these.
.PP
The inputs are the equatorial coordinate offsets entered in the
following order (NO checking is done for consistency):
.nf
    dantpos = A1,X1,Y1,Z1,A2,X2,Y2,Z2,A3,X3,Y3,Z3,....
.fi
The input values are the antenna number and the three equatorial
coordinate offsets (entered in units of nanoseconds).  These input
values are added to the absolute coordinates read from the data.
Antenna present in the data but not included in the input value
list are treated as having a zero coordinate offset.
.PP
The arcane unit of nanoseconds is used for historical compatibililty.
Note 1 nanosec = 0.2997 meters.
.PP
A collection of parameter files giving the corrections to apply are
stored in $MIRCAT. These have names of the form "dantpos.yymmdd"
where "yymmdd" is the date of the start of a new array configuration.
If a data file is present, you can instruct atfix to read this
directly using the indirect parameter input. For example, to read
parameters appropriate for a hypothetical array configuration
starting on 16 October 2002, use
.nf
    dantpos=@$MIRCAT/dantpos.021016
.TP
\fIoptions\fP
.fi
Extra processing options. Several options can be given,
separated by commas. Minimum match is supported. Possible values
are:
.nf
  opcorr    Apply a correction to account for the opacity of the
            atmosphere. This option should generally be used
            at 12mm only. For observations taken before October 2003,
            you will need to set the ``mdata'' parameter (see below)
            when using this option.
  tsys      Ensure the system temperature correction is applied to
            the data. It is not uncommon when observing at 3mm to
            not apply the system temperature correction to the data
            on-line.
  gainel    This applies a instrumental gain/elevation correction
            to the data. Currently the gains of the antennas are
            a function of elevation.
.TP
\fImdata\fP
.fi
To apply an opacity correction, this task needs to estimate the
atmospheric opacity based on meteorological conditions. Prior to
October 2003, meteorological data were not saved in the datasets, and
so the meterological data needs to be provided separately.
.PP
This input parameter gives a data file containing the meteorological
conditions at the observatory. It is only required if the dataset
does not already contain this information. 
.PP
Given a model of the atmosphere, and knowing
the elevation of the observation, a model correction for the atmosphere
can be deduced and applied. Note that this is just that - a model
correction - which will have limitations.
.PP
NOTE: This correction should NOT be used with 3-mm data when the 3mm
system temperature corrections are used. The 3-mm system temperatures
are, by their nature, so-called "above atmosphere" system temperature
value, which corrects for atmospheric opacity.
.PP
This parameter can be used with the 12-mm system, where the system
temperatures are those measured at the ground.
.PP
For more help on getting the meteorological data appropriate to your
observation, see the help on "weatherdata".
