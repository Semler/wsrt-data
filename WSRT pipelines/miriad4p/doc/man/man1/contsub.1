.TH contsub 1
.SH NAME
contsub - Subtract continuum from a datacube
.SH PERSON RESPONSIBLE
bpw
.SH CATEGORIES
map manipulation
.SH DESCRIPTION
CONTSUB is used to subtract the continuum from a datacube, which
can be arbitrarily oriented. Several subtraction algorithms are
possible, selected using the mode keyword. The maximum length of
the frequency axis is 8192 pixels.
.SH PARAMETERS
.TP
\fIin\fP
This is the standard name for an input dataset -- usually
an image, though sometimes either an image or visibility dataset
or some foreign dataset format. Some tasks support multiple
input datasets at a time (with wildcard supported) whereas
others can handle only a single one at a time. There is
generally no default input name.
.TP
\fIregion\fP
This selects a subregion of an image. Multiple subregions can be
selected, which are "ored" together. The following subcommands are
recognized (each of which can be abbreviated to uniqueness).
.PP
.nf
  images(z1,z2)
.fi
Select image planes z1 to z2 inclusive. z2 defaults to z1.
.PP
.nf
  quarter(z1,z2)
.fi
Select the inner quarter of the image planes z1 to z2 inclusive.
If both z1 and z2 are missing, then all planes are selected. If
only z2 is omitted, z2 defaults to z1.
.PP
.nf
  boxes(xmin,ymin,xmax,ymax)(z1,z2)
.fi
Select the pixels within a box with corners xmin,ymin,xmax,ymax.
z1 and z2 are the same as in the "image" subcommand. If z1 and z2
are omitted, all planes are selected.
.PP
.nf
  polygon(x0,y0,x1,y1,x2,y2,...)(z1,z2)
.fi
Select the pixels within the polygon defined by the list of vertices.
z1 and z2 are the same as in the "image" subcommand. If z1 and z2 are
missing, all planes are selected. If only z2 is omitted, it defaults
to z1.
.PP
.nf
  mask(file)
.fi
Select  pixels according to the mask given in the file.
.PP
The units of the numbers given in the above commands are, in
general, absolute pixels. But this can be changed (and rechanged)
by using one of the following subcommands.
.PP
.nf
  abspixel
.fi
Coordinates are interpreted as absolute pixel values, the default.
.nf
  relpixel
.fi
Coordinates are relative to the reference pixel of the map.
.nf
  relcenter
.fi
Coordinates are relative to the central pixel of the map,
(defined as (naxis1/2+1,naxis2/2+1)).
.nf
  arcsec
.fi
Coordinates are in arcseconds, relative to the reference pixel.
.nf
  kms
.fi
Coordinates in the third dimension are in km/s.
Anything outside the region is untouched.
(for the moment only rectangular boxes are recognized by contsub).
.TP
\fIout\fP
The name of the output dataset, either image or visibility.
There is generally no default (although some tasks simply
do not produce an output if this is not set).
For modes poly, mean and avgs either this keyword or cont= must be
specified, for mode subtr it must always be given. This output
dataset will contain only those channels from the input dataset
selected by the 'region' keyword.
.TP
\fIcont\fP
An optional output dataset which will be one plane deep and contain
the continuum map. For mode=poly, this is the interpolated map at the
channel halfway between the first and last line channel selected. For
mode=mean, it is the averaged image that was subtracted from the
channel maps. For mode=avgs it is the average of the two average
images that are calculated (see description of mode keyword). For
mode=subtr this is an input set, which will be subtracted from each
plane of the input dataset. For modes poly, mean and avgs either this
keyword or out= must be specified, while for mode subtr it must always
be given.
.TP
\fIcontchan\fP
Select channels that supposedly contain continuum signal only. A
selection is a list of ranges. A range is specified as (z1,z2),
meaning channels z1 through z2, or as (z3) for a single channel.
Ranges are separated by commas. E.g.: (z1,z2),(z3),(z4,z5).
For options 'poly' (or 'mean'), the fit (or average), is done
using all selected continuum channels. For option 'avgs', the
selection of continuum channels must consist of two ranges.
The 'contchan' keyword always refers to frequency channels,
independent of whether the frequency axis is the 'z'-axis, the
'x'-axis or any other axis.
(Not used for mode='subtr').
.TP
\fImode\fP
Selects the continuum-determination algorithm. In each case the
operation is done separately for each image pixel. May be
abbreviated. The default is 'poly,1'.
.PP
'poly,#' - a polynomial fit of order # is made through the selected
continuum channels. The interpolated continuum intensity is
subtracted from each separate channels. The maximum order is 8.
.PP
'mean' - the average intensity of the selected continuum channels is
calculated and the average is subtracted from each channel.
(equivalent to 'poly,0').
.PP
'avgs' - an average of the first range and of the second range of
channels is calculated and a linear interpolation between the two
averages is subtracted from each channel.
.PP
'subtr' - at each position, subtract the value in the dataset given
by cont= from the profile at the same position in the input dataset.
.TP
\fIoptions\fP
Select which output to write with the cont keyword. By default the
continuum is written. If options=coeff,# one of the coefficients of
the polynomial fit is written instead. '#' gives the order for which
the coefficient is written (e.g. if cont=a+bx: 0='a', 1='b', etc).
.TP
\fIverbose\fP
verbose=true makes contsub print out some info every now and then.
.TP
\fIvelaxis\fP
For datacubes where the 'VELO' axis-identification does not occur in
the header, this can be used to indicate which direction is the
velocity (channel) axis. The value given must be one of 'x', 'y',
'z', 'a', 'b', .... If velaxis is not given and the 'VELO' axis is
not present, contsub assumes 'z'.
