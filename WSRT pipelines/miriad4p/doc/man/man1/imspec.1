.TH imspec 1
.SH NAME
imspec - Plots spectra from image data
.SH PERSON RESPONSIBLE
bpw
.SH CATEGORIES
map analysis
.SH DESCRIPTION
Imspec plots spectra. The flux, primary-beam-corrected-flux, mean or
sum of an area can be plotted. Data can be averaged/summed in ra-dec,
ra-vel or dec-vel (etc) planes, to obtain profiles along the vel, dec
or ra axes, respectively. See the description of the keyword axes.
To get fluxes the sum of the beam in an area of the same size as the
input region is calculated, using the beam keyword.
The data can be converted to Kelvin, by using 'options=tb' and the
beam keyword.
Output can be written to the terminal, a log file, or a plot. The 
options keyword gives control over the plot.
To write the spectrum to an ASCII file use options=list,noheader and
log=logfile.
The plotheader can be suppressed by using options=noheader. An
alternative title can be put on the plot by options=title. A useful
combination is 'options=noh,ti,title', to get only the string 'title',
instead of the full header.
.SH PARAMETERS
.TP
\fIin\fP
This is the standard name for an input dataset -- usually
an image, though sometimes either an image or visibility dataset
or some foreign dataset format. Some tasks support multiple
input datasets at a time (with wildcard supported) whereas
others can handle only a single one at a time. There is
generally no default input name.
.TP
\fIregion\fP
This selects a subregion of an image. Multiple subregions can be
selected, which are "ored" together. The following subcommands are
recognized (each of which can be abbreviated to uniqueness).
.PP
.nf
  images(z1,z2)
.fi
Select image planes z1 to z2 inclusive. z2 defaults to z1.
.PP
.nf
  quarter(z1,z2)
.fi
Select the inner quarter of the image planes z1 to z2 inclusive.
If both z1 and z2 are missing, then all planes are selected. If
only z2 is omitted, z2 defaults to z1.
.PP
.nf
  boxes(xmin,ymin,xmax,ymax)(z1,z2)
.fi
Select the pixels within a box with corners xmin,ymin,xmax,ymax.
z1 and z2 are the same as in the "image" subcommand. If z1 and z2
are omitted, all planes are selected.
.PP
.nf
  polygon(x0,y0,x1,y1,x2,y2,...)(z1,z2)
.fi
Select the pixels within the polygon defined by the list of vertices.
z1 and z2 are the same as in the "image" subcommand. If z1 and z2 are
missing, all planes are selected. If only z2 is omitted, it defaults
to z1.
.PP
.nf
  mask(file)
.fi
Select  pixels according to the mask given in the file.
.PP
The units of the numbers given in the above commands are, in
general, absolute pixels. But this can be changed (and rechanged)
by using one of the following subcommands.
.PP
.nf
  abspixel
.fi
Coordinates are interpreted as absolute pixel values, the default.
.nf
  relpixel
.fi
Coordinates are relative to the reference pixel of the map.
.nf
  relcenter
.fi
Coordinates are relative to the central pixel of the map,
(defined as (naxis1/2+1,naxis2/2+1)).
.nf
  arcsec
.fi
Coordinates are in arcseconds, relative to the reference pixel.
.nf
  kms
.fi
Coordinates in the third dimension are in km/s.
For the moment imspec only recognizes rectangular boxes. It will use
the mask associated with the input image.
.TP
\fIplot\fP
This selects what will be plotted as function of e.g. velocity.
To convert data to fluxes the input of the beam keyword is used.
Minimal matching is applied. The default is 'flux'.
.PP
.nf
 'mean'        Plot the mean
 'sum'         Plot the sum
 'flux'        Plot the flux
 'pbcflux'     Plot the primary-beam-corrected flux
               (not yet implemented)
.TP
\fIoptions\fP
.fi
The options control the characteristics of the plot.
Possible options are (minimal matching is done):
.PP
.nf
 'tb'          Convert the units of mean or sum to brightness
               temperature, using the input for the beam keyword
.fi
.PP
.nf
 'hanning,#'   Hanning smooth the data first over # pixels (must be
               an odd number)
 'boxcar,#'    Boxcar smooth the data first over # pixels
 'deriv,#'     Take the derivative after smoothing. If #=1 a one-sided
               derivative is taken, for #=2 a two-sided. Useful for
               Zeeman work.
.fi
.PP
.nf
 'noheader'    Do not write the header information, just the numbers,
               producing an ASCII file for a plotting program
 'list'        Write the spectrum to the screen/logfile
 'eformat'     Always use format 'e' instead of 'g' to write results
 'guaranteespaces' Make sure there is always a space between columns
               (at the cost of precision)
.fi
.PP
.nf
 'xmin,#'      Give lower x-value on axis
 'xmax,#'      Give upper x-value on axis
 'ymin,#'      Give lower y-value on axis
 'ymax,#'      Give upper y-value on axis
               (for these four options the default is autoscaling)
 'title,#1,#2,#3' Put the string #1 at x-position #2 and y-position #3,
               with positions measured in units of the coordinates
               on the axes. If 'title' is the last option, the title
               is put in the upper left hand corner.
 'style,#'     This selects the plot style.
               #=connect means connect the datapoints
               #=step means make one-bin wide connected horizontal
               line segments
               #=histo means bins are drawn as a horizontal line
               surrounded by two vertical lines
.TP
\fIcutoff\fP
.fi
All datavalues below the cutoff are not used for the calculation of
statistics. Give one real value. This may be followed by the string
',abs' to get a cutoff in the absolute value of the datavalues, or
',lower' to exclude all values above the cutoff.
Default is no cutoff.
.TP
\fIbeam\fP
If plot=flux is used, imspec calculates the sum divided by the sum
of the beam to get the flux in the selected area, if the units of the
input data are 'per beam'.
If the name of a dataset is given, it assumes this is a beampattern
and sums the data in a region of the same size as the input region.
Else, it assumes that 'beam' gives the major and minor axes of the
beam in arcsec and it calculates the sum of a gaussian beam of that
size.
If 'beam' is omitted, but 'flux' was selected, the beam is found from
the header (items bmaj and bmin). If neither is present, the sum is
assumed to be 1.  
.TP
\fIaxes\fP
This keyword gives the axis or axes along which data are averaged
to obtain one datapoint on the profile. Combined with the region
keyword, this can be used to get a profile as function of any
coordinate. The specifications are admittedly complex, because data
averaging is allowed.
.PP
- Example 1: to get a profile along the velocity axis (a spectrum) use
.nf
  axes=ra,dec, region=relpix,box(-31,-31,32,32)(10,40)
.fi
to average in ra from -31 to 32, in dec from -31 to 32 and plot the
average as function of velocity from channel 10 to 40.
- Example 2: to get a profile along the ra axis (at a given
declination) use
.nf
  axes=dec,vel, region=relpix,box(-31,0,32,0)(10)
.fi
to plot a profile along ra from ra=-31 to ra=32, at dec 0 and in
plane 10.
- Example 3: to get a set of profiles along the ra axis (at a number
of declinations) use
.nf
  axes=vel, region=relpix,box(-31,-31,32,32)(10)
.fi
to plot a profile along ra, for plane 10, one for each declination
between -31 and 32.
- Example 4: to get a profile along the declination axis (at a given
ra) use
.nf
  axes=ra,vel, region=relpix,box(-10,-31,10,32)(10)
.fi
to plot a profile along declination, with ra averaged from ra=-10 to
ra=10, and in plane 10.
.PP
The default is to make a spectrum in the velocity direction (example
1).
Possible values for axes are: 'rascension', 'declination',
'longitude', 'latitude', 'glongitude', 'glatitude', 'velocity',
'frequency', 'channel', 'stokes', 'x', 'y', 'z', 'a', 'b'. Upper
case and capitalized versions and the string 'R.A.' are also
recognized. Minimal matching is applied. One or two axes may be
given.
.TP
\fIdevice\fP
This keyword is used to specify the plotting device. It uses
the normal PGPLOT format of
.PP
.nf
  file/type
.fi
.PP
.nf
 or
.fi
.PP
.nf
  device/type
.fi
.PP
Usually there exists a default file name or device name.
The part after the slash gives the plotter type. Examples of the
type are:
.PP
.nf
  xs    X-windows output. This window is resizeable and
        persistent. 
  xd    X-windows output. A transient window that disappears
        when your task exists.
  ps    Postscript (landscape mode).
  vps   Postscript (portrait mode).
  cps   Colour postscript (landscape mode).
  vcps  Colour postscript (portrait mode).
  tek   Tektronix 4010 or equivalent.
  xterm Xterm window (better for simple plots over
        slow X links).
.fi
.PP
A `?' as device name generates a complete list of the recognized
types (which will most likely vary from system to system).
.PP
Files created for hardcopy devices must be spooled to the printer
separately. The command needed for this is site and device specific.
.TP
\fIlog\fP
If specified, output is written to the file given by log= instead
of to the terminal.
