# MAB_QT
#
# Checks for QT library
# ------------------------
# lofar_QT
#
# Macro to check for QT installation
#
# lofar_QT(option)
#     option 0 means that QT++ is optional, otherwise mandatory.
#
# e.g. lofar_QT(1)
# -------------------------
#
AC_DEFUN([MAB_QT],[
AC_PREREQ(2.13)
##
## Look for header file in suggested locations or in its include subdir
##
  qt_inclist="$HOME/usr/qt3 $HOME/usr/lib/qt3 /usr/local/qt3 /usr/local/lib/qt3  /usr/qt3 /usr/lib/qt3  $HOME/usr $HOME/usr/lib /usr /usr/lib  /usr/local /usr/local/lib";

  with_qt_libdir=
  for qtdir in $qt_inclist;
  do
   AC_CHECK_FILE([$qtdir/include/qt3/qprogressbar.h],
			[mab_header_qt=$qtdir/include/qt3],
			[mab_header_qt=no])
    if test "$mab_header_qt" != "no" ; then
      if test "$with_qt_libdir" = ""; then
        with_qt_libdir=$qtdir/lib;
        with_qt_bindir=$qtdir/bin;
        break;
      fi
    fi
  done

  if test "$with_qt_libdir" != ""; then
    AC_CHECK_FILE([$with_qt_libdir/libqt-mt.la],
			[mab_lib_qt=$with_qt_libdir],
			[mab_lib_qt=no])
  fi

  if test "$mab_header_qt" != "no"  -a  "$mab_lib_qt" != "no" ; then
    QT_CPPFLAGS="-I$mab_header_qt"
    QT_CXXFLAGS="-Wno-unused -Wall"

    QT_LDFLAGS="-L$mab_lib_qt"
    QT_LIBS="-lqt-mt"

    QT_MOC=$with_qt_bindir/moc

    CPPFLAGS="$CPPFLAGS $QT_CPPFLAGS"
    CXXFLAGS="$CXXFLAGS $QT_CXXFLAGS"
    LDFLAGS="$LDFLAGS $QT_LDFLAGS"
    LIBS="$LIBS $QT_LIBS"

    AC_SUBST(CPPFLAGS)
    AC_SUBST(CXXFLAGS)
    AC_SUBST(LDFLAGS)
    AC_SUBST(LIBS)
    AC_SUBST(QT_MOC)

    AC_DEFINE(HAVE_QT, 1, [Define if QT is installed])
  else
    AC_MSG_ERROR([Could not find QT headers or library in $qt_prefix])
    enable_qt=no
  fi
AM_CONDITIONAL(HAVE_QT, [test "$enable_qt" = "yes"])
])
# End of MAB_QT

