# MAB_AIPSPP
#
# Checks for Aips++ via the AIPSPATH evironment variable
#-------------------------------------------------------
AC_DEFUN([MAB_AIPSPP],[
AC_MSG_CHECKING([whether  AIPSPATH environment variable is set])
if test "${AIPSPATH+set}" != "set"; then
   AC_MSG_RESULT([no])
   AC_MSG_ERROR([AIPSPATH not set. Aips++ not found])
else
   AC_MSG_RESULT([yes])
   AIPSPP_PATH=`expr "$AIPSPATH" : "\(.*\) .* .* .*"`;
   AIPSPP_ARCH=`expr "$AIPSPATH" : ".* \(.*\) .* .*"`;
fi
AC_CHECK_FILE([$AIPSPP_PATH/include/casacore],
	         [aipspp_inc=yes], [aipspp_inc=no])
AC_CHECK_FILE([$AIPSPP_PATH/lib],
	         [aipspp_lib=yes], [aipspp_lib=no])
if test "$aipspp_lib" = "yes" && test "$aipspp_inc" = "yes"; then
   case `uname -s` in
	   SunOS)  arch=SOLARIS;;
	   Linux)  arch=LINUX;;
	   IRIX32) arch=IRIX;;
	   IRIX64) arch=IRIX;;
	   *)      arch=UNKNOWN;;
   esac
   
#   AIPSPP_CPPFLAGS="-I$AIPSPP_PATH/code/include -DAIPS_$arch -DAIPS_STDLIB -DAIPS_AUTO_STL -DAIPS_NO_TEMPLATE_SRC"
   AIPSPP_CPPFLAGS="-I$AIPSPP_PATH/include/casacore -DAIPS_$arch -DAIPS_STDLIB -DAIPS_AUTO_STL"
   AIPSPP_LDFLAGS="-L$AIPSPP_PATH/lib -L/usr/X11/lib"
   AIPSPP_LIBS="-lcasa_ms -lcasa_measures -lcasa_tables -lcasa_scimath -lcasa_scimath_f -lcasa_casa"
   CPPFLAGS="$CPPFLAGS $AIPSPP_CPPFLAGS"
   LDFLAGS="$LDFLAGS $AIPSPP_LDFLAGS"
   LIBS="$LIBS $AIPSPP_LIBS"

   AC_SUBST(CPPFLAGS)
   AC_SUBST(CXXFLAGS)
   AC_SUBST(LDFLAGS)
   AC_SUBST(LIBS)

   AC_DEFINE([HAVE_AIPSPP], [1], [Define if AIPS++ is installed])
else
   AC_MSG_ERROR([Aips++ not found at $AIPSPP_PATH])
fi
])
# End of MAB_AIPSPP
