#include <uvplot/TimeslotTable.cc>

#include <iostream>
#include <exception>

using namespace std;
using namespace uvplot;

int main(int argc, char* argv[])
{
    bool failed = false;
    TimeslotTable t(0.01);
    t.addTime(10.0);
    t.addTime(10.0);
    t.addTime(10.0);
    t.addTime(10.0);
    t.addTime(10.0);
    t.addTime(20.0);
    t.addTime(20.0);
    t.addTime(20.0);
    t.addTime(20.0);
    t.addTime(20.0);
    t.addTime(20.0);
    t.addTime(20.0004);
    t.addTime(30.0);
    t.addTime(0.0);
    t.addTime(19.99999);
    t.addTime(50.0);
    t.addTime(40.0);
    t.addTime(49.99997);
    t.addTime(50.0);
    t.addTime(50.0);
    t.addTime(50.00003);
    t.addTime(50.0);
    t.addTime(50.0);

    size_t N = t.getNumberOfTimeslots();
    for(size_t i = 0; i < N; i++){
        cout << t.getTime(i) << endl;
    }
    try{
        failed |= N != 6;
        failed |= t.getTimeslot(10.00001) != 1;
        failed |= t.getTimeslot(50.0) != 5;
    }catch(TimeslotNotFound &e){
        failed = true;
        cout << e.what() << endl;;
    }

    bool caught = false;
    try{
        t.getTimeslot(25.0);
    }catch(TimeslotNotFound & e){
        cout << e.what() << endl;
        caught = true;
    }
    failed |= !caught;
    caught = false;
    try{
        t.getTimeslot(7777777.0);
    }catch(TimeslotNotFound & e){
        cout << e.what() << endl;
        caught = true;
    }
    failed |= !caught;

    return failed;
}
