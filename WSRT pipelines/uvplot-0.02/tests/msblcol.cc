#include <uvplot/MSBaselineCollection.h>

#include <iostream>
#include <ctime>


using namespace uvplot;
using namespace std;

int main(int argc, char* argv[])
{
    bool failed = false;
    timespec t;
    t.tv_sec=5;
    t.tv_nsec=0;

    MSBaselineCollection bls("/dop41_0/brentjens/data/abell2256/10403394_S0_T0.MS/");
    bls.readBaselinesMT(3, "DATA");
    
    while(bls.busyReading()){
        cout << "BUSY" << endl;
        nanosleep(&t,0);
    }
    for(int i = 0; i < 16; i++){
        for(int j = 0; j < 16; j++){
            cout.width(5);
            cout << bls.ifrNumber(i,j);
        }
        cout << endl;
    }

    failed |= bls.ifrNumber(13,13) != 104;

    SimpleArray<float,2> re;
    SimpleArray<float,2> im;
    SimpleArray<bool,2>  flag;

    for(int ant2=0; ant2 < 100; ant2++){
        int a2 = ant2%14;
        bls.gridBaselineData(&re, &im, &flag, 3,a2,XX);
    }
    

    return failed;
}
