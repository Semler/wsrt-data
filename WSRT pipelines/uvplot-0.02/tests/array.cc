#ifdef HAVE_CONFIG
#include <config.h>
#endif

#include <uvplot/SimpleArray.h>
#include <vector>
#include <iostream>



int main(int argc, char*argv[])
{
    using namespace uvplot;
    using namespace std;
    
    bool failed = false;
    
    vector<int> shape0(0);
    vector<int> shape1(3);
    vector<int> shape2(3);
    vector<int> shape3(2);
    shape2[0] = 2;  //frames
    shape2[1] = 10; // rows
    shape2[2] = 15; // cols

    shape3[0] = 10;
    shape3[1] = 20;

    SimpleArray<int,0> aint(shape0);
    SimpleArray<int,3> bint(shape1);
    SimpleArray<int,3> cint(shape2);
    SimpleArray<int,3> dint(cint);
    SimpleArray<double,2> eint(shape3);
    SimpleArray<double,2> fint;

    cint = 3;
    dint = 1;
    
    failed |= cint.getData()[37] != 3;
    failed |= dint.getData()[37] != 1;
    failed |= aint.getData() != 0;
    failed |= bint.getData() != 0;
    failed |= cint.getData() == 0;
    failed |= dint.getData() == 0;
    failed |= cint.getData() == dint.getData();

    bool caught=false;
    try{
        eint.reshape(shape2);
    }
    catch(IncompatibleShape){
        cout << "Caught Exception: IncompatibleShape" << endl;
        caught = true;
    }
    failed |= !caught;

    dint(1,9,12) = 15;
    int result = dint(1,9,12);

    failed |= result != 15;
    
    cout << "cint[37] = " << cint.getData()[37] << endl
         << "dint[37] = " << dint.getData()[37] << endl
         << "aint.getData() = " << aint.getData() << endl
         << "bint.getData() = " << bint.getData() << endl       
         << "cint.getData() = " << cint.getData() << endl       
         << "dint.getData() = " << dint.getData() << endl
         << "dint(1,9,12)   = " << dint(1,9,12) << endl;
    return int(failed);
}
