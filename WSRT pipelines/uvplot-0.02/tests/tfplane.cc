#ifdef HAVE_CONFIG
#include <config.h>
#endif

#include <uvplot/TimeFrequencyPlane.h>
#include <vector>
#include <iostream>



int main(int argc, char*argv[])
{
    using namespace uvplot;
    using namespace std;
    
    bool failed = false;
   
    int num_times(1437);
    int num_chan(64);
    TimeFrequencyPlane b14(num_times, num_chan, XX);
    failed |= int(b14.getCorrelationType()) != 9;
    
    {
        vector<fcomplex> spectrum(64);
        spectrum[13] = fcomplex(1.0, 17.0);
        b14.setTimeslotData(150, spectrum);
        const vector<fcomplex>& spect149 = b14.getTimeslotData(149);
        cout << "V(149,13) = " << spect149[13] << endl;
        const vector<fcomplex>& spect150 = b14.getTimeslotData(150);
        cout << "V(150,13) = " << spect150[13] << endl;
        failed |= spectrum[13] != spect150[13];
        failed |= spect150[13] == spect149[13];
    }
    {
        vector<bool> spectrum(64);
        spectrum[13] = true;
        b14.setTimeslotFlags(150, spectrum);
        const vector<bool>& spect149 = b14.getTimeslotFlags(149);
        cout << "F(149,13) = " << spect149[13] << endl;
        const vector<bool>& spect150 = b14.getTimeslotFlags(150);
        cout << "F(150,13) = " << spect150[13] << endl;
        failed |= spectrum[13] != spect150[13];
        failed |= spect150[13] == spect149[13];
    }
    return int(failed);
}
