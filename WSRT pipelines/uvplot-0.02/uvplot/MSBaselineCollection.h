#ifndef UVPLOT_MSBASELINECOLLECTION_H
#define UVPLOT_MSBASELINECOLLECTION_H

#include <uvplot/global.h>

#include <uvplot/AntennaPair.h>
#include <uvplot/Baseline.h>
#include <uvplot/TimeslotTable.h>
#include <uvplot/SimpleArray.h>

#include <string>
#include <vector>

#include <pthread.h>

namespace uvplot{

    class MSBaselineCollection
    {
        public:
            MSBaselineCollection();
            MSBaselineCollection(const std::string& msname);
            ~MSBaselineCollection();

            
            //! Sets itsMeasurementSetName and calls readMetaData()
            void setMeasurementSetName(const std::string& msname);

            //!\returns name of MeasurementSet
            std::string getMeasurementSetName() const;

            std::vector<CorrelationType> getCorrelationTypes() const;

            std::vector<std::string>     getAntennaNames() const;
            
            /*! Fill in itsTimeslotTable and allocate/reshape
             * itsBaselineIndex.
             */
            void readMetaData();


            int  ifrNumber(int ant1, int ant2) const;

            /*! Read all baselines of which antenna is a part*/
            void readBaselines(const int antenna, const std::string& column);

            
            /*! reads baseline data in separate thread. Returns true
             *  if instantiation is already busy reading, false, if no
             *  problems occur and thread is started successfully
             */
            bool readBaselinesMT(const int antenna, const std::string& column);
            static void* startReaderThread(void* This);
            bool busyReading();
            int  percentDoneReading();



            /*! Grids data of baseline and correlation onto real, imag
             * and flag arrays. Resizes the arrays if necessary.
             */
            bool gridBaselineData(SimpleArray<float,2>* real,
                                  SimpleArray<float,2>* imag,
                                  SimpleArray<bool,2>*  flag,
                                  int ant1, int ant2,
                                  CorrelationType corr);
            
        protected:
            std::string                     itsMeasurementSetName;
            TimeslotTable                   itsTimeslotTable;
            std::vector<CorrelationType>    itsCorrelationType;
            size_t                          itsNumberOfAntennae;
            std::vector< std::string>       itsAntennaNames;
            std::vector< std::vector<size_t> > itsAntennaRows;
            std::vector< std::vector<int> > itsBaselineIndex;
            std::vector<Baseline>           itsBaselineData;
            std::vector< std::vector<double> > itsFrequencyTable;
            

            int                             itsAntennaToRead;
            std::string                     itsColumnToRead;
            
            bool                            itsBusyReading;
            pthread_mutex_t                 itsBusyReadingMutex;
            pthread_t                       itsReaderThread;
            pthread_attr_t                  itsThreadAttributes;

            int                             itsPercentDoneReading;
            pthread_mutex_t                 itsPercentDoneReadingMutex;

        private:

            MSBaselineCollection(const MSBaselineCollection& other);
            //! Assignment operator with copy semantics.
            void operator =(const MSBaselineCollection& other);

    };
}

#endif
