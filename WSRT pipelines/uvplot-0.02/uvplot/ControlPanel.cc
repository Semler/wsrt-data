#include <uvplot/ControlPanel.h>

#include <sstream>
#include <cmath>


using namespace std;

namespace uvplot{

    //==========>>>  ControlPanel::ControlPanel  <<<==========

    ControlPanel::ControlPanel(QWidget*    parent,
                               const char* name):
        QWidget(parent,name),
        itsAntenna1(0),
        itsAntenna2(0),
        itsCorrelationType(None),
        itsColumnName("DATA"),
        itsMaximumAmplitude(1.0),
        itsShowFlags(true)
    {
        
        itsColumnEntryName= new QLabel("Column", this);
        itsColumnEntry    = new QComboBox(true,this);

        itsAnt1Name  = new QLabel("Antenna 1:", this);
        itsAnt1Slider= new QSlider(Qt::Horizontal, this);
        itsAnt1Label = new QLabel("", this);
        
        itsLoadButton = new QPushButton("Load",this);

        itsAnt2Name  = new QLabel("Antenna 2:", this);
        itsAnt2Slider= new QSlider(Qt::Horizontal,this);
        itsAnt2Label = new QLabel("", this);
        
        itsCorrSliderName = new QLabel("Corr:", this);
        itsCorrSlider     = new QSlider(Qt::Horizontal,this);
        itsCorrLabel      = new QLabel(this);

        itsMaximumAmplitudeName      = new QLabel("Max. amp.:",this);
        itsMaximumAmplitudeLabel     = new QLabel(this);
        itsLogMaximumAmplitudeSlider = new QSlider(Qt::Horizontal, this);
       
        itsShowFlagsButton = new QPushButton("Show Flags", this);
        itsShowFlagsButton->setToggleButton(true);
        itsShowFlagsButton->setOn(itsShowFlags);

        
        itsProgressBar    = new QProgressBar(this);
        
        
        itsColumnEntry->insertItem("DATA");
        itsColumnEntry->insertItem("MODEL_DATA");
        itsColumnEntry->insertItem("CORRECTED_DATA");
        itsColumnEntry->insertItem("RESIDUAL_DATA");
    
        
        itsVLayout   = new QVBoxLayout(this);
        QHBoxLayout* h3 = new QHBoxLayout(0);
        itsVLayout->addLayout(h3);
        h3->addWidget(itsColumnEntryName,1);
        h3->addWidget(itsColumnEntry,4);
       
        QHBoxLayout *h1 = new QHBoxLayout(0);
        itsVLayout->addLayout(h1);
        h1->addWidget(itsAnt1Name,2);
        h1->addWidget(itsAnt1Label,3);
        itsVLayout->addWidget(itsAnt1Slider);

        itsVLayout->addWidget(itsLoadButton);

        itsVLayout->addSpacing(1);
        
        QHBoxLayout* h2 = new QHBoxLayout(0);
        itsVLayout->addLayout(h2);
        h2->addWidget(itsAnt2Name,2);
        h2->addWidget(itsAnt2Label,3);
        itsVLayout->addWidget(itsAnt2Slider);

        itsVLayout->addSpacing(1);
        
        QHBoxLayout* h4 = new QHBoxLayout(0);
        itsVLayout->addLayout(h4);
        h4->addWidget(itsCorrSliderName,2);
        h4->addWidget(itsCorrLabel,3);
        itsVLayout->addWidget(itsCorrSlider);
        
        itsVLayout->addSpacing(1);

        QHBoxLayout* h5 = new QHBoxLayout(0);
        itsVLayout->addLayout(h5);
        h5->addWidget(itsMaximumAmplitudeName,2);
        h5->addWidget(itsMaximumAmplitudeLabel,3);
        
        itsVLayout->addWidget(itsLogMaximumAmplitudeSlider);

        itsVLayout->addWidget(itsShowFlagsButton);
        
        
        itsVLayout->addStretch();

        itsVLayout->addWidget(itsProgressBar);


        vector<string>          antnames(0);
        vector<CorrelationType> corrs(0);
        
        setMSParameters(antnames, corrs);
       
        QObject::connect(itsColumnEntry, SIGNAL(textChanged(const QString&)),
                         this, SLOT(slotColumnChanged(const QString&)));
        
        QObject::connect(itsColumnEntry, SIGNAL(highlighted(const QString&)),
                         this, SLOT(slotColumnChanged(const QString&)));

        QObject::connect(itsAnt1Slider, SIGNAL(valueChanged(int)),
                         this, SLOT(slotAntenna1Changed(int)));

        QObject::connect(itsAnt2Slider, SIGNAL(valueChanged(int)),
                         this, SLOT(slotAntenna2Changed(int)));
        
        QObject::connect(itsLoadButton, SIGNAL(pressed()),
                         this, SLOT(slotLoadButtonPressed()));

        QObject::connect(itsCorrSlider, SIGNAL(valueChanged(int)),
                         this, SLOT(slotCorrelationTypeChanged(int)));
        
        QObject::connect(itsLogMaximumAmplitudeSlider,SIGNAL(valueChanged(int)),
                         this, SLOT(slotMaximumAmplitudeChanged(int)));
        
        QObject::connect(itsShowFlagsButton,SIGNAL(toggled(bool)),
                         this, SLOT(slotShowFlagsButtonToggled(bool)));
    }


    //==========>>>  ControlPanel::~ControlPanel  <<<==========

    ControlPanel::~ControlPanel()
    {
    }



    //==========>>>  ControlPanel::getAntennaString  <<<==========
    
    string ControlPanel::getAntennaString(int ant_number) const
    {
        string name("");
        
        if(ant_number >= 0 && ant_number < int(itsAntennaNames.size())){
            ostringstream out;
            out.width(2);
            out << ant_number << ": "<<itsAntennaNames[ant_number];
            name = out.str();
        }
        return name;    
    }

    

    //==========>>>  ControlPanel::getAntenna1  <<<==========

    int ControlPanel::getAntenna1() const
    {
        return itsAntenna1;
    }

    
    //==========>>>  ControlPanel::getAntenna2  <<<==========

    int ControlPanel::getAntenna2() const
    {
        return itsAntenna2;
    }




    //==========>>>  ControlPanel::getCorrelationType  <<<==========

    CorrelationType ControlPanel::getCorrelationType() const
    {
        return itsCorrelationType;
    }
    
    
    
    //==========>>>  ControlPanel::getColumnName  <<<==========

    std::string ControlPanel::getColumnName() const
    {
        return itsColumnName;
    }

    //==========>>>  ControlPanel::getMaximumAmplitude  <<<==========

    double ControlPanel::getMaximumAmplitude() const
    {
        return itsMaximumAmplitude;
    }


    //==========>>>  ControlPanel::getShowFlags  <<<==========

    bool ControlPanel::getShowFlags() const
    {
        return itsShowFlags;
    }



    //==========>>>  ControlPanel::setMSParameters  <<<==========

    void ControlPanel::setMSParameters(const vector<string>&          ant_names,
                                       const vector<CorrelationType>& corrs)
    {
        itsAntennaNames     = ant_names;
        itsCorrelationTypes = corrs;
        itsCorrelationTypeNames.resize(corrs.size());
        for(size_t i = 0; i < corrs.size(); i++){
            itsCorrelationTypeNames[i] = CorrelationTypeName(corrs[i]);
        }
        if(ant_names.size() != 0){
            itsAnt1Slider->setRange(0,ant_names.size()-1);
            itsAnt1Slider->setEnabled(true);
            itsAnt2Slider->setRange(0,ant_names.size()-1);
            itsAnt2Slider->setEnabled(true);
        }else{
            itsAnt1Slider->setRange(0,0);
            itsAnt1Slider->setEnabled(false);
            
            itsAnt2Slider->setRange(0,0);
            itsAnt2Slider->setEnabled(false);
        }
        slotAntenna1Changed(itsAnt1Slider->value());
        slotAntenna2Changed(itsAnt2Slider->value());
        
        if(corrs.size() !=0){
            itsCorrSlider->setRange(0,corrs.size()-1);
            itsCorrSlider->setEnabled(true);
        }else{
            itsCorrSlider->setRange(0,0);
            itsCorrSlider->setEnabled(false);
        }
        slotCorrelationTypeChanged(itsCorrSlider->value());

        itsLogMaximumAmplitudeSlider->setRange(-40,40);
        slotMaximumAmplitudeChanged(itsLogMaximumAmplitudeSlider->value());
    }



    //==========>>>  ControlPanel::setProgressTotalSteps  <<<==========

    void ControlPanel::setProgressTotalSteps(int total_steps)
    {
        itsProgressBar->setTotalSteps(total_steps);
    }   

    

    //==========>>>  ControlPanel::setProgress  <<<==========

    void ControlPanel::setProgress(int steps)
    {
        itsProgressBar->setProgress(steps);
    }   

    
    
    //==========>>>  ControlPanel::slotAntenna1Changed  <<<==========

    void ControlPanel::slotAntenna1Changed(int ant1)
    {
        itsAntenna1 = ant1;
        string n = getAntennaString(ant1);
        itsAnt1Label->setText(n);
        emit antenna1Changed(ant1);
    }




    //==========>>>  ControlPanel::slotAntenna2Changed  <<<==========

    void ControlPanel::slotAntenna2Changed(int ant2)
    {
        itsAntenna2 = ant2;
        string n = getAntennaString(ant2);
        itsAnt2Label->setText(n);
        emit antenna2Changed(ant2);
    }




    //==========>>>  ControlPanel::slotLoadButtonPressed  <<<==========

    void ControlPanel::slotLoadButtonPressed()
    {
        emit loadButtonPressed();
    }



    //==========>>>  ControlPanel::slotCorrelationTypeChanged  <<<==========

    void ControlPanel::slotCorrelationTypeChanged(int corr)
    {
        if(corr >= 0 && corr < int(itsCorrelationTypes.size())){
            itsCorrelationType = itsCorrelationTypes[corr];
            itsCorrLabel->setText(itsCorrelationTypeNames[corr]);
        }else{
            itsCorrelationType = None;
            itsCorrLabel->setText(CorrelationTypeName(None));
        }
        emit correlationTypeChanged(itsCorrelationType);
    }


    //==========>>>  ControlPanel::slotColumnChanged  <<<==========

    void ControlPanel::slotColumnChanged(const QString& column)
    {
        itsColumnName = column.latin1();
        emit columnChanged(column.latin1());
    }



    //==========>>>  ControlPanel::slotMaximumAmplitudeChanged  <<<==========

    void ControlPanel::slotMaximumAmplitudeChanged(int log_maxamp)
    {
        double max_amp = pow(10.0, log_maxamp/10.0);
        itsMaximumAmplitude = max_amp; 
        ostringstream msg;
        msg << max_amp;
        itsMaximumAmplitudeLabel->setText(msg.str());
        emit maximumAmplitudeChanged(max_amp);
    }
    




    //==========>>>  ControlPanel::slotShowFlagsButtonToggled  <<<==========

    void ControlPanel::slotShowFlagsButtonToggled(bool on)
    {
        itsShowFlags = on;
        emit showFlagsChanged(on);
    }
}
