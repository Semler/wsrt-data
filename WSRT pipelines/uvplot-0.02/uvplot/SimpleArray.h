#ifndef UVPLOT_SIMPLEARRAY_H
#define UVPLOT_SIMPLEARRAY_H

#include <uvplot/global.h>

#include <vector>
#include <cstring>
#include <cassert>

#if HAVE_BLAS
#include <uvplot/cblas.h>
#endif



namespace uvplot{

    /*!Exception class */
    DECLARE_EXCEPTION_CLASS(IncompatibleShape);

template<class T, int NDIM> class SimpleArray
{
    public:
    /*! Constructor. The shape contains the number of elements along every
     * dimension. The fastest varying axis is the last one.
     */
                SimpleArray();
                SimpleArray(const std::vector<int> &shape);
                SimpleArray(const SimpleArray<T,NDIM>& other);

               ~SimpleArray();

               
    /*! Assignment operator. It COPIES the contents of \param other
     */           
    void        operator=(const SimpleArray<T,NDIM>& other);

    /*! Initialize with value \param scalar
     */
    void        operator=(const T& scalar);

    
    /*! Other arithmatic operators first scalar, then element-wise
     */
    void        operator*=(const T& scalar);
    void        operator/=(const T& scalar);
    void        operator+=(const T& scalar);
    void        operator-=(const T& scalar);
    
    void        operator*=(const SimpleArray<T,NDIM>& other);
    void        operator/=(const SimpleArray<T,NDIM>& other);
    void        operator+=(const SimpleArray<T,NDIM>& other);
    void        operator-=(const SimpleArray<T,NDIM>& other);

    
    /*! get value of element a(1,4,3)get value of element a(1,4,3)
     *  i0 is the fastest varying index
     */
    const T&    operator()(int i0) const;
    const T&    operator()(int i1, int i0) const;
    const T&    operator()(int i2, int i1, int i0) const;
    const T&    operator()(int i3, int i2, int i1, int i0) const;
    const T&    operator()(int i4, int i3, int i2, int i1, int i0) const;
    const T&    operator()(int i5, int i4, int i3, int i2, int i1, int i0) const;

    T&          operator()(int i0);
    T&          operator()(int i1, int i0);
    T&          operator()(int i2, int i1, int i0);
    T&          operator()(int i3, int i2, int i1, int i0);
    T&          operator()(int i4, int i3, int i2, int i1, int i0);
    T&          operator()(int i5, int i4, int i3, int i2, int i1, int i0);
    
    /*!\returns pointer to first element of the data
     */    
    T*          getData();

    /*!\returns pointer to first element of the data
     */    
    const T*    getData() const;

    /*!\returns shape of the array. The highest index element of the
     * array is the dimension of the fastest varying index, for
     * example:
     * {num_planes, num_rows, num_cols}
     */
    std::vector<int> getShape() const;

    /*! Resize array. All present data are lost forever.
     */
    void        reshape(const std::vector<int>& shape);
    
    protected:
        T*               itsData;
        std::vector<int> itsShape;
        unsigned long    itsNumberOfElements;
}; // class SimpleArray
 








template<class T, int NDIM>
SimpleArray<T,NDIM>::SimpleArray()
    :itsData(0),
    itsShape(NDIM),
    itsNumberOfElements(0)
{
    for(unsigned int i = 0; i < NDIM; i++){
        itsShape[i] = 0;
    }
}


template<class T, int NDIM>
SimpleArray<T,NDIM>::SimpleArray(const std::vector<int>&shape)
    :itsData(0),
    itsShape(NDIM),
    itsNumberOfElements(0)
{
    reshape(shape);
}



template<class T, int NDIM>
SimpleArray<T,NDIM>::SimpleArray(const SimpleArray<T,NDIM>&other)
    :itsData(0),
    itsShape(NDIM),
    itsNumberOfElements(0)
{
    *this=other;
}



template<class T, int NDIM>
SimpleArray<T,NDIM>::~SimpleArray()
{
    if(itsData != 0){
        delete[] itsData;
    }
}
    



template<class T, int NDIM> void
SimpleArray<T,NDIM>::operator=(const SimpleArray<T,NDIM>&other)
{
    reshape(other.itsShape);
    memcpy(itsData, other.itsData, other.itsNumberOfElements); 
}



template<class T, int NDIM> void
SimpleArray<T,NDIM>::operator=(const T& scalar)
{
    for(unsigned long i=0; i < itsNumberOfElements; i++){
        itsData[i] = scalar;
    }
}





template<class T, int NDIM> void
SimpleArray<T,NDIM>::reshape(const std::vector<int>& shape)
{
    if(shape.size() != NDIM){
        throw IncompatibleShape("SimpleArray<T,NDIM>::reshape");
    }

    unsigned int old_number_of_elements = itsNumberOfElements;
    itsNumberOfElements = 1;
    for(unsigned int i = 0; i < shape.size(); i++){
        itsNumberOfElements *= shape[i];
    }
    itsShape            = shape;
    if(shape.size() == 0){
        itsNumberOfElements = 0;
        if(itsData != 0){
            delete[] itsData;
            itsData = 0;
        }
    }else{
        if(itsNumberOfElements > 0 && 
           old_number_of_elements != itsNumberOfElements){
            if(itsData != 0){
                delete[] itsData;
                itsData = 0;
            }
            itsData = new T[itsNumberOfElements];
        }
    }
}



template<class T, int NDIM> T*
SimpleArray<T,NDIM>::getData()
{ 
    return itsData;
}


template<class T, int NDIM> const T*
SimpleArray<T,NDIM>::getData() const
{
    return itsData;
}




template<class T, int NDIM> std::vector<int>
SimpleArray<T,NDIM>::getShape() const
{
    return itsShape;
}





//====================>>>  Get methods  <<<====================

template<class T, int NDIM> const T&
SimpleArray<T,NDIM>::operator()(int i0) const
{
    return itsData[i0];
}


template<class T, int NDIM> const T&
SimpleArray<T,NDIM>::operator()(int i1, int i0) const
{
    return itsData[i0 +itsShape[1]*i1];
}



template<class T, int NDIM> const T&
SimpleArray<T,NDIM>::operator()(int i2, int i1, int i0) const
{
    return itsData[i0 + itsShape[2]*(i1+itsShape[1]*i2)];
}



template<class T, int NDIM> const T&
SimpleArray<T,NDIM>::operator()(int i3, int i2, int i1, int i0) const
{
    return itsData[i0+itsShape[3]*(i1+itsShape[2]*(i2+itsShape[1]*i3))];
}



template<class T, int NDIM> const T&
SimpleArray<T,NDIM>::operator()(int i4, int i3, int i2, int i1, int i0) const
{
    return itsData[i0+itsShape[4]*(i1+itsShape[3]*(i2+itsShape[2]*(i3+itsShape[1]*i4)))];
}



template<class T, int NDIM> const T&
SimpleArray<T,NDIM>::operator()(int i5, int i4, int i3, int i2, int i1, int i0) const
{
    return itsData[i0+itsShape[5]*(i1+itsShape[4]*(i2+itsShape[3]*(i3+itsShape[2]*(i4+itsShape[1]*i5))))];
}



//====================>>> Set operators <<<====================

template<class T, int NDIM>  T&
SimpleArray<T,NDIM>::operator()(int i0) 
{
    return itsData[i0];
}


template<class T, int NDIM>  T&
SimpleArray<T,NDIM>::operator()(int i1, int i0) 
{
    return itsData[i0 +itsShape[1]*i1];
}



template<class T, int NDIM>  T&
SimpleArray<T,NDIM>::operator()(int i2, int i1, int i0) 
{
    return itsData[i0 + itsShape[2]*(i1+itsShape[1]*i2)];
}



template<class T, int NDIM>  T&
SimpleArray<T,NDIM>::operator()(int i3, int i2, int i1, int i0) 
{
    return itsData[i0+itsShape[3]*(i1+itsShape[2]*(i2+itsShape[1]*i3))];
}


template<class T, int NDIM>  T&
SimpleArray<T,NDIM>::operator()(int i4, int i3, int i2, int i1, int i0) 
{
    return itsData[i0+itsShape[4]*(i1+itsShape[3]*(i2+itsShape[2]*(i3+itsShape[1]*i4)))];
}



template<class T, int NDIM>  T&
SimpleArray<T,NDIM>::operator()(int i5, int i4, int i3, int i2, int i1, int i0) 
{
    return itsData[i0+itsShape[5]*(i1+itsShape[4]*(i2+itsShape[3]*(i3+itsShape[2]*(i4+itsShape[1]*i5))))];
}




//====================>>>  Arithmetic operators  <<<====================

template<class T, int NDIM> void
SimpleArray<T,NDIM>::operator *=(const T& scalar)
{
    for(unsigned int i=0; i < itsNumberOfElements; i++){
        itsData[i] *= scalar;
    }
}


    
template<class T, int NDIM> void
SimpleArray<T,NDIM>::operator /=(const T& scalar)
{
    for(unsigned int i=0; i < itsNumberOfElements; i++){
        itsData[i] /= scalar;
    }
}



template<class T, int NDIM> void
SimpleArray<T,NDIM>::operator +=(const T& scalar)
{
    for(unsigned int i=0; i < itsNumberOfElements; i++){
        itsData[i] += scalar;
    }
}



template<class T, int NDIM> void
SimpleArray<T,NDIM>::operator -=(const T& scalar)
{
    for(unsigned int i=0; i < itsNumberOfElements; i++){
        itsData[i] -= scalar;
    }
}


// Assert wether arrays are incompatible

template<class T, int NDIM> void
SimpleArray<T,NDIM>::operator *=(const SimpleArray<T,NDIM>& other)
{
    assert(itsNumberOfElements == other.itsNumberOfElements);
    for(unsigned int i=0; i < itsNumberOfElements; i++){
        itsData[i] *= other.itsData[i];
    }
}


    
template<class T, int NDIM> void
SimpleArray<T,NDIM>::operator /=(const SimpleArray<T,NDIM>& other)
{
    assert(itsNumberOfElements == other.itsNumberOfElements);
    for(unsigned int i=0; i < itsNumberOfElements; i++){
        itsData[i] /= other.itsData[i];
    }
}



template<class T, int NDIM> void
SimpleArray<T,NDIM>::operator +=(const SimpleArray<T,NDIM>& other)
{
    assert(itsNumberOfElements == other.itsNumberOfElements);
    for(unsigned int i=0; i < itsNumberOfElements; i++){
        itsData[i] += other.itsData[i];
    }
}



template<class T, int NDIM> void
SimpleArray<T,NDIM>::operator -=(const SimpleArray<T,NDIM>& other)
{
    assert(itsNumberOfElements == other.itsNumberOfElements);
    for(unsigned int i=0; i < itsNumberOfElements; i++){
        itsData[i] -= other.itsData[i];
    }
}






} // Namespace uvplot

#endif
