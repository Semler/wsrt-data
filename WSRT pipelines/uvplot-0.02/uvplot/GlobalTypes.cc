#include <uvplot/GlobalTypes.h>

namespace uvplot{
//==========>>>  CorrelationTypeName  <<<==========
    
    std::string CorrelationTypeName(CorrelationType corr)
    {
        std::string res="";
        switch(corr){
            case None:
                res = "None";
                break;

            case I:
                res = "I";
                break;
                
            case Q:
                res = "Q";
                break;
                
            case U:
                res = "U";
                break;
                
            case V:
                res = "V";
                break;
                
            case RR:
                res = "RR";
                break;
                
            case RL:
                res = "RL";
                break;
                
            case LR:
                res = "LR";
                break;
                
            case LL:
                res = "LL";
                break;
                
            case XX:
                res = "XX";
                break;
                
            case XY:
                res = "XY";
                break;
                
            case YX:
                res = "YX";
                break;
                
            case YY:
                res = "YY";
                break;
                
        }
        return res;
    }
}
