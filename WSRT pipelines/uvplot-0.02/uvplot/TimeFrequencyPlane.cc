#include <uvplot/TimeFrequencyPlane.h>

#include <vector>

using namespace std;

namespace uvplot{

    //=============>>>  TimeFrequencyPlane::TimeFrequencyPlane  <<<=============

    TimeFrequencyPlane::TimeFrequencyPlane(int num_times,
            int num_channels,
            CorrelationType corrtype)
        :itsData(0),
        itsFlags(0),
        itsCorrelationType(corrtype),
        itsNumOfTimeslots(num_times),
        itsNumOfChannels(num_channels)
    {
        reshape(num_times, num_channels);
    }


    //=============>>>  TimeFrequencyPlane::TimeFrequencyPlane  <<<=============

    TimeFrequencyPlane::TimeFrequencyPlane(const TimeFrequencyPlane& other):
        itsData(other.itsData),
        itsFlags(other.itsFlags),
        itsCorrelationType(other.itsCorrelationType),
        itsNumOfTimeslots(other.itsNumOfTimeslots),
        itsNumOfChannels(other.itsNumOfChannels)
    {
    }



    //=============>>>  TimeFrequencyPlane::TimeFrequencyPlane  <<<=============

    TimeFrequencyPlane::~TimeFrequencyPlane()
    {
    }



    //=============>>>  TimeFrequencyPlane::operator =  <<<=============

    void TimeFrequencyPlane::operator=(const TimeFrequencyPlane& other)
    {
        itsData           = other.itsData;
        itsFlags          = other.itsFlags;
        itsNumOfTimeslots = other.itsNumOfTimeslots;
        itsNumOfChannels  = other.itsNumOfChannels;
        itsCorrelationType= other.itsCorrelationType;
    }



    //=============>>>  TimeFrequencyPlane::reshape  <<<=============

    void TimeFrequencyPlane::reshape(unsigned int num_times,
                                     unsigned int num_channels)
    {
        itsData           = vector<vector<fcomplex> >(num_times);
        itsNumOfTimeslots = num_times;
        itsNumOfChannels  = num_channels;
        for(unsigned int i = 0; i < num_times; i++){
            itsData[i] = vector<fcomplex>(num_channels);
        }

        itsFlags          = vector<vector<bool> >(num_times);
        for(unsigned int i = 0; i < num_times; i++){
            itsFlags[i] = vector<bool>(num_channels);
        }
        
    }


    //=============>>>  TimeFrequencyPlane::getCorrelationType  <<<=============

    CorrelationType TimeFrequencyPlane::getCorrelationType() const
    {
        return itsCorrelationType;
    }
    

    //=============>>>  TimeFrequencyPlane::getNumberOfTimeslots  <<<============

    unsigned int TimeFrequencyPlane::getNumberOfTimeslots() const
    {
        return itsNumOfTimeslots;
    }



    //=============>>>  TimeFrequencyPlane::getNumberOfChannels  <<<============

    unsigned int TimeFrequencyPlane::getNumberOfChannels() const
    {
        return itsNumOfChannels;
    }
    



    //=============>>>  TimeFrequencyPlane::setTimeslotData  <<<============

    void TimeFrequencyPlane::setTimeslotData(unsigned int timeslot,
                                             const vector<fcomplex>& data)
    {
        itsData[timeslot] = data;
    }



    //=============>>>  TimeFrequencyPlane::setTimeslotFlags  <<<============

    void TimeFrequencyPlane::setTimeslotFlags(unsigned int timeslot,
                                              const vector<bool>& flags)
    {
        itsFlags[timeslot] = flags;
    }

    
    //=============>>>  TimeFrequencyPlane::getTimeslotData  <<<============

    const std::vector<fcomplex>& 
    TimeFrequencyPlane::getTimeslotData(int timeslot) const
    {
        return itsData[timeslot];
    }

    //=============>>>  TimeFrequencyPlane::getTimeslotData  <<<============

    const std::vector<bool>& 
    TimeFrequencyPlane::getTimeslotFlags(int timeslot) const
    {
        return itsFlags[timeslot];
    }
    
    
} // namespace uvplot
