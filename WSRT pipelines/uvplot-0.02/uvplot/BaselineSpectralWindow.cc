#include <uvplot/global.h>
#include <uvplot/BaselineSpectralWindow.h>
#include <sstream>


namespace uvplot{

using namespace std;
    
    //=========>>>  BaselineSpectralWindow::BaselineSpectralWindow  <<<=========

    BaselineSpectralWindow::BaselineSpectralWindow():
        itsCorrelations(0),
        itsNumCorrelations(0)
    {
    }

    //=========>>>  BaselineSpectralWindow::BaselineSpectralWindow  <<<=========

    BaselineSpectralWindow::BaselineSpectralWindow
        (const BaselineSpectralWindow&other):
        itsCorrelations(other.itsCorrelations),
        itsNumCorrelations(other.itsNumCorrelations)
    {
    }

   
    //=========>>>  BaselineSpectralWindow::~BaselineSpectralWindow  <<<========

    BaselineSpectralWindow::~BaselineSpectralWindow()
    {
    }

 
    //=========>>>  BaselineSpectralWindow::operator =  <<<=========

    void BaselineSpectralWindow::operator= (const BaselineSpectralWindow&other)
        {
            itsNumCorrelations = other.itsNumCorrelations;
            itsCorrelations    = other.itsCorrelations;
        }



    
    //=========>>>  BaselineSpectralWindow::reshape  <<<========
    
    void BaselineSpectralWindow::reshape(const vector<CorrelationType>&pols,
                                         int num_times,
                                         int num_chan)
    {
        itsCorrelations.resize(pols.size());
        for(size_t i=0; i < pols.size(); i++){
           itsCorrelations[i] = TimeFrequencyPlane(num_times,
                                                   num_chan, pols[i]); 
        }
        itsNumCorrelations = pols.size();
    }
    

    

    //=========>>>  BaselineSpectralWindow::addPlane  <<<========
    
    void BaselineSpectralWindow::addPlane(const TimeFrequencyPlane&plane)
    {
        itsCorrelations.push_back(plane);
        itsNumCorrelations++;
    }


    
    //=========>>>  BaselineSpectralWindow::addPlane  <<<========
    
    const TimeFrequencyPlane& BaselineSpectralWindow::getPlane(
            CorrelationType corr) const throw(CorrelationNotPresent)
    {
        unsigned int index(0);
        bool         found(false);
        
        for(unsigned int i = 0; i < itsNumCorrelations; i++){
            if(itsCorrelations[i].getCorrelationType() == corr){
                index = i;
                found = true;
                break;
            }
        }

        if(!found){
            ostringstream msg;
            msg << __FILE__ << ":" << __LINE__ << ": " << CorrelationTypeName(corr) << " not in [";
            for(unsigned int i = 0; i < itsNumCorrelations; i++){
                msg << CorrelationTypeName(itsCorrelations[i].getCorrelationType());
                if( i < itsNumCorrelations-1){
                    msg << ", ";
                }
            }
            msg << "]";
            //throw CorrelationNotPresent(msg.str());
        }
        return itsCorrelations[index];
    }




    //=========>>>  BaselineSpectralWindow::addPlane  <<<========

    TimeFrequencyPlane& BaselineSpectralWindow::getPlane(
            CorrelationType corr) throw(CorrelationNotPresent)
    {
        unsigned int index(0);
        bool         found(false);
        
        for(unsigned int i = 0; i < itsNumCorrelations; i++){
            if(itsCorrelations[i].getCorrelationType() == corr){
                index = i;
                found = true;
                break;
            }
        }

        if(!found){
            ostringstream msg;
            msg << __FILE__ << ":" << __LINE__ << ": " << CorrelationTypeName(corr) << " not in [";
            for(unsigned int i = 0; i < itsNumCorrelations; i++){
                 msg << CorrelationTypeName(itsCorrelations[i].getCorrelationType());
                 if( i < itsNumCorrelations-1){
                     msg << ", ";
                 }
            }
            msg << "]";
            //throw CorrelationNotPresent(msg.str());
        }
        return itsCorrelations[index];
    }



    
}
