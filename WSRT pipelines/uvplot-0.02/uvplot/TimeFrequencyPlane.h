#ifndef UVPLOT_TIMEFREQUENCYPLANE_H
#define UVPLOT_TIMEFREQUENCYPLANE_H

#include <uvplot/global.h>
#include <uvplot/GlobalTypes.h>

#include <vector>

namespace uvplot{


    
class TimeFrequencyPlane{
    public:
         TimeFrequencyPlane(int num_times=0, int num_channels=0,
                            CorrelationType corrtype=None);
         TimeFrequencyPlane(const TimeFrequencyPlane& other);
        ~TimeFrequencyPlane();

        void            operator=(const TimeFrequencyPlane& other);

        
        void            reshape(unsigned int num_times,
                                unsigned int num_channels);
        
        CorrelationType getCorrelationType() const;
        
        unsigned int    getNumberOfTimeslots() const;

        unsigned int    getNumberOfChannels() const;

        void            setTimeslotData(unsigned int timeslot,
                                        const std::vector<fcomplex>& data);
        
        void            setTimeslotFlags(unsigned int timeslot,
                                         const std::vector<bool>& flags);
        
        const std::vector<fcomplex>& getTimeslotData(int timeslot) const;

        const std::vector<bool>&     getTimeslotFlags(int timeslot) const;
        
    protected:
        std::vector<std::vector<fcomplex> > itsData;
        std::vector<std::vector<bool> >     itsFlags;
        CorrelationType                     itsCorrelationType;
        unsigned int                        itsNumOfTimeslots;
        unsigned int                        itsNumOfChannels;
};

}
#endif
