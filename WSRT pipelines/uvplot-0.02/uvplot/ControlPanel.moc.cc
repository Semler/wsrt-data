/****************************************************************************
** uvplot::ControlPanel meta object code from reading C++ file 'ControlPanel.h'
**
** Created: Thu Jun 16 20:40:53 2005
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.3   edited Aug 5 16:40 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "ControlPanel.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *uvplot::ControlPanel::className() const
{
    return "uvplot::ControlPanel";
}

QMetaObject *uvplot::ControlPanel::metaObj = 0;
static QMetaObjectCleanUp cleanUp_uvplot__ControlPanel( "uvplot::ControlPanel", &uvplot::ControlPanel::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString uvplot::ControlPanel::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "uvplot::ControlPanel", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString uvplot::ControlPanel::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "uvplot::ControlPanel", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* uvplot::ControlPanel::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QWidget::staticMetaObject();
    static const QUParameter param_slot_0[] = {
	{ "ant1", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_0 = {"slotAntenna1Changed", 1, param_slot_0 };
    static const QUParameter param_slot_1[] = {
	{ "ant2", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_1 = {"slotAntenna2Changed", 1, param_slot_1 };
    static const QUMethod slot_2 = {"slotLoadButtonPressed", 0, 0 };
    static const QUParameter param_slot_3[] = {
	{ "corr", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_3 = {"slotCorrelationTypeChanged", 1, param_slot_3 };
    static const QUParameter param_slot_4[] = {
	{ "column", &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_4 = {"slotColumnChanged", 1, param_slot_4 };
    static const QUParameter param_slot_5[] = {
	{ "log_maxamp_db", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_5 = {"slotMaximumAmplitudeChanged", 1, param_slot_5 };
    static const QUParameter param_slot_6[] = {
	{ "on", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod slot_6 = {"slotShowFlagsButtonToggled", 1, param_slot_6 };
    static const QMetaData slot_tbl[] = {
	{ "slotAntenna1Changed(int)", &slot_0, QMetaData::Public },
	{ "slotAntenna2Changed(int)", &slot_1, QMetaData::Public },
	{ "slotLoadButtonPressed()", &slot_2, QMetaData::Public },
	{ "slotCorrelationTypeChanged(int)", &slot_3, QMetaData::Public },
	{ "slotColumnChanged(const QString&)", &slot_4, QMetaData::Public },
	{ "slotMaximumAmplitudeChanged(int)", &slot_5, QMetaData::Public },
	{ "slotShowFlagsButtonToggled(bool)", &slot_6, QMetaData::Public }
    };
    static const QUParameter param_signal_0[] = {
	{ "ant1", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod signal_0 = {"antenna1Changed", 1, param_signal_0 };
    static const QUParameter param_signal_1[] = {
	{ "ant2", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod signal_1 = {"antenna2Changed", 1, param_signal_1 };
    static const QUParameter param_signal_2[] = {
	{ "corr", &static_QUType_ptr, "CorrelationType", QUParameter::In }
    };
    static const QUMethod signal_2 = {"correlationTypeChanged", 1, param_signal_2 };
    static const QUParameter param_signal_3[] = {
	{ "column", &static_QUType_ptr, "std::string", QUParameter::In }
    };
    static const QUMethod signal_3 = {"columnChanged", 1, param_signal_3 };
    static const QUMethod signal_4 = {"loadButtonPressed", 0, 0 };
    static const QUParameter param_signal_5[] = {
	{ "max_amp", &static_QUType_double, 0, QUParameter::In }
    };
    static const QUMethod signal_5 = {"maximumAmplitudeChanged", 1, param_signal_5 };
    static const QUParameter param_signal_6[] = {
	{ "show", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod signal_6 = {"showFlagsChanged", 1, param_signal_6 };
    static const QMetaData signal_tbl[] = {
	{ "antenna1Changed(int)", &signal_0, QMetaData::Public },
	{ "antenna2Changed(int)", &signal_1, QMetaData::Public },
	{ "correlationTypeChanged(CorrelationType)", &signal_2, QMetaData::Public },
	{ "columnChanged(const std::string&)", &signal_3, QMetaData::Public },
	{ "loadButtonPressed()", &signal_4, QMetaData::Public },
	{ "maximumAmplitudeChanged(double)", &signal_5, QMetaData::Public },
	{ "showFlagsChanged(bool)", &signal_6, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"uvplot::ControlPanel", parentObject,
	slot_tbl, 7,
	signal_tbl, 7,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_uvplot__ControlPanel.setMetaObject( metaObj );
    return metaObj;
}

void* uvplot::ControlPanel::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "uvplot::ControlPanel" ) )
	return this;
    return QWidget::qt_cast( clname );
}

// SIGNAL antenna1Changed
void uvplot::ControlPanel::antenna1Changed( int t0 )
{
    activate_signal( staticMetaObject()->signalOffset() + 0, t0 );
}

// SIGNAL antenna2Changed
void uvplot::ControlPanel::antenna2Changed( int t0 )
{
    activate_signal( staticMetaObject()->signalOffset() + 1, t0 );
}

#include <qobjectdefs.h>
#include <qsignalslotimp.h>

// SIGNAL correlationTypeChanged
void uvplot::ControlPanel::correlationTypeChanged( CorrelationType t0 )
{
    if ( signalsBlocked() )
	return;
    QConnectionList *clist = receivers( staticMetaObject()->signalOffset() + 2 );
    if ( !clist )
	return;
    QUObject o[2];
    static_QUType_ptr.set(o+1,&t0);
    activate_signal( clist, o );
}

// SIGNAL columnChanged
void uvplot::ControlPanel::columnChanged( const std::string& t0 )
{
    if ( signalsBlocked() )
	return;
    QConnectionList *clist = receivers( staticMetaObject()->signalOffset() + 3 );
    if ( !clist )
	return;
    QUObject o[2];
    static_QUType_ptr.set(o+1,&t0);
    activate_signal( clist, o );
}

// SIGNAL loadButtonPressed
void uvplot::ControlPanel::loadButtonPressed()
{
    activate_signal( staticMetaObject()->signalOffset() + 4 );
}

// SIGNAL maximumAmplitudeChanged
void uvplot::ControlPanel::maximumAmplitudeChanged( double t0 )
{
    activate_signal( staticMetaObject()->signalOffset() + 5, t0 );
}

// SIGNAL showFlagsChanged
void uvplot::ControlPanel::showFlagsChanged( bool t0 )
{
    activate_signal_bool( staticMetaObject()->signalOffset() + 6, t0 );
}

bool uvplot::ControlPanel::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: slotAntenna1Changed((int)static_QUType_int.get(_o+1)); break;
    case 1: slotAntenna2Changed((int)static_QUType_int.get(_o+1)); break;
    case 2: slotLoadButtonPressed(); break;
    case 3: slotCorrelationTypeChanged((int)static_QUType_int.get(_o+1)); break;
    case 4: slotColumnChanged((const QString&)static_QUType_QString.get(_o+1)); break;
    case 5: slotMaximumAmplitudeChanged((int)static_QUType_int.get(_o+1)); break;
    case 6: slotShowFlagsButtonToggled((bool)static_QUType_bool.get(_o+1)); break;
    default:
	return QWidget::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool uvplot::ControlPanel::qt_emit( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->signalOffset() ) {
    case 0: antenna1Changed((int)static_QUType_int.get(_o+1)); break;
    case 1: antenna2Changed((int)static_QUType_int.get(_o+1)); break;
    case 2: correlationTypeChanged((CorrelationType)(*((CorrelationType*)static_QUType_ptr.get(_o+1)))); break;
    case 3: columnChanged((const std::string&)*((const std::string*)static_QUType_ptr.get(_o+1))); break;
    case 4: loadButtonPressed(); break;
    case 5: maximumAmplitudeChanged((double)static_QUType_double.get(_o+1)); break;
    case 6: showFlagsChanged((bool)static_QUType_bool.get(_o+1)); break;
    default:
	return QWidget::qt_emit(_id,_o);
    }
    return TRUE;
}
#ifndef QT_NO_PROPERTIES

bool uvplot::ControlPanel::qt_property( int id, int f, QVariant* v)
{
    return QWidget::qt_property( id, f, v);
}

bool uvplot::ControlPanel::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
