#ifndef UVPLOT_ANTENNAPAIR_H
#define UVPLOT_ANTENNAPAIR_H

namespace uvplot{
    class AntennaPair
    {
        public:
            AntennaPair(unsigned int ant1=0, unsigned int ant2=0);
            AntennaPair(const AntennaPair& other);
            ~AntennaPair();

            void operator =(const AntennaPair &other);

            unsigned int itsAntenna1;
            unsigned int itsAntenna2;
    };
}

#endif
