#include <uvplot/global.h>
#include <uvplot/MainWindow.h>

#include <stdexcept>
#include <sstream>

#include <qapplication.h>
#include <qmessagebox.h>
#include <qfiledialog.h>
#include <qlayout.h>
#include <tables/Tables/TableError.h>

using namespace std;

namespace uvplot{

    //==========>>>  MainWindow::MainWindow  <<<==========

    MainWindow::MainWindow(QWidget *parent, const char * name, WFlags f):
        QMainWindow(parent, name, f),
        itsBaselines(),
        itsCurrentAntenna1(0),
        itsMenuBar(0),
        itsFileMenu(0)
    {
        itsRealArray = new SimpleArray<float,2>;
        itsImagArray = new SimpleArray<float,2>;
        itsFlagArray = new SimpleArray<bool,2>;
        
        setCaption("Uvplot - nothing loaded");
        
        createMenu();

        QWidget* CentralWidget = new QWidget(this);
         
        itsControlPanel = new ControlPanel(CentralWidget);
        itsScrollView   = new QScrollView(CentralWidget);
        itsDisplayArea  = new DisplayArea(itsScrollView->viewport());
        itsScrollView->viewport()->setEraseColor(Qt::white);
        itsScrollView->addChild(itsDisplayArea);
        QHBoxLayout* hl = new QHBoxLayout(CentralWidget);
            
        hl->addWidget(itsScrollView, 5);
        hl->addWidget(itsControlPanel, 1);
            
        setCentralWidget(CentralWidget);

        QObject::connect(itsControlPanel, SIGNAL(loadButtonPressed()),
                         this, SLOT(slotLoadBaselineData()));        
        QObject::connect(itsControlPanel, SIGNAL(antenna2Changed(int)),
                         this, SLOT(slotGridAndRedraw()));        
        QObject::connect(itsControlPanel, SIGNAL(correlationTypeChanged(CorrelationType)),
                         this, SLOT(slotGridAndRedraw()));        
        
        QObject::connect(itsControlPanel, SIGNAL(maximumAmplitudeChanged(double)),
                         itsDisplayArea, SLOT(setMaximumAmplitude(double)));
        
        QObject::connect(itsControlPanel, SIGNAL(showFlagsChanged(bool)),
                         itsDisplayArea, SLOT(setShowFlags(bool)));
    }



    //==========>>>  MainWindow::~MainWindow  <<<==========

    MainWindow::~MainWindow()
    {
        delete itsRealArray;
        delete itsImagArray;
        delete itsFlagArray;
    }


    //==========>>>  MainWindow::createMenu  <<<==========

    void MainWindow::createMenu()
    {
        if(itsMenuBar == 0 && itsFileMenu == 0){
            itsFileMenu = new QPopupMenu;
            itsFileMenu->insertItem("&Open", this, SLOT(slotFileOpen()));
            itsFileMenu->insertItem("&Quit", qApp, SLOT(quit()));

            itsMenuBar = new QMenuBar(this);
            itsMenuBar->insertItem("&File", itsFileMenu);
        }else{
            throw runtime_error("MainWindow::createMenu(): one of menu objects is non-NULL. Called multiple times?");
        }
    }


    //==========>>>  MainWindow::slotFileOpen  <<<==========

    void MainWindow::slotFileOpen()
    {
        QString qdirname = QFileDialog::getExistingDirectory(QString::null,this, 0, "Select MeasurementSet");
        if(qdirname != QString::null){
            string dirname(qdirname.latin1());
            try{
                itsBaselines.setMeasurementSetName(dirname);
                vector<string>          ant_names= itsBaselines.getAntennaNames();
                vector<CorrelationType> corrs= itsBaselines.getCorrelationTypes();
                itsControlPanel->setMSParameters(ant_names,
                                                 corrs);
                ostringstream caption;
                caption << "Uvplot - " << dirname;
                setCaption(caption.str());
            }catch(casa::AipsError &e){
                ostringstream out;
                out << "Error opening table " << dirname << endl << e.what();
                QMessageBox::critical(this, "Uvplot error", out.str());
            }
        }
    }


    //==========>>>  MainWindow::slotLoadBaselineData  <<<==========
    
    void MainWindow::slotLoadBaselineData()
    {
        cout << itsControlPanel->getColumnName() << endl;
        bool busy = itsBaselines.readBaselinesMT(itsControlPanel->getAntenna1(), itsControlPanel->getColumnName());
        if(!busy){
            itsCurrentAntenna1 = itsControlPanel->getAntenna1();
            timespec sleeptime;
            sleeptime.tv_sec=0;
            sleeptime.tv_nsec=40000000;
            itsControlPanel->setProgressTotalSteps(100);
            itsControlPanel->setProgress(0);
            while(itsBaselines.busyReading()){
                int percent = itsBaselines.percentDoneReading();
                itsControlPanel->setProgress(percent);
                qApp->processEvents();
            }
            itsControlPanel->setProgress(0);
            slotGridAndRedraw();
        }else{
            QMessageBox::critical(this, "Uvplot error", "Already busy reading a MeasurementSet");
        }
    }


    //==========>>>  MainWindow::slotGridAndRedraw  <<<==========
    
    void MainWindow::slotGridAndRedraw()
    {
        int ant1 = itsControlPanel->getAntenna1();
        int ant2 = itsControlPanel->getAntenna2();
        CorrelationType corr = itsControlPanel->getCorrelationType();
        if(corr != None && itsBaselines.getMeasurementSetName() != ""){
            itsBaselines.gridBaselineData(itsRealArray,
                    itsImagArray,
                    itsFlagArray,
                    ant1, ant2, corr);
            itsDisplayArea->setData(itsRealArray,
                                    itsImagArray,
                                    itsFlagArray);
            
        }
    }
}


