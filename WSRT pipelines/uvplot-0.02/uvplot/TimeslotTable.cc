#include <uvplot/TimeslotTable.h>
#include <cmath>
#include <sstream>

#include <iostream>
using namespace std;

namespace uvplot{

    using namespace std;
    
    //==========>>>  TimeslotTable::TimeslotTable  <<<==========

    TimeslotTable::TimeslotTable(double epsilon):
        itsTimeslots(0),
        itsEpsilon(epsilon)
    {
    }


    //==========>>>  TimeslotTable::TimeslotTable  <<<==========

    TimeslotTable::TimeslotTable(const TimeslotTable& other):
        itsTimeslots(other.itsTimeslots),
        itsEpsilon(other.itsEpsilon)
    {
    }

    
    //==========>>>  TimeslotTable::~TimeslotTable  <<<==========

    TimeslotTable::~TimeslotTable()
    {
    }


    

    //==========>>>  TimeslotTable::operator=  <<<==========

    void TimeslotTable::operator=(const TimeslotTable& other)
    {
        itsTimeslots = other.itsTimeslots;
        itsEpsilon   = other.itsEpsilon;
    }



    //==========>>>  TimeslotTable::clear  <<<==========

    void TimeslotTable::clear()
    {
        itsTimeslots.clear();
    }
    
    //==========>>>  TimeslotTable::addTime  <<<==========

    void TimeslotTable::addTime(double time)
    {
        const size_t N(itsTimeslots.size());

        if(N == 0 || itsTimeslots[N-1] + itsEpsilon < time){
            itsTimeslots.push_back(time);
            //cout << "back: " << endl;
        }else{
            // Search from back to front for existence of timeslot.
            // If new: add timeslot at appropriate location.
            vector<double>::reverse_iterator i;
            for(i = itsTimeslots.rbegin(); i != itsTimeslots.rend(); i++){
                if(time > *i+itsEpsilon){
                    if(fabs(*(--i) -time) >= itsEpsilon){
                        vector<double>::iterator pos(i.base());
                        pos--;
              //          cout << "inner: " << endl;
                        itsTimeslots.insert(pos,time);
                    }
                    break;
                } 
            }// for
            if(i == itsTimeslots.rend() && itsTimeslots[0] -time >= itsEpsilon){
                //cout << "beginning: " << endl;
                itsTimeslots.insert(itsTimeslots.begin(),time);
            }
        } // if else
        
    }


    //==========>>>  TimeslotTable::getTime  <<<==========

    double TimeslotTable::getTime(size_t timeslot) const
    {
        return itsTimeslots[timeslot];
    }

    
    
    
    //==========>>>  TimeslotTable::getTimeslot  <<<==========

    size_t TimeslotTable::getTimeslot(double time) const throw(TimeslotNotFound)
    {
        const size_t N(itsTimeslots.size());
        size_t range(N);
        size_t first = 0;
        size_t last  = N;
        size_t midpoint=N/2;
        
        // Early error handling
        if( N == 0 || time < itsTimeslots[0]-itsEpsilon ||
                time > itsTimeslots[N-1]+itsEpsilon){
            ostringstream message;
            message << __FILE__ << ":" <<__LINE__<< ": Time ("
                    << time << ") not in timeslot table:" 
                    <<" N = " << N << " time = " << time;
            throw TimeslotNotFound(message.str());
        }
        
        // Do a Binary search
        while(range > 0){
            size_t range_left = range/2;
            midpoint          = first + range_left;
            double current    = itsTimeslots[midpoint];

            if(fabs(current - time) < itsEpsilon){
                return midpoint;
            }else if(time < current){
                last  = midpoint;
            }else{
                first = midpoint+1;
            }
            range = last - first;
        }
        
        // Final error handling
        if(fabs(itsTimeslots[midpoint] -time) > itsEpsilon){
            ostringstream message;
            message << __FILE__ << ":" <<__LINE__<< ": Time ("<< time
                    <<") not in timeslot table:"
                    <<" itsTimeslots[midpoint] = "<< itsTimeslots[midpoint]
                    <<" midpoint = " << midpoint << "/" << N;
            throw TimeslotNotFound(message.str());
        }
        return midpoint;
    }


    //==========>>>  TimeslotTable::getNumberOfTimeslots  <<<==========

    size_t TimeslotTable::getNumberOfTimeslots() const
    {
        return itsTimeslots.size();
    }

    
}
