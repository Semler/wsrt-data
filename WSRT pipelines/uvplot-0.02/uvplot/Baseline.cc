#include <uvplot/global.h>
#include <uvplot/Baseline.h>

namespace uvplot{

using namespace std;

    //==========>>>  Baseline::Baseline  <<<==========
    
    Baseline::Baseline(unsigned int num_spw,
                       unsigned int ant1,
                       unsigned int ant2):
        itsSpectralWindows(num_spw),
        itsAntenna1(ant1),
        itsAntenna2(ant2)
    {
    }

    //==========>>>  Baseline::Baseline  <<<==========
    
    Baseline::Baseline(const Baseline& other):
        itsSpectralWindows(other.itsSpectralWindows),
        itsAntenna1(other.itsAntenna1),
        itsAntenna2(other.itsAntenna2)
    {
    }


    //==========>>>  Baseline::~Baseline  <<<==========
    
    Baseline::~Baseline()
    {
    }





    //==========>>>  Baseline::operator=  <<<==========
    
    void Baseline::operator=(const Baseline& other)
    {
        itsSpectralWindows = other.itsSpectralWindows;
        itsAntenna1 = other.itsAntenna1;
        itsAntenna2 = other.itsAntenna2;
    }


    //==========>>>  Baseline::reshape  <<<==========
    
    void Baseline::reshape(int num_spw,
                           const vector<CorrelationType>& pols,
                           int num_times,
                           int num_chan)
    {
        itsSpectralWindows.resize(num_spw);
        for(int i = 0; i < num_spw; i++){
            itsSpectralWindows[i].reshape(pols, num_times, num_chan);
        }
    }
    
    //==========>>>  Baseline::setSpectralWindows  <<<==========
    
    void Baseline::setSpectralWindows(unsigned int num_spw)
    {
        if(num_spw == 0){
            itsSpectralWindows.clear();
        }else{
            itsSpectralWindows.resize(num_spw);
        }
    }


    //==========>>>  Baseline::setSpectralWindow  <<<==========
    
    void Baseline::setSpectralWindow(unsigned int spw,
                                     const BaselineSpectralWindow& data)
    {
        itsSpectralWindows[spw] = data;
    }



    //==========>>>  Baseline::getSpectralWindow  <<<==========
    
    const BaselineSpectralWindow& Baseline::getSpectralWindow(unsigned int spw) const throw (SpectralWindowNotPresent)
    {
        if(spw >= itsSpectralWindows.size()){
            ostringstream msg;
            msg << __FILE__ << ":" << __LINE__+1<< ": spw " << spw << " not present" << endl;
            throw SpectralWindowNotPresent(msg.str());
        }
        return itsSpectralWindows[spw];
    }
 


    //==========>>>  Baseline::getSpectralWindow  <<<==========
    
    BaselineSpectralWindow& Baseline::getSpectralWindow(unsigned int spw) throw(SpectralWindowNotPresent)
    {
        if(spw >= itsSpectralWindows.size()){
            ostringstream msg;
            msg << __FILE__ << ":" << __LINE__+1<< ": spw " << spw << " not present" << endl;
            throw SpectralWindowNotPresent(msg.str());
        }
        return itsSpectralWindows[spw];
    }
    
    //==========>>>  Baseline::getNumberOfSpectralWindows  <<<==========
    
    size_t Baseline::getNumberOfSpectralWindows() const
    {
        return itsSpectralWindows.size();
    }
    

 
  
    //==========>>>  Baseline::getAntenna1  <<<==========

    unsigned int Baseline::getAntenna1() const
    {
        return itsAntenna1;
    }


    //==========>>>  Baseline::getAntenna2  <<<==========

    unsigned int Baseline::getAntenna2() const
    {
        return itsAntenna2;
    }

    
    //==========>>>  Baseline::hasAntenna  <<<==========
    
    bool Baseline::hasAntenna(unsigned int ant) const
    {
        return (itsAntenna1 == ant) || (itsAntenna2 == ant);
    }


    //==========>>>  Baseline::setAntennas  <<<==========
    
    void Baseline::setAntennas(unsigned int ant1, unsigned int ant2)
    {
        itsAntenna1 = ant1;
        itsAntenna2 = ant2;
    }
}
