#ifndef UVPLOT_CONTROLPANEL_H
#define UVPLOT_CONTROLPANEL_H

#include <uvplot/global.h>
#include <uvplot/GlobalTypes.h>

#include <string>
#include <vector>

#include <qwidget.h>
#include <qslider.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qcombobox.h>
#include <qprogressbar.h>
#include <qlayout.h>

namespace uvplot{
    class ControlPanel:public QWidget
    {
        Q_OBJECT;

        public:
            ControlPanel(QWidget*    parent = 0,
                         const char* name   = 0);
           ~ControlPanel();


           void setMSParameters(const std::vector<std::string>&     ant_names,
                                const std::vector<CorrelationType>& corr);
           
           int             getAntenna1()        const;
           int             getAntenna2()        const;
           CorrelationType getCorrelationType() const;
           std::string     getColumnName()      const;
           double          getMaximumAmplitude()const;
           bool            getShowFlags()       const;


           std::string     getAntennaString(int ant_number) const;
           
           void            setProgressTotalSteps(int total_steps);
           void            setProgress(int steps_done);


       signals:
           void antenna1Changed(int ant1);
           void antenna2Changed(int ant2);
           void correlationTypeChanged(CorrelationType corr);
           void columnChanged(const std::string& column);
           void loadButtonPressed();
           void maximumAmplitudeChanged(double max_amp);
           void showFlagsChanged(bool show);
               
       public slots:
           void slotAntenna1Changed(int ant1);
           void slotAntenna2Changed(int ant2);
           void slotLoadButtonPressed();
           void slotCorrelationTypeChanged(int corr);
           void slotColumnChanged(const QString& column);
           void slotMaximumAmplitudeChanged(int log_maxamp_db);
           void slotShowFlagsButtonToggled(bool on);

       private:
           QVBoxLayout*    itsVLayout; 
          
           QLabel*         itsAnt1Name;
           QSlider*        itsAnt1Slider;
           QLabel*         itsAnt1Label;
          
           QLabel*         itsAnt2Name;
           QSlider*        itsAnt2Slider;
           QLabel*         itsAnt2Label;

           QPushButton*    itsLoadButton;
          
           QLabel*         itsCorrSliderName;
           QSlider*        itsCorrSlider;
           QLabel*         itsCorrLabel; 

           QLabel*         itsColumnEntryName;
           QComboBox*      itsColumnEntry;

           QLabel*         itsMaximumAmplitudeName;
           QLabel*         itsMaximumAmplitudeLabel;
           QSlider*        itsLogMaximumAmplitudeSlider;
           
           QPushButton*    itsShowFlagsButton;
           
           QProgressBar*   itsProgressBar;

           int             itsAntenna1;
           int             itsAntenna2;
           CorrelationType itsCorrelationType;
           std::string     itsColumnName;
           double          itsMaximumAmplitude;
           bool            itsShowFlags;

           std::vector<std::string>     itsAntennaNames;
           std::vector<CorrelationType> itsCorrelationTypes;
           std::vector<std::string>     itsCorrelationTypeNames;
    };
}

#endif
