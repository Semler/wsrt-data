#include <uvplot/global.h>
#include <uvplot/MSBaselineCollection.h>

#include <cassert>
#include <iostream>


#include <casa/Arrays/Vector.h>
#include <casa/Arrays/Array.h>
#include <casa/Arrays/IPosition.h>
#include <tables/Tables/RefRows.h>
#include <ms/MeasurementSets/MeasurementSet.h>
#include <ms/MeasurementSets/MSMainColumns.h>

namespace uvplot{

    //==========>>>  MSBaselineCollection::MSBaselineCollection  <<<==========

    MSBaselineCollection::MSBaselineCollection():
        itsTimeslotTable(0.1),
        itsCorrelationType(0),
        itsNumberOfAntennae(0),
        itsAntennaNames(0),
        itsAntennaRows(0),
        itsBaselineIndex(0),
        itsBaselineData(0),
        itsFrequencyTable(0),
        itsAntennaToRead(0),
        itsColumnToRead("DATA"),
        itsBusyReading(false),
        itsPercentDoneReading(0) 
    {
        pthread_attr_init(&itsThreadAttributes);
        pthread_attr_setdetachstate(&itsThreadAttributes, PTHREAD_CREATE_JOINABLE);
        pthread_mutex_init(&itsBusyReadingMutex,0);
        pthread_mutex_init(&itsPercentDoneReadingMutex,0);
    }



    //==========>>>  MSBaselineCollection::MSBaselineCollection  <<<==========

    MSBaselineCollection::MSBaselineCollection(const std::string& msname):
        itsMeasurementSetName(msname),
        itsTimeslotTable(0.1),
        itsCorrelationType(0),
        itsNumberOfAntennae(0),
        itsAntennaNames(0),
        itsAntennaRows(0),
        itsBaselineIndex(0),
        itsBaselineData(0),
        itsFrequencyTable(0),
        itsAntennaToRead(0),
        itsColumnToRead("DATA"),
        itsBusyReading(false),
        itsPercentDoneReading(0)
    {
        pthread_attr_init(&itsThreadAttributes);
        pthread_attr_setdetachstate(&itsThreadAttributes, PTHREAD_CREATE_JOINABLE);
        pthread_mutex_init(&itsBusyReadingMutex,0);
        pthread_mutex_init(&itsPercentDoneReadingMutex,0);

        readMetaData();
    }
    

    //==========>>>  MSBaselineCollection::MSBaselineCollection  <<<==========

    MSBaselineCollection::MSBaselineCollection(const MSBaselineCollection& other):
        itsMeasurementSetName(other.itsMeasurementSetName),
        itsTimeslotTable(other.itsTimeslotTable),
        itsCorrelationType(other.itsCorrelationType),
        itsNumberOfAntennae(other.itsNumberOfAntennae),
        itsAntennaNames(other.itsAntennaNames),
        itsAntennaRows(other.itsAntennaRows),
        itsBaselineIndex(other.itsBaselineIndex),
        itsBaselineData(other.itsBaselineData),
        itsFrequencyTable(other.itsFrequencyTable)
            
    {
    }
 


    //==========>>>  MSBaselineCollection::~MSBaselineCollection  <<<==========

    MSBaselineCollection::~MSBaselineCollection()
    {
        //Wait for reader thread to finish if it's still busy
        pthread_join(itsReaderThread,0);
        pthread_attr_destroy(&itsThreadAttributes);
        pthread_mutex_destroy(&itsBusyReadingMutex);
        pthread_mutex_destroy(&itsPercentDoneReadingMutex);
    }



    //==========>>>  MSBaselineCollection::operator=  <<<==========

    void MSBaselineCollection::operator=(const MSBaselineCollection& other)
    {
        itsMeasurementSetName = other.itsMeasurementSetName;
        itsTimeslotTable      = other.itsTimeslotTable;
        itsCorrelationType    = other.itsCorrelationType;
        itsNumberOfAntennae   = other.itsNumberOfAntennae;
        itsAntennaNames       = other.itsAntennaNames;
        itsAntennaRows        = other.itsAntennaRows;
        itsBaselineIndex      = other.itsBaselineIndex;
        itsBaselineData       = other.itsBaselineData;
        itsFrequencyTable     = other.itsFrequencyTable;
    }


    
    //==========>>>  MSBaselineCollection::setMeasurementSetName <<<==========

    void MSBaselineCollection::setMeasurementSetName(const string& msname)
    {
        itsMeasurementSetName = msname;
        readMetaData();
    }

    
    //==========>>>  MSBaselineCollection::getMeasurementSetName <<<==========

    string MSBaselineCollection::getMeasurementSetName() const
    {
        return itsMeasurementSetName;
    }


    //==========>>>  MSBaselineCollection::getAntennaNames  <<<==========

    vector<string> MSBaselineCollection::getAntennaNames() const
    {
        return itsAntennaNames;
    }


    //==========>>>  MSBaselineCollection::getCorrelationTypes  <<<==========

    vector<CorrelationType> MSBaselineCollection::getCorrelationTypes() const
    {
        return itsCorrelationType;
    }
    
    
    //==========>>>  MSBaselineCollection::ifrNumber  <<<==========
    
    int MSBaselineCollection::ifrNumber(int ant1, int ant2) const
    {
        size_t ifr(0);
        int N = itsNumberOfAntennae;
        if(ant1 >= N || ant2 >=N || ant1 < 0 || ant2 <0){
            return -1;
        }
        
        if(ant1 >= ant2){
            ifr = N*ant2 + ant1 - (ant2*(ant2+1))/2;
        }else{
            ifr = N*ant1 + ant2 - (ant1*(ant1+1))/2;
        }
        return ifr;
    }
    

    //==========>>>  MSBaselineCollection::readMetaData <<<==========

    void MSBaselineCollection::readMetaData()
    {
        assert(itsMeasurementSetName != "");
        using namespace casa;
        using namespace std;

        MeasurementSet ms(itsMeasurementSetName);

        // Fill timeslot table
        ROMSMainColumns cols(ms);
        Vector<double> time_vector = cols.timeCentroid().getColumn();
        const double  *p_time      = time_vector.data();
        size_t          nrows      = cols.nrow();

        itsTimeslotTable.clear();
        for(size_t i=0; i < nrows; i++){
            itsTimeslotTable.addTime(*p_time++);
        }

        // Antenna table
        itsNumberOfAntennae = ms.antenna().nrow();
        itsAntennaNames.resize(itsNumberOfAntennae);

        size_t num_ant      = itsNumberOfAntennae;
        ROScalarColumn<String> ant_names_col(ms.antenna(), "NAME");
        Vector<String>         ant_names = ant_names_col.getColumn();

        itsBaselineData =  vector<Baseline>((num_ant*(num_ant+1))/2);

        itsBaselineIndex = vector< vector<int> >(num_ant);
        for(size_t i=0; i < num_ant; i++){
            itsBaselineIndex[i] = vector<int>(num_ant);
            itsAntennaNames[i] = string(ant_names(i));
            for(size_t j = 0; j < num_ant; j++){
                itsBaselineIndex[i][j] = ifrNumber(i,j);
                if( j >= i){
                    itsBaselineData[ifrNumber(i,j)].setAntennas(i,j);
                }
            }
        }

        // Correlation type
        ROArrayColumn<int> corr_type_col(ms.polarization(), "CORR_TYPE");
        Array<int>  pols    = corr_type_col.getColumn();
        size_t      num_pol = pols.shape()(0);

        itsCorrelationType.resize(num_pol);
        for(size_t i = 0; i < num_pol; i++){
            itsCorrelationType[i] = CorrelationType(pols(IPosition(2,i,0)));
        }


        // FrequencyTable
        size_t num_spw = ms.spectralWindow().nrow();
        ROArrayColumn<double> chan_freq_col(ms.spectralWindow(), "CHAN_FREQ");
        ROScalarColumn<int>   num_chan_col(ms.spectralWindow(), "NUM_CHAN");
        
        Array<double>         freqs    = chan_freq_col.getColumn();
        const double*         p_freq   = freqs.data();
        Vector<int>           num_chan = num_chan_col.getColumn();
        itsFrequencyTable = vector< vector<double> >(num_spw);
        for(size_t i = 0; i < num_spw; i++){
            size_t num_freq = num_chan(i);
            itsFrequencyTable[i] = vector<double>(num_freq);
            memcpy(&itsFrequencyTable[i][0], p_freq, sizeof(double)*num_freq);
            p_freq += num_freq;
        }
        

        // Rows containing data belonging to antenna i

        itsAntennaRows = vector< vector<size_t> >(num_ant);
        Vector<int> ant1 = cols.antenna1().getColumn();
        Vector<int> ant2 = cols.antenna2().getColumn();
        const int* p_ant1= ant1.data();
        const int* p_ant2= ant2.data();
        for(size_t row = 0; row < nrows; row++){
            itsAntennaRows[*p_ant1++].push_back(row);
            itsAntennaRows[*p_ant2++].push_back(row);
        }
    }



    //==========>>>  MSBaselineCollection::readBaselines <<<==========

    void MSBaselineCollection::readBaselines(const int antenna,
                                             const string& column)
    {
            using namespace casa;
            using namespace std;

            pthread_mutex_lock(&itsPercentDoneReadingMutex);
            itsPercentDoneReading=0; 
            pthread_mutex_unlock(&itsPercentDoneReadingMutex);

        if(itsMeasurementSetName != ""){
            int num_pol   = itsCorrelationType.size();
            int num_times = itsTimeslotTable.getNumberOfTimeslots();
            int num_freq  = itsFrequencyTable[0].size();
            int num_spw   = itsFrequencyTable.size();

            //cout << num_pol << "x" << num_times << "x" << num_freq << "x" << num_spw<<endl;

            for(size_t i =0; i < itsBaselineData.size(); i++){
                if(itsBaselineData[i].hasAntenna(antenna)){
                    itsBaselineData[i].reshape(num_spw, itsCorrelationType,
                            num_times, num_freq);
                //    cout << "reshaping: " << itsBaselineData[i].getAntenna1() << "-" << itsBaselineData[i].getAntenna2() << endl;
                }else{
                    itsBaselineData[i].setSpectralWindows(0);
                }    
            }

            MeasurementSet  ms(itsMeasurementSetName);
            ROMSMainColumns cols(ms);

            size_t         nrows = itsAntennaRows[antenna].size();
            size_t         step  = nrows/100;// MUST BE 100 for %donereading to work
            size_t         start(0);



            if(step == 0){
                step = 1;
            }
            while(start < nrows){
                if(start + step > nrows){
                    step = nrows-start;
                }
               // Vector<size_t> row_vector(IPosition(1,step));
                Vector<unsigned int> row_vector(IPosition(1,step));
                for(size_t i = 0; i < step; i++){
                    row_vector[i] = itsAntennaRows[antenna][start+i];
                }
                RefRows rows(row_vector);

                Vector<int> ant1 = cols.antenna1().getColumnCells(rows);
                Vector<int> ant2 = cols.antenna2().getColumnCells(rows);
                Vector<int> data_desc_id = cols.dataDescId().getColumnCells(rows);
                Vector<double> time = cols.timeCentroid().getColumnCells(rows);
               
                Array<fcomplex> data;
                
                if(column == "RESIDUAL_DATA"){
                    ROArrayColumn<fcomplex> corrected_col(ms, "CORRECTED_DATA");
                    Array<fcomplex> corrected = corrected_col.getColumnCells(rows);
                    
                    ROArrayColumn<fcomplex> model_col(ms, "MODEL_DATA");
                    Array<fcomplex> model = model_col.getColumnCells(rows);
                    data = corrected;
                    fcomplex* p_data = data.data();
                    const fcomplex* p_model = model.data();
                    for(size_t i = 0; i < step*num_pol*num_freq; i++){
                        *p_data++ -= *p_model++;
                    }
                }else{
                    ROArrayColumn<fcomplex > data_col(ms, column);
                    data = data_col.getColumnCells(rows);     
                }
                //Array<fcomplex > data = cols.data().getColumnCells(rows);
                Array<bool>            flag = cols.flag().getColumnCells(rows);

                const int*    p_ant1 = ant1.data();
                const int*    p_ant2 = ant2.data();
                const int*    p_ddid = data_desc_id.data();
                const double* p_time = time.data();

                const fcomplex* p_data = data.data();
                const bool*           p_flag = flag.data();

                vector<fcomplex> data_collected(num_freq);
                vector<bool>            flag_collected(num_freq);
                for(size_t i = 0; i < step; i++){
                    int a1 = *p_ant1++;
                    int a2 = *p_ant2++;
                    size_t baseline_index = itsBaselineIndex[a1][a2];

                    int spw= *p_ddid++;
                    int timeslot = itsTimeslotTable.getTimeslot(*p_time++);
                    for(int j = 0; j < num_pol; j++){
                        const fcomplex* p_data_base = p_data + j;
                        const bool*           p_flag_base = p_flag + j;
                        TimeFrequencyPlane &tfplane = itsBaselineData[baseline_index].getSpectralWindow(spw).getPlane(itsCorrelationType[j]);
                        for(int k = 0; k < num_freq; k++){
                            data_collected[k] = *p_data_base;
                            flag_collected[k] = *p_flag_base;
                            p_data_base += num_pol;
                            p_flag_base += num_pol;
                        }
                        tfplane.setTimeslotData(timeslot, data_collected);
                        tfplane.setTimeslotFlags(timeslot, flag_collected);
                    }
                    p_data += num_pol*num_freq;
                    p_flag += num_pol*num_freq;

                }
                start += step;

                pthread_mutex_lock(&itsPercentDoneReadingMutex);
                itsPercentDoneReading++;
                pthread_mutex_unlock(&itsPercentDoneReadingMutex);
            }

            pthread_mutex_lock(&itsPercentDoneReadingMutex);
            itsPercentDoneReading=0; 
            pthread_mutex_unlock(&itsPercentDoneReadingMutex);
        }
    }



    //==========>>>  MSBaselineCollection::readBaselines  <<<==========
        
    bool MSBaselineCollection::readBaselinesMT(const int antenna,
                                               const string& column)
    {
        bool busy;
        pthread_mutex_lock(&itsBusyReadingMutex);
        busy = itsBusyReading;
        pthread_mutex_unlock(&itsBusyReadingMutex);
                
        if(busy){
            return true;
        }else{
            itsAntennaToRead = antenna;
            itsColumnToRead  = column;
            pthread_mutex_lock(&itsBusyReadingMutex);
            itsBusyReading = true;
            pthread_mutex_unlock(&itsBusyReadingMutex);
            
            pthread_create(&itsReaderThread, 0, startReaderThread, this);

        }
        return false;
    }


    //==========>>>  MSBaselineCollection::startReaderThread  <<<==========
        
    void *MSBaselineCollection::startReaderThread(void* This_p)
    {
        MSBaselineCollection* This = reinterpret_cast<MSBaselineCollection *>(This_p);
        This->readBaselines(This->itsAntennaToRead, This->itsColumnToRead);
        
        pthread_mutex_lock(&This->itsBusyReadingMutex);
        This->itsBusyReading = false;
        pthread_mutex_unlock(&This->itsBusyReadingMutex);
        pthread_exit(0);
        return 0;
    }


    
    //==========>>>  MSBaselineCollection::busyReading  <<<==========

    bool MSBaselineCollection::busyReading()
    {
        bool busy;
        pthread_mutex_lock(&itsBusyReadingMutex);
        busy = itsBusyReading;
        pthread_mutex_unlock(&itsBusyReadingMutex);
        return busy;
    }

    
    //==========>>>  MSBaselineCollection::percentDoneReading  <<<==========

    int MSBaselineCollection::percentDoneReading()
    {
        int done;
        pthread_mutex_lock(&itsPercentDoneReadingMutex);
        done = itsPercentDoneReading;
        pthread_mutex_unlock(&itsPercentDoneReadingMutex);
        return done;
    }


    //==========>>>  MSBaselineCollection::gridBaselineData  <<<==========
    
    bool MSBaselineCollection::gridBaselineData(SimpleArray<float,2>* real,
            SimpleArray<float,2>* imag,
            SimpleArray<bool,2>*  flag,
            int ant1, int ant2,
            CorrelationType corr)
    {
        if(busyReading()){
            return true;
        }
        int num_times   = itsTimeslotTable.getNumberOfTimeslots();
        int num_spw     = itsFrequencyTable.size();
        int spw_spacing = 5; //pixels
        vector<int> spw_offsets(num_spw);
        int current_offset = 0;
        int total_channels = 0;
        for(int i = 0; i < num_spw; i++){
            spw_offsets[i] = current_offset;
            current_offset += itsFrequencyTable[i].size() + spw_spacing;
            total_channels += itsFrequencyTable[i].size(); 
        }
        int width = total_channels+spw_spacing*(num_spw-1);
        vector<int> shape(2);
        shape[0] = num_times;
        shape[1] = width;
        real->reshape(shape);
        imag->reshape(shape);
        flag->reshape(shape);

        *real = 0;
        *imag = 0;
        *flag = false;

        const Baseline* bl = &itsBaselineData[itsBaselineIndex[ant1][ant2]];
        if(bl->getNumberOfSpectralWindows() != size_t(num_spw)){
            return true;
        }
        for(int spw = 0; spw < num_spw; spw++){
            int xstart = spw_offsets[spw];

            const BaselineSpectralWindow* spw_data = & bl->getSpectralWindow(spw);

            const TimeFrequencyPlane* tfplane = & spw_data->getPlane(corr);

            //Iterate over timeslots
            for(int timeslot = 0; timeslot < num_times; timeslot++){
                int num_chan = itsFrequencyTable[spw].size();
                
                const vector<fcomplex>& tf_data = tfplane->getTimeslotData(timeslot);
                const vector<bool>&     tf_flags= tfplane->getTimeslotFlags(timeslot);
                vector<fcomplex>::const_iterator d=tf_data.begin();
                vector<bool>::const_iterator     f=tf_flags.begin();
                    
                long       offset = timeslot*width+xstart;
                float* real_start = real->getData()+offset;
                float* imag_start = imag->getData()+offset;
                bool*  flag_start = flag->getData()+offset;
                
                for(int i = 0; i <num_chan; i++){
                    real_start[i] = d->real();
                    imag_start[i] = d->imag();
                    flag_start[i] = *f++;
                    d++;      
                }
            }

        }
        return false;
    }

    
}
