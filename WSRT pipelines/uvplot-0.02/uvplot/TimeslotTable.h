#ifndef UVPLOT_TIMESLOTTABLE_H
#define UVPLOT_TIMESLOTTABLE_H

#include <uvplot/global.h>
#include <vector>

namespace uvplot{

    
    //! \brief Exception class.
    /*! Thrown if the requested time is not in the timeslot table. As it is
     * derived from std::domain_error, one can get an error message through the
     * TimeslotNotFound::what() method, which returns a string.
    */
    DECLARE_EXCEPTION_CLASS(TimeslotNotFound);
//    class TimeslotNotFound:public std::domain_error{
//        public:
//            /*! \param msg is the message that is returned by the what() method
//             * that can be called from the catch body.
//             */
//            TimeslotNotFound(const std::string& msg):std::domain_error(msg){};
//    };

        
    //! A place to store a timeslot table.
    /*! The class has no knowledge about the source of the timeslot data.
     * Therefore it has to be fed data from outside. It only maintains a sorted
     * array of all unique times that have been supplied to the addTime()
     * method.
     */
    class TimeslotTable
    {
        public:
                         /*! \param epsilon is the accuracy with which
                          * timeslots are matched. If fabs(supplied_time -
                          * stored_time) < epsilon, the supplied_time and
                          * stored_time are considered equal.
                          */
                         TimeslotTable(double epsilon=0.01);

                         //! Copy constructor
                         TimeslotTable(const TimeslotTable& other);

                         //! Destructor
                        ~TimeslotTable();

                        /*! Assignment operator. It makes a deep copy of the
                         * other, so it is fairly slow.
                         */
            void         operator=(const TimeslotTable& other);


            //! Erase itsTimeslots
            void         clear();

            /*! Adds time to itsTimeslots if it is not already there.
             * A timeslot is matched  if fabs(time-stored_time) < itsEpsilon
             * \param time is usually in seconds, but in fact the class does
             * not care at all.
             */
            void         addTime(double time);
            

            /*! \returns the number of the timeslot that matches time. If time
             * is larger than the maximum, smaller than the minimum, or differs
             * more than itsEpsilon from any time in the table, a
             * TimeslotNotFound exception is thrown.
             */
            size_t       getTimeslot(double time) const throw(TimeslotNotFound);

            //!\returns the value stored at timeslot
            double       getTime(size_t timeslot) const;

            /*! \returns the total number of unique times
             */
            size_t       getNumberOfTimeslots() const;
            
        protected:
            std::vector<double> itsTimeslots;
            double              itsEpsilon;
                
    };
}


#endif
