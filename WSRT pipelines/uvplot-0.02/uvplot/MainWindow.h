#ifndef UVPLOT_MAINWINDOW_H
#define UVPLOT_MAINWINDOW_H

#include <qmainwindow.h>
#include <qmenubar.h>
#include <qpopupmenu.h>
#include <qscrollview.h>

#include <uvplot/MSBaselineCollection.h>
#include <uvplot/ControlPanel.h>
#include <uvplot/DisplayArea.h>


namespace uvplot{

    class MainWindow:public QMainWindow
    {
        Q_OBJECT;
            
        public:
            MainWindow(QWidget * parent = 0,
                       const char * name = 0,
                       WFlags f = WType_TopLevel);
            ~MainWindow();


        private slots:
            void slotFileOpen();
            void slotLoadBaselineData();
            void slotGridAndRedraw();

        private:
            void createMenu();
            
        private:
            MSBaselineCollection itsBaselines;
            int                  itsCurrentAntenna1;
            SimpleArray<float,2>*  itsRealArray;
            SimpleArray<float,2>*  itsImagArray;
            SimpleArray<bool,2>*   itsFlagArray;
            
            QMenuBar*            itsMenuBar;
            QPopupMenu*          itsFileMenu;
            ControlPanel*        itsControlPanel;
            DisplayArea*         itsDisplayArea;
            QScrollView*         itsScrollView;
   };
}

#endif
