#include <uvplot/DisplayArea.h>

#include <qpainter.h>

namespace uvplot{

    using namespace std;

    //==========>>>  DisplayArea::DisplayArea  <<<==========

    DisplayArea::DisplayArea(QWidget*    parent,
                             const char* name):
       QWidget(parent,name),
        itsScaleFactor(1.0),
        itsShowFlags(true),
        itsRealArray(0),
        itsImagArray(0),
        itsFlagArray(0)
    {
        // Make a minimal buffer in order not to upset the paint
        // routines.
        itsBuffer.resize(10,10);
        createColormap(10);
    }

    //==========>>>  DisplayArea::~DisplayArea  <<<==========

    DisplayArea::~DisplayArea()
    {
    }


    
    //==========>>>  DisplayArea::createColormap  <<<==========

    void DisplayArea::createColormap(int amp_levels)
    {
        const int num_phase(360);
        vector<double> R(num_phase);
        vector<double> G(num_phase);
        vector<double> B(num_phase);
        double quarter = num_phase/4.0;

        // fill basic color scheme
        for(int i=0; i < num_phase; i++){
            if(i < round(quarter)){
                R[i] = 1.0 - i/quarter;
                G[i] = 0.0 + i/quarter;
                B[i] = 0.0;
            }else if(i < round(quarter*2)){
                double k = i-round(quarter);
                R[i] = k/(quarter);
                G[i] = 1.0 - k/quarter;
                B[i] = k/(2*quarter);
            }else if(i < round(quarter*3)){
                double k = i-round(quarter*2);
                R[i] = 1.0 - k/(quarter);
                G[i] = k/(quarter);
                B[i] = 0.5;
            }else{
                double k = i-round(quarter*3);
                R[i] = k/quarter;
                G[i] = 1.0-k/(quarter);
                B[i] = 0.5 -k/(2.0*quarter);
            }
            
            if(R[i] > 1.0) R[i] = 1.0;
            if(G[i] > 1.0) G[i] = 1.0;
            if(B[i] > 1.0) B[i] = 1.0;
                
            if(R[i] < 0.0) R[i] = 0.0;
            if(G[i] < 0.0) G[i] = 0.0;
            if(B[i] < 0.0) B[i] = 0.0;
        }

        int N(2*amp_levels+1);

        itsComplexColormap.resize(N*N);
        itsRealIndex.resize(N);
        itsImagIndex.resize(N);
        
        for(int i = 0; i < N; i++){
            itsRealIndex[i] = i;
            itsImagIndex[i] = i*N;
        }

        int mid = N/2;
        for(int i_index=0; i_index < N; i_index++){
            for(int r_index=0; r_index < N; r_index++){
                double im = (i_index - mid)/double(amp_levels);
                double re = (r_index - mid)/double(amp_levels);
                int amp = int(round(sqrt(re*re+im*im)*255));
                double phase = atan2(im,re)*num_phase/(2*M_PI);
                if(phase < 0.0) phase += num_phase;
                
                int phase_index = int(round(phase));
                if(amp > 255){
                    itsComplexColormap[itsRealIndex[r_index]+itsImagIndex[i_index]].setRgb(255,255,255);
                }else{
                    int red   =int(round(pow(R[phase_index],0.4)*amp));
                    int green =int(round(pow(G[phase_index],0.4)*amp));
                    int blue  =int(round(pow(B[phase_index],0.4)*amp));
                    itsComplexColormap[itsRealIndex[r_index]+itsImagIndex[i_index]].setRgb(red,green,blue);
                }
            }
        }
    }
    
        
    //==========>>>  DisplayArea::paintEvent  <<<==========
    
    void DisplayArea::paintEvent(QPaintEvent * /*event*/)
    {
      bitBlt(this, 0, 0, &itsBuffer);
    }
      

    //==========>>>  DisplayArea::resizeEvent  <<<==========
    
    void DisplayArea::resizeEvent(QResizeEvent * event)
    {
      itsBuffer.resize(event->size());
      itsBuffer.fill(white);
      drawView();
    }
      


    //==========>>>  DisplayArea::setData  <<<==========

    void DisplayArea::setData(const SimpleArray<float,2>* re,
            const SimpleArray<float,2>* im,
            const SimpleArray<bool,2>*  flags)
    {
        itsRealArray = re;
        itsImagArray = im;
        itsFlagArray = flags;
        vector<int> shape = re->getShape();
        setFixedSize(shape[1], shape[0]);
        //itsBuffer.resize(shape[1], shape[0]);
        drawView();
    }
    

    
    //==========>>>  DisplayArea::setMaximumAmplitude  <<<==========

    void DisplayArea::setMaximumAmplitude(double max_amp)
    {
        itsScaleFactor= ((itsRealIndex.size()-1)/2)/max_amp;
        drawView();
    }



    //==========>>>  DisplayArea::setShowFlags  <<<==========

    void DisplayArea::setShowFlags(bool show)
    {
        itsShowFlags = show;
        drawView();
    }


    
    //==========>>>  DisplayArea::drawView  <<<==========

    void DisplayArea::drawView()
    {
        if(itsRealArray == 0 || itsImagArray == 0 || itsFlagArray == 0){
            return;
        }

        QPainter buffer_painter;
        buffer_painter.begin(&itsBuffer);
        buffer_painter.eraseRect(0,0,width(),height()); 
        QColor Blue(0,0,255);
        QColor White(255,255,255);
        vector<int> shape = itsRealArray->getShape();
        int h = shape[0];
        int w = shape[1];
        const float* re      = itsRealArray->getData();
        const float* im      = itsImagArray->getData();
        const bool*  flagged = itsFlagArray->getData();

        int amp_levels = (itsRealIndex.size()-1)/2;
        int num_col    = itsRealIndex.size();
        for(int y = 0; y < h; y++){
            for(int x = 0; x < w; x++){
                if(itsShowFlags && *flagged){
                    buffer_painter.setPen(Blue);
                }else{
                    int colre = int(*re*itsScaleFactor+0.5)+amp_levels;
                    int colim = int(*im*itsScaleFactor+0.5)+amp_levels;
                    if(colre < 0 || colre >= num_col ||
                       colim < 0 || colim >= num_col){
                        buffer_painter.setPen(White);
                    }else{

                        buffer_painter.setPen(itsComplexColormap[itsRealIndex[colre]+itsImagIndex[colim]]);
                    }
                }
                buffer_painter.drawPoint(x,y);
                re++;
                im++;
                flagged++;
            }
        }
        
        buffer_painter.end();
        bitBlt(this,0,0,&itsBuffer);
    }

}

