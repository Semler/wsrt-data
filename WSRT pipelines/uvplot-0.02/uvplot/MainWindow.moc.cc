/****************************************************************************
** uvplot::MainWindow meta object code from reading C++ file 'MainWindow.h'
**
** Created: Wed Sep 7 14:51:16 2005
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.3   edited Aug 5 16:40 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "MainWindow.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *uvplot::MainWindow::className() const
{
    return "uvplot::MainWindow";
}

QMetaObject *uvplot::MainWindow::metaObj = 0;
static QMetaObjectCleanUp cleanUp_uvplot__MainWindow( "uvplot::MainWindow", &uvplot::MainWindow::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString uvplot::MainWindow::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "uvplot::MainWindow", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString uvplot::MainWindow::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "uvplot::MainWindow", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* uvplot::MainWindow::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QMainWindow::staticMetaObject();
    static const QUMethod slot_0 = {"slotFileOpen", 0, 0 };
    static const QUMethod slot_1 = {"slotLoadBaselineData", 0, 0 };
    static const QUMethod slot_2 = {"slotGridAndRedraw", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "slotFileOpen()", &slot_0, QMetaData::Private },
	{ "slotLoadBaselineData()", &slot_1, QMetaData::Private },
	{ "slotGridAndRedraw()", &slot_2, QMetaData::Private }
    };
    metaObj = QMetaObject::new_metaobject(
	"uvplot::MainWindow", parentObject,
	slot_tbl, 3,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_uvplot__MainWindow.setMetaObject( metaObj );
    return metaObj;
}

void* uvplot::MainWindow::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "uvplot::MainWindow" ) )
	return this;
    return QMainWindow::qt_cast( clname );
}

bool uvplot::MainWindow::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: slotFileOpen(); break;
    case 1: slotLoadBaselineData(); break;
    case 2: slotGridAndRedraw(); break;
    default:
	return QMainWindow::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool uvplot::MainWindow::qt_emit( int _id, QUObject* _o )
{
    return QMainWindow::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool uvplot::MainWindow::qt_property( int id, int f, QVariant* v)
{
    return QMainWindow::qt_property( id, f, v);
}

bool uvplot::MainWindow::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
