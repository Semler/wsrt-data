#include <uvplot/AntennaPair.h>


namespace uvplot{
    
    //==========>>>  AntennaPair::AntennaPair  <<<==========

    AntennaPair::AntennaPair(unsigned int ant1,
                             unsigned int ant2):
        itsAntenna1(ant1),
        itsAntenna2(ant2)
    {
    }
    
    
    
    //==========>>>  AntennaPair::AntennaPair  <<<==========

    AntennaPair::AntennaPair(const AntennaPair& other):
        itsAntenna1(other.itsAntenna1),
        itsAntenna2(other.itsAntenna2)
    {
    }
    
    
    
    //==========>>>  AntennaPair::~AntennaPair  <<<==========

    AntennaPair::~AntennaPair()
    {
    }



    //==========>>>  AntennaPair::operator =  <<<==========

    void AntennaPair::operator=(const AntennaPair& other)
    {
        itsAntenna1 = other.itsAntenna1;
        itsAntenna2 = other.itsAntenna2;
    }

}
