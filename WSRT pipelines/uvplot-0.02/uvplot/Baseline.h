#ifndef UVPLOT_BASELINE_H
#define UVPLOT_BASELINE_H

#include <uvplot/global.h>
#include <uvplot/GlobalTypes.h>
#include <uvplot/BaselineSpectralWindow.h>



namespace uvplot{
    DECLARE_EXCEPTION_CLASS(SpectralWindowNotPresent);
    
    class Baseline
    {
        public:
            Baseline(unsigned int num_spw=0,
                     unsigned int ant1=0,
                     unsigned int ant2=0);
            Baseline(const Baseline& other);
            ~Baseline();

            void operator=(const Baseline& other);
            

            void reshape(int num_spw,
                         const std::vector<CorrelationType>& pols,
                         int num_times,
                         int num_chan);
            
            void reshape(const std::vector<int>& num_pol,
                         const std::vector<int>& num_times,
                         const std::vector<int>& num_chan);
            

            void setSpectralWindows(unsigned int num_spw);

            void setSpectralWindow(unsigned int spw,
                                   const BaselineSpectralWindow& data);

            const BaselineSpectralWindow& getSpectralWindow(unsigned int spw) const throw(SpectralWindowNotPresent);
            BaselineSpectralWindow& getSpectralWindow(unsigned int spw) throw (SpectralWindowNotPresent);


            bool         hasAntenna(unsigned int ant) const;
            unsigned int getAntenna1() const;
            unsigned int getAntenna2() const;

            void         setAntennas(unsigned int ant1, unsigned int ant2);

            size_t       getNumberOfSpectralWindows() const;

        protected:
            std::vector<BaselineSpectralWindow> itsSpectralWindows;
            unsigned int                        itsAntenna1;
            unsigned int                        itsAntenna2;
    };
}

#endif
