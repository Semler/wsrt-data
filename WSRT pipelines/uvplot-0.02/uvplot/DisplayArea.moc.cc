/****************************************************************************
** uvplot::DisplayArea meta object code from reading C++ file 'DisplayArea.h'
**
** Created: Thu Jun 16 20:40:53 2005
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.3   edited Aug 5 16:40 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "DisplayArea.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *uvplot::DisplayArea::className() const
{
    return "uvplot::DisplayArea";
}

QMetaObject *uvplot::DisplayArea::metaObj = 0;
static QMetaObjectCleanUp cleanUp_uvplot__DisplayArea( "uvplot::DisplayArea", &uvplot::DisplayArea::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString uvplot::DisplayArea::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "uvplot::DisplayArea", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString uvplot::DisplayArea::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "uvplot::DisplayArea", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* uvplot::DisplayArea::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QWidget::staticMetaObject();
    static const QUParameter param_slot_0[] = {
	{ "max_amp", &static_QUType_double, 0, QUParameter::In }
    };
    static const QUMethod slot_0 = {"setMaximumAmplitude", 1, param_slot_0 };
    static const QUParameter param_slot_1[] = {
	{ "show_flags", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod slot_1 = {"setShowFlags", 1, param_slot_1 };
    static const QMetaData slot_tbl[] = {
	{ "setMaximumAmplitude(double)", &slot_0, QMetaData::Public },
	{ "setShowFlags(bool)", &slot_1, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"uvplot::DisplayArea", parentObject,
	slot_tbl, 2,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_uvplot__DisplayArea.setMetaObject( metaObj );
    return metaObj;
}

void* uvplot::DisplayArea::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "uvplot::DisplayArea" ) )
	return this;
    return QWidget::qt_cast( clname );
}

bool uvplot::DisplayArea::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: setMaximumAmplitude((double)static_QUType_double.get(_o+1)); break;
    case 1: setShowFlags((bool)static_QUType_bool.get(_o+1)); break;
    default:
	return QWidget::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool uvplot::DisplayArea::qt_emit( int _id, QUObject* _o )
{
    return QWidget::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool uvplot::DisplayArea::qt_property( int id, int f, QVariant* v)
{
    return QWidget::qt_property( id, f, v);
}

bool uvplot::DisplayArea::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
