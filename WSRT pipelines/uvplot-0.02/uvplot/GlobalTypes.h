#ifndef UVPLOT_GLOBALTYPES_H
#define UVPLOT_GLOBALTYPES_H

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <string>

namespace uvplot{
    
    enum CorrelationType{
        None=0,
        I=1,Q=2,U=3,V=4,
        RR=5,RL=6,LR=7,LL=8,
        XX=9,XY=10,YX=11,YY=12};

    std::string CorrelationTypeName(CorrelationType corr);    



}


#endif
