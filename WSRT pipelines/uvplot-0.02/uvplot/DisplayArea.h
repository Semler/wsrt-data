#ifndef UVPLOT_DISPLAYAREA_H
#define UVPLOT_DISPLAYAREA_H

#include <uvplot/global.h>
#include <uvplot/SimpleArray.h>

#include <qwidget.h>
#include <qpixmap.h>
#include <qcolor.h>

#include <vector>

namespace uvplot{
    class DisplayArea:public QWidget
    {
        Q_OBJECT
        
        public:
            DisplayArea(QWidget*    parent=0,
                        const char* name=0);
           ~DisplayArea(); 


           void setData(const SimpleArray<float,2>* re,
                        const SimpleArray<float,2>* im,
                        const SimpleArray<bool,2>*  flags);
           
           void drawView();

           /*! \param amp_levels sets the number of color levels in
            * the amplitude scale
            */
           void createColormap(int extent);
           
           
           virtual void paintEvent(QPaintEvent *event);
           virtual void resizeEvent(QResizeEvent *event);
        public slots:
           void setMaximumAmplitude(double max_amp); 
           void setShowFlags(bool show_flags);

           
        protected:
           QPixmap             itsBuffer;
           std::vector<QColor> itsComplexColormap;
           std::vector<int>    itsRealIndex;
           std::vector<int>    itsImagIndex;

           double              itsScaleFactor;
           bool                itsShowFlags;

           const SimpleArray<float,2>* itsRealArray;
           const SimpleArray<float,2>* itsImagArray;
           const SimpleArray<bool,2>*  itsFlagArray;
    };
}
#endif

