#ifndef UVPLOT_GLOBAL_H
#define UVPLOT_GLOBAL_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <complex>
#include <string>
#include <stdexcept>
#include <iostream>

namespace uvplot{
    typedef std::complex<float>  fcomplex;
    typedef std::complex<double> dcomplex;
}

#define DECLARE_EXCEPTION_CLASS(x) class x : public std::runtime_error{public: x(const std::string& msg):std::runtime_error(msg){}; };

#define UVPLOT_LOG std::cerr << __FILE__ << ":" << __LINE__ << ":"

#endif
