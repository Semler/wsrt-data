#ifndef UVPLOT_BASELINESPECTRALWINDOW_H 
#define UVPLOT_BASELINESPECTRALWINDOW_H 

#include <uvplot/global.h>
#include <uvplot/TimeFrequencyPlane.h>

#include <vector>


namespace uvplot{


    DECLARE_EXCEPTION_CLASS(CorrelationNotPresent);//thrown by getplane
    
    class BaselineSpectralWindow
    {
        public:
            BaselineSpectralWindow();
            BaselineSpectralWindow(const BaselineSpectralWindow&other);
            ~BaselineSpectralWindow();

            void  operator=(const BaselineSpectralWindow&other);
            

            void  reshape(const std::vector<CorrelationType>& pols,
                          int num_times,
                          int num_chan);
            
            void                      addPlane(const TimeFrequencyPlane& plane);
            
            const TimeFrequencyPlane& getPlane(CorrelationType corr) const
                throw(CorrelationNotPresent);
            
            TimeFrequencyPlane& getPlane(CorrelationType corr)
                throw(CorrelationNotPresent);

        protected:
            std::vector<TimeFrequencyPlane> itsCorrelations;
            unsigned int                    itsNumCorrelations;
    };
}

#endif
