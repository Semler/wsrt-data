#include <uvplot/global.h>
#include <uvplot/MainWindow.h>

#include <qapplication.h>

#include <iostream>
#include <stdexcept>

using namespace std;
using namespace uvplot;


int main(int argc, char* argv[])
try{
    cout << PACKAGE_NAME << " " << PACKAGE_VERSION  << " (c) 2005 " << PACKAGE_BUGREPORT << endl;
    QApplication app(argc, argv);

    MainWindow* mainwin = new MainWindow();
    mainwin->resize(1024,768);
    app.setMainWidget(mainwin);
    mainwin->show();
    return app.exec();
}catch(exception &e)
{
    cout << "Exception: "<< e.what() << endl;
    cout << "Aborting..." << endl;
}
