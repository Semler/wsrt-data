# Local required definitions for AIPS++
# This is to define PGPLOT_FONT and LD_LIBRARY_PATH
# where required.
#--------------------------------------------------------

#echo AIPSPATH $AIPSPATH
set A_ROOT=`/bin/echo $AIPSPATH | awk '{print $1}'`
#echo $A_ROOT

if ( ! $?LD_LIBRARY_PATH) then
   setenv LD_LIBRARY_PATH $A_ROOT/linux_gnu/lib
else
   setenv LD_LIBRARY_PATH $A_ROOT/linux_gnu/lib:$LD_LIBRARY_PATH
endif
