#!/bin/sh
# Local required definitions for AIPS++
# This is to define PGPLOT_FONT and LD_LIBRARY_PATH
# where required.
#--------------------------------------------------------
if [ "$PGPLOT_FONT" = "" ] ; then
   PGPLOT_FONT=/usr/lib/pgplot/grfont.dat; export PGPLOT_FONT
fi
if [ "$LD_LIBRARY_PATH" = "" ] ; then
   LD_LIBRARY_PATH=/aips++/V19.1872_listfen_gcc410/linux_gnu/lib
else
   LD_LIBRARY_PATH=/aips++/V19.1872_listfen_gcc410/linux_gnu/lib:$LD_LIBRARY_PATH
fi
export LD_LIBRARY_PATH
