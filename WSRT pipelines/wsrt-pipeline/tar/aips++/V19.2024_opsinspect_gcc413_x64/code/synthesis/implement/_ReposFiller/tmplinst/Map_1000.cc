// Map_1000.cc -- Tue Apr  1 12:27:41 BST 2008 -- renting
#include <casa/Containers/Map.cc>
#include <synthesis/Parallel/Algorithm.h>
namespace casa { //# NAMESPACE - BEGIN
template class ConstMapIter<Int, Algorithm *>;
template class MapIter<Int, Algorithm *>;
template class MapIterRep<Int, Algorithm *>;
template class MapRep<Int, Algorithm *>;
template class Map<Int, Algorithm *>;
} //# NAMESPACE - END
