// ArrayLogical_1000.cc -- Tue Apr  1 12:25:02 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allNearAbs(Array<Complex> const &, Array<Complex> const &, Double tol);
} //# NAMESPACE - END
