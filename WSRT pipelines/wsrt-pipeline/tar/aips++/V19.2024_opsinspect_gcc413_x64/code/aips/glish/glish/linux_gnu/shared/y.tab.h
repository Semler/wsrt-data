/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     START_KEYWORDS = 258,
     TOK_ACTIVATE = 259,
     TOK_AWAIT = 260,
     TOK_BREAK = 261,
     TOK_DO = 262,
     TOK_ELSE = 263,
     TOK_EXCEPT = 264,
     TOK_EXIT = 265,
     TOK_FAIL = 266,
     TOK_FOR = 267,
     TOK_FUNCTION = 268,
     TOK_GLOBAL = 269,
     TOK_IF = 270,
     TOK_IN = 271,
     TOK_INCLUDE = 272,
     TOK_LINK = 273,
     TOK_LOCAL = 274,
     TOK_NEXT = 275,
     TOK_ONLY = 276,
     TOK_PRINT = 277,
     TOK_RETURN = 278,
     TOK_SUBSEQUENCE = 279,
     TOK_TO = 280,
     TOK_UNLINK = 281,
     TOK_WHENEVER = 282,
     TOK_WHILE = 283,
     TOK_WIDER = 284,
     TOK_REF = 285,
     TOK_VAL = 286,
     END_KEYWORDS = 287,
     TOK_CONST = 288,
     TOK_CONSTANT = 289,
     TOK_ID = 290,
     TOK_REGEX = 291,
     TOK_ELLIPSIS = 292,
     TOK_APPLYRX = 293,
     TOK_ATTR = 294,
     TOK_LAST_EVENT = 295,
     TOK_LAST_REGEX = 296,
     TOK_FLEX_ERROR = 297,
     NULL_TOK = 298,
     TOK_ASSIGN = 299,
     TOK_OR_OR = 300,
     TOK_AND_AND = 301,
     TOK_NE = 302,
     TOK_EQ = 303,
     TOK_GE = 304,
     TOK_LE = 305,
     TOK_GT = 306,
     TOK_LT = 307,
     TOK_REQUEST = 308,
     TOK_ARROW = 309
   };
#endif
/* Tokens.  */
#define START_KEYWORDS 258
#define TOK_ACTIVATE 259
#define TOK_AWAIT 260
#define TOK_BREAK 261
#define TOK_DO 262
#define TOK_ELSE 263
#define TOK_EXCEPT 264
#define TOK_EXIT 265
#define TOK_FAIL 266
#define TOK_FOR 267
#define TOK_FUNCTION 268
#define TOK_GLOBAL 269
#define TOK_IF 270
#define TOK_IN 271
#define TOK_INCLUDE 272
#define TOK_LINK 273
#define TOK_LOCAL 274
#define TOK_NEXT 275
#define TOK_ONLY 276
#define TOK_PRINT 277
#define TOK_RETURN 278
#define TOK_SUBSEQUENCE 279
#define TOK_TO 280
#define TOK_UNLINK 281
#define TOK_WHENEVER 282
#define TOK_WHILE 283
#define TOK_WIDER 284
#define TOK_REF 285
#define TOK_VAL 286
#define END_KEYWORDS 287
#define TOK_CONST 288
#define TOK_CONSTANT 289
#define TOK_ID 290
#define TOK_REGEX 291
#define TOK_ELLIPSIS 292
#define TOK_APPLYRX 293
#define TOK_ATTR 294
#define TOK_LAST_EVENT 295
#define TOK_LAST_REGEX 296
#define TOK_FLEX_ERROR 297
#define NULL_TOK 298
#define TOK_ASSIGN 299
#define TOK_OR_OR 300
#define TOK_AND_AND 301
#define TOK_NE 302
#define TOK_EQ 303
#define TOK_GE 304
#define TOK_LE 305
#define TOK_GT 306
#define TOK_LT 307
#define TOK_REQUEST 308
#define TOK_ARROW 309




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 81 "../../parse.y"
{
	char* id;
	last_event_type event_type;
	last_regex_type regex_type;
	Expr* expr;
	expr_list* exprlist;
	EventDesignator* event;
	Stmt* stmt;
	event_dsg_list* ev_list;
	PDict(Expr)* record;
	parameter_list* param_list;
	Parameter* param;
	value_reftype val_type;
	int ival;
	}
/* Line 1489 of yacc.c.  */
#line 173 "y.tab.h"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;

