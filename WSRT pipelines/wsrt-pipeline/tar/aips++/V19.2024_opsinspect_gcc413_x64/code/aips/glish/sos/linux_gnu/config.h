/* sos/linux_gnu/config.h.  Generated automatically by configure.  */
/* $Id: config.h.in,v 19.0 2003/07/16 05:17:50 aips2adm Exp $
**
*/

/***************************************************************/
/* custom config.h template - do not recreate using autoheader */
/***************************************************************/

/* Define if you have <unistd.h>.  */
#define HAVE_UNISTD_H 1

/* Define if compiler does not support const keyword */
/* #undef const */

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* Define if you have the ANSI C header files.  */
/* #undef WORDS_BIGENDIAN */

/* Define if you have <sys/uio.h>.  */
#define HAVE_SYS_UIO_H 1

/* Define if you have <sys/limits.h>.  */
/* #undef HAVE_SYS_LIMITS_H */

/* Define if you have have "writev()".  */
#define HAVE_WRITEV 1

/* Define if your system doesn't declare 'writev()'.  */
/* #undef WRITEV_NOT_DECLARED */

/* Define if you have the <malloc.h> header file.  */
#define HAVE_MALLOC_H 1
