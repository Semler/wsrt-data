/* editline/linux_gnu/config.h.  Generated automatically by configure.  */
/* $Id: config.h.in,v 19.1 2004/02/02 17:43:04 wyoung Exp $
**
** Editline configure header file
*/

/***************************************************************/
/* custom config.h template - do not recreate using autoheader */
/***************************************************************/

/* Define if you have <unistd.h>.  */
#define HAVE_UNISTD_H 1

/* Define if you have <stdlib.h>.  */
#define HAVE_STDLIB_H 1

/* Define if you have <dirent.h>.  */
#define HAVE_DIRENT_H 1

/* Define if you have <termio.h>.  */
#define HAVE_TERMIO_H 1

/* Define if you have <termios.h>.  */
#define HAVE_TERMIOS_H 1

/* Define if the tcgetattr() function is available. */
#define HAVE_TCGETATTR 1

/* Define if compiler does not support const keyword */
/* #undef const */

/* Define if compiler size_t is not defined */
/* #undef size_t */
