/* glish/clients/glishtk/linux_gnu/config.h.  Generated automatically by configure.  */
/* $Id: config.h.in,v 19.13 2004/11/03 20:38:59 cvsmgr Exp $
** Copyright (c) 1993 The Regents of the University of California.
** Copyright (c) 1997,2002 Associated Universities Inc.
*/

/***************************************************************/
/* custom config.h template - do not recreate using autoheader */
/***************************************************************/

/* Define if you have <sys/wait.h> that is POSIX.1 compatible.  */
#define HAVE_SYS_WAIT_H 1

/* Define as the return type of signal handlers (int or void).  */
#define RETSIGTYPE void

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* Define if you have the gethostname function.  */
#define HAVE_GETHOSTNAME 1

#ifndef _AIX
/* Define if you have the setrlimit function.  */
#define HAVE_SETRLIMIT 1
#endif

/* Define if you have the setsockopt function.  */
#define HAVE_SETSOCKOPT 1

/* Define if you have the sigprocmask function.  */
#define HAVE_SIGPROCMASK 1

/* Define if you have the strdup function.  */
#define HAVE_STRDUP 1

/* Define if you have the uname function.  */
#define HAVE_UNAME 1

/* Define if you have the waitpid function.  */
#define HAVE_WAITPID 1

/* Define if you have the <X11/fd.h> header file.  */
/* #undef HAVE_X11_FD_H */

/* Define if you have the <libc.h> header file.  */
/* #undef HAVE_LIBC_H */

/* Define if you have the <sigLib.h> header file.  */
/* #undef HAVE_SIGLIB_H */

/* Define if you have the <sys/filio.h> header file.  */
/* #undef HAVE_SYS_FILIO_H */

/* Define if you have the <malloc.h> header file.  */
#define HAVE_MALLOC_H 1

/* Define if you have the <sys/time.h> header file.  */
#define HAVE_SYS_TIME_H 1

/* Define if you have the <sys/select.h> header file.  */
#define HAVE_SYS_SELECT_H 1

/* Define if you have the <sys/signal.h> header file.  */
#define HAVE_SYS_SIGNAL_H 1

/* Define if you have the <sys/shm.h> header file.  */
#define HAVE_SYS_SHM_H 1

/* Define if you have the shmget() function.  */
#define HAVE_SHMGET 1

/* Define if you have the <unistd.h> header file.  */
#define HAVE_UNISTD_H 1

/* Define if you have the random() BSD function.  */
#define HAVE_RANDOM 1

/* Define if you have the lrand48() SYSV function.  */
#define HAVE_LRAND48 1

/* Define if you have <fp.h>.  */
/* #undef HAVE_FP_H */

/* Define to be "int" if C++ for loops have local scope.  */
#define LOOPDECL int

/* Define if you have <vfork.h>.  */
/* #undef HAVE_VFORK_H */

/* Define vfork as fork if vfork does not work.  */
#define vfork fork

/* Define to be the mask type for select() calls.  */
#define SELECT_MASK_TYPE fd_set

/* Define if gettimeofday is not declared (for C++) */
/* #undef GETTOD_NOT_DECLARED_CXX */
#ifdef GETTOD_NOT_DECLARED_CXX
/* #undef GETTOD_NOT_DECLARED */
#endif

/* Define if gettimeofday is not declared */
/* #undef SELECT_NOT_DECLARED */

/* Define if setrlimit is not declared */
/* #undef SETRLIMIT_NOT_DECLARED */

/* Define if gettimeofday is not declared */
/* #undef SHMGET_NOT_DECLARED */

/* Define if you have the BSDgettimeofday() function */
/* #undef HAVE_BSDGETTIMEOFDAY */

/* Define if you have the <limits.h> header file.  */
#define HAVE_LIMITS_H 1

/* Define if you have the <float.h> header file.  */
#define HAVE_FLOAT_H 1

/* Define if you have the <machine/fpu.h> header file.  */
/* #undef HAVE_MACHINE_FPU_H */

/* Define if you have the <siginfo.h> header file.  */
/* #undef HAVE_SIGINFO_H */

/* Define if you have the <ucontext.h> header file.  */
#define HAVE_UCONTEXT_H 1

/* Define if you have <stropts.h> header file */
#define HAVE_STROPTS_H 1

/* Define if you have the <floatingpoint.h> header file.  */
/* #undef HAVE_FLOATINGPOINT_H */

/* Define if you have the sigfpe() function */
/* #undef HAVE_SIGFPE */

/* Define if you have the <osfcn.h> header file.  */
/* #undef HAVE_OSFCN_H */

/* Define if pipe() doesn't create a stream. */
#define HAVE_STREAMLESS_PIPE 1

/* Define if you're running 4.3 BSD Reno */
#define HAVE_BSD_RENO 1

/* Define if you have <sys/uio.h>.  */
#define HAVE_SYS_UIO_H 1

/* Define if you have the <mach-o/dyld.h> header file.  */
/* #undef HAVE_MACH_O_DYLD_H */

/* Define ifyou have the <crt_externs.h> header file. */
/* #undef HAVE_CRT_EXTERNS_H */

/* Define if you can include <iostream.h> without the compiler whining. */
#define HAVE_IOSTREAM_H 1

/* If your version of TCL defines Tcl_GetSetringResult().  */
/* #undef HAVE_TCL_GETSTRINGRESULT */

/* #undef ENVIRON_NOT_DECLARED */
#ifdef ENVIRON_NOT_DECLARED
#ifdef __cplusplus
extern "C" char **environ;
#else
extern char **environ;
#endif
#endif

#define GTKCONST const
