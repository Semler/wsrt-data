/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     START_KEYWORDS = 258,
     TOK_ACTIVATE = 259,
     TOK_AWAIT = 260,
     TOK_BREAK = 261,
     TOK_DO = 262,
     TOK_ELSE = 263,
     TOK_EXCEPT = 264,
     TOK_EXIT = 265,
     TOK_FAIL = 266,
     TOK_FOR = 267,
     TOK_FUNCTION = 268,
     TOK_GLOBAL = 269,
     TOK_IF = 270,
     TOK_IN = 271,
     TOK_INCLUDE = 272,
     TOK_LINK = 273,
     TOK_LOCAL = 274,
     TOK_NEXT = 275,
     TOK_ONLY = 276,
     TOK_PRINT = 277,
     TOK_RETURN = 278,
     TOK_SUBSEQUENCE = 279,
     TOK_TO = 280,
     TOK_UNLINK = 281,
     TOK_WHENEVER = 282,
     TOK_WHILE = 283,
     TOK_WIDER = 284,
     TOK_REF = 285,
     TOK_VAL = 286,
     END_KEYWORDS = 287,
     TOK_CONST = 288,
     TOK_CONSTANT = 289,
     TOK_ID = 290,
     TOK_REGEX = 291,
     TOK_ELLIPSIS = 292,
     TOK_APPLYRX = 293,
     TOK_ATTR = 294,
     TOK_LAST_EVENT = 295,
     TOK_LAST_REGEX = 296,
     TOK_FLEX_ERROR = 297,
     NULL_TOK = 298,
     TOK_ASSIGN = 299,
     TOK_OR_OR = 300,
     TOK_AND_AND = 301,
     TOK_NE = 302,
     TOK_EQ = 303,
     TOK_GE = 304,
     TOK_LE = 305,
     TOK_GT = 306,
     TOK_LT = 307,
     TOK_REQUEST = 308,
     TOK_ARROW = 309
   };
#endif
/* Tokens.  */
#define START_KEYWORDS 258
#define TOK_ACTIVATE 259
#define TOK_AWAIT 260
#define TOK_BREAK 261
#define TOK_DO 262
#define TOK_ELSE 263
#define TOK_EXCEPT 264
#define TOK_EXIT 265
#define TOK_FAIL 266
#define TOK_FOR 267
#define TOK_FUNCTION 268
#define TOK_GLOBAL 269
#define TOK_IF 270
#define TOK_IN 271
#define TOK_INCLUDE 272
#define TOK_LINK 273
#define TOK_LOCAL 274
#define TOK_NEXT 275
#define TOK_ONLY 276
#define TOK_PRINT 277
#define TOK_RETURN 278
#define TOK_SUBSEQUENCE 279
#define TOK_TO 280
#define TOK_UNLINK 281
#define TOK_WHENEVER 282
#define TOK_WHILE 283
#define TOK_WIDER 284
#define TOK_REF 285
#define TOK_VAL 286
#define END_KEYWORDS 287
#define TOK_CONST 288
#define TOK_CONSTANT 289
#define TOK_ID 290
#define TOK_REGEX 291
#define TOK_ELLIPSIS 292
#define TOK_APPLYRX 293
#define TOK_ATTR 294
#define TOK_LAST_EVENT 295
#define TOK_LAST_REGEX 296
#define TOK_FLEX_ERROR 297
#define NULL_TOK 298
#define TOK_ASSIGN 299
#define TOK_OR_OR 300
#define TOK_AND_AND 301
#define TOK_NE 302
#define TOK_EQ 303
#define TOK_GE 304
#define TOK_LE 305
#define TOK_GT 306
#define TOK_LT 307
#define TOK_REQUEST 308
#define TOK_ARROW 309




/* Copy the first part of user declarations.  */
#line 57 "../../parse.y"

#if defined(_AIX) && defined(YYBISON)
/* Must occur before the first line of C++ code - barf! */
/* Actually only required when using bison. */
#pragma alloca
#endif

#include "config.h"
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>

#include "BinOpExpr.h"
#include "Event.h"
#include "Stmt.h"
#include "Func.h"
#include "Glish/Reporter.h"
#include "Pager.h"
#include "Sequencer.h"
#include "Task.h"
#include "input.h"
#include "system.h"


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 81 "../../parse.y"
{
	char* id;
	last_event_type event_type;
	last_regex_type regex_type;
	Expr* expr;
	expr_list* exprlist;
	EventDesignator* event;
	Stmt* stmt;
	event_dsg_list* ev_list;
	PDict(Expr)* record;
	parameter_list* param_list;
	Parameter* param;
	value_reftype val_type;
	int ival;
	}
/* Line 187 of yacc.c.  */
#line 244 "y.tab.c"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */
#line 97 "../../parse.y"

extern "C" {
	int yyparse();
	void yyerror( char msg[] );
}

/* returns non-zero if look-ahead should be cleared */
extern int begin_literal_ids( int );
extern void end_literal_ids();

expr_list *glish_current_subsequence = 0;
/* reset glish state after an error */
static void error_reset( );

#if ! defined(PURE_PARSER)
extern int yylex();
#else
extern int yylex( YYSTYPE * );
#endif

/* Used for recovery after a ^C */
extern jmp_buf glish_top_jmpbuf;
extern int glish_top_jmpbuf_set;

Sequencer* current_sequencer = 0;
static IValue *parse_error = 0;

/* Communication of status between glish_parser() and yyparse() */
static int status;
static int scope_depth = 0;
static Stmt *cur_stmt = null_stmt;
static evalOpt *eval_options;

static int *lookahead = 0;
static int empty_token = 0;

int first_line = 1;
extern int glish_regex_matched;
extern void putback_token( int );
Expr* compound_assignment( Expr* lhs, int tok_type, Expr* rhs );


/* Line 216 of yacc.c.  */
#line 298 "y.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1388

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  75
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  50
/* YYNRULES -- Number of rules.  */
#define YYNRULES  161
/* YYNRULES -- Number of states.  */
#define YYNSTATES  290

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   309

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    63,     2,     2,     2,    60,    49,     2,
      67,    68,    58,    56,    44,    57,    64,    59,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    61,    71,
       2,    72,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    65,     2,    66,    62,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    74,    48,    73,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    45,
      46,    47,    50,    51,    52,    53,    54,    55,    69,    70
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     4,     7,     9,    10,    13,    14,    16,
      18,    19,    21,    23,    25,    29,    33,    37,    39,    45,
      51,    55,    60,    67,    70,    74,    81,    90,    99,   106,
     109,   112,   115,   119,   122,   126,   130,   134,   137,   139,
     143,   146,   150,   154,   158,   162,   166,   170,   174,   178,
     182,   186,   190,   194,   198,   202,   206,   210,   214,   218,
     222,   226,   229,   232,   235,   238,   243,   247,   250,   256,
     260,   264,   270,   274,   279,   282,   287,   289,   291,   294,
     296,   298,   300,   302,   304,   308,   310,   314,   316,   320,
     322,   326,   328,   332,   334,   338,   340,   344,   346,   357,
     359,   361,   362,   368,   369,   371,   372,   376,   378,   382,
     385,   387,   388,   391,   392,   394,   395,   399,   401,   403,
     407,   409,   411,   412,   416,   418,   420,   425,   429,   431,
     433,   434,   438,   441,   443,   446,   448,   450,   454,   456,
     460,   463,   465,   468,   470,   472,   476,   478,   480,   482,
     483,   487,   489,   495,   499,   504,   506,   508,   510,   511,
     512,   513
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      76,     0,    -1,    -1,    77,    83,    -1,     1,    -1,    -1,
      78,    83,    -1,    -1,    86,    -1,    79,    -1,    -1,    79,
      -1,   116,    -1,    93,    -1,    19,    87,    71,    -1,    14,
      89,    71,    -1,    29,    91,    71,    -1,    84,    -1,    18,
     118,    25,   118,    71,    -1,    26,   118,    25,   118,    71,
      -1,     5,   118,    71,    -1,     5,    21,   118,    71,    -1,
       5,    21,   118,     9,   118,    71,    -1,     4,    71,    -1,
       4,    79,    71,    -1,    15,    67,    79,    68,   121,    83,
      -1,    15,    67,    79,    68,   121,    83,     8,    83,    -1,
      12,    67,    82,    16,    79,    68,   121,    83,    -1,    28,
      67,    79,    68,   121,    83,    -1,    20,    71,    -1,     6,
      71,    -1,    23,    71,    -1,    23,    79,    71,    -1,    10,
      71,    -1,    10,    79,    71,    -1,    22,   104,    71,    -1,
      11,    80,    71,    -1,    81,    71,    -1,    71,    -1,    85,
       7,    83,    -1,    27,   118,    -1,    67,    86,    68,    -1,
      86,    45,    86,    -1,    86,    46,    86,    -1,    86,    47,
      86,    -1,    86,    48,    86,    -1,    86,    49,    86,    -1,
      86,    55,    86,    -1,    86,    54,    86,    -1,    86,    53,
      86,    -1,    86,    52,    86,    -1,    86,    51,    86,    -1,
      86,    50,    86,    -1,    55,    86,    54,    -1,    86,    56,
      86,    -1,    86,    57,    86,    -1,    86,    58,    86,    -1,
      86,    59,    86,    -1,    86,    60,    86,    -1,    86,    62,
      86,    -1,    86,    38,    86,    -1,    38,    86,    -1,    57,
      86,    -1,    56,    86,    -1,    63,    86,    -1,    86,    65,
     113,    66,    -1,    86,    64,    35,    -1,    86,    39,    -1,
      86,    39,    65,    86,    66,    -1,    86,    39,    35,    -1,
      65,    72,    66,    -1,    65,   123,   107,   124,    66,    -1,
      86,    61,    86,    -1,    86,    67,   110,    68,    -1,   120,
      86,    -1,   119,    67,   104,    68,    -1,    40,    -1,    41,
      -1,    17,    86,    -1,    95,    -1,   116,    -1,    34,    -1,
      36,    -1,    42,    -1,    87,    44,    88,    -1,    88,    -1,
      35,    45,    79,    -1,    35,    -1,    89,    44,    90,    -1,
      90,    -1,    35,    45,    79,    -1,    35,    -1,    91,    44,
      92,    -1,    92,    -1,    35,    45,    79,    -1,    35,    -1,
      94,    78,    73,    -1,    74,    -1,    96,   117,    67,    99,
      68,   121,    97,   121,   115,   122,    -1,    13,    -1,    24,
      -1,    -1,    61,    65,    98,   107,    66,    -1,    -1,   100,
      -1,    -1,   100,    44,   101,    -1,   101,    -1,   102,    35,
     103,    -1,    37,   103,    -1,   120,    -1,    -1,    72,    79,
      -1,    -1,   105,    -1,    -1,   105,    44,   106,    -1,   106,
      -1,    79,    -1,    35,    72,    79,    -1,    37,    -1,   108,
      -1,    -1,   108,    44,   109,    -1,   109,    -1,    79,    -1,
      33,    35,    72,    79,    -1,    35,    72,    79,    -1,    37,
      -1,   111,    -1,    -1,   111,    44,   112,    -1,   111,    44,
      -1,    44,    -1,    44,   112,    -1,   112,    -1,    79,    -1,
      35,    72,    79,    -1,    37,    -1,   113,    44,   114,    -1,
     113,    44,    -1,    44,    -1,    44,   114,    -1,   114,    -1,
      79,    -1,    74,    78,    73,    -1,    86,    -1,    35,    -1,
      35,    -1,    -1,   118,    44,   119,    -1,   119,    -1,    86,
      70,    65,    86,    66,    -1,    86,    70,    35,    -1,    86,
      70,    58,   122,    -1,    30,    -1,    33,    -1,    31,    -1,
      -1,    -1,    -1,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   143,   143,   143,   164,   181,   190,   193,   198,   203,
     205,   209,   215,   220,   222,   225,   228,   231,   233,   236,
     239,   243,   247,   252,   255,   258,   260,   263,   268,   271,
     274,   277,   280,   283,   286,   289,   292,   295,   298,   302,
     309,   316,   319,   322,   324,   327,   329,   332,   334,   336,
     338,   340,   342,   345,   352,   354,   356,   358,   360,   362,
     365,   373,   385,   387,   389,   392,   395,   398,   401,   404,
     407,   410,   413,   416,   419,   422,   427,   430,   433,   436,
     438,   440,   442,   444,   452,   454,   457,   467,   475,   477,
     480,   491,   499,   501,   504,   523,   541,   549,   556,   616,
     622,   632,   632,   638,   642,   644,   647,   650,   657,   671,
     681,   683,   687,   690,   694,   696,   699,   702,   709,   712,
     718,   741,   743,   746,   749,   756,   759,   765,   771,   794,
     796,   799,   801,   806,   813,   820,   827,   830,   836,   859,
     861,   863,   869,   875,   883,   887,   890,   895,   908,   910,
     914,   916,   923,   929,   934,   942,   944,   946,   951,   954,
     958,   967
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "START_KEYWORDS", "TOK_ACTIVATE",
  "TOK_AWAIT", "TOK_BREAK", "TOK_DO", "TOK_ELSE", "TOK_EXCEPT", "TOK_EXIT",
  "TOK_FAIL", "TOK_FOR", "TOK_FUNCTION", "TOK_GLOBAL", "TOK_IF", "TOK_IN",
  "TOK_INCLUDE", "TOK_LINK", "TOK_LOCAL", "TOK_NEXT", "TOK_ONLY",
  "TOK_PRINT", "TOK_RETURN", "TOK_SUBSEQUENCE", "TOK_TO", "TOK_UNLINK",
  "TOK_WHENEVER", "TOK_WHILE", "TOK_WIDER", "TOK_REF", "TOK_VAL",
  "END_KEYWORDS", "TOK_CONST", "TOK_CONSTANT", "TOK_ID", "TOK_REGEX",
  "TOK_ELLIPSIS", "TOK_APPLYRX", "TOK_ATTR", "TOK_LAST_EVENT",
  "TOK_LAST_REGEX", "TOK_FLEX_ERROR", "NULL_TOK", "','", "TOK_ASSIGN",
  "TOK_OR_OR", "TOK_AND_AND", "'|'", "'&'", "TOK_NE", "TOK_EQ", "TOK_GE",
  "TOK_LE", "TOK_GT", "TOK_LT", "'+'", "'-'", "'*'", "'/'", "'%'", "':'",
  "'^'", "'!'", "'.'", "'['", "']'", "'('", "')'", "TOK_REQUEST",
  "TOK_ARROW", "';'", "'='", "'}'", "'{'", "$accept", "glish", "@1",
  "statement_list", "scoped_expr", "opt_scoped_expr", "stand_alone_expr",
  "scoped_lhs_var", "statement", "whenever", "whenever_head", "expression",
  "local_list", "local_item", "global_list", "global_item", "wider_list",
  "wider_item", "block", "block_head", "function", "function_head",
  "func_attributes", "@2", "formal_param_list", "formal_params",
  "formal_param", "formal_param_class", "formal_param_default",
  "actual_param_list", "actual_params", "actual_param",
  "array_record_ctor_list", "array_record_params", "array_record_param",
  "opt_actual_param_list", "opt_actual_params", "opt_actual_param",
  "subscript_list", "subscript", "func_body", "var", "opt_id",
  "event_list", "event", "value_type", "cont", "no_cont",
  "begin_literal_ids", "end_literal_ids", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,    44,   299,   300,   301,   124,    38,
     302,   303,   304,   305,   306,   307,    43,    45,    42,    47,
      37,    58,    94,    33,    46,    91,    93,    40,    41,   308,
     309,    59,    61,   125,   123
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 glish_yyr1[] =
{
       0,    75,    77,    76,    76,    76,    78,    78,    79,    80,
      80,    81,    82,    83,    83,    83,    83,    83,    83,    83,
      83,    83,    83,    83,    83,    83,    83,    83,    83,    83,
      83,    83,    83,    83,    83,    83,    83,    83,    83,    84,
      85,    86,    86,    86,    86,    86,    86,    86,    86,    86,
      86,    86,    86,    86,    86,    86,    86,    86,    86,    86,
      86,    86,    86,    86,    86,    86,    86,    86,    86,    86,
      86,    86,    86,    86,    86,    86,    86,    86,    86,    86,
      86,    86,    86,    86,    87,    87,    88,    88,    89,    89,
      90,    90,    91,    91,    92,    92,    93,    94,    95,    96,
      96,    98,    97,    97,    99,    99,   100,   100,   101,   101,
     102,   102,   103,   103,   104,   104,   105,   105,   106,   106,
     106,   107,   107,   108,   108,   109,   109,   109,   109,   110,
     110,   111,   111,   111,   111,   111,   112,   112,   112,   113,
     113,   113,   113,   113,   114,   115,   115,   116,   117,   117,
     118,   118,   119,   119,   119,   120,   120,   120,   121,   122,
     123,   124
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 glish_yyr2[] =
{
       0,     2,     0,     2,     1,     0,     2,     0,     1,     1,
       0,     1,     1,     1,     3,     3,     3,     1,     5,     5,
       3,     4,     6,     2,     3,     6,     8,     8,     6,     2,
       2,     2,     3,     2,     3,     3,     3,     2,     1,     3,
       2,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     2,     2,     2,     2,     4,     3,     2,     5,     3,
       3,     5,     3,     4,     2,     4,     1,     1,     2,     1,
       1,     1,     1,     1,     3,     1,     3,     1,     3,     1,
       3,     1,     3,     1,     3,     1,     3,     1,    10,     1,
       1,     0,     5,     0,     1,     0,     3,     1,     3,     2,
       1,     0,     2,     0,     1,     0,     3,     1,     1,     3,
       1,     1,     0,     3,     1,     1,     4,     3,     1,     1,
       0,     3,     2,     1,     2,     1,     1,     3,     1,     3,
       2,     1,     2,     1,     1,     3,     1,     1,     1,     0,
       3,     1,     5,     3,     4,     1,     1,     1,     0,     0,
       0,     0
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 glish_yydefact[] =
{
       0,     4,     0,     0,     1,     0,     0,     0,     0,    10,
       0,    99,     0,     0,     0,     0,     0,     0,   115,     0,
     100,     0,     0,     0,     0,   155,   157,   156,    81,   147,
      82,     0,    76,    77,    83,     0,     0,     0,     0,   160,
       0,    38,    97,    11,     0,     3,    17,     0,     8,    13,
       7,    79,   149,    80,     0,     0,    23,     0,     0,     0,
       0,   151,    30,    33,     0,     9,     0,     0,    91,     0,
      89,     0,    78,     0,    87,     0,    85,    29,   147,   120,
     118,     0,   114,   117,    31,     0,     0,    40,     0,    95,
       0,    93,    61,     0,    63,    62,    64,     0,   122,     0,
      37,     0,     0,    67,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   130,     0,     0,   148,     0,   115,
      74,    24,     0,     0,    20,    34,    36,     0,    12,     0,
       0,    15,     0,     0,     0,     0,    14,     0,    35,     0,
      32,     0,     0,     0,     0,    16,    53,    70,   156,   147,
     128,   125,   161,   121,   124,    41,    39,    60,    69,     0,
      42,    43,    44,    45,    46,    52,    51,    50,    49,    48,
      47,    54,    55,    56,    57,    58,    72,    59,    66,   141,
     144,     0,   143,   147,   138,   133,   136,     0,   129,   135,
     153,   159,     0,    96,     6,   105,     0,     0,    21,   150,
       0,    90,    88,   158,     0,    86,    84,   119,   116,     0,
     158,    94,    92,     0,     0,     0,     0,     0,   142,   140,
      65,     0,   134,    73,   132,   154,     0,   113,     0,   104,
     107,     0,   110,    75,     0,     0,     0,    18,    19,     0,
       0,   127,    71,   123,    68,   139,   137,   131,   152,     0,
     109,   158,   111,   113,    22,   158,    25,    28,   126,   112,
     103,   106,   108,     0,     0,     0,   158,    27,    26,   101,
       0,   122,     7,   146,   159,     0,     0,    98,   102,   145
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 glish_yydefgoto[] =
{
      -1,     2,     3,   126,    43,    66,    44,   137,   204,    46,
      47,    48,    75,    76,    69,    70,    90,    91,    49,    50,
      51,    52,   276,   281,   238,   239,   240,   241,   260,    81,
      82,    83,   162,   163,   164,   197,   198,   199,   191,   192,
     284,    53,   128,    60,    54,    55,   246,   235,    98,   225
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -210
static const yytype_int16 glish_yypact[] =
{
     269,  -210,    19,   482,  -210,   572,   752,   -51,   617,   977,
     -44,  -210,   -10,   -40,   977,   977,    -6,   -38,   797,   662,
    -210,   977,   977,   -36,    -1,  -210,  -210,  -210,  -210,  -210,
    -210,   977,  -210,  -210,  -210,   977,   977,   977,   977,   -24,
     977,  -210,  -210,  -210,    -3,  -210,  -210,    63,  1138,  -210,
    -210,  -210,    37,  -210,     8,   977,  -210,     5,   977,  1138,
     -18,     8,  -210,  -210,    14,  -210,    17,    44,    57,   -16,
    -210,   977,    82,    -9,    66,   -14,  -210,  -210,    54,  -210,
    -210,    45,    84,  -210,  -210,    61,    87,    91,   977,    95,
     -12,  -210,    43,  1168,    22,    22,    82,    70,   842,  1045,
    -210,   482,   977,   106,   977,   977,   977,   977,   977,   977,
     977,   977,   977,   977,   977,   977,   977,   977,   977,   977,
     977,   977,   110,   887,   707,   -11,   340,  -210,    83,   797,
      82,  -210,    -8,   977,  -210,  -210,  -210,   139,  -210,   977,
     -10,  -210,    89,   977,   977,    -6,  -210,   977,  -210,   797,
    -210,   977,    90,   977,    -1,  -210,  1022,  -210,   124,   109,
    -210,  -210,  -210,   118,  -210,  -210,  -210,    43,  -210,   977,
    1138,  1198,  1228,  1258,  1288,  1318,  1318,  1318,  1318,  1318,
    1318,   -21,   -21,    39,    39,    39,    22,    22,  -210,   977,
    -210,    71,  -210,   111,  -210,   932,  -210,   114,   141,  -210,
    -210,  -210,   977,  -210,  -210,    92,   119,   977,  -210,     8,
     977,  -210,  -210,  -210,    -2,  -210,  -210,  -210,  -210,    20,
    -210,  -210,  -210,   116,   977,   120,   842,  1078,  -210,   977,
    -210,   977,  -210,  -210,   932,  -210,  1108,   117,   125,   147,
    -210,   160,  -210,  -210,    27,   128,   482,  -210,  -210,   482,
     977,  -210,  -210,  -210,  -210,  -210,  -210,  -210,  -210,   977,
    -210,  -210,   123,   117,  -210,  -210,   189,  -210,  -210,  -210,
     137,  -210,  -210,   482,   482,   134,  -210,  -210,  -210,  -210,
     527,   842,  -210,  1138,  -210,   135,   411,  -210,  -210,  -210
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 glish_yypgoto[] =
{
    -210,  -210,  -210,   -82,    -5,  -210,  -210,  -210,     4,  -210,
    -210,    59,  -210,    58,  -210,    64,  -210,    52,  -210,  -210,
    -210,  -210,  -210,  -210,  -210,  -210,   -54,  -210,   -52,    80,
    -210,    65,   -69,  -210,    -4,  -210,  -210,  -183,  -210,  -179,
    -210,   146,  -210,   -13,     0,  -200,  -209,   -68,  -210,  -210
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -112
static const yytype_int16 yytable[] =
{
      57,   207,    73,    64,    65,   242,    61,    45,    86,    87,
     228,   249,   232,    80,    85,    61,   143,   102,   103,     4,
      62,    61,    61,    67,   200,    68,   133,    71,   140,    74,
     145,    88,   154,    77,    89,   133,   133,   117,   118,   119,
     120,   121,   133,   122,   123,   132,   124,   201,    97,   125,
     255,   257,   270,   134,   202,   141,   273,   146,    61,   155,
     102,   103,   242,   208,   133,    59,   142,   280,   100,   247,
     101,   133,   127,    72,    59,   129,   131,   102,   103,    29,
      59,    59,   103,   152,   121,   135,   122,   123,   136,   124,
      92,   248,   125,   161,    93,    94,    95,    96,   264,    99,
     120,   121,   139,   122,   123,   166,   124,   122,   123,   125,
     124,   144,   151,   125,   130,   229,   148,    59,   190,   196,
     102,   103,    25,    26,    80,    27,   147,  -111,   149,   237,
     214,   133,   150,   209,   211,   133,   157,   230,   219,   215,
     153,   168,   217,    61,    80,   188,   122,   123,   221,   124,
     205,    61,   125,    25,    26,   210,    27,   213,   220,   223,
     237,   167,   226,   170,   171,   172,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,   185,   186,
     187,   224,   233,   231,   190,   234,   252,   243,   250,   259,
     196,   262,    59,   261,   244,   263,   265,   274,   275,   279,
     286,   288,    59,   216,   212,   245,   222,    61,   271,   206,
      59,   272,   285,   138,   218,   179,   287,     0,     0,   251,
       0,   161,   253,     0,   190,     0,   256,     0,   227,   196,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   268,     0,     0,     0,     0,
     266,     0,     0,   267,   269,     0,     0,     0,     0,     0,
       0,   236,     0,     0,     0,     0,    59,     0,     0,    -5,
       1,     0,     0,    -2,    -2,    -2,   161,   277,   278,    -2,
      -2,    -2,    -2,    -2,    -2,     0,    -2,    -2,    -2,    -2,
       0,    -2,    -2,    -2,     0,    -2,    -2,    -2,    -2,    -2,
      -2,     0,    -2,    -2,    -2,    -2,     0,    -2,     0,    -2,
      -2,    -2,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    -2,    -2,    -2,     0,     0,     0,
       0,     0,    -2,     0,    -2,     0,    -2,     0,     0,   283,
      -2,     0,     0,    -2,     5,     6,     7,     0,     0,     0,
       8,     9,    10,    11,    12,    13,     0,    14,    15,    16,
      17,     0,    18,    19,    20,     0,    21,    22,    23,    24,
      25,    26,     0,    27,    28,    29,    30,     0,    31,     0,
      32,    33,    34,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    35,    36,    37,     0,     0,
       0,     0,     0,    38,     0,    39,     0,    40,     0,     0,
       0,    41,     0,   203,    42,     5,     6,     7,     0,     0,
       0,     8,     9,    10,    11,    12,    13,     0,    14,    15,
      16,    17,     0,    18,    19,    20,     0,    21,    22,    23,
      24,    25,    26,     0,    27,    28,    29,    30,     0,    31,
       0,    32,    33,    34,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    35,    36,    37,     0,
       0,     0,     0,     0,    38,     0,    39,     0,    40,     0,
       0,     0,    41,     0,   289,    42,     5,     6,     7,     0,
       0,     0,     8,     9,    10,    11,    12,    13,     0,    14,
      15,    16,    17,     0,    18,    19,    20,     0,    21,    22,
      23,    24,    25,    26,     0,    27,    28,    29,    30,     0,
      31,     0,    32,    33,    34,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    35,    36,    37,
      11,     0,     0,     0,    14,    38,     0,    39,     0,    40,
       0,    20,     0,    41,     0,     0,    42,    25,    26,     0,
      27,    28,    29,    30,     0,    31,     0,    32,    33,    34,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    35,    36,    37,    11,     0,     0,     0,    14,
      38,     0,    39,     0,    40,     0,    20,     0,     0,     0,
       0,   282,    25,    26,     0,    27,    28,    29,    30,     0,
      31,     0,    32,    33,    34,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    35,    36,    37,
      11,     0,     0,     0,    14,    38,     0,    39,     0,    40,
       0,    20,     0,    56,     0,     0,     0,    25,    26,     0,
      27,    28,    29,    30,     0,    31,     0,    32,    33,    34,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    35,    36,    37,    11,     0,     0,     0,    14,
      38,     0,    39,     0,    40,     0,    20,     0,    63,     0,
       0,     0,    25,    26,     0,    27,    28,    29,    30,     0,
      31,     0,    32,    33,    34,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    35,    36,    37,
      11,     0,     0,     0,    14,    38,     0,    39,     0,    40,
       0,    20,     0,    84,     0,     0,     0,    25,    26,     0,
      27,    28,   193,    30,   194,    31,     0,    32,    33,    34,
       0,   195,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    35,    36,    37,    11,     0,     0,     0,    14,
      38,     0,    39,    58,    40,     0,    20,     0,     0,     0,
       0,     0,    25,    26,     0,    27,    28,    29,    30,     0,
      31,     0,    32,    33,    34,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    35,    36,    37,
      11,     0,     0,     0,    14,    38,     0,    39,     0,    40,
       0,    20,     0,     0,     0,     0,     0,    25,    26,     0,
      27,    28,    78,    30,    79,    31,     0,    32,    33,    34,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    35,    36,    37,    11,     0,     0,     0,    14,
      38,     0,    39,     0,    40,     0,    20,     0,     0,     0,
       0,     0,    25,    26,     0,   158,    28,   159,    30,   160,
      31,     0,    32,    33,    34,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    35,    36,    37,
      11,     0,     0,     0,    14,    38,     0,    39,     0,    40,
       0,    20,     0,     0,     0,     0,     0,    25,    26,     0,
      27,    28,    29,    30,     0,    31,     0,    32,    33,    34,
       0,   189,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    35,    36,    37,    11,     0,     0,     0,    14,
      38,     0,    39,     0,    40,     0,    20,     0,     0,     0,
       0,     0,    25,    26,     0,    27,    28,   193,    30,   194,
      31,     0,    32,    33,    34,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    35,    36,    37,
      11,     0,     0,     0,    14,    38,     0,    39,     0,    40,
       0,    20,     0,     0,     0,     0,     0,    25,    26,     0,
      27,    28,    29,    30,     0,    31,     0,    32,    33,    34,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    35,    36,    37,    11,     0,     0,     0,    14,
      38,     0,    39,     0,    40,     0,    20,     0,     0,     0,
       0,     0,    25,    26,     0,    27,    28,    29,    30,     0,
      31,     0,    32,    33,    34,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,  -112,    36,    37,
       0,     0,     0,   102,   103,    38,     0,    39,     0,    40,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,     0,   122,
     123,     0,   124,   165,     0,   125,   102,   103,     0,     0,
       0,     0,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,     0,   122,   123,   254,   124,   102,   103,   125,     0,
       0,     0,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,     0,   122,   123,   258,   124,   102,   103,   125,     0,
       0,     0,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,     0,   122,   123,     0,   124,   102,   103,   125,     0,
       0,     0,     0,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   156,   114,   115,   116,   117,   118,   119,   120,
     121,     0,   122,   123,     0,   124,   102,   103,   125,     0,
       0,     0,     0,     0,     0,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,     0,   122,   123,     0,   124,   102,   103,   125,     0,
       0,     0,     0,     0,     0,     0,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,     0,   122,   123,     0,   124,   102,   103,   125,     0,
       0,     0,     0,     0,     0,     0,     0,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,     0,   122,   123,     0,   124,   102,   103,   125,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,     0,   122,   123,     0,   124,   102,   103,   125,     0,
       0,     0,     0,     0,     0,     0,     0,     0,  -112,  -112,
    -112,  -112,  -112,  -112,   115,   116,   117,   118,   119,   120,
     121,     0,   122,   123,     0,   124,     0,     0,   125
};

static const yytype_int16 yycheck[] =
{
       5,     9,    15,     8,     9,   205,     6,     3,    21,    22,
     189,   220,   195,    18,    19,    15,    25,    38,    39,     0,
      71,    21,    22,    67,    35,    35,    44,    67,    44,    35,
      44,    67,    44,    71,    35,    44,    44,    58,    59,    60,
      61,    62,    44,    64,    65,    58,    67,    58,    72,    70,
     229,   234,   261,    71,    65,    71,   265,    71,    58,    71,
      38,    39,   262,    71,    44,     6,    71,   276,    71,    71,
       7,    44,    35,    14,    15,    67,    71,    38,    39,    35,
      21,    22,    39,    88,    62,    71,    64,    65,    71,    67,
      31,    71,    70,    98,    35,    36,    37,    38,    71,    40,
      61,    62,    45,    64,    65,   101,    67,    64,    65,    70,
      67,    45,    25,    70,    55,    44,    71,    58,   123,   124,
      38,    39,    30,    31,   129,    33,    72,    35,    44,    37,
     143,    44,    71,   133,   139,    44,    66,    66,   151,   144,
      45,    35,   147,   143,   149,    35,    64,    65,   153,    67,
      67,   151,    70,    30,    31,    16,    33,    68,    68,    35,
      37,   102,    44,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,    72,    68,    72,   189,    44,    66,    68,    72,    72,
     195,    44,   133,    68,   207,    35,    68,     8,    61,    65,
     282,    66,   143,   145,   140,   210,   154,   207,   262,   129,
     151,   263,   281,    67,   149,   156,   284,    -1,    -1,   224,
      -1,   226,   226,    -1,   229,    -1,   231,    -1,   169,   234,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   250,    -1,    -1,    -1,    -1,
     246,    -1,    -1,   249,   259,    -1,    -1,    -1,    -1,    -1,
      -1,   202,    -1,    -1,    -1,    -1,   207,    -1,    -1,     0,
       1,    -1,    -1,     4,     5,     6,   281,   273,   274,    10,
      11,    12,    13,    14,    15,    -1,    17,    18,    19,    20,
      -1,    22,    23,    24,    -1,    26,    27,    28,    29,    30,
      31,    -1,    33,    34,    35,    36,    -1,    38,    -1,    40,
      41,    42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    55,    56,    57,    -1,    -1,    -1,
      -1,    -1,    63,    -1,    65,    -1,    67,    -1,    -1,   280,
      71,    -1,    -1,    74,     4,     5,     6,    -1,    -1,    -1,
      10,    11,    12,    13,    14,    15,    -1,    17,    18,    19,
      20,    -1,    22,    23,    24,    -1,    26,    27,    28,    29,
      30,    31,    -1,    33,    34,    35,    36,    -1,    38,    -1,
      40,    41,    42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    55,    56,    57,    -1,    -1,
      -1,    -1,    -1,    63,    -1,    65,    -1,    67,    -1,    -1,
      -1,    71,    -1,    73,    74,     4,     5,     6,    -1,    -1,
      -1,    10,    11,    12,    13,    14,    15,    -1,    17,    18,
      19,    20,    -1,    22,    23,    24,    -1,    26,    27,    28,
      29,    30,    31,    -1,    33,    34,    35,    36,    -1,    38,
      -1,    40,    41,    42,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    55,    56,    57,    -1,
      -1,    -1,    -1,    -1,    63,    -1,    65,    -1,    67,    -1,
      -1,    -1,    71,    -1,    73,    74,     4,     5,     6,    -1,
      -1,    -1,    10,    11,    12,    13,    14,    15,    -1,    17,
      18,    19,    20,    -1,    22,    23,    24,    -1,    26,    27,
      28,    29,    30,    31,    -1,    33,    34,    35,    36,    -1,
      38,    -1,    40,    41,    42,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    55,    56,    57,
      13,    -1,    -1,    -1,    17,    63,    -1,    65,    -1,    67,
      -1,    24,    -1,    71,    -1,    -1,    74,    30,    31,    -1,
      33,    34,    35,    36,    -1,    38,    -1,    40,    41,    42,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    55,    56,    57,    13,    -1,    -1,    -1,    17,
      63,    -1,    65,    -1,    67,    -1,    24,    -1,    -1,    -1,
      -1,    74,    30,    31,    -1,    33,    34,    35,    36,    -1,
      38,    -1,    40,    41,    42,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    55,    56,    57,
      13,    -1,    -1,    -1,    17,    63,    -1,    65,    -1,    67,
      -1,    24,    -1,    71,    -1,    -1,    -1,    30,    31,    -1,
      33,    34,    35,    36,    -1,    38,    -1,    40,    41,    42,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    55,    56,    57,    13,    -1,    -1,    -1,    17,
      63,    -1,    65,    -1,    67,    -1,    24,    -1,    71,    -1,
      -1,    -1,    30,    31,    -1,    33,    34,    35,    36,    -1,
      38,    -1,    40,    41,    42,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    55,    56,    57,
      13,    -1,    -1,    -1,    17,    63,    -1,    65,    -1,    67,
      -1,    24,    -1,    71,    -1,    -1,    -1,    30,    31,    -1,
      33,    34,    35,    36,    37,    38,    -1,    40,    41,    42,
      -1,    44,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    55,    56,    57,    13,    -1,    -1,    -1,    17,
      63,    -1,    65,    21,    67,    -1,    24,    -1,    -1,    -1,
      -1,    -1,    30,    31,    -1,    33,    34,    35,    36,    -1,
      38,    -1,    40,    41,    42,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    55,    56,    57,
      13,    -1,    -1,    -1,    17,    63,    -1,    65,    -1,    67,
      -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,
      33,    34,    35,    36,    37,    38,    -1,    40,    41,    42,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    55,    56,    57,    13,    -1,    -1,    -1,    17,
      63,    -1,    65,    -1,    67,    -1,    24,    -1,    -1,    -1,
      -1,    -1,    30,    31,    -1,    33,    34,    35,    36,    37,
      38,    -1,    40,    41,    42,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    55,    56,    57,
      13,    -1,    -1,    -1,    17,    63,    -1,    65,    -1,    67,
      -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,
      33,    34,    35,    36,    -1,    38,    -1,    40,    41,    42,
      -1,    44,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    55,    56,    57,    13,    -1,    -1,    -1,    17,
      63,    -1,    65,    -1,    67,    -1,    24,    -1,    -1,    -1,
      -1,    -1,    30,    31,    -1,    33,    34,    35,    36,    37,
      38,    -1,    40,    41,    42,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    55,    56,    57,
      13,    -1,    -1,    -1,    17,    63,    -1,    65,    -1,    67,
      -1,    24,    -1,    -1,    -1,    -1,    -1,    30,    31,    -1,
      33,    34,    35,    36,    -1,    38,    -1,    40,    41,    42,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    55,    56,    57,    13,    -1,    -1,    -1,    17,
      63,    -1,    65,    -1,    67,    -1,    24,    -1,    -1,    -1,
      -1,    -1,    30,    31,    -1,    33,    34,    35,    36,    -1,
      38,    -1,    40,    41,    42,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    55,    56,    57,
      -1,    -1,    -1,    38,    39,    63,    -1,    65,    -1,    67,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    -1,    64,
      65,    -1,    67,    68,    -1,    70,    38,    39,    -1,    -1,
      -1,    -1,    -1,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    -1,    64,    65,    66,    67,    38,    39,    70,    -1,
      -1,    -1,    -1,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    -1,    64,    65,    66,    67,    38,    39,    70,    -1,
      -1,    -1,    -1,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    -1,    64,    65,    -1,    67,    38,    39,    70,    -1,
      -1,    -1,    -1,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    -1,    64,    65,    -1,    67,    38,    39,    70,    -1,
      -1,    -1,    -1,    -1,    -1,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    -1,    64,    65,    -1,    67,    38,    39,    70,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    -1,    64,    65,    -1,    67,    38,    39,    70,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    -1,    64,    65,    -1,    67,    38,    39,    70,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    -1,    64,    65,    -1,    67,    38,    39,    70,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    -1,    64,    65,    -1,    67,    -1,    -1,    70
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     1,    76,    77,     0,     4,     5,     6,    10,    11,
      12,    13,    14,    15,    17,    18,    19,    20,    22,    23,
      24,    26,    27,    28,    29,    30,    31,    33,    34,    35,
      36,    38,    40,    41,    42,    55,    56,    57,    63,    65,
      67,    71,    74,    79,    81,    83,    84,    85,    86,    93,
      94,    95,    96,   116,   119,   120,    71,    79,    21,    86,
     118,   119,    71,    71,    79,    79,    80,    67,    35,    89,
      90,    67,    86,   118,    35,    87,    88,    71,    35,    37,
      79,   104,   105,   106,    71,    79,   118,   118,    67,    35,
      91,    92,    86,    86,    86,    86,    86,    72,   123,    86,
      71,     7,    38,    39,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    64,    65,    67,    70,    78,    35,   117,    67,
      86,    71,   118,    44,    71,    71,    71,    82,   116,    45,
      44,    71,    79,    25,    45,    44,    71,    72,    71,    44,
      71,    25,    79,    45,    44,    71,    54,    66,    33,    35,
      37,    79,   107,   108,   109,    68,    83,    86,    35,    65,
      86,    86,    86,    86,    86,    86,    86,    86,    86,    86,
      86,    86,    86,    86,    86,    86,    86,    86,    35,    44,
      79,   113,   114,    35,    37,    44,    79,   110,   111,   112,
      35,    58,    65,    73,    83,    67,   104,     9,    71,   119,
      16,    79,    90,    68,   118,    79,    88,    79,   106,   118,
      68,    79,    92,    35,    72,   124,    44,    86,   114,    44,
      66,    72,   112,    68,    44,   122,    86,    37,    99,   100,
     101,   102,   120,    68,   118,    79,   121,    71,    71,   121,
      72,    79,    66,   109,    66,   114,    79,   112,    66,    72,
     103,    68,    44,    35,    71,    68,    83,    83,    79,    79,
     121,   101,   103,   121,     8,    61,    97,    83,    83,    65,
     121,    98,    74,    86,   115,   107,    78,   122,    66,    73
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = glish_yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = glish_yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = glish_yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto glish_yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto glish_yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| glish_yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
glish_yydefault:
  yyn = glish_yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = glish_yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 143 "../../parse.y"
    {first_line = 0;}
    break;

  case 3:
#line 144 "../../parse.y"
    {
			first_line = 1;
			glish_regex_matched = 0;
			if ( interactive( ) )
				status = 0;

			if ( ! lookahead )
				{
				lookahead = & ( yyclearin );
				empty_token = *lookahead;
				}

			cur_stmt = (yyvsp[(2) - (2)].stmt);

			if ( *lookahead != empty_token )
				putback_token( *lookahead );

			YYACCEPT;
			}
    break;

  case 4:
#line 165 "../../parse.y"
    {
			first_line = 1;
			status = 1;
			if ( interactive( ) )
				{
				// Try to throw away the rest of the line.
				set_statement_can_end( );
				first_line = 1;
				YYACCEPT;
				}
			else
				{
				YYABORT;
				}
			}
    break;

  case 5:
#line 181 "../../parse.y"
    {
			first_line = 1;
			YYABORT;
			}
    break;

  case 6:
#line 191 "../../parse.y"
    { (yyval.stmt) = merge_stmts( (yyvsp[(1) - (2)].stmt), (yyvsp[(2) - (2)].stmt) ); }
    break;

  case 7:
#line 193 "../../parse.y"
    { (yyval.stmt) = null_stmt; }
    break;

  case 8:
#line 199 "../../parse.y"
    { (yyval.expr) = (yyval.expr)->BuildFrameInfo( SCOPE_UNKNOWN ); }
    break;

  case 10:
#line 205 "../../parse.y"
    { (yyval.expr) = 0; }
    break;

  case 11:
#line 210 "../../parse.y"
    { (yyval.expr)->StandAlone( ); }
    break;

  case 12:
#line 216 "../../parse.y"
    { (yyval.expr) = (yyval.expr)->BuildFrameInfo( SCOPE_LHS ); }
    break;

  case 14:
#line 223 "../../parse.y"
    { (yyval.stmt) = (yyvsp[(2) - (3)].stmt); }
    break;

  case 15:
#line 226 "../../parse.y"
    { (yyval.stmt) = (yyvsp[(2) - (3)].stmt); }
    break;

  case 16:
#line 229 "../../parse.y"
    { (yyval.stmt) = (yyvsp[(2) - (3)].stmt); }
    break;

  case 18:
#line 234 "../../parse.y"
    { (yyval.stmt) = new LinkStmt( (yyvsp[(2) - (5)].ev_list), (yyvsp[(4) - (5)].ev_list), current_sequencer ); }
    break;

  case 19:
#line 237 "../../parse.y"
    { (yyval.stmt) = new UnLinkStmt( (yyvsp[(2) - (5)].ev_list), (yyvsp[(4) - (5)].ev_list), current_sequencer ); }
    break;

  case 20:
#line 240 "../../parse.y"
    {
			(yyval.stmt) = new AwaitStmt( (yyvsp[(2) - (3)].ev_list), 0, 0, current_sequencer );
			}
    break;

  case 21:
#line 244 "../../parse.y"
    {
			(yyval.stmt) = new AwaitStmt( (yyvsp[(3) - (4)].ev_list), 1, 0, current_sequencer );
			}
    break;

  case 22:
#line 248 "../../parse.y"
    {
			(yyval.stmt) = new AwaitStmt( (yyvsp[(3) - (6)].ev_list), 1, (yyvsp[(5) - (6)].ev_list), current_sequencer );
			}
    break;

  case 23:
#line 253 "../../parse.y"
    { (yyval.stmt) = new ActivateStmt( (yyvsp[(1) - (2)].ival), 0, current_sequencer ); }
    break;

  case 24:
#line 256 "../../parse.y"
    { (yyval.stmt) = new ActivateStmt( (yyvsp[(1) - (3)].ival), (yyvsp[(2) - (3)].expr), current_sequencer ); }
    break;

  case 25:
#line 259 "../../parse.y"
    { (yyval.stmt) = new IfStmt( (yyvsp[(3) - (6)].expr), (yyvsp[(6) - (6)].stmt), 0 ); }
    break;

  case 26:
#line 261 "../../parse.y"
    { (yyval.stmt) = new IfStmt( (yyvsp[(3) - (8)].expr), (yyvsp[(6) - (8)].stmt), (yyvsp[(8) - (8)].stmt) ); }
    break;

  case 27:
#line 264 "../../parse.y"
    {
			(yyval.stmt) = new ForStmt( (yyvsp[(3) - (8)].expr), (yyvsp[(5) - (8)].expr), (yyvsp[(8) - (8)].stmt) );
			}
    break;

  case 28:
#line 269 "../../parse.y"
    { (yyval.stmt) = new WhileStmt( (yyvsp[(3) - (6)].expr), (yyvsp[(6) - (6)].stmt) ); }
    break;

  case 29:
#line 272 "../../parse.y"
    { (yyval.stmt) = new LoopStmt; }
    break;

  case 30:
#line 275 "../../parse.y"
    { (yyval.stmt) = new BreakStmt; }
    break;

  case 31:
#line 278 "../../parse.y"
    { (yyval.stmt) = new ReturnStmt( 0 ); }
    break;

  case 32:
#line 281 "../../parse.y"
    { (yyval.stmt) = new ReturnStmt( (yyvsp[(2) - (3)].expr) ); }
    break;

  case 33:
#line 284 "../../parse.y"
    { (yyval.stmt) = new ExitStmt( 0, current_sequencer ); }
    break;

  case 34:
#line 287 "../../parse.y"
    { (yyval.stmt) = new ExitStmt( (yyvsp[(2) - (3)].expr), current_sequencer ); }
    break;

  case 35:
#line 290 "../../parse.y"
    { (yyval.stmt) = new PrintStmt( (yyvsp[(2) - (3)].param_list) ); }
    break;

  case 36:
#line 293 "../../parse.y"
    { (yyval.stmt) = new FailStmt( (yyvsp[(2) - (3)].expr) ); }
    break;

  case 37:
#line 296 "../../parse.y"
    { (yyval.stmt) = new ExprStmt( (yyvsp[(1) - (2)].expr) ); }
    break;

  case 38:
#line 299 "../../parse.y"
    { (yyval.stmt) = null_stmt; }
    break;

  case 39:
#line 303 "../../parse.y"
    {
			((WheneverStmtCtor*) (yyvsp[(1) - (3)].stmt))->SetStmt((yyvsp[(3) - (3)].stmt));
			(yyval.stmt) = (yyvsp[(1) - (3)].stmt);
			}
    break;

  case 40:
#line 310 "../../parse.y"
    {
		        (yyval.stmt) = new WheneverStmtCtor( (yyvsp[(2) - (2)].ev_list), current_sequencer );
			}
    break;

  case 41:
#line 317 "../../parse.y"
    { (yyval.expr) = (yyvsp[(2) - (3)].expr); }
    break;

  case 42:
#line 320 "../../parse.y"
    { (yyval.expr) = compound_assignment( (yyvsp[(1) - (3)].expr), (yyvsp[(2) - (3)].ival), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 43:
#line 323 "../../parse.y"
    { (yyval.expr) = new OrExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 44:
#line 325 "../../parse.y"
    { (yyval.expr) = new AndExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 45:
#line 328 "../../parse.y"
    { (yyval.expr) = new LogOrExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 46:
#line 330 "../../parse.y"
    { (yyval.expr) = new LogAndExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 47:
#line 333 "../../parse.y"
    { (yyval.expr) = new LT_Expr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 48:
#line 335 "../../parse.y"
    { (yyval.expr) = new GT_Expr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 49:
#line 337 "../../parse.y"
    { (yyval.expr) = new LE_Expr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 50:
#line 339 "../../parse.y"
    { (yyval.expr) = new GE_Expr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 51:
#line 341 "../../parse.y"
    { (yyval.expr) = new EQ_Expr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 52:
#line 343 "../../parse.y"
    { (yyval.expr) = new NE_Expr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 53:
#line 346 "../../parse.y"
    {
			Expr* id = current_sequencer->LookupID( string_dup("_"), LOCAL_SCOPE, 1, 0, 0 );
			Ref(id);
			(yyval.expr) = compound_assignment( id, 0, new GenerateExpr((yyvsp[(2) - (3)].expr)) );
			}
    break;

  case 54:
#line 353 "../../parse.y"
    { (yyval.expr) = new AddExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 55:
#line 355 "../../parse.y"
    { (yyval.expr) = new SubtractExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 56:
#line 357 "../../parse.y"
    { (yyval.expr) = new MultiplyExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 57:
#line 359 "../../parse.y"
    { (yyval.expr) = new DivideExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 58:
#line 361 "../../parse.y"
    { (yyval.expr) = new ModuloExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 59:
#line 363 "../../parse.y"
    { (yyval.expr) = new PowerExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 60:
#line 366 "../../parse.y"
    {
			if ( (yyvsp[(2) - (3)].ival) == '!' )
				(yyval.expr) = new NotExpr( new ApplyRegExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr), current_sequencer, (yyvsp[(2) - (3)].ival) ) );
			else
				(yyval.expr) = new ApplyRegExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr), current_sequencer, (yyvsp[(2) - (3)].ival) );
			}
    break;

  case 61:
#line 374 "../../parse.y"
    {
			Expr* id = current_sequencer->LookupID( string_dup("_"), ANY_SCOPE, 0, 0 );
			if ( ! id ) id = current_sequencer->InstallID( string_dup("_"), LOCAL_SCOPE, 0 );
			Ref(id);

			if ( (yyvsp[(1) - (2)].ival) == '!' )
				(yyval.expr) = new NotExpr( new ApplyRegExpr( id, (yyvsp[(2) - (2)].expr), current_sequencer, (yyvsp[(1) - (2)].ival) ) );
			else
				(yyval.expr) = new ApplyRegExpr( id, (yyvsp[(2) - (2)].expr), current_sequencer, (yyvsp[(1) - (2)].ival) );
			}
    break;

  case 62:
#line 386 "../../parse.y"
    { (yyval.expr) = new NegExpr( (yyvsp[(2) - (2)].expr) ); }
    break;

  case 63:
#line 388 "../../parse.y"
    { (yyval.expr) = (yyvsp[(2) - (2)].expr); }
    break;

  case 64:
#line 390 "../../parse.y"
    { (yyval.expr) = new NotExpr( (yyvsp[(2) - (2)].expr) ); }
    break;

  case 65:
#line 393 "../../parse.y"
    { (yyval.expr) = new ArrayRefExpr( (yyvsp[(1) - (4)].expr), (yyvsp[(3) - (4)].exprlist) ); }
    break;

  case 66:
#line 396 "../../parse.y"
    { (yyval.expr) = new RecordRefExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].id) ); }
    break;

  case 67:
#line 399 "../../parse.y"
    { (yyval.expr) = new AttributeRefExpr( (yyvsp[(1) - (2)].expr) ); }
    break;

  case 68:
#line 402 "../../parse.y"
    { (yyval.expr) = new AttributeRefExpr( (yyvsp[(1) - (5)].expr), (yyvsp[(4) - (5)].expr) ); }
    break;

  case 69:
#line 405 "../../parse.y"
    { (yyval.expr) = new AttributeRefExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].id) ); }
    break;

  case 70:
#line 408 "../../parse.y"
    { (yyval.expr) = new ConstructExpr( 0 ); }
    break;

  case 71:
#line 411 "../../parse.y"
    { (yyval.expr) = new ConstructExpr( (yyvsp[(3) - (5)].param_list) ); }
    break;

  case 72:
#line 414 "../../parse.y"
    { (yyval.expr) = new RangeExpr( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr) ); }
    break;

  case 73:
#line 417 "../../parse.y"
    { (yyval.expr) = new CallExpr( (yyvsp[(1) - (4)].expr), (yyvsp[(3) - (4)].param_list), current_sequencer ); }
    break;

  case 74:
#line 420 "../../parse.y"
    { (yyval.expr) = new RefExpr( (yyvsp[(2) - (2)].expr), (yyvsp[(1) - (2)].val_type) ); }
    break;

  case 75:
#line 423 "../../parse.y"
    {
			(yyval.expr) = new SendEventExpr( (yyvsp[(1) - (4)].event), (yyvsp[(3) - (4)].param_list) );
			}
    break;

  case 76:
#line 428 "../../parse.y"
    { (yyval.expr) = new LastEventExpr( current_sequencer, (yyvsp[(1) - (1)].event_type) ); }
    break;

  case 77:
#line 431 "../../parse.y"
    { (yyval.expr) = new LastRegexExpr( current_sequencer, (yyvsp[(1) - (1)].regex_type) ); }
    break;

  case 78:
#line 434 "../../parse.y"
    { (yyval.expr) = new IncludeExpr( (yyvsp[(2) - (2)].expr), current_sequencer ); }
    break;

  case 83:
#line 445 "../../parse.y"
    {
			error_reset();
			YYERROR;
			}
    break;

  case 84:
#line 453 "../../parse.y"
    { (yyval.stmt) = merge_stmts( (yyvsp[(1) - (3)].stmt), (yyvsp[(3) - (3)].stmt) ); }
    break;

  case 86:
#line 458 "../../parse.y"
    {
			Expr* id = current_sequencer->LookupID( (yyvsp[(1) - (3)].id), LOCAL_SCOPE, 1, 0, 0 );

			Ref(id);
			(yyval.stmt) = new ExprStmt( compound_assignment( id, (yyvsp[(2) - (3)].ival), (yyvsp[(3) - (3)].expr) ) );

			if ( (yyvsp[(2) - (3)].ival) != 0 )
				glish_warn->Report( "compound assignment in", (yyval.stmt) );
			}
    break;

  case 87:
#line 468 "../../parse.y"
    {
			(void) current_sequencer->LookupID( (yyvsp[(1) - (1)].id), LOCAL_SCOPE, 1, 0, 0, 1 );
			(yyval.stmt) = null_stmt;
			}
    break;

  case 88:
#line 476 "../../parse.y"
    { (yyval.stmt) = merge_stmts( (yyvsp[(1) - (3)].stmt), (yyvsp[(3) - (3)].stmt) ); }
    break;

  case 90:
#line 481 "../../parse.y"
    {
			Expr* id =
				current_sequencer->LookupID( (yyvsp[(1) - (3)].id), GLOBAL_SCOPE, 1, 0 );

			Ref(id);
			(yyval.stmt) = new ExprStmt( compound_assignment( id, (yyvsp[(2) - (3)].ival), (yyvsp[(3) - (3)].expr) ) );

			if ( (yyvsp[(2) - (3)].ival) != 0 )
				glish_warn->Report( "compound assignment in", (yyval.stmt) );
			}
    break;

  case 91:
#line 492 "../../parse.y"
    {
			(void) current_sequencer->LookupID( (yyvsp[(1) - (1)].id), GLOBAL_SCOPE, 1, 0, 1, 1 );
			(yyval.stmt) = null_stmt;
			}
    break;

  case 92:
#line 500 "../../parse.y"
    { (yyval.stmt) = merge_stmts( (yyvsp[(1) - (3)].stmt), (yyvsp[(3) - (3)].stmt) ); }
    break;

  case 94:
#line 505 "../../parse.y"
    {
			Expr* id =
				current_sequencer->LookupID( string_dup((yyvsp[(1) - (3)].id)), ANY_SCOPE, 0, 0 );

			if ( ! id )
				{
				glish_warn->Report( "\"",(yyvsp[(1) - (3)].id),"\" ", "not found in 'wider', non-global scope; defaults to 'local'");
				id = current_sequencer->InstallID( (yyvsp[(1) - (3)].id), LOCAL_SCOPE, 0 );
				}
			else
				free_memory( (yyvsp[(1) - (3)].id) );

			Ref(id);
			(yyval.stmt) = new ExprStmt( compound_assignment( id, (yyvsp[(2) - (3)].ival), (yyvsp[(3) - (3)].expr) ) );

			if ( (yyvsp[(2) - (3)].ival) != 0 )
				glish_warn->Report( "compound assignment in", (yyval.stmt) );
			}
    break;

  case 95:
#line 524 "../../parse.y"
    {
			Expr* id =
				current_sequencer->LookupID( string_dup((yyvsp[(1) - (1)].id)), ANY_SCOPE, 0, 0 );

			if ( ! id )
				{
				glish_warn->Report( "\"",(yyvsp[(1) - (1)].id),"\" ", "not found in 'wider', non-global scope; defaults to 'local'");
				id = current_sequencer->InstallID( (yyvsp[(1) - (1)].id), LOCAL_SCOPE, 0 );
				}
			else
				free_memory( (yyvsp[(1) - (1)].id) );

			(yyval.stmt) = null_stmt;
			}
    break;

  case 96:
#line 542 "../../parse.y"
    {
			int frame_size = current_sequencer->PopScope();

			(yyval.stmt) = new StmtBlock( frame_size, (yyvsp[(2) - (3)].stmt), current_sequencer );
			}
    break;

  case 97:
#line 550 "../../parse.y"
    {
			current_sequencer->PushScope();
			(yyval.expr) = 0;
			}
    break;

  case 98:
#line 558 "../../parse.y"
    {
			IValue *attributes = (yyvsp[(7) - (10)].expr) ? (yyvsp[(7) - (10)].expr)->CopyEval(*eval_options) : 0;
			Unref((yyvsp[(7) - (10)].expr));

			back_offsets_type *back_refs = 0;
			int frame_size = current_sequencer->PopScope( &back_refs );
			IValue *err = 0;

			UserFunc* ufunc = new UserFunc( (yyvsp[(4) - (10)].param_list), (yyvsp[(9) - (10)].stmt), frame_size, back_refs,
							current_sequencer, (yyvsp[(1) - (10)].expr), err,
							attributes);

			if ( ! err )
				{
				(yyval.expr) = new FuncExpr( ufunc, attributes );

				if ( (yyvsp[(2) - (10)].id) )
					{
					if ( current_sequencer->GetScopeType() == GLOBAL_SCOPE )
						{ 
						// Create global reference to function.
						Expr* func =
							current_sequencer->LookupID(
							(yyvsp[(2) - (10)].id), LOCAL_SCOPE );

						IValue* ref = new IValue( ufunc );
						if ( attributes ) ref->AssignAttributes(copy_value(attributes));
						/* keep ufunc from being deleted with $$ */
						Ref(ufunc);

						if ( err = func->Assign( *eval_options, ref ) )
							{
							Unref( (yyval.expr) );
							(yyval.expr) = new ConstExpr( err );
							}
						}
					else
						{
						Expr *lhs = 
							current_sequencer->LookupID( (yyvsp[(2) - (10)].id), LOCAL_SCOPE);
						Ref(lhs);
						(yyval.expr) = compound_assignment( lhs, 0, (yyval.expr) );
						}
					}
				}
			else
				{
				(yyval.expr) = new ValExpr( err );
				Ref(err);
				Unref( ufunc );
				}

			int tmplen;
			if ( (yyvsp[(1) - (10)].expr) && (tmplen = glish_current_subsequence->length()) )
				glish_current_subsequence->remove_nth(tmplen-1);
			}
    break;

  case 99:
#line 617 "../../parse.y"
    {
			current_sequencer->PushScope( FUNC_SCOPE );
			(yyval.expr) = 0;
			}
    break;

  case 100:
#line 623 "../../parse.y"
    {
			current_sequencer->PushScope( FUNC_SCOPE );
			(yyval.expr) = current_sequencer->InstallID( string_dup( "self" ), LOCAL_SCOPE );
			glish_current_subsequence->append((yyval.expr));
			Ref((yyval.expr));
			}
    break;

  case 101:
#line 632 "../../parse.y"
    { current_sequencer->StashScope(); }
    break;

  case 102:
#line 633 "../../parse.y"
    {
			(yyval.expr) = new ConstructExpr( (yyvsp[(4) - (5)].param_list) );
			current_sequencer->RestoreScope();
			}
    break;

  case 103:
#line 638 "../../parse.y"
    { (yyval.expr) = 0; }
    break;

  case 105:
#line 644 "../../parse.y"
    { (yyval.param_list) = new parameter_list; }
    break;

  case 106:
#line 648 "../../parse.y"
    { (yyvsp[(1) - (3)].param_list)->append( (yyvsp[(3) - (3)].param) ); }
    break;

  case 107:
#line 651 "../../parse.y"
    {
			(yyval.param_list) = new parameter_list;
			(yyval.param_list)->append( (yyvsp[(1) - (1)].param) );
			}
    break;

  case 108:
#line 658 "../../parse.y"
    {
			Expr* id = current_sequencer->LookupID( string_dup((yyvsp[(2) - (3)].id)), LOCAL_SCOPE, 0, 0 );
			if ( id )
				{
				yyerror("multiple parameters with same name");
				YYERROR;
				}

			Expr* param = current_sequencer->InstallID( (yyvsp[(2) - (3)].id), LOCAL_SCOPE );
			Ref(param);
			(yyval.param) = new FormalParameter( (const char *) (yyvsp[(2) - (3)].id), (yyvsp[(1) - (3)].val_type), param, 0, (yyvsp[(3) - (3)].expr) );
			}
    break;

  case 109:
#line 672 "../../parse.y"
    {
			Expr* ellipsis = current_sequencer->LookupID(string_dup("..."), LOCAL_SCOPE, 1, 0 );

			Ref(ellipsis);
			(yyval.param) = new FormalParameter( VAL_CONST, ellipsis, 1, (yyvsp[(2) - (2)].expr) );
			}
    break;

  case 111:
#line 683 "../../parse.y"
    { (yyval.val_type) = VAL_VAL; }
    break;

  case 112:
#line 688 "../../parse.y"
    { (yyval.expr) = (yyvsp[(2) - (2)].expr); }
    break;

  case 113:
#line 690 "../../parse.y"
    { (yyval.expr) = 0; }
    break;

  case 115:
#line 696 "../../parse.y"
    { (yyval.param_list) = new parameter_list; }
    break;

  case 116:
#line 700 "../../parse.y"
    { (yyvsp[(1) - (3)].param_list)->append( (yyvsp[(3) - (3)].param) ); }
    break;

  case 117:
#line 703 "../../parse.y"
    {
			(yyval.param_list) = new parameter_list;
			(yyval.param_list)->append( (yyvsp[(1) - (1)].param) );
			}
    break;

  case 118:
#line 710 "../../parse.y"
    { (yyval.param) = new ActualParameter( VAL_VAL, (yyvsp[(1) - (1)].expr) ); }
    break;

  case 119:
#line 713 "../../parse.y"
    {
			Ref( (yyvsp[(3) - (3)].expr) );
			(yyval.param) = new ActualParameter( (yyvsp[(1) - (3)].id), VAL_VAL, (yyvsp[(3) - (3)].expr), 0, (yyvsp[(3) - (3)].expr) );
			}
    break;

  case 120:
#line 719 "../../parse.y"
    {
			Expr* ellipsis =
				current_sequencer->LookupID(
					string_dup( "..." ), LOCAL_SCOPE, 0 );

			if ( ! ellipsis )
				{
				glish_error->Report( "\"...\" not available" );
				IValue *v = error_ivalue();
				(yyval.param) = new ActualParameter( VAL_VAL,
					new ConstExpr( v ) );
				}

			else
				{
				Ref(ellipsis);
				(yyval.param) = new ActualParameter( VAL_VAL, ellipsis, 1 );
				}
			}
    break;

  case 122:
#line 743 "../../parse.y"
    { (yyval.param_list) = new parameter_list; }
    break;

  case 123:
#line 747 "../../parse.y"
    { (yyvsp[(1) - (3)].param_list)->append( (yyvsp[(3) - (3)].param) ); }
    break;

  case 124:
#line 750 "../../parse.y"
    {
			(yyval.param_list) = new parameter_list;
			(yyval.param_list)->append( (yyvsp[(1) - (1)].param) );
			}
    break;

  case 125:
#line 757 "../../parse.y"
    { (yyval.param) = new ActualParameter( VAL_VAL, (yyvsp[(1) - (1)].expr) ); }
    break;

  case 126:
#line 760 "../../parse.y"
    {
			Ref( (yyvsp[(4) - (4)].expr) );
			(yyval.param) = new ActualParameter( (yyvsp[(2) - (4)].id), VAL_CONST, (yyvsp[(4) - (4)].expr), 0, (yyvsp[(4) - (4)].expr) );
			}
    break;

  case 127:
#line 766 "../../parse.y"
    {
			Ref( (yyvsp[(3) - (3)].expr) );
			(yyval.param) = new ActualParameter( (yyvsp[(1) - (3)].id), VAL_VAL, (yyvsp[(3) - (3)].expr), 0, (yyvsp[(3) - (3)].expr) );
			}
    break;

  case 128:
#line 772 "../../parse.y"
    {
			Expr* ellipsis =
				current_sequencer->LookupID(
					string_dup( "..." ), LOCAL_SCOPE, 0 );

			if ( ! ellipsis )
				{
				glish_error->Report( "\"...\" not available" ); 
				IValue *v = error_ivalue();
				(yyval.param) = new ActualParameter( VAL_VAL,
					new ConstExpr( v ) );
				}

			else
				{
				Ref(ellipsis);
				(yyval.param) = new ActualParameter( VAL_VAL, ellipsis, 1 );
				}
			}
    break;

  case 130:
#line 796 "../../parse.y"
    { (yyval.param_list) = new parameter_list; }
    break;

  case 131:
#line 800 "../../parse.y"
    { (yyvsp[(1) - (3)].param_list)->append( (yyvsp[(3) - (3)].param) ); }
    break;

  case 132:
#line 802 "../../parse.y"
    {
			(yyvsp[(1) - (2)].param_list)->append( new ActualParameter() );
			}
    break;

  case 133:
#line 807 "../../parse.y"
    { // Something like "foo(,)" - two missing parameters.
			(yyval.param_list) = new parameter_list;
			(yyval.param_list)->append( new ActualParameter() );
			(yyval.param_list)->append( new ActualParameter() );
       			}
    break;

  case 134:
#line 814 "../../parse.y"
    {
			(yyval.param_list) = new parameter_list;
			(yyval.param_list)->append( new ActualParameter() );
			(yyval.param_list)->append( (yyvsp[(2) - (2)].param) );
       			}
    break;

  case 135:
#line 821 "../../parse.y"
    {
			(yyval.param_list) = new parameter_list;
			(yyval.param_list)->append( (yyvsp[(1) - (1)].param) );
       			}
    break;

  case 136:
#line 828 "../../parse.y"
    { (yyval.param) = new ActualParameter( VAL_VAL, (yyvsp[(1) - (1)].expr) ); }
    break;

  case 137:
#line 831 "../../parse.y"
    {
			Ref( (yyvsp[(3) - (3)].expr) );
			(yyval.param) = new ActualParameter( (yyvsp[(1) - (3)].id), VAL_VAL, (yyvsp[(3) - (3)].expr), 0, (yyvsp[(3) - (3)].expr) );
			}
    break;

  case 138:
#line 837 "../../parse.y"
    {
			Expr* ellipsis =
				current_sequencer->LookupID(
					string_dup( "..." ), LOCAL_SCOPE, 0 );

			if ( ! ellipsis )
				{
				glish_error->Report( "\"...\" not available" ); 
				IValue *v = error_ivalue();
				(yyval.param) = new ActualParameter( VAL_VAL,
					new ConstExpr( v ) );
				}

			else
				{
				Ref(ellipsis);
				(yyval.param) = new ActualParameter( VAL_VAL, ellipsis, 1 );
				}
			}
    break;

  case 139:
#line 860 "../../parse.y"
    { (yyvsp[(1) - (3)].exprlist)->append( (yyvsp[(3) - (3)].expr) ); }
    break;

  case 140:
#line 862 "../../parse.y"
    { (yyvsp[(1) - (2)].exprlist)->append( 0 ); }
    break;

  case 141:
#line 864 "../../parse.y"
    {
			(yyval.exprlist) = new expr_list;
			(yyval.exprlist)->append( 0 );
			(yyval.exprlist)->append( 0 );
			}
    break;

  case 142:
#line 870 "../../parse.y"
    {
			(yyval.exprlist) = new expr_list;
			(yyval.exprlist)->append( 0 );
			(yyval.exprlist)->append( (yyvsp[(2) - (2)].expr) );
			}
    break;

  case 143:
#line 876 "../../parse.y"
    {
			(yyval.exprlist) = new expr_list;
			(yyval.exprlist)->append( (yyvsp[(1) - (1)].expr) );
			}
    break;

  case 145:
#line 888 "../../parse.y"
    { (yyval.stmt) = (yyvsp[(2) - (3)].stmt); }
    break;

  case 146:
#line 891 "../../parse.y"
    { (yyval.stmt) = new ExprStmt( (yyvsp[(1) - (1)].expr)->BuildFrameInfo( SCOPE_UNKNOWN ) ); }
    break;

  case 147:
#line 896 "../../parse.y"
    {
			(yyval.expr) = current_sequencer->LookupID( string_dup((yyvsp[(1) - (1)].id)), LOCAL_SCOPE, 0 );

			if ( ! (yyval.expr) )
				(yyval.expr) = CreateVarExpr( (yyvsp[(1) - (1)].id), current_sequencer );
			else
				free_memory( (yyvsp[(1) - (1)].id) );

			Ref((yyval.expr));
			}
    break;

  case 149:
#line 910 "../../parse.y"
    { (yyval.id) = 0; }
    break;

  case 150:
#line 915 "../../parse.y"
    { (yyval.ev_list)->append( (yyvsp[(3) - (3)].event) ); }
    break;

  case 151:
#line 917 "../../parse.y"
    {
			(yyval.ev_list) = new event_dsg_list;
			(yyval.ev_list)->append( (yyvsp[(1) - (1)].event) );
			}
    break;

  case 152:
#line 924 "../../parse.y"
    {
			(yyvsp[(1) - (5)].expr) = (yyvsp[(1) - (5)].expr)->BuildFrameInfo( SCOPE_UNKNOWN );	/* necessary? */
			(yyvsp[(4) - (5)].expr) = (yyvsp[(4) - (5)].expr)->BuildFrameInfo( SCOPE_UNKNOWN );	/* necessary? */
			(yyval.event) = new EventDesignator( (yyvsp[(1) - (5)].expr), (yyvsp[(4) - (5)].expr) );
			}
    break;

  case 153:
#line 930 "../../parse.y"
    {
			(yyvsp[(1) - (3)].expr) = (yyvsp[(1) - (3)].expr)->BuildFrameInfo( SCOPE_UNKNOWN );	/* necessary? */
			(yyval.event) = new EventDesignator( (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].id) );
			}
    break;

  case 154:
#line 935 "../../parse.y"
    {
			(yyvsp[(1) - (4)].expr) = (yyvsp[(1) - (4)].expr)->BuildFrameInfo( SCOPE_UNKNOWN );	/* necessary? */
			(yyval.event) = new EventDesignator( (yyvsp[(1) - (4)].expr), (Expr*) 0 );
			}
    break;

  case 155:
#line 943 "../../parse.y"
    { (yyval.val_type) = VAL_REF; }
    break;

  case 156:
#line 945 "../../parse.y"
    { (yyval.val_type) = VAL_CONST; }
    break;

  case 157:
#line 947 "../../parse.y"
    { (yyval.val_type) = VAL_VAL; }
    break;

  case 158:
#line 951 "../../parse.y"
    { clear_statement_can_end( ); }
    break;

  case 159:
#line 954 "../../parse.y"
    { set_statement_can_end( ); }
    break;

  case 160:
#line 958 "../../parse.y"
    {
			if ( begin_literal_ids( *lookahead == empty_token ? NULL_TOK : *lookahead ) )
				{
				yyclearin;
				}
			}
    break;

  case 161:
#line 967 "../../parse.y"
    {
			end_literal_ids();
			}
    break;


/* Line 1267 of yacc.c.  */
#line 3051 "y.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", glish_yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = glish_yyr1[yyn];

  yystate = glish_yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = glish_yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = glish_yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


#line 972 "../../parse.y"


void error_reset( )
	{
	extern void scanner_reset( );
	scanner_reset( );

	current_sequencer->ClearWhenevers();

	if ( interactive( ) )
		while ( current_sequencer->ScopeDepth() > scope_depth )
			current_sequencer->PopScope();

	if ( in_evaluation( ) ) scan_strings( 0 );
	}

extern "C"
void yyerror( char msg[] )
	{
	if ( ! status && ! in_evaluation( ) )
		{
		if ( glish_regex_matched )
			{
			parse_error = (IValue*) ValCtor::error( msg, " at or near '", yytext,
					"'; a regular expression\n",
					"was matched in this statement, perhaps it is a mistaken arithmetic expression?" );
			glish_error->Report( msg, " at or near '", yytext, "'; a regular expression\n",
				       "was matched in this statement, perhaps it is a mistaken arithmetic expression?" );
			}
		else
			{
			parse_error = (IValue*) ValCtor::error( msg, " at or near '", yytext, "'" );
			glish_error->Report( msg, " at or near '", yytext, "'" );
			}
		}

	error_reset( );
	}


void clear_error()
	{
	status = 0;
	}

IValue *glish_parser( evalOpt &opt, Stmt *&stmt )
	{
	cur_stmt = stmt = null_stmt;
	eval_options = &opt;

	glish_error->SetCount(0);
	status = 0;

	if ( interactive( ) )
		scope_depth = current_sequencer->ScopeDepth();

	while ( ! yyparse() )
		{
		if ( ! interactive( ) )
			stmt = merge_stmts( stmt, cur_stmt );

		else
			{
			if ( status ) continue;
			Stmt *loc_stmt = cur_stmt;
			cur_stmt = null_stmt;

			IValue *val = 0;
			if ( setjmp(glish_top_jmpbuf) == 0 )
				{
				glish_top_jmpbuf_set = 1;
				opt.set(evalOpt::VALUE_NEEDED);
				val = loc_stmt->Exec( opt );
				if ( ! opt.Next() )
					glish_warn->Report("control flow (loop/break/return) ignored" );
				}

			glish_top_jmpbuf_set = 0;

			if ( current_sequencer->ErrorResult() && current_sequencer->ErrorResult()->Type() == TYPE_FAIL &&
			     ! current_sequencer->ErrorResult()->FailMarked() )
				{
				if ( Sequencer::CurSeq()->System().OLog() )
					Sequencer::CurSeq()->System().DoOLog(current_sequencer->ErrorResult());

				pager->Report( current_sequencer->ErrorResult() );
				current_sequencer->ClearErrorResult();
				}
			else if ( val )
				{
				pager->Report( val );
				Unref( val );
				}

			NodeUnref(loc_stmt);
			first_line = 1;
			scope_depth = current_sequencer->ScopeDepth();
			}
		}

	if ( ! status ) return 0;
	status = 0;
	IValue *tmp = parse_error;
	parse_error = 0;
	return tmp ? tmp : error_ivalue( "parse error" );
	}

Expr* compound_assignment( Expr* lhs, int tok_type, Expr* rhs )
	{
	switch ( tok_type )
		{
		case 0:	// ordinary :=
			return new AssignExpr( lhs, rhs );

#define CMPD(token, expr)						\
	case token:							\
		Ref(lhs);						\
		return new AssignExpr( lhs, new expr( lhs, rhs ) );

		CMPD('+', AddExpr);
		CMPD('-', SubtractExpr);
		CMPD('*', MultiplyExpr);
		CMPD('/', DivideExpr);
		CMPD('%', ModuloExpr);
		CMPD('^', PowerExpr);

		case '~':
			Ref(lhs);
			return new AssignExpr( lhs, new ApplyRegExpr( lhs, rhs, current_sequencer ) );

		CMPD('|', LogOrExpr);
		CMPD('&', LogAndExpr);

		CMPD(TOK_OR_OR, OrExpr);
		CMPD(TOK_AND_AND, AndExpr);

		default:
			glish_fatal->Report( "unknown compound assignment type =",
					     tok_type );
			return 0;
		}
	}


