/* regx/linux_gnu/config.h.  Generated automatically by configure.  */
/* $Id: config.h.in,v 19.0 2003/07/16 05:18:05 aips2adm Exp $
** Copyright (c) 2002 Associated Universities Inc.
*/

/***************************************************************/
/* custom config.h template - do not recreate using autoheader */
/***************************************************************/

/* Define if you have the <malloc.h> header file.  */
#define HAVE_MALLOC_H 1
