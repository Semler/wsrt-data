// ApplicationObject_1010.cc -- Tue Apr  1 12:36:02 BST 2008 -- renting
#include <tasking/Tasking/ApplicationObject.cc>
#include <../misc/aipsrc.h>
#include <../misc/sysinfo.h>
#include <../misc/logtable.h>
#include <../misc/appstate.h>
#include <../misc/os.h>
namespace casa { //# NAMESPACE - BEGIN
template class StandardObjectFactory<aipsrc>;
template class StandardObjectFactory<sysinfo>;
template class StandardObjectFactory<logtable>;
template class StandardObjectFactory<appstate>;
template class StandardObjectFactory<os>;
} //# NAMESPACE - END
