// ApplicationObject_1000.cc -- Tue Apr  1 12:35:49 BST 2008 -- renting
#include <tasking/Tasking/ApplicationObject.cc>
#include <../ionosphere/DOionosphere.h>
#include <../ionosphere/DOrinex.h>
namespace casa { //# NAMESPACE - BEGIN
template class StandardObjectFactory<ionosphere>;
template class StandardObjectFactory<rinex>;
} //# NAMESPACE - END
