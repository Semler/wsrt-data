// FITSFieldFillers_1000.cc -- Tue Apr  1 12:37:06 BST 2008 -- renting
#include <FITSFieldFillers.cc>
template class FITSScalarWriter<casa::Bool, casa::Bool>;
template class FITSScalarWriter<casa::uChar, casa::uChar>;
template class FITSScalarWriter<casa::Short, casa::Short>;
template class FITSScalarWriter<casa::Int, casa::Int>;
template class FITSScalarWriter<casa::Float, casa::Float>;
template class FITSScalarWriter<casa::Double, casa::Double>;
template class FITSScalarWriter<casa::Complex, casa::Complex>;
template class FITSScalarWriter<casa::DComplex, casa::DComplex>;
template class FITSScalarWriter<casa::String, casa::String>;
template class FITSArrayWriter<casa::Bool, casa::Bool>;
template class FITSArrayWriter<casa::uChar, casa::uChar>;
template class FITSArrayWriter<casa::Short, casa::Short>;
template class FITSArrayWriter<casa::Int, casa::Int>;
template class FITSArrayWriter<casa::Float, casa::Float>;
template class FITSArrayWriter<casa::Double, casa::Double>;
template class FITSArrayWriter<casa::Complex, casa::Complex>;
template class FITSArrayWriter<casa::DComplex, casa::DComplex>;
