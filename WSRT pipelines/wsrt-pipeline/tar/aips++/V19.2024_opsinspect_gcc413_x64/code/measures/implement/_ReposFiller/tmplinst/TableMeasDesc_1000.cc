// TableMeasDesc_1000.cc -- Tue Apr  1 12:12:29 BST 2008 -- renting
#include <measures/TableMeasures/TableMeasDesc.cc>
#include <measures/Measures/MBaseline.h>
#include <measures/Measures/MDirection.h>
#include <measures/Measures/MDoppler.h>
#include <measures/Measures/MEarthMagnetic.h>
#include <measures/Measures/MEpoch.h>
#include <measures/Measures/MFrequency.h>
#include <measures/Measures/MPosition.h>
#include <measures/Measures/MRadialVelocity.h>
#include <measures/Measures/Muvw.h>
namespace casa { //# NAMESPACE - BEGIN
template class TableMeasDesc<MBaseline>;
template class TableMeasDesc<MDirection>;
template class TableMeasDesc<MDoppler>;
template class TableMeasDesc<MEarthMagnetic>;
template class TableMeasDesc<MEpoch>;
template class TableMeasDesc<MFrequency>;
template class TableMeasDesc<MPosition>;
template class TableMeasDesc<MRadialVelocity>;
template class TableMeasDesc<Muvw>;
} //# NAMESPACE - END
