// Array_1030.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <measures/Measures/MeasureHolder.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<MeasureHolder>;
#ifdef AIPS_SUN_NATIVE
template class Array<MeasureHolder>::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
