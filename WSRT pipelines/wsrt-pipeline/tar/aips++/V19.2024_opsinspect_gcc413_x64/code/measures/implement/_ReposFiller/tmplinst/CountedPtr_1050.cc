// CountedPtr_1050.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <measures/TableMeasures/TableMeasDescBase.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<TableMeasDescBase>;
template class CountedConstPtr<TableMeasDescBase>;
template class PtrRep<TableMeasDescBase>;
template class SimpleCountedPtr<TableMeasDescBase>;
template class SimpleCountedConstPtr<TableMeasDescBase>;
} //# NAMESPACE - END
