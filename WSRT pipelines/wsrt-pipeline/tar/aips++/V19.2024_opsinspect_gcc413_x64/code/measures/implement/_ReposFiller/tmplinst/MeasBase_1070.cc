// MeasBase_1070.cc -- Tue Apr  1 12:12:29 BST 2008 -- renting
#include <measures/Measures/MeasBase.cc>
#include <casa/Quanta/MVRadialVelocity.h>
#include <measures/Measures/MRadialVelocity.h>
#include <measures/Measures/MeasRef.h>
namespace casa { //# NAMESPACE - BEGIN
template class MeasBase<MVRadialVelocity, MeasRef<MRadialVelocity> >;
} //# NAMESPACE - END
