// ArrayLogical_1000.cc -- Tue Apr  1 12:15:01 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allEQ(Array<Complex> const &, Complex const &);
template Bool allEQ(Array<DComplex> const &, DComplex const &);
template Bool allNear(Array<DComplex> const &, DComplex const &, Double);
template Bool allNear(Array<Complex> const &, Array<Complex> const &, Double);
template Bool allNear(Array<DComplex> const &, Array<DComplex> const &, Double);
} //# NAMESPACE - END
