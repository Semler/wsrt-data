// LatticeExpr_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LatticeExpr.cc>
namespace casa { //# NAMESPACE - BEGIN
template class LatticeExpr<Float>;
template class LatticeExpr<Double>;
template class LatticeExpr<Complex>;
template class LatticeExpr<DComplex>;
template class LatticeExpr<Bool>;
} //# NAMESPACE - END
