// TempLattice_1010.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/TempLattice.cc>
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/Lattice.h>
namespace casa { //# NAMESPACE - BEGIN
template class TempLattice<Bool>;
template class CountedConstPtr<Lattice<Bool> >;
template class CountedPtr<Lattice<Bool> >;
template class PtrRep<Lattice<Bool> >;
template class SimpleCountedConstPtr<Lattice<Bool> >;
template class SimpleCountedPtr<Lattice<Bool> >;
} //# NAMESPACE - END
