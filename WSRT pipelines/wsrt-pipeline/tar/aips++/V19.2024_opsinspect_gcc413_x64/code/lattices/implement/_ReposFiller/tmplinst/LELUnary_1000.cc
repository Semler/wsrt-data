// LELUnary_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LELUnary.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class LELUnaryConst<Float>;
template class LELUnaryConst<Double>;
template class LELUnaryConst<Complex>;
template class LELUnaryConst<DComplex>;
template class LELUnaryConst<Bool>;
template class LELUnary<Float>;
template class LELUnary<Double>;
template class LELUnary<Complex>;
template class LELUnary<DComplex>;
} //# NAMESPACE - END
