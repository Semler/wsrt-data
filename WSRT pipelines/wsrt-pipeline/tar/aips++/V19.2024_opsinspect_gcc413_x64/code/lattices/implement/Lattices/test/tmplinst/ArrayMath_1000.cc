// ArrayMath_1000.cc -- Tue Apr  1 12:15:01 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<Int> operator-(Array<Int> const &, Int const &);
template void indgen(Array<Complex> &);
template void indgen(Array<Complex> &, Complex, Complex);
} //# NAMESPACE - END
