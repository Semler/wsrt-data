// CountedPtr_1120.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/LatticeIterInterface.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<LatticeIterInterface<Int> >;
template class CountedConstPtr<LatticeIterInterface<Int> >;
template class SimpleCountedPtr<LatticeIterInterface<Int> >;
template class SimpleCountedConstPtr<LatticeIterInterface<Int> >;
template class PtrRep<LatticeIterInterface<Int> >;
} //# NAMESPACE - END
