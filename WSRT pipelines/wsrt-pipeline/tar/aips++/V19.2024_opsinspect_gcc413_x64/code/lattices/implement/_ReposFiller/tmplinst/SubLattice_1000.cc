// SubLattice_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/SubLattice.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class SubLattice<Int>;
template class SubLattice<Float>;
template class SubLattice<Double>;
template class SubLattice<Complex>;
template class SubLattice<DComplex>;
template class SubLattice<Bool>;
} //# NAMESPACE - END
