// CountedPtr_1020.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/LELInterface.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<LELInterface<Float> >;
template class CountedPtr<LELInterface<Float> >;
template class PtrRep<LELInterface<Float> >;
template class SimpleCountedConstPtr<LELInterface<Float> >;
template class SimpleCountedPtr<LELInterface<Float> >;
template class CountedConstPtr<LELInterface<Double> >;
template class CountedPtr<LELInterface<Double> >;
template class PtrRep<LELInterface<Double> >;
template class SimpleCountedConstPtr<LELInterface<Double> >;
template class SimpleCountedPtr<LELInterface<Double> >;
template class CountedConstPtr<LELInterface<Complex> >;
template class CountedPtr<LELInterface<Complex> >;
template class PtrRep<LELInterface<Complex> >;
template class SimpleCountedConstPtr<LELInterface<Complex> >;
template class SimpleCountedPtr<LELInterface<Complex> >;
template class CountedConstPtr<LELInterface<DComplex> >;
template class CountedPtr<LELInterface<DComplex> >;
template class PtrRep<LELInterface<DComplex> >;
template class SimpleCountedConstPtr<LELInterface<DComplex> >;
template class SimpleCountedPtr<LELInterface<DComplex> >;
template class CountedConstPtr<LELInterface<Bool> >;
template class CountedPtr<LELInterface<Bool> >;
template class PtrRep<LELInterface<Bool> >;
template class SimpleCountedConstPtr<LELInterface<Bool> >;
template class SimpleCountedPtr<LELInterface<Bool> >;
} //# NAMESPACE - END
