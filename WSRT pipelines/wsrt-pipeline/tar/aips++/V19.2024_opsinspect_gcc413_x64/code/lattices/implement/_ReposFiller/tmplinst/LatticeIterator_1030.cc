// LatticeIterator_1030.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LatticeIterator.cc>
#include <lattices/Lattices/LatticeIterInterface.cc>
namespace casa { //# NAMESPACE - BEGIN
template class LatticeIterator<Float>;
template class RO_LatticeIterator<Float>;
template class LatticeIterInterface<Float>;
} //# NAMESPACE - END
