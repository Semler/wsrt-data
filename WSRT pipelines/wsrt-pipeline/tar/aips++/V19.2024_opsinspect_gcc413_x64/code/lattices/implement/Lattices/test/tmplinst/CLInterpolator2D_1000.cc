// CLInterpolator2D_1000.cc -- Tue Apr  1 12:15:01 BST 2008 -- renting
#include <lattices/Lattices/CLInterpolator2D.cc>
#include <lattices/Lattices/CLIPNearest2D.cc>
#include <lattices/Lattices/CurvedLattice2D.cc>
namespace casa { //# NAMESPACE - BEGIN
template class CLInterpolator2D<Int>;
template class CLIPNearest2D<Int>;
template class CurvedLattice2D<Int>;
} //# NAMESPACE - END
