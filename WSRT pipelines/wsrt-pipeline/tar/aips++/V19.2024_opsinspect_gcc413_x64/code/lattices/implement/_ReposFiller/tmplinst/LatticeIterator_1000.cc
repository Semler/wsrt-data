// LatticeIterator_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LatticeIterator.cc>
#include <lattices/Lattices/LatticeIterInterface.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class LatticeIterator<Complex>;
template class RO_LatticeIterator<Complex>;
template class LatticeIterInterface<Complex>;
template class LatticeIterator<DComplex>;
template class RO_LatticeIterator<DComplex>;
template class LatticeIterInterface<DComplex>;
} //# NAMESPACE - END
