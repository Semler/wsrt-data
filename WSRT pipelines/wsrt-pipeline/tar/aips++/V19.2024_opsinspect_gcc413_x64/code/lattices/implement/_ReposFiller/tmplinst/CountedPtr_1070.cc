// CountedPtr_1070.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/LatticeIterInterface.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<LatticeIterInterface<Complex> >;
template class CountedConstPtr<LatticeIterInterface<Complex> >;
template class SimpleCountedPtr<LatticeIterInterface<Complex> >;
template class SimpleCountedConstPtr<LatticeIterInterface<Complex> >;
template class PtrRep<LatticeIterInterface<Complex> >;
} //# NAMESPACE - END
