// CountedPtr_1140.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/TempLattice.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<TempLattice<Float> >;
template class CountedConstPtr<TempLattice<Float> >;
template class SimpleCountedPtr<TempLattice<Float> >;
template class SimpleCountedConstPtr<TempLattice<Float> >;
template class PtrRep<TempLattice<Float> >;
} //# NAMESPACE - END
