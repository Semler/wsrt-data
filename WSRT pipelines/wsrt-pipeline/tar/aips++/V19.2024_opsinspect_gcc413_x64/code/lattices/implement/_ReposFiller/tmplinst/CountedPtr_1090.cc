// CountedPtr_1090.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/LatticeIterInterface.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<LatticeIterInterface<Bool> >;
template class CountedConstPtr<LatticeIterInterface<Bool> >;
template class SimpleCountedPtr<LatticeIterInterface<Bool> >;
template class SimpleCountedConstPtr<LatticeIterInterface<Bool> >;
template class PtrRep<LatticeIterInterface<Bool> >;
} //# NAMESPACE - END
