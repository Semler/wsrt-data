// TempImage_1000.cc -- Tue Apr  1 12:18:13 BST 2008 -- renting
#include <images/Images/TempImage.cc>
#include <images/Images/ImageInterface.cc>
#include <lattices/Lattices/TempLattice.cc>
#include <casa/Utilities/CountedPtr.cc>
namespace casa { //# NAMESPACE - BEGIN
template class TempImage<Double>;
template class ImageInterface<Double>;
} //# NAMESPACE - END
