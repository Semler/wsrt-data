// AttVal_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <display/Display/AttVal.cc>
#include <casa/Quanta/QLogical.h>
namespace casa { //# NAMESPACE - BEGIN
template class AttributeValue<uInt>;
template class AttributeValue<Int>;
template class AttributeValue<Bool>;
template class AttributeValue<Float>;
template class AttributeValue<Double>;
template class AttributeValue<String>;
template class AttributeValue<Quantity>;
} //# NAMESPACE - END
