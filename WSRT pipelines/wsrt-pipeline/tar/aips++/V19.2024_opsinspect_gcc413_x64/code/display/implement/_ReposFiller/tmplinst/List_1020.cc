// List_1020.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Containers/List.cc>
#include <display/DisplayShapes/DSClosed.h>
namespace casa { //# NAMESPACE - BEGIN
template class List<DSClosed *>;
template class ListNotice<DSClosed *>;
template class ListIter<DSClosed *>;
template class ConstListIter<DSClosed *>;
} //# NAMESPACE - END
