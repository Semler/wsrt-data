//# QtViewerProxy: A proxy object to be used by external bindings
//#                 -- Functional level.
//# Copyright (C) 2005
//# Associated Universities, Inc. Washington DC, USA.
//#
//# This library is free software; you can redistribute it and/or modify it
//# under the terms of the GNU Library General Public License as published by
//# the Free Software Foundation; either version 2 of the License, or (at your
//# option) any later version.
//#
//# This library is distributed in the hope that it will be useful, but WITHOUT
//# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
//# License for more details.
//#
//# You should have received a copy of the GNU Library General Public License
//# along with this library; if not, write to the Free Software Foundation,
//# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//#
//# Correspondence concerning AIPS++ should be addressed as follows:
//#        Internet email: aips2-request@nrao.edu.
//#        Postal address: AIPS++ Project Office
//#                        National Radio Astronomy Observatory
//#                        520 Edgemont Road
//#                        Charlottesville, VA 22903-2475 USA
//#
//# $Id: QtViewerProxy.cc,v 1.1 2007/02/09 00:27:40 mmarquar Exp $

#include <display/QtViewer/QtViewerProxy.h>
#include <display/QtViewer/QtDisplayData.qo.h>
#include <display/Display/StandAloneDisplayApp.h>
        // (Configures pgplot for stand-alone Display Library apps).

//#include 

namespace casa {

QtViewerProxy::QtViewerProxy() : 
  itsDDCounter(0) {
  itsViewer = new QtViewer;
  itsDPG = new QtDisplayPanelGui(itsViewer);
  itsDPG->show();
}

QtViewerProxy::~QtViewerProxy() {
  // crashes the process
  // delete itsDPG;
  delete itsViewer;
}
  
Int QtViewerProxy::addDD(const String& filename, 
			 const String& datatype,
			 const String& displaytype) {
  QtDisplayData* qdd = itsViewer->createDD(filename, datatype, displaytype);
  if ( qdd != 0) {
    itsDDMap[itsDDCounter] = qdd;
    return itsDDCounter++;
  } else {
    throw AipsError(itsViewer->errMsg());
  }
}


void QtViewerProxy::removeDD(Int ddno) {

  if (itsDDMap.find(ddno) == itsDDMap.end()) {
    throw AipsError("Unknown dd number");
  }
  itsViewer->removeDD(itsDDMap[ddno]);
  itsDDMap.erase(ddno);
}


//Vector<Int> QtViewerProxy::getDDNumbers();
  
Record QtViewerProxy::getDDOptions(Int ddno) { 
  if (itsDDMap.find(ddno) == itsDDMap.end()) {
    throw AipsError("Unknown dd number");
  }
  return itsDDMap[ddno]->getOptions();
}

  void QtViewerProxy::setDDOptions(Int ddno, const Record& opts)
{
  if (itsDDMap.find(ddno) == itsDDMap.end()) {
    throw AipsError("Unknown dd number");
  }
  itsDDMap[ddno]->setOptions(opts);
}

Record QtViewerProxy::getPanelOptions() { return Record(); }
void QtViewerProxy::setPanelOptions() {;}

}
