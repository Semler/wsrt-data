// AttBufferTemplates_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <display/Display/AttBufferTemplates.cc>
namespace casa { //# NAMESPACE - BEGIN
template Bool AttributeBuffer::getValue<uInt>(String const &, uInt &) const;
template Bool AttributeBuffer::getValue<Int>(String const &, Int &) const;
template Bool AttributeBuffer::getValue<Bool>(String const &, Bool &) const;
template Bool AttributeBuffer::getValue<Float>(String const &, Float &) const;
template Bool AttributeBuffer::getValue<Double>(String const &, Double &) const;
template Bool AttributeBuffer::getValue<String>(String const &, String &) const;
template Bool AttributeBuffer::getValue<Quantity>(String const &, Quantity &) const;
template Bool AttributeBuffer::getValue<uInt>(String const &, Vector<uInt> &) const;
template Bool AttributeBuffer::getValue<Int>(String const &, Vector<Int> &) const;
template Bool AttributeBuffer::getValue<Bool>(String const &, Vector<Bool> &) const;
template Bool AttributeBuffer::getValue<Float>(String const &, Vector<Float> &) const;
template Bool AttributeBuffer::getValue<Double>(String const &, Vector<Double> &) const;
template Bool AttributeBuffer::getValue<String>(String const &, Vector<String> &) const;
template Bool AttributeBuffer::getValue<Quantity>(String const &, Vector<Quantity> &) const;
} //# NAMESPACE - END
