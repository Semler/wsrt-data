// Link_1020.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Containers/Link.cc>
#include <display/Display/WorldCanvas.h>
#include <display/Display/WorldCanvasHolder.h>
#include <display/Display/MultiWCHolder.h>
#include <display/DisplayDatas/DisplayData.h>
#include <display/DisplayEvents/WCRefreshEH.h>
namespace casa { //# NAMESPACE - BEGIN
template class Link<WorldCanvas *>;
template class Link<WorldCanvasHolder *>;
template class Link<MultiWCHolder *>;
template class Link<DisplayData *>;
template class Link<WCRefreshEH *>;
} //# NAMESPACE - END
