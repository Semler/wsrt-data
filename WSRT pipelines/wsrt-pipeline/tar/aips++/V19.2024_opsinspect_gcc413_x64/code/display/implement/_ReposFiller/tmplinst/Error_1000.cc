// Error_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Exceptions/Error.cc>
#include <display/Display/Colormap.h>
#include <display/Display/PixelCanvasColorTable.h>
namespace casa { //# NAMESPACE - BEGIN
template class indexError<Colormap const *>;
template class indexError<PixelCanvasColorTable *>;
template class indexError<uLong>;
} //# NAMESPACE - END
