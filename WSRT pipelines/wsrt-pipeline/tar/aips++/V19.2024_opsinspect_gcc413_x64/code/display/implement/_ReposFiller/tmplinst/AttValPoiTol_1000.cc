// AttValPoiTol_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <display/Display/AttValPoiTol.cc>
#include <casa/Quanta/QLogical.h>
namespace casa { //# NAMESPACE - BEGIN
template class AttributeValuePoiTol<uInt>;
template class AttributeValuePoiTol<Float>;
template class AttributeValuePoiTol<Double>;
template class AttributeValuePoiTol<Int>;
template class AttributeValuePoiTol<Quantity>;
} //# NAMESPACE - END
