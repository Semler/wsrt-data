// ArrColDesc_1000.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <tables/Tables/ArrColDesc.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template class ArrayColumnDesc<Bool>;
template class ArrayColumnDesc<Char>;
template class ArrayColumnDesc<uChar>;
template class ArrayColumnDesc<Short>;
template class ArrayColumnDesc<uShort>;
template class ArrayColumnDesc<Int>;
template class ArrayColumnDesc<uInt>;
template class ArrayColumnDesc<Float>;
template class ArrayColumnDesc<Double>;
template class ArrayColumnDesc<Complex>;
template class ArrayColumnDesc<DComplex>;
template class ArrayColumnDesc<String>;
} //# NAMESPACE - END
