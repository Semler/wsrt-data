// CountedPtr_1080.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <tables/Tables/TableRecordRep.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<TableRecordRep>;
template class CountedPtr<TableRecordRep>;
template class PtrRep<TableRecordRep>;
template class SimpleCountedConstPtr<TableRecordRep>;
template class SimpleCountedPtr<TableRecordRep>;
} //# NAMESPACE - END
