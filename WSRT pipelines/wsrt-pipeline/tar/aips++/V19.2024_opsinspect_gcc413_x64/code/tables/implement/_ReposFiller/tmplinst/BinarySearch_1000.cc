// BinarySearch_1000.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <casa/Utilities/BinarySearch.cc>
#include <tables/Tables/ScalarColumn.h>
namespace casa { //# NAMESPACE - BEGIN
template Int binarySearch(Bool &, ROScalarColumn<Double> const &, Double const &, uInt, Int);
} //# NAMESPACE - END
