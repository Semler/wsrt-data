// ArrayMath_1000.cc -- Tue Apr  1 12:09:53 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template void indgen(Array<Complex> &);
template void indgen(Array<Complex> &, Complex, Complex);
template void operator+=(Array<uChar> &, uChar const &);
template void operator+=(Array<uShort> &, uShort const &);
template Array<Complex> operator+(Array<Complex> const &, Complex const &);
template Array<String> operator+(Array<String> const &, String const &);
template Double stddev(Array<Double> const &, Double);
template Array<Float> fmod(Array<Float> const &, Float const &);
} //# NAMESPACE - END
