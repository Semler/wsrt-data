// VirtArrCol_1000.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <tables/Tables/VirtArrCol.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class VirtualArrayColumn<Float>;
template class VirtualArrayColumn<Complex>;
} //# NAMESPACE - END
