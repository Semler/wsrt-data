// OrderedPair_1020.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <casa/Containers/OrderedPair.cc>
#include <tables/Tables/BaseColDesc.h>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedPair<String, BaseColumnDesc *(*)(String const &)>;
} //# NAMESPACE - END
