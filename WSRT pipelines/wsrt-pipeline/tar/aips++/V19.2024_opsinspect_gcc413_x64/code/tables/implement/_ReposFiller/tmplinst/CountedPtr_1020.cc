// CountedPtr_1020.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <tables/Tables/ExprNode.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<TableExprNode> >;
template class CountedPtr<Block<TableExprNode> >;
template class PtrRep<Block<TableExprNode> >;
template class SimpleCountedConstPtr<Block<TableExprNode> >;
template class SimpleCountedPtr<Block<TableExprNode> >;
} //# NAMESPACE - END
