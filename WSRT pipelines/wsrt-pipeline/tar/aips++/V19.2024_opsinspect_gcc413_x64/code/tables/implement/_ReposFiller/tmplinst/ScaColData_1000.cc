// ScaColData_1000.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <tables/Tables/ScaColData.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template class ScalarColumnData<Bool>;
template class ScalarColumnData<Char>;
template class ScalarColumnData<uChar>;
template class ScalarColumnData<Short>;
template class ScalarColumnData<uShort>;
template class ScalarColumnData<Int>;
template class ScalarColumnData<uInt>;
template class ScalarColumnData<Float>;
template class ScalarColumnData<Double>;
template class ScalarColumnData<Complex>;
template class ScalarColumnData<DComplex>;
template class ScalarColumnData<String>;
} //# NAMESPACE - END
