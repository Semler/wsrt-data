// CountedPtr_1040.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <tables/Tables/TableIter.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<TableIterator> >;
template class CountedPtr<Block<TableIterator> >;
template class PtrRep<Block<TableIterator> >;
template class SimpleCountedConstPtr<Block<TableIterator> >;
template class SimpleCountedPtr<Block<TableIterator> >;
} //# NAMESPACE - END
