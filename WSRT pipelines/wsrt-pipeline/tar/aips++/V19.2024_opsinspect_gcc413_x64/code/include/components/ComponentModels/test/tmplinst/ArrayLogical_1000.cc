// ArrayLogical_1000.cc -- Tue Apr  1 12:17:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allNear(Array<DComplex> const &, Array<DComplex> const &, Double);
} //# NAMESPACE - END
