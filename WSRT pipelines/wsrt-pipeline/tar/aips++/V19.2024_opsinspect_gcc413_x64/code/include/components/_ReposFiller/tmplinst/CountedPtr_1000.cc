// CountedPtr_1000.cc -- Tue Apr  1 12:17:53 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <components/ComponentModels/SkyComponent.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<SkyComponent> >;
template class CountedPtr<Block<SkyComponent> >;
template class PtrRep<Block<SkyComponent> >;
template class SimpleCountedConstPtr<Block<SkyComponent> >;
template class SimpleCountedPtr<Block<SkyComponent> >;
} //# NAMESPACE - END
