// List_1230.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Containers/List.cc>
#include <display/QtViewer/QtDisplayData.qo.h>
namespace casa { //# NAMESPACE - BEGIN
#ifdef HAVE_QT4
template class List<QtDisplayData *>;
template class ListNotice<QtDisplayData *>;
template class ListIter<QtDisplayData *>;
template class ConstListIter<QtDisplayData *>;
#endif
} //# NAMESPACE - END
