// List_1030.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Containers/List.cc>
#include <display/DisplayShapes/DisplayShape.h>
namespace casa { //# NAMESPACE - BEGIN
template class List<DisplayShape *>;
template class ListNotice<DisplayShape *>;
template class ListIter<DisplayShape *>;
template class ConstListIter<DisplayShape *>;
} //# NAMESPACE - END
