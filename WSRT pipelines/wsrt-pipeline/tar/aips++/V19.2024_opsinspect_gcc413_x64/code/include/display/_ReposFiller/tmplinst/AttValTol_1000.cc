// AttValTol_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <display/Display/AttValTol.cc>
#include <casa/Quanta/QLogical.h>
#include <casa/Quanta/QMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class AttributeValueTol<Int>;
template class AttributeValueTol<Float>;
template class AttributeValueTol<Double>;
template class AttributeValueTol<Quantity>;
} //# NAMESPACE - END
