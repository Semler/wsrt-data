// DisplayOptionsTemplate_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <display/Utilities/DisplayOptionsTemplate.cc>
namespace casa { //# NAMESPACE - BEGIN
template Bool DisplayOptions::readOptionRecord<String>(String &, Bool &, Record const &, String const &) const;
template Bool DisplayOptions::readOptionRecord<Bool>(Bool &, Bool &, Record const &, String const &) const;
template Bool DisplayOptions::readOptionRecord<Float>(Float &, Bool &, Record const &, String const &) const;
template Bool DisplayOptions::readOptionRecord<Double>(Double &, Bool &, Record const &, String const &) const;
template Bool DisplayOptions::readOptionRecord<Int>(Int &, Bool &, Record const &, String const &) const;
template Bool DisplayOptions::readOptionRecord<Float>(Vector<Float> &, Bool &, Record const &, String const &) const;
template Bool DisplayOptions::readOptionRecord<String>(Vector<String> &, Bool &, Record const &, String const &) const;
template Bool DisplayOptions::readOptionRecord<int>(Vector<int> &, bool &, Record const &, String const &) const;
} //# NAMESPACE - END
