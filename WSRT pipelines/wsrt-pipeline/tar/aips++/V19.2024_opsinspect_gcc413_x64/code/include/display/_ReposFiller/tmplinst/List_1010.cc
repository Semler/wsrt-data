// List_1010.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Containers/List.cc>
#include <display/DisplayEvents/DisplayEH.h>
#include <display/DisplayEvents/WCMotionEH.h>
#include <display/DisplayEvents/WCPositionEH.h>
#include <display/DisplayEvents/WCRefreshEH.h>
#include <display/DisplayDatas/DisplayData.h>
#include <display/DisplayDatas/PrincipalAxesDD.h>
namespace casa { //# NAMESPACE - BEGIN
template class List<DisplayEH *>;
template class ListNotice<DisplayEH *>;
template class ListIter<DisplayEH *>;
template class ConstListIter<DisplayEH *>;
template class List<WCMotionEH *>;
template class ListNotice<WCMotionEH *>;
template class ListIter<WCMotionEH *>;
template class ConstListIter<WCMotionEH *>;
template class List<WCPositionEH *>;
template class ListNotice<WCPositionEH *>;
template class ListIter<WCPositionEH *>;
template class ConstListIter<WCPositionEH *>;
template class List<WCRefreshEH *>;
template class ListNotice<WCRefreshEH *>;
template class ListIter<WCRefreshEH *>;
template class ConstListIter<WCRefreshEH *>;
} //# NAMESPACE - END
