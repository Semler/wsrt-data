// ArrayIO_1070.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayIO.cc>
namespace casa { //# NAMESPACE - BEGIN
template istream & operator>>(istream &, Array<String> &);
template Bool read(istream &, Array<String> &, IPosition const *, Bool);
template Bool readArrayBlock(istream &, Bool &, IPosition &, Block<String> &, IPosition const *, Bool);
} //# NAMESPACE - END
