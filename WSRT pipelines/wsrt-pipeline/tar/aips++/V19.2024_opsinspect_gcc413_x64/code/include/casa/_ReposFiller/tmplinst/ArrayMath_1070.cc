// ArrayMath_1070.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<String> operator+(String const &, Array<String> const &);
template void operator+=(Array<String> &, Array<String> const &);
template void operator+=(Array<String> &, String const &);
} //# NAMESPACE - END
