// ArrayLogical_1000.cc -- Tue Apr  1 11:49:18 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allGE(Array<Int> const &, Array<Int> const &);
template Bool allGT(Array<Int> const &, Array<Int> const &);
template Bool allLE(Array<Int> const &, Array<Int> const &);
template Bool allLE(Array<Int> const &, Int const &);
template Bool allLT(Array<Int> const &, Array<Int> const &);
template Bool allNE(Array<Int> const &, Array<Int> const &);
template Bool allNE(Array<Int> const &, Int const &);
template Bool allNear(Float const &, Array<Float> const &, Double);
template Bool allNearAbs(Array<Complex> const &, Array<Complex> const &, Double);
template Bool allNearAbs(Array<DComplex> const &, Array<DComplex> const &, Double);
template Bool allNearAbs(Float const &, Array<Float> const &, Double);
template Bool anyEQ(Array<Int> const &, Array<Int> const &);
template Bool anyEQ(Int const &, Array<Int> const &);
template Bool anyGE(Array<Int> const &, Array<Int> const &);
template Bool anyGE(Array<Int> const &, Int const &);
template Bool anyGE(Int const &, Array<Int> const &);
template Bool anyGT(Array<Int> const &, Array<Int> const &);
template Bool anyGT(Int const &, Array<Int> const &);
template Bool anyLE(Array<Int> const &, Array<Int> const &);
template Bool anyLE(Int const &, Array<Int> const &);
template Bool anyLT(Array<Int> const &, Array<Int> const &);
template Bool anyLT(Int const &, Array<Int> const &);
template Bool anyNE(Int const &, Array<Int> const &);
template Bool anyNear(Array<Float> const &, Array<Float> const &, Double);
template Bool anyNear(Array<Float> const &, Float const &, Double);
template Bool anyNear(Float const &, Array<Float> const &, Double);
template Bool anyNearAbs(Array<Float> const &, Array<Float> const &, Double);
template Bool anyNearAbs(Array<Float> const &, Float const &, Double);
template Bool anyNearAbs(Float const &, Array<Float> const &, Double);
template LogicalArray operator!=(Array<Int> const &, Array<Int> const &);
template LogicalArray operator!=(Int const &, Array<Int> const &);
template LogicalArray operator<(Array<Double> const &, Double const &);
template LogicalArray operator<(Array<Int> const &, Array<Int> const &);
template LogicalArray operator<(Int const &, Array<Int> const &);
template LogicalArray operator<=(Array<Int> const &, Array<Int> const &);
template LogicalArray operator<=(Int const &, Array<Int> const &);
template LogicalArray operator==(Array<Int> const &, Array<Int> const &);
template LogicalArray operator==(Int const &, Array<Int> const &);
template LogicalArray operator>(Array<Int> const &, Array<Int> const &);
template LogicalArray operator>(Int const &, Array<Int> const &);
template LogicalArray operator>=(Array<Int> const &, Array<Int> const &);
template LogicalArray operator>=(Int const &, Array<Int> const &);
} //# NAMESPACE - END
