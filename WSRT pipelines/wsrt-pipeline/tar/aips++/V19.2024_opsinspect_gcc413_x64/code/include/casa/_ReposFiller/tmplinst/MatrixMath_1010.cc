// MatrixMath_1010.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/MatrixMath.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Matrix<Complex> transpose(Matrix<Complex> const &);
} //# NAMESPACE - END
