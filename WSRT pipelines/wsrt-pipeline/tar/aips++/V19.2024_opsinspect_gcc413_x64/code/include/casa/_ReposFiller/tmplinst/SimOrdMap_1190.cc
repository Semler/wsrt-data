// SimOrdMap_1190.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/SimOrdMap.cc>
namespace casa { //# NAMESPACE - BEGIN
template class SimpleOrderedMap<Int, uInt>;
template class SimpleOrderedMap<uInt, Int>;
template class SimpleOrderedMap<uInt, uInt>;
template class SimpleOrderedMap<Double, uInt>;
template class SimpleOrderedMap<Double, Int>;
} //# NAMESPACE - END
