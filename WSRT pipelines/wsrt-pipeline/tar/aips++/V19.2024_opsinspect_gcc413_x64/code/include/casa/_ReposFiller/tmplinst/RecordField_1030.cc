// RecordField_1030.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/RecordField.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class RecordFieldPtr<Bool>;
template class RecordFieldPtr<Complex>;
template class RecordFieldPtr<DComplex>;
template class RecordFieldPtr<Double>;
template class RecordFieldPtr<Float>;
template class RecordFieldPtr<Int>;
template class RecordFieldPtr<Record>;
template class RecordFieldPtr<Short>;
template class RecordFieldPtr<String>;
template class RecordFieldPtr<uChar>;
template class RecordFieldPtr<uInt>;
} //# NAMESPACE - END
