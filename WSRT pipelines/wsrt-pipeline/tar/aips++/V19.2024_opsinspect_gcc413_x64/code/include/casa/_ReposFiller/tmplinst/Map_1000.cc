// Map_1000.cc -- Tue Apr  1 11:50:44 BST 2008 -- renting
#include <casa/Containers/Map.cc>
#include <casa/BasicSL/String.h>
#include <casa/Containers/Block.h>
#include <casa/Arrays/IPosition.h>
namespace casa { //# NAMESPACE - BEGIN
template class ConstMapIter<String, Block<IPosition> >;
template class MapIter<String, Block<IPosition> >;
template class MapIterRep<String, Block<IPosition> >;
template class MapRep<String, Block<IPosition> >;
template class Map<String, Block<IPosition> >;
} //# NAMESPACE - END
