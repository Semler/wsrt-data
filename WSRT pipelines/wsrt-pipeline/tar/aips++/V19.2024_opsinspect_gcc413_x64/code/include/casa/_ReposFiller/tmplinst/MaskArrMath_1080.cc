// MaskArrMath_1080.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/MaskArrMath.cc>
namespace casa { //# NAMESPACE - BEGIN
template Float variance(MaskedArray<Float> const &);
template Float variance(MaskedArray<Float> const &, Float);
template MaskedArray<Float> operator-(MaskedArray<Float> const &, Float const &);
template MaskedArray<Float> operator-(Float const &, MaskedArray<Float> const &);
template const MaskedArray<Float> & operator*=(MaskedArray<Float> const &, MaskedArray<Float> const &);
template const MaskedArray<Float> & operator-=(MaskedArray<Float> const &, Float const &);
template const MaskedArray<Float> & operator-=(MaskedArray<Float> const &, MaskedArray<Float> const &);
} //# NAMESPACE - END
