// DefaultValue_1000.cc -- Tue Apr  1 11:50:47 BST 2008 -- renting
#include <casa/Utilities/DefaultValue.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template void defaultValue(Complex &);
template void defaultValue(DComplex &);
} //# NAMESPACE - END
