// ArrayMath_1160.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template uLong max(Array<uLong> const &);
template uLong min(Array<uLong> const &);
template void indgen(Array<uLong> &);
template void indgen(Array<uLong> &, uLong, uLong);
template void minMax(uLong &, uLong &, Array<uLong> const &);
} //# NAMESPACE - END
