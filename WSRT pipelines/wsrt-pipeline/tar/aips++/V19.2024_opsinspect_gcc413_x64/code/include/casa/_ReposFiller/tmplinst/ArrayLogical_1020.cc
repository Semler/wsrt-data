// ArrayLogical_1020.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allEQ(Array<String> const &, Array<String> const &);
template Bool allEQ(Array<String> const &, String const &);
template Bool anyEQ(Array<String> const &, String const &);
template Bool anyEQ(String const &, Array<String> const &);
template Bool anyNE(Array<String> const &, Array<String> const &);
template LogicalArray operator!=(Array<String> const &, Array<String> const &);
template LogicalArray operator!=(Array<String> const &, String const &);
template LogicalArray operator!=(String const &, Array<String> const &);
template LogicalArray operator==(Array<String> const &, Array<String> const &);
template LogicalArray operator==(Array<String> const &, String const &);
template LogicalArray operator==(String const &, Array<String> const &);
template LogicalArray operator>(Array<String> const &, Array<String> const &);
template LogicalArray operator>(Array<String> const &, String const &);
template LogicalArray operator>(String const &, Array<String> const &);
template LogicalArray operator>=(Array<String> const &, Array<String> const &);
template LogicalArray operator>=(Array<String> const &, String const &);
template LogicalArray operator>=(String const &, Array<String> const &);
} //# NAMESPACE - END
