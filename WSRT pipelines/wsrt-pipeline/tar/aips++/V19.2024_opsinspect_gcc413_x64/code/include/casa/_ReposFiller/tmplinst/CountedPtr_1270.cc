// CountedPtr_1270.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Quanta/MVRadialVelocity.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<MVRadialVelocity> >;
template class CountedPtr<Block<MVRadialVelocity> >;
template class PtrRep<Block<MVRadialVelocity> >;
template class SimpleCountedConstPtr<Block<MVRadialVelocity> >;
template class SimpleCountedPtr<Block<MVRadialVelocity> >;
} //# NAMESPACE - END
