// CountedPtr_1350.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Int> >;
template class CountedPtr<Block<Int> >;
template class PtrRep<Block<Int> >;
template class SimpleCountedConstPtr<Block<Int> >;
template class SimpleCountedPtr<Block<Int> >;
} //# NAMESPACE - END
