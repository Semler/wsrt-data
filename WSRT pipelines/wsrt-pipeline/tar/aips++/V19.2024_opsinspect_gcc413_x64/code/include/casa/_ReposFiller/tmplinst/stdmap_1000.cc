// stdmap_1000.cc -- Tue Apr  1 11:50:48 BST 2008 -- renting
#include <casa/stdmap.h>
#include <casa/BasicSL/String.h>
#include <casa/Quanta/UnitName.h>
#include <casa/Quanta/UnitVal.h>
template class std::map<casa::String, casa::UnitName>;
AIPS_MAP_AUX_TEMPLATES(casa::String, casa::UnitName)
template class std::map<casa::String, casa::UnitVal>;
AIPS_MAP_AUX_TEMPLATES(casa::String, casa::UnitVal)
