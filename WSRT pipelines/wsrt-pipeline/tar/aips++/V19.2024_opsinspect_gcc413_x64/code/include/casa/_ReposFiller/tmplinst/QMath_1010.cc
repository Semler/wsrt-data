// QMath_1010.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Quanta/QMath.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Quantum<Array<Double> > abs(Quantum<Array<Double> > const &);
template Quantum<Array<Double> > acos(Quantum<Array<Double> > const &);
template Quantum<Array<Double> > asin(Quantum<Array<Double> > const &);
template Quantum<Array<Double> > atan(Quantum<Array<Double> > const &);
template Quantum<Array<Double> > atan2(Quantum<Array<Double> > const &, Quantum<Array<Double> > const &);
template Quantum<Array<Double> > ceil(Quantum<Array<Double> > const &);
template Quantum<Array<Double> > cos(Quantum<Array<Double> > const &);
template Quantum<Array<Double> > floor(Quantum<Array<Double> > const &);
template Quantum<Array<Double> > sin(Quantum<Array<Double> > const &);
template Quantum<Array<Double> > tan(Quantum<Array<Double> > const &);
template Quantum<Array<Double> > log(Quantum<Array<Double> > const &);
template Quantum<Array<Double> > log10(Quantum<Array<Double> > const &);
template Quantum<Array<Double> > exp(Quantum<Array<Double> > const &);
template Quantum<Array<Double> > root(Quantum<Array<Double> > const &, Int);
template Quantum<Array<Double> > sqrt(Quantum<Array<Double> > const &);
} //# NAMESPACE - END
