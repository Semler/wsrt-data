// MaskArrMath_1010.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/MaskArrMath.cc>
#include <casa/Arrays/MaskedArray.h>
namespace casa { //# NAMESPACE - BEGIN
template Float min(MaskedArray<Float> const &);
template Float max(MaskedArray<Float> const &);
template Double min(MaskedArray<Double> const &);
template Double max(MaskedArray<Double> const &);
template MaskedArray<Float> abs(MaskedArray<Float> const &);
} //# NAMESPACE - END
