// Register_1040.cc -- Tue Apr  1 11:49:34 BST 2008 -- renting
#include <casa/Utilities/Register.cc>
#include <casa/Containers/OrderedMap.h>
#include <casa/Containers/OrderedPair.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template uInt Register(OrderedMapNotice<String, OrderedPair<String, uInt> > const *);
} //# NAMESPACE - END
