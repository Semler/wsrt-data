// COWPtr_1080.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/COWPtr.cc>
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class COWPtr<Array<DComplex> >;
template class CountedPtr<Array<DComplex> >;
template class PtrRep<Array<DComplex> >;
template class SimpleCountedConstPtr<Array<DComplex> >;
template class CountedConstPtr<Array<DComplex> >;
template class SimpleCountedPtr<Array<DComplex> >;
} //# NAMESPACE - END
