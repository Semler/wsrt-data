// CountedPtr_1450.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Logging/LogSinkInterface.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<LogSinkInterface>;
template class CountedPtr<LogSinkInterface>;
template class PtrRep<LogSinkInterface>;
template class SimpleCountedConstPtr<LogSinkInterface>;
template class SimpleCountedPtr<LogSinkInterface>;
} //# NAMESPACE - END
