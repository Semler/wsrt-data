// ArrayMath_1060.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template void convertArray(Array<Complex> &, Array<DComplex> const &);
template void convertArray(Array<Complex> &, Array<Double> const &);
template void convertArray(Array<Complex> &, Array<Float> const &);
template void convertArray(Array<Complex> &, Array<Int> const &);
template void convertArray(Array<Complex> &, Array<Short> const &);
template void convertArray(Array<Complex> &, Array<uChar> const &);
template void convertArray(Array<Complex> &, Array<uInt> const &);
template void convertArray(Array<DComplex> &, Array<Complex> const &);
template void convertArray(Array<DComplex> &, Array<DComplex> const &);
template void convertArray(Array<DComplex> &, Array<Double> const &);
template void convertArray(Array<DComplex> &, Array<Float> const &);
template void convertArray(Array<DComplex> &, Array<Int> const &);
template void convertArray(Array<DComplex> &, Array<Short> const &);
template void convertArray(Array<DComplex> &, Array<uChar> const &);
template void convertArray(Array<DComplex> &, Array<uInt> const &);
template void convertArray(Array<uInt> &, Array<Bool> const &);
} //# NAMESPACE - END
