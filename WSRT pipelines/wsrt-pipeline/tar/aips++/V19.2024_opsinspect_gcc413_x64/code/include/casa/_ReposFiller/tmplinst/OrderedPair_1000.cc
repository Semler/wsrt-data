// OrderedPair_1000.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/OrderedPair.cc>
#include <casa/Arrays/IPosition.h>
#include <casa/BasicSL/String.h>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedPair<String, Block<IPosition> >;
} //# NAMESPACE - END
