// ArrayMath_1010.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<Complex> abs(Array<Complex> const &);
template Array<Complex> cos(Array<Complex> const &);
template Array<Complex> cosh(Array<Complex> const &);
template Array<Complex> exp(Array<Complex> const &);
template Array<Complex> log(Array<Complex> const &);
template Array<Complex> log10(Array<Complex> const &);
template Array<Complex> operator*(Array<Complex> const &, Complex const &);
template Array<Complex> operator+(Array<Complex> const &, Array<Complex> const &);
template Array<Complex> operator-(Array<Complex> const &);
template Array<Complex> operator-(Complex const &, Array<Complex> const &);
template Array<Complex> operator-(Array<Complex> const &, Array<Complex> const &);
template Array<Complex> operator/(Array<Complex> const &, Complex const &);
template Array<Complex> operator/(Complex const &, Array<Complex> const &);
template Array<Complex> pow(Array<Complex> const &, Array<Complex> const &);
template Array<Complex> pow(Array<Complex> const &, Double const &);
template Array<Complex> sin(Array<Complex> const &);
template Array<Complex> sinh(Array<Complex> const &);
template Array<Complex> sqrt(Array<Complex> const &);
template Complex max(Array<Complex> const &);
template Complex min(Array<Complex> const &);
template void minMax(Complex &, Complex &, Array<Complex> const &);
template void operator*=(Array<Complex> &, Array<Complex> const &);
template void operator*=(Array<Complex> &, Complex const &);
template void operator+=(Array<Complex> &, Array<Complex> const &);
template void operator+=(Array<Complex> &, Complex const &);
template void operator-=(Array<Complex> &, Array<Complex> const &);
template void operator-=(Array<Complex> &, Complex const &);
template void operator/=(Array<Complex> &, Array<Complex> const &);
template void operator/=(Array<Complex> &, Complex const &);
} //# NAMESPACE - END
