// ArrayLogical_1000.cc -- Tue Apr  1 11:49:34 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allEQ(Array<Complex> const &, Complex const &);
template Bool allEQ(Array<DComplex> const &, DComplex const &);
template Bool allEQ(Array<Short> const &, Short const &);
} //# NAMESPACE - END
