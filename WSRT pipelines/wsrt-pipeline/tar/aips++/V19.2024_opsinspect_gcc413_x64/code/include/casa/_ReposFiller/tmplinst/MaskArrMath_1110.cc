// MaskArrMath_1110.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/MaskArrMath.cc>
namespace casa { //# NAMESPACE - BEGIN
template const MaskedArray<Int> & operator+=(MaskedArray<Int> const &, Int const &);
template MaskedArray<Int> operator-(MaskedArray<Int> const &);
template MaskedArray<uInt> operator-(MaskedArray<uInt> const &);
} //# NAMESPACE - END
