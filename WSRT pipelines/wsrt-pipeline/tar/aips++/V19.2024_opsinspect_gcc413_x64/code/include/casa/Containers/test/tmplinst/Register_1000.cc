// Register_1000.cc -- Tue Apr  1 11:49:34 BST 2008 -- renting
#include <casa/Utilities/Register.cc>
#include <casa/Containers/List.h>
#include <casa/Containers/OrderedPair.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template uInt Register(ListNotice<OrderedPair<String, Int> > const *);
} //# NAMESPACE - END
