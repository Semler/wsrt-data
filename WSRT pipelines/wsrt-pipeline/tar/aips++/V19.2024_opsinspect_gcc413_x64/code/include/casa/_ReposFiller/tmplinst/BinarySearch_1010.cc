// BinarySearch_1010.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/BinarySearch.cc>
#include <casa/Containers/Block.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Int binarySearchBrackets(Bool &, Block<Complex> const &, Complex const &, uInt, Int);
} //# NAMESPACE - END
