// QMath_1000.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Quanta/QMath.cc>
#include <casa/Quanta/Quantum.h>
namespace casa { //# NAMESPACE - BEGIN
template Quantum<Double> operator*(Double const &, Quantum<Double> const &);
template Quantum<Double> operator*(Quantum<Double> const &, Double const &);
template Quantum<Double> abs(Quantum<Double> const &);
template Quantum<Double> acos(Quantum<Double> const &);
template Quantum<Double> asin(Quantum<Double> const &);
template Quantum<Double> atan(Quantum<Double> const &);
template Quantum<Double> atan2(Quantum<Double> const &, Quantum<Double> const &);
template Quantum<Double> ceil(Quantum<Double> const &);
template Quantum<Double> cos(Quantum<Double> const &);
template Quantum<Double> floor(Quantum<Double> const &);
template Quantum<Double> operator/(Quantum<Double> const &, Double const &);
template Quantum<Double> pow(Quantum<Double> const &, Int);
template Quantum<Double> sin(Quantum<Double> const &);
template Quantum<Double> tan(Quantum<Double> const &);
template Quantum<Double> log(Quantum<Double> const &);
template Quantum<Double> log10(Quantum<Double> const &);
template Quantum<Double> exp(Quantum<Double> const &);
template Quantum<Double> root(Quantum<Double> const &, Int);
template Quantum<Double> sqrt(Quantum<Double> const &);
} //# NAMESPACE - END
