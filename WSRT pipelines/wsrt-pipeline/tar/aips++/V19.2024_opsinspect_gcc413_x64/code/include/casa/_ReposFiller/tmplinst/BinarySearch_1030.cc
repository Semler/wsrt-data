// BinarySearch_1030.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/BinarySearch.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template Int binarySearchBrackets(Bool &, Block<Float> const &, Float const &, uInt, Int);
} //# NAMESPACE - END
