// CountedPtr_1000.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Array<uInt> >;
template class CountedPtr<Array<uInt> >;
template class PtrRep<Array<uInt> >;
template class SimpleCountedConstPtr<Array<uInt> >;
template class SimpleCountedPtr<Array<uInt> >;
} //# NAMESPACE - END
