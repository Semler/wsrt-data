// CountedPtr_1100.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Array<Double> >;
template class CountedPtr<Array<Double> >;
template class PtrRep<Array<Double> >;
template class SimpleCountedConstPtr<Array<Double> >;
template class SimpleCountedPtr<Array<Double> >;
} //# NAMESPACE - END
