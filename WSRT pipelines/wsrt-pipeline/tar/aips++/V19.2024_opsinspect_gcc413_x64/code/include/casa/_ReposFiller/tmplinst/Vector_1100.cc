// Vector_1100.cc -- Tue Apr  1 11:50:44 BST 2008 -- renting
#include <casa/Arrays/Vector.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template class Vector<Bool>;
template class Vector<uChar>;
template class Vector<Short>;
template class Vector<uShort>;
template class Vector<Int>;
template class Vector<uInt>;
template class Vector<Float>;
template class Vector<Double>;
template class Vector<Complex>;
template class Vector<DComplex>;
template class Vector<String>;
} //# NAMESPACE - END
