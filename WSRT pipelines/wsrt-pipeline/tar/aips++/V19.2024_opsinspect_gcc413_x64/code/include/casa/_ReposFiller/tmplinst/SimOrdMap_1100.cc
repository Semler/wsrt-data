// SimOrdMap_1100.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/SimOrdMap.cc>
#include <casa/Containers/PoolStack.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template <class T> class AutoDiffRep;
template class SimpleOrderedMap<uInt, PoolStack<AutoDiffRep<DComplex>, uInt>*>;
} //# NAMESPACE - END
