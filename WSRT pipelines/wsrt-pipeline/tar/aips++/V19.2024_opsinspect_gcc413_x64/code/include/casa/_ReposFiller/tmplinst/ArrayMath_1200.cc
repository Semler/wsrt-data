// ArrayMath_1200.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template void operator*=(Array<uInt> &, Array<uInt> const &);
template void operator+=(Array<uInt> &, Array<uInt> const &);
template void operator-=(Array<uInt> &, Array<uInt> const &);
template void operator/=(Array<uInt> &, Array<uInt> const &);
} //# NAMESPACE - END
