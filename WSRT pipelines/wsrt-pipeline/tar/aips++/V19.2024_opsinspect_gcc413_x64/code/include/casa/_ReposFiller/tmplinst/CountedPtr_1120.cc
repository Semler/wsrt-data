// CountedPtr_1120.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Array<Int> >;
template class CountedPtr<Array<Int> >;
template class PtrRep<Array<Int> >;
template class SimpleCountedConstPtr<Array<Int> >;
template class SimpleCountedPtr<Array<Int> >;
} //# NAMESPACE - END
