// CountedPtr_1010.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Arrays/Slicer.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Slicer> >;
template class CountedPtr<Block<Slicer> >;
template class PtrRep<Block<Slicer> >;
template class SimpleCountedConstPtr<Block<Slicer> >;
template class SimpleCountedPtr<Block<Slicer> >;
} //# NAMESPACE - END
