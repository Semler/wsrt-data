// Array_1010.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <casa/Arrays/Vector.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<Vector<Complex> >;
#ifdef AIPS_SUN_NATIVE
template class Array<Vector<Complex> >::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
