// Array_1090.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <casa/Quanta/MVPosition.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<MVPosition>;
#ifdef AIPS_SUN_NATIVE
template class Array<MVPosition>::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
