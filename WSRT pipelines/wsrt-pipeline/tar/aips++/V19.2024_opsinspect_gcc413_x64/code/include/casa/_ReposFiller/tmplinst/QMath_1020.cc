// QMath_1020.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Quanta/QMath.cc>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template Quantum<Vector<Double> > abs(Quantum<Vector<Double> > const &);
template Quantum<Vector<Double> > acos(Quantum<Vector<Double> > const &);
template Quantum<Vector<Double> > asin(Quantum<Vector<Double> > const &);
template Quantum<Vector<Double> > atan(Quantum<Vector<Double> > const &);
template Quantum<Vector<Double> > atan2(Quantum<Vector<Double> > const &, Quantum<Vector<Double> > const &);
template Quantum<Vector<Double> > ceil(Quantum<Vector<Double> > const &);
template Quantum<Vector<Double> > cos(Quantum<Vector<Double> > const &);
template Quantum<Vector<Double> > floor(Quantum<Vector<Double> > const &);
template Quantum<Vector<Double> > sin(Quantum<Vector<Double> > const &);
template Quantum<Vector<Double> > tan(Quantum<Vector<Double> > const &);
template Quantum<Vector<Double> > log(Quantum<Vector<Double> > const &);
template Quantum<Vector<Double> > log10(Quantum<Vector<Double> > const &);
template Quantum<Vector<Double> > exp(Quantum<Vector<Double> > const &);
template Quantum<Vector<Double> > root(Quantum<Vector<Double> > const &, Int);
template Quantum<Vector<Double> > sqrt(Quantum<Vector<Double> > const &);
} //# NAMESPACE - END
