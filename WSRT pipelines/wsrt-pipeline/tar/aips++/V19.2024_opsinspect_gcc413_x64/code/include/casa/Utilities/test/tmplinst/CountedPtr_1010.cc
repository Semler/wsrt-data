// CountedPtr_1010.cc -- Tue Apr  1 11:57:26 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Utilities/test/tCountedPtr.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<myobj>;
template class CountedPtr<myobj>;
template class PtrRep<myobj>;
template class SimpleCountedConstPtr<myobj>;
template class SimpleCountedPtr<myobj>;
} //# NAMESPACE - END
