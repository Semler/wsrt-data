// Register_1000.cc -- Tue Apr  1 11:57:26 BST 2008 -- renting
#include <casa/Utilities/Register.cc>
#include <tRegister.h>
namespace casa { //# NAMESPACE - BEGIN
template uInt Register(bar2foo const *);
template uInt Register(foo2bar2 const *);
template uInt Register(bar const *);
template uInt Register(foobar const *);
template uInt Register(mytmp<Float> const *);
template uInt Register(mytmp<Int> const *);
template uInt Register(mytmp<Long> const *);
template uInt Register(mytmp<Short> const *);
template uInt Register(barfoo2 const *);
template uInt Register(foo const *);
template uInt Register(bar2 const *);
template uInt Register(mytmp2<Short> const *);
template uInt Register(mytmp2<Float> const *);
template uInt Register(mytmp2<Int> const *);
template uInt Register(mytmp2<Long> const *);
template uInt Register(foo2 const *);
template uInt Register(bar2foo2 const *);
template uInt Register(foobar2 const *);
template uInt Register(foo2bar const *);
} //# NAMESPACE - END
