// CountedPtr_1040.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Long> >;
template class CountedPtr<Block<Long> >;
template class PtrRep<Block<Long> >;
template class SimpleCountedConstPtr<Block<Long> >;
template class SimpleCountedPtr<Block<Long> >;
} //# NAMESPACE - END
