// QLogical_1000.cc -- Tue Apr  1 11:50:19 BST 2008 -- renting
#include <casa/Quanta/QLogical.cc>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool operator==(Quantum<Vector<Double> > const &, Quantum<Vector<Double> > const &);
template Bool operator==(Quantum<Vector<Double> > const &, Vector<Double> const &);
} //# NAMESPACE - END
