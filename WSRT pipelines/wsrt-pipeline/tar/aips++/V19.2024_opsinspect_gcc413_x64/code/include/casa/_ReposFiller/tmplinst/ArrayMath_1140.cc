// ArrayMath_1140.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Long max(Array<Long> const &);
template Long min(Array<Long> const &);
template void indgen(Array<Long> &);
template void indgen(Array<Long> &, Long, Long);
template void minMax(Long &, Long &, Array<Long> const &);
} //# NAMESPACE - END
