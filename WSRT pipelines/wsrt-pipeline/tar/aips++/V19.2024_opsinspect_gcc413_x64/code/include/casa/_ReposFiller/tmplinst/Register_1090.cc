// Register_1090.cc -- Tue Apr  1 11:50:47 BST 2008 -- renting
#include <casa/Utilities/Register.cc>
#include <casa/Containers/OrderedMap.h>
#include <casa/BasicSL/String.h>
#include <casa/Arrays/IPosition.h>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template uInt Register(OrderedMapNotice<String, Block<IPosition> > const *);
} //# NAMESPACE - END
