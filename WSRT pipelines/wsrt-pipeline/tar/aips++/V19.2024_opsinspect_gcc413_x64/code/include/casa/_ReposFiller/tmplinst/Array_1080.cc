// Array_1080.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <casa/Quanta/MVFrequency.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<MVFrequency>;
#ifdef AIPS_SUN_NATIVE
template class Array<MVFrequency>::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
