// ArrayLogical_1010.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allEQ(Array<DComplex> const &, Array<DComplex> const &);
template Bool allNearAbs(Array<DComplex> const &, DComplex const &, Double);
template Bool anyEQ(DComplex const &, Array<DComplex> const &);
template LogicalArray operator!=(Array<DComplex> const &, Array<DComplex> const &);
template LogicalArray operator!=(Array<DComplex> const &, DComplex const &);
template LogicalArray operator!=(DComplex const &, Array<DComplex> const &);
template LogicalArray operator==(Array<DComplex> const &, Array<DComplex> const &);
template LogicalArray operator==(Array<DComplex> const &, DComplex const &);
template LogicalArray operator==(DComplex const &, Array<DComplex> const &);
template LogicalArray operator>(Array<DComplex> const &, Array<DComplex> const &);
template LogicalArray operator>(Array<DComplex> const &, DComplex const &);
template LogicalArray operator>(DComplex const &, Array<DComplex> const &);
template LogicalArray operator>=(Array<DComplex> const &, Array<DComplex> const &);
template LogicalArray operator>=(Array<DComplex> const &, DComplex const &);
template LogicalArray operator>=(DComplex const &, Array<DComplex> const &);
template LogicalArray near(Array<DComplex> const &, Array<DComplex> const &, Double);
template LogicalArray near(Array<DComplex> const &, DComplex const &, Double);
template LogicalArray near(DComplex const &, Array<DComplex> const &, Double);
template LogicalArray nearAbs(Array<DComplex> const &, Array<DComplex> const &, Double);
template LogicalArray nearAbs(Array<DComplex> const &, DComplex const &, Double);
template LogicalArray nearAbs(DComplex const &, Array<DComplex> const &, Double);
} //# NAMESPACE - END
