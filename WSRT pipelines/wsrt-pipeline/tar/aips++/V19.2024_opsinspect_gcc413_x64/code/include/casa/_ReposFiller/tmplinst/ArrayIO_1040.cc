// ArrayIO_1040.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayIO.cc>
#include <casa/Logging/LogIO.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template LogIO & operator<<(LogIO &, Array<Complex> const &);
template LogIO & operator<<(LogIO &, Array<DComplex> const &);
template LogIO & operator<<(LogIO &, Array<String> const &);
template LogIO & operator<<(LogIO &, Array<Double> const &);
template LogIO & operator<<(LogIO &, Array<Float> const &);
template LogIO & operator<<(LogIO &, Array<Int> const &);
template LogIO & operator<<(LogIO &, Array<uInt> const &);
template LogIO & operator<<(LogIO &, Array<Short> const &);
template LogIO & operator<<(LogIO &, Array<uChar> const &);
template LogIO & operator<<(LogIO &, Array<Bool> const &);
} //# NAMESPACE - END
