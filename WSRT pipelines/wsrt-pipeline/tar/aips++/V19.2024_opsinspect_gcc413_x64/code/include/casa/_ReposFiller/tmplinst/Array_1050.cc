// Array_1050.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<DComplex>;
#ifdef AIPS_SUN_NATIVE
template class Array<DComplex>::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
