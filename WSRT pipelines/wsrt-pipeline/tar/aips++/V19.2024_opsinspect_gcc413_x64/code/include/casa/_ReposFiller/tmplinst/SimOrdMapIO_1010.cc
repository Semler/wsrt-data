// SimOrdMapIO_1010.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/SimOrdMapIO.cc>
namespace casa { //# NAMESPACE - BEGIN
template AipsIO & operator>>(AipsIO &, SimpleOrderedMap<Int, Int> &);
template AipsIO & operator<<(AipsIO &, SimpleOrderedMap<Int, Int> const &);
} //# NAMESPACE - END
