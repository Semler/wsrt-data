// CountedPtr_1300.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Quanta/Quantum.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<Quantum<Float> > >;
template class CountedConstPtr<Block<Quantum<Float> > >;
template class SimpleCountedPtr<Block<Quantum<Float> > >;
template class SimpleCountedConstPtr<Block<Quantum<Float> > >;
template class PtrRep<Block<Quantum<Float> > >;
} //# NAMESPACE - END
