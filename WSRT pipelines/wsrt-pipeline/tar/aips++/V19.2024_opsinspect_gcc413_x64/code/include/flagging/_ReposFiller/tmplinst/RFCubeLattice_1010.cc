// RFCubeLattice_1010.cc -- Tue Apr  1 12:24:01 BST 2008 -- renting
#include <flagging/Flagging/RFCubeLattice.cc>
#include <flagging/Flagging/RFChunkStats.h>
namespace casa { //# NAMESPACE - BEGIN
template class RFCubeLattice<uInt>;
template class RFCubeLatticeIterator<uInt>;
} //# NAMESPACE - END
