// OrderedMap_1000.cc -- Tue Apr  1 12:13:45 BST 2008 -- renting
#include <casa/Containers/OrderedMap.cc>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedMap<String, Double>;
template class OrderedMapRep<String, Double>;
template class OrderedMapNotice<String, Double>;
template class OrderedMapIterRep<String, Double>;
} //# NAMESPACE - END
