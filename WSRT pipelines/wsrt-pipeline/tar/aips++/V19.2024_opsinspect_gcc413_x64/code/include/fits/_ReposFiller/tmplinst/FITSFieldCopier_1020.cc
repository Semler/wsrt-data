// FITSFieldCopier_1020.cc -- Tue Apr  1 12:14:23 BST 2008 -- renting
#include <fits/FITS/FITSFieldCopier.h>
namespace casa { //# NAMESPACE - BEGIN
template class VariableArrayFITSFieldCopier<Bool, FitsLogical>;
template class VariableArrayFITSFieldCopier<Complex, Complex>;
template class VariableArrayFITSFieldCopier<DComplex, DComplex>;
template class VariableArrayFITSFieldCopier<Double, Double>;
template class VariableArrayFITSFieldCopier<Float, Float>;
template class VariableArrayFITSFieldCopier<Int, FitsLong>;
template class VariableArrayFITSFieldCopier<Short, Short>;
template class VariableArrayFITSFieldCopier<uChar, uChar>;
} //# NAMESPACE - END
