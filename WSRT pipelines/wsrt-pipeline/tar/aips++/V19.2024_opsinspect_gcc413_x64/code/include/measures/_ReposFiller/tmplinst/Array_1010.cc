// Array_1010.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <measures/Measures/MFrequency.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<MFrequency>;
#ifdef AIPS_SUN_NATIVE
template class Array<MFrequency>::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
