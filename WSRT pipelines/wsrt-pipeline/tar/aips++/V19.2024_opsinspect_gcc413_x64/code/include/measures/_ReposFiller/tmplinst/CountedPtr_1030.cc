// CountedPtr_1030.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <measures/Measures/MFrequency.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<MFrequency> >;
template class CountedPtr<Block<MFrequency> >;
template class PtrRep<Block<MFrequency> >;
template class SimpleCountedConstPtr<Block<MFrequency> >;
template class SimpleCountedPtr<Block<MFrequency> >;
} //# NAMESPACE - END
