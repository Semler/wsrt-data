// MeasBase_1020.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <measures/Measures/MeasBase.cc>
#include <casa/Quanta/MVDoppler.h>
#include <measures/Measures/MDoppler.h>
#include <measures/Measures/MeasRef.h>
namespace casa { //# NAMESPACE - BEGIN
template class MeasBase<MVDoppler, MeasRef<MDoppler> >;
} //# NAMESPACE - END
