// MeasBase_1030.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <measures/Measures/MeasBase.cc>
#include <casa/Quanta/MVEarthMagnetic.h>
#include <measures/Measures/MEarthMagnetic.h>
#include <measures/Measures/MeasRef.h>
namespace casa { //# NAMESPACE - BEGIN
template class MeasBase<MVEarthMagnetic, MeasRef<MEarthMagnetic> >;
} //# NAMESPACE - END
