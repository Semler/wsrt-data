// Array_1020.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <measures/Measures/MPosition.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<MPosition>;
#ifdef AIPS_SUN_NATIVE
template class Array<MPosition>::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
