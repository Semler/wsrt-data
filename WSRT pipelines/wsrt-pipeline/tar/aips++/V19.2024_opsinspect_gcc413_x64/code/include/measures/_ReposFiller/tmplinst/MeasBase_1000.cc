// MeasBase_1000.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <measures/Measures/MeasBase.cc>
#include <casa/Quanta/MVBaseline.h>
#include <measures/Measures/MBaseline.h>
#include <measures/Measures/MeasRef.h>
namespace casa { //# NAMESPACE - BEGIN
template class MeasBase<MVBaseline, MeasRef<MBaseline> >;
} //# NAMESPACE - END
