// CountedPtr_1020.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <measures/Measures/MDirection.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<MDirection> >;
template class CountedPtr<Block<MDirection> >;
template class PtrRep<Block<MDirection> >;
template class SimpleCountedConstPtr<Block<MDirection> >;
template class SimpleCountedPtr<Block<MDirection> >;
} //# NAMESPACE - END
