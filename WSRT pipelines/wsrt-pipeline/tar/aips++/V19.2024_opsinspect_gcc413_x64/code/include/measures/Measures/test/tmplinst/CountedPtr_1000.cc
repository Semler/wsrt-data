// CountedPtr_1000.cc -- Tue Apr  1 12:11:58 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Quanta/MVEpoch.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<MVEpoch> >;
template class CountedPtr<Block<MVEpoch> >;
template class PtrRep<Block<MVEpoch> >;
template class SimpleCountedConstPtr<Block<MVEpoch> >;
template class SimpleCountedPtr<Block<MVEpoch> >;
} //# NAMESPACE - END
