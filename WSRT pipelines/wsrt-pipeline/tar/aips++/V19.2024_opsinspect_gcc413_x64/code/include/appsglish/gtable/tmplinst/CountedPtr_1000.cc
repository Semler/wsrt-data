// CountedPtr_1000.cc -- Tue Apr  1 12:35:33 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <tables/Tables/TableIndexProxy.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<TableIndexProxy>;
template class CountedPtr<TableIndexProxy>;
template class PtrRep<TableIndexProxy>;
template class SimpleCountedConstPtr<TableIndexProxy>;
template class SimpleCountedPtr<TableIndexProxy>;
} //# NAMESPACE - END
