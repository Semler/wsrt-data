// RFCubeLattice_1000.cc -- Tue Apr  1 12:35:07 BST 2008 -- renting
#include <flagging/Flagging/RFCubeLattice.cc>
#include <flagging/Flagging/RFChunkStats.h>
namespace casa { //# NAMESPACE - BEGIN
template class RFCubeLattice<RFlagWord>;
template class RFCubeLatticeIterator<RFlagWord>;
} //# NAMESPACE - END
