// Register_1000.cc -- Tue Apr  1 12:27:41 BST 2008 -- renting
#include <casa/Utilities/Register.cc>
#include <casa/Containers/OrderedMap.h>
#include <synthesis/Parallel/Algorithm.h>
namespace casa { //# NAMESPACE - BEGIN
template uInt Register(OrderedMapNotice<Int, Algorithm *> const *);
} //# NAMESPACE - END
