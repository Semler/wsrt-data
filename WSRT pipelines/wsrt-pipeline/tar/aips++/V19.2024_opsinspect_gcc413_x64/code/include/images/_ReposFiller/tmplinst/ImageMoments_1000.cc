// ImageMoments_1000.cc -- Tue Apr  1 12:18:59 BST 2008 -- renting
#include <images/Images/ImageMoments.cc>
#include <images/Images/MomentCalculator.cc>
namespace casa { //# NAMESPACE - BEGIN
template class ImageMoments<Float>;
template class MomentCalcBase<Float>;
template class MomentClip<Float>;
template class MomentWindow<Float>;
template class MomentFit<Float>;
} //# NAMESPACE - END
