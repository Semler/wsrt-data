// SimOrdMap_1000.cc -- Tue Apr  1 12:18:58 BST 2008 -- renting
#include <casa/Containers/SimOrdMap.cc>
#include <casa/Containers/OrderedPair.cc>
#include <casa/Exceptions/Error.cc>
#include <images/Images/ImageOpener.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimpleOrderedMap<ImageOpener::ImageTypes, ImageOpener::OpenImageFunction*>;
template class OrderedPair<ImageOpener::ImageTypes, ImageOpener::OpenImageFunction*>;
template class indexError<ImageOpener::ImageTypes>;
} //# NAMESPACE - END
