// CountedPtr_1010.cc -- Tue Apr  1 12:22:33 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <msvis/MSVis/StokesVector.h>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<StokesVector> >;
template class CountedConstPtr<Block<StokesVector> >;
template class SimpleCountedPtr<Block<StokesVector> >;
template class SimpleCountedConstPtr<Block<StokesVector> >;
template class PtrRep<Block<StokesVector> >;
} //# NAMESPACE - END
