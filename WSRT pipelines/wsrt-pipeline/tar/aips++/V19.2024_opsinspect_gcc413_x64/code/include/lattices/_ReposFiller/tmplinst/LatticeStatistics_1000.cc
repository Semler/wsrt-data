// LatticeStatistics_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LatticeStatistics.cc>
#include <lattices/Lattices/LatticeApply.cc>
#include <lattices/Lattices/TiledCollapser.cc>
namespace casa { //# NAMESPACE - BEGIN
template class LatticeStatistics<Float>;
template class LatticeApply<Float, Double>;
template class StatsTiledCollapser<Float, Double>;
template class TiledCollapser<Float, Double>;
} //# NAMESPACE - END
