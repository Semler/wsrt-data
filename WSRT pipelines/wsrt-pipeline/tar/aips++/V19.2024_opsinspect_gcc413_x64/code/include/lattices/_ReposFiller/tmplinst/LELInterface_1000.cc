// LELInterface_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LELInterface.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class LELInterface<Float>;
template class LELInterface<Double>;
template class LELInterface<Complex>;
template class LELInterface<DComplex>;
template class LELInterface<Bool>;
} //# NAMESPACE - END
