// CountedPtr_1050.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/Lattice.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Lattice<Int> >;
template class CountedConstPtr<Lattice<Int> >;
template class SimpleCountedPtr<Lattice<Int> >;
template class SimpleCountedConstPtr<Lattice<Int> >;
template class PtrRep<Lattice<Int> >;
} //# NAMESPACE - END
