// CountedPtr_1060.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/Lattice.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Lattice<uInt> >;
template class CountedConstPtr<Lattice<uInt> >;
template class SimpleCountedPtr<Lattice<uInt> >;
template class SimpleCountedConstPtr<Lattice<uInt> >;
template class PtrRep<Lattice<uInt> >;
} //# NAMESPACE - END
