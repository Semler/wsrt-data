// LatticeIterator_1050.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LatticeIterator.cc>
#include <lattices/Lattices/LatticeIterInterface.cc>
namespace casa { //# NAMESPACE - BEGIN
template class LatticeIterator<uInt>;
template class RO_LatticeIterator<uInt>;
template class LatticeIterInterface<uInt>;
} //# NAMESPACE - END
