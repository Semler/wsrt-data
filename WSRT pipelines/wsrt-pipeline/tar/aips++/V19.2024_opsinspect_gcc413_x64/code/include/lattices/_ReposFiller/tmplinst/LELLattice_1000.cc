// LELLattice_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LELLattice.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class LELLattice<Float>;
template class LELLattice<Double>;
template class LELLattice<Complex>;
template class LELLattice<DComplex>;
template class LELLattice<Bool>;
} //# NAMESPACE - END
