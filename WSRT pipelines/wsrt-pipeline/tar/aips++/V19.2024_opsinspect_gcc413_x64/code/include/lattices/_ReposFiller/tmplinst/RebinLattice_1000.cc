// RebinLattice_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/RebinLattice.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class RebinLattice<Float>;
template class RebinLattice<Double>;
template class RebinLattice<Complex>;
template class RebinLattice<DComplex>;
template class RebinLattice<Bool>;
} //# NAMESPACE - END
