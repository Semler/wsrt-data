// LELFunction_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LELFunction.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class LELFunction1D<Float>;
template class LELFunction1D<Double>;
template class LELFunction1D<Complex>;
template class LELFunction1D<DComplex>;
template class LELFunctionReal1D<Float>;
template class LELFunctionReal1D<Double>;
template class LELFunctionND<Float>;
template class LELFunctionND<Double>;
template class LELFunctionND<Complex>;
template class LELFunctionND<DComplex>;
template class LELFunctionND<Bool>;
} //# NAMESPACE - END
