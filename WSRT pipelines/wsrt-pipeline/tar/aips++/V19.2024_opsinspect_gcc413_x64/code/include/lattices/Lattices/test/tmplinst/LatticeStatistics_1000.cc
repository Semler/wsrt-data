// LatticeStatistics_1000.cc -- Tue Apr  1 12:15:01 BST 2008 -- renting
#include <lattices/Lattices/LatticeStatistics.cc>
#include <casa/BasicSL/Complex.h>
#include <lattices/Lattices/LatticeApply.cc>
#include <lattices/Lattices/TiledCollapser.cc>
namespace casa { //# NAMESPACE - BEGIN
template class LatticeStatistics<Complex>;
template class LatticeApply<Complex, DComplex>;
template class StatsTiledCollapser<Complex, DComplex>;
template class TiledCollapser<Complex, DComplex>;
} //# NAMESPACE - END
