// LELCondition_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LELCondition.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class LELCondition<Float>;
template class LELCondition<Double>;
template class LELCondition<Complex>;
template class LELCondition<DComplex>;
template class LELCondition<Bool>;
} //# NAMESPACE - END
