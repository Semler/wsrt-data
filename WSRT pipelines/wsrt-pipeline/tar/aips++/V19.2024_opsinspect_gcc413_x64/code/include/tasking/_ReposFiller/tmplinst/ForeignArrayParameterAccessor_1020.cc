// ForeignArrayParameterAccessor_1020.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/ForeignArrayParameterAccessor.cc>
#include <measures/Measures/MeasureHolder.h>
#include <tasking/Tasking/ForeignBaseArrayParameterAccessor.cc>
namespace casa { //# NAMESPACE - BEGIN
template class ForeignVectorParameterAccessor<MeasureHolder>;
template class ForeignArrayParameterAccessor<MeasureHolder>;
template class ForeignBaseVectorParameterAccessor<MeasureHolder>;
template class ForeignBaseArrayParameterAccessor<MeasureHolder>;
} //# NAMESPACE - END
