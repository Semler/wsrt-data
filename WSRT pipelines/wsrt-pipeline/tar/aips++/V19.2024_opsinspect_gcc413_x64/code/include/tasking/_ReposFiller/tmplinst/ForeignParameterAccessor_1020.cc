// ForeignParameterAccessor_1020.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/ForeignParameterAccessor.cc>
#include <measures/Measures/MeasureHolder.h>
#include <measures/Measures/MEpoch.h>
#include <measures/Measures/MDirection.h>
#include <measures/Measures/MDoppler.h>
#include <measures/Measures/MFrequency.h>
#include <measures/Measures/MPosition.h>
#include <measures/Measures/MRadialVelocity.h>
#include <measures/Measures/MBaseline.h>
#include <measures/Measures/Muvw.h>
#include <measures/Measures/MEarthMagnetic.h>
#include <tasking/Tasking/ForeignBaseParameterAccessor.cc>
#include <tasking/Tasking/ForeignNSParameterAccessor.cc>
namespace casa { //# NAMESPACE - BEGIN
template class ForeignParameterAccessor<MeasureHolder>;
template class ForeignBaseParameterAccessor<MeasureHolder>;
template class ForeignBaseParameterAccessor<MEpoch>;
template class ForeignBaseParameterAccessor<MDirection>;
template class ForeignBaseParameterAccessor<MDoppler>;
template class ForeignBaseParameterAccessor<MFrequency>;
template class ForeignBaseParameterAccessor<MPosition>;
template class ForeignBaseParameterAccessor<MRadialVelocity>;
template class ForeignBaseParameterAccessor<MBaseline>;
template class ForeignBaseParameterAccessor<Muvw>;
template class ForeignBaseParameterAccessor<MEarthMagnetic>;
template class ForeignNSParameterAccessor<MEpoch>;
template class ForeignNSParameterAccessor<MDirection>;
template class ForeignNSParameterAccessor<MDoppler>;
template class ForeignNSParameterAccessor<MFrequency>;
template class ForeignNSParameterAccessor<MPosition>;
template class ForeignNSParameterAccessor<MRadialVelocity>;
template class ForeignNSParameterAccessor<MBaseline>;
template class ForeignNSParameterAccessor<Muvw>;
template class ForeignNSParameterAccessor<MEarthMagnetic>;
} //# NAMESPACE - END
