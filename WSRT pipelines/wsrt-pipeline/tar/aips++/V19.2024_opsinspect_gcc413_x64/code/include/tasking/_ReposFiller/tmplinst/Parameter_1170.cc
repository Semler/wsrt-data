// Parameter_1170.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/Parameter.cc>
#include <measures/Measures/MeasureHolder.h>
#include <measures/Measures/MEpoch.h>
#include <measures/Measures/MDirection.h>
#include <measures/Measures/MDoppler.h>
#include <measures/Measures/MFrequency.h>
#include <measures/Measures/MPosition.h>
#include <measures/Measures/MRadialVelocity.h>
#include <measures/Measures/MBaseline.h>
#include <measures/Measures/Muvw.h>
#include <measures/Measures/MEarthMagnetic.h>
namespace casa { //# NAMESPACE - BEGIN
template class Parameter<MeasureHolder>;
template class Parameter<MEpoch>;
template class Parameter<MDirection>;
template class Parameter<MDoppler>;
template class Parameter<MFrequency>;
template class Parameter<MPosition>;
template class Parameter<MRadialVelocity>;
template class Parameter<MBaseline>;
template class Parameter<Muvw>;
template class Parameter<MEarthMagnetic>;
} //# NAMESPACE - END
