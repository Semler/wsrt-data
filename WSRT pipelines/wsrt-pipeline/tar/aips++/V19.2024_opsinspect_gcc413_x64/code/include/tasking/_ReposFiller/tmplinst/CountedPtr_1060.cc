// CountedPtr_1060.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <tasking/Glish/XSysEvent.h>
#include <graphics/Graphics/X11Intrinsic.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<XSysEventSourceInfo>;
template class CountedPtr<XSysEventSourceInfo>;
template class PtrRep<XSysEventSourceInfo>;
template class SimpleCountedConstPtr<XSysEventSourceInfo>;
template class SimpleCountedPtr<XSysEventSourceInfo>;
} //# NAMESPACE - END
