// ParameterConstraint_1210.cc -- Tue Apr  1 12:28:14 BST 2008 -- renting
#include <tasking/Tasking/ParameterConstraint.cc>
#include <components/ComponentModels/SkyComponent.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class ParameterConstraint<Vector<SkyComponent> >;
template class ParameterConstraint<Array<SkyComponent> >;
} //# NAMESPACE - END
