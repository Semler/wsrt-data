// ParameterAccessor_1160.cc -- Tue Apr  1 12:28:14 BST 2008 -- renting
#include <tasking/Tasking/ParameterAccessor.cc>
#include <measures/Measures/MeasureHolder.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class ParameterAccessor<Vector<MeasureHolder> >;
template class ParameterAccessor<Array<MeasureHolder> >;
} //# NAMESPACE - END
