// CountedPtr_1000.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <tasking/Tasking/Index.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<Index> >;
template class CountedConstPtr<Block<Index> >;
template class SimpleCountedPtr<Block<Index> >;
template class SimpleCountedConstPtr<Block<Index> >;
template class PtrRep<Block<Index> >;
} //# NAMESPACE - END
