// Parameter_1000.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/Parameter.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class Parameter<Array<Complex> >;
template class Parameter<Array<DComplex> >;
} //# NAMESPACE - END
