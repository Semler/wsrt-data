// ArrayColumn_1000.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <tables/Tables/ArrayColumn.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template class ArrayColumn<Bool>;
template class ArrayColumn<uChar>;
template class ArrayColumn<Short>;
template class ArrayColumn<uShort>;
template class ArrayColumn<Int>;
template class ArrayColumn<uInt>;
template class ArrayColumn<Float>;
template class ArrayColumn<Double>;
template class ArrayColumn<Complex>;
template class ArrayColumn<DComplex>;
template class ArrayColumn<String>;
template class ROArrayColumn<Bool>;
template class ROArrayColumn<uChar>;
template class ROArrayColumn<uShort>;
template class ROArrayColumn<Short>;
template class ROArrayColumn<Int>;
template class ROArrayColumn<uInt>;
template class ROArrayColumn<Float>;
template class ROArrayColumn<Double>;
template class ROArrayColumn<Complex>;
template class ROArrayColumn<DComplex>;
template class ROArrayColumn<String>;
} //# NAMESPACE - END
