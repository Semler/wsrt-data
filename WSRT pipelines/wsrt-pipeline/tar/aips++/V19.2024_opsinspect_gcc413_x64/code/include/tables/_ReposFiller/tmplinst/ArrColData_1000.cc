// ArrColData_1000.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <tables/Tables/ArrColData.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template class ArrayColumnData<Bool>;
template class ArrayColumnData<Char>;
template class ArrayColumnData<uChar>;
template class ArrayColumnData<Short>;
template class ArrayColumnData<uShort>;
template class ArrayColumnData<Int>;
template class ArrayColumnData<uInt>;
template class ArrayColumnData<Float>;
template class ArrayColumnData<Double>;
template class ArrayColumnData<Complex>;
template class ArrayColumnData<DComplex>;
template class ArrayColumnData<String>;
} //# NAMESPACE - END
