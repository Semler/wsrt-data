// SimOrdMap_1000.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <casa/Containers/SimOrdMap.cc>
#include <casa/BasicSL/String.h>
#include <tables/Tables/BaseColDesc.h>
#include <tables/Tables/ColumnDesc.h>
#include <tables/Tables/DataManager.h>
#include <tables/Tables/RefColumn.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimpleOrderedMap<String, BaseColumnDesc *(*)(String const &)>;
template class SimpleOrderedMap<String, ColumnDesc>;
template class SimpleOrderedMap<String, DataManager *(*)(String const &, Record const &)>;
template class SimpleOrderedMap<String, RefColumn *>;
} //# NAMESPACE - END
