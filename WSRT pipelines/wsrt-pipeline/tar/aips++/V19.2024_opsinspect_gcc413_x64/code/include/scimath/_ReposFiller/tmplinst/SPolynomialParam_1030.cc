// SPolynomialParam_1030.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/SPolynomialParam.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class SPolynomialParam<AutoDiffA<Double> >;
template class SPolynomialParam<AutoDiffA<Float> >;
} //# NAMESPACE - END
