// SparseDiffMath_1020.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template SparseDiff<Complex> acos(SparseDiff<Complex> const &);
template SparseDiff<Complex> asin(SparseDiff<Complex> const &);
template SparseDiff<Complex> atan(SparseDiff<Complex> const &);
template SparseDiff<Complex> atan2(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template SparseDiff<Complex> cos(SparseDiff<Complex> const &);
template SparseDiff<Complex> cosh(SparseDiff<Complex> const &);
template SparseDiff<Complex> exp(SparseDiff<Complex> const &);
template SparseDiff<Complex> log(SparseDiff<Complex> const &);
template SparseDiff<Complex> log10(SparseDiff<Complex> const &);
template SparseDiff<Complex> erf(SparseDiff<Complex> const &);
template SparseDiff<Complex> erfc(SparseDiff<Complex> const &);
template SparseDiff<Complex> pow(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template SparseDiff<Complex> pow(SparseDiff<Complex> const &, Complex const &);
template SparseDiff<Complex> sin(SparseDiff<Complex> const &);
template SparseDiff<Complex> sinh(SparseDiff<Complex> const &);
template SparseDiff<Complex> sqrt(SparseDiff<Complex> const &);
template SparseDiff<Complex> abs(SparseDiff<Complex> const &);
template SparseDiff<Complex> ceil(SparseDiff<Complex> const &);
template SparseDiff<Complex> floor(SparseDiff<Complex> const &);
template SparseDiff<Complex> fmod(SparseDiff<Complex> const &, Complex const &);
template SparseDiff<Complex> fmod(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template SparseDiff<Complex> max(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template SparseDiff<Complex> min(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
} //# NAMESPACE - END
