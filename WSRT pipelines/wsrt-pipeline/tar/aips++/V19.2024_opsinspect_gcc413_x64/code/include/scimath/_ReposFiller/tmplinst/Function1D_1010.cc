// Function1D_1010.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Function1D.h>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template class Function1D<AutoDiff<Double> >;
template class Function1D<AutoDiff<Float> >;
} //# NAMESPACE - END
