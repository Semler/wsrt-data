// MatrixMathLA_1100.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/Arrays/Matrix.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template void CholeskyDecomp(Matrix<AutoDiff<Float> > &, Vector<AutoDiff<Float> > &);
template void CholeskySolve(Matrix<AutoDiff<Float> > &, Vector<AutoDiff<Float> > &, Vector<AutoDiff<Float> > &, Vector<AutoDiff<Float> > &);
} //# NAMESPACE - END
