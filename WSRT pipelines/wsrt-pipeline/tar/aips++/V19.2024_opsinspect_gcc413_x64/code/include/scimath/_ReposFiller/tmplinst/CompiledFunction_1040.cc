// CompiledFunction_1040.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/CompiledFunction.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class CompiledFunction<AutoDiffA<Double> >;
template class CompiledFunction<AutoDiffA<Float> >;
} //# NAMESPACE - END
