// SparseDiffMath_1010.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool operator!=(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template Bool operator<=(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template Bool operator<(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template Bool operator==(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template Bool operator>(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template Bool operator>=(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template Bool near(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template Bool operator!=(SparseDiff<DComplex> const &, DComplex const &);
template Bool operator<=(SparseDiff<DComplex> const &, DComplex const &);
template Bool operator<(SparseDiff<DComplex> const &, DComplex const &);
template Bool operator==(SparseDiff<DComplex> const &, DComplex const &);
template Bool operator>(SparseDiff<DComplex> const &, DComplex const &);
template Bool operator>=(SparseDiff<DComplex> const &, DComplex const &);
template Bool near(SparseDiff<DComplex> const &, DComplex const &);
template Bool operator!=(DComplex const &, SparseDiff<DComplex> const &);
template Bool operator<=(DComplex const &, SparseDiff<DComplex> const &);
template Bool operator<(DComplex const &, SparseDiff<DComplex> const &);
template Bool operator==(DComplex const &, SparseDiff<DComplex> const &);
template Bool operator>(DComplex const &, SparseDiff<DComplex> const &);
template Bool operator>=(DComplex const &, SparseDiff<DComplex> const &);
template Bool near(DComplex const &, SparseDiff<DComplex> const &);
} //# NAMESPACE - END
