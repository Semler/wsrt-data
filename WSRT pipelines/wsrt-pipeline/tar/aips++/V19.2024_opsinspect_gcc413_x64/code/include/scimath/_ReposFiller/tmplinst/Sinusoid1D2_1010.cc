// Sinusoid1D2_1010.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Functionals/Sinusoid1D2.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Sinusoid1D<AutoDiff<Double> >;
template class Sinusoid1D<AutoDiff<Float> >;
} //# NAMESPACE - END
