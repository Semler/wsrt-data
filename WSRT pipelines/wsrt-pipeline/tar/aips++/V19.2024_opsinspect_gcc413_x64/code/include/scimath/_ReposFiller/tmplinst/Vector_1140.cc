// Vector_1140.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Arrays/Vector.cc>
#include <scimath/Mathematics/RigidVector.h>
namespace casa { //# NAMESPACE - BEGIN
template class Vector<RigidVector<Double, 3> >;
template class Vector<RigidVector<Double, 2> >;
} //# NAMESPACE - END
