// AutoDiffMath_1110.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool near(AutoDiff<Float> const &, AutoDiff<Float> const &, Double const);
template Bool near(AutoDiff<Float> const &, Float const &, Double const);
template Bool near(Float const &, AutoDiff<Float> const &, Double const);
template Bool allnear(AutoDiff<Float> const &, AutoDiff<Float> const &);
template Bool allnear(AutoDiff<Float> const &, Float const &);
template Bool allnear(Float const &, AutoDiff<Float> const &);
template Bool allnear(AutoDiff<Float> const &, AutoDiff<Float> const &, Double const);
template Bool allnear(AutoDiff<Float> const &, Float const &, Double const);
template Bool allnear(Float const &, AutoDiff<Float> const &, Double const);
template Bool nearAbs(AutoDiff<Float> const &, AutoDiff<Float> const &);
template Bool nearAbs(AutoDiff<Float> const &, Float const &);
template Bool nearAbs(Float const &, AutoDiff<Float> const &);
template Bool nearAbs(AutoDiff<Float> const &, AutoDiff<Float> const &, Double const);
template Bool nearAbs(AutoDiff<Float> const &, Float const &, Double const);
template Bool nearAbs(Float const &, AutoDiff<Float> const &, Double const);
template Bool allnearAbs(AutoDiff<Float> const &, AutoDiff<Float> const &);
template Bool allnearAbs(AutoDiff<Float> const &, Float const &);
template Bool allnearAbs(Float const &, AutoDiff<Float> const &);
template Bool allnearAbs(AutoDiff<Float> const &, AutoDiff<Float> const &, Double const);
template Bool allnearAbs(AutoDiff<Float> const &, Float const &, Double const);
template Bool allnearAbs(Float const &, AutoDiff<Float> const &, Double const);
} //# NAMESPACE - END
