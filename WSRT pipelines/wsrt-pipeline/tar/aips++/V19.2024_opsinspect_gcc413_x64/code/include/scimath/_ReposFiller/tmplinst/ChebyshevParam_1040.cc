// ChebyshevParam_1040.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/ChebyshevParam.cc>
namespace casa { //# NAMESPACE - BEGIN
template class ChebyshevParam<Double>;
template class ChebyshevParam<Float>;
template class ChebyshevParamModeImpl<Double>;
template class ChebyshevParamModeImpl<Float>;
} //# NAMESPACE - END
