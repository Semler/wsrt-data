// GNoiseParam_1020.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/GNoiseParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class GNoiseParam<AutoDiff<Double> >;
template class GNoiseParam<AutoDiff<Float> >;
} //# NAMESPACE - END
