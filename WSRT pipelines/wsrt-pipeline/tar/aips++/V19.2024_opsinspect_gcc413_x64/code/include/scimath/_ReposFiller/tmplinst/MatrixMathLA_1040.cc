// MatrixMathLA_1040.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/Arrays/Matrix.h>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template void CholeskyDecomp(Matrix<DComplex> &, Vector<DComplex> &);
template void CholeskySolve(Matrix<DComplex> &, Vector<DComplex> &, Vector<DComplex> &, Vector<DComplex> &);
} //# NAMESPACE - END
