// BinarySearch_1000.cc -- Tue Apr  1 11:57:46 BST 2008 -- renting
#include <casa/Utilities/BinarySearch.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template Int binarySearchBrackets(Bool &, Block<Int> const &, Int const &, uInt, Int);
} //# NAMESPACE - END
