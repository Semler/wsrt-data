// FunctionHolder_1010.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/FunctionHolder.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool FunctionHolder<DComplex>::getType<AutoDiff<DComplex> >(String &, Function<AutoDiff<DComplex> > * &, RecordInterface const &);
} //# NAMESPACE - END
