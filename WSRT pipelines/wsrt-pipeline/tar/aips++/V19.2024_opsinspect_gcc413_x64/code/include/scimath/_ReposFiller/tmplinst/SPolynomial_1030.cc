// SPolynomial_1030.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/SPolynomial.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class SPolynomial<AutoDiffA<Double> >;
template class SPolynomial<AutoDiffA<Float> >;
} //# NAMESPACE - END
