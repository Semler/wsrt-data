// SparseDiffMath_1050.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template SparseDiff<DComplex> operator+(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> operator-(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> operator*(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template SparseDiff<DComplex> operator+(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template SparseDiff<DComplex> operator-(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template SparseDiff<DComplex> operator/(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template SparseDiff<DComplex> operator*(DComplex const &, SparseDiff<DComplex> const &);
template SparseDiff<DComplex> operator+(DComplex const &, SparseDiff<DComplex> const &);
template SparseDiff<DComplex> operator-(DComplex const &, SparseDiff<DComplex> const &);
template SparseDiff<DComplex> operator/(DComplex const &, SparseDiff<DComplex> const &);
template SparseDiff<DComplex> operator*(SparseDiff<DComplex> const &, DComplex const &);
template SparseDiff<DComplex> operator+(SparseDiff<DComplex> const &, DComplex const &);
template SparseDiff<DComplex> operator-(SparseDiff<DComplex> const &, DComplex const &);
template SparseDiff<DComplex> operator/(SparseDiff<DComplex> const &, DComplex const &);
} //# NAMESPACE - END
