// Function1D_1020.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Function1D.h>
#include <scimath/Mathematics/AutoDiffA.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class Function1D<AutoDiffA<Complex> >;
} //# NAMESPACE - END
