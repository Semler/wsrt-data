// ChebyshevParam_1020.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/ChebyshevParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class ChebyshevParam<AutoDiff<Double> >;
template class ChebyshevParam<AutoDiff<Float> >;
template class ChebyshevParamModeImpl<AutoDiff<Double> >;
template class ChebyshevParamModeImpl<AutoDiff<Float> >;
} //# NAMESPACE - END
