// Functional_1010.cc -- Tue Apr  1 11:57:46 BST 2008 -- renting
#include <casa/BasicMath/Functional.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template class Functional<Float, Array<Float> >;
template class Functional<Float, Double>;
template class Functional<Int, Double>;
template class Functional<uInt, Int>;
#include <casa/BasicSL/Complex.h>
template class Functional<Double, DComplex>;
template class Functional<uInt, DComplex>;
} //# NAMESPACE - END
