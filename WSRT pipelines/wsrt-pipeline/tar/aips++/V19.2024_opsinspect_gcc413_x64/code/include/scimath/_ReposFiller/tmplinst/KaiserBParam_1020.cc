// KaiserBParam_1020.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/KaiserBParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class KaiserBParam<AutoDiff<Double> >;
template class KaiserBParam<AutoDiff<Float> >;
} //# NAMESPACE - END
