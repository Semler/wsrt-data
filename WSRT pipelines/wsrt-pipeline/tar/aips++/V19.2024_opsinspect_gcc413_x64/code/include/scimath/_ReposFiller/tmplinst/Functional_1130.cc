// Functional_1130.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/BasicMath/Functional.cc>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template class Functional<Vector<Double>, AutoDiff<Double> >;
} //# NAMESPACE - END
