// Function1D_1040.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Function1D.h>
#include <scimath/Mathematics/AutoDiffA.h>
namespace casa { //# NAMESPACE - BEGIN
template class Function1D<AutoDiffA<Double> >;
template class Function1D<AutoDiffA<Float> >;
} //# NAMESPACE - END
