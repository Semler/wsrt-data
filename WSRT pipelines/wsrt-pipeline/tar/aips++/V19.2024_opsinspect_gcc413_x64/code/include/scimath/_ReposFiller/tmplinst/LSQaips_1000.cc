// LSQaips_1000.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LSQaips.cc>
#include <casa/Arrays/Array.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool LSQaips::getCovariance<Double>(Array<Double> &);
template Bool LSQaips::solveLoop<Double>(Double &, uInt &, Vector<Double> &, Bool doSVD);
template Bool LSQaips::solveLoop<Double>(uInt &, Vector<Double> &, Bool doSVD);
} //# NAMESPACE - END
