// AutoDiffMath_1100.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool near(AutoDiff<Double> const &, AutoDiff<Double> const &, Double const);
template Bool near(AutoDiff<Double> const &, Double const &, Double const);
template Bool near(Double const &, AutoDiff<Double> const &, Double const);
template Bool allnear(AutoDiff<Double> const &, AutoDiff<Double> const &);
template Bool allnear(AutoDiff<Double> const &, Double const &);
template Bool allnear(Double const &, AutoDiff<Double> const &);
template Bool allnear(AutoDiff<Double> const &, AutoDiff<Double> const &, Double const);
template Bool allnear(AutoDiff<Double> const &, Double const &, Double const);
template Bool allnear(Double const &, AutoDiff<Double> const &, Double const);
template Bool nearAbs(AutoDiff<Double> const &, AutoDiff<Double> const &);
template Bool nearAbs(AutoDiff<Double> const &, Double const &);
template Bool nearAbs(Double const &, AutoDiff<Double> const &);
template Bool nearAbs(AutoDiff<Double> const &, AutoDiff<Double> const &, Double const);
template Bool nearAbs(AutoDiff<Double> const &, Double const &, Double const);
template Bool nearAbs(Double const &, AutoDiff<Double> const &, Double const);
template Bool allnearAbs(AutoDiff<Double> const &, AutoDiff<Double> const &);
template Bool allnearAbs(AutoDiff<Double> const &, Double const &);
template Bool allnearAbs(Double const &, AutoDiff<Double> const &);
template Bool allnearAbs(AutoDiff<Double> const &, AutoDiff<Double> const &, Double const);
template Bool allnearAbs(AutoDiff<Double> const &, Double const &, Double const);
template Bool allnearAbs(Double const &, AutoDiff<Double> const &, Double const);
} //# NAMESPACE - END
