// MatrixMathLA_1000.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/Arrays/Matrix.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template void CholeskyDecomp(Matrix<Double> &, Vector<Double> &);
template void CholeskySolve(Matrix<Double> &, Vector<Double> &, Vector<Double> &, Vector<Double> &);
} //# NAMESPACE - END
