// LSQFit2_1030.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LSQFit2.cc>
#include <casa/Arrays/VectorSTLIterator.h>
namespace casa { //# NAMESPACE - BEGIN
typedef VectorSTLIterator<std::complex<Double> > It1;
typedef VectorSTLIterator<uInt> It1Int;
template void LSQFit::makeNorm<Double, It1>(It1 const &, Double const &, std::complex<Double> const &, Bool, Bool);
template void LSQFit::makeNorm<Double, It1>(It1 const &, Double const &, std::complex<Double> const &, LSQFit::AsReal, Bool, Bool);
template void LSQFit::makeNorm<Double, It1>(It1 const &, Double const &, std::complex<Double> const &, LSQFit::Complex, Bool, Bool);
template void LSQFit::makeNorm<Double, It1>(It1 const &, Double const &, std::complex<Double> const &, LSQFit::Conjugate, Bool, Bool);
template void LSQFit::makeNorm<Double, It1>(It1 const &, Double const &, std::complex<Double> const &, LSQFit::Separable, Bool, Bool);
template void LSQFit::makeNorm<Double, It1, It1Int>(uInt, It1Int const &, It1 const &, Double const &, std::complex<Double> const &, Bool, Bool);
template void LSQFit::makeNorm<Double, It1, It1Int>(uInt, It1Int const &, It1 const &, Double const &, std::complex<Double> const &, LSQFit::AsReal, Bool, Bool);
template void LSQFit::makeNorm<Double, It1, It1Int>(uInt, It1Int const &, It1 const &, Double const &, std::complex<Double> const &, LSQFit::Complex, Bool, Bool);
template void LSQFit::makeNorm<Double, It1, It1Int>(uInt, It1Int const &, It1 const &, Double const &, std::complex<Double> const &, LSQFit::Conjugate, Bool, Bool);
template void LSQFit::makeNorm<Double, It1, It1Int>(uInt, It1Int const &, It1 const &, Double const &, std::complex<Double> const &, LSQFit::Separable, Bool, Bool);
template void LSQFit::makeNorm<Double, std::complex<Double> >(std::vector<std::pair<uInt, std::complex<Double> > > const &, Double const &, std::complex<Double> const &, Bool, Bool);
template void LSQFit::makeNorm<Double, std::complex<Double> >(std::vector<std::pair<uInt, std::complex<Double> > > const &, Double const &, std::complex<Double> const &, LSQFit::AsReal, Bool, Bool);
template void LSQFit::makeNorm<Double, std::complex<Double> >(std::vector<std::pair<uInt, std::complex<Double> > > const &, Double const &, std::complex<Double> const &, LSQFit::Complex, Bool, Bool);
template void LSQFit::makeNorm<Double, std::complex<Double> >(std::vector<std::pair<uInt, std::complex<Double> > > const &, Double const &, std::complex<Double> const &, LSQFit::Conjugate, Bool, Bool);
template void LSQFit::makeNorm<Double, std::complex<Double> >(std::vector<std::pair<uInt, std::complex<Double> > > const &, Double const &, std::complex<Double> const &, LSQFit::Separable, Bool, Bool);
template Bool LSQFit::addConstraint<Double, It1>(It1 const &, std::complex<Double> const &);
template Bool LSQFit::addConstraint<Double, It1, It1Int>(uInt, It1Int const &, It1 const &, std::complex<Double> const &);
template Bool LSQFit::setConstraint<Double, It1>(uInt, It1 const &, std::complex<Double> const &);
template Bool LSQFit::setConstraint<Double, It1, It1Int>(uInt, uInt, It1Int const &, It1 const &, std::complex<Double> const &);
template Bool LSQFit::getConstraint<It1>(uInt, It1 &) const;
template void LSQFit::solve<It1>(It1 &);
template Bool LSQFit::solveLoop<It1>(Double &, uInt &, It1 &, Bool);
template Bool LSQFit::solveLoop<It1>(uInt &, It1 &, Bool);
template void LSQFit::copy<It1>(Double const *, Double const *, It1 &, LSQComplex);
template void LSQFit::uncopy<It1>(Double *, Double const *, It1 &, LSQComplex);
template void LSQFit::copyDiagonal<It1>(It1 &, LSQComplex);
template Bool LSQFit::getErrors<It1>(It1 &);
} //# NAMESPACE - END
