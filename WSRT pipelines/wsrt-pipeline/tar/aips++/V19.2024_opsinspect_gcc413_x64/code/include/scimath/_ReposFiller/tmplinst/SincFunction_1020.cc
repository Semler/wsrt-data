// SincFunction_1020.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/SincFunction.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class SincFunction<AutoDiff<Double> >;
template class SincFunction<AutoDiff<Float> >;
} //# NAMESPACE - END
