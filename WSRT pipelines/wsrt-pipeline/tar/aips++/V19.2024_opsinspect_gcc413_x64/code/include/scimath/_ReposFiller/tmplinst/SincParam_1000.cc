// SincParam_1000.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Functionals/SincParam.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class SincParam<AutoDiff<DComplex> >;
template class SincParam<AutoDiff<Complex> >;
} //# NAMESPACE - END
