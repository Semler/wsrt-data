// ArrayMath_1000.cc -- Tue Apr  1 11:57:46 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<Complex> operator*(Array<Complex> const &, Array<Complex> const &);
template Array<Complex> operator*(Complex const &, Array<Complex> const &);
template void indgen(Array<Complex> &);
template void indgen(Array<Complex> &, Complex, Complex);
template Array<Complex> operator+(Array<Complex> const &, Complex const &);
} //# NAMESPACE - END
