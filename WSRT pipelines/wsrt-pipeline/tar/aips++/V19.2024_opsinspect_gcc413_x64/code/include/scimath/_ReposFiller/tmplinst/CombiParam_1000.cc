// CombiParam_1000.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/CombiParam.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class CombiParam<AutoDiff<DComplex> >;
template class CombiParam<AutoDiff<Complex> >;
} //# NAMESPACE - END
