// AutoDiffMath_1040.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool operator!=(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template Bool operator<=(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template Bool operator<(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template Bool operator==(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template Bool operator>(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template Bool operator>=(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template Bool near(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template Bool operator!=(AutoDiff<Complex> const &, Complex const &);
template Bool operator<=(AutoDiff<Complex> const &, Complex const &);
template Bool operator<(AutoDiff<Complex> const &, Complex const &);
template Bool operator==(AutoDiff<Complex> const &, Complex const &);
template Bool operator>(AutoDiff<Complex> const &, Complex const &);
template Bool operator>=(AutoDiff<Complex> const &, Complex const &);
template Bool near(AutoDiff<Complex> const &, Complex const &);
template Bool operator!=(Complex const &, AutoDiff<Complex> const &);
template Bool operator<=(Complex const &, AutoDiff<Complex> const &);
template Bool operator<(Complex const &, AutoDiff<Complex> const &);
template Bool operator==(Complex const &, AutoDiff<Complex> const &);
template Bool operator>(Complex const &, AutoDiff<Complex> const &);
template Bool operator>=(Complex const &, AutoDiff<Complex> const &);
template Bool near(Complex const &, AutoDiff<Complex> const &);
} //# NAMESPACE - END
