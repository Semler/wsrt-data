// PolynomialParam_1020.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/PolynomialParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class PolynomialParam<AutoDiff<Double> >;
template class PolynomialParam<AutoDiff<Float> >;
} //# NAMESPACE - END
