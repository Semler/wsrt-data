// CompiledParam_1020.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/CompiledParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class CompiledParam<AutoDiff<Double> >;
template class CompiledParam<AutoDiff<Float> >;
} //# NAMESPACE - END
