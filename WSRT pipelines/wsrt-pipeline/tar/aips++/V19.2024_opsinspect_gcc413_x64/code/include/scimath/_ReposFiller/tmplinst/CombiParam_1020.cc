// CombiParam_1020.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/CombiParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class CombiParam<AutoDiff<Double> >;
template class CombiParam<AutoDiff<Float> >;
} //# NAMESPACE - END
