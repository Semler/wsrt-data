// AutoDiffMath_1060.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template AutoDiff<Double> acos(AutoDiff<Double> const &);
template AutoDiff<Double> asin(AutoDiff<Double> const &);
template AutoDiff<Double> atan(AutoDiff<Double> const &);
template AutoDiff<Double> atan2(AutoDiff<Double> const &, AutoDiff<Double> const &);
template AutoDiff<Double> cos(AutoDiff<Double> const &);
template AutoDiff<Double> cosh(AutoDiff<Double> const &);
template AutoDiff<Double> exp(AutoDiff<Double> const &);
template AutoDiff<Double> log(AutoDiff<Double> const &);
template AutoDiff<Double> log10(AutoDiff<Double> const &);
template AutoDiff<Double> erf(AutoDiff<Double> const &);
template AutoDiff<Double> erfc(AutoDiff<Double> const &);
template AutoDiff<Double> pow(AutoDiff<Double> const &, AutoDiff<Double> const &);
template AutoDiff<Double> pow(AutoDiff<Double> const &, Double const &);
template AutoDiff<Double> square(AutoDiff<Double> const &);
template AutoDiff<Double> cube(AutoDiff<Double> const &);
template AutoDiff<Double> sin(AutoDiff<Double> const &);
template AutoDiff<Double> sinh(AutoDiff<Double> const &);
template AutoDiff<Double> sqrt(AutoDiff<Double> const &);
template AutoDiff<Double> tan(AutoDiff<Double> const &);
template AutoDiff<Double> tanh(AutoDiff<Double> const &);
template AutoDiff<Double> abs(AutoDiff<Double> const &);
template AutoDiff<Double> ceil(AutoDiff<Double> const &);
template AutoDiff<Double> floor(AutoDiff<Double> const &);
template AutoDiff<Double> fmod(AutoDiff<Double> const &, Double const &);
template AutoDiff<Double> fmod(AutoDiff<Double> const &, AutoDiff<Double> const &);
template AutoDiff<Double> max(AutoDiff<Double> const &, AutoDiff<Double> const &);
template AutoDiff<Double> min(AutoDiff<Double> const &, AutoDiff<Double> const &);
} //# NAMESPACE - END
