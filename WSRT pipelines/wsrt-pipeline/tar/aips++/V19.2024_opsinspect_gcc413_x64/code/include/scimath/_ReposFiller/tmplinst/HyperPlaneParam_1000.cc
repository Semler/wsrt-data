// HyperPlaneParam_1000.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/HyperPlaneParam.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class HyperPlaneParam<AutoDiff<DComplex> >;
template class HyperPlaneParam<AutoDiff<Complex> >;
} //# NAMESPACE - END
