// KaiserBFunction_1000.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/KaiserBFunction.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class KaiserBFunction<AutoDiff<DComplex> >;
template class KaiserBFunction<AutoDiff<Complex> >;
} //# NAMESPACE - END
