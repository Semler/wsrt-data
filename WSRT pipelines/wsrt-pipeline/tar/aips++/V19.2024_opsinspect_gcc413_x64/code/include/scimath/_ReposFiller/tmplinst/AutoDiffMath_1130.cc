// AutoDiffMath_1130.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool operator!=(AutoDiff<Float> const &, AutoDiff<Float> const &);
template Bool operator<=(AutoDiff<Float> const &, AutoDiff<Float> const &);
template Bool operator<(AutoDiff<Float> const &, AutoDiff<Float> const &);
template Bool operator==(AutoDiff<Float> const &, AutoDiff<Float> const &);
template Bool operator>(AutoDiff<Float> const &, AutoDiff<Float> const &);
template Bool operator>=(AutoDiff<Float> const &, AutoDiff<Float> const &);
template Bool near(AutoDiff<Float> const &, AutoDiff<Float> const &);
template Bool operator!=(AutoDiff<Float> const &, Float const &);
template Bool operator<=(AutoDiff<Float> const &, Float const &);
template Bool operator<(AutoDiff<Float> const &, Float const &);
template Bool operator==(AutoDiff<Float> const &, Float const &);
template Bool operator>(AutoDiff<Float> const &, Float const &);
template Bool operator>=(AutoDiff<Float> const &, Float const &);
template Bool near(AutoDiff<Float> const &, Float const &);
template Bool operator!=(Float const &, AutoDiff<Float> const &);
template Bool operator<=(Float const &, AutoDiff<Float> const &);
template Bool operator<(Float const &, AutoDiff<Float> const &);
template Bool operator==(Float const &, AutoDiff<Float> const &);
template Bool operator>(Float const &, AutoDiff<Float> const &);
template Bool operator>=(Float const &, AutoDiff<Float> const &);
template Bool near(Float const &, AutoDiff<Float> const &);
} //# NAMESPACE - END
