// GNoiseFunction_1000.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/GNoiseFunction.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class GNoiseFunction<AutoDiff<DComplex> >;
template class GNoiseFunction<AutoDiff<Complex> >;
} //# NAMESPACE - END
