// Compound2Function_1010.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Compound2Function.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class CompoundFunction<AutoDiff<Double> >;
template class CompoundFunction<AutoDiff<Float> >;
} //# NAMESPACE - END
