// SincFunction_1000.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/SincFunction.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class SincFunction<AutoDiff<DComplex> >;
template class SincFunction<AutoDiff<Complex> >;
} //# NAMESPACE - END
