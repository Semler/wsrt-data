// UnaryFunction_1030.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Functionals/UnaryFunction.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class UnaryFunction<Double>;
template class UnaryFunction<Float>;
} //# NAMESPACE - END
