// LinearFit_1000.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LinearFit.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class LinearFit<AutoDiff<Complex> >;
template class LinearFit<AutoDiffA<Complex> >;
} //# NAMESPACE - END
