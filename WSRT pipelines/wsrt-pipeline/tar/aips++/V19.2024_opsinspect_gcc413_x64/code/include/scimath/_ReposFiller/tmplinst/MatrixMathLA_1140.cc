// MatrixMathLA_1140.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Matrix<Complex> invert(Matrix<Complex> const &);
} //# NAMESPACE - END
