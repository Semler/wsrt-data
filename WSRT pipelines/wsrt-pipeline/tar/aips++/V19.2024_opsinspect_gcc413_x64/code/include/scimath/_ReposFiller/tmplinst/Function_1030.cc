// Function_1030.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Function.cc>
#include <scimath/Mathematics/AutoDiffA.h>
namespace casa { //# NAMESPACE - BEGIN
template class Function<AutoDiffA<Double> >;
template class Function<AutoDiffA<Float> >;
} //# NAMESPACE - END
