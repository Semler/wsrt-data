// AutoDiffMath_1090.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template AutoDiff<Float> operator+(AutoDiff<Float> const &);
template AutoDiff<Float> operator-(AutoDiff<Float> const &);
template AutoDiff<Float> operator*(AutoDiff<Float> const &, AutoDiff<Float> const &);
template AutoDiff<Float> operator+(AutoDiff<Float> const &, AutoDiff<Float> const &);
template AutoDiff<Float> operator-(AutoDiff<Float> const &, AutoDiff<Float> const &);
template AutoDiff<Float> operator/(AutoDiff<Float> const &, AutoDiff<Float> const &);
template AutoDiff<Float> operator*(Float const &, AutoDiff<Float> const &);
template AutoDiff<Float> operator+(Float const &, AutoDiff<Float> const &);
template AutoDiff<Float> operator-(Float const &, AutoDiff<Float> const &);
template AutoDiff<Float> operator/(Float const &, AutoDiff<Float> const &);
template AutoDiff<Float> operator*(AutoDiff<Float> const &, Float const &);
template AutoDiff<Float> operator+(AutoDiff<Float> const &, Float const &);
template AutoDiff<Float> operator-(AutoDiff<Float> const &, Float const &);
template AutoDiff<Float> operator/(AutoDiff<Float> const &, Float const &);
} //# NAMESPACE - END
