// Cube_1040.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/Cube.cc>
#include <casa/Containers/OrderedMap.h>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class Cube<OrderedMap<Double, SquareMatrix<Complex, 2>*>*>;
} //# NAMESPACE - END
