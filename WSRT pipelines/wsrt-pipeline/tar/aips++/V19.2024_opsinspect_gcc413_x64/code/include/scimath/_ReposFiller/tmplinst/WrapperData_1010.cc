// WrapperData_1010.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Functionals/WrapperData.h>
namespace casa { //# NAMESPACE - BEGIN
template class WrapperData<Double, Double, Double, True, True>;
template class WrapperData<Double, Double, Double, True, False>;
template class WrapperData<Double, Double, Double, False, True>;
template class WrapperData<Double, Double, Double, False, False>;
template class WrapperData<Double, Double, Vector<Double>, True, True>;
template class WrapperData<Double, Double, Vector<Double>, False, True>;
template class WrapperData<Double, Vector<Double>, Double, True, True>;
template class WrapperData<Double, Vector<Double>, Double, True, False>;
template class WrapperData<Double, Vector<Double>, Vector<Double>, True, True>;
template class WrapperData<Float, Float, Float, True, True>;
template class WrapperData<Float, Float, Float, True, False>;
template class WrapperData<Float, Float, Float, False, True>;
template class WrapperData<Float, Float, Float, False, False>;
template class WrapperData<Float, Float, Vector<Float>, True, True>;
template class WrapperData<Float, Float, Vector<Float>, False, True>;
template class WrapperData<Float, Vector<Float>, Float, True, True>;
template class WrapperData<Float, Vector<Float>, Float, True, False>;
template class WrapperData<Float, Vector<Float>, Vector<Float>, True, True>;
} //# NAMESPACE - END
