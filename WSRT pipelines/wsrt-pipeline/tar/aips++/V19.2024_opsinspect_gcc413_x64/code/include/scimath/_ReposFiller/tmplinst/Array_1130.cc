// Array_1130.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <scimath/Mathematics/AutoDiffA.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<AutoDiffA<Float> >;
#ifdef AIPS_SUN_NATIVE
template class Array<AutoDiffA<Float> >::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
