// Function_1000.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Function.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class Function<AutoDiff<Complex> >;
template class Function<AutoDiff<DComplex> >;
} //# NAMESPACE - END
