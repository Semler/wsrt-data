// AutoDiffMath_1010.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template AutoDiff<Complex> operator+(AutoDiff<Complex> const &);
template AutoDiff<Complex> operator-(AutoDiff<Complex> const &);
template AutoDiff<Complex> operator*(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template AutoDiff<Complex> operator+(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template AutoDiff<Complex> operator-(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template AutoDiff<Complex> operator/(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template AutoDiff<Complex> operator*(Complex const &, AutoDiff<Complex> const &);
template AutoDiff<Complex> operator+(Complex const &, AutoDiff<Complex> const &);
template AutoDiff<Complex> operator-(Complex const &, AutoDiff<Complex> const &);
template AutoDiff<Complex> operator/(Complex const &, AutoDiff<Complex> const &);
template AutoDiff<Complex> operator*(AutoDiff<Complex> const &, Complex const &);
template AutoDiff<Complex> operator+(AutoDiff<Complex> const &, Complex const &);
template AutoDiff<Complex> operator-(AutoDiff<Complex> const &, Complex const &);
template AutoDiff<Complex> operator/(AutoDiff<Complex> const &, Complex const &);
} //# NAMESPACE - END
