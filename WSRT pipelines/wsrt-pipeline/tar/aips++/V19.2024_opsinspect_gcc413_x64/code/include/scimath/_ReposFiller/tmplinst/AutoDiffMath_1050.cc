// AutoDiffMath_1050.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool operator!=(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template Bool operator<=(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template Bool operator<(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template Bool operator==(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template Bool operator>(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template Bool operator>=(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template Bool near(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template Bool operator!=(AutoDiff<DComplex> const &, DComplex const &);
template Bool operator<=(AutoDiff<DComplex> const &, DComplex const &);
template Bool operator<(AutoDiff<DComplex> const &, DComplex const &);
template Bool operator==(AutoDiff<DComplex> const &, DComplex const &);
template Bool operator>(AutoDiff<DComplex> const &, DComplex const &);
template Bool operator>=(AutoDiff<DComplex> const &, DComplex const &);
template Bool near(AutoDiff<DComplex> const &, DComplex const &);
template Bool operator!=(DComplex const &, AutoDiff<DComplex> const &);
template Bool operator<=(DComplex const &, AutoDiff<DComplex> const &);
template Bool operator<(DComplex const &, AutoDiff<DComplex> const &);
template Bool operator==(DComplex const &, AutoDiff<DComplex> const &);
template Bool operator>(DComplex const &, AutoDiff<DComplex> const &);
template Bool operator>=(DComplex const &, AutoDiff<DComplex> const &);
template Bool near(DComplex const &, AutoDiff<DComplex> const &);
} //# NAMESPACE - END
