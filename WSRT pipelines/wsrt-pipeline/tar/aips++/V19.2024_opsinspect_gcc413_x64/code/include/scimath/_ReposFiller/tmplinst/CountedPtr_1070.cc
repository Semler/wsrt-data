// CountedPtr_1070.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <scimath/Mathematics/RigidVector.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<RigidVector<Double, 3> > >;
template class CountedConstPtr<Block<RigidVector<Double, 3> > >;
template class SimpleCountedPtr<Block<RigidVector<Double, 3> > >;
template class SimpleCountedConstPtr<Block<RigidVector<Double, 3> > >;
template class PtrRep<Block<RigidVector<Double, 3> > >;
template class CountedPtr<Block<RigidVector<Double, 2> > >;
template class CountedConstPtr<Block<RigidVector<Double, 2> > >;
template class SimpleCountedPtr<Block<RigidVector<Double, 2> > >;
template class SimpleCountedConstPtr<Block<RigidVector<Double, 2> > >;
template class PtrRep<Block<RigidVector<Double, 2> > >;
} //# NAMESPACE - END
