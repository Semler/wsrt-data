// CountedPtr_1060.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Utilities/CountedPtr.h>
#include <scimath/Functionals/Function1D.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<CountedPtr<Function1D<Float> > > >;
template class CountedPtr<Block<CountedPtr<Function1D<Float> > > >;
template class SimpleCountedConstPtr<Block<CountedPtr<Function1D<Float> > > >;
template class SimpleCountedPtr<Block<CountedPtr<Function1D<Float> > > >;
template class PtrRep<Block<CountedPtr<Function1D<Float> > > >;
} //# NAMESPACE - END
