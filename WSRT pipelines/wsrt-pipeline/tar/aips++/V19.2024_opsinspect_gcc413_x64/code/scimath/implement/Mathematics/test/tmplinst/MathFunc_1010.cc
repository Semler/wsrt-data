// MathFunc_1010.cc -- Tue Apr  1 11:58:13 BST 2008 -- renting
#include <scimath/Mathematics/MathFunc.cc>
namespace casa { //# NAMESPACE - BEGIN
template class ExpSincConv<Float>;
template class GaussianConv<Float>;
template class KB_Conv<Float>;
template class MathFunc<Float>;
template class Mod_KB_Conv<Float>;
template class Sinc_Conv<Float>;
template class Sph_Conv<Float>;
template class Unary<Float>;
} //# NAMESPACE - END
