// NonLinearFit_1040.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/NonLinearFit.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class NonLinearFit<AutoDiff<Float> >;
template class NonLinearFit<AutoDiffA<Float> >;
} //# NAMESPACE - END
