// Functional_1020.cc -- Tue Apr  1 11:57:46 BST 2008 -- renting
#include <casa/BasicMath/Functional.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template class Functional<uInt, Array<Float> >;
template class Functional<uInt, Array<DComplex> >;
} //# NAMESPACE - END
