// MaskedArray_1060.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/MaskedArray.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class MaskedArray<SquareMatrix<Complex, 4> >;
} //# NAMESPACE - END
