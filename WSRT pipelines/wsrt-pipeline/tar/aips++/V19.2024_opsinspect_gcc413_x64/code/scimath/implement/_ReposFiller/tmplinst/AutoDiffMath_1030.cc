// AutoDiffMath_1030.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template AutoDiff<DComplex> operator+(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> operator-(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> operator*(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template AutoDiff<DComplex> operator+(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template AutoDiff<DComplex> operator-(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template AutoDiff<DComplex> operator/(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template AutoDiff<DComplex> operator*(DComplex const &, AutoDiff<DComplex> const &);
template AutoDiff<DComplex> operator+(DComplex const &, AutoDiff<DComplex> const &);
template AutoDiff<DComplex> operator-(DComplex const &, AutoDiff<DComplex> const &);
template AutoDiff<DComplex> operator/(DComplex const &, AutoDiff<DComplex> const &);
template AutoDiff<DComplex> operator*(AutoDiff<DComplex> const &, DComplex const &);
template AutoDiff<DComplex> operator+(AutoDiff<DComplex> const &, DComplex const &);
template AutoDiff<DComplex> operator-(AutoDiff<DComplex> const &, DComplex const &);
template AutoDiff<DComplex> operator/(AutoDiff<DComplex> const &, DComplex const &);
} //# NAMESPACE - END
