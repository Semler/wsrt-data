// CountedPtr_1050.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Containers/OrderedMap.h>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<OrderedMap<Double, SquareMatrix<Complex, 2>*>*> >;
template class CountedPtr<Block<OrderedMap<Double, SquareMatrix<Complex, 2>*>*> >;
template class SimpleCountedConstPtr<Block<OrderedMap<Double, SquareMatrix<Complex, 2>*>*> >;
template class SimpleCountedPtr<Block<OrderedMap<Double, SquareMatrix<Complex, 2>*>*> >;
template class PtrRep<Block<OrderedMap<Double, SquareMatrix<Complex, 2>*>*> >;
} //# NAMESPACE - END
