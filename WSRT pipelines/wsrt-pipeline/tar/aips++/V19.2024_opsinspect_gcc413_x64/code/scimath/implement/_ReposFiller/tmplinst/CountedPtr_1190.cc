// CountedPtr_1190.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <scimath/Mathematics/SquareMatrix.h>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimpleCountedConstPtr<SquareMatrix<Complex, 4> >;
template class SimpleCountedPtr<SquareMatrix<Complex, 4> >;
template class CountedConstPtr<SquareMatrix<Complex, 4> >;
template class CountedPtr<SquareMatrix<Complex, 4> >;
template class PtrRep<SquareMatrix<Complex, 4> >;
template class SimpleCountedConstPtr<SquareMatrix<Complex, 2> >;
template class SimpleCountedPtr<SquareMatrix<Complex, 2> >;
template class CountedConstPtr<SquareMatrix<Complex, 2> >;
template class CountedPtr<SquareMatrix<Complex, 2> >;
template class PtrRep<SquareMatrix<Complex, 2> >;
template class SimpleCountedConstPtr<Block<CountedPtr<SquareMatrix<Complex, 4> > > >;
template class SimpleCountedPtr<Block<CountedPtr<SquareMatrix<Complex, 4> > > >;
template class CountedConstPtr<Block<CountedPtr<SquareMatrix<Complex, 4> > > >;
template class CountedPtr<Block<CountedPtr<SquareMatrix<Complex, 4> > > >;
template class PtrRep<Block<CountedPtr<SquareMatrix<Complex, 4> > > >;
template class SimpleCountedConstPtr<Block<CountedPtr<SquareMatrix<Complex, 2> > > >;
template class SimpleCountedPtr<Block<CountedPtr<SquareMatrix<Complex, 2> > > >;
template class CountedConstPtr<Block<CountedPtr<SquareMatrix<Complex, 2> > > >;
template class CountedPtr<Block<CountedPtr<SquareMatrix<Complex, 2> > > >;
template class PtrRep<Block<CountedPtr<SquareMatrix<Complex, 2> > > >;
} //# NAMESPACE - END
