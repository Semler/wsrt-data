// SparseDiffMath_1060.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool near(SparseDiff<Double> const &, SparseDiff<Double> const &, Double const);
template Bool near(SparseDiff<Double> const &, Double const &, Double const);
template Bool near(Double const &, SparseDiff<Double> const &, Double const);
template Bool allnear(SparseDiff<Double> const &, SparseDiff<Double> const &);
template Bool allnear(SparseDiff<Double> const &, Double const &);
template Bool allnear(Double const &, SparseDiff<Double> const &);
template Bool allnear(SparseDiff<Double> const &, SparseDiff<Double> const &, Double const);
template Bool allnear(SparseDiff<Double> const &, Double const &, Double const);
template Bool allnear(Double const &, SparseDiff<Double> const &, Double const);
template Bool nearAbs(SparseDiff<Double> const &, SparseDiff<Double> const &);
template Bool nearAbs(SparseDiff<Double> const &, Double const &);
template Bool nearAbs(Double const &, SparseDiff<Double> const &);
template Bool nearAbs(SparseDiff<Double> const &, SparseDiff<Double> const &, Double const);
template Bool nearAbs(SparseDiff<Double> const &, Double const &, Double const);
template Bool nearAbs(Double const &, SparseDiff<Double> const &, Double const);
template Bool allnearAbs(SparseDiff<Double> const &, SparseDiff<Double> const &);
template Bool allnearAbs(SparseDiff<Double> const &, Double const &);
template Bool allnearAbs(Double const &, SparseDiff<Double> const &);
template Bool allnearAbs(SparseDiff<Double> const &, SparseDiff<Double> const &, Double const);
template Bool allnearAbs(SparseDiff<Double> const &, Double const &, Double const);
template Bool allnearAbs(Double const &, SparseDiff<Double> const &, Double const);
} //# NAMESPACE - END
