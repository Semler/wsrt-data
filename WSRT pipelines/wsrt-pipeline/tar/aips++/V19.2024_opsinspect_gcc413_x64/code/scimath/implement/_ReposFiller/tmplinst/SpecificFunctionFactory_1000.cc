// SpecificFunctionFactory_1000.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Functionals/SpecificFunctionFactory.h>
#include <scimath/Functionals/MarshButterworthBandpass.h>
namespace casa { //# NAMESPACE - BEGIN
template class SpecificFunctionFactory<Double, MarshButterworthBandpass<Double> >;
} //# NAMESPACE - END
