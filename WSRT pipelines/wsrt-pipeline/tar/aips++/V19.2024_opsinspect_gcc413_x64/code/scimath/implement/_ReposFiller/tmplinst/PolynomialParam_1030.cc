// PolynomialParam_1030.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/PolynomialParam.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class PolynomialParam<AutoDiffA<Double> >;
template class PolynomialParam<AutoDiffA<Float> >;
} //# NAMESPACE - END
