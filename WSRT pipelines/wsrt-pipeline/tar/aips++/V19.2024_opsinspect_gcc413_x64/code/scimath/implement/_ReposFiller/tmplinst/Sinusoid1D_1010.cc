// Sinusoid1D_1010.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Functionals/Sinusoid1D.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Sinusoid1D<AutoDiffA<Double> >;
template class Sinusoid1D<AutoDiffA<Float> >;
} //# NAMESPACE - END
