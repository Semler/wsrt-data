// MatrixMathLA_1090.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/Arrays/Matrix.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template void CholeskyDecomp(Matrix<AutoDiff<Double> > &, Vector<AutoDiff<Double> > &);
template void CholeskySolve(Matrix<AutoDiff<Double> > &, Vector<AutoDiff<Double> > &, Vector<AutoDiff<Double> > &, Vector<AutoDiff<Double> > &);
} //# NAMESPACE - END
