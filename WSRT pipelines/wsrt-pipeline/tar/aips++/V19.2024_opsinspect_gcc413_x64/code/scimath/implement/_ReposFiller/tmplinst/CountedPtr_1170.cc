// CountedPtr_1170.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<AutoDiffA<Double> > >;
template class CountedConstPtr<Block<AutoDiffA<Double> > >;
template class SimpleCountedPtr<Block<AutoDiffA<Double> > >;
template class SimpleCountedConstPtr<Block<AutoDiffA<Double> > >;
template class PtrRep<Block<AutoDiffA<Double> > >;
} //# NAMESPACE - END
