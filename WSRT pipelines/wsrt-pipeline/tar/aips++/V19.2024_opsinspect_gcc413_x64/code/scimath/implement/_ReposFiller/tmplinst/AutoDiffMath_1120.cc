// AutoDiffMath_1120.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool operator!=(AutoDiff<Double> const &, AutoDiff<Double> const &);
template Bool operator<=(AutoDiff<Double> const &, AutoDiff<Double> const &);
template Bool operator<(AutoDiff<Double> const &, AutoDiff<Double> const &);
template Bool operator==(AutoDiff<Double> const &, AutoDiff<Double> const &);
template Bool operator>(AutoDiff<Double> const &, AutoDiff<Double> const &);
template Bool operator>=(AutoDiff<Double> const &, AutoDiff<Double> const &);
template Bool near(AutoDiff<Double> const &, AutoDiff<Double> const &);
template Bool operator!=(AutoDiff<Double> const &, Double const &);
template Bool operator<=(AutoDiff<Double> const &, Double const &);
template Bool operator<(AutoDiff<Double> const &, Double const &);
template Bool operator==(AutoDiff<Double> const &, Double const &);
template Bool operator>(AutoDiff<Double> const &, Double const &);
template Bool operator>=(AutoDiff<Double> const &, Double const &);
template Bool near(AutoDiff<Double> const &, Double const &);
template Bool operator!=(Double const &, AutoDiff<Double> const &);
template Bool operator<=(Double const &, AutoDiff<Double> const &);
template Bool operator<(Double const &, AutoDiff<Double> const &);
template Bool operator==(Double const &, AutoDiff<Double> const &);
template Bool operator>(Double const &, AutoDiff<Double> const &);
template Bool operator>=(Double const &, AutoDiff<Double> const &);
template Bool near(Double const &, AutoDiff<Double> const &);
} //# NAMESPACE - END
