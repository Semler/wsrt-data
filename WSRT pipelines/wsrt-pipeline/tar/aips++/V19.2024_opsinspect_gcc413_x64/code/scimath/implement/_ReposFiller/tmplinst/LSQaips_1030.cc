// LSQaips_1030.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LSQaips.cc>
#include <casa/Arrays/Array.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool LSQaips::getCovariance<std::complex<Float> >(Array<std::complex<Float> > &);
template Bool LSQaips::solveLoop<std::complex<Float> >(Double &, uInt &, Vector<std::complex<Float> > &, Bool doSVD);
template Bool LSQaips::solveLoop<std::complex<Float> >(uInt &, Vector<std::complex<Float> > &, Bool doSVD);
} //# NAMESPACE - END
