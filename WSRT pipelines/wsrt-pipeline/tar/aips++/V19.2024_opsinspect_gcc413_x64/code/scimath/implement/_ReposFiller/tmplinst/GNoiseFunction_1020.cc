// GNoiseFunction_1020.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/GNoiseFunction.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class GNoiseFunction<AutoDiff<Double> >;
template class GNoiseFunction<AutoDiff<Float> >;
} //# NAMESPACE - END
