// SparseDiffMath_1110.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template SparseDiff<Double> operator+(SparseDiff<Double> const &);
template SparseDiff<Double> operator-(SparseDiff<Double> const &);
template SparseDiff<Double> operator*(SparseDiff<Double> const &, SparseDiff<Double> const &);
template SparseDiff<Double> operator+(SparseDiff<Double> const &, SparseDiff<Double> const &);
template SparseDiff<Double> operator-(SparseDiff<Double> const &, SparseDiff<Double> const &);
template SparseDiff<Double> operator/(SparseDiff<Double> const &, SparseDiff<Double> const &);
template SparseDiff<Double> operator*(Double const &, SparseDiff<Double> const &);
template SparseDiff<Double> operator+(Double const &, SparseDiff<Double> const &);
template SparseDiff<Double> operator-(Double const &, SparseDiff<Double> const &);
template SparseDiff<Double> operator/(Double const &, SparseDiff<Double> const &);
template SparseDiff<Double> operator*(SparseDiff<Double> const &, Double const &);
template SparseDiff<Double> operator+(SparseDiff<Double> const &, Double const &);
template SparseDiff<Double> operator-(SparseDiff<Double> const &, Double const &);
template SparseDiff<Double> operator/(SparseDiff<Double> const &, Double const &);
} //# NAMESPACE - END
