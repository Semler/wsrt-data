// OddPolynomialParam_1010.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/OddPolynomialParam.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class OddPolynomialParam<DComplex>;
template class OddPolynomialParam<Complex>;
} //# NAMESPACE - END
