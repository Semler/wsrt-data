// CountedPtr_1120.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <scimath/Functionals/Function1D.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimpleCountedConstPtr<Function1D<Float> >;
template class SimpleCountedPtr<Function1D<Float> >;
template class CountedConstPtr<Function1D<Float> >;
template class CountedPtr<Function1D<Float> >;
template class PtrRep<Function1D<Float> >;
} //# NAMESPACE - END
