// UnaryFunction_1000.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Functionals/UnaryFunction.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class UnaryFunction<AutoDiff<DComplex> >;
template class UnaryFunction<AutoDiff<Complex> >;
} //# NAMESPACE - END
