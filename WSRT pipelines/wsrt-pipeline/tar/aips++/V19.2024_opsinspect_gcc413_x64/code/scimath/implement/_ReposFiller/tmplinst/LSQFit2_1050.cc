// LSQFit2_1050.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LSQFit2.cc>
#include <complex>
namespace casa { //# NAMESPACE - BEGIN
typedef std::complex<Double>* It3;
typedef uInt* It3Int;
template void LSQFit::makeNorm<Double, It3>(It3 const &, Double const &, std::complex<Double> const &, Bool, Bool);
template void LSQFit::makeNorm<Double, It3>(It3 const &, Double const &, std::complex<Double> const &, LSQFit::AsReal, Bool, Bool);
template void LSQFit::makeNorm<Double, It3>(It3 const &, Double const &, std::complex<Double> const &, LSQFit::Complex, Bool, Bool);
template void LSQFit::makeNorm<Double, It3>(It3 const &, Double const &, std::complex<Double> const &, LSQFit::Conjugate, Bool, Bool);
template void LSQFit::makeNorm<Double, It3>(It3 const &, Double const &, std::complex<Double> const &, LSQFit::Separable, Bool, Bool);
template void LSQFit::makeNorm<Double, It3, It3Int>(uInt, It3Int const &, It3 const &, Double const &, std::complex<Double> const &, Bool, Bool);
template void LSQFit::makeNorm<Double, It3, It3Int>(uInt, It3Int const &, It3 const &, Double const &, std::complex<Double> const &, LSQFit::AsReal, Bool, Bool);
template void LSQFit::makeNorm<Double, It3, It3Int>(uInt, It3Int const &, It3 const &, Double const &, std::complex<Double> const &, LSQFit::Complex, Bool, Bool);
template void LSQFit::makeNorm<Double, It3, It3Int>(uInt, It3Int const &, It3 const &, Double const &, std::complex<Double> const &, LSQFit::Conjugate, Bool, Bool);
template void LSQFit::makeNorm<Double, It3, It3Int>(uInt, It3Int const &, It3 const &, Double const &, std::complex<Double> const &, LSQFit::Separable, Bool, Bool);
template Bool LSQFit::addConstraint<Double, It3>(It3 const &, std::complex<Double> const &);
template Bool LSQFit::addConstraint<Double, It3, It3Int>(uInt, It3Int const &, It3 const &, std::complex<Double> const &);
template Bool LSQFit::setConstraint<Double, It3>(uInt, It3 const &, std::complex<Double> const &);
template Bool LSQFit::setConstraint<Double, It3, It3Int>(uInt, uInt, It3Int const &, It3 const &, std::complex<Double> const &);
template Bool LSQFit::getConstraint<Double>(uInt, std::complex<Double> *) const;
template void LSQFit::solve<Double>(std::complex<Double> *);
template Bool LSQFit::solveLoop<Double>(Double &, uInt &, std::complex<Double> *, Bool);
template Bool LSQFit::solveLoop<Double>(uInt &, std::complex<Double> *, Bool);
template void LSQFit::copy<std::complex<Double> >(Double const *, Double const *, std::complex<Double> *, LSQComplex);
template void LSQFit::uncopy<std::complex<Double> >(Double *, Double const *, std::complex<Double> *, LSQComplex);
template Bool LSQFit::getErrors<Double>(std::complex<Double> *);
template Bool LSQFit::getCovariance<Double>(std::complex<Double> *);
} //# NAMESPACE - END
