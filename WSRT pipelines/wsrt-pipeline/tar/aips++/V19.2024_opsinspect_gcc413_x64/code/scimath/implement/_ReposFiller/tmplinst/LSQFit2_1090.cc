// LSQFit2_1090.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LSQFit2.cc>
namespace casa { //# NAMESPACE - BEGIN
typedef Float* It7;
typedef uInt* It7Int;
template void LSQFit::makeNorm<Double, It7>(It7 const &, Double const &, Double const &, Bool, Bool);
template void LSQFit::makeNorm<Double, It7>(It7 const &, Double const &, Double const &, LSQFit::Real, Bool, Bool);
template void LSQFit::makeNorm<Double, It7, It7Int>(uInt, It7Int const &, It7 const &, Double const &, Double const &, Bool, Bool);
template void LSQFit::makeNorm<Double, It7, It7Int>(uInt, It7Int const &, It7 const &, Double const &, Double const &, LSQFit::Real, Bool, Bool);
template Bool LSQFit::addConstraint<Double, It7>(It7 const &, Double const &);
template Bool LSQFit::addConstraint<Double, It7, It7Int>(uInt, It7Int const &, It7 const &, Double const &);
template Bool LSQFit::setConstraint<Double, It7>(uInt, It7 const &, Double const &);
template Bool LSQFit::setConstraint<Double, It7, It7Int>(uInt, uInt, It7Int const &, It7 const &, Double const &);
} //# NAMESPACE - END
