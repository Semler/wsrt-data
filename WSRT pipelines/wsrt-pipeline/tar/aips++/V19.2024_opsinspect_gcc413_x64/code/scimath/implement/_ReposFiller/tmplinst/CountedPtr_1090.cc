// CountedPtr_1090.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <scimath/Mathematics/SquareMatrix.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<SquareMatrix<Complex, 4> > >;
template class CountedConstPtr<Block<SquareMatrix<Complex, 4> > >;
template class SimpleCountedPtr<Block<SquareMatrix<Complex, 4> > >;
template class SimpleCountedConstPtr<Block<SquareMatrix<Complex, 4> > >;
template class PtrRep<Block<SquareMatrix<Complex, 4> > >;
} //# NAMESPACE - END
