// FunctionParam_1020.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/FunctionParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
#include <scimath/Mathematics/AutoDiffIO.h>
namespace casa { //# NAMESPACE - BEGIN
template class FunctionParam<AutoDiff<Double> >;
template class FunctionParam<AutoDiff<Float> >;
} //# NAMESPACE - END
