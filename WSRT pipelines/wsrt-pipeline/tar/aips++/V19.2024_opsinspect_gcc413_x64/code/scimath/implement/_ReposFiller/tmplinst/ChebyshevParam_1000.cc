// ChebyshevParam_1000.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/ChebyshevParam.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class ChebyshevParam<AutoDiff<DComplex> >;
template class ChebyshevParam<AutoDiff<Complex> >;
template class ChebyshevParamModeImpl<AutoDiff<DComplex> >;
template class ChebyshevParamModeImpl<AutoDiff<Complex> >;
} //# NAMESPACE - END
