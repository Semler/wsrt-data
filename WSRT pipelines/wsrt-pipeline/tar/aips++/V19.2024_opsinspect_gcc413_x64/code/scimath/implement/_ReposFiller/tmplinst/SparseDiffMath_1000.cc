// SparseDiffMath_1000.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool operator!=(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template Bool operator<=(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template Bool operator<(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template Bool operator==(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template Bool operator>(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template Bool operator>=(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template Bool near(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template Bool operator!=(SparseDiff<Complex> const &, Complex const &);
template Bool operator<=(SparseDiff<Complex> const &, Complex const &);
template Bool operator<(SparseDiff<Complex> const &, Complex const &);
template Bool operator==(SparseDiff<Complex> const &, Complex const &);
template Bool operator>(SparseDiff<Complex> const &, Complex const &);
template Bool operator>=(SparseDiff<Complex> const &, Complex const &);
template Bool near(SparseDiff<Complex> const &, Complex const &);
template Bool operator!=(Complex const &, SparseDiff<Complex> const &);
template Bool operator<=(Complex const &, SparseDiff<Complex> const &);
template Bool operator<(Complex const &, SparseDiff<Complex> const &);
template Bool operator==(Complex const &, SparseDiff<Complex> const &);
template Bool operator>(Complex const &, SparseDiff<Complex> const &);
template Bool operator>=(Complex const &, SparseDiff<Complex> const &);
template Bool near(Complex const &, SparseDiff<Complex> const &);
} //# NAMESPACE - END
