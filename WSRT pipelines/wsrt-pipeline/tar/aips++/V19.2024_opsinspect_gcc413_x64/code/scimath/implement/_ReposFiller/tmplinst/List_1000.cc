// List_1000.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Containers/List.cc>
#include <casa/Containers/OrderedPair.h>
#include <scimath/Functionals/AbstractFunctionFactory.h>
namespace casa { //# NAMESPACE - BEGIN
template class List<OrderedPair<String, OrderedPair<FunctionFactory<Double>*, Bool> > >;
template class ListIter<OrderedPair<String, OrderedPair<FunctionFactory<Double>*, Bool> > >;
template class ListNotice<OrderedPair<String, OrderedPair<FunctionFactory<Double>*, Bool> > >;
template class ConstListIter<OrderedPair<String, OrderedPair<FunctionFactory<Double>*, Bool> > >;
} //# NAMESPACE - END
