// LSQFit2_1070.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LSQFit2.cc>
namespace casa { //# NAMESPACE - BEGIN
typedef Double* It5;
typedef uInt* It5Int;
template void LSQFit::makeNorm<Double, It5>(It5 const &, Double const &, Double const &, Bool, Bool);
template void LSQFit::makeNorm<Double, It5>(It5 const &, Double const &, Double const &, LSQFit::Real, Bool, Bool);
template void LSQFit::makeNorm<Double, It5, It5Int>(uInt, It5Int const &, It5 const &, Double const &, Double const &, Bool, Bool);
template void LSQFit::makeNorm<Double, It5, It5Int>(uInt, It5Int const &, It5 const &, Double const &, Double const &, LSQFit::Real, Bool, Bool);
template Bool LSQFit::addConstraint<Double, It5>(It5 const &, Double const &);
template Bool LSQFit::addConstraint<Double, It5, It5Int>(uInt, It5Int const &, It5 const &, Double const &);
template Bool LSQFit::setConstraint<Double, It5>(uInt, It5 const &, Double const &);
template Bool LSQFit::setConstraint<Double, It5, It5Int>(uInt, uInt, It5Int const &, It5 const &, Double const &);
template Bool LSQFit::getConstraint<Double>(uInt, Double *) const;
template void LSQFit::solve<Double>(Double *);
template Bool LSQFit::solveLoop<Double>(Double &, uInt &, Double *, Bool);
template Bool LSQFit::solveLoop<Double>(uInt &, Double *, Bool);
template void LSQFit::copy<Double>(Double const *, Double const *, Double *, LSQReal);
template void LSQFit::uncopy<Double>(Double *, Double const *, Double *, LSQReal);
template Bool LSQFit::getErrors<Double>(Double *);
template Bool LSQFit::getCovariance<Double>(Double *);
} //# NAMESPACE - END
