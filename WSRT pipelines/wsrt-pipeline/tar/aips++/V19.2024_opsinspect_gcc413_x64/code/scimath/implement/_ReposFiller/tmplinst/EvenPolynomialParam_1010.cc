// EvenPolynomialParam_1010.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/EvenPolynomialParam.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class EvenPolynomialParam<DComplex>;
template class EvenPolynomialParam<Complex>;
} //# NAMESPACE - END
