// MaskedArray_1090.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/MaskedArray.cc>
#include <casa/Utilities/CountedPtr.h>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class MaskedArray<CountedPtr<SquareMatrix<Complex, 4> > >;
template class MaskedArray<CountedPtr<SquareMatrix<Complex, 2> > >;
} //# NAMESPACE - END
