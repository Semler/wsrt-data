// CountedPtr_1180.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<AutoDiffA<Float> > >;
template class CountedConstPtr<Block<AutoDiffA<Float> > >;
template class SimpleCountedPtr<Block<AutoDiffA<Float> > >;
template class SimpleCountedConstPtr<Block<AutoDiffA<Float> > >;
template class PtrRep<Block<AutoDiffA<Float> > >;
} //# NAMESPACE - END
