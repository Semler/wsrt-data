// MatrixMathLA_1000.cc -- Tue Apr  1 11:57:47 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/Arrays/Matrix.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template void CholeskyDecomp(Matrix<Float> &, Vector<Float> &);
template void CholeskySolve(Matrix<Float> &, Vector<Float> &, Vector<Float> &, Vector<Float> &);
} //# NAMESPACE - END
