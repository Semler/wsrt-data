// SincParam_1020.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Functionals/SincParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class SincParam<AutoDiff<Double> >;
template class SincParam<AutoDiff<Float> >;
} //# NAMESPACE - END
