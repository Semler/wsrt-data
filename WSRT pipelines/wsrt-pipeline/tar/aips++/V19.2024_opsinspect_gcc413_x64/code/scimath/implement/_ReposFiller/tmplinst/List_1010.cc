// List_1010.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Containers/List.cc>
#include <casa/Containers/OrderedPair.h>
#include <scimath/Functionals/AbstractFunctionFactory.h>
namespace casa { //# NAMESPACE - BEGIN
template class List<OrderedPair<String, OrderedPair<FunctionFactory<Float>*, Bool> > >;
template class ListIter<OrderedPair<String, OrderedPair<FunctionFactory<Float>*, Bool> > >;
template class ListNotice<OrderedPair<String, OrderedPair<FunctionFactory<Float>*, Bool> > >;
template class ConstListIter<OrderedPair<String, OrderedPair<FunctionFactory<Float>*, Bool> > >;
} //# NAMESPACE - END
