// Interpolate2D2_1010.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/Interpolate2D2.cc>
namespace casa { //# NAMESPACE - BEGIN
template Bool Interpolate2D::interpCubic<Float>(Float &result, Vector<Double> const &where, Matrix<Float> const &data, Matrix<Bool> const* &maskPtr) const;
template Bool Interpolate2D::interpLinear<Float>(Float &result, Vector<Double> const &where, Matrix<Float> const &data, Matrix<Bool> const* &maskPtr) const;
template Bool Interpolate2D::interpNearest<Float>(Float &result, Vector<Double> const &where, Matrix<Float> const &data, Matrix<Bool> const* &maskPtr) const;
template Bool Interpolate2D::interpLinear2<Float>(Float &resultI, Float &resultJ, Vector<Double> const &where, Matrix<Float> const &dataI, Matrix<Float> const &dataJ, Matrix<Bool> const &mask) const;
} //# NAMESPACE - END
