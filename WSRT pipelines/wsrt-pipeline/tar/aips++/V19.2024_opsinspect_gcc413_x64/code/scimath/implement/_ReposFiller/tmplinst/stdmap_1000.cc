// stdmap_1000.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <casa/stdmap.h>
#include <casa/BasicSL/String.h>
#include <scimath/Functionals/FuncExprData.h>
template class std::map<casa::String, casa::FuncExprData::ExprOperator>;
AIPS_MAP_AUX_TEMPLATES(casa::String, casa::FuncExprData::ExprOperator)
