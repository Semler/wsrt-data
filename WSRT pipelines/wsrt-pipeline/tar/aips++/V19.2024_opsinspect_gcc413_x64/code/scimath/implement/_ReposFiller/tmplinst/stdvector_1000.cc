// stdvector_1000.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <casa/stdvector.h>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
#include <scimath/Mathematics/AutoDiffIO.h>
template class std::vector<casa::AutoDiff<casa::Complex> >;
AIPS_VECTOR_AUX_TEMPLATES(casa::AutoDiff<casa::Complex>)
