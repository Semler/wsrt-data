// Polynomial2_1010.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/Polynomial2.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Polynomial<AutoDiff<Double> >;
template class Polynomial<AutoDiff<Float> >;
} //# NAMESPACE - END
