// Array_1110.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <casa/Utilities/CountedPtr.h>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<CountedPtr<SquareMatrix<Complex, 4> > >;
template class Array<CountedPtr<SquareMatrix<Complex, 2> > >;
} //# NAMESPACE - END
