// Gaussian1DParam_1000.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Gaussian1DParam.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Gaussian1DParam<AutoDiff<DComplex> >;
template class Gaussian1DParam<AutoDiff<Complex> >;
} //# NAMESPACE - END
