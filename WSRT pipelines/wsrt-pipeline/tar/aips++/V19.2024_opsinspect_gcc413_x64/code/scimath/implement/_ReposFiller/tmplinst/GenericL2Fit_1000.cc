// GenericL2Fit_1000.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/GenericL2Fit.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class GenericL2Fit<AutoDiff<Complex> >;
template class GenericL2Fit<AutoDiffA<Complex> >;
} //# NAMESPACE - END
