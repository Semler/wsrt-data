// Gaussian1DParam_1020.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Gaussian1DParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Gaussian1DParam<AutoDiff<Double> >;
template class Gaussian1DParam<AutoDiff<Float> >;
} //# NAMESPACE - END
