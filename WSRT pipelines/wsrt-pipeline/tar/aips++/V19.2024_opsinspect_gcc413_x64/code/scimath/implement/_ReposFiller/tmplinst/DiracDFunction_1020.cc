// DiracDFunction_1020.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/DiracDFunction.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class DiracDFunction<AutoDiff<Double> >;
template class DiracDFunction<AutoDiff<Float> >;
} //# NAMESPACE - END
