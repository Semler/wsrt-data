// ScalarSampledFunctional_1000.cc -- Tue Apr  1 11:57:47 BST 2008 -- renting
#include <scimath/Functionals/ScalarSampledFunctional.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class ScalarSampledFunctional<Int>;
template class ScalarSampledFunctional<DComplex>;
} //# NAMESPACE - END
