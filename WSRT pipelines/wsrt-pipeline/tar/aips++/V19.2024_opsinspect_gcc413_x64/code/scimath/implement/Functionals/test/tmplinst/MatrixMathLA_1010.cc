// MatrixMathLA_1010.cc -- Tue Apr  1 11:57:47 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/Arrays/Matrix.h>
namespace casa { //# NAMESPACE - BEGIN
template Matrix<Float> invertSymPosDef(Matrix<Float> const &);
template void invertSymPosDef(Matrix<Float> &, Float &, Matrix<Float> const &);
} //# NAMESPACE - END
