// Gaussian2D2_1010.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Gaussian2D2.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Gaussian2D<AutoDiff<Double> >;
template class Gaussian2D<AutoDiff<Float> >;
} //# NAMESPACE - END
