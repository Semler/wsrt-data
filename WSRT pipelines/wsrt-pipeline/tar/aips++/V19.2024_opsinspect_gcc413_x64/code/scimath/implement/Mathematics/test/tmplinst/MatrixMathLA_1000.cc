// MatrixMathLA_1000.cc -- Tue Apr  1 11:58:13 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Matrix<DComplex> invert(Matrix<DComplex> const &);
template void invert(Matrix<DComplex> &, DComplex &, Matrix<DComplex> const &);
} //# NAMESPACE - END
