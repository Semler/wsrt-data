// HyperPlane_1010.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/HyperPlane.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class HyperPlane<AutoDiffA<Double> >;
template class HyperPlane<AutoDiffA<Float> >;
#include <casa/BasicSL/Complex.h>
template class HyperPlane<AutoDiffA<DComplex> >;
template class HyperPlane<AutoDiffA<Complex> >;
} //# NAMESPACE - END
