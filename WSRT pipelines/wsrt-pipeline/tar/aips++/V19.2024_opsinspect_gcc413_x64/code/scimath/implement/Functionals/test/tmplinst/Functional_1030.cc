// Functional_1030.cc -- Tue Apr  1 11:57:46 BST 2008 -- renting
#include <casa/BasicMath/Functional.cc>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class Functional<Vector<Float>, Double>;
template class Functional<Vector<Int>, Double>;
#include <casa/BasicSL/Complex.h>
template class Functional<Vector<Double>, DComplex>;
#include <casa/Arrays/Array.h>
template class Functional<Vector<Float>, Array<Float> >;
} //# NAMESPACE - END
