// Link_1010.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Containers/Link.cc>
#include <casa/Containers/OrderedPair.h>
#include <casa/BasicSL/String.h>
#include <scimath/Functionals/AbstractFunctionFactory.h>
namespace casa { //# NAMESPACE - BEGIN
template class Link<OrderedPair<String, OrderedPair<FunctionFactory<Float>*, Bool> > >;
} //# NAMESPACE - END
