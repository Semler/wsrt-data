// AutoDiffMath_1000.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template AutoDiff<Complex> acos(AutoDiff<Complex> const &);
template AutoDiff<Complex> asin(AutoDiff<Complex> const &);
template AutoDiff<Complex> atan(AutoDiff<Complex> const &);
template AutoDiff<Complex> atan2(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template AutoDiff<Complex> cos(AutoDiff<Complex> const &);
template AutoDiff<Complex> cosh(AutoDiff<Complex> const &);
template AutoDiff<Complex> exp(AutoDiff<Complex> const &);
template AutoDiff<Complex> log(AutoDiff<Complex> const &);
template AutoDiff<Complex> log10(AutoDiff<Complex> const &);
template AutoDiff<Complex> erf(AutoDiff<Complex> const &);
template AutoDiff<Complex> erfc(AutoDiff<Complex> const &);
template AutoDiff<Complex> pow(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template AutoDiff<Complex> pow(AutoDiff<Complex> const &, Complex const &);
template AutoDiff<Complex> sin(AutoDiff<Complex> const &);
template AutoDiff<Complex> sinh(AutoDiff<Complex> const &);
template AutoDiff<Complex> sqrt(AutoDiff<Complex> const &);
template AutoDiff<Complex> abs(AutoDiff<Complex> const &);
template AutoDiff<Complex> ceil(AutoDiff<Complex> const &);
template AutoDiff<Complex> floor(AutoDiff<Complex> const &);
template AutoDiff<Complex> fmod(AutoDiff<Complex> const &, Complex const &);
template AutoDiff<Complex> fmod(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template AutoDiff<Complex> max(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
template AutoDiff<Complex> min(AutoDiff<Complex> const &, AutoDiff<Complex> const &);
} //# NAMESPACE - END
