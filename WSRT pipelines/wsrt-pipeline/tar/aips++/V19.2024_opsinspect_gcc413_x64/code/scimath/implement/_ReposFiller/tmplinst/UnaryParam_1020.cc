// UnaryParam_1020.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Functionals/UnaryParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class UnaryParam<AutoDiff<Double> >;
template class UnaryParam<AutoDiff<Float> >;
} //# NAMESPACE - END
