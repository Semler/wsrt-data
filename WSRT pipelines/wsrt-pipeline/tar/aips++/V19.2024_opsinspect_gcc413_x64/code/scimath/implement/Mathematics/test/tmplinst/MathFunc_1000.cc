// MathFunc_1000.cc -- Tue Apr  1 11:58:13 BST 2008 -- renting
#include <scimath/Mathematics/MathFunc.cc>
namespace casa { //# NAMESPACE - BEGIN
template class ExpSincConv<Double>;
template class GaussianConv<Double>;
template class KB_Conv<Double>;
template class MathFunc<Double>;
template class Mod_KB_Conv<Double>;
template class Sinc_Conv<Double>;
template class Sph_Conv<Double>;
template class Unary<Double>;
} //# NAMESPACE - END
