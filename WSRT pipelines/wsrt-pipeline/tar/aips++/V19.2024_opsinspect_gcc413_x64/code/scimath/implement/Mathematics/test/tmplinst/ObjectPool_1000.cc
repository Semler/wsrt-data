// ObjectPool_1000.cc -- Tue Apr  1 11:58:13 BST 2008 -- renting
#include <casa/Containers/ObjectPool.cc>
#include <scimath/Mathematics/AutoDiffRep.h>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template class ObjectPool<AutoDiffRep<AutoDiff<Double> >, uInt>;
} //# NAMESPACE - END
