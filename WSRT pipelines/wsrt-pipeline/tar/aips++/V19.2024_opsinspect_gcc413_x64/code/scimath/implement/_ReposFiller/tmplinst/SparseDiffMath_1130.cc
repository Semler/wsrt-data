// SparseDiffMath_1130.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template SparseDiff<Float> operator+(SparseDiff<Float> const &);
template SparseDiff<Float> operator-(SparseDiff<Float> const &);
template SparseDiff<Float> operator*(SparseDiff<Float> const &, SparseDiff<Float> const &);
template SparseDiff<Float> operator+(SparseDiff<Float> const &, SparseDiff<Float> const &);
template SparseDiff<Float> operator-(SparseDiff<Float> const &, SparseDiff<Float> const &);
template SparseDiff<Float> operator/(SparseDiff<Float> const &, SparseDiff<Float> const &);
template SparseDiff<Float> operator*(Float const &, SparseDiff<Float> const &);
template SparseDiff<Float> operator+(Float const &, SparseDiff<Float> const &);
template SparseDiff<Float> operator-(Float const &, SparseDiff<Float> const &);
template SparseDiff<Float> operator/(Float const &, SparseDiff<Float> const &);
template SparseDiff<Float> operator*(SparseDiff<Float> const &, Float const &);
template SparseDiff<Float> operator+(SparseDiff<Float> const &, Float const &);
template SparseDiff<Float> operator-(SparseDiff<Float> const &, Float const &);
template SparseDiff<Float> operator/(SparseDiff<Float> const &, Float const &);
} //# NAMESPACE - END
