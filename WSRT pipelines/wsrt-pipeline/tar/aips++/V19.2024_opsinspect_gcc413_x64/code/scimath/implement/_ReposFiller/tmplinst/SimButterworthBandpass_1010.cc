// SimButterworthBandpass_1010.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/SimButterworthBandpass.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimButterworthBandpass<AutoDiff<Double> >;
template class SimButterworthBandpass<AutoDiff<Float> >;
} //# NAMESPACE - END
