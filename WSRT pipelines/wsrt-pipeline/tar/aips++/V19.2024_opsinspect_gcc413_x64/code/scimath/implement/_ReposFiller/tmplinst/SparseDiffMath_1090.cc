// SparseDiffMath_1090.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool operator!=(SparseDiff<Float> const &, SparseDiff<Float> const &);
template Bool operator<=(SparseDiff<Float> const &, SparseDiff<Float> const &);
template Bool operator<(SparseDiff<Float> const &, SparseDiff<Float> const &);
template Bool operator==(SparseDiff<Float> const &, SparseDiff<Float> const &);
template Bool operator>(SparseDiff<Float> const &, SparseDiff<Float> const &);
template Bool operator>=(SparseDiff<Float> const &, SparseDiff<Float> const &);
template Bool near(SparseDiff<Float> const &, SparseDiff<Float> const &);
template Bool operator!=(SparseDiff<Float> const &, Float const &);
template Bool operator<=(SparseDiff<Float> const &, Float const &);
template Bool operator<(SparseDiff<Float> const &, Float const &);
template Bool operator==(SparseDiff<Float> const &, Float const &);
template Bool operator>(SparseDiff<Float> const &, Float const &);
template Bool operator>=(SparseDiff<Float> const &, Float const &);
template Bool near(SparseDiff<Float> const &, Float const &);
template Bool operator!=(Float const &, SparseDiff<Float> const &);
template Bool operator<=(Float const &, SparseDiff<Float> const &);
template Bool operator<(Float const &, SparseDiff<Float> const &);
template Bool operator==(Float const &, SparseDiff<Float> const &);
template Bool operator>(Float const &, SparseDiff<Float> const &);
template Bool operator>=(Float const &, SparseDiff<Float> const &);
template Bool near(Float const &, SparseDiff<Float> const &);
} //# NAMESPACE - END
