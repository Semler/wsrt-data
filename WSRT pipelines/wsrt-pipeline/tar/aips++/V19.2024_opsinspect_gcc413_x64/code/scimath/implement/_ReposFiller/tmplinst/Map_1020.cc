// Map_1020.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Containers/Map.cc>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class Map<Double, SquareMatrix<Complex, 2>*>;
template class ConstMapIter<Double, SquareMatrix<Complex, 2>*>;
template class MapIter<Double, SquareMatrix<Complex, 2>*>;
template class MapIterRep<Double, SquareMatrix<Complex, 2>*>;
template class MapRep<Double, SquareMatrix<Complex, 2>*>;
} //# NAMESPACE - END
