// HyperPlaneParam_1030.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/HyperPlaneParam.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class HyperPlaneParam<AutoDiffA<Double> >;
template class HyperPlaneParam<AutoDiffA<Float> >;
#include <casa/BasicSL/Complex.h>
template class HyperPlaneParam<AutoDiffA<Complex> >;
template class HyperPlaneParam<AutoDiffA<DComplex> >;
} //# NAMESPACE - END
