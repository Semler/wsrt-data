// AutoDiffMath_1070.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template AutoDiff<Double> operator+(AutoDiff<Double> const &);
template AutoDiff<Double> operator-(AutoDiff<Double> const &);
template AutoDiff<Double> operator*(AutoDiff<Double> const &, AutoDiff<Double> const &);
template AutoDiff<Double> operator+(AutoDiff<Double> const &, AutoDiff<Double> const &);
template AutoDiff<Double> operator-(AutoDiff<Double> const &, AutoDiff<Double> const &);
template AutoDiff<Double> operator/(AutoDiff<Double> const &, AutoDiff<Double> const &);
template AutoDiff<Double> operator*(Double const &, AutoDiff<Double> const &);
template AutoDiff<Double> operator+(Double const &, AutoDiff<Double> const &);
template AutoDiff<Double> operator-(Double const &, AutoDiff<Double> const &);
template AutoDiff<Double> operator/(Double const &, AutoDiff<Double> const &);
template AutoDiff<Double> operator*(AutoDiff<Double> const &, Double const &);
template AutoDiff<Double> operator+(AutoDiff<Double> const &, Double const &);
template AutoDiff<Double> operator-(AutoDiff<Double> const &, Double const &);
template AutoDiff<Double> operator/(AutoDiff<Double> const &, Double const &);
} //# NAMESPACE - END
