// Gaussian3DParam_1020.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/Gaussian3DParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Gaussian3DParam<AutoDiff<Double> >;
} //# NAMESPACE - END
