// CountedPtr_1030.cc -- Tue Apr  1 12:17:53 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <components/ComponentModels/SkyCompRep.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<SkyCompRep>;
template class CountedConstPtr<SkyCompRep>;
template class SimpleCountedPtr<SkyCompRep>;
template class SimpleCountedConstPtr<SkyCompRep>;
template class PtrRep<SkyCompRep>;
} //# NAMESPACE - END
