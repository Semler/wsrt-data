// CountedPtr_1020.cc -- Tue Apr  1 12:17:53 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <components/ComponentModels/ComponentShape.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<ComponentShape>;
template class CountedConstPtr<ComponentShape>;
template class SimpleCountedPtr<ComponentShape>;
template class SimpleCountedConstPtr<ComponentShape>;
template class PtrRep<ComponentShape>;
} //# NAMESPACE - END
