// ParameterAccessor_1100.cc -- Tue Apr  1 12:28:14 BST 2008 -- renting
#include <tasking/Tasking/ParameterAccessor.cc>
#include <casa/Quanta/QuantumHolder.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class ParameterAccessor<QuantumHolder>;
template class ParameterAccessor<Quantum<Double> >;
template class ParameterAccessor<Quantum<Vector<Double> > >;
template class ParameterAccessor<Quantum<Array<Double> > >;
} //# NAMESPACE - END
