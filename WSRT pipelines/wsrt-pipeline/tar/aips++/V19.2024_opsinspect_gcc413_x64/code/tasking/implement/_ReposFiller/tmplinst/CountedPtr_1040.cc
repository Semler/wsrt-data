// CountedPtr_1040.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <tasking/Glish/GlishEvent.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<GlishSysEventSource>;
template class CountedPtr<GlishSysEventSource>;
template class PtrRep<GlishSysEventSource>;
template class SimpleCountedConstPtr<GlishSysEventSource>;
template class SimpleCountedPtr<GlishSysEventSource>;
} //# NAMESPACE - END
