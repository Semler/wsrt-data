// Parameter_1160.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/Parameter.cc>
#include <measures/Measures/MeasureHolder.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class Parameter<Vector<MeasureHolder> >;
template class Parameter<Array<MeasureHolder> >;
} //# NAMESPACE - END
