// ParameterConstraint_1190.cc -- Tue Apr  1 12:28:14 BST 2008 -- renting
#include <tasking/Tasking/ParameterConstraint.cc>
#include <casa/Quanta/QuantumHolder.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class ParameterConstraint<Vector<QuantumHolder> >;
template class ParameterConstraint<Array<QuantumHolder> >;
template class ParameterConstraint<Vector<Quantum<Double> > >;
template class ParameterConstraint<Array<Quantum<Double> > >;
} //# NAMESPACE - END
