// Parameter_1110.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/Parameter.cc>
#include <casa/Quanta/QuantumHolder.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class Parameter<Vector<QuantumHolder> >;
template class Parameter<Array<QuantumHolder> >;
template class Parameter<Vector<Quantum<Double> > >;
template class Parameter<Array<Quantum<Double> > >;
} //# NAMESPACE - END
