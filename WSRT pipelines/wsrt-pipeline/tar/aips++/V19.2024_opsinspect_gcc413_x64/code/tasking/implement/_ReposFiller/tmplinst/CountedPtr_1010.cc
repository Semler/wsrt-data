// CountedPtr_1010.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <graphics/Graphics/X11Intrinsic.h>
#include <tasking/Glish/XSysEvent.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<_XEvent>;
template class CountedPtr<_XEvent>;
template class PtrRep<_XEvent>;
template class SimpleCountedPtr<_XEvent>;
template class SimpleCountedConstPtr<_XEvent>;
} //# NAMESPACE - END
