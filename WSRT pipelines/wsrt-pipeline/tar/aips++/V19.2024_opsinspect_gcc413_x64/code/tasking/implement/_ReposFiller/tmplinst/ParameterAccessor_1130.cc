// ParameterAccessor_1130.cc -- Tue Apr  1 12:28:14 BST 2008 -- renting
#include <tasking/Tasking/ParameterAccessor.cc>
#include <components/ComponentModels/SkyComponent.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class ParameterAccessor<Vector<SkyComponent> >;
template class ParameterAccessor<Array<SkyComponent> >;
} //# NAMESPACE - END
