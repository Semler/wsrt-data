// FITSFieldCopier_1010.cc -- Tue Apr  1 12:14:23 BST 2008 -- renting
#include <fits/FITS/FITSFieldCopier.h>
namespace casa { //# NAMESPACE - BEGIN
template class ScalarFITSFieldCopier<Bool, FitsLogical>;
template class ScalarFITSFieldCopier<Complex, Complex>;
template class ScalarFITSFieldCopier<DComplex, DComplex>;
template class ScalarFITSFieldCopier<Double, Double>;
template class ScalarFITSFieldCopier<Float, Float>;
template class ScalarFITSFieldCopier<Int, FitsLong>;
template class ScalarFITSFieldCopier<Short, Short>;
template class ScalarFITSFieldCopier<uChar, uChar>;
} //# NAMESPACE - END
