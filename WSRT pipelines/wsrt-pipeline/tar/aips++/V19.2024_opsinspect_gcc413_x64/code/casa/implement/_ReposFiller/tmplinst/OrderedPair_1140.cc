// OrderedPair_1140.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/OrderedPair.cc>
#include <casa/Containers/PoolStack.h>
namespace casa { //# NAMESPACE - BEGIN
template <class T> class AutoDiffRep;
template class OrderedPair<uInt, PoolStack<AutoDiffRep<Float>, uInt> *>;
} //# NAMESPACE - END
