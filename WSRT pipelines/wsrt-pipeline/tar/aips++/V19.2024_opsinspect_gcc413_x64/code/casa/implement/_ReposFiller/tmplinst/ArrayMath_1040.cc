// ArrayMath_1040.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template DComplex median(Array<DComplex> const &, Bool);
template DComplex median(Array<DComplex> const &, Bool, Bool, Bool);
} //# NAMESPACE - END
