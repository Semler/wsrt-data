// ArrayMath_1100.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<Int> abs(Array<Int> const &);
template Array<Int> max(Array<Int> const &, Array<Int> const &);
template Array<Int> min(Array<Int> const &, Array<Int> const &);
template Array<Int> operator*(Array<Int> const &, Array<Int> const &);
template Array<Int> operator*(Array<Int> const &, Int const &);
template Array<Int> operator*(Int const &, Array<Int> const &);
template Array<Int> operator+(Array<Int> const &, Int const &);
template Array<Int> operator+(Array<Int> const &, Array<Int> const &);
template Array<Int> operator-(Array<Int> const &);
template Array<Int> operator/(Array<Int> const &, Int const &);
template Int max(Array<Int> const &);
template Int min(Array<Int> const &);
template Int product(Array<Int> const &);
template Int sum(Array<Int> const &);
template void indgen(Array<Int> &);
template void indgen(Array<Int> &, Int);
template void indgen(Array<Int> &, Int, Int);
template void max(Array<Int> &, Array<Int> const &, Array<Int> const &);
template void min(Array<Int> &, Array<Int> const &, Array<Int> const &);
template void minMax(Int &, Int &, Array<Int> const &);
template void operator*=(Array<Int> &, Array<Int> const &);
template void operator*=(Array<Int> &, Int const &);
template void operator+=(Array<Int> &, Array<Int> const &);
template void operator+=(Array<Int> &, Int const &);
template void operator-=(Array<Int> &, Array<Int> const &);
template void operator-=(Array<Int> &, Int const &);
template void operator/=(Array<Int> &, Array<Int> const &);
template void operator/=(Array<Int> &, Int const &);
} //# NAMESPACE - END
