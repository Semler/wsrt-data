// LinearSearch_1000.cc -- Tue Apr  1 11:57:26 BST 2008 -- renting
#include <casa/Utilities/LinearSearch.cc>
#include <casa/Arrays/IPosition.h>
namespace casa { //# NAMESPACE - BEGIN
template Int linearSearch(Bool &, IPosition const &, Int const &, uInt, uInt);
template Int linearSearch1(IPosition const &, Int const &, uInt);
template Int linearSearchBrackets(Bool &, Int * const &, Int const &, uInt, uInt);
} //# NAMESPACE - END
