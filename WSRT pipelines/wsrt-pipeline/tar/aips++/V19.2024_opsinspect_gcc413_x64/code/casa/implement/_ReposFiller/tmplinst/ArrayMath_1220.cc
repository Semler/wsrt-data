// ArrayMath_1220.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
namespace casa { //# NAMESPACE - BEGIN
template Array<Bool> slidingArrayMath(Array<Bool> const &, IPosition const &, Bool(*)(Array<Bool> const &), Bool);
template Array<Double> slidingArrayMath(Array<Double> const &, IPosition const &, Double(*)(Array<Double> const &), Bool);
} //# NAMESPACE - END
