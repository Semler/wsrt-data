// CountedPtr_1470.cc -- Tue Apr  1 11:50:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/OS/SysEvent.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<SysEventTarget>;
template class CountedPtr<SysEventTarget>;
template class PtrRep<SysEventTarget>;
template class SimpleCountedConstPtr<SysEventTarget>;
template class SimpleCountedPtr<SysEventTarget>;
} //# NAMESPACE - END
