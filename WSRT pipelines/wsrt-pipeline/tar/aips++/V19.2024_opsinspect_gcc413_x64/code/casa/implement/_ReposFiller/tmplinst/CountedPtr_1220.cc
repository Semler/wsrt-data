// CountedPtr_1220.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/BasicSL/String.h>
#include <casa/Utilities/Regex.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimpleCountedConstPtr<Regex>;
template class CountedConstPtr<Regex>;
template class CountedPtr<Regex>;
template class PtrRep<Regex>;
template class SimpleCountedPtr<Regex>;
} //# NAMESPACE - END
