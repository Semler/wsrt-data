// ArrayMath_1190.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template void operator*=(Array<Bool> &, Array<Bool> const &);
template Bool sum(Array<Bool> const &);
} //# NAMESPACE - END
