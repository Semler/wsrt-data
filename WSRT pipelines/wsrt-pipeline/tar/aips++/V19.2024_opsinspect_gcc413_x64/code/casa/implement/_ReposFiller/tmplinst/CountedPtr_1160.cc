// CountedPtr_1160.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Arrays/Matrix.h>
#include <casa/BasicMath/Random.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Matrix<CountedPtr<Random> > > >;
template class CountedPtr<Block<Matrix<CountedPtr<Random> > > >;
template class SimpleCountedConstPtr<Block<Matrix<CountedPtr<Random> > > >;
template class SimpleCountedPtr<Block<Matrix<CountedPtr<Random> > > >;
template class PtrRep<Block<Matrix<CountedPtr<Random> > > >;
} //# NAMESPACE - END
