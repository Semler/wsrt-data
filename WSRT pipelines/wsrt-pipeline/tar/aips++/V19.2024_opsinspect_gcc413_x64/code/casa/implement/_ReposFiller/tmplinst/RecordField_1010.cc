// RecordField_1010.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/RecordField.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class RecordFieldPtr<Array<Bool> >;
template class RecordFieldPtr<Array<Char> >;
template class RecordFieldPtr<Array<Complex> >;
template class RecordFieldPtr<Array<DComplex> >;
template class RecordFieldPtr<Array<Double> >;
template class RecordFieldPtr<Array<Float> >;
template class RecordFieldPtr<Array<Int> >;
template class RecordFieldPtr<Array<Short> >;
template class RecordFieldPtr<Array<String> >;
template class RecordFieldPtr<Array<uChar> >;
template class RecordFieldPtr<Array<uInt> >;
} //# NAMESPACE - END
