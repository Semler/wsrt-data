// CountedPtr_1210.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<DComplex> >;
template class CountedPtr<Block<DComplex> >;
template class PtrRep<Block<DComplex> >;
template class SimpleCountedConstPtr<Block<DComplex> >;
template class SimpleCountedPtr<Block<DComplex> >;
} //# NAMESPACE - END
