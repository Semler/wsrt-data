// CountedPtr_1150.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Array<Double> > >;
template class CountedPtr<Block<Array<Double> > >;
template class PtrRep<Block<Array<Double> > >;
template class SimpleCountedConstPtr<Block<Array<Double> > >;
template class SimpleCountedPtr<Block<Array<Double> > >;
} //# NAMESPACE - END
