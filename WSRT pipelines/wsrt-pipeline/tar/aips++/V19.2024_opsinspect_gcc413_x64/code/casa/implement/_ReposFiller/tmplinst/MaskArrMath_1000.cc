// MaskArrMath_1000.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/MaskArrMath.cc>
#include <casa/Arrays/MaskedArray.h>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<Complex> & operator/=(Array<Complex> &, MaskedArray<Complex> const &);
} //# NAMESPACE - END
