// MaskArrMath_1120.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/MaskArrMath.cc>
namespace casa { //# NAMESPACE - BEGIN
template void minMax(Float &, Float &, IPosition &, IPosition &, MaskedArray<Float> const &);
template void minMax(Float &, Float &, MaskedArray<Float> const &);
} //# NAMESPACE - END
