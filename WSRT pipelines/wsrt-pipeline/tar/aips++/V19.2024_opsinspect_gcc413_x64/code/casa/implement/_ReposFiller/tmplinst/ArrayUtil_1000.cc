// ArrayUtil_1000.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/ArrayUtil.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<Bool> concatenateArray(Array<Bool> const &, Array<Bool> const &);
template Array<Int> concatenateArray(Array<Int> const &, Array<Int> const &);
template Array<uInt> concatenateArray(Array<uInt> const &, Array<uInt> const &);
template Array<Float> concatenateArray(Array<Float> const &, Array<Float> const &);
template Array<Double> concatenateArray(Array<Double> const &, Array<Double> const &);
template Array<Complex> concatenateArray(Array<Complex> const &, Array<Complex> const &);
template Array<DComplex> concatenateArray(Array<DComplex> const &, Array<DComplex> const &);
template Array<String> concatenateArray(Array<String> const &, Array<String> const &);
} //# NAMESPACE - END
