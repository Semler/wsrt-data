// GenSort_1070.cc -- Tue Apr  1 11:50:47 BST 2008 -- renting
#include <casa/Utilities/GenSort.cc>
namespace casa { //# NAMESPACE - BEGIN
template class GenSortIndirect<Double>;
template uInt genSort(Vector<uInt> &, Block<Double> const &);
template uInt genSort(Vector<uInt> &, Block<Double> const &, Sort::Order);
} //# NAMESPACE - END
