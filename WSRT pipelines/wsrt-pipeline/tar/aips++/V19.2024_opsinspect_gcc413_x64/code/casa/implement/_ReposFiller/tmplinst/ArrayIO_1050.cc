// ArrayIO_1050.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayIO.cc>
#include <casa/Quanta/Quantum.h>
namespace casa { //# NAMESPACE - BEGIN
template ostream & operator<<(ostream &, Array<Quantum<Double> > const &);
} //# NAMESPACE - END
