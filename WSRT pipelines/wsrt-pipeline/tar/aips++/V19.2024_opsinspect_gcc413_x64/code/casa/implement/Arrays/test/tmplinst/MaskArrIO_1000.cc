// MaskArrIO_1000.cc -- Tue Apr  1 11:49:18 BST 2008 -- renting
#include <casa/Arrays/MaskArrIO.cc>
namespace casa { //# NAMESPACE - BEGIN
template ostream & operator<<(ostream &, MaskedArray<Bool> const &);
template ostream & operator<<(ostream &, MaskedArray<Int> const &);
} //# NAMESPACE - END
