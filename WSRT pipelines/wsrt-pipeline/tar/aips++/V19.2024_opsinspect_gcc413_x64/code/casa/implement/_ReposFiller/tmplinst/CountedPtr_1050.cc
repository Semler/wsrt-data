// CountedPtr_1050.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<uLong> >;
template class CountedPtr<Block<uLong> >;
template class PtrRep<Block<uLong> >;
template class SimpleCountedConstPtr<Block<uLong> >;
template class SimpleCountedPtr<Block<uLong> >;
} //# NAMESPACE - END
