// CountedPtr_1060.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<void *> >;
template class CountedPtr<Block<void *> >;
template class PtrRep<Block<void *> >;
template class SimpleCountedConstPtr<Block<void *> >;
template class SimpleCountedPtr<Block<void *> >;
} //# NAMESPACE - END
