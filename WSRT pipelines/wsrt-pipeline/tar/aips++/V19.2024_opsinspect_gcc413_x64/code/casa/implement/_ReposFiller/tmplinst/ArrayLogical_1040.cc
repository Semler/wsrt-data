// ArrayLogical_1040.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allEQ(Array<Double> const &, Array<Double> const &);
template Bool allEQ(Array<Double> const &, Double const &);
template Bool allGE(Array<Double> const &, Double const &);
template Bool allLE(Array<Double> const &, Double const &);
template Bool allNear(Array<Double> const &, Array<Double> const &, Double);
template Bool allNear(Array<Double> const &, Double const &, Double);
template Bool allNearAbs(Array<Double> const &, Array<Double> const &, Double);
template Bool anyEQ(Double const &, Array<Double> const &);
template Bool anyGT(Array<Double> const &, Double const &);
template Bool anyLT(Array<Double> const &, Double const &);
template Bool anyNE(Array<Double> const &, Array<Double> const &);
template Bool anyNE(Array<Double> const &, Double const &);
template LogicalArray operator!=(Array<Double> const &, Array<Double> const &);
template LogicalArray operator!=(Array<Double> const &, Double const &);
template LogicalArray operator!=(Double const &, Array<Double> const &);
template LogicalArray operator==(Array<Double> const &, Array<Double> const &);
template LogicalArray operator==(Array<Double> const &, Double const &);
template LogicalArray operator==(Double const &, Array<Double> const &);
template LogicalArray operator>(Array<Double> const &, Array<Double> const &);
template LogicalArray operator>(Array<Double> const &, Double const &);
template LogicalArray operator>(Double const &, Array<Double> const &);
template LogicalArray operator>=(Array<Double> const &, Array<Double> const &);
template LogicalArray operator>=(Array<Double> const &, Double const &);
template LogicalArray operator>=(Double const &, Array<Double> const &);
template LogicalArray operator<=(Array<Double> const &, Double const &);
template LogicalArray near(Array<Double> const &, Array<Double> const &, Double);
template LogicalArray near(Array<Double> const &, Double const &, Double);
template LogicalArray near(Double const &, Array<Double> const &, Double);
template LogicalArray nearAbs(Array<Double> const &, Array<Double> const &, Double);
template LogicalArray nearAbs(Array<Double> const &, Double const &, Double);
template LogicalArray nearAbs(Double const &, Array<Double> const &, Double);
} //# NAMESPACE - END
