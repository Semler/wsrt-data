// OrderedPair_1050.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/OrderedPair.cc>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedPair<Int, Vector<Int> >;
template class OrderedPair<Int, Vector<uInt> >;
template class OrderedPair<Int, Vector<Double> >;
} //# NAMESPACE - END
