// RecordField_1020.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/RecordField.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class RORecordFieldPtr<Complex>;
template class RORecordFieldPtr<DComplex>;
template class RORecordFieldPtr<Bool>;
template class RORecordFieldPtr<Double>;
template class RORecordFieldPtr<Float>;
template class RORecordFieldPtr<Int>;
template class RORecordFieldPtr<Record>;
template class RORecordFieldPtr<Short>;
template class RORecordFieldPtr<String>;
template class RORecordFieldPtr<uChar>;
template class RORecordFieldPtr<uInt>;
} //# NAMESPACE - END
