// Register_1000.cc -- Tue Apr  1 11:50:47 BST 2008 -- renting
#include <casa/Utilities/Register.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template <class Qtype> class Quantum;
template <class T> class Array;
template uInt Register(Quantum<Array<Complex> > const *);
} //# NAMESPACE - END
