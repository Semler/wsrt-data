// CountedPtr_1290.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Quanta/Quantum.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Quantum<Double> > >;
template class CountedPtr<Block<Quantum<Double> > >;
template class PtrRep<Block<Quantum<Double> > >;
template class SimpleCountedConstPtr<Block<Quantum<Double> > >;
template class SimpleCountedPtr<Block<Quantum<Double> > >;
} //# NAMESPACE - END
