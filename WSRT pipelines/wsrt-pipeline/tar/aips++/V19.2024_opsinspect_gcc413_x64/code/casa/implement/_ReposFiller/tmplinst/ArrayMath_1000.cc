// ArrayMath_1000.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
#include <casa/Quanta/MVTime.h>
namespace casa { //# NAMESPACE - BEGIN
template void convertArray(Array<Double> &, Array<MVTime> const &);
} //# NAMESPACE - END
