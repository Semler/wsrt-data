// ArrayIter_1000.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayIter.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template class ArrayIterator<Bool>;
template class ArrayIterator<uChar>;
template class ArrayIterator<Short>;
template class ArrayIterator<uShort>;
template class ArrayIterator<Int>;
template class ArrayIterator<uInt>;
template class ArrayIterator<Float>;
template class ArrayIterator<Double>;
template class ArrayIterator<Complex>;
template class ArrayIterator<DComplex>;
template class ArrayIterator<String>;
} //# NAMESPACE - END
