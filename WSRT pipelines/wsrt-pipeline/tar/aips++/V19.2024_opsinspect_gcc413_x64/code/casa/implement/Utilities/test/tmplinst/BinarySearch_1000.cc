// BinarySearch_1000.cc -- Tue Apr  1 11:57:26 BST 2008 -- renting
#include <casa/Utilities/BinarySearch.cc>
#include <casa/Arrays/IPosition.h>
namespace casa { //# NAMESPACE - BEGIN
template Int binarySearch(Bool &, IPosition const &, Int const &, uInt, Int);
template Int binarySearchBrackets(Bool &, Int * const &, Int const &, uInt, Int);
} //# NAMESPACE - END
