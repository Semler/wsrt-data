// RecordFieldWriter_1010.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/RecordFieldWriter.cc>
namespace casa { //# NAMESPACE - BEGIN
template class RecordFieldCopier<Bool, Bool>;
template class RecordFieldCopier<Complex, Complex>;
template class RecordFieldCopier<DComplex, DComplex>;
template class RecordFieldCopier<Double, Double>;
template class RecordFieldCopier<Float, Float>;
template class RecordFieldCopier<Float, Int>;
template class RecordFieldCopier<Int, Int>;
template class RecordFieldCopier<Short, Short>;
template class RecordFieldCopier<String, String>;
template class RecordFieldCopier<uChar, uChar>;
template class RecordFieldCopier<Double, Float>;
template class RecordFieldCopier<Int, Short>;
} //# NAMESPACE - END
