// ArrayLogical_1100.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allNear(Array<Float> const &, Array<Float> const &, Double);
template Bool allNear(Array<Float> const &, Float const &, Double);
template Bool allNearAbs(Array<Float> const &, Array<Float> const &, Double);
template Bool allNearAbs(Array<Float> const &, Float const &, Double);
template LogicalArray operator!=(Array<Float> const &, Array<Float> const &);
template LogicalArray operator!=(Array<Float> const &, Float const &);
template LogicalArray operator!=(Float const &, Array<Float> const &);
template LogicalArray operator<(Array<Float> const &, Array<Float> const &);
template LogicalArray operator<(Array<Float> const &, Float const &);
template LogicalArray operator<(Float const &, Array<Float> const &);
template LogicalArray operator<=(Array<Float> const &, Array<Float> const &);
template LogicalArray operator<=(Array<Float> const &, Float const &);
template LogicalArray operator<=(Float const &, Array<Float> const &);
template LogicalArray operator==(Array<Float> const &, Array<Float> const &);
template LogicalArray operator==(Array<Float> const &, Float const &);
template LogicalArray operator==(Float const &, Array<Float> const &);
template LogicalArray operator>(Array<Float> const &, Array<Float> const &);
template LogicalArray operator>(Array<Float> const &, Float const &);
template LogicalArray operator>(Float const &, Array<Float> const &);
template LogicalArray operator>=(Array<Float> const &, Array<Float> const &);
template LogicalArray operator>=(Array<Float> const &, Float const &);
template LogicalArray operator>=(Float const &, Array<Float> const &);
} //# NAMESPACE - END
