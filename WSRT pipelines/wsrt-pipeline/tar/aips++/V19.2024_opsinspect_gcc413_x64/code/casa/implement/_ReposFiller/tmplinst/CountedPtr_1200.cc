// CountedPtr_1200.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Complex> >;
template class CountedPtr<Block<Complex> >;
template class PtrRep<Block<Complex> >;
template class SimpleCountedConstPtr<Block<Complex> >;
template class SimpleCountedPtr<Block<Complex> >;
} //# NAMESPACE - END
