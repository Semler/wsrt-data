// OrderedMap_1000.cc -- Tue Apr  1 11:50:44 BST 2008 -- renting
#include <casa/Containers/OrderedMap.cc>
#include <casa/Arrays/IPosition.h>
#include <casa/BasicSL/String.h>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedMapRep<String, Block<IPosition> >;
template class OrderedMapIterRep<String, Block<IPosition> >;
template class OrderedMap<String, Block<IPosition> >;
template class OrderedMapNotice<String, Block<IPosition> >;
} //# NAMESPACE - END
