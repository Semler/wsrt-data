// MaskArrMath_1100.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/MaskArrMath.cc>
namespace casa { //# NAMESPACE - BEGIN
template const MaskedArray<Float> & operator+=(MaskedArray<Float> const &, MaskedArray<Float> const &);
template const MaskedArray<Float> & operator/=(MaskedArray<Float> const &, MaskedArray<Float> const &);
} //# NAMESPACE - END
