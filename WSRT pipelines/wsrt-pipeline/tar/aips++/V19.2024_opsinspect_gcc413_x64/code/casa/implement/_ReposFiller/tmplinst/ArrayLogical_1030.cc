// ArrayLogical_1030.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
#include <casa/Quanta/MVTime.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool anyEQ(MVTime const &, Array<MVTime> const &);
template LogicalArray operator==(MVTime const &, Array<MVTime> const &);
template LogicalArray operator!=(Array<MVTime> const &, Array<MVTime> const &);
template LogicalArray operator!=(Array<MVTime> const &, MVTime const &);
template LogicalArray operator!=(MVTime const &, Array<MVTime> const &);
template LogicalArray operator>(Array<MVTime> const &, Array<MVTime> const &);
template LogicalArray operator>(Array<MVTime> const &, MVTime const &);
template LogicalArray operator>(MVTime const &, Array<MVTime> const &);
template LogicalArray operator>=(Array<MVTime> const &, Array<MVTime> const &);
template LogicalArray operator>=(Array<MVTime> const &, MVTime const &);
template LogicalArray operator>=(MVTime const &, Array<MVTime> const &);
} //# NAMESPACE - END
