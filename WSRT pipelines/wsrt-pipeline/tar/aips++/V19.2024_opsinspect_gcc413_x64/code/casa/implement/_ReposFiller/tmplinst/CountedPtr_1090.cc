// CountedPtr_1090.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Array<Bool> >;
template class CountedPtr<Array<Bool> >;
template class PtrRep<Array<Bool> >;
template class SimpleCountedConstPtr<Array<Bool> >;
template class SimpleCountedPtr<Array<Bool> >;
} //# NAMESPACE - END
