// Map_1010.cc -- Tue Apr  1 11:50:44 BST 2008 -- renting
#include <casa/Containers/Map.cc>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template class ConstMapIter<String, Int>;
template class MapIter<String, Int>;
template class MapIterRep<String, Int>;
template class MapRep<String, Int>;
template class Map<String, Int>;
} //# NAMESPACE - END
