// ArrayLogical_1110.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool anyNE(Array<Bool> const &, Array<Bool> const &);
template Bool anyNE(Array<Bool> const &, Bool const &);
template Bool allAND(Array<Bool> const &, Bool const &);
template Bool allEQ(Array<Bool> const &, Array<Bool> const &);
template Bool allEQ(Array<Bool> const &, Bool const &);
template Bool allOR(Array<Bool> const &, Array<Bool> const &);
template Bool anyEQ(Array<Bool> const &, Bool const &);
template Bool anyEQ(Bool const &, Array<Bool> const &);
template uInt ntrue(Array<Bool> const &);
template uInt nfalse(Array<Bool> const &);
template LogicalArray operator!(Array<Bool> const &);
template LogicalArray operator!=(Array<Bool> const &, Array<Bool> const &);
template LogicalArray operator!=(Array<Bool> const &, Bool const &);
template LogicalArray operator!=(Bool const &, Array<Bool> const &);
template LogicalArray operator&&(Array<Bool> const &, Array<Bool> const &);
template LogicalArray operator&&(Array<Bool> const &, Bool const &);
template LogicalArray operator&&(Bool const &, Array<Bool> const &);
template LogicalArray operator==(Array<Bool> const &, Array<Bool> const &);
template LogicalArray operator==(Array<Bool> const &, Bool const &);
template LogicalArray operator==(Bool const &, Array<Bool> const &);
template LogicalArray operator||(Array<Bool> const &, Array<Bool> const &);
template LogicalArray operator||(Array<Bool> const &, Bool const &);
template LogicalArray operator||(Bool const &, Array<Bool> const &);
} //# NAMESPACE - END
