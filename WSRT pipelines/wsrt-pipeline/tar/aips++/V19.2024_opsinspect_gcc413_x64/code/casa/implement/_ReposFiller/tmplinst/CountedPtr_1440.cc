// CountedPtr_1440.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/IO/TypeIO.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<TypeIO>;
template class CountedConstPtr<TypeIO>;
template class SimpleCountedPtr<TypeIO>;
template class SimpleCountedConstPtr<TypeIO>;
template class PtrRep<TypeIO>;
} //# NAMESPACE - END
