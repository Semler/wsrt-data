// CountedPtr_1070.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/System/PGPlotterInterface.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<PGPlotterInterface>;
template class CountedConstPtr<PGPlotterInterface>;
template class SimpleCountedPtr<PGPlotterInterface>;
template class SimpleCountedConstPtr<PGPlotterInterface>;
template class PtrRep<PGPlotterInterface>;
} //# NAMESPACE - END
