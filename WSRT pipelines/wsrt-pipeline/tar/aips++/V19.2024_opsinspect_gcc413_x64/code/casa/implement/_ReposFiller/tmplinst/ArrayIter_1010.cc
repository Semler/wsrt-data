// ArrayIter_1010.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayIter.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template class ReadOnlyArrayIterator<Bool>;
template class ReadOnlyArrayIterator<uChar>;
template class ReadOnlyArrayIterator<Short>;
template class ReadOnlyArrayIterator<uShort>;
template class ReadOnlyArrayIterator<Int>;
template class ReadOnlyArrayIterator<uInt>;
template class ReadOnlyArrayIterator<Float>;
template class ReadOnlyArrayIterator<Double>;
template class ReadOnlyArrayIterator<Complex>;
template class ReadOnlyArrayIterator<DComplex>;
template class ReadOnlyArrayIterator<String>;
} //# NAMESPACE - END
