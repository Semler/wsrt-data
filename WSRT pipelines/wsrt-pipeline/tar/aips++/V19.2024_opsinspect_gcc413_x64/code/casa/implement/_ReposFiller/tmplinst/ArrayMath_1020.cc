// ArrayMath_1020.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<DComplex> abs(Array<DComplex> const &);
template Array<DComplex> cos(Array<DComplex> const &);
template Array<DComplex> cosh(Array<DComplex> const &);
template Array<DComplex> exp(Array<DComplex> const &);
template Array<DComplex> log(Array<DComplex> const &);
template Array<DComplex> log10(Array<DComplex> const &);
template Array<DComplex> max(Array<DComplex> const &, Array<DComplex> const &);
template Array<DComplex> max(Array<DComplex> const &, DComplex const &);
template Array<DComplex> min(Array<DComplex> const &, Array<DComplex> const &);
template Array<DComplex> min(Array<DComplex> const &, DComplex const &);
template Array<DComplex> operator*(Array<DComplex> const &, Array<DComplex> const &);
template Array<DComplex> operator*(Array<DComplex> const &, DComplex const &);
template Array<DComplex> operator*(DComplex const &, Array<DComplex> const &);
template Array<DComplex> operator+(Array<DComplex> const &, Array<DComplex> const &);
template Array<DComplex> operator+(Array<DComplex> const &, DComplex const &);
template Array<DComplex> operator+(DComplex const &, Array<DComplex> const &);
template Array<DComplex> operator-(Array<DComplex> const &);
template Array<DComplex> operator-(Array<DComplex> const &, Array<DComplex> const &);
template Array<DComplex> operator-(Array<DComplex> const &, DComplex const &);
template Array<DComplex> operator-(DComplex const &, Array<DComplex> const &);
template Array<DComplex> operator/(Array<DComplex> const &, Array<DComplex> const &);
template Array<DComplex> operator/(Array<DComplex> const &, DComplex const &);
template Array<DComplex> operator/(DComplex const &, Array<DComplex> const &);
template Array<DComplex> pow(Array<DComplex> const &, Array<DComplex> const &);
template Array<DComplex> pow(Array<DComplex> const &, Double const &);
template Array<DComplex> pow(DComplex const &, Array<DComplex> const &);
template Array<DComplex> sin(Array<DComplex> const &);
template Array<DComplex> sinh(Array<DComplex> const &);
template Array<DComplex> sqrt(Array<DComplex> const &);
template DComplex max(Array<DComplex> const &);
template DComplex min(Array<DComplex> const &);
template DComplex product(Array<DComplex> const &);
template DComplex sum(Array<DComplex> const &);
template Complex sum(Array<Complex> const &);
template void max(Array<DComplex> &, Array<DComplex> const &, Array<DComplex> const &);
template void max(Array<DComplex> &, Array<DComplex> const &, DComplex const &);
template void min(Array<DComplex> &, Array<DComplex> const &, Array<DComplex> const &);
template void min(Array<DComplex> &, Array<DComplex> const &, DComplex const &);
template void minMax(DComplex &, DComplex &, Array<DComplex> const &);
template void operator*=(Array<DComplex> &, Array<DComplex> const &);
template void operator*=(Array<DComplex> &, DComplex const &);
template void operator+=(Array<DComplex> &, Array<DComplex> const &);
template void operator+=(Array<DComplex> &, DComplex const &);
template void operator-=(Array<DComplex> &, Array<DComplex> const &);
template void operator-=(Array<DComplex> &, DComplex const &);
template void operator/=(Array<DComplex> &, Array<DComplex> const &);
template void operator/=(Array<DComplex> &, DComplex const &);
} //# NAMESPACE - END
