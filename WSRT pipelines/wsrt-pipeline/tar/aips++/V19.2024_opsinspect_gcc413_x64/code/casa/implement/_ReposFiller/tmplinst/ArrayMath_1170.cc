// ArrayMath_1170.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template uShort max(Array<uShort> const &);
template uShort min(Array<uShort> const &);
template void indgen(Array<uShort> &);
template void indgen(Array<uShort> &, uShort, uShort);
template void minMax(uShort &, uShort &, Array<uShort> const &);
} //# NAMESPACE - END
