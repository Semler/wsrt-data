// ArrayIO_1010.cc -- Tue Apr  1 11:49:18 BST 2008 -- renting
#include <casa/Arrays/ArrayIO.cc>
#include <casa/Quanta/Quantum.h>
namespace casa { //# NAMESPACE - BEGIN
template istream & operator>>(istream &, Array<Quantum<Double> > &);
template Bool read(istream &, Array<Quantum<Double> > &, IPosition const *, Bool);
template Bool readArrayBlock(istream &, Bool &, IPosition &, Block<Quantum<Double> > &, IPosition const *, Bool);
} //# NAMESPACE - END
