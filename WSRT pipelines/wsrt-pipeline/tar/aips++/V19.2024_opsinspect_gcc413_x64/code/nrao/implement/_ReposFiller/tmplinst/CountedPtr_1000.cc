// CountedPtr_1000.cc -- Tue Apr  1 12:40:17 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <nrao/VLA/VLAEnum.h>
namespace casa { //# NAMESPACE - BEGIN
template class PtrRep<Block<VLAEnum::IF> >;
template class CountedPtr<Block<VLAEnum::IF> >;
template class SimpleCountedPtr<Block<VLAEnum::IF> >;
template class SimpleCountedConstPtr<Block<VLAEnum::IF> >;
template class CountedConstPtr<Block<VLAEnum::IF> >;
} //# NAMESPACE - END
