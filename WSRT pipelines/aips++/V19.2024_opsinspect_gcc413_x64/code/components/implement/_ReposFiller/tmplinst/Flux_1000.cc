// Flux_1000.cc -- Tue Apr  1 12:17:53 BST 2008 -- renting
#include <components/ComponentModels/Flux.cc>
#include <casa/Utilities/CountedPtr.cc>
namespace casa { //# NAMESPACE - BEGIN
template class Flux<Double>;
template class FluxRep<Double>;
template class CountedPtr<FluxRep<Double> >;
template class PtrRep<FluxRep<Double> >;
template class SimpleCountedConstPtr<FluxRep<Double> >;
template class CountedConstPtr<FluxRep<Double> >;
template class SimpleCountedPtr<FluxRep<Double> >;
} //# NAMESPACE - END
