// CountedPtr_1040.cc -- Tue Apr  1 12:17:53 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <components/ComponentModels/SpectralModel.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<SpectralModel>;
template class CountedConstPtr<SpectralModel>;
template class SimpleCountedPtr<SpectralModel>;
template class SimpleCountedConstPtr<SpectralModel>;
template class PtrRep<SpectralModel>;
} //# NAMESPACE - END
