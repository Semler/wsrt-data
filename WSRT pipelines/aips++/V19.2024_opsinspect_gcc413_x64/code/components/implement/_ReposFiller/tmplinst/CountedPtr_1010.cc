// CountedPtr_1010.cc -- Tue Apr  1 12:17:53 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <components/ComponentModels/Flux.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Flux<Double> > >;
template class CountedPtr<Block<Flux<Double> > >;
template class PtrRep<Block<Flux<Double> > >;
template class SimpleCountedConstPtr<Block<Flux<Double> > >;
template class SimpleCountedPtr<Block<Flux<Double> > >;
} //# NAMESPACE - END
