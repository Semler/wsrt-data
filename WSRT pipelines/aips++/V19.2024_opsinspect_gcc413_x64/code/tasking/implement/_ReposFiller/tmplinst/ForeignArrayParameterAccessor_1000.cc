// ForeignArrayParameterAccessor_1000.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/ForeignArrayParameterAccessor.cc>
#include <casa/Quanta/QuantumHolder.h>
#include <casa/Quanta/Quantum.h>
#include <tasking/Tasking/ForeignBaseArrayParameterAccessor.cc>
#include <tasking/Tasking/ForeignNSArrayParameterAccessor.cc>
namespace casa { //# NAMESPACE - BEGIN
template class ForeignVectorParameterAccessor<QuantumHolder>;
template class ForeignArrayParameterAccessor<QuantumHolder>;
template class ForeignBaseVectorParameterAccessor<QuantumHolder>;
template class ForeignBaseArrayParameterAccessor<QuantumHolder>;
template class ForeignBaseVectorParameterAccessor<Quantum<Double> >;
template class ForeignBaseArrayParameterAccessor<Quantum<Double> >;
template class ForeignNSVectorParameterAccessor<Quantum<Double> >;
template class ForeignNSArrayParameterAccessor<Quantum<Double> >;
} //# NAMESPACE - END
