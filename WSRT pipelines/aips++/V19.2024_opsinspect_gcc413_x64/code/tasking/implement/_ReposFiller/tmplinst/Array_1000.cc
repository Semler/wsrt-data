// Array_1000.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <tasking/Tasking/Index.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<Index>;
#ifdef AIPS_SUN_NATIVE
template class Array<Index>::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
