// CountedPtr_1050.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <tasking/Glish/GlishRecord.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimpleCountedConstPtr<GlishRecord>;
template class CountedConstPtr<GlishRecord>;
template class CountedPtr<GlishRecord>;
template class PtrRep<GlishRecord>;
template class SimpleCountedPtr<GlishRecord>;
} //# NAMESPACE - END
