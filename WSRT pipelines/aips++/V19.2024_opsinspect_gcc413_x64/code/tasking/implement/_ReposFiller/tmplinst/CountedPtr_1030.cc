// CountedPtr_1030.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <graphics/Graphics/X11Intrinsic.h>
#include <tasking/Glish/XSysEvent.h>
namespace casa { //# NAMESPACE - BEGIN
template class PtrRep<_XtAppStruct *>;
template class SimpleCountedPtr<_XtAppStruct *>;
template class SimpleCountedConstPtr<_XtAppStruct *>;
} //# NAMESPACE - END
