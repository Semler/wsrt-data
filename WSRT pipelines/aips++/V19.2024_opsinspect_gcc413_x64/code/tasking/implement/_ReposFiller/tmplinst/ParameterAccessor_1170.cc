// ParameterAccessor_1170.cc -- Tue Apr  1 12:28:14 BST 2008 -- renting
#include <tasking/Tasking/ParameterAccessor.cc>
#include <measures/Measures/MeasureHolder.h>
#include <measures/Measures/MEpoch.h>
#include <measures/Measures/MDirection.h>
#include <measures/Measures/MDoppler.h>
#include <measures/Measures/MFrequency.h>
#include <measures/Measures/MPosition.h>
#include <measures/Measures/MRadialVelocity.h>
#include <measures/Measures/MBaseline.h>
#include <measures/Measures/Muvw.h>
#include <measures/Measures/MEarthMagnetic.h>
namespace casa { //# NAMESPACE - BEGIN
template class ParameterAccessor<MeasureHolder>;
template class ParameterAccessor<MEpoch>;
template class ParameterAccessor<MDirection>;
template class ParameterAccessor<MDoppler>;
template class ParameterAccessor<MFrequency>;
template class ParameterAccessor<MPosition>;
template class ParameterAccessor<MRadialVelocity>;
template class ParameterAccessor<MBaseline>;
template class ParameterAccessor<Muvw>;
template class ParameterAccessor<MEarthMagnetic>;
} //# NAMESPACE - END
