// CountedPtr_1020.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <graphics/Graphics/X11Intrinsic.h>
#include <tasking/Glish/XSysEvent.h>
namespace casa { //# NAMESPACE - BEGIN
template class PtrRep<_WidgetRec *>;
template class SimpleCountedPtr<_WidgetRec *>;
template class SimpleCountedConstPtr<_WidgetRec *>;
} //# NAMESPACE - END
