// ForeignParameterAccessor_1010.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/ForeignParameterAccessor.cc>
#include <casa/Quanta/QuantumHolder.h>
#include <casa/Arrays/Vector.h>
#include <casa/Quanta/Quantum.h>
#include <tasking/Tasking/ForeignNSParameterAccessor.cc>
#include <tasking/Tasking/ForeignBaseParameterAccessor.cc>
namespace casa { //# NAMESPACE - BEGIN
template class ForeignParameterAccessor<QuantumHolder>;
template class ForeignBaseParameterAccessor<QuantumHolder>;
template class ForeignBaseParameterAccessor<Quantum<Double> >;
template class ForeignBaseParameterAccessor<Quantum<Vector<Double> > >;
template class ForeignBaseParameterAccessor<Quantum<Array<Double> > >;
template class ForeignNSParameterAccessor<Quantum<Double> >;
template class ForeignNSParameterAccessor<Quantum<Vector<Double> > >;
template class ForeignNSParameterAccessor<Quantum<Array<Double> > >;
} //# NAMESPACE - END
