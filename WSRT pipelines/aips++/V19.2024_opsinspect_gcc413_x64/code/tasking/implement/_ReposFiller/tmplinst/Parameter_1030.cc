// Parameter_1030.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/Parameter.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template class Parameter<Array<Double> >;
template class Parameter<Array<Float> >;
template class Parameter<Array<Int> >;
template class Parameter<Array<Bool> >;
} //# NAMESPACE - END
