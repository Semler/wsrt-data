// ForeignParameterAccessor_1030.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/ForeignParameterAccessor.cc>
#include <scimath/Functionals/FunctionHolder.h>
#include <tasking/Tasking/ForeignBaseParameterAccessor.cc>
namespace casa { //# NAMESPACE - BEGIN
template class ForeignParameterAccessor<FunctionHolder<Double> >;
template class ForeignBaseParameterAccessor<FunctionHolder<Double> >;
} //# NAMESPACE - END
