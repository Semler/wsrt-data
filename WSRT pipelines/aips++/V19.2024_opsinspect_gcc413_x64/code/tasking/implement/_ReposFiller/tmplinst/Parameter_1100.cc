// Parameter_1100.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/Parameter.cc>
#include <casa/Quanta/QuantumHolder.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class Parameter<QuantumHolder>;
template class Parameter<Quantum<Double> >;
template class Parameter<Quantum<Vector<Double> > >;
} //# NAMESPACE - END
