// ParameterConstraint_1180.cc -- Tue Apr  1 12:28:14 BST 2008 -- renting
#include <tasking/Tasking/ParameterConstraint.cc>
#include <casa/Quanta/QuantumHolder.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class ParameterConstraint<QuantumHolder>;
template class ParameterConstraint<Quantum<Double> >;
template class ParameterConstraint<Quantum<Vector<Double> > >;
template class ParameterConstraint<Quantum<Array<Double> > >;
} //# NAMESPACE - END
