// ArrayLogical_1000.cc -- Tue Apr  1 12:20:00 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allNearAbs(Array<Double> const &, Double const &, Double);
template Bool allNearAbs(Array<Complex> const &, Array<Complex> const &, Double);
} //# NAMESPACE - END
