// CountedPtr_1000.cc -- Tue Apr  1 12:22:33 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <msvis/MSVis/StokesVector.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<CStokesVector> >;
template class CountedConstPtr<Block<CStokesVector> >;
template class SimpleCountedPtr<Block<CStokesVector> >;
template class SimpleCountedConstPtr<Block<CStokesVector> >;
template class PtrRep<Block<CStokesVector> >;
} //# NAMESPACE - END
