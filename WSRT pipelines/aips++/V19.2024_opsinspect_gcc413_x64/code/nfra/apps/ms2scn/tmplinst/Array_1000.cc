// Array_1000.cc -- Tue Apr  1 12:41:54 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <casa/Arrays/MaskedArray.cc>
#include <casa/Containers/Block.h>
#include <casa/Utilities/CountedPtr.cc>
#include <SubGroupInfo.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<SubGroupInfo>;
template class MaskedArray<SubGroupInfo>;
template class SimpleCountedConstPtr<Block<SubGroupInfo> >;
template class PtrRep<Block<SubGroupInfo> >;
template class CountedPtr<Block<SubGroupInfo> >;
template class CountedConstPtr<Block<SubGroupInfo> >;
template class SimpleCountedPtr<Block<SubGroupInfo> >;
} //# NAMESPACE - END
