// HyperPlane2_1010.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/HyperPlane2.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class HyperPlane<AutoDiff<Double> >;
template class HyperPlane<AutoDiff<Float> >;
} //# NAMESPACE - END
