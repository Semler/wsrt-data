// GNoiseParam_1000.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/GNoiseParam.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class GNoiseParam<AutoDiff<DComplex> >;
template class GNoiseParam<AutoDiff<Complex> >;
} //# NAMESPACE - END
