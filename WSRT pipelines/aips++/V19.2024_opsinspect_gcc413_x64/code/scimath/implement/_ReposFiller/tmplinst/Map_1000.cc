// Map_1000.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Containers/Map.cc>
#include <casa/Containers/OrderedPair.h>
#include <scimath/Functionals/AbstractFunctionFactory.h>
namespace casa { //# NAMESPACE - BEGIN
template class Map<String, OrderedPair<FunctionFactory<Double>*, Bool> >;
template class MapRep<String, OrderedPair<FunctionFactory<Double>*, Bool> >;
template class ConstMapIter<String, OrderedPair<FunctionFactory<Double>*, Bool> >;
template class MapIter<String, OrderedPair<FunctionFactory<Double>*, Bool> >;
template class MapIterRep<String, OrderedPair<FunctionFactory<Double>*, Bool> >;
} //# NAMESPACE - END
