// KaiserBFunction_1010.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/KaiserBFunction.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class KaiserBFunction<DComplex>;
template class KaiserBFunction<Complex>;
} //# NAMESPACE - END
