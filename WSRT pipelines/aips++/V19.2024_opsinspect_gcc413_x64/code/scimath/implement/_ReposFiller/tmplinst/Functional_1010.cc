// Functional_1010.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/BasicMath/Functional.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class Functional<Vector<AutoDiffA<Complex> >, AutoDiffA<Complex> >;
} //# NAMESPACE - END
