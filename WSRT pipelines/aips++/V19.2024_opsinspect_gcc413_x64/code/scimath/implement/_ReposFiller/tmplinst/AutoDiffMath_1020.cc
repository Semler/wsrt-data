// AutoDiffMath_1020.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template AutoDiff<DComplex> acos(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> asin(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> atan(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> atan2(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template AutoDiff<DComplex> cos(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> cosh(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> exp(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> log(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> log10(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> erf(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> erfc(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> pow(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template AutoDiff<DComplex> pow(AutoDiff<DComplex> const &, DComplex const &);
template AutoDiff<DComplex> sin(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> sinh(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> sqrt(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> abs(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> ceil(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> floor(AutoDiff<DComplex> const &);
template AutoDiff<DComplex> fmod(AutoDiff<DComplex> const &, DComplex const &);
template AutoDiff<DComplex> fmod(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template AutoDiff<DComplex> max(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
template AutoDiff<DComplex> min(AutoDiff<DComplex> const &, AutoDiff<DComplex> const &);
} //# NAMESPACE - END
