// CountedPtr_1130.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <casa/Containers/Block.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<AutoDiff<Complex> > >;
template class CountedConstPtr<Block<AutoDiff<Complex> > >;
template class SimpleCountedPtr<Block<AutoDiff<Complex> > >;
template class SimpleCountedConstPtr<Block<AutoDiff<Complex> > >;
template class PtrRep<Block<AutoDiff<Complex> > >;
} //# NAMESPACE - END
