// CompoundParam_1020.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/CompoundParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class CompoundParam<AutoDiff<Double> >;
template class CompoundParam<AutoDiff<Float> >;
} //# NAMESPACE - END
