// DiracDFunction_1030.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/DiracDFunction.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class DiracDFunction<Double>;
template class DiracDFunction<Float>;
} //# NAMESPACE - END
