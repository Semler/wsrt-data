// OddPolynomial_1000.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/OddPolynomial.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class OddPolynomial<DComplex>;
template class OddPolynomial<Complex>;
} //# NAMESPACE - END
