// AutoDiffMath_1080.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffMath.cc>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template AutoDiff<Float> acos(AutoDiff<Float> const &);
template AutoDiff<Float> asin(AutoDiff<Float> const &);
template AutoDiff<Float> atan(AutoDiff<Float> const &);
template AutoDiff<Float> atan2(AutoDiff<Float> const &, AutoDiff<Float> const &);
template AutoDiff<Float> cos(AutoDiff<Float> const &);
template AutoDiff<Float> cosh(AutoDiff<Float> const &);
template AutoDiff<Float> exp(AutoDiff<Float> const &);
template AutoDiff<Float> log(AutoDiff<Float> const &);
template AutoDiff<Float> log10(AutoDiff<Float> const &);
template AutoDiff<Float> erf(AutoDiff<Float> const &);
template AutoDiff<Float> erfc(AutoDiff<Float> const &);
template AutoDiff<Float> pow(AutoDiff<Float> const &, AutoDiff<Float> const &);
template AutoDiff<Float> pow(AutoDiff<Float> const &, Float const &);
template AutoDiff<Float> square(AutoDiff<Float> const &);
template AutoDiff<Float> cube(AutoDiff<Float> const &);
template AutoDiff<Float> sin(AutoDiff<Float> const &);
template AutoDiff<Float> sinh(AutoDiff<Float> const &);
template AutoDiff<Float> sqrt(AutoDiff<Float> const &);
template AutoDiff<Float> tan(AutoDiff<Float> const &);
template AutoDiff<Float> tanh(AutoDiff<Float> const &);
template AutoDiff<Float> abs(AutoDiff<Float> const &);
template AutoDiff<Float> ceil(AutoDiff<Float> const &);
template AutoDiff<Float> floor(AutoDiff<Float> const &);
template AutoDiff<Float> fmod(AutoDiff<Float> const &, Float const &);
template AutoDiff<Float> fmod(AutoDiff<Float> const &, AutoDiff<Float> const &);
template AutoDiff<Float> max(AutoDiff<Float> const &, AutoDiff<Float> const &);
template AutoDiff<Float> min(AutoDiff<Float> const &, AutoDiff<Float> const &);
} //# NAMESPACE - END
