// Chebyshev_1000.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/Chebyshev.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class Chebyshev<DComplex>;
template class Chebyshev<Complex>;
} //# NAMESPACE - END
