// CountedPtr_1020.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Arrays/Vector.h>
#include <scimath/Mathematics/SquareMatrix.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<Vector<SquareMatrix<Complex, 2> > > >;
template class CountedConstPtr<Block<Vector<SquareMatrix<Complex, 2> > > >;
template class SimpleCountedPtr<Block<Vector<SquareMatrix<Complex, 2> > > >;
template class SimpleCountedConstPtr<Block<Vector<SquareMatrix<Complex, 2> > > >;
template class PtrRep<Block<Vector<SquareMatrix<Complex, 2> > > >;
} //# NAMESPACE - END
