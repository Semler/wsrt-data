// MatrixMathLA_1070.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/Arrays/Matrix.h>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template Matrix<Complex> invertSymPosDef(Matrix<Complex> const &);
template void invertSymPosDef(Matrix<Complex> &, Complex &, Matrix<Complex> const &);
} //# NAMESPACE - END
