// Sinusoid1DParam_1020.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Functionals/Sinusoid1DParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Sinusoid1DParam<AutoDiff<Double> >;
template class Sinusoid1DParam<AutoDiff<Float> >;
} //# NAMESPACE - END
