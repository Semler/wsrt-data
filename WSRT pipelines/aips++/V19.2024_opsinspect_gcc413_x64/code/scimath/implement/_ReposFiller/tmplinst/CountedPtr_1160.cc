// CountedPtr_1160.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<AutoDiff<Float> > >;
template class CountedConstPtr<Block<AutoDiff<Float> > >;
template class SimpleCountedPtr<Block<AutoDiff<Float> > >;
template class SimpleCountedConstPtr<Block<AutoDiff<Float> > >;
template class PtrRep<Block<AutoDiff<Float> > >;
} //# NAMESPACE - END
