// Functional_1030.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/BasicMath/Functional.cc>
#include <casa/Arrays/Vector.h>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template class Functional<Vector<AutoDiff<Float> >, AutoDiff<Float> >;
} //# NAMESPACE - END
