// CountedPtr_1000.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <casa/Containers/Block.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimpleCountedConstPtr<Block<AutoDiffA<Complex> > >;
template class SimpleCountedPtr<Block<AutoDiffA<Complex> > >;
template class CountedConstPtr<Block<AutoDiffA<Complex> > >;
template class CountedPtr<Block<AutoDiffA<Complex> > >;
template class PtrRep<Block<AutoDiffA<Complex> > >;
} //# NAMESPACE - END
