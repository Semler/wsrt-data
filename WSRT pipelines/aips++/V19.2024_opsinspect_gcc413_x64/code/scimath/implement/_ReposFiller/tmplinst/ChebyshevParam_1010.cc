// ChebyshevParam_1010.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/ChebyshevParam.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class ChebyshevParam<DComplex>;
template class ChebyshevParam<Complex>;
template class ChebyshevParamModeImpl<DComplex>;
template class ChebyshevParamModeImpl<Complex>;
} //# NAMESPACE - END
