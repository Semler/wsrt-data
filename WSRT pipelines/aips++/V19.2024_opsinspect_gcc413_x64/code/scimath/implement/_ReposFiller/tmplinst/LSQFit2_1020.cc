// LSQFit2_1020.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LSQFit2.cc>
#include <casa/Arrays/VectorSTLIterator.h>
namespace casa { //# NAMESPACE - BEGIN
template void LSQFit::makeNorm<Float, VectorSTLIterator<Float> >(VectorSTLIterator<Float> const &, Float const &, Float const &, Bool, Bool);
template void LSQFit::makeNorm<Float, VectorSTLIterator<Float> >(VectorSTLIterator<Float> const &, Float const &, Float const &, LSQFit::Real, Bool, Bool);
template void LSQFit::makeNorm<Float, VectorSTLIterator<Float>, VectorSTLIterator<uInt> >(uInt, VectorSTLIterator<uInt> const &, VectorSTLIterator<Float> const &, Float const &, Float const &, Bool, Bool);
template void LSQFit::makeNorm<Float, VectorSTLIterator<Float>, VectorSTLIterator<uInt> >(uInt, VectorSTLIterator<uInt> const &, VectorSTLIterator<Float> const &, Float const &, Float const &, LSQFit::Real, Bool, Bool);
template void LSQFit::makeNorm<Float, Float>(std::vector<std::pair<uInt, Float> > const &, Float const &, Float const &, Bool, Bool);
template void LSQFit::makeNorm<Float, Float>(std::vector<std::pair<uInt, Float> > const &, Float const &, Float const &, LSQFit::Real, Bool, Bool);
template Bool LSQFit::addConstraint<Float, VectorSTLIterator<Float> >(VectorSTLIterator<Float> const &, Float const &);
template Bool LSQFit::addConstraint<Float, VectorSTLIterator<Float>, VectorSTLIterator<uInt> >(uInt, VectorSTLIterator<uInt> const &, VectorSTLIterator<Float> const &, Float const &);
template Bool LSQFit::setConstraint<Float, VectorSTLIterator<Float> >(uInt, VectorSTLIterator<Float> const &, Float const &);
template Bool LSQFit::setConstraint<Float, VectorSTLIterator<Float>, VectorSTLIterator<uInt> >(uInt, uInt, VectorSTLIterator<uInt> const &, VectorSTLIterator<Float> const &, Float const &);
template Bool LSQFit::getConstraint<VectorSTLIterator<Float> >(uInt, VectorSTLIterator<Float> &) const;
template void LSQFit::solve<VectorSTLIterator<Float> >(VectorSTLIterator<Float> &);
template Bool LSQFit::solveLoop<VectorSTLIterator<Float> >(Double &, uInt &, VectorSTLIterator<Float> &, Bool);
template Bool LSQFit::solveLoop<VectorSTLIterator<Float> >(uInt &, VectorSTLIterator<Float> &, Bool);
template void LSQFit::copy<VectorSTLIterator<Float> >(Double const *, Double const *, VectorSTLIterator<Float> &, LSQReal);
template void LSQFit::uncopy<VectorSTLIterator<Float> >(Double *, Double const *, VectorSTLIterator<Float> &, LSQReal);
template void LSQFit::copyDiagonal<VectorSTLIterator<Float> >(VectorSTLIterator<Float> &, LSQReal);
template Bool LSQFit::getErrors<VectorSTLIterator<Float> >(VectorSTLIterator<Float> &);
} //# NAMESPACE - END
