// MatrixMathLA_1030.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/Arrays/Matrix.h>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template void CholeskyDecomp(Matrix<Complex> &, Vector<Complex> &);
template void CholeskySolve(Matrix<Complex> &, Vector<Complex> &, Vector<Complex> &, Vector<Complex> &);
} //# NAMESPACE - END
