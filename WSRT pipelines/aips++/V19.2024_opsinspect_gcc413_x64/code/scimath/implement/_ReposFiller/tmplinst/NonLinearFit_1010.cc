// NonLinearFit_1010.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/NonLinearFit.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class NonLinearFit<AutoDiff<DComplex> >;
template class NonLinearFit<AutoDiffA<DComplex> >;
} //# NAMESPACE - END
