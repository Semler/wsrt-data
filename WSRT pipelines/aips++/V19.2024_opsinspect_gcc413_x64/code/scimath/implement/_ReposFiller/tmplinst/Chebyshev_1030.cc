// Chebyshev_1030.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/Chebyshev.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Chebyshev<AutoDiffA<Double> >;
template class Chebyshev<AutoDiffA<Float> >;
} //# NAMESPACE - END
