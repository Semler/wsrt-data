// NonLinearFitLM_1030.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/NonLinearFitLM.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class NonLinearFitLM<AutoDiff<Double> >;
template class NonLinearFitLM<AutoDiffA<Double> >;
} //# NAMESPACE - END
