// MaskedArray_1040.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/MaskedArray.cc>
#include <casa/Arrays/Vector.h>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class MaskedArray<Vector<SquareMatrix<Float, 2> > >;
} //# NAMESPACE - END
