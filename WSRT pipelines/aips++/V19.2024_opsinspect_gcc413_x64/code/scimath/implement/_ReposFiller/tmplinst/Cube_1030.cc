// Cube_1030.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/Cube.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class Cube<SquareMatrix<Complex, 4> >;
} //# NAMESPACE - END
