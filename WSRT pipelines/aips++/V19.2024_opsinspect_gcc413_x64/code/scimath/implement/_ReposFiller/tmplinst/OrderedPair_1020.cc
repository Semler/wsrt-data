// OrderedPair_1020.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Containers/OrderedPair.cc>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedPair<Double, SquareMatrix<Complex, 2>*>;
} //# NAMESPACE - END
