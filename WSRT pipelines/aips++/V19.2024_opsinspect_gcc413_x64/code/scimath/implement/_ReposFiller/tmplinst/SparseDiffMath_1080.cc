// SparseDiffMath_1080.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool operator!=(SparseDiff<Double> const &, SparseDiff<Double> const &);
template Bool operator<=(SparseDiff<Double> const &, SparseDiff<Double> const &);
template Bool operator<(SparseDiff<Double> const &, SparseDiff<Double> const &);
template Bool operator==(SparseDiff<Double> const &, SparseDiff<Double> const &);
template Bool operator>(SparseDiff<Double> const &, SparseDiff<Double> const &);
template Bool operator>=(SparseDiff<Double> const &, SparseDiff<Double> const &);
template Bool near(SparseDiff<Double> const &, SparseDiff<Double> const &);
template Bool operator!=(SparseDiff<Double> const &, Double const &);
template Bool operator<=(SparseDiff<Double> const &, Double const &);
template Bool operator<(SparseDiff<Double> const &, Double const &);
template Bool operator==(SparseDiff<Double> const &, Double const &);
template Bool operator>(SparseDiff<Double> const &, Double const &);
template Bool operator>=(SparseDiff<Double> const &, Double const &);
template Bool near(SparseDiff<Double> const &, Double const &);
template Bool operator!=(Double const &, SparseDiff<Double> const &);
template Bool operator<=(Double const &, SparseDiff<Double> const &);
template Bool operator<(Double const &, SparseDiff<Double> const &);
template Bool operator==(Double const &, SparseDiff<Double> const &);
template Bool operator>(Double const &, SparseDiff<Double> const &);
template Bool operator>=(Double const &, SparseDiff<Double> const &);
template Bool near(Double const &, SparseDiff<Double> const &);
} //# NAMESPACE - END
