// MatrixMathLA_1050.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/Arrays/Matrix.h>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template Matrix<AutoDiff<Complex> > invertSymPosDef(Matrix<AutoDiff<Complex> > const &);
template void invertSymPosDef(Matrix<AutoDiff<Complex> > &, AutoDiff<Complex> &, Matrix<AutoDiff<Complex> > const &);
} //# NAMESPACE - END
