// DiracDParam_1000.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/DiracDParam.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class DiracDParam<AutoDiff<DComplex> >;
template class DiracDParam<AutoDiff<Complex> >;
} //# NAMESPACE - END
