// Cube_1000.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/Cube.cc>
#include <casa/Arrays/Vector.h>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class Cube<Vector<SquareMatrix<Complex, 2> > >;
} //# NAMESPACE - END
