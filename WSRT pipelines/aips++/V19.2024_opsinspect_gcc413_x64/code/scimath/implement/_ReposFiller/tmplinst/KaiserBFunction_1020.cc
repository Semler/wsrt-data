// KaiserBFunction_1020.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/KaiserBFunction.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class KaiserBFunction<AutoDiff<Double> >;
template class KaiserBFunction<AutoDiff<Float> >;
} //# NAMESPACE - END
