// SparseDiffMath_1030.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template SparseDiff<Complex> operator+(SparseDiff<Complex> const &);
template SparseDiff<Complex> operator-(SparseDiff<Complex> const &);
template SparseDiff<Complex> operator*(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template SparseDiff<Complex> operator+(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template SparseDiff<Complex> operator-(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template SparseDiff<Complex> operator/(SparseDiff<Complex> const &, SparseDiff<Complex> const &);
template SparseDiff<Complex> operator*(Complex const &, SparseDiff<Complex> const &);
template SparseDiff<Complex> operator+(Complex const &, SparseDiff<Complex> const &);
template SparseDiff<Complex> operator-(Complex const &, SparseDiff<Complex> const &);
template SparseDiff<Complex> operator/(Complex const &, SparseDiff<Complex> const &);
template SparseDiff<Complex> operator*(SparseDiff<Complex> const &, Complex const &);
template SparseDiff<Complex> operator+(SparseDiff<Complex> const &, Complex const &);
template SparseDiff<Complex> operator-(SparseDiff<Complex> const &, Complex const &);
template SparseDiff<Complex> operator/(SparseDiff<Complex> const &, Complex const &);
} //# NAMESPACE - END
