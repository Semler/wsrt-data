// SPolynomial_1010.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/SPolynomial.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class SPolynomial<AutoDiff<Complex> >;
template class SPolynomial<AutoDiff<DComplex> >;
} //# NAMESPACE - END
