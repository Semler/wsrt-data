// LSQaips_1010.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LSQaips.cc>
#include <casa/Arrays/Array.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool LSQaips::getCovariance<Float>(Array<Float> &);
template Bool LSQaips::solveLoop<Float>(Double &, uInt &, Vector<Float> &, Bool doSVD);
template Bool LSQaips::solveLoop<Float>(uInt &, Vector<Float> &, Bool doSVD);
} //# NAMESPACE - END
