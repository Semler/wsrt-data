// SparseDiffMath_1100.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template SparseDiff<Double> acos(SparseDiff<Double> const &);
template SparseDiff<Double> asin(SparseDiff<Double> const &);
template SparseDiff<Double> atan(SparseDiff<Double> const &);
template SparseDiff<Double> atan2(SparseDiff<Double> const &, SparseDiff<Double> const &);
template SparseDiff<Double> cos(SparseDiff<Double> const &);
template SparseDiff<Double> cosh(SparseDiff<Double> const &);
template SparseDiff<Double> exp(SparseDiff<Double> const &);
template SparseDiff<Double> log(SparseDiff<Double> const &);
template SparseDiff<Double> log10(SparseDiff<Double> const &);
template SparseDiff<Double> erf(SparseDiff<Double> const &);
template SparseDiff<Double> erfc(SparseDiff<Double> const &);
template SparseDiff<Double> pow(SparseDiff<Double> const &, SparseDiff<Double> const &);
template SparseDiff<Double> pow(SparseDiff<Double> const &, Double const &);
template SparseDiff<Double> square(SparseDiff<Double> const &);
template SparseDiff<Double> cube(SparseDiff<Double> const &);
template SparseDiff<Double> sin(SparseDiff<Double> const &);
template SparseDiff<Double> sinh(SparseDiff<Double> const &);
template SparseDiff<Double> sqrt(SparseDiff<Double> const &);
template SparseDiff<Double> tan(SparseDiff<Double> const &);
template SparseDiff<Double> tanh(SparseDiff<Double> const &);
template SparseDiff<Double> abs(SparseDiff<Double> const &);
template SparseDiff<Double> ceil(SparseDiff<Double> const &);
template SparseDiff<Double> floor(SparseDiff<Double> const &);
template SparseDiff<Double> fmod(SparseDiff<Double> const &, Double const &);
template SparseDiff<Double> fmod(SparseDiff<Double> const &, SparseDiff<Double> const &);
template SparseDiff<Double> max(SparseDiff<Double> const &, SparseDiff<Double> const &);
template SparseDiff<Double> min(SparseDiff<Double> const &, SparseDiff<Double> const &);
} //# NAMESPACE - END
