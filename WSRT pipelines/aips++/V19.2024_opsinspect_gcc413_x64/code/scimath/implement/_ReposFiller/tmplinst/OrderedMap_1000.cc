// OrderedMap_1000.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Containers/OrderedMap.cc>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedMap<Double, SquareMatrix<Complex, 2>*>;
template class OrderedMapRep<Double, SquareMatrix<Complex, 2>*>;
template class OrderedMapIterRep<Double, SquareMatrix<Complex, 2>*>;
template class OrderedMapNotice<Double, SquareMatrix<Complex, 2>*>;
} //# NAMESPACE - END
