// DiracDFunction_1000.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/DiracDFunction.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class DiracDFunction<AutoDiff<DComplex> >;
template class DiracDFunction<AutoDiff<Complex> >;
} //# NAMESPACE - END
