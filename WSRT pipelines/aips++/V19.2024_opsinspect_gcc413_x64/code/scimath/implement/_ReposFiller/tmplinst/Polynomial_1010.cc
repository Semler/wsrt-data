// Polynomial_1010.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/Polynomial.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Polynomial<AutoDiffA<Double> >;
template class Polynomial<AutoDiffA<Float> >;
} //# NAMESPACE - END
