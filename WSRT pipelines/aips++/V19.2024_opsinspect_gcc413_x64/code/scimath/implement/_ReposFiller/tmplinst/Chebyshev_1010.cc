// Chebyshev_1010.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/Chebyshev.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Chebyshev<AutoDiff<DComplex> >;
template class Chebyshev<AutoDiff<Complex> >;
} //# NAMESPACE - END
