// Gaussian1D_1010.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Gaussian1D.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Gaussian1D<AutoDiffA<Double> >;
template class Gaussian1D<AutoDiffA<Float> >;
} //# NAMESPACE - END
