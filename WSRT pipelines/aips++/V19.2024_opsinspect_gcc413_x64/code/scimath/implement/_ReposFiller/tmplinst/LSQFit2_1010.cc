// LSQFit2_1010.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LSQFit2.cc>
#include <casa/Arrays/VectorSTLIterator.h>
namespace casa { //# NAMESPACE - BEGIN
template void LSQFit::makeNorm<Double, VectorSTLIterator<Double> >(VectorSTLIterator<Double> const &, Double const &, Double const &, Bool, Bool);
template void LSQFit::makeNorm<Double, VectorSTLIterator<Double> >(VectorSTLIterator<Double> const &, Double const &, Double const &, LSQFit::Real, Bool, Bool);
template void LSQFit::makeNorm<Double, VectorSTLIterator<Double>, VectorSTLIterator<uInt> >(uInt, VectorSTLIterator<uInt> const &, VectorSTLIterator<Double> const &, Double const &, Double const &, Bool, Bool);
template void LSQFit::makeNorm<Double, VectorSTLIterator<Double>, VectorSTLIterator<uInt> >(uInt, VectorSTLIterator<uInt> const &, VectorSTLIterator<Double> const &, Double const &, Double const &, LSQFit::Real, Bool, Bool);
template void LSQFit::makeNorm<Double, Double>(std::vector<std::pair<uInt, Double> > const &, Double const &, Double const &, Bool, Bool);
template void LSQFit::makeNorm<Double, Double>(std::vector<std::pair<uInt, Double> > const &, Double const &, Double const &, LSQFit::Real, Bool, Bool);
template Bool LSQFit::addConstraint<Double, VectorSTLIterator<Double> >(VectorSTLIterator<Double> const &, Double const &);
template Bool LSQFit::addConstraint<Double, VectorSTLIterator<Double>, VectorSTLIterator<uInt> >(uInt, VectorSTLIterator<uInt> const &, VectorSTLIterator<Double> const &, Double const &);
template Bool LSQFit::setConstraint<Double, VectorSTLIterator<Double> >(uInt n, VectorSTLIterator<Double> const &, Double const &);
template Bool LSQFit::setConstraint<Double, VectorSTLIterator<Double>, VectorSTLIterator<uInt> >(uInt n, uInt, VectorSTLIterator<uInt> const &, VectorSTLIterator<Double> const &, Double const &);
template Bool LSQFit::getConstraint<VectorSTLIterator<Double> >(uInt, VectorSTLIterator<Double> &) const;
template void LSQFit::solve<VectorSTLIterator<Double> >(VectorSTLIterator<Double> &);
template Bool LSQFit::solveLoop<VectorSTLIterator<Double> >(Double &, uInt &, VectorSTLIterator<Double> &, Bool);
template Bool LSQFit::solveLoop<VectorSTLIterator<Double> >(uInt &, VectorSTLIterator<Double> &, Bool);
template void LSQFit::copy<VectorSTLIterator<Double> >(Double const *, Double const *, VectorSTLIterator<Double> &, LSQReal);
template void LSQFit::uncopy<VectorSTLIterator<Double> >(Double *, Double const *, VectorSTLIterator<Double> &, LSQReal);
template void LSQFit::copyDiagonal<VectorSTLIterator<Double> >(VectorSTLIterator<Double> &, LSQReal);
template Bool LSQFit::getErrors<VectorSTLIterator<Double> >(VectorSTLIterator<Double> &);
} //# NAMESPACE - END
