// SpecificFunctionFactory_1030.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Functionals/SpecificFunctionFactory.h>
#include <scimath/Functionals/MarshallableChebyshev.h>
namespace casa { //# NAMESPACE - BEGIN
template class SpecificFunctionFactory<Float, MarshallableChebyshev<Float> >;
} //# NAMESPACE - END
