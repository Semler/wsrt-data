// DiracDParam_1020.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/DiracDParam.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class DiracDParam<AutoDiff<Double> >;
template class DiracDParam<AutoDiff<Float> >;
} //# NAMESPACE - END
