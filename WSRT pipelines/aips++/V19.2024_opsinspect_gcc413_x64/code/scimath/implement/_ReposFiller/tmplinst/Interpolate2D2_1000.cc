// Interpolate2D2_1000.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/Interpolate2D2.cc>
namespace casa { //# NAMESPACE - BEGIN
template Bool Interpolate2D::interpCubic<Double>(Double &result, Vector<Double> const &where, Matrix<Double> const &data, Matrix<Bool> const* &maskPtr) const;
template Bool Interpolate2D::interpLinear<Double>(Double &result, Vector<Double> const &where, Matrix<Double> const &data, Matrix<Bool> const* &maskPtr) const;
template Bool Interpolate2D::interpNearest<Double>(Double &result, Vector<Double> const &where, Matrix<Double> const &data, Matrix<Bool> const* &maskPtr) const;
template Bool Interpolate2D::interpLinear2<Double>(Double &resultI, Double &resultJ, Vector<Double> const &where, Matrix<Double> const &dataI, Matrix<Double> const &dataJ, Matrix<Bool> const &mask) const;
} //# NAMESPACE - END
