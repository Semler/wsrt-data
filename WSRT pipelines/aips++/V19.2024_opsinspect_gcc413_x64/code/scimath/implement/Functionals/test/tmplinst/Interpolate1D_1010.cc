// Interpolate1D_1010.cc -- Tue Apr  1 11:57:47 BST 2008 -- renting
#include <scimath/Functionals/Interpolate1D.cc>
namespace casa { //# NAMESPACE - BEGIN
template class Interpolate1D<Float, Double>;
template class Interpolate1D<Float, Float>;
template class Interpolate1D<Int, Double>;
#include <casa/BasicSL/Complex.h>
template class Interpolate1D<Double, DComplex>;
} //# NAMESPACE - END
