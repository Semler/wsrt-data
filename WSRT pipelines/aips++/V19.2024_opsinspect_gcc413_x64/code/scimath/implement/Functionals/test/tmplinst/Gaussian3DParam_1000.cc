// Gaussian3DParam_1000.cc -- Tue Apr  1 11:57:47 BST 2008 -- renting
#include <scimath/Functionals/Gaussian3DParam.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Gaussian3DParam<AutoDiffA<Double> >;
} //# NAMESPACE - END
