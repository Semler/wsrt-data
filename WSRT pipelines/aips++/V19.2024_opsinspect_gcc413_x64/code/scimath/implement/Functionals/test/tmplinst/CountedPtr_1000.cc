// CountedPtr_1000.cc -- Tue Apr  1 11:57:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Array<Float> > >;
template class CountedPtr<Block<Array<Float> > >;
template class PtrRep<Block<Array<Float> > >;
template class SimpleCountedConstPtr<Block<Array<Float> > >;
template class SimpleCountedPtr<Block<Array<Float> > >;
} //# NAMESPACE - END
