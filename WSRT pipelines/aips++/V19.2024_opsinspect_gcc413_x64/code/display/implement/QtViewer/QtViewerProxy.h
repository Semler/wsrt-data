//# QtViewerProxy: A proxy object to be used by external bindings
//#                 -- Functional level.
//# Copyright (C) 2005
//# Associated Universities, Inc. Washington DC, USA.
//#
//# This library is free software; you can redistribute it and/or modify it
//# under the terms of the GNU Library General Public License as published by
//# the Free Software Foundation; either version 2 of the License, or (at your
//# option) any later version.
//#
//# This library is distributed in the hope that it will be useful, but WITHOUT
//# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
//# License for more details.
//#
//# You should have received a copy of the GNU Library General Public License
//# along with this library; if not, write to the Free Software Foundation,
//# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//#
//# Correspondence concerning AIPS++ should be addressed as follows:
//#        Internet email: aips2-request@nrao.edu.
//#        Postal address: AIPS++ Project Office
//#                        National Radio Astronomy Observatory
//#                        520 Edgemont Road
//#                        Charlottesville, VA 22903-2475 USA
//#
//# $Id: QtViewerProxy.h,v 1.1 2007/02/09 00:27:40 mmarquar Exp $

#ifndef QTVIEWERPROXY_H
#define QTVIEWERPROXY_H

#include <casa/aips.h>
#include <casa/BasicSL/String.h>
#include <casa/Containers/Record.h>
#include <display/QtViewer/QtViewer.qo.h>
#include <display/QtViewer/QtDisplayPanelGui.qo.h>
#include <map>

namespace casa {

class QtDisplayData;

class QtViewerProxy {
 public:
  QtViewerProxy();
  ~QtViewerProxy();
  
  Int addDD(const String& filename, 
	    const String& datatype="image",
	    const String& displaytype="raster");

  void removeDD(Int ddno);

  //Vector<Int> getDDNumbers();
  
  Record getDDOptions(Int ddno);
  void setDDOptions(Int ddno, const Record& opts);
  
  Record getPanelOptions();
  void setPanelOptions();

 private:
  QtViewer* itsViewer;
  QtDisplayPanelGui* itsDPG;
  Int itsDDCounter;
  std::map<int, QtDisplayData*> itsDDMap;
};
 
}

#endif
