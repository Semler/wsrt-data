// X11PCColoredVectorGraphics_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <display/Display/X11PCColoredVectorGraphics.cc>
namespace casa { //# NAMESPACE - BEGIN
template void X11PixelCanvas_drawColoredLines(X11PixelCanvas *, Vector<Int> const &, Vector<Int> const &, Vector<Int> const &, Vector<Int> const &, Vector<uInt> const &);
template void X11PixelCanvas_drawColoredLines(X11PixelCanvas *, Vector<Float> const &, Vector<Float> const &, Vector<Float> const &, Vector<Float> const &, Vector<uInt> const &);
template void X11PixelCanvas_drawColoredLines(X11PixelCanvas *, Vector<Double> const &, Vector<Double> const &, Vector<Double> const &, Vector<Double> const &, Vector<uInt> const &);
template void X11PixelCanvas_drawColoredPoints(X11PixelCanvas *, Vector<Int> const &, Vector<Int> const &, Vector<uInt> const &);
template void X11PixelCanvas_drawColoredPoints(X11PixelCanvas *, Vector<Float> const &, Vector<Float> const &, Vector<uInt> const &);
template void X11PixelCanvas_drawColoredPoints(X11PixelCanvas *, Vector<Double> const &, Vector<Double> const &, Vector<uInt> const &);
} //# NAMESPACE - END
