// ArrayMath_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Quanta/Quantum.h>
namespace casa { //# NAMESPACE - BEGIN
template void operator+=(Array<Bool> &, Array<Bool> const &);
template void operator+=(Array<Quantity> &, Array<Quantity> const &);
template Array<uInt> operator+(Array<uInt> const &, Array<uInt> const &);
template Array<Int> operator-(Array<Int> const &, Int const &);
} //# NAMESPACE - END
