// Link_1030.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Containers/Link.cc>
#include <display/DisplayEvents/DisplayEH.h>
#include <display/DisplayEvents/WCMotionEH.h>
#include <display/DisplayEvents/WCPositionEH.h>
namespace casa { //# NAMESPACE - BEGIN
template class Link<DisplayEH *>;
template class Link<WCMotionEH *>;
template class Link<WCPositionEH *>;
} //# NAMESPACE - END
