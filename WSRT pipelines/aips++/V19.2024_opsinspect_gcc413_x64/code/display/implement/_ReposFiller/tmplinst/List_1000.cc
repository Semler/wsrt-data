// List_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Containers/List.cc>
#include <display/Display/WorldCanvas.h>
#include <display/Display/WorldCanvasHolder.h>
#include <display/Display/MultiWCHolder.h>
#include <display/DisplayDatas/DisplayData.h>
#include <display/DisplayDatas/PrincipalAxesDD.h>
#include <display/DisplayShapes/DSClosed.h>
#include <display/DisplayShapes/DisplayShape.h>
namespace casa { //# NAMESPACE - BEGIN
template class List<WorldCanvas *>;
template class ListNotice<WorldCanvas *>;
template class ListIter<WorldCanvas *>;
template class ConstListIter<WorldCanvas *>;
template class List<WorldCanvasHolder *>;
template class ListNotice<WorldCanvasHolder *>;
template class ListIter<WorldCanvasHolder *>;
template class ConstListIter<WorldCanvasHolder *>;
template class List<MultiWCHolder *>;
template class ListNotice<MultiWCHolder *>;
template class ListIter<MultiWCHolder *>;
template class ConstListIter<MultiWCHolder *>;
template class List<DisplayData *>;
template class ListNotice<DisplayData *>;
template class ListIter<DisplayData *>;
template class ConstListIter<DisplayData *>;
} //# NAMESPACE - END
