// AttValPoi_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <display/Display/AttValPoi.cc>
#include <casa/Quanta/QLogical.h>
namespace casa { //# NAMESPACE - BEGIN
template class AttributeValuePoi<uInt>;
template class AttributeValuePoi<Int>;
template class AttributeValuePoi<Bool>;
template class AttributeValuePoi<Float>;
template class AttributeValuePoi<Double>;
template class AttributeValuePoi<String>;
template class AttributeValuePoi<Quantity>;
} //# NAMESPACE - END
