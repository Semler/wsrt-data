// X11PCVectorGraphics_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <display/Display/X11PCVectorGraphics.cc>
namespace casa { //# NAMESPACE - BEGIN
template void X11PixelCanvas_drawLines(X11PixelCanvas *, Vector<Int> const &, Vector<Int> const &, Vector<Int> const &, Vector<Int> const &);
template void X11PixelCanvas_drawLines(X11PixelCanvas *, Vector<Float> const &, Vector<Float> const &, Vector<Float> const &, Vector<Float> const &);
template void X11PixelCanvas_drawLines(X11PixelCanvas *, Vector<Double> const &, Vector<Double> const &, Vector<Double> const &, Vector<Double> const &);
template void X11PixelCanvas_drawLines(X11PixelCanvas *, Matrix<Int> const &);
template void X11PixelCanvas_drawLines(X11PixelCanvas *, Matrix<Float> const &);
template void X11PixelCanvas_drawLines(X11PixelCanvas *, Matrix<Double> const &);
template void X11PixelCanvas_drawPoints(X11PixelCanvas *, Vector<Int> const &, Vector<Int> const &);
template void X11PixelCanvas_drawPoints(X11PixelCanvas *, Vector<Float> const &, Vector<Float> const &);
template void X11PixelCanvas_drawPoints(X11PixelCanvas *, Vector<Double> const &, Vector<Double> const &);
template void X11PixelCanvas_drawPoints(X11PixelCanvas *, Matrix<Int> const &);
template void X11PixelCanvas_drawPoints(X11PixelCanvas *, Matrix<Float> const &);
template void X11PixelCanvas_drawPoints(X11PixelCanvas *, Matrix<Double> const &);
template void X11PixelCanvas_drawPolyline(X11PixelCanvas *, Matrix<Int> const &);
template void X11PixelCanvas_drawPolyline(X11PixelCanvas *, Matrix<Float> const &);
template void X11PixelCanvas_drawPolyline(X11PixelCanvas *, Matrix<Double> const &);
template void X11PixelCanvas_drawPolyline(X11PixelCanvas *, Vector<Int> const &, Vector<Int> const &);
template void X11PixelCanvas_drawPolyline(X11PixelCanvas *, Vector<Float> const &, Vector<Float> const &);
template void X11PixelCanvas_drawPolyline(X11PixelCanvas *, Vector<Double> const &, Vector<Double> const &);
template void X11PixelCanvas_drawFilledPolygon(X11PixelCanvas *, Vector<Int> const &, Vector<Int> const &);
template void X11PixelCanvas_drawFilledPolygon(X11PixelCanvas *, Vector<Float> const &, Vector<Float> const &);
template void X11PixelCanvas_drawFilledPolygon(X11PixelCanvas *, Vector<Double> const &, Vector<Double> const &);
template void X11PixelCanvas_drawFilledRectangle(X11PixelCanvas *, Int const &, Int const &, Int const &, Int const &);
template void X11PixelCanvas_drawFilledRectangle(X11PixelCanvas *, Float const &, Float const &, Float const &, Float const &);
template void X11PixelCanvas_drawFilledRectangle(X11PixelCanvas *, Double const &, Double const &, Double const &, Double const &);
} //# NAMESPACE - END
