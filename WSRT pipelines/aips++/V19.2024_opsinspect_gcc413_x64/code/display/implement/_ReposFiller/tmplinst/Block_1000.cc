// Block_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Containers/Block.h>
#include <display/X11PixelCanvas/X11PCDisplayListObject.h>
#include <display/Display/Attribute.h>
#include <display/DisplayShapes/DisplayShape.h>
namespace casa { //# NAMESPACE - BEGIN
template class PtrBlock<PtrBlock<X11PCDisplayListObject *> *>;
template class PtrBlock<PtrBlock<DisplayShape*> *>;
template class PtrBlock<X11PCDisplayListObject *>;
template class PtrBlock<Attribute *>;
} //# NAMESPACE - END
