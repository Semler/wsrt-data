// CountedPtr_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <display/DisplayDatas/DDDHandle.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<DDDHandle> >;
template class CountedPtr<Block<DDDHandle> >;
template class PtrRep<Block<DDDHandle> >;
template class SimpleCountedConstPtr<Block<DDDHandle> >;
template class SimpleCountedPtr<Block<DDDHandle> >;
} //# NAMESPACE - END
