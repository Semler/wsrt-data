// X11PCColTblTemplates_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <display/Display/X11PCColTblTemplates.cc>
namespace casa { //# NAMESPACE - BEGIN
template void X11PixelCanvasColorTable_mapToColor(uLong *, uInt, uInt, Array<uChar> &, Bool);
template void X11PixelCanvasColorTable_mapToColor(uLong *, uInt, uInt, Array<uInt> &, Bool);
template void X11PixelCanvasColorTable_mapToColor(uLong *, uInt, uInt, Array<uLong> &, Bool);
template void X11PixelCanvasColorTable_mapToColor(uLong *, uInt, uInt, Array<uShort> &, Bool);
} //# NAMESPACE - END
