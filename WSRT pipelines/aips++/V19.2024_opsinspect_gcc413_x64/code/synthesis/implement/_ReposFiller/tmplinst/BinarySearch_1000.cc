// BinarySearch_1000.cc -- Tue Apr  1 12:27:41 BST 2008 -- renting
#include <casa/Utilities/BinarySearch.cc>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template Int binarySearch(Bool &, Vector<Double> const &, Double const &, uInt, Int);
} //# NAMESPACE - END
