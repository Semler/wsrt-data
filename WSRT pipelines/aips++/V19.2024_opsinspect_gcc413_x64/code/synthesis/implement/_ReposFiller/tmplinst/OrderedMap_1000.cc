// OrderedMap_1000.cc -- Tue Apr  1 12:27:41 BST 2008 -- renting
#include <casa/Containers/OrderedMap.cc>
#include <synthesis/Parallel/Algorithm.h>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedMapRep<Int, Algorithm *>;
template class OrderedMapIterRep<Int, Algorithm *>;
template class OrderedMap<Int, Algorithm *>;
template class OrderedMapNotice<Int, Algorithm *>;
} //# NAMESPACE - END
