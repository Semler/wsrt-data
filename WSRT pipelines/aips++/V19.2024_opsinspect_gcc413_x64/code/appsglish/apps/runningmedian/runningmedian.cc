//# runningmedian.cc: Calculate running medians of an image
//# Copyright (C) 2006
//# Associated Universities, Inc. Washington DC, USA.
//#
//# This program is free software; you can redistribute it and/or modify it
//# under the terms of the GNU General Public License as published by the Free
//# Software Foundation; either version 2 of the License, or(at your option)
//# any later version.
//#
//# This program is distributed in the hope that it will be useful, but WITHOUT
//# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//# more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with this program; if not, write to the Free Software Foundation, Inc.,
//# 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//#
//# Correspondence concerning AIPS++ should be addressed as follows:
//#        Internet email: aips2-request@nrao.edu.
//#        Postal address: AIPS++ Project Office
//#                        National Radio Astronomy Observatory
//#                        520 Edgemont Road
//#                        Charlottesville, VA 22903-2475 USA
//#
//# $Id: runningmedian.cc,v 1.1 2007/08/02 22:24:34 gvandiep Exp $

#include <images/Images/ImageUtilities.h>
#include <images/Images/MIRIADImage.h>
#include <images/Images/FITSImage.h>
#include <images/Images/PagedImage.h>
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/MaskArrMath.cc>
#include <casa/Arrays/ArrayUtil.h>
#include <iostream>

namespace casa {
  template Array<Float> slidingArrayMath(const Array<Float>&,
					 const IPosition&,
					 Float (*) (const Array<Float>&),
					 Bool);
  template Array<Float> slidingArrayMath(const MaskedArray<Float>&,
					 const IPosition&,
					 Float (*) (const MaskedArray<Float>&),
					 Bool);
}

using namespace casa;
using namespace std;


void doIt (const String& in, const String& out, const IPosition& halfBoxSize,
	   Bool useMask)
{
  FITSImage::registerOpenFunction();
  MIRIADImage::registerOpenFunction();
  cout << "Creating " << out << " from running medians of " << in
       << " box half-width " << halfBoxSize << endl;
  LogIO logio;
  ImageInterface<Float>* img=0;
  ImageUtilities::openImage (img, in, logio);
  PagedImage<Float> pOut(img->shape(), img->coordinates(), out);
  if (useMask  &&  img->isMasked()) {
    cout << "A mask is being used" << endl;
    MaskedArray<Float> marr(img->get(), img->getMask());
    pOut.put (slidingArrayMath(marr, halfBoxSize, casa::median));
  } else {
    pOut.put (slidingArrayMath(img->get(), halfBoxSize, casa::median));
  }
  delete img;
}

int main(int argc, const char* argv[])
{
  if (argc < 4) {
    cout << " runningmedian calculates the running medians of an image and creates" << endl;
    cout << " a new image from it. Thereafter LEL can be used to subtract it." << endl;
    cout << " The box of the running median can be multi-dimensional (e.g. RA,DEC)" << endl;
    cout << " and its halfsize must be given like 10,10 (no spaces)." << endl;
    cout << " (Thus in this example the full box size is 21,21)" << endl;
    cout << endl;
    cout << " The input image can be any image type (AIPS++, FITS, Miriad)." << endl;
    cout << " The output is always an AIPS++ image." << endl;

    cout << "Run as: runningmedian namein nameout halfboxsize [usemask]"
	 << endl;
    return 1;
  }
  // Convert the command line argument to shapes.
  Vector<String> boxV (stringToVector (argv[3]));
  uInt nrdim = boxV.nelements();
  IPosition box(nrdim);
  for (uInt i=0; i<nrdim; i++) {
    istringstream istr(boxV(i).chars());
    istr >> box(i);
    if (box(i) < 0) {
      cout << "Halfboxsize " << box(i) << " must be >= 0" << endl;
      return 1;
    }
  }
  Bool useMask = True;
  if (argc > 4) {
    useMask = String(argv[4]) == "1";
  }
  try {
    doIt (argv[1], argv[2], box, useMask);
  } catch (exception& x) {
    cout << "Unexpected exception: " << x.what() << endl;
    return 1;
  }
  return 0;
}
