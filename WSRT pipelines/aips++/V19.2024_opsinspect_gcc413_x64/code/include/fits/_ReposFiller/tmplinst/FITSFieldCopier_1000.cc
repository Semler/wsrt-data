// FITSFieldCopier_1000.cc -- Tue Apr  1 12:14:23 BST 2008 -- renting
#include <fits/FITS/FITSFieldCopier.h>
namespace casa { //# NAMESPACE - BEGIN
template class ArrayFITSFieldCopier<Bool, FitsLogical>;
template class ArrayFITSFieldCopier<Complex, Complex>;
template class ArrayFITSFieldCopier<DComplex, DComplex>;
template class ArrayFITSFieldCopier<Double, Double>;
template class ArrayFITSFieldCopier<Float, Float>;
template class ArrayFITSFieldCopier<Int, FitsLong>;
template class ArrayFITSFieldCopier<Short, Short>;
template class ArrayFITSFieldCopier<uChar, uChar>;
} //# NAMESPACE - END
