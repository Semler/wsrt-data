// MeasBase_1060.cc -- Tue Apr  1 12:12:29 BST 2008 -- renting
#include <measures/Measures/MeasBase.cc>
#include <casa/Quanta/MVPosition.h>
#include <measures/Measures/MPosition.h>
#include <measures/Measures/MeasRef.h>
namespace casa { //# NAMESPACE - BEGIN
template class MeasBase<MVPosition, MeasRef<MPosition> >;
} //# NAMESPACE - END
