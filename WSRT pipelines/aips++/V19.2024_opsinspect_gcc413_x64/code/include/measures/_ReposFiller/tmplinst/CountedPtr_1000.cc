// CountedPtr_1000.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <measures/Measures/MeasureHolder.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<MeasureHolder> >;
template class CountedPtr<Block<MeasureHolder> >;
template class PtrRep<Block<MeasureHolder> >;
template class SimpleCountedConstPtr<Block<MeasureHolder> >;
template class SimpleCountedPtr<Block<MeasureHolder> >;
} //# NAMESPACE - END
