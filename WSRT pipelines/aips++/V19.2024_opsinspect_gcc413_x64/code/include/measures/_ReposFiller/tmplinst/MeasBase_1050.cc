// MeasBase_1050.cc -- Tue Apr  1 12:12:29 BST 2008 -- renting
#include <measures/Measures/MeasBase.cc>
#include <casa/Quanta/MVFrequency.h>
#include <measures/Measures/MFrequency.h>
#include <measures/Measures/MeasRef.h>
namespace casa { //# NAMESPACE - BEGIN
template class MeasBase<MVFrequency, MeasRef<MFrequency> >;
} //# NAMESPACE - END
