// MeasBase_1040.cc -- Tue Apr  1 12:12:29 BST 2008 -- renting
#include <measures/Measures/MeasBase.cc>
#include <casa/Quanta/MVEpoch.h>
#include <measures/Measures/MEpoch.h>
#include <measures/Measures/MeasRef.h>
namespace casa { //# NAMESPACE - BEGIN
template class MeasBase<MVEpoch, MeasRef<MEpoch> >;
} //# NAMESPACE - END
