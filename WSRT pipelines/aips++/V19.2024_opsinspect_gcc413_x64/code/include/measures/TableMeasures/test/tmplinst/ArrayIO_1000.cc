// ArrayIO_1000.cc -- Tue Apr  1 12:13:35 BST 2008 -- renting
#include <casa/Arrays/ArrayIO.cc>
#include <casa/Quanta/Quantum.h>
#include <measures/Measures/MEpoch.h>
#include <measures/Measures/MFrequency.h>
namespace casa { //# NAMESPACE - BEGIN
template ostream & operator<<(ostream &, Array<MEpoch> const &);
template ostream & operator<<(ostream &, Array<MFrequency> const &);
} //# NAMESPACE - END
