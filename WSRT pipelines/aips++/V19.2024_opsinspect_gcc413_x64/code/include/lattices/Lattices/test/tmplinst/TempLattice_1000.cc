// TempLattice_1000.cc -- Tue Apr  1 12:15:01 BST 2008 -- renting
#include <lattices/Lattices/TempLattice.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/Lattice.h>
namespace casa { //# NAMESPACE - BEGIN
template class TempLattice<DComplex>;
template class CountedConstPtr<Lattice<DComplex> >;
template class CountedPtr<Lattice<DComplex> >;
template class PtrRep<Lattice<DComplex> >;
template class SimpleCountedConstPtr<Lattice<DComplex> >;
template class SimpleCountedPtr<Lattice<DComplex> >;
} //# NAMESPACE - END
