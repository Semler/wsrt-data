// CountedPtr_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/BasicSL/Complex.h>
#include <lattices/Lattices/Lattice.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Lattice<Complex> >;
template class CountedPtr<Lattice<Complex> >;
template class PtrRep<Lattice<Complex> >;
template class SimpleCountedConstPtr<Lattice<Complex> >;
template class SimpleCountedPtr<Lattice<Complex> >;
} //# NAMESPACE - END
