// CountedPtr_1040.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/Lattice.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Lattice<Float> >;
template class CountedConstPtr<Lattice<Float> >;
template class SimpleCountedPtr<Lattice<Float> >;
template class SimpleCountedConstPtr<Lattice<Float> >;
template class PtrRep<Lattice<Float> >;
} //# NAMESPACE - END
