// PagedArray_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/PagedArray.cc>
#include <lattices/Lattices/PagedArrIter.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class PagedArray<Complex>;
template class PagedArrIter<Complex>;
} //# NAMESPACE - END
