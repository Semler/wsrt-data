// TempLattice_1020.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/TempLattice.cc>
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/Lattice.h>
namespace casa { //# NAMESPACE - BEGIN
template class TempLattice<Double>;
template class CountedConstPtr<Lattice<Double> >;
template class CountedPtr<Lattice<Double> >;
template class PtrRep<Lattice<Double> >;
template class SimpleCountedConstPtr<Lattice<Double> >;
template class SimpleCountedPtr<Lattice<Double> >;
} //# NAMESPACE - END
