// MaskedLatticeIterator_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/MaskedLatticeIterator.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class RO_MaskedLatticeIterator<Float>;
template class RO_MaskedLatticeIterator<Double>;
template class RO_MaskedLatticeIterator<Complex>;
template class RO_MaskedLatticeIterator<DComplex>;
template class RO_MaskedLatticeIterator<Bool>;
} //# NAMESPACE - END
