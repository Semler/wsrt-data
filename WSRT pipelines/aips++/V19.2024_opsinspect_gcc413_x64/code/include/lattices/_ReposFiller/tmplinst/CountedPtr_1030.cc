// CountedPtr_1030.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/LELLattCoordBase.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<LELLattCoordBase>;
template class CountedPtr<LELLattCoordBase>;
template class PtrRep<LELLattCoordBase>;
template class SimpleCountedConstPtr<LELLattCoordBase>;
template class SimpleCountedPtr<LELLattCoordBase>;
} //# NAMESPACE - END
