// CountedPtr_1030.cc -- Tue Apr  1 12:40:17 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <nrao/SDD/SDD12mOnLine.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<SDD12mOnLine>;
template class CountedPtr<SDD12mOnLine>;
template class PtrRep<SDD12mOnLine>;
template class SimpleCountedConstPtr<SDD12mOnLine>;
template class SimpleCountedPtr<SDD12mOnLine>;
} //# NAMESPACE - END
