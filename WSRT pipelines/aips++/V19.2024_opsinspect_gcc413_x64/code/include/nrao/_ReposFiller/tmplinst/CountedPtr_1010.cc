// CountedPtr_1010.cc -- Tue Apr  1 12:40:17 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <nrao/VLA/VLAArchiveInput.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<VLAArchiveInput>;
template class CountedConstPtr<VLAArchiveInput>;
template class SimpleCountedPtr<VLAArchiveInput>;
template class SimpleCountedConstPtr<VLAArchiveInput>;
template class PtrRep<VLAArchiveInput>;
} //# NAMESPACE - END
