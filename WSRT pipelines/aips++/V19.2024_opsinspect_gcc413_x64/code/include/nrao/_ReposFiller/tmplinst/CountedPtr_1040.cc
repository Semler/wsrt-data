// CountedPtr_1040.cc -- Tue Apr  1 12:40:17 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <nrao/SDD/SDDOnLine.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<SDDOnLine>;
template class CountedPtr<SDDOnLine>;
template class PtrRep<SDDOnLine>;
template class SimpleCountedConstPtr<SDDOnLine>;
template class SimpleCountedPtr<SDDOnLine>;
} //# NAMESPACE - END
