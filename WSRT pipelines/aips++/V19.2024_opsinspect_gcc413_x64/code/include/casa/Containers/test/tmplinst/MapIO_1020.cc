// MapIO_1020.cc -- Tue Apr  1 11:49:34 BST 2008 -- renting
#include <casa/Containers/MapIO.cc>
#include <casa/Containers/OrderedPair.h>
#include <casa/Containers/OrdPairIO.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template ostream & operator<<(ostream &, Map<String, OrderedPair<String, uInt> > const &);
} //# NAMESPACE - END
