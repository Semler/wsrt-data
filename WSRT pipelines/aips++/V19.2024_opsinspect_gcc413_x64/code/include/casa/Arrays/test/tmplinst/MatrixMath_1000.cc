// MatrixMath_1000.cc -- Tue Apr  1 11:49:18 BST 2008 -- renting
#include <casa/Arrays/MatrixMath.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Matrix<Complex> product(Vector<Complex> const &, Matrix<Complex> const &);
template Matrix<DComplex> product(Matrix<DComplex> const &, Matrix<DComplex> const &);
template Matrix<DComplex> product(Vector<DComplex> const &, Matrix<DComplex> const &);
template Matrix<Double> product(Matrix<Double> const &, Matrix<Double> const &);
template Matrix<Double> product(Vector<Double> const &, Matrix<Double> const &);
template Matrix<Float> product(Matrix<Float> const &, Matrix<Float> const &);
template Matrix<Float> product(Vector<Float> const &, Matrix<Float> const &);
template Matrix<Int> directProduct(Matrix<Int> const &, Matrix<Int> const &);
template Matrix<Int> product(Matrix<Int> const &, Matrix<Int> const &);
template Matrix<Int> product(Vector<Int> const &, Matrix<Int> const &);
template Vector<Complex> crossProduct(Vector<Complex> const &, Vector<Complex> const &);
template Vector<Complex> product(Matrix<Complex> const &, Vector<Complex> const &);
template Vector<DComplex> crossProduct(Vector<DComplex> const &, Vector<DComplex> const &);
template Vector<DComplex> product(Matrix<DComplex> const &, Vector<DComplex> const &);
template Vector<Double> crossProduct(Vector<Double> const &, Vector<Double> const &);
template Vector<Float> crossProduct(Vector<Float> const &, Vector<Float> const &);
template Vector<Int> crossProduct(Vector<Int> const &, Vector<Int> const &);
template Vector<Int> product(Matrix<Int> const &, Vector<Int> const &);
} //# NAMESPACE - END
