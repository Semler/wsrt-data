// ArrayMath_1000.cc -- Tue Apr  1 11:49:18 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<Int> cube(Array<Int> const &);
template Array<Int> operator-(Array<Int> const &, Array<Int> const &);
template Array<Int> operator-(Array<Int> const &, Int const &);
template Array<Int> square(Array<Int> const &);
template Int mean(Array<Int> const &);
template Int median(Array<Int> const &, Bool);
template Int median(Array<Int> const &, Bool, Bool, Bool);
} //# NAMESPACE - END
