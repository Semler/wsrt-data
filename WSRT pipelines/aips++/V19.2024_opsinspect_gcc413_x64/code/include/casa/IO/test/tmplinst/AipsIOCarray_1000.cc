// AipsIOCarray_1000.cc -- Tue Apr  1 11:49:53 BST 2008 -- renting
#include <casa/IO/AipsIOCarray.cc>
namespace casa { //# NAMESPACE - BEGIN
#if !defined(AIPS_STDLIB)
template void putAipsIO(AipsIO &, uInt, uInt const *);
template void getAipsIO(AipsIO &, uInt, uInt *);
template void getnewAipsIO(AipsIO &, uInt &, uInt **);
#endif
} //# NAMESPACE - END
