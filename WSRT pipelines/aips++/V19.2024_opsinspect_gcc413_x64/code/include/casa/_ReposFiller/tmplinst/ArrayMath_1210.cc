// ArrayMath_1210.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<Double> partialMins(Array<Double> const &, IPosition const &);
template Array<Double> partialMaxs(Array<Double> const &, IPosition const &);
template Array<Double> partialSums(Array<Double> const &, IPosition const &);
template Array<Double> partialProducts(Array<Double> const &, IPosition const &);
template Array<Double> partialMeans(Array<Double> const &, IPosition const &);
template Array<Double> partialVariances(Array<Double> const &, IPosition const &, Array<Double> const &);
template Array<Double> partialAvdevs(Array<Double> const &, IPosition const &, Array<Double> const &);
template Array<Double> partialRmss(Array<Double> const &, IPosition const &);
template Array<Double> partialMedians(Array<Double> const &, IPosition const &, Bool, Bool);
template Array<Double> partialFractiles(Array<Double> const &, IPosition const &, Float, Bool);
template Array<DComplex> partialSums(Array<DComplex> const &, IPosition const &);
template Array<DComplex> partialProducts(Array<DComplex> const &, IPosition const &);
} //# NAMESPACE - END
