// CountedPtr_1230.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<String> >;
template class CountedPtr<Block<String> >;
template class PtrRep<Block<String> >;
template class SimpleCountedConstPtr<Block<String> >;
template class SimpleCountedPtr<Block<String> >;
} //# NAMESPACE - END
