// MaskedArray_1050.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/MaskedArray.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template class MaskedArray<Bool>;
template class MaskedArray<uChar>;
template class MaskedArray<Short>;
template class MaskedArray<uShort>;
template class MaskedArray<Int>;
template class MaskedArray<uInt>;
template class MaskedArray<Float>;
template class MaskedArray<Double>;
template class MaskedArray<Complex>;
template class MaskedArray<DComplex>;
template class MaskedArray<String>;
} //# NAMESPACE - END
