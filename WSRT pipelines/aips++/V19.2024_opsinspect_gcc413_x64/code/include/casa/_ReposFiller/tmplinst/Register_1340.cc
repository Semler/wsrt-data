// Register_1340.cc -- Tue Apr  1 11:50:47 BST 2008 -- renting
#include <casa/Utilities/Register.cc>
namespace casa { //# NAMESPACE - BEGIN
template <class Qtype> class Quantum;
template <class T> class Matrix;
template uInt Register(Quantum<Matrix<Double> > const *);
} //# NAMESPACE - END
