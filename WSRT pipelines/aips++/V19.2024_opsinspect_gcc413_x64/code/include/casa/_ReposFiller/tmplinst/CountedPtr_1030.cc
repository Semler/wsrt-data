// CountedPtr_1030.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Quanta/Unit.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<Unit> >;
template class CountedConstPtr<Block<Unit> >;
template class PtrRep<Block<Unit> >;
template class SimpleCountedPtr<Block<Unit> >;
template class SimpleCountedConstPtr<Block<Unit> >;
} //# NAMESPACE - END
