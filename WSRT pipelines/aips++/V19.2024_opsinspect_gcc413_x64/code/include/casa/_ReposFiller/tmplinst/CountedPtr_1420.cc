// CountedPtr_1420.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/RecordRep.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<RecordRep>;
template class CountedPtr<RecordRep>;
template class PtrRep<RecordRep>;
template class SimpleCountedConstPtr<RecordRep>;
template class SimpleCountedPtr<RecordRep>;
} //# NAMESPACE - END
