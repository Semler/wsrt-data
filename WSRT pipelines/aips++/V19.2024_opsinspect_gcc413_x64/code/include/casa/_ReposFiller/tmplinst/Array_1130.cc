// Array_1130.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <casa/Quanta/Quantum.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<Quantum<Float> >;
#ifdef AIPS_SUN_NATIVE
template class Array<Quantum<Float> >::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
