// BinarySearch_1040.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/BinarySearch.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template Int binarySearchBrackets(Bool &, Block<uInt> const &, uInt const &, uInt, Int);
} //# NAMESPACE - END
