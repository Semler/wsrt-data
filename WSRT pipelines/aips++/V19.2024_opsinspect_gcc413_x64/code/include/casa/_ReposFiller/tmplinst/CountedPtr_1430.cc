// CountedPtr_1430.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/IO/ByteIO.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<ByteIO>;
template class CountedConstPtr<ByteIO>;
template class SimpleCountedPtr<ByteIO>;
template class SimpleCountedConstPtr<ByteIO>;
template class PtrRep<ByteIO>;
} //# NAMESPACE - END
