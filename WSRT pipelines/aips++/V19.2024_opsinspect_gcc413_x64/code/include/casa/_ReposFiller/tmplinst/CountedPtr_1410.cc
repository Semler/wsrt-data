// CountedPtr_1410.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/RecordDescRep.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<RecordDescRep>;
template class CountedPtr<RecordDescRep>;
template class PtrRep<RecordDescRep>;
template class SimpleCountedConstPtr<RecordDescRep>;
template class SimpleCountedPtr<RecordDescRep>;
} //# NAMESPACE - END
