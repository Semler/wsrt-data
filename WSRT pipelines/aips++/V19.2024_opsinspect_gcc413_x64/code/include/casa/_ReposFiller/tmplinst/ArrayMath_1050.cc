// ArrayMath_1050.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Float product(Array<Float> const &);;
template Array<Bool> operator*(Array<Bool> const &, Array<Bool> const &);
} //# NAMESPACE - END
