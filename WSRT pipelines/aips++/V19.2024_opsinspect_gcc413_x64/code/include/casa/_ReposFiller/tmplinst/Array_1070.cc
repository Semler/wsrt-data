// Array_1070.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <casa/Quanta/MVDirection.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<MVDirection>;
#ifdef AIPS_SUN_NATIVE
template class Array<MVDirection>::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
