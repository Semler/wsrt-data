// CountedPtr_1320.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Char> >;
template class CountedPtr<Block<Char> >;
template class PtrRep<Block<Char> >;
template class SimpleCountedConstPtr<Block<Char> >;
template class SimpleCountedPtr<Block<Char> >;
} //# NAMESPACE - END
