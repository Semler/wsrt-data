// CountedPtr_1330.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Double> >;
template class CountedPtr<Block<Double> >;
template class PtrRep<Block<Double> >;
template class SimpleCountedConstPtr<Block<Double> >;
template class SimpleCountedPtr<Block<Double> >;
} //# NAMESPACE - END
