// CountedPtr_1170.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Arrays/Vector.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Vector<Complex> > >;
template class CountedPtr<Block<Vector<Complex> > >;
template class PtrRep<Block<Vector<Complex> > >;
template class SimpleCountedConstPtr<Block<Vector<Complex> > >;
template class SimpleCountedPtr<Block<Vector<Complex> > >;
} //# NAMESPACE - END
