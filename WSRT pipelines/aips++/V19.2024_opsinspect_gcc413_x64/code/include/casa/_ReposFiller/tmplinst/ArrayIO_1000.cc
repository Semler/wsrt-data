// ArrayIO_1000.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayIO.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template AipsIO & operator<<(AipsIO &, Array<Bool> const &);
template AipsIO & operator<<(AipsIO &, Array<uChar> const &);
template AipsIO & operator<<(AipsIO &, Array<Short> const &);
template AipsIO & operator<<(AipsIO &, Array<Int> const &);
template AipsIO & operator<<(AipsIO &, Array<uInt> const &);
template AipsIO & operator<<(AipsIO &, Array<Float> const &);
template AipsIO & operator<<(AipsIO &, Array<Double> const &);
template AipsIO & operator<<(AipsIO &, Array<Complex> const &);
template AipsIO & operator<<(AipsIO &, Array<DComplex> const &);
template AipsIO & operator<<(AipsIO &, Array<String> const &);
template void putArray(AipsIO &, Array<Bool> const &, Char const *);
template void putArray(AipsIO &, Array<uChar> const &, Char const *);
template void putArray(AipsIO &, Array<Short> const &, Char const *);
template void putArray(AipsIO &, Array<Int> const &, Char const *);
template void putArray(AipsIO &, Array<uInt> const &, Char const *);
template void putArray(AipsIO &, Array<Float> const &, Char const *);
template void putArray(AipsIO &, Array<Double> const &, Char const *);
template void putArray(AipsIO &, Array<Complex> const &, Char const *);
template void putArray(AipsIO &, Array<DComplex> const &, Char const *);
template void putArray(AipsIO &, Array<String> const &, Char const *);
} //# NAMESPACE - END
