// ArrayMath_1150.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Short max(Array<Short> const &);
template Short min(Array<Short> const &);
template void indgen(Array<Short> &);
template void indgen(Array<Short> &, Short, Short);
template void minMax(Short &, Short &, Array<Short> const &);
template void operator+=(Array<Short> &, Short const &);
template void operator-=(Array<Short> &, Short const &);
} //# NAMESPACE - END
