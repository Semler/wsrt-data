// LinearSearch_1000.cc -- Tue Apr  1 11:50:47 BST 2008 -- renting
#include <casa/Utilities/LinearSearch.cc>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template Int linearSearch(Bool &, Vector<Int> const &, Int const &, uInt, uInt);
template Int linearSearch(Bool &, Vector<uInt> const &, uInt const &, uInt, uInt);
} //# NAMESPACE - END
