// CountedPtr_1240.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Quanta/MVDirection.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<MVDirection> >;
template class CountedPtr<Block<MVDirection> >;
template class PtrRep<Block<MVDirection> >;
template class SimpleCountedConstPtr<Block<MVDirection> >;
template class SimpleCountedPtr<Block<MVDirection> >;
} //# NAMESPACE - END
