// ArrayMath_1180.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template void convertArray(Array<Double> &, Array<Double> const &);
template void convertArray(Array<Double> &, Array<Float> const &);
template void convertArray(Array<Double> &, Array<Int> const &);
template void convertArray(Array<Double> &, Array<Short> const &);
template void convertArray(Array<Double> &, Array<uChar> const &);
template void convertArray(Array<Double> &, Array<uInt> const &);
template void convertArray(Array<Double> &, Array<uShort> const &);
template void convertArray(Array<Float> &, Array<Double> const &);
template void convertArray(Array<Float> &, Array<Int> const &);
template void convertArray(Array<Float> &, Array<Short> const &);
template void convertArray(Array<Float> &, Array<uShort> const & );
template void convertArray(Array<Float> &, Array<uChar> const &);
template void convertArray(Array<Float> &, Array<uInt> const &);
template void convertArray(Array<Int> &, Array<Short> const &);
template void convertArray(Array<Int> &, Array<uChar> const &);
template void convertArray(Array<Int> &, Array<uInt> const &);
template void convertArray(Array<Int> &, Array<uShort> const &);
template void convertArray(Array<Int> &, Array<Bool> const &);
template void convertArray(Array<Int> &, Array<Float> const &);
template void convertArray(Array<Int> &, Array<Double> const &);
template void convertArray(Array<Short> &, Array<Char> const &);
template void convertArray(Array<Short> &, Array<Double> const &);
template void convertArray(Array<Short> &, Array<Float> const &);
template void convertArray(Array<Short> &, Array<Int> const &);
template void convertArray(Array<Short> &, Array<Long> const &);
template void convertArray(Array<Short> &, Array<Short> const &);
template void convertArray(Array<Short> &, Array<uChar> const &);
template void convertArray(Array<Short> &, Array<uInt> const &);
template void convertArray(Array<Short> &, Array<uLong> const &);
template void convertArray(Array<Short> &, Array<uShort> const &);
template void convertArray(Array<uChar> &, Array<Int> const &);
template void convertArray(Array<uChar> &, Array<Short> const &);
template void convertArray(Array<uChar> &, Array<Float> const &);
template void convertArray(Array<uChar> &, Array<Double> const &);
template void convertArray(Array<uInt> &, Array<Int> const &);
template void convertArray(Array<uInt> &, Array<Short> const &);
template void convertArray(Array<uInt> &, Array<uChar> const &);
template void convertArray(Array<uInt> &, Array<Float> const &);
template void convertArray(Array<uInt> &, Array<Double> const &);
template void convertArray(Array<uShort> &, Array<uChar> const &);
template void convertArray(Array<uShort> &, Array<Short> const &);
template void convertArray(Array<uShort> &, Array<Int> const &);
template void convertArray(Array<uShort> &, Array<Float> const &);
template void convertArray(Array<uShort> &, Array<Double> const &);
template void convertArray(Array<Bool> &, Array<uChar> const &);
template void convertArray(Array<Bool> &, Array<Short> const &);
template void convertArray(Array<Bool> &, Array<Int> const &);
template void convertArray(Array<Bool> &, Array<Float> const &);
template void convertArray(Array<Bool> &, Array<Double> const &);
} //# NAMESPACE - END
