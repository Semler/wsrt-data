// ArrayLogical_1060.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allEQ(Array<Int> const &, Array<Int> const &);
template Bool allEQ(Array<Int> const &, Int const &);
template Bool allGE(Array<Int> const &, Int const &);
template Bool allLT(Array<Int> const &, Int const &);
template Bool anyEQ(Array<Int> const &, Int const &);
template Bool anyGE(Array<Int> const &, Int const &);
template Bool anyGT(Array<Int> const &, Int const &);
template Bool anyLE(Array<Int> const &, Int const &);
template Bool anyLT(Array<Int> const &, Int const &);
template Bool anyNE(Array<Int> const &, Int const &);
template Bool anyNE(Array<Int> const &, Array<Int> const &);
template Array<Bool> operator!=(Array<Int> const &, Int const &);
template Array<Bool> operator==(Array<Int> const &, Int const &);
template Array<Bool> operator>(Array<Int> const &, Int const &);
template Array<Bool> operator<(Array<Int> const &, Int const &);
template Array<Bool> operator>=(Array<Int> const &, Int const &);
template Array<Bool> operator<=(Array<Int> const &, Int const &);
} //# NAMESPACE - END
