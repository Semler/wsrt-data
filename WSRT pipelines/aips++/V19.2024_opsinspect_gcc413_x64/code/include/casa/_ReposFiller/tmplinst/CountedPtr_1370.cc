// CountedPtr_1370.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<uChar> >;
template class CountedPtr<Block<uChar> >;
template class PtrRep<Block<uChar> >;
template class SimpleCountedConstPtr<Block<uChar> >;
template class SimpleCountedPtr<Block<uChar> >;
} //# NAMESPACE - END
