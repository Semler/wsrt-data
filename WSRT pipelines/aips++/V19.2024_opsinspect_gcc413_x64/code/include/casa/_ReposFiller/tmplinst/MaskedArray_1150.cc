// MaskedArray_1150.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/MaskedArray.cc>
#include <casa/Utilities/CountedPtr.h>
#include <casa/BasicMath/Random.h>
#include <casa/Arrays/Matrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class MaskedArray<Matrix<CountedPtr<Random> > >;
} //# NAMESPACE - END
