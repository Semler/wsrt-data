// OrderedPair_1150.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/OrderedPair.cc>
#include <casa/Quanta/Quantum.h>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedPair<Int, Quantum<Array<Double> > >;
} //# NAMESPACE - END
