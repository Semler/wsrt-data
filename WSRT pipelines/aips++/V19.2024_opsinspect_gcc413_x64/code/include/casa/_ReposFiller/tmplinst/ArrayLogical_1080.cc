// ArrayLogical_1080.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allEQ(Array<uChar> const &, Array<uChar> const &);
template Bool allEQ(Array<uChar> const &, uChar const &);
template Array<Bool> operator==(Array<uChar> const &, uChar const &);
template Array<Bool> operator>(Array<uChar> const &, uChar const &);
} //# NAMESPACE - END
