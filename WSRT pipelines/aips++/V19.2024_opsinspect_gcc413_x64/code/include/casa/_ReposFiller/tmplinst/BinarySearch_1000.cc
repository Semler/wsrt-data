// BinarySearch_1000.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/BinarySearch.cc>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template Int binarySearchBrackets(Bool &, Vector<Float> const &, Float const &, uInt, Int);
} //# NAMESPACE - END
