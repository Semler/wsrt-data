// CountedPtr_1340.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Float> >;
template class CountedPtr<Block<Float> >;
template class PtrRep<Block<Float> >;
template class SimpleCountedConstPtr<Block<Float> >;
template class SimpleCountedPtr<Block<Float> >;
} //# NAMESPACE - END
