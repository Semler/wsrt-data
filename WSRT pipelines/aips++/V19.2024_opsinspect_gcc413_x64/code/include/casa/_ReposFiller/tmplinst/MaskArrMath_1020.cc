// MaskArrMath_1020.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/MaskArrMath.cc>
#include <casa/Arrays/MaskedArray.h>
namespace casa { //# NAMESPACE - BEGIN
template Float sum(MaskedArray<Float> const &);
template Float mean(MaskedArray<Float> const &);
} //# NAMESPACE - END
