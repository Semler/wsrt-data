// CountedPtr_1130.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Arrays/Vector.h>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimpleCountedConstPtr<Block<Vector<Float> > >;
template class PtrRep<Block<Vector<Float> > >;
template class CountedPtr<Block<Vector<Float> > >;
template class CountedConstPtr<Block<Vector<Float> > >;
template class SimpleCountedPtr<Block<Vector<Float> > >;
} //# NAMESPACE - END
