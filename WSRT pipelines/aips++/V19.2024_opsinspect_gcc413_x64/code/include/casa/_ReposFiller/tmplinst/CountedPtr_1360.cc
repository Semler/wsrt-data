// CountedPtr_1360.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Short> >;
template class CountedPtr<Block<Short> >;
template class PtrRep<Block<Short> >;
template class SimpleCountedConstPtr<Block<Short> >;
template class SimpleCountedPtr<Block<Short> >;
} //# NAMESPACE - END
