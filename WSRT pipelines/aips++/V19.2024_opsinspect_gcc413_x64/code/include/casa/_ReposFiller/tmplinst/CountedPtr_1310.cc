// CountedPtr_1310.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Bool> >;
template class CountedPtr<Block<Bool> >;
template class PtrRep<Block<Bool> >;
template class SimpleCountedConstPtr<Block<Bool> >;
template class SimpleCountedPtr<Block<Bool> >;
} //# NAMESPACE - END
