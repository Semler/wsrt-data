// ArrayLogical_1050.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allEQ(Array<Float> const &, Array<Float> const &);
template Bool allEQ(Array<Float> const &, Float const &);
template Bool allGE(Array<Float> const &, Float const &);
template Bool allLT(Array<Float> const &, Float const &);
template Bool anyEQ(Array<Float> const &, Float const &);
template Bool anyGT(Array<Float> const &, Float const &);
template Bool anyLE(Array<Float> const &, Float const &);
template Bool anyLT(Array<Float> const &, Float const &);
template Bool anyNE(Array<Float> const &, Float const &);
template Bool anyNE(Array<Float> const &, Array<Float> const &);
} //# NAMESPACE - END
