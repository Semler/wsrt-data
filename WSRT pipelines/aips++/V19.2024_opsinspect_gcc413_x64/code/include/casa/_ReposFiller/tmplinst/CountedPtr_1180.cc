// CountedPtr_1180.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Vector<Double> > >;
template class CountedPtr<Block<Vector<Double> > >;
template class PtrRep<Block<Vector<Double> > >;
template class SimpleCountedConstPtr<Block<Vector<Double> > >;
template class SimpleCountedPtr<Block<Vector<Double> > >;
} //# NAMESPACE - END
