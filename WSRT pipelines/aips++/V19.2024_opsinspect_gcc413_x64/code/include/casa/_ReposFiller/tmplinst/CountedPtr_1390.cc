// CountedPtr_1390.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<uShort> >;
template class CountedPtr<Block<uShort> >;
template class PtrRep<Block<uShort> >;
template class SimpleCountedConstPtr<Block<uShort> >;
template class SimpleCountedPtr<Block<uShort> >;
} //# NAMESPACE - END
