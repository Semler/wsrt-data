// CountedPtr_1460.cc -- Tue Apr  1 11:50:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/OS/DataConversion.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<DataConversion>;
template class CountedConstPtr<DataConversion>;
template class SimpleCountedPtr<DataConversion>;
template class SimpleCountedConstPtr<DataConversion>;
template class PtrRep<DataConversion>;
} //# NAMESPACE - END
