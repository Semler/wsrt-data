// CountedPtr_1490.cc -- Tue Apr  1 11:50:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/fstream.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<fstream>;
template class CountedPtr<fstream>;
template class PtrRep<fstream>;
template class SimpleCountedConstPtr<fstream>;
template class SimpleCountedPtr<fstream>;
} //# NAMESPACE - END
