// ArrayMath_1110.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<uChar> min<uChar>(Array<uChar> const &, Array<uChar> const &);
template void min<uChar>(Array<uChar> &, Array<uChar> const &, Array<uChar> const &);
template uChar max(Array<uChar> const &);
template uChar min(Array<uChar> const &);
template void indgen(Array<uChar> &);
template void indgen(Array<uChar> &, uChar, uChar);
template void minMax(uChar &, uChar &, Array<uChar> const &);
} //# NAMESPACE - END
