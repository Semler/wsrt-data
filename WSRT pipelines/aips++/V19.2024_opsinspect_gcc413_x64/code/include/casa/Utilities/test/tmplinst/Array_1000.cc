// Array_1000.cc -- Tue Apr  1 11:57:26 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <casa/Utilities/Regex.h>
#include <casa/Arrays/ArrayIO.cc>
#include <casa/Arrays/MaskedArray.cc>
#include <casa/Arrays/Vector.cc>
namespace casa { //# NAMESPACE - BEGIN
template class Array<Regex>;
template class MaskedArray<Regex>;
template class Vector<Regex>;
template ostream & operator<<(ostream &, Array<Regex> const &);
} //# NAMESPACE - END
