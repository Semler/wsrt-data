// GenSort_1000.cc -- Tue Apr  1 11:57:26 BST 2008 -- renting
#include <casa/Utilities/GenSort.cc>
namespace casa { //# NAMESPACE - BEGIN
template uInt genSort(Int *, uInt, Sort::Order, Int);
template uInt genSort(Vector<uInt> &, Int const *, uInt, Sort::Order, Int);
} //# NAMESPACE - END
