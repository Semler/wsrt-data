// CountedPtr_1000.cc -- Tue Apr  1 12:27:41 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <synthesis/MeasurementComponents/PBMathInterface.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<PBMathInterface>;
template class CountedPtr<PBMathInterface>;
template class PtrRep<PBMathInterface>;
template class SimpleCountedConstPtr<PBMathInterface>;
template class SimpleCountedPtr<PBMathInterface>;
} //# NAMESPACE - END
