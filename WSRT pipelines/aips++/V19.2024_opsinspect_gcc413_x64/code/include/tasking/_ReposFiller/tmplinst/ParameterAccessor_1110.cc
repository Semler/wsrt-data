// ParameterAccessor_1110.cc -- Tue Apr  1 12:28:14 BST 2008 -- renting
#include <tasking/Tasking/ParameterAccessor.cc>
#include <casa/Quanta/QuantumHolder.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class ParameterAccessor<Vector<QuantumHolder> >;
template class ParameterAccessor<Array<QuantumHolder> >;
template class ParameterAccessor<Vector<Quantum<Double> > >;
template class ParameterAccessor<Array<Quantum<Double> > >;
} //# NAMESPACE - END
