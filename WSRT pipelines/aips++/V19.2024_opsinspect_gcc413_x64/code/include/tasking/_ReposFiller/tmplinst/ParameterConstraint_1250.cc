// ParameterConstraint_1250.cc -- Tue Apr  1 12:28:14 BST 2008 -- renting
#include <tasking/Tasking/ParameterConstraint.cc>
#include <measures/Measures/MeasureHolder.h>
#include <measures/Measures/MEpoch.h>
#include <measures/Measures/MDirection.h>
#include <measures/Measures/MDoppler.h>
#include <measures/Measures/MFrequency.h>
#include <measures/Measures/MPosition.h>
#include <measures/Measures/MRadialVelocity.h>
#include <measures/Measures/MBaseline.h>
#include <measures/Measures/Muvw.h>
#include <measures/Measures/MEarthMagnetic.h>
namespace casa { //# NAMESPACE - BEGIN
template class ParameterConstraint<MeasureHolder>;
template class ParameterConstraint<MEpoch>;
template class ParameterConstraint<MDirection>;
template class ParameterConstraint<MDoppler>;
template class ParameterConstraint<MFrequency>;
template class ParameterConstraint<MPosition>;
template class ParameterConstraint<MRadialVelocity>;
template class ParameterConstraint<MBaseline>;
template class ParameterConstraint<Muvw>;
template class ParameterConstraint<MEarthMagnetic>;
} //# NAMESPACE - END
