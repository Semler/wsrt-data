// Parameter_1130.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/Parameter.cc>
#include <components/ComponentModels/SkyComponent.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class Parameter<Vector<SkyComponent> >;
template class Parameter<Array<SkyComponent> >;
} //# NAMESPACE - END
