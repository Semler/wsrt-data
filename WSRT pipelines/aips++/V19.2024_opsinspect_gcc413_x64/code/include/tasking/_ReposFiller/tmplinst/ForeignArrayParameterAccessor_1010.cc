// ForeignArrayParameterAccessor_1010.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/ForeignArrayParameterAccessor.cc>
#include <components/ComponentModels/SkyComponent.h>
#include <tasking/Tasking/ForeignBaseArrayParameterAccessor.cc>
namespace casa { //# NAMESPACE - BEGIN
template class ForeignVectorParameterAccessor<SkyComponent>;
template class ForeignArrayParameterAccessor<SkyComponent>;
template class ForeignBaseVectorParameterAccessor<SkyComponent>;
template class ForeignBaseArrayParameterAccessor<SkyComponent>;
} //# NAMESPACE - END
