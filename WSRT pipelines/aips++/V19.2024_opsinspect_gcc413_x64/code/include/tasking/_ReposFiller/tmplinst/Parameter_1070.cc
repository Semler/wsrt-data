// Parameter_1070.cc -- Tue Apr  1 12:28:13 BST 2008 -- renting
#include <tasking/Tasking/Parameter.cc>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class Parameter<Vector<Double> >;
template class Parameter<Vector<Float> >;
template class Parameter<Vector<Int> >;
template class Parameter<Vector<Bool> >;
} //# NAMESPACE - END
