// Function_1010.cc -- Tue Apr  1 11:57:47 BST 2008 -- renting
#include <scimath/Functionals/Function.cc>
namespace casa { //# NAMESPACE - BEGIN
template class Function<Float, Double>;
template class Function<Int, Double>;
template ostream & operator<<(ostream &, Function<Double, Double> const &);
#include <casa/BasicSL/Complex.h>
template class Function<Double, DComplex>;
} //# NAMESPACE - END
