// ArraySampledFunctional_1010.cc -- Tue Apr  1 11:57:47 BST 2008 -- renting
#include <scimath/Functionals/ArraySampledFunctional.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template class ArraySampledFunctional<Array<Double> >;
template class ArraySampledFunctional<Array<Float> >;
} //# NAMESPACE - END
