// GenSort_1000.cc -- Tue Apr  1 11:57:46 BST 2008 -- renting
#include <casa/Utilities/GenSort.cc>
#include <casa/Arrays/Vector.h>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template uInt genSort(Vector<uInt> &, Block<Float> const &);
} //# NAMESPACE - END
