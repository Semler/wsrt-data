// SampledFunctional_1020.cc -- Tue Apr  1 11:57:47 BST 2008 -- renting
#include <scimath/Functionals/SampledFunctional.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class SampledFunctional<Int>;
template class SampledFunctional<DComplex>;
} //# NAMESPACE - END
