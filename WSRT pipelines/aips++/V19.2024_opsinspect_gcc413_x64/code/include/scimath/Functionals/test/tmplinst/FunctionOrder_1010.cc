// FunctionOrder_1010.cc -- Tue Apr  1 11:57:47 BST 2008 -- renting
#include <scimath/Functionals/FunctionOrder.cc>
namespace casa { //# NAMESPACE - BEGIN
template const String &FunctionOrder<Double>::ident() const;
template Bool FunctionOrder<Double>::toRecord(String &, RecordInterface &out) const;
template Bool FunctionOrder<Double>::fromString(String &, String const &);
template Bool FunctionOrder<Double>::fromRecord(String &, RecordInterface const &);
} //# NAMESPACE - END
