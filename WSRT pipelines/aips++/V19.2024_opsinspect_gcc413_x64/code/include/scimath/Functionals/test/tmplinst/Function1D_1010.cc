// Function1D_1010.cc -- Tue Apr  1 11:57:47 BST 2008 -- renting
#include <scimath/Functionals/Function1D.h>
namespace casa { //# NAMESPACE - BEGIN
template class Function1D<Float, Double>;
template class Function1D<Int, Double>;
#include <casa/BasicSL/Complex.h>
template class Function1D<Double, DComplex>;
} //# NAMESPACE - END
