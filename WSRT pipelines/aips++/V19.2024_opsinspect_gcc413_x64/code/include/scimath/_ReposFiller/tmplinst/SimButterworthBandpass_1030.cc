// SimButterworthBandpass_1030.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/SimButterworthBandpass.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimButterworthBandpass<AutoDiffA<Double> >;
template class SimButterworthBandpass<AutoDiffA<Float> >;
} //# NAMESPACE - END
