// Vector_1030.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Arrays/Vector.cc>
#include <casa/Arrays/Vector.h>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class Vector<Vector<SquareMatrix<Complex, 4> > >;
} //# NAMESPACE - END
