// FunctionHolder_1030.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/FunctionHolder.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool FunctionHolder<DComplex>::getType<AutoDiff<DComplex> >(String &, Function<AutoDiff<DComplex> > * &);
} //# NAMESPACE - END
