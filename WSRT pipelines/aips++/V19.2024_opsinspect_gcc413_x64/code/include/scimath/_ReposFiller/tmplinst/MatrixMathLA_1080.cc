// MatrixMathLA_1080.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/Arrays/Matrix.h>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template Matrix<DComplex> invertSymPosDef(Matrix<DComplex> const &);
template void invertSymPosDef(Matrix<DComplex> &, DComplex &, Matrix<DComplex> const &);
} //# NAMESPACE - END
