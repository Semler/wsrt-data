// Function_1010.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Function.cc>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template class Function<AutoDiff<Double> >;
template class Function<AutoDiff<Float> >;
} //# NAMESPACE - END
