// LSQFit2_1080.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LSQFit2.cc>
namespace casa { //# NAMESPACE - BEGIN
typedef Float* It6;
typedef uInt* It6Int;
template void LSQFit::makeNorm<Float, It6>(It6 const &, Float const &, Float const &, Bool, Bool);
template void LSQFit::makeNorm<Float, It6>(It6 const &, Float const &, Float const &, LSQFit::Real, Bool, Bool);
template void LSQFit::makeNorm<Float, It6, It6Int>(uInt, It6Int const &, It6 const &, Float const &, Float const &, Bool, Bool);
template void LSQFit::makeNorm<Float, It6, It6Int>(uInt, It6Int const &, It6 const &, Float const &, Float const &, LSQFit::Real, Bool, Bool);
template Bool LSQFit::addConstraint<Float, It6>(It6 const &, Float const &);
template Bool LSQFit::addConstraint<Float, It6, It6Int>(uInt, It6Int const &, It6 const &, Float const &);
template Bool LSQFit::setConstraint<Float, It6>(uInt, It6 const &, Float const &);
template Bool LSQFit::setConstraint<Float, It6, It6Int>(uInt, uInt, It6Int const &, It6 const &, Float const &);
template Bool LSQFit::getConstraint<Float>(uInt, Float *) const;
template void LSQFit::solve<Float>(Float *);
template Bool LSQFit::solveLoop<Float>(Double &, uInt &, Float *, Bool);
template Bool LSQFit::solveLoop<Float>(uInt &, Float *, Bool);
template void LSQFit::copy<Float>(Double const *, Double const *, Float *, LSQReal);
template void LSQFit::uncopy<Float>(Double *, Double const *, Float *, LSQReal);
template Bool LSQFit::getErrors<Float>(Float *);
template Bool LSQFit::getCovariance<Float>(Float *);
} //# NAMESPACE - END
