// CompoundParam_1000.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/CompoundParam.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class CompoundParam<AutoDiff<DComplex> >;
template class CompoundParam<AutoDiff<Complex> >;
} //# NAMESPACE - END
