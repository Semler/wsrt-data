// FunctionParam_1040.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/FunctionParam.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffIO.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class FunctionParam<AutoDiffA<Double> >;
template class FunctionParam<AutoDiffA<Float> >;
} //# NAMESPACE - END
