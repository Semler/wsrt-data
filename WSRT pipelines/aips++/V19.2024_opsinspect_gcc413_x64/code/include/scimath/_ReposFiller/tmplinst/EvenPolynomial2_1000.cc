// EvenPolynomial2_1000.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/EvenPolynomial2.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class EvenPolynomial<AutoDiff<DComplex> >;
template class EvenPolynomial<AutoDiff<Complex> >;
} //# NAMESPACE - END
