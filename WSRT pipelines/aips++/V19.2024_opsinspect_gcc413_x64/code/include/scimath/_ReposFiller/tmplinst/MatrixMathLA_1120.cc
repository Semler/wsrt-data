// MatrixMathLA_1120.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/Arrays/Matrix.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template Matrix<AutoDiff<Float> > invertSymPosDef(Matrix<AutoDiff<Float> > const &);
template void invertSymPosDef(Matrix<AutoDiff<Float> > &, AutoDiff<Float> &, Matrix<AutoDiff<Float> > const &);
} //# NAMESPACE - END
