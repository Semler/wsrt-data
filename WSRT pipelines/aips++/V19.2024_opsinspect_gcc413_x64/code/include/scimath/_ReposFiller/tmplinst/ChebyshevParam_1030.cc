// ChebyshevParam_1030.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/ChebyshevParam.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class ChebyshevParam<AutoDiffA<Double> >;
template class ChebyshevParam<AutoDiffA<Float> >;
template class ChebyshevParamModeImpl<AutoDiffA<Double> >;
template class ChebyshevParamModeImpl<AutoDiffA<Float> >;
} //# NAMESPACE - END
