// WrapperData_1000.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Functionals/WrapperData.h>
namespace casa { //# NAMESPACE - BEGIN
template class WrapperData<AutoDiff<Double>, AutoDiff<Double>, AutoDiff<Double>, True, True>;
template class WrapperData<AutoDiff<Double>, AutoDiff<Double>, AutoDiff<Double>, True, False>;
template class WrapperData<AutoDiff<Double>, AutoDiff<Double>, AutoDiff<Double>, False, True>;
template class WrapperData<AutoDiff<Double>, AutoDiff<Double>, AutoDiff<Double>, False, False>;
template class WrapperData<AutoDiff<Double>, AutoDiff<Double>, Vector<AutoDiff<Double> >, True, True>;
template class WrapperData<AutoDiff<Double>, AutoDiff<Double>, Vector<AutoDiff<Double> >, False, True>;
template class WrapperData<AutoDiff<Double>, Vector<AutoDiff<Double> >, AutoDiff<Double>, True, True>;
template class WrapperData<AutoDiff<Double>, Vector<AutoDiff<Double> >, AutoDiff<Double>, True, False>;
template class WrapperData<AutoDiff<Double>, Vector<AutoDiff<Double> >, Vector<AutoDiff<Double> >, True, True>;
template class WrapperData<AutoDiff<Float>, AutoDiff<Float>, AutoDiff<Float>, True, True>;
template class WrapperData<AutoDiff<Float>, AutoDiff<Float>, AutoDiff<Float>, True, False>;
template class WrapperData<AutoDiff<Float>, AutoDiff<Float>, AutoDiff<Float>, False, True>;
template class WrapperData<AutoDiff<Float>, AutoDiff<Float>, AutoDiff<Float>, False, False>;
template class WrapperData<AutoDiff<Float>, AutoDiff<Float>, Vector<AutoDiff<Float> >, True, True>;
template class WrapperData<AutoDiff<Float>, AutoDiff<Float>, Vector<AutoDiff<Float> >, False, True>;
template class WrapperData<AutoDiff<Float>, Vector<AutoDiff<Float> >, AutoDiff<Float>, True, True>;
template class WrapperData<AutoDiff<Float>, Vector<AutoDiff<Float> >, AutoDiff<Float>, True, False>;
template class WrapperData<AutoDiff<Float>, Vector<AutoDiff<Float> >, Vector<AutoDiff<Float> >, True, True>;
} //# NAMESPACE - END
