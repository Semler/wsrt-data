// ListMap_1000.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Containers/ListMap.cc>
#include <scimath/Functionals/AbstractFunctionFactory.h>
namespace casa { //# NAMESPACE - BEGIN
template class ListMap<String, OrderedPair<FunctionFactory<Double>*, Bool> >;
template class ListMapRep<String, OrderedPair<FunctionFactory<Double>*, Bool> >;
template class ListMapIterRep<String, OrderedPair<FunctionFactory<Double>*, Bool> >;
} //# NAMESPACE - END
