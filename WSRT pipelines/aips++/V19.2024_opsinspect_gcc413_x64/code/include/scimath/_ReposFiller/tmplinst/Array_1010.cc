// Array_1010.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<AutoDiff<DComplex> >;
#ifdef AIPS_SUN_NATIVE
template class Array<AutoDiff<DComplex> >::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
