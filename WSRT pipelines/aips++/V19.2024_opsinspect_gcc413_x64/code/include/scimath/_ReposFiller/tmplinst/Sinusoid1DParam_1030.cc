// Sinusoid1DParam_1030.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Functionals/Sinusoid1DParam.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Sinusoid1DParam<AutoDiffA<Double> >;
template class Sinusoid1DParam<AutoDiffA<Float> >;
} //# NAMESPACE - END
