// OrderedPair_1000.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Containers/OrderedPair.cc>
#include <casa/BasicSL/String.h>
#include <scimath/Functionals/AbstractFunctionFactory.h>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedPair<FunctionFactory<Double>*, Bool>;
template class OrderedPair<String, OrderedPair<FunctionFactory<Double>*, Bool> >;
} //# NAMESPACE - END
