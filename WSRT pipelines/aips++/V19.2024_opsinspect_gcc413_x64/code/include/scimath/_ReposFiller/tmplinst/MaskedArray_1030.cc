// MaskedArray_1030.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/MaskedArray.cc>
#include <casa/Arrays/Vector.h>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template class MaskedArray<Vector<SquareMatrix<Complex, 4> > >;
} //# NAMESPACE - END
