// CompiledParam_1030.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/CompiledParam.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class CompiledParam<AutoDiffA<Double> >;
template class CompiledParam<AutoDiffA<Float> >;
} //# NAMESPACE - END
