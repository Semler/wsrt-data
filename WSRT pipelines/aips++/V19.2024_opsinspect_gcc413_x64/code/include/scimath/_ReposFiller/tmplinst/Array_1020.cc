// Array_1020.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<AutoDiff<Double> >;
#ifdef AIPS_SUN_NATIVE
template class Array<AutoDiff<Double> >::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
