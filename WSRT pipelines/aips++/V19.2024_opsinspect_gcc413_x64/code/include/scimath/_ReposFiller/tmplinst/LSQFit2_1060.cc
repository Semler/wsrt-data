// LSQFit2_1060.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LSQFit2.cc>
#include <complex>
namespace casa { //# NAMESPACE - BEGIN
typedef std::complex<Float>* It4;
typedef uInt* It4Int;
template void LSQFit::makeNorm<Float, It4>(It4 const &, Float const &, std::complex<Float> const &, Bool, Bool);
template void LSQFit::makeNorm<Float, It4>(It4 const &, Float const &, std::complex<Float> const &, LSQFit::AsReal, Bool, Bool);
template void LSQFit::makeNorm<Float, It4>(It4 const &, Float const &, std::complex<Float> const &, LSQFit::Complex, Bool, Bool);
template void LSQFit::makeNorm<Float, It4>(It4 const &, Float const &, std::complex<Float> const &, LSQFit::Conjugate, Bool, Bool);
template void LSQFit::makeNorm<Float, It4>(It4 const &, Float const &, std::complex<Float> const &, LSQFit::Separable, Bool, Bool);
template void LSQFit::makeNorm<Float, It4, It4Int>(uInt, It4Int const &, It4 const &, Float const &, std::complex<Float> const &, Bool, Bool);
template void LSQFit::makeNorm<Float, It4, It4Int>(uInt, It4Int const &, It4 const &, Float const &, std::complex<Float> const &, LSQFit::AsReal, Bool, Bool);
template void LSQFit::makeNorm<Float, It4, It4Int>(uInt, It4Int const &, It4 const &, Float const &, std::complex<Float> const &, LSQFit::Complex, Bool, Bool);
template void LSQFit::makeNorm<Float, It4, It4Int>(uInt, It4Int const &, It4 const &, Float const &, std::complex<Float> const &, LSQFit::Conjugate, Bool, Bool);
template void LSQFit::makeNorm<Float, It4, It4Int>(uInt, It4Int const &, It4 const &, Float const &, std::complex<Float> const &, LSQFit::Separable, Bool, Bool);
template Bool LSQFit::addConstraint<Float, It4>(It4 const &, std::complex<Float> const &);
template Bool LSQFit::addConstraint<Float, It4, It4Int>(uInt, It4Int const &, It4 const &, std::complex<Float> const &);
template Bool LSQFit::setConstraint<Float, It4>(uInt, It4 const &, std::complex<Float> const &);
template Bool LSQFit::setConstraint<Float, It4, It4Int>(uInt, uInt, It4Int const &, It4 const &, std::complex<Float> const &);
template Bool LSQFit::getConstraint<Float>(uInt, std::complex<Float> *) const;
template void LSQFit::solve<Float>(std::complex<Float> *);
template Bool LSQFit::solveLoop<Float>(Double &, uInt &, std::complex<Float> *, Bool);
template Bool LSQFit::solveLoop<Float>(uInt &, std::complex<Float> *, Bool);
template void LSQFit::copy<std::complex<Float> >(Double const *, Double const *, std::complex<Float> *, LSQComplex);
template void LSQFit::uncopy<std::complex<Float> >(Double *, Double const *, std::complex<Float> *, LSQComplex);
template Bool LSQFit::getErrors<Float>(std::complex<Float> *);
template Bool LSQFit::getCovariance<Float>(std::complex<Float> *);
} //# NAMESPACE - END
