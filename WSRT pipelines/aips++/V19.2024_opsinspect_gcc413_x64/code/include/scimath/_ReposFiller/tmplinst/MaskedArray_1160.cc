// MaskedArray_1160.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/MaskedArray.cc>
#include <scimath/Mathematics/RigidVector.h>
namespace casa { //# NAMESPACE - BEGIN
template class MaskedArray<RigidVector<Double, 3> >;
template class MaskedArray<RigidVector<Double, 2> >;
} //# NAMESPACE - END
