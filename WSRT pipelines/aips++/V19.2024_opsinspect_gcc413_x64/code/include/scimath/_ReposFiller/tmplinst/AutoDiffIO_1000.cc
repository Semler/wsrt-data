// AutoDiffIO_1000.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/AutoDiffIO.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template ostream & operator<<(ostream &, AutoDiff<Complex> const &);
} //# NAMESPACE - END
