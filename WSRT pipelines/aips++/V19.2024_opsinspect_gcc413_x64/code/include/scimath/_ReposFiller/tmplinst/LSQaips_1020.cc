// LSQaips_1020.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LSQaips.cc>
#include <casa/Arrays/Array.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool LSQaips::getCovariance<std::complex<Double> >(Array<std::complex<Double> > &);
template Bool LSQaips::solveLoop<std::complex<Double> >(Double &, uInt &, Vector<std::complex<Double> > &, Bool doSVD);
template Bool LSQaips::solveLoop<std::complex<Double> >(uInt &, Vector<std::complex<Double> > &, Bool doSVD);
} //# NAMESPACE - END
