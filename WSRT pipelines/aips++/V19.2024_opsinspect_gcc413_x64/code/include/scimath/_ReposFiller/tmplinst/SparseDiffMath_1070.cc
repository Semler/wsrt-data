// SparseDiffMath_1070.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool near(SparseDiff<Float> const &, SparseDiff<Float> const &, Double const);
template Bool near(SparseDiff<Float> const &, Float const &, Double const);
template Bool near(Float const &, SparseDiff<Float> const &, Double const);
template Bool allnear(SparseDiff<Float> const &, SparseDiff<Float> const &);
template Bool allnear(SparseDiff<Float> const &, Float const &);
template Bool allnear(Float const &, SparseDiff<Float> const &);
template Bool allnear(SparseDiff<Float> const &, SparseDiff<Float> const &, Double const);
template Bool allnear(SparseDiff<Float> const &, Float const &, Double const);
template Bool allnear(Float const &, SparseDiff<Float> const &, Double const);
template Bool nearAbs(SparseDiff<Float> const &, SparseDiff<Float> const &);
template Bool nearAbs(SparseDiff<Float> const &, Float const &);
template Bool nearAbs(Float const &, SparseDiff<Float> const &);
template Bool nearAbs(SparseDiff<Float> const &, SparseDiff<Float> const &, Double const);
template Bool nearAbs(SparseDiff<Float> const &, Float const &, Double const);
template Bool nearAbs(Float const &, SparseDiff<Float> const &, Double const);
template Bool allnearAbs(SparseDiff<Float> const &, SparseDiff<Float> const &);
template Bool allnearAbs(SparseDiff<Float> const &, Float const &);
template Bool allnearAbs(Float const &, SparseDiff<Float> const &);
template Bool allnearAbs(SparseDiff<Float> const &, SparseDiff<Float> const &, Double const);
template Bool allnearAbs(SparseDiff<Float> const &, Float const &, Double const);
template Bool allnearAbs(Float const &, SparseDiff<Float> const &, Double const);
} //# NAMESPACE - END
