// Gaussian2DParam_1000.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Gaussian2DParam.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Gaussian2DParam<AutoDiff<DComplex> >;
template class Gaussian2DParam<AutoDiff<Complex> >;
} //# NAMESPACE - END
