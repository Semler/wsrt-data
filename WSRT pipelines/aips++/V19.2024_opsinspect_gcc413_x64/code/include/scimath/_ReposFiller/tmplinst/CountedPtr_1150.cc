// CountedPtr_1150.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<AutoDiff<Double> > >;
template class CountedConstPtr<Block<AutoDiff<Double> > >;
template class SimpleCountedPtr<Block<AutoDiff<Double> > >;
template class SimpleCountedConstPtr<Block<AutoDiff<Double> > >;
template class PtrRep<Block<AutoDiff<Double> > >;
} //# NAMESPACE - END
