// MaskedArray_1080.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/MaskedArray.cc>
#include <casa/Utilities/CountedPtr.h>
#include <scimath/Functionals/Function1D.h>
namespace casa { //# NAMESPACE - BEGIN
template class MaskedArray<CountedPtr<Function1D<Float> > >;
} //# NAMESPACE - END
