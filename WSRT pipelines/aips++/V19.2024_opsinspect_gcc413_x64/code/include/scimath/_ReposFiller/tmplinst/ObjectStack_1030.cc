// ObjectStack_1030.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Containers/ObjectStack.cc>
#include <scimath/Mathematics/SparseDiffRep.h>
#include <scimath/Mathematics/SparseDiff.h>
#include <casa/vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class ObjectStack<SparseDiffRep<Float> >;
} //# NAMESPACE - END
