// Chebyshev_1020.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/Chebyshev.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Chebyshev<AutoDiff<Double> >;
template class Chebyshev<AutoDiff<Float> >;
} //# NAMESPACE - END
