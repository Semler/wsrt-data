// LSQFit2_1040.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LSQFit2.cc>
#include <casa/Arrays/VectorSTLIterator.h>
namespace casa { //# NAMESPACE - BEGIN
typedef VectorSTLIterator<std::complex<Float> > It2;
typedef VectorSTLIterator<uInt> It2Int;
template void LSQFit::makeNorm<Float, It2>(It2 const &, Float const &, std::complex<Float> const &, Bool, Bool);
template void LSQFit::makeNorm<Float, It2>(It2 const &, Float const &, std::complex<Float> const &, LSQFit::AsReal, Bool, Bool);
template void LSQFit::makeNorm<Float, It2>(It2 const &, Float const &, std::complex<Float> const &, LSQFit::Complex, Bool, Bool);
template void LSQFit::makeNorm<Float, It2>(It2 const &, Float const &, std::complex<Float> const &, LSQFit::Conjugate, Bool, Bool);
template void LSQFit::makeNorm<Float, It2>(It2 const &, Float const &, std::complex<Float> const &, LSQFit::Separable, Bool, Bool);
template void LSQFit::makeNorm<Float, It2, It2Int>(uInt, It2Int const &, It2 const &, Float const &, std::complex<Float> const &, Bool, Bool);
template void LSQFit::makeNorm<Float, It2, It2Int>(uInt, It2Int const &, It2 const &, Float const &, std::complex<Float> const &, LSQFit::AsReal, Bool, Bool);
template void LSQFit::makeNorm<Float, It2, It2Int>(uInt, It2Int const &, It2 const &, Float const &, std::complex<Float> const &, LSQFit::Complex, Bool, Bool);
template void LSQFit::makeNorm<Float, It2, It2Int>(uInt, It2Int const &, It2 const &, Float const &, std::complex<Float> const &, LSQFit::Conjugate, Bool, Bool);
template void LSQFit::makeNorm<Float, It2, It2Int>(uInt, It2Int const &, It2 const &, Float const &, std::complex<Float> const &, LSQFit::Separable, Bool, Bool);
template void LSQFit::makeNorm<Float, std::complex<Float> >(std::vector<std::pair<uInt, std::complex<Float> > > const &, Float const &, std::complex<Float> const &, Bool, Bool);
template void LSQFit::makeNorm<Float, std::complex<Float> >(std::vector<std::pair<uInt, std::complex<Float> > > const &, Float const &, std::complex<Float> const &, LSQFit::AsReal, Bool, Bool);
template void LSQFit::makeNorm<Float, std::complex<Float> >(std::vector<std::pair<uInt, std::complex<Float> > > const &, Float const &, std::complex<Float> const &, LSQFit::Complex, Bool, Bool);
template void LSQFit::makeNorm<Float, std::complex<Float> >(std::vector<std::pair<uInt, std::complex<Float> > > const &, Float const &, std::complex<Float> const &, LSQFit::Conjugate, Bool, Bool);
template void LSQFit::makeNorm<Float, std::complex<Float> >(std::vector<std::pair<uInt, std::complex<Float> > > const &, Float const &, std::complex<Float> const &, LSQFit::Separable, Bool, Bool);
template Bool LSQFit::addConstraint<Float, It2>(It2 const &, std::complex<Float> const &);
template Bool LSQFit::addConstraint<Float, It2, It2Int>(uInt, It2Int const &, It2 const &, std::complex<Float> const &);
template Bool LSQFit::setConstraint<Float, It2>(uInt, It2 const &, std::complex<Float> const &);
template Bool LSQFit::setConstraint<Float, It2, It2Int>(uInt, uInt, It2Int const &, It2 const &, std::complex<Float> const &);
template Bool LSQFit::getConstraint<It2>(uInt, It2 &) const;
template void LSQFit::solve<It2>(It2 &);
template Bool LSQFit::solveLoop<It2>(Double &, uInt &, It2 &, Bool);
template Bool LSQFit::solveLoop<It2>(uInt &, It2 &, Bool);
template void LSQFit::copy<It2>(Double const *, Double const *, It2 &, LSQComplex);
template void LSQFit::uncopy<It2>(Double *, Double const *, It2 &, LSQComplex);
template void LSQFit::copyDiagonal<It2>(It2 &, LSQComplex);
template Bool LSQFit::getErrors<It2>(It2 &);
} //# NAMESPACE - END
