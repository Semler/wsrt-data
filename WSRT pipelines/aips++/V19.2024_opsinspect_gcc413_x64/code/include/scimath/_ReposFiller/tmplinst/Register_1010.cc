// Register_1010.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <casa/Utilities/Register.cc>
#include <casa/Containers/OrderedPair.h>
#include <casa/Containers/List.h>
#include <casa/BasicSL/String.h>
#include <scimath/Functionals/AbstractFunctionFactory.h>
namespace casa { //# NAMESPACE - BEGIN
template uInt Register(ListNotice<OrderedPair<String, OrderedPair<FunctionFactory<Double>*, Bool> > > const *);
} //# NAMESPACE - END
