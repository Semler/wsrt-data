// SincFunction_1030.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/SincFunction.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class SincFunction<Double>;
template class SincFunction<Float>;
} //# NAMESPACE - END
