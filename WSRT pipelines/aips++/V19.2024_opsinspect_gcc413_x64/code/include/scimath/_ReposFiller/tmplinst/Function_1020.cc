// Function_1020.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Function.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class Function<AutoDiffA<Complex> >;
template class Function<AutoDiffA<DComplex> >;
} //# NAMESPACE - END
