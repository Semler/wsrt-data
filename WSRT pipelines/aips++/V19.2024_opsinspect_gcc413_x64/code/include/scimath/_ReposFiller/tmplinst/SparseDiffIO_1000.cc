// SparseDiffIO_1000.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffIO.cc>
#include <scimath/Mathematics/SparseDiff.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template ostream & operator<<(ostream &, SparseDiff<Complex> const &);
} //# NAMESPACE - END
