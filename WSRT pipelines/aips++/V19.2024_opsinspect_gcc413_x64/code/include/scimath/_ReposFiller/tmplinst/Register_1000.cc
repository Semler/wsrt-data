// Register_1000.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/Register.cc>
#include <casa/Containers/OrderedMap.h>
#include <scimath/Mathematics/SquareMatrix.h>
namespace casa { //# NAMESPACE - BEGIN
template uInt Register(OrderedMapNotice<Double, SquareMatrix<Complex, 2>*> const *);
} //# NAMESPACE - END
