// LinearFitSVD_1020.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/LinearFitSVD.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class LinearFitSVD<AutoDiff<DComplex> >;
template class LinearFitSVD<AutoDiffA<DComplex> >;
} //# NAMESPACE - END
