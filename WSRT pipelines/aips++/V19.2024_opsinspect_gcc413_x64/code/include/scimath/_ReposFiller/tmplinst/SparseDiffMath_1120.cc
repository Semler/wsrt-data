// SparseDiffMath_1120.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template SparseDiff<Float> acos(SparseDiff<Float> const &);
template SparseDiff<Float> asin(SparseDiff<Float> const &);
template SparseDiff<Float> atan(SparseDiff<Float> const &);
template SparseDiff<Float> atan2(SparseDiff<Float> const &, SparseDiff<Float> const &);
template SparseDiff<Float> cos(SparseDiff<Float> const &);
template SparseDiff<Float> cosh(SparseDiff<Float> const &);
template SparseDiff<Float> exp(SparseDiff<Float> const &);
template SparseDiff<Float> log(SparseDiff<Float> const &);
template SparseDiff<Float> log10(SparseDiff<Float> const &);
template SparseDiff<Float> erf(SparseDiff<Float> const &);
template SparseDiff<Float> erfc(SparseDiff<Float> const &);
template SparseDiff<Float> pow(SparseDiff<Float> const &, SparseDiff<Float> const &);
template SparseDiff<Float> pow(SparseDiff<Float> const &, Float const &);
template SparseDiff<Float> square(SparseDiff<Float> const &);
template SparseDiff<Float> cube(SparseDiff<Float> const &);
template SparseDiff<Float> sin(SparseDiff<Float> const &);
template SparseDiff<Float> sinh(SparseDiff<Float> const &);
template SparseDiff<Float> sqrt(SparseDiff<Float> const &);
template SparseDiff<Float> tan(SparseDiff<Float> const &);
template SparseDiff<Float> tanh(SparseDiff<Float> const &);
template SparseDiff<Float> abs(SparseDiff<Float> const &);
template SparseDiff<Float> ceil(SparseDiff<Float> const &);
template SparseDiff<Float> floor(SparseDiff<Float> const &);
template SparseDiff<Float> fmod(SparseDiff<Float> const &, Float const &);
template SparseDiff<Float> fmod(SparseDiff<Float> const &, SparseDiff<Float> const &);
template SparseDiff<Float> max(SparseDiff<Float> const &, SparseDiff<Float> const &);
template SparseDiff<Float> min(SparseDiff<Float> const &, SparseDiff<Float> const &);
} //# NAMESPACE - END
