// MatrixMathLA_1060.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/Arrays/Matrix.h>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template Matrix<AutoDiff<DComplex> > invertSymPosDef(Matrix<AutoDiff<DComplex> > const &);
template void invertSymPosDef(Matrix<AutoDiff<DComplex> > &, AutoDiff<DComplex> &, Matrix<AutoDiff<DComplex> > const &);
} //# NAMESPACE - END
