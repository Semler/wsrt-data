// ObjectStack_1010.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Containers/ObjectStack.cc>
#include <scimath/Mathematics/SparseDiffRep.h>
#include <scimath/Mathematics/SparseDiff.h>
#include <casa/vector.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class ObjectStack<SparseDiffRep<DComplex> >;
} //# NAMESPACE - END
