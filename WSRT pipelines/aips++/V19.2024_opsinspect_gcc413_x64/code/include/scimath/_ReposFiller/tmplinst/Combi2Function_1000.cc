// Combi2Function_1000.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Functionals/Combi2Function.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class CombiFunction<AutoDiff<DComplex> >;
template class CombiFunction<AutoDiff<Complex> >;
} //# NAMESPACE - END
