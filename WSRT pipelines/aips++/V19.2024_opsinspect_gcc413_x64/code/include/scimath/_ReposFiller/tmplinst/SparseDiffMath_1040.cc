// SparseDiffMath_1040.cc -- Tue Apr  1 11:58:32 BST 2008 -- renting
#include <scimath/Mathematics/SparseDiffMath.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/SparseDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template SparseDiff<DComplex> acos(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> asin(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> atan(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> atan2(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template SparseDiff<DComplex> cos(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> cosh(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> exp(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> log(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> log10(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> erf(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> erfc(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> pow(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template SparseDiff<DComplex> pow(SparseDiff<DComplex> const &, DComplex const &);
template SparseDiff<DComplex> sin(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> sinh(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> sqrt(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> abs(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> ceil(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> floor(SparseDiff<DComplex> const &);
template SparseDiff<DComplex> fmod(SparseDiff<DComplex> const &, DComplex const &);
template SparseDiff<DComplex> fmod(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template SparseDiff<DComplex> max(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
template SparseDiff<DComplex> min(SparseDiff<DComplex> const &, SparseDiff<DComplex> const &);
} //# NAMESPACE - END
