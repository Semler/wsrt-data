// EvenPolynomial_1000.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/EvenPolynomial.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class EvenPolynomial<DComplex>;
template class EvenPolynomial<Complex>;
} //# NAMESPACE - END
