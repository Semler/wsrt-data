// Gaussian2DParam_1030.cc -- Tue Apr  1 11:58:29 BST 2008 -- renting
#include <scimath/Functionals/Gaussian2DParam.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class Gaussian2DParam<AutoDiffA<Double> >;
template class Gaussian2DParam<AutoDiffA<Float> >;
} //# NAMESPACE - END
