// Array_1050.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<AutoDiffA<DComplex> >;
#ifdef AIPS_SUN_NATIVE
template class Array<AutoDiffA<DComplex> >::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
