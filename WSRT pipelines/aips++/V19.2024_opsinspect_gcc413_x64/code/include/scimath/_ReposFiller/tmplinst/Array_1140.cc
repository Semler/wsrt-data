// Array_1140.cc -- Tue Apr  1 11:58:26 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <scimath/Mathematics/RigidVector.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<RigidVector<Double, 3> >;
template class Array<RigidVector<Double, 2> >;
} //# NAMESPACE - END
