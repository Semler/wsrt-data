// Interpolate1D_1010.cc -- Tue Apr  1 11:58:30 BST 2008 -- renting
#include <scimath/Functionals/Interpolate1D.cc>
#include <scimath/Functionals/Function1D.h>
namespace casa { //# NAMESPACE - BEGIN
template class Interpolate1D<Double, Float>;
template class Function1D<Double, Float>;
} //# NAMESPACE - END
