// Functional_1050.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/BasicMath/Functional.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class Functional<DComplex, AutoDiff<DComplex> >;
} //# NAMESPACE - END
