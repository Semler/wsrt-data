// GenericL2Fit_1010.cc -- Tue Apr  1 11:58:28 BST 2008 -- renting
#include <scimath/Fitting/GenericL2Fit.cc>
#include <casa/BasicSL/Complex.h>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffA.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template class GenericL2Fit<AutoDiff<DComplex> >;
template class GenericL2Fit<AutoDiffA<DComplex> >;
} //# NAMESPACE - END
