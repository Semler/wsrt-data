// ListMap_1010.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Containers/ListMap.cc>
#include <scimath/Functionals/AbstractFunctionFactory.h>
namespace casa { //# NAMESPACE - BEGIN
template class ListMap<String, OrderedPair<FunctionFactory<Float>*, Bool> >;
template class ListMapRep<String, OrderedPair<FunctionFactory<Float>*, Bool> >;
template class ListMapIterRep<String, OrderedPair<FunctionFactory<Float>*, Bool> >;
} //# NAMESPACE - END
