// MatrixMathLA_1130.cc -- Tue Apr  1 11:58:31 BST 2008 -- renting
#include <scimath/Mathematics/MatrixMathLA.cc>
#include <casa/Arrays/Matrix.h>
namespace casa { //# NAMESPACE - BEGIN
template Matrix<Double> invertSymPosDef(Matrix<Double> const &);
template void invertSymPosDef(Matrix<Double> &, Double &, Matrix<Double> const &);
} //# NAMESPACE - END
