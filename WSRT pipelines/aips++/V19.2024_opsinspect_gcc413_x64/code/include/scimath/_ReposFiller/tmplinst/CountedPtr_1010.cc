// CountedPtr_1010.cc -- Tue Apr  1 11:58:27 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <scimath/Mathematics/AutoDiffA.h>
#include <casa/Containers/Block.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimpleCountedConstPtr<Block<AutoDiffA<DComplex> > >;
template class SimpleCountedPtr<Block<AutoDiffA<DComplex> > >;
template class CountedConstPtr<Block<AutoDiffA<DComplex> > >;
template class CountedPtr<Block<AutoDiffA<DComplex> > >;
template class PtrRep<Block<AutoDiffA<DComplex> > >;
} //# NAMESPACE - END
