// Functional_1000.cc -- Tue Apr  1 11:57:38 BST 2008 -- renting
#include <casa/BasicMath/Functional.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class Functional<Vector<Complex>, Vector<Complex> >;
template class Functional<Vector<DComplex>, Vector<DComplex> >;
} //# NAMESPACE - END
