// SimOrdMap_1000.cc -- Tue Apr  1 11:58:13 BST 2008 -- renting
#include <casa/Containers/SimOrdMap.cc>
#include <casa/Containers/PoolStack.h>
#include <scimath/Mathematics/AutoDiff.h>
namespace casa { //# NAMESPACE - BEGIN
template <class T> class AutoDiffRep;
template class SimpleOrderedMap<uInt, PoolStack<AutoDiffRep<AutoDiff<Double> >, uInt>*>;
} //# NAMESPACE - END
