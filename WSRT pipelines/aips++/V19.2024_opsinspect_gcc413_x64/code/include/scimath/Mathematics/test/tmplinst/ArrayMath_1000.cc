// ArrayMath_1000.cc -- Tue Apr  1 11:58:13 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffMath.h>
namespace casa { //# NAMESPACE - BEGIN
template void operator+=(Array<AutoDiff<Double> > &, AutoDiff<Double> const &);
template void operator-=(Array<AutoDiff<Double> > &, AutoDiff<Double> const &);
template void operator*=(Array<AutoDiff<Double> > &, AutoDiff<Double> const &);
template void operator/=(Array<AutoDiff<Double> > &, AutoDiff<Double> const &);
template void operator+=(Array<AutoDiff<Double> > &, Array<AutoDiff<Double> > const &);
template void operator-=(Array<AutoDiff<Double> > &, Array<AutoDiff<Double> > const &);
template void operator*=(Array<AutoDiff<Double> > &, Array<AutoDiff<Double> > const &);
template void operator/=(Array<AutoDiff<Double> > &, Array<AutoDiff<Double> > const &);
template Array<AutoDiff<Double> > operator-(Array<AutoDiff<Double> > const &);
} //# NAMESPACE - END
