// ArrayIO_1000.cc -- Tue Apr  1 11:58:13 BST 2008 -- renting
#include <casa/Arrays/ArrayIO.cc>
#include <scimath/Mathematics/AutoDiff.h>
#include <scimath/Mathematics/AutoDiffIO.h>
namespace casa { //# NAMESPACE - BEGIN
template ostream & operator<<(ostream &, Array<AutoDiff<Double> > const &);
} //# NAMESPACE - END
