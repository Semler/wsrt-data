// RFCubeLattice_1000.cc -- Tue Apr  1 12:24:01 BST 2008 -- renting
#include <flagging/Flagging/RFCubeLattice.cc>
#include <flagging/Flagging/RFChunkStats.h>
namespace casa { //# NAMESPACE - BEGIN
template class RFCubeLattice<Float>;
template class RFCubeLatticeIterator<Float>;
} //# NAMESPACE - END
