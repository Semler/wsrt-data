// CountedPtr_1000.cc -- Tue Apr  1 12:17:40 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <coordinates/Coordinates/CoordinateSystem.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<CoordinateSystem>;
template class CountedPtr<CoordinateSystem>;
template class PtrRep<CoordinateSystem>;
template class SimpleCountedConstPtr<CoordinateSystem>;
template class SimpleCountedPtr<CoordinateSystem>;
} //# NAMESPACE - END
