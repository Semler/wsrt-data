// MWCAniTemplates_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <display/DisplayEvents/MWCAniTemplates.cc>
namespace casa { //# NAMESPACE - BEGIN
template void MWCAnimator::setLinearRestriction<uInt>(String const &, uInt const &, uInt const &, uInt const &);
template void MWCAnimator::setLinearRestriction<Int>(String const &, Int const &, Int const &, Int const &);
template void MWCAnimator::setLinearRestriction<Float>(String const &, Float const &, Float const &, Float const &);
template void MWCAnimator::setLinearRestriction<Double>(String const &, Double const &, Double const &, Double const &);
} //# NAMESPACE - END
