// Register_1010.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Utilities/Register.cc>
#include <casa/Containers/List.h>
#include <display/DisplayEvents/DisplayEH.h>
#include <display/DisplayEvents/WCMotionEH.h>
#include <display/DisplayEvents/WCPositionEH.h>
namespace casa { //# NAMESPACE - BEGIN
template uInt Register(ListNotice<DisplayEH *> const *);;
template uInt Register(ListNotice<WCMotionEH *> const *);
template uInt Register(ListNotice<WCPositionEH *> const *);
} //# NAMESPACE - END
