// TblAsContourDDTemplates_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <display/DisplayDatas/TblAsContourDDTemplates.cc>
namespace casa { //# NAMESPACE - BEGIN
template Bool TblAsContourDD::getColumnKeyword<String>(String &value, String const, Regex const &) const;
template Bool TblAsContourDD::getColumnKeyword<String>(String &value, String const, String const) const;
} //# NAMESPACE - END
