// Register_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Utilities/Register.cc>
#include <casa/Containers/List.h>
#include <display/Display/WorldCanvas.h>
#include <display/Display/WorldCanvasHolder.h>
#include <display/Display/MultiWCHolder.h>
#include <display/DisplayDatas/DisplayData.h>
#include <display/DisplayShapes/DSClosed.h>
#include <display/DisplayShapes/DisplayShape.h>
#include <display/DisplayEvents/WCRefreshEH.h>
namespace casa { //# NAMESPACE - BEGIN
template uInt Register(ListNotice<WorldCanvas *> const *);;
template uInt Register(ListNotice<WorldCanvasHolder *> const *);
template uInt Register(ListNotice<MultiWCHolder *> const *);
template uInt Register(ListNotice<DisplayData *> const *);
template uInt Register(ListNotice<DisplayShape *> const *);
template uInt Register(ListNotice<DSClosed *> const *);
template uInt Register(ListNotice<WCRefreshEH *> const *);
} //# NAMESPACE - END
