// PassiveTableDDTemplates_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <display/DisplayDatas/PassiveTableDDTemplates.cc>
namespace casa { //# NAMESPACE - BEGIN
template Bool PassiveTableDD::getTableKeyword<uInt>(uInt &value, String const) const;
template Bool PassiveTableDD::getTableKeyword<Int>(Int &value, String const) const;
template Bool PassiveTableDD::getTableKeyword<Bool>(Bool &value, String const) const;
template Bool PassiveTableDD::getTableKeyword<Float>(Float &value, String const) const;
template Bool PassiveTableDD::getTableKeyword<Double>(Double &value, String const) const;
template Bool PassiveTableDD::getTableKeyword<String>(String &value, String const) const;
template Bool PassiveTableDD::getTableKeyword<uInt>(uInt &value, Regex const &) const;
template Bool PassiveTableDD::getTableKeyword<Int>(Int &value, Regex const &) const;
template Bool PassiveTableDD::getTableKeyword<Bool>(Bool &value, Regex const &) const;
template Bool PassiveTableDD::getTableKeyword<Float>(Float &value, Regex const &) const;
template Bool PassiveTableDD::getTableKeyword<Double>(Double &value, Regex const &) const;
template Bool PassiveTableDD::getTableKeyword<String>(String &value, Regex const &) const;
template Bool PassiveTableDD::getColumnKeyword<uInt>(uInt &value, String const, String const) const;
template Bool PassiveTableDD::getColumnKeyword<Int>(Int &value, String const, String const) const;
template Bool PassiveTableDD::getColumnKeyword<Bool>(Bool &value, String const, String const) const;
template Bool PassiveTableDD::getColumnKeyword<Float>(Float &value, String const, String const) const;
template Bool PassiveTableDD::getColumnKeyword<Double>(Double &value, String const, String const) const;
template Bool PassiveTableDD::getColumnKeyword<String>(String &value, String const, String const) const;
template Bool PassiveTableDD::getColumnKeyword<uInt>(uInt &value, String const, Regex const &) const;
template Bool PassiveTableDD::getColumnKeyword<Int>(Int &value, String const, Regex const &) const;
template Bool PassiveTableDD::getColumnKeyword<Bool>(Bool &value, String const, Regex const &) const;
template Bool PassiveTableDD::getColumnKeyword<Float>(Float &value, String const, Regex const &) const;
template Bool PassiveTableDD::getColumnKeyword<Double>(Double &value, String const, Regex const &) const;
template Bool PassiveTableDD::getColumnKeyword<String>(String &value, String const, Regex const &) const;
} //# NAMESPACE - END
