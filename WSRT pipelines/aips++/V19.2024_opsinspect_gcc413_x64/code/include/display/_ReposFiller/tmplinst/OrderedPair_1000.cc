// OrderedPair_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Containers/OrderedPair.cc>
#include <display/Display/X11PixelCanvasColorTable.h>
#include <display/Display/PixelCanvasColorTable.h>
#include <display/Display/ColormapManager.h>
#include <display/Display/PanelDisplay.h>
#include <display/DisplayEvents/MultiWCTool.h>
#include <display/Display/SlicePanelDisplay.h>
#include <display/Display/WorldCanvas.h>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedPair<String, X11PixelCanvasColorTable *>;
template class OrderedPair<uLong, X11PixelCanvasColorTable *>;
template class OrderedPair<PixelCanvasColorTable *, uInt>;
template class OrderedPair<Colormap const *, ColormapInfo *>;
template class OrderedPair<String, MultiWCTool*>;
template class OrderedPair<String, PanelDisplay*>;
template class OrderedPair<String, SliceEH*>;
template class OrderedPair<void*, WorldCanvas::ColorIndexedImage_*>;
} //# NAMESPACE - END
