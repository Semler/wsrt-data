// SimOrdMap_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Containers/SimOrdMap.cc>
#include <display/Display/X11PixelCanvasColorTable.h>
#include <display/Display/PixelCanvasColorTable.h>
#include <display/Display/ColormapManager.h>
#include <display/Display/PanelDisplay.h>
#include <display/DisplayEvents/MultiWCTool.h>
#include <display/Display/SlicePanelDisplay.h>
#include <display/Display/WorldCanvas.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimpleOrderedMap<String, X11PixelCanvasColorTable *>;
template class SimpleOrderedMap<uLong, X11PixelCanvasColorTable *>;
template class SimpleOrderedMap<PixelCanvasColorTable *, uInt>;
template class SimpleOrderedMap<Colormap const *, ColormapInfo *>;
template class SimpleOrderedMap<String, MultiWCTool*>;
template class SimpleOrderedMap<String, PanelDisplay*>;
template class SimpleOrderedMap<String, SliceEH*>;
template class SimpleOrderedMap<void*, WorldCanvas::ColorIndexedImage_*>;
} //# NAMESPACE - END
