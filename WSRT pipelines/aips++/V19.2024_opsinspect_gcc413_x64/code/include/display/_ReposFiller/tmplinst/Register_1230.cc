// Register_1230.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Utilities/Register.cc>
#include <casa/Containers/List.h>
#include <display/QtViewer/QtDisplayData.qo.h>
namespace casa { //# NAMESPACE - BEGIN
#ifdef HAVE_QT4
template uInt Register(ListNotice<QtDisplayData *> const *);
#endif
} //# NAMESPACE - END
