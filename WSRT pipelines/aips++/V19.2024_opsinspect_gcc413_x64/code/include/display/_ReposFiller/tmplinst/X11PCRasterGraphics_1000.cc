// X11PCRasterGraphics_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <display/Display/X11PCRasterGraphics.cc>
namespace casa { //# NAMESPACE - BEGIN
template void X11PixelCanvas_drawImage(X11PixelCanvas *, Matrix<uInt> const &, Int, Int);
template void X11PixelCanvas_drawImage(X11PixelCanvas *, Matrix<Int> const &, Int, Int);
template void X11PixelCanvas_drawImage(X11PixelCanvas *, Matrix<uLong> const &, Int, Int);
template void X11PixelCanvas_drawImage(X11PixelCanvas *, Matrix<Float> const &, Int, Int);
template void X11PixelCanvas_drawImage(X11PixelCanvas *, Matrix<Double> const &, Int, Int);
template void X11PixelCanvas_drawImage(X11PixelCanvas *, Matrix<uInt> const &, Int, Int, uInt, uInt);
template void X11PixelCanvas_drawImage(X11PixelCanvas *, Matrix<Int> const &, Int, Int, uInt, uInt);
template void X11PixelCanvas_drawImage(X11PixelCanvas *, Matrix<uLong> const &, Int, Int, uInt, uInt);
template void X11PixelCanvas_drawImage(X11PixelCanvas *, Matrix<Float> const &, Int, Int, uInt, uInt);
template void X11PixelCanvas_drawImage(X11PixelCanvas *, Matrix<Double> const &, Int, Int, uInt, uInt);
} //# NAMESPACE - END
