// MatrixMath_1000.cc -- Tue Apr  1 12:33:31 BST 2008 -- renting
#include <casa/Arrays/MatrixMath.cc>
namespace casa { //# NAMESPACE - BEGIN
template Matrix<Double> product(Matrix<Double> const &, Matrix<Double> const &);
template Matrix<Float> product(Matrix<Float> const &, Matrix<Float> const &);
} //# NAMESPACE - END
