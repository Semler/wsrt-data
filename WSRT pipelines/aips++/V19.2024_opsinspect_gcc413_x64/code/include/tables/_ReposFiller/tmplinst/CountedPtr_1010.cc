// CountedPtr_1010.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <tables/TablePlot/BasePlot.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<BasePlot<Float> > >;
template class CountedPtr<Block<BasePlot<Float> > >;
template class PtrRep<Block<BasePlot<Float> > >;
template class SimpleCountedConstPtr<Block<BasePlot<Float> > >;
template class SimpleCountedPtr<Block<BasePlot<Float> > >;
} //# NAMESPACE - END
