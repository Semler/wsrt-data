// MappedArrayEngine_1000.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <tables/Tables/MappedArrayEngine.cc>
#include <tables/Tables/BaseMappedArrayEngine.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class MappedArrayEngine<Complex, DComplex>;
template class BaseMappedArrayEngine<Complex, DComplex>;
} //# NAMESPACE - END
