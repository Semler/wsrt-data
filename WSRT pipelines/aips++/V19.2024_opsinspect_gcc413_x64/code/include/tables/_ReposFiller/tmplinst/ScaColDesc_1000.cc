// ScaColDesc_1000.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <tables/Tables/ScaColDesc.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template class ScalarColumnDesc<Bool>;
template class ScalarColumnDesc<Char>;
template class ScalarColumnDesc<uChar>;
template class ScalarColumnDesc<Short>;
template class ScalarColumnDesc<uShort>;
template class ScalarColumnDesc<Int>;
template class ScalarColumnDesc<uInt>;
template class ScalarColumnDesc<Float>;
template class ScalarColumnDesc<Double>;
template class ScalarColumnDesc<Complex>;
template class ScalarColumnDesc<DComplex>;
template class ScalarColumnDesc<String>;
} //# NAMESPACE - END
