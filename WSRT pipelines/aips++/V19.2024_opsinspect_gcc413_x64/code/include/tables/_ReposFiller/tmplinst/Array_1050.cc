// Array_1050.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <tables/Tables/TableRecord.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<TableRecord>;
#ifdef AIPS_SUN_NATIVE
template class Array<TableRecord>::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
