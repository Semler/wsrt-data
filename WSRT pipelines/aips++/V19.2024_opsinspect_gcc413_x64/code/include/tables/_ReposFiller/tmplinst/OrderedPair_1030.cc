// OrderedPair_1030.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <casa/Containers/OrderedPair.cc>
#include <tables/Tables/DataManager.h>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedPair<String, DataManager *(*)(String const &, Record const &)>;
} //# NAMESPACE - END
