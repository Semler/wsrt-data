// BaseMappedArrayEngine_1000.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <tables/Tables/BaseMappedArrayEngine.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class BaseMappedArrayEngine<Complex, Int>;
template class BaseMappedArrayEngine<Float, Short>;
} //# NAMESPACE - END
