// CountedPtr_1060.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <tables/Tables/TiledFileAccess.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<TiledFileAccess>;
template class CountedPtr<TiledFileAccess>;
template class PtrRep<TiledFileAccess>;
template class SimpleCountedConstPtr<TiledFileAccess>;
template class SimpleCountedPtr<TiledFileAccess>;
} //# NAMESPACE - END
