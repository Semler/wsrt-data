// TableVector_1000.cc -- Tue Apr  1 12:09:54 BST 2008 -- renting
#include <tables/Tables/TableVector.cc>
#include <tables/Tables/TVec.cc>
#include <tables/Tables/TVecScaCol.cc>
#include <tables/Tables/TVecTemp.cc>
namespace casa { //# NAMESPACE - BEGIN
template class ROTableVector<Float>;
template class TableVector<Float>;
template class TabVecRep<Float>;
template class TabVecScaCol<Float>;
template class TabVecTemp<Float>;
} //# NAMESPACE - END
