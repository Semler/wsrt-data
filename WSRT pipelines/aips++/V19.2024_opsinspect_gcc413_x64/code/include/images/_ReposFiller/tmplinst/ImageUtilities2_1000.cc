// ImageUtilities2_1000.cc -- Tue Apr  1 12:18:59 BST 2008 -- renting
#include <images/Images/ImageUtilities2.cc>
#include <images/Images/ImageInterface.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template void ImageUtilities::copyMiscellaneous<Float, Float>(ImageInterface<Float> &, ImageInterface<Float> const &);
template void ImageUtilities::copyMiscellaneous<Complex, Float>(ImageInterface<Complex> &, ImageInterface<Float> const &);
template void ImageUtilities::fitProfiles<Float>(ImageInterface<Float>* &, ImageInterface<Float>* &, ImageInterface<Float> const &, ImageInterface<Float>* &, uInt, uInt, Int, Bool);
} //# NAMESPACE - END
