// ScalarQuantColumn_1000.cc -- Tue Apr  1 12:13:35 BST 2008 -- renting
#include <measures/TableMeasures/ScalarQuantColumn.cc>
#include <casa/Quanta/Quantum.h>
namespace casa { //# NAMESPACE - BEGIN
template class ROScalarQuantColumn<Complex>;
template class ROScalarQuantColumn<Int>;
template class ScalarQuantColumn<Complex>;
template class ScalarQuantColumn<Int>;
} //# NAMESPACE - END
