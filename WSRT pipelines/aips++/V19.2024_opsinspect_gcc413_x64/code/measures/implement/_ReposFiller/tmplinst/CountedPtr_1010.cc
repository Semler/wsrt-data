// CountedPtr_1010.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <measures/Measures/Stokes.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<Stokes::StokesTypes> >;
template class CountedConstPtr<Block<Stokes::StokesTypes> >;
template class SimpleCountedPtr<Block<Stokes::StokesTypes> >;
template class SimpleCountedConstPtr<Block<Stokes::StokesTypes> >;
template class PtrRep<Block<Stokes::StokesTypes> >;
} //# NAMESPACE - END
