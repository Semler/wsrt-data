// LinearSearch_1000.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <casa/Utilities/LinearSearch.cc>
#include <casa/Arrays/Vector.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template Int linearSearch1 (const Vector<String>&, const String&, uInt);
template Int linearSearchBrackets (Bool&, const uInt* const &, const uInt&, uInt, uInt);
} //# NAMESPACE - END
