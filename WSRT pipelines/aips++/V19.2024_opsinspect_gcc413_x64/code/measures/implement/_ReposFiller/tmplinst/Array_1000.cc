// Array_1000.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <measures/Measures/MDirection.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<MDirection>;
#ifdef AIPS_SUN_NATIVE
template class Array<MDirection>::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
