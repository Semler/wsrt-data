// ScalarQuantColumn_1000.cc -- Tue Apr  1 12:12:29 BST 2008 -- renting
#include <measures/TableMeasures/ScalarQuantColumn.cc>
namespace casa { //# NAMESPACE - BEGIN
template class ROScalarQuantColumn<Double>;
template class ROScalarQuantColumn<Float>;
template class ScalarQuantColumn<Double>;
template class ScalarQuantColumn<Float>;
} //# NAMESPACE - END
