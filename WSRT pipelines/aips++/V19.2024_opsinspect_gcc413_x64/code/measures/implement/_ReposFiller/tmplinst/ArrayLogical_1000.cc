// ArrayLogical_1000.cc -- Tue Apr  1 12:12:28 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
#include <measures/Measures/Stokes.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allEQ(Array<Stokes::StokesTypes> const &, Array<Stokes::StokesTypes> const &);
template Bool anyNE(Array<Stokes::StokesTypes> const &, Array<Stokes::StokesTypes> const &);
} //# NAMESPACE - END
