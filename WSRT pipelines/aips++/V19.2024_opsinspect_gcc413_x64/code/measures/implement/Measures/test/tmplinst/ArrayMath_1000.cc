// ArrayMath_1000.cc -- Tue Apr  1 12:11:58 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Quanta/Quantum.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<Quantum<Double> > operator*(Quantum<Double> const &, Array<Quantum<Double> > const &);
template Array<Quantum<Double> > operator-(Array<Quantum<Double> > const &);
template void operator*=(Array<Quantum<Double> > &, Array<Quantum<Double> > const &);
} //# NAMESPACE - END
