/* sos/linux_gnu/config_p.h.  Generated automatically by configure.  */
/* $Id: config_p.h.in,v 19.1 2004/07/13 22:37:01 dschieb Exp $
** Copyright (c) 1993 The Regents of the University of California.
** Copyright (c) 1997 Associated Universities Inc.
*/

/***************************************************************/
/* custom config.h template - do not recreate using autoheader */
/***************************************************************/

/* Define to `void*' if <sys/types.h> and <stdlib.h> don't define.  */
#define malloc_t void*

/* Define const to be empty of C compiler does not support it.  */
/* #undef const */

/* Define if you can include <iostream.h> without the compiler whining. */
#define HAVE_IOSTREAM_H 1
