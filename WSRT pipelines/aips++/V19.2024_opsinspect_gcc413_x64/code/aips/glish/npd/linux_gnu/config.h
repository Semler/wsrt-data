/* npd/linux_gnu/config.h.  Generated automatically by configure.  */
/* $Id: config.h.in,v 19.0 2003/07/16 05:16:57 aips2adm Exp $
**
*/

/***************************************************************/
/* custom config.h template - do not recreate using autoheader */
/***************************************************************/

/* Define if you have <sys/file.h>.  */
/* #undef HAVE_SYS_FILE_H */

/* Define if you have <sys/time.h>.  */
#define HAVE_SYS_TIME_H 1

/* Define if you have <sys/utsname.h>.  */
/* #undef HAVE_SYS_UTSNAME_H */

/* Define if you have <unistd.h>.  */
#define HAVE_UNISTD_H 1

/* Define if you have <stdlib.h>.  */
#define HAVE_STDLIB_H 1

/* Define if you have the random() BSD function.  */
#define HAVE_RANDOM 1

/* Define if you have the lrand48() SYSV function.  */
#define HAVE_LRAND48 1

/* Define if you have the flock() BSD function.  */
#define HAVE_FLOCK 1

/* Define if you have the lockf() SYSV function.  */
#define HAVE_LOCKF 1

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* Define if you have vprintf() function.  */
#define HAVE_VPRINTF 1

/* Define if you have the gethostname function.  */
#define HAVE_GETHOSTNAME 1

/* Define if compiler does not support const keyword */
/* #undef const */
