// AipsIOCarray_1010.cc -- Tue Apr  1 11:49:53 BST 2008 -- renting
#include <casa/IO/AipsIOCarray.cc>
#include <casa/IO/test/tAipsIOCarray.h>
namespace casa { //# NAMESPACE - BEGIN
template void putAipsIO(AipsIO &, uInt, AipsIOCarrayEx1 const *);
template void getAipsIO(AipsIO &, uInt, AipsIOCarrayEx1 *);
template void getnewAipsIO(AipsIO &, uInt &, AipsIOCarrayEx1 **);
} //# NAMESPACE - END
