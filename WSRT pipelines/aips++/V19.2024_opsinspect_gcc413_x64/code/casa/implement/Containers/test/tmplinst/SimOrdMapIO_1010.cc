// SimOrdMapIO_1010.cc -- Tue Apr  1 11:49:34 BST 2008 -- renting
#include <casa/Containers/SimOrdMapIO.cc>
#include <casa/IO/AipsIO.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template AipsIO & operator<<(AipsIO &, SimpleOrderedMap<String, Int> const &);
} //# NAMESPACE - END
