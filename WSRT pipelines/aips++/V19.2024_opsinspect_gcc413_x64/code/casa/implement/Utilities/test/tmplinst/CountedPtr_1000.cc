// CountedPtr_1000.cc -- Tue Apr  1 11:57:26 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Utilities/Regex.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Regex> >;
template class CountedPtr<Block<Regex> >;
template class PtrRep<Block<Regex> >;
template class SimpleCountedConstPtr<Block<Regex> >;
template class SimpleCountedPtr<Block<Regex> >;
} //# NAMESPACE - END
