// ArrayLogical_1000.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<Bool> operator!=(Array<Complex> const &, Array<Complex> const &);
template Array<Bool> operator!=(Array<Complex> const &, Complex const &);
template Array<Bool> operator!=(Complex const &, Array<Complex> const &);
template Array<Bool> operator<(Array<Complex> const &, Array<Complex> const &);
template Array<Bool> operator<(Array<Complex> const &, Complex const &);
template Array<Bool> operator<(Complex const &, Array<Complex> const &);
template Array<Bool> operator<=(Array<Complex> const &, Array<Complex> const &);
template Array<Bool> operator<=(Array<Complex> const &, Complex const &);
template Array<Bool> operator<=(Complex const &, Array<Complex> const &);
template Array<Bool> operator==(Array<Complex> const &, Array<Complex> const &);
template Array<Bool> operator==(Array<Complex> const &, Complex const &);
template Array<Bool> operator==(Complex const &, Array<Complex> const &);
template Array<Bool> operator>(Array<Complex> const &, Array<Complex> const &);
template Array<Bool> operator>(Array<Complex> const &, Complex const &);
template Array<Bool> operator>(Complex const &, Array<Complex> const &);
template Array<Bool> operator>=(Array<Complex> const &, Array<Complex> const &);
template Array<Bool> operator>=(Array<Complex> const &, Complex const &);
template Array<Bool> operator>=(Complex const &, Array<Complex> const &);
template Bool allEQ(Array<Complex> const &, Array<Complex> const &);
template Bool allNear(Array<Complex> const &, Complex const &, Double);
template Bool allNearAbs(Array<Complex> const &, Complex const &, Double);
} //# NAMESPACE - END
