// CountedPtr_1110.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Array<Float> >;
template class CountedPtr<Array<Float> >;
template class PtrRep<Array<Float> >;
template class SimpleCountedConstPtr<Array<Float> >;
template class SimpleCountedPtr<Array<Float> >;
} //# NAMESPACE - END
