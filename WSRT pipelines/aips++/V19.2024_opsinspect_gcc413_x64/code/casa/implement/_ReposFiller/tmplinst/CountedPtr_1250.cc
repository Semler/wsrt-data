// CountedPtr_1250.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Quanta/MVFrequency.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<MVFrequency> >;
template class CountedPtr<Block<MVFrequency> >;
template class PtrRep<Block<MVFrequency> >;
template class SimpleCountedConstPtr<Block<MVFrequency> >;
template class SimpleCountedPtr<Block<MVFrequency> >;
} //# NAMESPACE - END
