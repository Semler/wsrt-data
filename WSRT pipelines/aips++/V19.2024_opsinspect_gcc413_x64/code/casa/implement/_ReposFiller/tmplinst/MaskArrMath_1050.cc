// MaskArrMath_1050.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/MaskArrMath.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template const MaskedArray<Complex> & operator+=(MaskedArray<Complex> const &, MaskedArray<Complex> const &);
} //# NAMESPACE - END
