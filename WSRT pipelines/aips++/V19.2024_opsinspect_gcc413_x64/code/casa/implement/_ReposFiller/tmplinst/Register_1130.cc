// Register_1130.cc -- Tue Apr  1 11:50:47 BST 2008 -- renting
#include <casa/Utilities/Register.cc>
#include <casa/Containers/List.h>
#include <casa/Utilities/CountedPtr.h>
#include <casa/OS/SysEvent.h>
namespace casa { //# NAMESPACE - BEGIN
template uInt Register(ListNotice<CountedPtr<SysEventTargetInfo> > const *);
} //# NAMESPACE - END
