// CountedPtr_1480.cc -- Tue Apr  1 11:50:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/OS/SysEvent.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<SysEventTargetInfo>;
template class CountedPtr<SysEventTargetInfo>;
template class PtrRep<SysEventTargetInfo>;
template class SimpleCountedConstPtr<SysEventTargetInfo>;
template class SimpleCountedPtr<SysEventTargetInfo>;
} //# NAMESPACE - END
