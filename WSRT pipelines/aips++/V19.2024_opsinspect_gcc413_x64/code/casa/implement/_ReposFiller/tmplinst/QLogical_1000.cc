// QLogical_1000.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Quanta/QLogical.cc>
#include <casa/Quanta/Quantum.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool near(Quantum<Double> const &, Quantum<Double> const &);
template Bool nearAbs(Quantum<Double> const &, Quantum<Double> const &, Double);
template Bool operator<=(Quantum<Double> const &, Quantum<Double> const &);
template Bool operator>(Quantum<Double> const &, Quantum<Double> const &);
template Bool operator>=(Quantum<Double> const &, Quantum<Double> const &);
template Bool operator<(Quantum<Double> const &, Quantum<Double> const &);
template Bool operator!=(Quantum<Double> const &, Quantum<Double> const &);
template Bool operator==(Quantum<Double> const &, Quantum<Double> const &);
} //# NAMESPACE - END
