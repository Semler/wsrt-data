// CountedPtr_1280.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Quanta/MVTime.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Block<MVTime> >;
template class SimpleCountedPtr<Block<MVTime> >;
template class CountedConstPtr<Block<MVTime> >;
template class SimpleCountedConstPtr<Block<MVTime> >;
template class PtrRep<Block<MVTime> >;
} //# NAMESPACE - END
