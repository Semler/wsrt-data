// Array_1140.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <casa/Quanta/QuantumHolder.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<QuantumHolder>;
#ifdef AIPS_SUN_NATIVE
template class Array<QuantumHolder>::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
