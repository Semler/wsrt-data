// OrderedPair_1190.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/OrderedPair.cc>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedPair<Int, Double>;
template class OrderedPair<Int, uInt>;
template class OrderedPair<Double, uInt>;
template class OrderedPair<Double, Int>;
} //# NAMESPACE - END
