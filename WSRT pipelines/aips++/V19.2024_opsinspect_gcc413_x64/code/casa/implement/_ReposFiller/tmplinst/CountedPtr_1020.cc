// CountedPtr_1020.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Quanta/QuantumHolder.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<QuantumHolder> >;
template class CountedPtr<Block<QuantumHolder> >;
template class PtrRep<Block<QuantumHolder> >;
template class SimpleCountedConstPtr<Block<QuantumHolder> >;
template class SimpleCountedPtr<Block<QuantumHolder> >;
} //# NAMESPACE - END
