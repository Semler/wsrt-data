// ArrayLogical_1090.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allEQ(Array<uInt> const &, Array<uInt> const &);
template Bool allEQ(Array<uInt> const &, uInt const &);
template Bool allGE(Array<uInt> const &, uInt const &);
template Bool allLT(Array<uInt> const &, uInt const &);
template Bool anyEQ(Array<uInt> const &, uInt const &);
template Bool anyGE(Array<uInt> const &, uInt const &);
template Bool anyGT(Array<uInt> const &, uInt const &);
template Bool anyLE(Array<uInt> const &, uInt const &);
template Bool anyLT(Array<uInt> const &, uInt const &);
template Bool anyNE(Array<uInt> const &, uInt const &);
template Bool anyNE(Array<uInt> const &, Array<uInt> const &);
template Array<Bool> operator!=(Array<uInt> const &, uInt const &);
template Array<Bool> operator==(Array<uInt> const &, uInt const &);
template Array<Bool> operator>(Array<uInt> const &, uInt const &);
template Array<Bool> operator<(Array<uInt> const &, uInt const &);
template Array<Bool> operator||(Array<uInt> const &, Array<uInt> const &);
} //# NAMESPACE - END
