// ArrayIO_1010.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayIO.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template AipsIO & operator>>(AipsIO &, Array<Bool> &);
template AipsIO & operator>>(AipsIO &, Array<uChar> &);
template AipsIO & operator>>(AipsIO &, Array<Short> &);
template AipsIO & operator>>(AipsIO &, Array<Int> &);
template AipsIO & operator>>(AipsIO &, Array<uInt> &);
template AipsIO & operator>>(AipsIO &, Array<Float> &);
template AipsIO & operator>>(AipsIO &, Array<Double> &);
template AipsIO & operator>>(AipsIO &, Array<Complex> &);
template AipsIO & operator>>(AipsIO &, Array<DComplex> &);
template AipsIO & operator>>(AipsIO &, Array<String> &);
} //# NAMESPACE - END
