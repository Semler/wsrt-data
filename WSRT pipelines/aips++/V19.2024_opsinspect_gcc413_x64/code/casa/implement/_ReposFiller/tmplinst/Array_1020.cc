// Array_1020.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<Vector<Double> >;
#ifdef AIPS_SUN_NATIVE
template class Array<Vector<Double> >::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
