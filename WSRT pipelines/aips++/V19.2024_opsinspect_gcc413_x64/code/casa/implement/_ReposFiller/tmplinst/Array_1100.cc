// Array_1100.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/Array.cc>
#include <casa/Quanta/MVRadialVelocity.h>
namespace casa { //# NAMESPACE - BEGIN
template class Array<MVRadialVelocity>;
#ifdef AIPS_SUN_NATIVE
template class Array<MVRadialVelocity>::ConstIteratorSTL;
#endif
} //# NAMESPACE - END
