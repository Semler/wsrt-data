// CountedPtr_1140.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/BasicMath/Random.h>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimpleCountedConstPtr<Random>;
template class SimpleCountedPtr<Random>;
template class CountedConstPtr<Random>;
template class CountedPtr<Random>;
template class PtrRep<Random>;
} //# NAMESPACE - END
