// ArrayLogical_1070.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool allEQ(Array<Short> const &, Array<Short> const &);
template Array<Bool> operator==(Array<Short> const &, Short const &);
template Array<Bool> operator>(Array<Short> const &, Short const &);
} //# NAMESPACE - END
