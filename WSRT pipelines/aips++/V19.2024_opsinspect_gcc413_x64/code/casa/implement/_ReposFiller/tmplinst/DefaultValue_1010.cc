// DefaultValue_1010.cc -- Tue Apr  1 11:50:47 BST 2008 -- renting
#include <casa/Utilities/DefaultValue.h>
namespace casa { //# NAMESPACE - BEGIN
template void defaultValue(Bool &);
template void defaultValue(Double &);
template void defaultValue(Float &);
template void defaultValue(Int &);
} //# NAMESPACE - END
