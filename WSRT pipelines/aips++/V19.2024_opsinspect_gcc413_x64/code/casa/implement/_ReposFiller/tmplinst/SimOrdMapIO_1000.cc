// SimOrdMapIO_1000.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/SimOrdMapIO.cc>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template AipsIO & operator>>(AipsIO &, SimpleOrderedMap<String, String> &);
template AipsIO & operator<<(AipsIO &, SimpleOrderedMap<String, String> const &);
} //# NAMESPACE - END
