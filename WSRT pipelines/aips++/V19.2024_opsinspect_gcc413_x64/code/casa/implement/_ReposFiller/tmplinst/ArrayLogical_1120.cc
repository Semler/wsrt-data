// ArrayLogical_1120.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayLogical.cc>
namespace casa { //# NAMESPACE - BEGIN
template Array<uInt> partialNTrue(Array<Bool> const &, IPosition const &);
template Array<uInt> partialNFalse(Array<Bool> const &, IPosition const &);
} //# NAMESPACE - END
