// ArrayMath_1120.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Array<uInt> operator*(Array<uInt> const &, uInt const &);
template Array<uInt> operator-(Array<uInt> const &, uInt const &);
template Array<uInt> operator/(Array<uInt> const &, uInt const &);
template uInt max(Array<uInt> const &);
template uInt min(Array<uInt> const &);
template uInt sum(Array<uInt> const &);
template void indgen(Array<uInt> &);
template void indgen(Array<uInt> &, uInt);
template void indgen(Array<uInt> &, uInt, uInt);
template void minMax(uInt &, uInt &, Array<uInt> const &);
template void operator*=(Array<uInt> &, uInt const &);
template void operator+=(Array<uInt> &, uInt const &);
template void operator-=(Array<uInt> &, uInt const &);
template void operator/=(Array<uInt> &, uInt const &);
} //# NAMESPACE - END
