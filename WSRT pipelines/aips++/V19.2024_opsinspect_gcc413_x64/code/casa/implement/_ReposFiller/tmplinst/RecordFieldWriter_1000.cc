// RecordFieldWriter_1000.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/RecordFieldWriter.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template class RecordFieldCopier<Array<Bool>, Array<Bool> >;
template class RecordFieldCopier<Array<Complex>, Array<Complex> >;
template class RecordFieldCopier<Array<DComplex>, Array<DComplex> >;
template class RecordFieldCopier<Array<Double>, Array<Double> >;
template class RecordFieldCopier<Array<Float>, Array<Float> >;
template class RecordFieldCopier<Array<Int>, Array<Int> >;
template class RecordFieldCopier<Array<Short>, Array<Short> >;
template class RecordFieldCopier<Array<String>, Array<String> >;
template class RecordFieldCopier<Array<uChar>, Array<uChar> >;
} //# NAMESPACE - END
