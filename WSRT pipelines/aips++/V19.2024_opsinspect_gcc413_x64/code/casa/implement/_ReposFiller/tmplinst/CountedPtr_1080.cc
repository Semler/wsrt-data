// CountedPtr_1080.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<Array<Complex> >;
template class PtrRep<Array<Complex> >;
template class SimpleCountedConstPtr<Array<Complex> >;
template class CountedConstPtr<Array<Complex> >;
template class SimpleCountedPtr<Array<Complex> >;
} //# NAMESPACE - END
