// CountedPtr_1190.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/BasicMath/Random.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<CountedPtr<Random> > >;
template class CountedPtr<Block<CountedPtr<Random> > >;
template class SimpleCountedConstPtr<Block<CountedPtr<Random> > >;
template class SimpleCountedPtr<Block<CountedPtr<Random> > >;
template class PtrRep<Block<CountedPtr<Random> > >;
} //# NAMESPACE - END
