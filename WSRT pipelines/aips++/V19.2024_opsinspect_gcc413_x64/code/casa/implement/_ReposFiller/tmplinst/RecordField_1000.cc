// RecordField_1000.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/RecordField.cc>
#include <casa/Arrays/Array.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class RORecordFieldPtr<Array<Complex> >;
template class RORecordFieldPtr<Array<DComplex> >;
template class RORecordFieldPtr<Array<Bool> >;
template class RORecordFieldPtr<Array<Char> >;
template class RORecordFieldPtr<Array<Double> >;
template class RORecordFieldPtr<Array<Float> >;
template class RORecordFieldPtr<Array<Int> >;
template class RORecordFieldPtr<Array<Short> >;
template class RORecordFieldPtr<Array<String> >;
template class RORecordFieldPtr<Array<uChar> >;
template class RORecordFieldPtr<Array<uInt> >;
} //# NAMESPACE - END
