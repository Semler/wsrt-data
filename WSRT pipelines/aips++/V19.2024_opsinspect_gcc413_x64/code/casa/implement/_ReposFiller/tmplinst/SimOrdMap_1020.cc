// SimOrdMap_1020.cc -- Tue Apr  1 11:50:45 BST 2008 -- renting
#include <casa/Containers/SimOrdMap.cc>
#include <casa/Arrays/Vector.h>
namespace casa { //# NAMESPACE - BEGIN
template class SimpleOrderedMap<Int, Vector<uInt> >;
template class SimpleOrderedMap<Int, Vector<Int> >;
template class SimpleOrderedMap<Int, Vector<Double> >;
} //# NAMESPACE - END
