// CountedPtr_1260.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Quanta/MVPosition.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<MVPosition> >;
template class CountedPtr<Block<MVPosition> >;
template class PtrRep<Block<MVPosition> >;
template class SimpleCountedConstPtr<Block<MVPosition> >;
template class SimpleCountedPtr<Block<MVPosition> >;
} //# NAMESPACE - END
