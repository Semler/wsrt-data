// CountedPtr_1380.cc -- Tue Apr  1 11:50:46 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<uInt> >;
template class CountedPtr<Block<uInt> >;
template class PtrRep<Block<uInt> >;
template class SimpleCountedConstPtr<Block<uInt> >;
template class SimpleCountedPtr<Block<uInt> >;
} //# NAMESPACE - END
