// OrderedMap_1010.cc -- Tue Apr  1 11:50:44 BST 2008 -- renting
#include <casa/Containers/OrderedMap.cc>
#include <casa/BasicSL/String.h>
namespace casa { //# NAMESPACE - BEGIN
template class OrderedMapRep<String, Int>;
template class OrderedMapIterRep<String, Int>;
template class OrderedMap<String, Int>;
template class OrderedMapNotice<String, Int>;
} //# NAMESPACE - END
