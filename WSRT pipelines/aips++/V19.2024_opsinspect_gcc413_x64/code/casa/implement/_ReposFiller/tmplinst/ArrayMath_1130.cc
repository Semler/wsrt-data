// ArrayMath_1130.cc -- Tue Apr  1 11:50:42 BST 2008 -- renting
#include <casa/Arrays/ArrayMath.cc>
#include <casa/Arrays/Array.h>
namespace casa { //# NAMESPACE - BEGIN
template Char max(Array<Char> const &);
template Char min(Array<Char> const &);
template void indgen(Array<Char> &);
template void indgen(Array<Char> &, Char, Char);
template void minMax(Char &, Char &, Array<Char> const &);
} //# NAMESPACE - END
