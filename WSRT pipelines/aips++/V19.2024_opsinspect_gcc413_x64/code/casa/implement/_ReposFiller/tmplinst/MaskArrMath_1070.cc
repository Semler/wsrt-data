// MaskArrMath_1070.cc -- Tue Apr  1 11:50:43 BST 2008 -- renting
#include <casa/Arrays/MaskArrMath.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template const MaskedArray<Float> & operator+=(MaskedArray<Float> const &, Array<Float> const &);
template Array<Float> & operator+=(Array<Float> &, MaskedArray<Float> const &);
template const MaskedArray<Complex> & operator+=(MaskedArray<Complex> const &, Array<Complex> const &);
} //# NAMESPACE - END
