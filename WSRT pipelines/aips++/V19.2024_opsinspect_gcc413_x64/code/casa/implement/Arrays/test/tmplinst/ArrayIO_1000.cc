// ArrayIO_1000.cc -- Tue Apr  1 11:49:18 BST 2008 -- renting
#include <casa/Arrays/ArrayIO.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template Bool read(istream &, Array<Complex> &, IPosition const *, Bool);
template Bool read(istream &, Array<Double> &, IPosition const *, Bool);
template Bool readArrayBlock(istream &, Bool &, IPosition &, Block<Complex> &, IPosition const *, Bool);
template Bool readArrayBlock(istream &, Bool &, IPosition &, Block<Double> &, IPosition const *, Bool);
template istream & operator>>(istream &, Array<Double> &);
template void readAsciiMatrix(Matrix<Int> &, Char const *);
template void readAsciiVector(Vector<Double> &, Char const *);
template void read_array(Array<Int> &, Char const *);
template void read_array(Array<Int> &, String const &);
template void writeAsciiMatrix(Matrix<Int> const &, Char const *);
template void writeAsciiVector(Vector<Double> const &, Char const *);
template void write_array(Array<Int> const &, Char const *);
template void write_array(Array<Int> const &, String const &);
} //# NAMESPACE - END
