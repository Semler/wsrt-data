// MaskArrLogi_1000.cc -- Tue Apr  1 11:49:18 BST 2008 -- renting
#include <casa/Arrays/MaskArrLogi.cc>
namespace casa { //# NAMESPACE - BEGIN
template Bool allAND(Bool const &, MaskedArray<Bool> const &);
template Bool allAND(MaskedArray<Bool> const &, Bool const &);
template Bool allEQ(MaskedArray<Bool> const &, MaskedArray<Bool> const &);
template Bool allGE(Int const &, MaskedArray<Int> const &);
template Bool allGT(Array<Int> const &, MaskedArray<Int> const &);
template Bool allLE(Array<Int> const &, MaskedArray<Int> const &);
template Bool allLE(Int const &, MaskedArray<Int> const &);
template Bool allLE(MaskedArray<Int> const &, Array<Int> const &);
template Bool allLE(MaskedArray<Int> const &, Int const &);
template Bool allLE(MaskedArray<Int> const &, MaskedArray<Int> const &);
template Bool allLT(MaskedArray<Int> const &, Int const &);
template Bool allLT(MaskedArray<Int> const &, MaskedArray<Int> const &);
template Bool allOR(Bool const &, MaskedArray<Bool> const &);
template Bool allOR(MaskedArray<Bool> const &, Bool const &);
template Bool anyAND(Array<Bool> const &, MaskedArray<Bool> const &);
template Bool anyAND(Bool const &, MaskedArray<Bool> const &);
template Bool anyAND(MaskedArray<Bool> const &, Array<Bool> const &);
template Bool anyAND(MaskedArray<Bool> const &, Bool const &);
template Bool anyAND(MaskedArray<Bool> const &, MaskedArray<Bool> const &);
template Bool anyGE(Int const &, MaskedArray<Int> const &);
template Bool anyGT(Array<Int> const &, MaskedArray<Int> const &);
template Bool anyLE(MaskedArray<Int> const &, Array<Int> const &);
template Bool anyLT(MaskedArray<Int> const &, Int const &);
template Bool anyLT(MaskedArray<Int> const &, MaskedArray<Int> const &);
template Bool anyOR(Array<Bool> const &, MaskedArray<Bool> const &);
template Bool anyOR(Bool const &, MaskedArray<Bool> const &);
template Bool anyOR(MaskedArray<Bool> const &, Array<Bool> const &);
template Bool anyOR(MaskedArray<Bool> const &, Bool const &);
template Bool anyOR(MaskedArray<Bool> const &, MaskedArray<Bool> const &);
template MaskedLogicalArray operator!(MaskedArray<Bool> const &);
template MaskedLogicalArray operator&&(Bool const &, MaskedArray<Bool> const &);
template MaskedLogicalArray operator&&(MaskedArray<Bool> const &, Bool const &);
template MaskedLogicalArray operator<=(Array<Int> const &, MaskedArray<Int> const &);
template MaskedLogicalArray operator<=(MaskedArray<Int> const &, Array<Int> const &);
template MaskedLogicalArray operator<=(MaskedArray<Int> const &, Int const &);
template MaskedLogicalArray operator<=(MaskedArray<Int> const &, MaskedArray<Int> const &);
template MaskedLogicalArray operator>(Int const &, MaskedArray<Int> const &);
template MaskedLogicalArray operator||(Bool const &, MaskedArray<Bool> const &);
template MaskedLogicalArray operator||(MaskedArray<Bool> const &, Bool const &);
} //# NAMESPACE - END
