// CountedPtr_1110.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/LatticeIterInterface.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<LatticeIterInterface<Float> >;
template class CountedConstPtr<LatticeIterInterface<Float> >;
template class SimpleCountedPtr<LatticeIterInterface<Float> >;
template class SimpleCountedConstPtr<LatticeIterInterface<Float> >;
template class PtrRep<LatticeIterInterface<Float> >;
} //# NAMESPACE - END
