// CountedPtr_1010.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/MaskedLattice.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<MaskedLattice<Float> >;
template class CountedPtr<MaskedLattice<Float> >;
template class PtrRep<MaskedLattice<Float> >;
template class SimpleCountedConstPtr<MaskedLattice<Float> >;
template class SimpleCountedPtr<MaskedLattice<Float> >;
template class CountedConstPtr<MaskedLattice<Double> >;
template class CountedPtr<MaskedLattice<Double> >;
template class PtrRep<MaskedLattice<Double> >;
template class SimpleCountedConstPtr<MaskedLattice<Double> >;
template class SimpleCountedPtr<MaskedLattice<Double> >;
template class CountedConstPtr<MaskedLattice<Complex> >;
template class CountedPtr<MaskedLattice<Complex> >;
template class PtrRep<MaskedLattice<Complex> >;
template class SimpleCountedConstPtr<MaskedLattice<Complex> >;
template class SimpleCountedPtr<MaskedLattice<Complex> >;
template class CountedConstPtr<MaskedLattice<DComplex> >;
template class CountedPtr<MaskedLattice<DComplex> >;
template class PtrRep<MaskedLattice<DComplex> >;
template class SimpleCountedConstPtr<MaskedLattice<DComplex> >;
template class SimpleCountedPtr<MaskedLattice<DComplex> >;
template class CountedConstPtr<MaskedLattice<Bool> >;
template class CountedPtr<MaskedLattice<Bool> >;
template class PtrRep<MaskedLattice<Bool> >;
template class SimpleCountedConstPtr<MaskedLattice<Bool> >;
template class SimpleCountedPtr<MaskedLattice<Bool> >;
} //# NAMESPACE - END
