// ExtendLattice_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/ExtendLattice.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class ExtendLattice<Float>;
template class ExtendLattice<Complex>;
template class ExtendLattice<Bool>;
} //# NAMESPACE - END
