// LatticeIterator_1040.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LatticeIterator.cc>
#include <lattices/Lattices/LatticeIterInterface.cc>
namespace casa { //# NAMESPACE - BEGIN
template class LatticeIterator<Int>;
template class RO_LatticeIterator<Int>;
template class LatticeIterInterface<Int>;
} //# NAMESPACE - END
