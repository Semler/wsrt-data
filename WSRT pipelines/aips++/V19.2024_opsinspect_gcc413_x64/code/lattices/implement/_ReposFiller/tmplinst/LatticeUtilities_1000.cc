// LatticeUtilities_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LatticeUtilities.cc>
#include <casa/Arrays/IPosition.h>
#include <lattices/Lattices/Lattice.h>
namespace casa { //# NAMESPACE - BEGIN
template void minMax(Float &, Float &, IPosition &, IPosition &, Lattice<Float> const &);
#include <casa/Logging/LogIO.h>
#include <lattices/Lattices/MaskedLattice.h>
template void LatticeUtilities::copyDataAndMask<Float>(LogIO &, MaskedLattice<Float> &, MaskedLattice<Float> const &, Bool);
#include <casa/BasicSL/Complex.h>
template void LatticeUtilities::copyDataAndMask<Complex>(LogIO &, MaskedLattice<Complex> &, MaskedLattice<Complex> const &, Bool);
#include <casa/Arrays/Array.h>
template void LatticeUtilities::collapse<Float>(Array<Float> &, IPosition const &, MaskedLattice<Float> const &, Bool);
template void LatticeUtilities::collapse<Float>(Array<Float> &, Array<Bool> &, IPosition const &, MaskedLattice<Float> const &, Bool, Bool, Bool);
#include <casa/Arrays/Slicer.h>
template void LatticeUtilities::replicate<Float>(Lattice<Float> &, Slicer const &, Array<Float> const &);
template void LatticeUtilities::replicate<Bool>(Lattice<Bool> &, Slicer const &, Array<Bool> const &);
template void LatticeUtilities::addDegenerateAxes<Float>(Lattice<Float>* &, Lattice<Float> const &, uInt);
} //# NAMESPACE - END
