// CountedPtr_1130.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/LatticeIterInterface.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<LatticeIterInterface<uInt> >;
template class CountedConstPtr<LatticeIterInterface<uInt> >;
template class SimpleCountedPtr<LatticeIterInterface<uInt> >;
template class SimpleCountedConstPtr<LatticeIterInterface<uInt> >;
template class PtrRep<LatticeIterInterface<uInt> >;
} //# NAMESPACE - END
