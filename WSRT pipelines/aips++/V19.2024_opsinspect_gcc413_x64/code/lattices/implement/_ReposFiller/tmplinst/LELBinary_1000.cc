// LELBinary_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LELBinary.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class LELBinary<Float>;
template class LELBinary<Double>;
template class LELBinary<Complex>;
template class LELBinary<DComplex>;
template class LELBinaryCmp<Float>;
template class LELBinaryCmp<Double>;
template class LELBinaryCmp<Complex>;
template class LELBinaryCmp<DComplex>;
} //# NAMESPACE - END
