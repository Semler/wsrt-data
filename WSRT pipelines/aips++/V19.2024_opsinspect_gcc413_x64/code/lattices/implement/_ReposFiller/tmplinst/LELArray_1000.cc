// LELArray_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LELArray.cc>
#include <lattices/Lattices/LELScalar.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class LELArray<Bool>;
template class LELArray<Float>;
template class LELArray<Double>;
template class LELArray<Complex>;
template class LELArray<DComplex>;
template class LELArrayRef<Bool>;
template class LELArrayRef<Float>;
template class LELArrayRef<Double>;
template class LELArrayRef<Complex>;
template class LELArrayRef<DComplex>;
template class LELScalar<Bool>;
template class LELScalar<Float>;
template class LELScalar<Double>;
template class LELScalar<Complex>;
template class LELScalar<DComplex>;
} //# NAMESPACE - END
