// CountedPtr_1080.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/LatticeIterInterface.h>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<LatticeIterInterface<DComplex> >;
template class CountedConstPtr<LatticeIterInterface<DComplex> >;
template class SimpleCountedPtr<LatticeIterInterface<DComplex> >;
template class SimpleCountedConstPtr<LatticeIterInterface<DComplex> >;
template class PtrRep<LatticeIterInterface<DComplex> >;
} //# NAMESPACE - END
