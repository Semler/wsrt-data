// CountedPtr_1100.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <lattices/Lattices/LatticeIterInterface.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedPtr<LatticeIterInterface<Double> >;
template class CountedConstPtr<LatticeIterInterface<Double> >;
template class SimpleCountedPtr<LatticeIterInterface<Double> >;
template class SimpleCountedConstPtr<LatticeIterInterface<Double> >;
template class PtrRep<LatticeIterInterface<Double> >;
} //# NAMESPACE - END
