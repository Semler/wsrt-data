// LELConvert_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LELConvert.cc>
#include <casa/Arrays/ArrayMath.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class LELConvert<Double, Float>;
template class LELConvert<Complex, Float>;
template class LELConvert<DComplex, Float>;
template class LELConvert<Float, Double>;
template class LELConvert<Complex, Double>;
template class LELConvert<DComplex, Double>;
template class LELConvert<DComplex, Complex>;
template class LELConvert<Complex, DComplex>;
} //# NAMESPACE - END
