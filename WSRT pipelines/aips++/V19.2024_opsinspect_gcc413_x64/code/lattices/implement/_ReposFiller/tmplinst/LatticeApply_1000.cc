// LatticeApply_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LatticeApply.cc>
#include <lattices/Lattices/LineCollapser.cc>
#include <lattices/Lattices/TiledCollapser.cc>
namespace casa { //# NAMESPACE - BEGIN
template class LatticeApply<Int>;
template class LineCollapser<Int>;
template class TiledCollapser<Int>;
template class LatticeApply<Float>;
template class LineCollapser<Float>;
template class TiledCollapser<Float>;
} //# NAMESPACE - END
