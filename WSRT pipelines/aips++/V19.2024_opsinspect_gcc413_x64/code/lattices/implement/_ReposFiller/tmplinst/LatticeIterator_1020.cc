// LatticeIterator_1020.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/LatticeIterator.cc>
#include <lattices/Lattices/LatticeIterInterface.cc>
namespace casa { //# NAMESPACE - BEGIN
template class LatticeIterator<Double>;
template class RO_LatticeIterator<Double>;
template class LatticeIterInterface<Double>;
} //# NAMESPACE - END
