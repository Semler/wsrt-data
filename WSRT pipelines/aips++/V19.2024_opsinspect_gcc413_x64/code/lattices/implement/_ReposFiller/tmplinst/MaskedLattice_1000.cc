// MaskedLattice_1000.cc -- Tue Apr  1 12:15:47 BST 2008 -- renting
#include <lattices/Lattices/MaskedLattice.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class MaskedLattice<Int>;
template class MaskedLattice<Float>;
template class MaskedLattice<Double>;
template class MaskedLattice<Complex>;
template class MaskedLattice<DComplex>;
template class MaskedLattice<Bool>;
} //# NAMESPACE - END
