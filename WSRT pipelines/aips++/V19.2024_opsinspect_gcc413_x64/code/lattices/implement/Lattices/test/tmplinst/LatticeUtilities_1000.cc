// LatticeUtilities_1000.cc -- Tue Apr  1 12:15:01 BST 2008 -- renting
#include <lattices/Lattices/LatticeUtilities.cc>
#include <casa/Arrays/MaskedArray.cc>
namespace casa { //# NAMESPACE - BEGIN
template void LatticeUtilities::bin<Float>(MaskedArray<Float> &, MaskedArray<Float> const &, uInt, uInt);
} //# NAMESPACE - END
