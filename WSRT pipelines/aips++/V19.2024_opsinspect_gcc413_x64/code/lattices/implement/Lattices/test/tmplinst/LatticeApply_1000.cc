// LatticeApply_1000.cc -- Tue Apr  1 12:15:01 BST 2008 -- renting
#include <lattices/Lattices/LatticeApply.cc>
#include <lattices/Lattices/LineCollapser.cc>
#include <lattices/Lattices/TiledCollapser.cc>
namespace casa { //# NAMESPACE - BEGIN
template class LatticeApply<Complex>;
template class LineCollapser<Complex>;
template class TiledCollapser<Complex>;
} //# NAMESPACE - END
