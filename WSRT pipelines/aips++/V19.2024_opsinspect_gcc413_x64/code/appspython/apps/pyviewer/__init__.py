import os
import distutils.sysconfig

# if no AIPSPATH assume the data/gui directory is in the python module
if not os.environ.has_key("AIPSPATH"):
    os.environ["AIPSPATH"] = "%s dummy dummy dummy" % __path__

from _pyviewer import viewer
