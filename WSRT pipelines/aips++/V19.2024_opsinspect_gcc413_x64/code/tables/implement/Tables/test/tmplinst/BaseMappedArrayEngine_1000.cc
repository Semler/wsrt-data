// BaseMappedArrayEngine_1000.cc -- Tue Apr  1 12:09:54 BST 2008 -- renting
#include <tables/Tables/BaseMappedArrayEngine.cc>
#include <tables/Tables/ScaledArrayEngine.cc>
namespace casa { //# NAMESPACE - BEGIN
template class BaseMappedArrayEngine<Double, Int>;
template class BaseMappedArrayEngine<Float, uChar>;
template class ScaledArrayEngine<Double, Int>;
template class ScaledArrayEngine<Float, uChar>;
} //# NAMESPACE - END
