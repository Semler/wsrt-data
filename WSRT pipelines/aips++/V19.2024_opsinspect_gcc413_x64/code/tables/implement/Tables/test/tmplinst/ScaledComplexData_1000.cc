// ScaledComplexData_1000.cc -- Tue Apr  1 12:09:54 BST 2008 -- renting
#include <tables/Tables/ScaledComplexData.cc>
#include <tables/Tables/BaseMappedArrayEngine.cc>
#include <tables/Tables/VirtArrCol.cc>
#include <casa/BasicSL/Complex.h>
namespace casa { //# NAMESPACE - BEGIN
template class BaseMappedArrayEngine<DComplex, Int>;
template class BaseMappedArrayEngine<Complex, Short>;
template class ScaledComplexData<DComplex, Int>;
template class ScaledComplexData<Complex, Short>;
template class VirtualArrayColumn<DComplex>;
} //# NAMESPACE - END
