// CountedPtr_1000.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <casa/Arrays/IPosition.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<IPosition> >;
template class CountedPtr<Block<IPosition> >;
template class PtrRep<Block<IPosition> >;
template class SimpleCountedConstPtr<Block<IPosition> >;
template class SimpleCountedPtr<Block<IPosition> >;
} //# NAMESPACE - END
