// CountedPtr_1030.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <tables/Tables/Table.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<Table> >;
template class CountedPtr<Block<Table> >;
template class PtrRep<Block<Table> >;
template class SimpleCountedConstPtr<Block<Table> >;
template class SimpleCountedPtr<Block<Table> >;
} //# NAMESPACE - END
