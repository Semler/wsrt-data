// LoggerHolder_1000.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <tables/LogTables/LoggerHolder.h>
#include <casa/Utilities/CountedPtr.cc>
namespace casa { //# NAMESPACE - BEGIN
template class PtrRep<LoggerHolderRep>;
template class CountedPtr<LoggerHolderRep>;
template class CountedConstPtr<LoggerHolderRep>;
template class SimpleCountedPtr<LoggerHolderRep>;
template class SimpleCountedConstPtr<LoggerHolderRep>;
} //# NAMESPACE - END
