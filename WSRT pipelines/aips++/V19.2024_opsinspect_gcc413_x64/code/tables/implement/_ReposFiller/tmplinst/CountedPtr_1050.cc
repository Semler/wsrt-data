// CountedPtr_1050.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <casa/Utilities/CountedPtr.cc>
#include <casa/Containers/Block.h>
#include <tables/Tables/TableRecord.h>
namespace casa { //# NAMESPACE - BEGIN
template class CountedConstPtr<Block<TableRecord> >;
template class CountedPtr<Block<TableRecord> >;
template class PtrRep<Block<TableRecord> >;
template class SimpleCountedConstPtr<Block<TableRecord> >;
template class SimpleCountedPtr<Block<TableRecord> >;
} //# NAMESPACE - END
