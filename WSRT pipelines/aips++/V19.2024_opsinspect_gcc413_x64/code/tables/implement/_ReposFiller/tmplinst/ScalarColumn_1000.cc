// ScalarColumn_1000.cc -- Tue Apr  1 12:08:29 BST 2008 -- renting
#include <tables/Tables/ScalarColumn.cc>
#include <casa/BasicSL/Complex.h>
#include <casa/BasicSL/String.h>
#include <tables/Tables/TableRecord.h>
namespace casa { //# NAMESPACE - BEGIN
template class ROScalarColumn<Bool>;
template class ROScalarColumn<uChar>;
template class ROScalarColumn<Short>;
template class ROScalarColumn<uShort>;
template class ROScalarColumn<Int>;
template class ROScalarColumn<uInt>;
template class ROScalarColumn<Float>;
template class ROScalarColumn<Double>;
template class ROScalarColumn<Complex>;
template class ROScalarColumn<DComplex>;
template class ROScalarColumn<String>;
template class ROScalarColumn<TableRecord>;
template class ScalarColumn<Bool>;
template class ScalarColumn<uChar>;
template class ScalarColumn<Short>;
template class ScalarColumn<uShort>;
template class ScalarColumn<Int>;
template class ScalarColumn<uInt>;
template class ScalarColumn<Float>;
template class ScalarColumn<Double>;
template class ScalarColumn<Complex>;
template class ScalarColumn<DComplex>;
template class ScalarColumn<String>;
template class ScalarColumn<TableRecord>;
} //# NAMESPACE - END
