//# images.dox: doxygen description of images package
//# Copyright (C) 2005
//# Associated Universities, Inc. Washington DC, USA.
//#
//# This library is free software; you can redistribute it and/or modify it
//# under the terms of the GNU Library General Public License as published by
//# the Free Software Foundation; either version 2 of the License, or (at your
//# option) any later version.
//#
//# This library is distributed in the hope that it will be useful, but WITHOUT
//# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
//# License for more details.
//#
//# You should have received a copy of the GNU Library General Public License
//# along with this library; if not, write to the Free Software Foundation,
//# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//#
//# Correspondence concerning AIPS++ should be addressed as follows:
//#        Internet email: aips2-request@nrao.edu.
//#        Postal address: AIPS++ Project Office
//#                        National Radio Astronomy Observatory
//#                        520 Edgemont Road
//#                        Charlottesville, VA 22903-2475 USA
//#
//# $Id: images.dox,v 1.2 2007/11/07 08:01:21 gvandiep Exp $

namespace casa {

// \defgroup images images package (libimages)
//
// The images package handles N-dimensional images, their masks,
// coordinates, and auxiliary info like history.
// <br> It is possible to use virtual images like:
// <ul>
//  <li> slices of images
//  <li> regions in images
//  <li> expressions of images
//   (using <a href="http://www.astron.nl/aips++/docs/notes/223">LEL</a>)
//  <li> concatenation of images
// </ul>
//
// It is built on top of the
// <a href="group__lattices.html">lattices</a> module.

}
