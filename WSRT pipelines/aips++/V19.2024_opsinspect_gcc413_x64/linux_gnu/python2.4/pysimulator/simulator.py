from _pysimulator import Simulator
from pyquanta import quanta
from pymeasures import measures, is_measure

class simulator(Simulator):
    def __init__(self, msname = None):
	self.dq = quanta()
	self.dm = measures()
	Simulator.__init__(self, msname)
    
    def setdata(self, spwid = 0, fieldid = 0, msselect = ' '):
	return Simulator.setdata(self, spwid, fieldid, msselect)

    def setconfig(self, telname='VLA', x=0.0, y=0.0, z=0.0,
		  dishDiameter=0.0, offset=0.0, mount='', antName='',
		  coordSys='global', refPos=None):
	if refPos is None:
	    refPos = self.dm.observatory('VLA')
	return Simulator.setconfig(self, telname, x, y, z,
				   dishDiameter, offset, mount, antName,
				   coordSys, refPos)

    def setspwindow(self, spwname='XBAND', freq='8.0GHz', deltaFreq='50.0MHz',
		    freqResolution='50.0MHz', nchannels=1, stokes='RR LL'):
	return Simulator.setspwindow(self, spwname, self.dq.quantity(freq),
				     self.dq.quantity(deltaFreq),
				     self.dq.quantity(freqResolution),
				     nchannels, stokes)

    def setfield(self, srcname='unknown', srcdirection=None, calcnode='',
		 distance='0km'):
	if srcdirection is None:
	    srcdirection = self.dm.direction('J2000', '0deg', '90deg')
	return Simulator.setfield(self, srcname, srcdirection, calcnode, self.dq.quantity(distance))

    def setlimits(self, shadowLimit=1e-6, elevationLimit='8deg'):
	return Simulator.setlimits(self, shadowLimit, self.dq.quantity(elevationLimit))

    def setfeed(self, mode='perfect R L', x=0.0, y=0.0, pol=''):
	return Simulator.setfeed(self, mode, x, y, pol)

    def settimes(self, integrationTime='10s', useHourAngle=True, refTime=None):
	if refTime is None:
	   refTime = self.dm.epoch('UTC', 'today')
	return Simulator.settimes(self, self.dq.quantity(integrationTime), useHourAngle, refTime)

    def observe(self, sourceName='', spwName='', startTime='0s', stopTime='3600s'):
	return Simulator.observe(self, sourceName, spwName, self.dq.quantity(startTime), self.dq.quantity(stopTime))

    def setvp(self, doVp=True, defaultVp=True, vpTable='', doSquint=True, parAngleInc='360deg', skyPosThreshold='180deg', pbLimit=0.01):
	return Simulator.setvp(self, doVp, defaultVp, vpTable, doSquint,
			       self.dq.quantity(parAngleInc),
			       self.dq.quantity(skyPosThreshold),
			       pbLimit)
 
    def predict(self, modelImage='', compList='', incremental=False):
	return Simulator.predict(self, modelImage, compList, incremental)

