import codecs
import struct

file = open('Reading_MT/F000001.MT','rb')

data = file.read()

file.close()


"""
Dataset contains fixed blocks of blocklength 3840 bytes (characters in EBCDIC code)
each block 30 records of 128 bytes
first 24 bytes contain identification data

Block nr    | Type

1           | File Descriptor
2,3         | Observation Header
...



Use codecs.decode(data, 'cp500') to read strings (C)
Use struct.unpack('>l',data) to read 4byte long (F?)
"""
#Read first block of data
block_1 = data[:3840:]

record_size = 128
records = 30
id_data = block_1[:24]
block_1 = [block_1[24+i:24+i+record_size] for i in range(0,records*record_size, record_size)]
print((block_1[0:2]))
#print(codecs.decode(block_1[0],'unicode-escape'))
print(type(block_1[0]))