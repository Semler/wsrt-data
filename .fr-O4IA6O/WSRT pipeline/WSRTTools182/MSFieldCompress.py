#!/usr/bin/env python
#
# $Id: MSFieldCompress.py,v 1.3 2008/04/21 21:09:41 schoenma Exp $
#
# Script: MSFieldCompress.py
# Author: A.P. Schoenmakers (ASTRON)
#
# This script compresses the FIELD and SOURCE table of a Measurement Set.
# It leaves only the unique entries in these tables and adapts the FIELD_ID
# references in the MAIN table.
# This is convenient if the observation was done using a mosaic
# pattern such as A -> B -> C -> A -> B -> C etc. All these pointings
# end up individually in the FIELD and SOURCE table. When converting the
# MS to UVFits, this would lead to a large SU table in the UVFits
# file. By compressing the list in the FIELD and SOURCE table beforehand, the
# created SU table is much shorter. This makes it much easier to
# collect all data related to a single pointing. 
#
# Usage: MSFieldCompress.py <MSName>
#
# IMPORTANT: The input MS will be adapted; reversing the process is not
# possible! 
#

# This we need for commandline handling and file checking
import os,sys

# This we need for MS interfacing
from pyrap.tables import *

# Numpy has several methods for array comparison and index finding
from numpy import *

def MSFieldCompress(ms):

    # Open the FIELD table.
    field = table(ms.getkeyword('FIELD'),readonly=False,ack=False)
    fieldname = field.name()
    fieldrows = field.nrows()
    if (fieldrows == 1):
        print "Field table only has one row; nothing to do!"
        sys.exit(0)

    # The NAME column holds the names of the mosaicing position. Multiple
    # entries can have the same NAME.
    fieldnames = array(field.getcol("NAME"))
    uniq_fieldname = list(set(fieldnames));
    new_fieldrows = len(uniq_fieldname)
    if (new_fieldrows == fieldrows):
        print "FIELD table already contains unique entries only; nothing to do!"
        sys.exit(0)
        
    print "Compressing FIELD subtable from "+str(fieldrows)+" to "+str(new_fieldrows)+" entries"

    # old_index holds the current entries in the FIELD table, new_index will
    # contain the indices of the remaining (unique) entries.
    old_index = arange(len(fieldnames))
    new_index = copy(old_index)
    # Create a temporary compressed FIELD table, this will be copied into the 
    # input MS later.
    field.copy("TMP_FIELD",copynorows=True)
    tmp_field = table("TMP_FIELD",readonly=False,ack=False)

    # Check the SOURCE table.
    source = table(ms.getkeyword('SOURCE'),readonly=False,ack=False)
    sourcename = source.name()
    sourcerows = source.nrows()
    if (sourcerows != fieldrows and sourcerows > 0):
        print "Number of entries in SOURCE table does not match that in FIELD table;"
        print "Cannot proceed"
        exit(1)

    # Create a temporary source table
    source.copy("TMP_SOURCE",copynorows=True)
    tmp_source = table("TMP_SOURCE",readonly=False,ack=False)

    # Create the array new_index that holds the new index for each entry
    # in the FIELD table.
    # Also, copy each unique name entry in the FIELD table to the temporary
    # table. Also for SOURCE entries if there is a valid SOURCE table.
    for index,uf in enumerate(uniq_fieldname):
       match = where(fieldnames == uf)
       new_index[match] = index
       old_row = match[0][0] # First match
       field.copyrows(tmp_field,startrowin=old_row,nrow=1)
       if (sourcerows > 0):
           tmp_field.putcell("SOURCE_ID",index,index)
           source.copyrows(tmp_source,startrowin=old_row,nrow=1)
           tmp_source.putcell("SOURCE_ID",index,index)

    field.close()
    del field
    source.close()
    del source
    # Copy the compressed FIELD table to the input MS
    tmp_field.flush()
    tmp_field.copy(fieldname,deep=True)
    tmp_field.close()
    tabledelete("TMP_FIELD",ack=False)

    # Copy the compressed SOURCE table to the input MS
    tmp_source.flush()
    tmp_source.copy(sourcename,deep=True)
    tmp_source.close()
    tabledelete("TMP_SOURCE",ack=False)

    # Now adapt the FIELD_ID pointers in the MAIN table
    field_id = ms.getcol("FIELD_ID")
    tmp = copy(field_id)
    for i in old_index:
        tmp[field_id == i] = new_index[i]
        
    ms.putcol("FIELD_ID",tmp)
    ms.flush()

#
# MAIN program starts here
#
if (len(sys.argv) == 2):
    msname = sys.argv[1]
    
else:
    print "Usage:"
    print "\t"+sys.argv[0]+" <MS>"
    print "\t  <MS> is required"
    sys.exit(1)

if (os.path.isdir(msname) == 0):
    print "Could not find MS: "+msname
    sys.exit(1)
    
ms = table(msname,readonly=False,ack=False)
MSFieldCompress(ms)
ms.close()
print "Done"

sys.exit(0)

